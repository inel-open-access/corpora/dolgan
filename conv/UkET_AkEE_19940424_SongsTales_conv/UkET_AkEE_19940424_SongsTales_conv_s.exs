<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDC63C404E-6989-7EAD-C4C9-8F270DB7B61C">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>UkET_AkEE_19940424_LifeSongsTales_conv</transcription-name>
         <referenced-file url="UkET_AkEE_19940424_SongsTales_conv.wav" />
         <referenced-file url="UkET_AkEE_19940424_SongsTales_conv.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\conv\UkET_AkEE_19940424_SongsTales_conv\UkET_AkEE_19940424_SongsTales_conv.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">1610</ud-information>
            <ud-information attribute-name="# HIAT:w">1108</ud-information>
            <ud-information attribute-name="# e">1107</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">3</ud-information>
            <ud-information attribute-name="# HIAT:u">156</ud-information>
            <ud-information attribute-name="# sc">76</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="AkEE">
            <abbreviation>AkEE</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
         <speaker id="UkET">
            <abbreviation>UkET</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" time="0.0" type="appl" />
         <tli id="T2" time="0.725" type="appl" />
         <tli id="T3" time="1.45" type="appl" />
         <tli id="T4" time="2.175" type="appl" />
         <tli id="T5" time="2.9" type="appl" />
         <tli id="T6" time="3.625" type="appl" />
         <tli id="T7" time="4.35" type="appl" />
         <tli id="T8" time="5.075" type="appl" />
         <tli id="T9" time="5.8" type="appl" />
         <tli id="T10" time="6.4864999999999995" type="appl" />
         <tli id="T11" time="7.173" type="appl" />
         <tli id="T12" time="7.859500000000001" type="appl" />
         <tli id="T13" time="8.546" type="appl" />
         <tli id="T14" time="9.2325" type="appl" />
         <tli id="T15" time="9.919" type="appl" />
         <tli id="T16" time="10.548285714285715" type="appl" />
         <tli id="T17" time="11.17757142857143" type="appl" />
         <tli id="T18" time="11.806857142857144" type="appl" />
         <tli id="T19" time="12.436142857142858" type="appl" />
         <tli id="T20" time="13.065428571428573" type="appl" />
         <tli id="T21" time="13.669" type="appl" />
         <tli id="T22" time="13.694714285714285" type="appl" />
         <tli id="T23" time="14.0447" type="appl" />
         <tli id="T24" time="14.324" type="appl" />
         <tli id="T25" time="14.4204" type="appl" />
         <tli id="T26" time="14.796100000000001" type="appl" />
         <tli id="T27" time="15.171800000000001" type="appl" />
         <tli id="T28" time="15.5475" type="appl" />
         <tli id="T29" time="15.923200000000001" type="appl" />
         <tli id="T30" time="16.2989" type="appl" />
         <tli id="T31" time="16.6746" type="appl" />
         <tli id="T32" time="17.0503" type="appl" />
         <tli id="T33" time="17.426000000000002" type="appl" />
         <tli id="T34" time="17.8017" type="appl" />
         <tli id="T35" time="18.1774" type="appl" />
         <tli id="T36" time="18.5531" type="appl" />
         <tli id="T37" time="18.9288" type="appl" />
         <tli id="T38" time="19.3045" type="appl" />
         <tli id="T39" time="19.6802" type="appl" />
         <tli id="T40" time="20.0559" type="appl" />
         <tli id="T41" time="20.4316" type="appl" />
         <tli id="T42" time="20.807299999999998" type="appl" />
         <tli id="T43" time="21.183" type="appl" />
         <tli id="T44" time="21.630777777777777" type="appl" />
         <tli id="T45" time="22.078555555555557" type="appl" />
         <tli id="T46" time="22.526333333333334" type="appl" />
         <tli id="T47" time="22.97411111111111" type="appl" />
         <tli id="T48" time="23.42188888888889" type="appl" />
         <tli id="T49" time="23.869666666666667" type="appl" />
         <tli id="T50" time="24.317444444444444" type="appl" />
         <tli id="T51" time="24.765222222222224" type="appl" />
         <tli id="T52" time="25.213" type="appl" />
         <tli id="T53" time="26.022000000000002" type="appl" />
         <tli id="T54" time="26.831" type="appl" />
         <tli id="T55" time="27.64" type="appl" />
         <tli id="T56" time="28.193076923076923" type="appl" />
         <tli id="T57" time="28.746153846153845" type="appl" />
         <tli id="T58" time="29.299230769230768" type="appl" />
         <tli id="T59" time="29.852307692307694" type="appl" />
         <tli id="T60" time="30.405384615384616" type="appl" />
         <tli id="T61" time="30.95846153846154" type="appl" />
         <tli id="T62" time="31.51153846153846" type="appl" />
         <tli id="T63" time="32.06461538461539" type="appl" />
         <tli id="T64" time="32.61769230769231" type="appl" />
         <tli id="T65" time="33.17076923076923" type="appl" />
         <tli id="T66" time="33.723846153846154" type="appl" />
         <tli id="T67" time="34.276923076923076" type="appl" />
         <tli id="T68" time="34.83" type="appl" />
         <tli id="T69" time="35.363846153846154" type="appl" />
         <tli id="T70" time="35.89769230769231" type="appl" />
         <tli id="T71" time="36.43153846153846" type="appl" />
         <tli id="T72" time="36.965384615384615" type="appl" />
         <tli id="T73" time="37.49923076923077" type="appl" />
         <tli id="T74" time="38.033076923076926" type="appl" />
         <tli id="T75" time="38.566923076923075" type="appl" />
         <tli id="T76" time="39.10076923076923" type="appl" />
         <tli id="T77" time="39.63461538461539" type="appl" />
         <tli id="T78" time="40.16846153846154" type="appl" />
         <tli id="T79" time="40.70230769230769" type="appl" />
         <tli id="T80" time="41.23615384615385" type="appl" />
         <tli id="T81" time="41.77" type="appl" />
         <tli id="T82" time="42.982" type="appl" />
         <tli id="T83" time="43.5336" type="appl" />
         <tli id="T84" time="44.0852" type="appl" />
         <tli id="T85" time="44.6368" type="appl" />
         <tli id="T86" time="45.1884" type="appl" />
         <tli id="T87" time="45.74" type="appl" />
         <tli id="T88" time="46.415" type="appl" />
         <tli id="T89" time="47.09" type="appl" />
         <tli id="T90" time="47.765" type="appl" />
         <tli id="T91" time="48.44" type="appl" />
         <tli id="T92" time="49.04633333333333" type="appl" />
         <tli id="T93" time="49.65266666666667" type="appl" />
         <tli id="T94" time="50.259" type="appl" />
         <tli id="T95" time="50.727266666666665" type="appl" />
         <tli id="T96" time="51.19553333333334" type="appl" />
         <tli id="T97" time="51.6638" type="appl" />
         <tli id="T98" time="52.13206666666667" type="appl" />
         <tli id="T99" time="52.60033333333333" type="appl" />
         <tli id="T100" time="53.0686" type="appl" />
         <tli id="T101" time="53.53686666666667" type="appl" />
         <tli id="T102" time="54.00513333333333" type="appl" />
         <tli id="T103" time="54.4734" type="appl" />
         <tli id="T104" time="54.94166666666667" type="appl" />
         <tli id="T105" time="55.409933333333335" type="appl" />
         <tli id="T106" time="55.8782" type="appl" />
         <tli id="T107" time="56.34646666666667" type="appl" />
         <tli id="T108" time="56.814733333333336" type="appl" />
         <tli id="T109" time="57.283" type="appl" />
         <tli id="T110" time="57.794000000000004" type="appl" />
         <tli id="T111" time="58.305" type="appl" />
         <tli id="T112" time="58.816" type="appl" />
         <tli id="T113" time="59.327000000000005" type="appl" />
         <tli id="T114" time="59.838" type="appl" />
         <tli id="T115" time="60.349000000000004" type="appl" />
         <tli id="T116" time="60.86" type="appl" />
         <tli id="T117" time="61.371" type="appl" />
         <tli id="T118" time="61.882000000000005" type="appl" />
         <tli id="T119" time="62.393" type="appl" />
         <tli id="T120" time="62.904" type="appl" />
         <tli id="T121" time="63.525400000000005" type="appl" />
         <tli id="T122" time="64.1468" type="appl" />
         <tli id="T123" time="64.7682" type="appl" />
         <tli id="T124" time="65.3896" type="appl" />
         <tli id="T125" time="66.011" type="appl" />
         <tli id="T126" time="66.708" type="appl" />
         <tli id="T127" time="67.405" type="appl" />
         <tli id="T128" time="68.102" type="appl" />
         <tli id="T129" time="68.79899999999999" type="appl" />
         <tli id="T130" time="69.496" type="appl" />
         <tli id="T131" time="70.193" type="appl" />
         <tli id="T132" time="70.89" type="appl" />
         <tli id="T133" time="71.587" type="appl" />
         <tli id="T134" time="72.284" type="appl" />
         <tli id="T135" time="72.98100000000001" type="appl" />
         <tli id="T136" time="73.678" type="appl" />
         <tli id="T137" time="74.375" type="appl" />
         <tli id="T138" time="75.072" type="appl" />
         <tli id="T139" time="75.769" type="appl" />
         <tli id="T140" time="76.29275000000001" type="appl" />
         <tli id="T141" time="76.8165" type="appl" />
         <tli id="T142" time="77.34025" type="appl" />
         <tli id="T143" time="77.864" type="appl" />
         <tli id="T144" time="78.38775000000001" type="appl" />
         <tli id="T145" time="78.9115" type="appl" />
         <tli id="T146" time="79.43525" type="appl" />
         <tli id="T147" time="79.959" type="appl" />
         <tli id="T148" time="80.48275000000001" type="appl" />
         <tli id="T149" time="81.0065" type="appl" />
         <tli id="T150" time="81.53025" type="appl" />
         <tli id="T151" time="82.054" type="appl" />
         <tli id="T152" time="82.57775000000001" type="appl" />
         <tli id="T153" time="83.1015" type="appl" />
         <tli id="T154" time="83.62525" type="appl" />
         <tli id="T155" time="84.149" type="appl" />
         <tli id="T156" time="84.7705" type="appl" />
         <tli id="T157" time="85.392" type="appl" />
         <tli id="T158" time="86.0135" type="appl" />
         <tli id="T159" time="86.635" type="appl" />
         <tli id="T160" time="87.2565" type="appl" />
         <tli id="T161" time="87.878" type="appl" />
         <tli id="T162" time="88.4995" type="appl" />
         <tli id="T163" time="89.121" type="appl" />
         <tli id="T164" time="89.74249999999999" type="appl" />
         <tli id="T165" time="90.36399999999999" type="appl" />
         <tli id="T166" time="90.9855" type="appl" />
         <tli id="T167" time="91.607" type="appl" />
         <tli id="T168" time="92.2285" type="appl" />
         <tli id="T169" time="92.65961544374514" />
         <tli id="T170" time="93.35357142857143" type="appl" />
         <tli id="T171" time="93.85714285714285" type="appl" />
         <tli id="T172" time="94.36071428571428" type="appl" />
         <tli id="T173" time="94.86428571428571" type="appl" />
         <tli id="T174" time="95.36785714285715" type="appl" />
         <tli id="T175" time="95.87142857142857" type="appl" />
         <tli id="T176" time="96.375" type="appl" />
         <tli id="T177" time="96.87857142857143" type="appl" />
         <tli id="T178" time="97.38214285714285" type="appl" />
         <tli id="T179" time="97.88571428571429" type="appl" />
         <tli id="T180" time="98.38928571428572" type="appl" />
         <tli id="T181" time="98.89285714285714" type="appl" />
         <tli id="T182" time="99.39642857142857" type="appl" />
         <tli id="T183" time="99.9" type="appl" />
         <tli id="T184" time="100.3216" type="appl" />
         <tli id="T185" time="100.7432" type="appl" />
         <tli id="T186" time="101.1648" type="appl" />
         <tli id="T187" time="101.413" type="appl" />
         <tli id="T188" time="101.5864" type="appl" />
         <tli id="T189" time="101.9044" type="appl" />
         <tli id="T190" time="102.008" type="appl" />
         <tli id="T191" time="102.3958" type="appl" />
         <tli id="T192" time="102.8872" type="appl" />
         <tli id="T193" time="103.3786" type="appl" />
         <tli id="T194" time="103.87" type="appl" />
         <tli id="T195" time="104.362875" type="appl" />
         <tli id="T196" time="104.85575" type="appl" />
         <tli id="T197" time="105.348625" type="appl" />
         <tli id="T198" time="105.8415" type="appl" />
         <tli id="T199" time="106.33437500000001" type="appl" />
         <tli id="T200" time="106.82725" type="appl" />
         <tli id="T201" time="107.320125" type="appl" />
         <tli id="T202" time="107.813" type="appl" />
         <tli id="T203" time="108.304" type="appl" />
         <tli id="T204" time="108.795" type="appl" />
         <tli id="T205" time="109.286" type="appl" />
         <tli id="T206" time="109.777" type="appl" />
         <tli id="T207" time="110.268" type="appl" />
         <tli id="T208" time="110.759" type="appl" />
         <tli id="T209" time="111.13287210930508" />
         <tli id="T210" time="111.59311764705882" type="appl" />
         <tli id="T211" time="111.93623529411765" type="appl" />
         <tli id="T212" time="112.27935294117647" type="appl" />
         <tli id="T213" time="112.62247058823529" type="appl" />
         <tli id="T214" time="112.96558823529412" type="appl" />
         <tli id="T215" time="113.30870588235294" type="appl" />
         <tli id="T216" time="113.65182352941176" type="appl" />
         <tli id="T217" time="113.99494117647059" type="appl" />
         <tli id="T218" time="114.33805882352941" type="appl" />
         <tli id="T219" time="114.68117647058824" type="appl" />
         <tli id="T220" time="115.02429411764706" type="appl" />
         <tli id="T221" time="115.36741176470588" type="appl" />
         <tli id="T222" time="115.71052941176471" type="appl" />
         <tli id="T223" time="116.05364705882353" type="appl" />
         <tli id="T224" time="116.39676470588235" type="appl" />
         <tli id="T225" time="116.73988235294118" type="appl" />
         <tli id="T226" time="117.083" type="appl" />
         <tli id="T227" time="117.48828571428571" type="appl" />
         <tli id="T228" time="117.89357142857143" type="appl" />
         <tli id="T229" time="118.29885714285714" type="appl" />
         <tli id="T230" time="118.70414285714286" type="appl" />
         <tli id="T231" time="119.10942857142857" type="appl" />
         <tli id="T232" time="119.51471428571429" type="appl" />
         <tli id="T233" time="119.92" type="appl" />
         <tli id="T234" time="120.28672222222222" type="appl" />
         <tli id="T235" time="120.65344444444445" type="appl" />
         <tli id="T236" time="121.02016666666667" type="appl" />
         <tli id="T237" time="121.38688888888889" type="appl" />
         <tli id="T238" time="121.75361111111111" type="appl" />
         <tli id="T239" time="122.12033333333333" type="appl" />
         <tli id="T240" time="122.48705555555556" type="appl" />
         <tli id="T241" time="122.85377777777778" type="appl" />
         <tli id="T242" time="123.2205" type="appl" />
         <tli id="T243" time="123.58722222222222" type="appl" />
         <tli id="T244" time="123.95394444444445" type="appl" />
         <tli id="T245" time="124.32066666666667" type="appl" />
         <tli id="T246" time="124.68738888888889" type="appl" />
         <tli id="T247" time="125.05411111111111" type="appl" />
         <tli id="T248" time="125.42083333333333" type="appl" />
         <tli id="T249" time="125.78755555555556" type="appl" />
         <tli id="T250" time="126.15427777777778" type="appl" />
         <tli id="T251" time="126.521" type="appl" />
         <tli id="T252" time="127.2555" type="appl" />
         <tli id="T253" time="127.99" type="appl" />
         <tli id="T254" time="128.4094" type="appl" />
         <tli id="T255" time="128.8288" type="appl" />
         <tli id="T256" time="129.2482" type="appl" />
         <tli id="T257" time="129.6676" type="appl" />
         <tli id="T258" time="130.087" type="appl" />
         <tli id="T259" time="130.50639999999999" type="appl" />
         <tli id="T260" time="130.92579999999998" type="appl" />
         <tli id="T261" time="131.34519999999998" type="appl" />
         <tli id="T262" time="131.7646" type="appl" />
         <tli id="T263" time="132.184" type="appl" />
         <tli id="T264" time="132.6034" type="appl" />
         <tli id="T265" time="133.0228" type="appl" />
         <tli id="T266" time="133.44219999999999" type="appl" />
         <tli id="T267" time="133.86159999999998" type="appl" />
         <tli id="T268" time="134.28099999999998" type="appl" />
         <tli id="T269" time="134.7004" type="appl" />
         <tli id="T270" time="135.1198" type="appl" />
         <tli id="T271" time="135.5392" type="appl" />
         <tli id="T272" time="135.9586" type="appl" />
         <tli id="T273" time="136.378" type="appl" />
         <tli id="T274" time="137.075" type="appl" />
         <tli id="T275" time="137.772" type="appl" />
         <tli id="T276" time="138.469" type="appl" />
         <tli id="T277" time="139.166" type="appl" />
         <tli id="T278" time="139.64414285714287" type="appl" />
         <tli id="T279" time="140.1222857142857" type="appl" />
         <tli id="T280" time="140.60042857142858" type="appl" />
         <tli id="T281" time="141.07857142857142" type="appl" />
         <tli id="T282" time="141.5567142857143" type="appl" />
         <tli id="T283" time="142.03485714285713" type="appl" />
         <tli id="T284" time="142.513" type="appl" />
         <tli id="T285" time="142.99114285714288" type="appl" />
         <tli id="T286" time="143.46928571428572" type="appl" />
         <tli id="T287" time="143.9474285714286" type="appl" />
         <tli id="T288" time="144.42557142857143" type="appl" />
         <tli id="T289" time="144.9037142857143" type="appl" />
         <tli id="T290" time="145.38185714285714" type="appl" />
         <tli id="T291" time="145.86" type="appl" />
         <tli id="T292" time="146.38987500000002" type="appl" />
         <tli id="T293" time="146.91975000000002" type="appl" />
         <tli id="T294" time="147.449625" type="appl" />
         <tli id="T295" time="147.9795" type="appl" />
         <tli id="T296" time="148.509375" type="appl" />
         <tli id="T297" time="149.03924999999998" type="appl" />
         <tli id="T298" time="149.56912499999999" type="appl" />
         <tli id="T299" time="150.099" type="appl" />
         <tli id="T300" time="150.5605625" type="appl" />
         <tli id="T301" time="151.022125" type="appl" />
         <tli id="T302" time="151.4836875" type="appl" />
         <tli id="T303" time="151.94525" type="appl" />
         <tli id="T304" time="152.4068125" type="appl" />
         <tli id="T305" time="152.868375" type="appl" />
         <tli id="T306" time="153.3299375" type="appl" />
         <tli id="T307" time="153.79149999999998" type="appl" />
         <tli id="T308" time="154.2530625" type="appl" />
         <tli id="T309" time="154.714625" type="appl" />
         <tli id="T310" time="155.1761875" type="appl" />
         <tli id="T311" time="155.63775" type="appl" />
         <tli id="T312" time="156.0993125" type="appl" />
         <tli id="T313" time="156.560875" type="appl" />
         <tli id="T314" time="157.0224375" type="appl" />
         <tli id="T315" time="157.484" type="appl" />
         <tli id="T316" time="158.068" type="appl" />
         <tli id="T317" time="158.65200000000002" type="appl" />
         <tli id="T318" time="159.236" type="appl" />
         <tli id="T319" time="159.82" type="appl" />
         <tli id="T320" time="160.404" type="appl" />
         <tli id="T321" time="160.988" type="appl" />
         <tli id="T322" time="161.42731578947368" type="appl" />
         <tli id="T323" time="161.86663157894736" type="appl" />
         <tli id="T324" time="162.30594736842104" type="appl" />
         <tli id="T325" time="162.74526315789473" type="appl" />
         <tli id="T326" time="163.18457894736844" type="appl" />
         <tli id="T327" time="163.62389473684212" type="appl" />
         <tli id="T328" time="164.0632105263158" type="appl" />
         <tli id="T329" time="164.50252631578948" type="appl" />
         <tli id="T330" time="164.94184210526316" type="appl" />
         <tli id="T331" time="165.38115789473684" type="appl" />
         <tli id="T332" time="165.82047368421053" type="appl" />
         <tli id="T333" time="166.2597894736842" type="appl" />
         <tli id="T334" time="166.6991052631579" type="appl" />
         <tli id="T335" time="167.13842105263157" type="appl" />
         <tli id="T336" time="167.57773684210528" type="appl" />
         <tli id="T337" time="168.01705263157896" type="appl" />
         <tli id="T338" time="168.45636842105264" type="appl" />
         <tli id="T339" time="168.89568421052633" type="appl" />
         <tli id="T340" time="169.335" type="appl" />
         <tli id="T341" time="169.971125" type="appl" />
         <tli id="T342" time="170.60725000000002" type="appl" />
         <tli id="T343" time="171.24337500000001" type="appl" />
         <tli id="T344" time="171.8795" type="appl" />
         <tli id="T345" time="172.515625" type="appl" />
         <tli id="T346" time="173.15175" type="appl" />
         <tli id="T347" time="173.787875" type="appl" />
         <tli id="T348" time="174.424" type="appl" />
         <tli id="T349" time="174.96057142857143" type="appl" />
         <tli id="T350" time="175.49714285714288" type="appl" />
         <tli id="T351" time="176.0337142857143" type="appl" />
         <tli id="T352" time="176.57028571428572" type="appl" />
         <tli id="T353" time="177.10685714285714" type="appl" />
         <tli id="T354" time="177.6434285714286" type="appl" />
         <tli id="T355" time="178.18" type="appl" />
         <tli id="T356" time="178.80333333333334" type="appl" />
         <tli id="T357" time="179.42666666666668" type="appl" />
         <tli id="T358" time="180.05" type="appl" />
         <tli id="T359" time="180.67333333333332" type="appl" />
         <tli id="T360" time="181.29666666666665" type="appl" />
         <tli id="T361" time="181.92" type="appl" />
         <tli id="T362" time="182.35452173913043" type="appl" />
         <tli id="T363" time="182.78904347826085" type="appl" />
         <tli id="T364" time="183.2235652173913" type="appl" />
         <tli id="T365" time="183.65808695652171" type="appl" />
         <tli id="T366" time="184.09260869565216" type="appl" />
         <tli id="T367" time="184.5271304347826" type="appl" />
         <tli id="T368" time="184.96165217391302" type="appl" />
         <tli id="T369" time="185.39617391304347" type="appl" />
         <tli id="T370" time="185.8306956521739" type="appl" />
         <tli id="T371" time="186.26521739130433" type="appl" />
         <tli id="T372" time="186.69973913043478" type="appl" />
         <tli id="T373" time="187.1342608695652" type="appl" />
         <tli id="T374" time="187.56878260869564" type="appl" />
         <tli id="T375" time="188.0033043478261" type="appl" />
         <tli id="T376" time="188.4378260869565" type="appl" />
         <tli id="T377" time="188.87234782608695" type="appl" />
         <tli id="T378" time="189.30686956521737" type="appl" />
         <tli id="T379" time="189.74139130434781" type="appl" />
         <tli id="T380" time="190.17591304347826" type="appl" />
         <tli id="T381" time="190.61043478260868" type="appl" />
         <tli id="T382" time="191.04495652173912" type="appl" />
         <tli id="T383" time="191.47947826086954" type="appl" />
         <tli id="T384" time="191.914" type="appl" />
         <tli id="T385" time="192.64266666666666" type="appl" />
         <tli id="T386" time="193.37133333333333" type="appl" />
         <tli id="T387" time="194.1" type="appl" />
         <tli id="T388" time="194.752625" type="appl" />
         <tli id="T389" time="195.40525" type="appl" />
         <tli id="T390" time="196.057875" type="appl" />
         <tli id="T391" time="196.7105" type="appl" />
         <tli id="T392" time="197.363125" type="appl" />
         <tli id="T393" time="198.01575" type="appl" />
         <tli id="T394" time="198.668375" type="appl" />
         <tli id="T395" time="199.321" type="appl" />
         <tli id="T396" time="199.9275" type="appl" />
         <tli id="T397" time="200.534" type="appl" />
         <tli id="T398" time="201.1405" type="appl" />
         <tli id="T399" time="201.747" type="appl" />
         <tli id="T400" time="202.3535" type="appl" />
         <tli id="T401" time="202.96" type="appl" />
         <tli id="T402" time="203.48160000000001" type="appl" />
         <tli id="T403" time="204.00320000000002" type="appl" />
         <tli id="T404" time="204.5248" type="appl" />
         <tli id="T405" time="205.0464" type="appl" />
         <tli id="T406" time="205.568" type="appl" />
         <tli id="T407" time="206.19542857142858" type="appl" />
         <tli id="T408" time="206.82285714285715" type="appl" />
         <tli id="T409" time="207.4502857142857" type="appl" />
         <tli id="T410" time="208.0777142857143" type="appl" />
         <tli id="T411" time="208.70514285714287" type="appl" />
         <tli id="T412" time="209.33257142857144" type="appl" />
         <tli id="T413" time="209.96" type="appl" />
         <tli id="T414" time="210.404" type="appl" />
         <tli id="T415" time="210.848" type="appl" />
         <tli id="T416" time="211.292" type="appl" />
         <tli id="T417" time="211.73600000000002" type="appl" />
         <tli id="T418" time="212.18" type="appl" />
         <tli id="T419" time="212.624" type="appl" />
         <tli id="T420" time="213.068" type="appl" />
         <tli id="T421" time="213.512" type="appl" />
         <tli id="T422" time="213.95600000000002" type="appl" />
         <tli id="T423" time="214.4" type="appl" />
         <tli id="T424" time="214.74743750000002" type="appl" />
         <tli id="T425" time="215.094875" type="appl" />
         <tli id="T426" time="215.4423125" type="appl" />
         <tli id="T427" time="215.78975" type="appl" />
         <tli id="T428" time="216.1371875" type="appl" />
         <tli id="T429" time="216.484625" type="appl" />
         <tli id="T430" time="216.8320625" type="appl" />
         <tli id="T431" time="217.17950000000002" type="appl" />
         <tli id="T432" time="217.5269375" type="appl" />
         <tli id="T433" time="217.87437500000001" type="appl" />
         <tli id="T434" time="218.2218125" type="appl" />
         <tli id="T435" time="218.56925" type="appl" />
         <tli id="T436" time="218.9166875" type="appl" />
         <tli id="T437" time="219.264125" type="appl" />
         <tli id="T438" time="219.6115625" type="appl" />
         <tli id="T439" time="219.959" type="appl" />
         <tli id="T440" time="220.51466666666667" type="appl" />
         <tli id="T441" time="221.07033333333334" type="appl" />
         <tli id="T442" time="221.626" type="appl" />
         <tli id="T443" time="222.18166666666667" type="appl" />
         <tli id="T444" time="222.73733333333334" type="appl" />
         <tli id="T445" time="223.293" type="appl" />
         <tli id="T446" time="223.7383" type="appl" />
         <tli id="T447" time="224.1836" type="appl" />
         <tli id="T448" time="224.62890000000002" type="appl" />
         <tli id="T449" time="225.07420000000002" type="appl" />
         <tli id="T450" time="225.5195" type="appl" />
         <tli id="T451" time="225.9648" type="appl" />
         <tli id="T452" time="226.4101" type="appl" />
         <tli id="T453" time="226.8554" type="appl" />
         <tli id="T454" time="227.3007" type="appl" />
         <tli id="T455" time="227.746" type="appl" />
         <tli id="T456" time="228.1913" type="appl" />
         <tli id="T457" time="228.63660000000002" type="appl" />
         <tli id="T458" time="229.08190000000002" type="appl" />
         <tli id="T459" time="229.52720000000002" type="appl" />
         <tli id="T460" time="229.97250000000003" type="appl" />
         <tli id="T461" time="230.4178" type="appl" />
         <tli id="T462" time="230.8631" type="appl" />
         <tli id="T463" time="231.3084" type="appl" />
         <tli id="T464" time="231.7537" type="appl" />
         <tli id="T465" time="232.199" type="appl" />
         <tli id="T466" time="232.64113636363638" type="appl" />
         <tli id="T467" time="233.08327272727274" type="appl" />
         <tli id="T468" time="233.5254090909091" type="appl" />
         <tli id="T469" time="233.96754545454547" type="appl" />
         <tli id="T470" time="234.40968181818184" type="appl" />
         <tli id="T471" time="234.85181818181817" type="appl" />
         <tli id="T472" time="235.29395454545454" type="appl" />
         <tli id="T473" time="235.7360909090909" type="appl" />
         <tli id="T474" time="236.17822727272727" type="appl" />
         <tli id="T475" time="236.62036363636363" type="appl" />
         <tli id="T476" time="237.0625" type="appl" />
         <tli id="T477" time="237.50463636363637" type="appl" />
         <tli id="T478" time="237.94677272727273" type="appl" />
         <tli id="T479" time="238.3889090909091" type="appl" />
         <tli id="T480" time="238.83104545454546" type="appl" />
         <tli id="T481" time="239.27318181818183" type="appl" />
         <tli id="T482" time="239.71531818181816" type="appl" />
         <tli id="T483" time="240.15745454545453" type="appl" />
         <tli id="T484" time="240.5995909090909" type="appl" />
         <tli id="T485" time="241.04172727272726" type="appl" />
         <tli id="T486" time="241.48386363636362" type="appl" />
         <tli id="T487" time="241.926" type="appl" />
         <tli id="T488" time="242.3990769230769" type="appl" />
         <tli id="T489" time="242.87215384615382" type="appl" />
         <tli id="T490" time="243.34523076923077" type="appl" />
         <tli id="T491" time="243.81830769230768" type="appl" />
         <tli id="T492" time="244.2913846153846" type="appl" />
         <tli id="T493" time="244.76446153846152" type="appl" />
         <tli id="T494" time="245.23753846153846" type="appl" />
         <tli id="T495" time="245.71061538461538" type="appl" />
         <tli id="T496" time="246.1836923076923" type="appl" />
         <tli id="T497" time="246.6567692307692" type="appl" />
         <tli id="T498" time="247.12984615384616" type="appl" />
         <tli id="T499" time="247.60292307692308" type="appl" />
         <tli id="T500" time="248.076" type="appl" />
         <tli id="T501" time="248.46966666666665" type="appl" />
         <tli id="T502" time="248.86333333333332" type="appl" />
         <tli id="T503" time="249.257" type="appl" />
         <tli id="T504" time="249.65066666666667" type="appl" />
         <tli id="T505" time="250.04433333333333" type="appl" />
         <tli id="T506" time="250.438" type="appl" />
         <tli id="T507" time="250.8039090909091" type="appl" />
         <tli id="T508" time="251.16981818181816" type="appl" />
         <tli id="T509" time="251.53572727272726" type="appl" />
         <tli id="T510" time="251.90163636363636" type="appl" />
         <tli id="T511" time="252.26754545454546" type="appl" />
         <tli id="T512" time="252.63345454545453" type="appl" />
         <tli id="T513" time="252.99936363636363" type="appl" />
         <tli id="T514" time="253.36527272727272" type="appl" />
         <tli id="T515" time="253.73118181818182" type="appl" />
         <tli id="T516" time="254.0970909090909" type="appl" />
         <tli id="T517" time="254.463" type="appl" />
         <tli id="T518" time="255.1464" type="appl" />
         <tli id="T519" time="255.8298" type="appl" />
         <tli id="T520" time="256.5132" type="appl" />
         <tli id="T521" time="257.1966" type="appl" />
         <tli id="T522" time="257.88" type="appl" />
         <tli id="T523" time="258.67" type="appl" />
         <tli id="T524" time="259.46000000000004" type="appl" />
         <tli id="T525" time="260.25" type="appl" />
         <tli id="T526" time="260.80558427043337" />
         <tli id="T527" time="261.42600000000004" type="appl" />
         <tli id="T528" time="261.812" type="appl" />
         <tli id="T529" time="262.198" type="appl" />
         <tli id="T530" time="262.584" type="appl" />
         <tli id="T531" time="262.97" type="appl" />
         <tli id="T532" time="263.356" type="appl" />
         <tli id="T533" time="263.74199999999996" type="appl" />
         <tli id="T534" time="264.128" type="appl" />
         <tli id="T535" time="264.514" type="appl" />
         <tli id="T536" time="264.9" type="appl" />
         <tli id="T537" time="266.335" type="appl" />
         <tli id="T538" time="266.90166666666664" type="appl" />
         <tli id="T539" time="267.46833333333336" type="appl" />
         <tli id="T540" time="268.035" type="appl" />
         <tli id="T541" time="268.4871428571429" type="appl" />
         <tli id="T542" time="268.93928571428575" type="appl" />
         <tli id="T543" time="269.3914285714286" type="appl" />
         <tli id="T544" time="269.8435714285714" type="appl" />
         <tli id="T545" time="270.29571428571427" type="appl" />
         <tli id="T546" time="270.74785714285713" type="appl" />
         <tli id="T547" time="271.2" type="appl" />
         <tli id="T548" time="271.6479090909091" type="appl" />
         <tli id="T549" time="272.0958181818182" type="appl" />
         <tli id="T550" time="272.54372727272727" type="appl" />
         <tli id="T551" time="272.99163636363636" type="appl" />
         <tli id="T552" time="273.43954545454545" type="appl" />
         <tli id="T553" time="273.88745454545455" type="appl" />
         <tli id="T554" time="274.33536363636364" type="appl" />
         <tli id="T555" time="274.78327272727273" type="appl" />
         <tli id="T556" time="275.2311818181818" type="appl" />
         <tli id="T557" time="275.6790909090909" type="appl" />
         <tli id="T558" time="276.127" type="appl" />
         <tli id="T559" time="276.9146666666667" type="appl" />
         <tli id="T560" time="277.70233333333334" type="appl" />
         <tli id="T561" time="278.49" type="appl" />
         <tli id="T562" time="279.0775" type="appl" />
         <tli id="T563" time="279.665" type="appl" />
         <tli id="T564" time="280.2525" type="appl" />
         <tli id="T565" time="280.84000000000003" type="appl" />
         <tli id="T566" time="281.4275" type="appl" />
         <tli id="T567" time="282.015" type="appl" />
         <tli id="T568" time="282.6025" type="appl" />
         <tli id="T569" time="283.19" type="appl" />
         <tli id="T570" time="284.4015" type="appl" />
         <tli id="T571" time="285.613" type="appl" />
         <tli id="T572" time="286.8245" type="appl" />
         <tli id="T573" time="288.036" type="appl" />
         <tli id="T574" time="288.5706666666667" type="appl" />
         <tli id="T575" time="289.1053333333333" type="appl" />
         <tli id="T576" time="289.4787986040939" />
         <tli id="T577" time="290.1714285714286" type="appl" />
         <tli id="T578" time="290.7028571428571" type="appl" />
         <tli id="T579" time="291.2342857142857" type="appl" />
         <tli id="T580" time="291.7657142857143" type="appl" />
         <tli id="T581" time="292.2971428571429" type="appl" />
         <tli id="T582" time="292.8285714285714" type="appl" />
         <tli id="T583" time="293.2654495553888" />
         <tli id="T584" time="294.04241666666667" type="appl" />
         <tli id="T585" time="294.7248333333333" type="appl" />
         <tli id="T586" time="295.40725" type="appl" />
         <tli id="T587" time="296.0896666666667" type="appl" />
         <tli id="T588" time="296.77208333333334" type="appl" />
         <tli id="T589" time="297.4545" type="appl" />
         <tli id="T590" time="298.13691666666665" type="appl" />
         <tli id="T591" time="298.8193333333333" type="appl" />
         <tli id="T592" time="299.50175" type="appl" />
         <tli id="T593" time="300.18416666666667" type="appl" />
         <tli id="T594" time="300.8665833333333" type="appl" />
         <tli id="T595" time="301.549" type="appl" />
         <tli id="T596" time="307.294" type="appl" />
         <tli id="T597" time="317.159" type="appl" />
         <tli id="T598" time="325.985" type="appl" />
         <tli id="T599" time="331.362" type="appl" />
         <tli id="T600" time="339.274" type="appl" />
         <tli id="T601" time="345.348" type="appl" />
         <tli id="T602" time="352.88" type="appl" />
         <tli id="T603" time="361.011" type="appl" />
         <tli id="T604" time="365.304" type="appl" />
         <tli id="T605" time="373.649" type="appl" />
         <tli id="T606" time="380.495" type="appl" />
         <tli id="T607" time="389.663" type="appl" />
         <tli id="T608" time="393.794" type="appl" />
         <tli id="T609" time="394.4645" type="appl" />
         <tli id="T610" time="395.135" type="appl" />
         <tli id="T611" time="395.6457142857143" type="appl" />
         <tli id="T612" time="396.15642857142853" type="appl" />
         <tli id="T613" time="396.66714285714284" type="appl" />
         <tli id="T614" time="397.17785714285714" type="appl" />
         <tli id="T615" time="397.68857142857144" type="appl" />
         <tli id="T616" time="398.1992857142857" type="appl" />
         <tli id="T617" time="398.71" type="appl" />
         <tli id="T618" time="399.2265" type="appl" />
         <tli id="T619" time="399.743" type="appl" />
         <tli id="T620" time="400.2595" type="appl" />
         <tli id="T621" time="400.776" type="appl" />
         <tli id="T622" time="401.30605" type="appl" />
         <tli id="T623" time="401.8361" type="appl" />
         <tli id="T624" time="402.36615" type="appl" />
         <tli id="T625" time="402.8962" type="appl" />
         <tli id="T626" time="403.42625" type="appl" />
         <tli id="T627" time="403.9563" type="appl" />
         <tli id="T628" time="404.48635" type="appl" />
         <tli id="T629" time="405.01640000000003" type="appl" />
         <tli id="T630" time="405.54645" type="appl" />
         <tli id="T631" time="406.0765" type="appl" />
         <tli id="T632" time="406.60655" type="appl" />
         <tli id="T633" time="407.1366" type="appl" />
         <tli id="T634" time="407.66665" type="appl" />
         <tli id="T635" time="408.1967" type="appl" />
         <tli id="T636" time="408.72675" type="appl" />
         <tli id="T637" time="409.2568" type="appl" />
         <tli id="T638" time="409.78685" type="appl" />
         <tli id="T639" time="410.31690000000003" type="appl" />
         <tli id="T640" time="410.84695" type="appl" />
         <tli id="T641" time="411.377" type="appl" />
         <tli id="T642" time="411.9037272727273" type="appl" />
         <tli id="T643" time="412.43045454545455" type="appl" />
         <tli id="T644" time="412.9571818181818" type="appl" />
         <tli id="T645" time="413.4839090909091" type="appl" />
         <tli id="T646" time="414.01063636363637" type="appl" />
         <tli id="T647" time="414.53736363636364" type="appl" />
         <tli id="T648" time="415.0640909090909" type="appl" />
         <tli id="T649" time="415.5908181818182" type="appl" />
         <tli id="T650" time="416.11754545454545" type="appl" />
         <tli id="T651" time="416.6442727272727" type="appl" />
         <tli id="T652" time="417.171" type="appl" />
         <tli id="T653" time="417.68664285714283" type="appl" />
         <tli id="T654" time="418.2022857142857" type="appl" />
         <tli id="T655" time="418.71792857142856" type="appl" />
         <tli id="T656" time="419.2335714285714" type="appl" />
         <tli id="T657" time="419.7492142857143" type="appl" />
         <tli id="T658" time="420.2648571428571" type="appl" />
         <tli id="T659" time="420.78049999999996" type="appl" />
         <tli id="T660" time="421.29614285714285" type="appl" />
         <tli id="T661" time="421.8117857142857" type="appl" />
         <tli id="T662" time="422.3274285714286" type="appl" />
         <tli id="T663" time="422.8430714285714" type="appl" />
         <tli id="T664" time="423.35871428571426" type="appl" />
         <tli id="T665" time="423.87435714285715" type="appl" />
         <tli id="T666" time="424.39" type="appl" />
         <tli id="T667" time="424.90566666666666" type="appl" />
         <tli id="T668" time="425.42133333333334" type="appl" />
         <tli id="T669" time="425.937" type="appl" />
         <tli id="T670" time="433.848" type="appl" />
         <tli id="T671" time="437.6" type="appl" />
         <tli id="T672" time="446.618" type="appl" />
         <tli id="T673" time="456.149" type="appl" />
         <tli id="T674" time="463.769" type="appl" />
         <tli id="T675" time="473.161" type="appl" />
         <tli id="T676" time="475.58" type="appl" />
         <tli id="T677" time="483.49" type="appl" />
         <tli id="T678" time="487.236" type="appl" />
         <tli id="T679" time="494.6" type="appl" />
         <tli id="T680" time="498.282" type="appl" />
         <tli id="T681" time="505.951" type="appl" />
         <tli id="T682" time="512.96" type="appl" />
         <tli id="T683" time="518.276" type="appl" />
         <tli id="T684" time="527.884" type="appl" />
         <tli id="T685" time="528.3013333333333" type="appl" />
         <tli id="T686" time="528.7186666666666" type="appl" />
         <tli id="T687" time="529.136" type="appl" />
         <tli id="T688" time="529.5533333333334" type="appl" />
         <tli id="T689" time="529.9706666666667" type="appl" />
         <tli id="T690" time="530.388" type="appl" />
         <tli id="T691" time="530.8053333333334" type="appl" />
         <tli id="T692" time="531.2226666666667" type="appl" />
         <tli id="T693" time="531.64" type="appl" />
         <tli id="T694" time="532.0573333333333" type="appl" />
         <tli id="T695" time="532.4746666666667" type="appl" />
         <tli id="T696" time="532.892" type="appl" />
         <tli id="T697" time="533.3093333333334" type="appl" />
         <tli id="T698" time="533.7266666666667" type="appl" />
         <tli id="T699" time="534.144" type="appl" />
         <tli id="T700" time="534.5613333333333" type="appl" />
         <tli id="T701" time="534.9786666666666" type="appl" />
         <tli id="T702" time="535.3960000000001" type="appl" />
         <tli id="T703" time="535.8133333333334" type="appl" />
         <tli id="T704" time="536.2306666666667" type="appl" />
         <tli id="T705" time="536.648" type="appl" />
         <tli id="T706" time="537.032" type="appl" />
         <tli id="T707" time="537.416" type="appl" />
         <tli id="T708" time="537.8" type="appl" />
         <tli id="T709" time="538.184" type="appl" />
         <tli id="T710" time="538.568" type="appl" />
         <tli id="T711" time="538.952" type="appl" />
         <tli id="T712" time="539.336" type="appl" />
         <tli id="T713" time="539.72" type="appl" />
         <tli id="T714" time="540.1039999999999" type="appl" />
         <tli id="T715" time="540.4879999999999" type="appl" />
         <tli id="T716" time="540.872" type="appl" />
         <tli id="T717" time="541.2394545454545" type="appl" />
         <tli id="T718" time="541.6069090909091" type="appl" />
         <tli id="T719" time="541.9743636363636" type="appl" />
         <tli id="T720" time="542.3418181818181" type="appl" />
         <tli id="T721" time="542.7092727272727" type="appl" />
         <tli id="T722" time="543.0767272727272" type="appl" />
         <tli id="T723" time="543.4441818181818" type="appl" />
         <tli id="T724" time="543.8116363636364" type="appl" />
         <tli id="T725" time="544.1790909090909" type="appl" />
         <tli id="T726" time="544.5465454545455" type="appl" />
         <tli id="T727" time="544.914" type="appl" />
         <tli id="T728" time="545.308" type="appl" />
         <tli id="T729" time="545.702" type="appl" />
         <tli id="T730" time="546.096" type="appl" />
         <tli id="T731" time="546.49" type="appl" />
         <tli id="T732" time="546.82" type="appl" />
         <tli id="T733" time="547.15" type="appl" />
         <tli id="T734" time="547.48" type="appl" />
         <tli id="T735" time="547.8100000000001" type="appl" />
         <tli id="T736" time="548.14" type="appl" />
         <tli id="T737" time="548.47" type="appl" />
         <tli id="T738" time="548.869" type="appl" />
         <tli id="T739" time="549.268" type="appl" />
         <tli id="T740" time="549.667" type="appl" />
         <tli id="T741" time="550.066" type="appl" />
         <tli id="T742" time="550.465" type="appl" />
         <tli id="T743" time="550.864" type="appl" />
         <tli id="T744" time="551.263" type="appl" />
         <tli id="T745" time="551.662" type="appl" />
         <tli id="T746" time="552.061" type="appl" />
         <tli id="T747" time="552.46" type="appl" />
         <tli id="T748" time="552.859" type="appl" />
         <tli id="T749" time="553.258" type="appl" />
         <tli id="T750" time="553.657" type="appl" />
         <tli id="T751" time="553.9769" type="appl" />
         <tli id="T752" time="554.2968000000001" type="appl" />
         <tli id="T753" time="554.6167" type="appl" />
         <tli id="T754" time="554.9366" type="appl" />
         <tli id="T755" time="555.2565" type="appl" />
         <tli id="T756" time="555.5764" type="appl" />
         <tli id="T757" time="555.8963" type="appl" />
         <tli id="T758" time="556.2162" type="appl" />
         <tli id="T759" time="556.5361" type="appl" />
         <tli id="T760" time="556.856" type="appl" />
         <tli id="T761" time="557.3074444444444" type="appl" />
         <tli id="T762" time="557.7588888888889" type="appl" />
         <tli id="T763" time="558.2103333333333" type="appl" />
         <tli id="T764" time="558.6617777777777" type="appl" />
         <tli id="T765" time="559.1132222222222" type="appl" />
         <tli id="T766" time="559.5646666666667" type="appl" />
         <tli id="T767" time="560.0161111111111" type="appl" />
         <tli id="T768" time="560.4675555555556" type="appl" />
         <tli id="T769" time="560.9376719945434" />
         <tli id="T770" time="561.4451" type="appl" />
         <tli id="T771" time="561.9712" type="appl" />
         <tli id="T772" time="562.4973" type="appl" />
         <tli id="T773" time="563.0233999999999" type="appl" />
         <tli id="T774" time="563.5495" type="appl" />
         <tli id="T775" time="564.0756" type="appl" />
         <tli id="T776" time="564.6016999999999" type="appl" />
         <tli id="T777" time="565.1278" type="appl" />
         <tli id="T778" time="565.6538999999999" type="appl" />
         <tli id="T779" time="566.18" type="appl" />
         <tli id="T780" time="566.6148999999999" type="appl" />
         <tli id="T781" time="567.0498" type="appl" />
         <tli id="T782" time="567.4847" type="appl" />
         <tli id="T783" time="567.9196" type="appl" />
         <tli id="T784" time="568.3544999999999" type="appl" />
         <tli id="T785" time="568.7894" type="appl" />
         <tli id="T786" time="569.2243" type="appl" />
         <tli id="T787" time="569.6591999999999" type="appl" />
         <tli id="T788" time="570.0941" type="appl" />
         <tli id="T789" time="570.529" type="appl" />
         <tli id="T790" time="570.989" type="appl" />
         <tli id="T791" time="571.449" type="appl" />
         <tli id="T792" time="571.909" type="appl" />
         <tli id="T793" time="572.5482857142857" type="appl" />
         <tli id="T794" time="573.1875714285715" type="appl" />
         <tli id="T795" time="573.8268571428572" type="appl" />
         <tli id="T796" time="574.4661428571428" type="appl" />
         <tli id="T797" time="575.1054285714285" type="appl" />
         <tli id="T798" time="575.7447142857143" type="appl" />
         <tli id="T799" time="576.384" type="appl" />
         <tli id="T800" time="576.9668333333334" type="appl" />
         <tli id="T801" time="577.5496666666667" type="appl" />
         <tli id="T802" time="578.1324999999999" type="appl" />
         <tli id="T803" time="578.7153333333333" type="appl" />
         <tli id="T804" time="579.2981666666667" type="appl" />
         <tli id="T805" time="579.7509272490399" />
         <tli id="T806" time="580.50125" type="appl" />
         <tli id="T807" time="581.1215" type="appl" />
         <tli id="T808" time="581.7417499999999" type="appl" />
         <tli id="T809" time="582.362" type="appl" />
         <tli id="T810" time="582.98225" type="appl" />
         <tli id="T811" time="583.6025" type="appl" />
         <tli id="T812" time="584.2227499999999" type="appl" />
         <tli id="T813" time="584.843" type="appl" />
         <tli id="T814" time="585.461" type="appl" />
         <tli id="T815" time="586.079" type="appl" />
         <tli id="T816" time="586.697" type="appl" />
         <tli id="T817" time="587.315" type="appl" />
         <tli id="T818" time="587.933" type="appl" />
         <tli id="T819" time="588.551" type="appl" />
         <tli id="T820" time="589.0398571428572" type="appl" />
         <tli id="T821" time="589.5287142857143" type="appl" />
         <tli id="T822" time="590.0175714285714" type="appl" />
         <tli id="T823" time="590.5064285714286" type="appl" />
         <tli id="T824" time="590.9952857142857" type="appl" />
         <tli id="T825" time="591.4841428571428" type="appl" />
         <tli id="T826" time="591.973" type="appl" />
         <tli id="T827" time="592.4275" type="appl" />
         <tli id="T828" time="592.882" type="appl" />
         <tli id="T829" time="593.3365" type="appl" />
         <tli id="T830" time="593.7909999999999" type="appl" />
         <tli id="T831" time="594.2455" type="appl" />
         <tli id="T832" time="594.6999999999999" type="appl" />
         <tli id="T833" time="595.1545" type="appl" />
         <tli id="T834" time="595.6089999999999" type="appl" />
         <tli id="T835" time="596.0635" type="appl" />
         <tli id="T836" time="596.5179999999999" type="appl" />
         <tli id="T837" time="596.9725" type="appl" />
         <tli id="T838" time="597.427" type="appl" />
         <tli id="T839" time="597.8815" type="appl" />
         <tli id="T840" time="598.336" type="appl" />
         <tli id="T841" time="598.7905" type="appl" />
         <tli id="T842" time="599.245" type="appl" />
         <tli id="T843" time="599.6995" type="appl" />
         <tli id="T844" time="600.154" type="appl" />
         <tli id="T845" time="600.6084999999999" type="appl" />
         <tli id="T846" time="601.063" type="appl" />
         <tli id="T847" time="601.5174999999999" type="appl" />
         <tli id="T848" time="601.972" type="appl" />
         <tli id="T849" time="602.7413333333333" type="appl" />
         <tli id="T850" time="603.5106666666667" type="appl" />
         <tli id="T851" time="604.28" type="appl" />
         <tli id="T852" time="604.6503076923077" type="appl" />
         <tli id="T853" time="605.0206153846153" type="appl" />
         <tli id="T854" time="605.3909230769231" type="appl" />
         <tli id="T855" time="605.7612307692308" type="appl" />
         <tli id="T856" time="606.1315384615384" type="appl" />
         <tli id="T857" time="606.5018461538461" type="appl" />
         <tli id="T858" time="606.8721538461539" type="appl" />
         <tli id="T859" time="607.2424615384616" type="appl" />
         <tli id="T860" time="607.6127692307692" type="appl" />
         <tli id="T861" time="607.983076923077" type="appl" />
         <tli id="T862" time="608.3533846153847" type="appl" />
         <tli id="T863" time="608.7236923076923" type="appl" />
         <tli id="T864" time="609.094" type="appl" />
         <tli id="T865" time="609.5524444444445" type="appl" />
         <tli id="T866" time="610.010888888889" type="appl" />
         <tli id="T867" time="610.4693333333333" type="appl" />
         <tli id="T868" time="610.9277777777778" type="appl" />
         <tli id="T869" time="611.3862222222223" type="appl" />
         <tli id="T870" time="611.8446666666667" type="appl" />
         <tli id="T871" time="612.3031111111111" type="appl" />
         <tli id="T872" time="612.7615555555556" type="appl" />
         <tli id="T873" time="613.22" type="appl" />
         <tli id="T874" time="613.639" type="appl" />
         <tli id="T875" time="614.058" type="appl" />
         <tli id="T876" time="614.477" type="appl" />
         <tli id="T877" time="614.896" type="appl" />
         <tli id="T878" time="615.315" type="appl" />
         <tli id="T879" time="615.734" type="appl" />
         <tli id="T880" time="616.153" type="appl" />
         <tli id="T881" time="616.572" type="appl" />
         <tli id="T882" time="616.991" type="appl" />
         <tli id="T883" time="617.41" type="appl" />
         <tli id="T884" time="617.8285" type="appl" />
         <tli id="T885" time="618.247" type="appl" />
         <tli id="T886" time="618.6655" type="appl" />
         <tli id="T887" time="619.084" type="appl" />
         <tli id="T888" time="619.5205" type="appl" />
         <tli id="T889" time="619.957" type="appl" />
         <tli id="T890" time="620.3934999999999" type="appl" />
         <tli id="T891" time="620.8299999999999" type="appl" />
         <tli id="T892" time="621.2665" type="appl" />
         <tli id="T893" time="621.703" type="appl" />
         <tli id="T894" time="622.1395" type="appl" />
         <tli id="T895" time="622.576" type="appl" />
         <tli id="T896" time="623.0124999999999" type="appl" />
         <tli id="T897" time="623.449" type="appl" />
         <tli id="T898" time="623.8855" type="appl" />
         <tli id="T899" time="624.322" type="appl" />
         <tli id="T900" time="624.7585" type="appl" />
         <tli id="T901" time="625.1949999999999" type="appl" />
         <tli id="T902" time="625.6315" type="appl" />
         <tli id="T903" time="626.068" type="appl" />
         <tli id="T904" time="626.4083333333333" type="appl" />
         <tli id="T905" time="626.7486666666666" type="appl" />
         <tli id="T906" time="627.0889999999999" type="appl" />
         <tli id="T907" time="627.4293333333334" type="appl" />
         <tli id="T908" time="627.7696666666667" type="appl" />
         <tli id="T909" time="627.9640604880619" />
         <tli id="T910" time="628.4912222222223" type="appl" />
         <tli id="T911" time="628.8724444444445" type="appl" />
         <tli id="T912" time="629.2536666666667" type="appl" />
         <tli id="T913" time="629.6348888888889" type="appl" />
         <tli id="T914" time="630.0161111111112" type="appl" />
         <tli id="T915" time="630.3973333333333" type="appl" />
         <tli id="T916" time="630.7785555555556" type="appl" />
         <tli id="T917" time="631.1597777777778" type="appl" />
         <tli id="T918" time="631.541" type="appl" />
         <tli id="T919" time="632.2141428571429" type="appl" />
         <tli id="T920" time="632.8872857142858" type="appl" />
         <tli id="T921" time="633.5604285714286" type="appl" />
         <tli id="T922" time="634.2335714285715" type="appl" />
         <tli id="T923" time="634.9067142857143" type="appl" />
         <tli id="T924" time="635.5798571428572" type="appl" />
         <tli id="T925" time="636.253" type="appl" />
         <tli id="T926" time="636.9261428571428" type="appl" />
         <tli id="T927" time="637.5992857142858" type="appl" />
         <tli id="T928" time="638.2724285714286" type="appl" />
         <tli id="T929" time="638.9455714285715" type="appl" />
         <tli id="T930" time="639.6187142857143" type="appl" />
         <tli id="T931" time="640.2918571428572" type="appl" />
         <tli id="T932" time="640.965" type="appl" />
         <tli id="T933" time="641.3943636363637" type="appl" />
         <tli id="T934" time="641.8237272727273" type="appl" />
         <tli id="T935" time="642.2530909090909" type="appl" />
         <tli id="T936" time="642.6824545454546" type="appl" />
         <tli id="T937" time="643.1118181818182" type="appl" />
         <tli id="T938" time="643.5411818181818" type="appl" />
         <tli id="T939" time="643.9705454545455" type="appl" />
         <tli id="T940" time="644.3999090909091" type="appl" />
         <tli id="T941" time="644.8292727272727" type="appl" />
         <tli id="T942" time="645.2586363636364" type="appl" />
         <tli id="T943" time="645.688" type="appl" />
         <tli id="T944" time="646.04465" type="appl" />
         <tli id="T945" time="646.4013" type="appl" />
         <tli id="T946" time="646.7579499999999" type="appl" />
         <tli id="T947" time="647.1146" type="appl" />
         <tli id="T948" time="647.47125" type="appl" />
         <tli id="T949" time="647.8279" type="appl" />
         <tli id="T950" time="648.18455" type="appl" />
         <tli id="T951" time="648.5412" type="appl" />
         <tli id="T952" time="648.8978500000001" type="appl" />
         <tli id="T953" time="649.2545" type="appl" />
         <tli id="T954" time="649.61115" type="appl" />
         <tli id="T955" time="649.9678" type="appl" />
         <tli id="T956" time="650.3244500000001" type="appl" />
         <tli id="T957" time="650.6811" type="appl" />
         <tli id="T958" time="651.03775" type="appl" />
         <tli id="T959" time="651.3944" type="appl" />
         <tli id="T960" time="651.7510500000001" type="appl" />
         <tli id="T961" time="652.1077" type="appl" />
         <tli id="T962" time="652.46435" type="appl" />
         <tli id="T963" time="652.821" type="appl" />
         <tli id="T964" time="653.4313333333333" type="appl" />
         <tli id="T965" time="654.0416666666667" type="appl" />
         <tli id="T966" time="654.652" type="appl" />
         <tli id="T967" time="655.08" type="appl" />
         <tli id="T968" time="655.508" type="appl" />
         <tli id="T969" time="655.936" type="appl" />
         <tli id="T970" time="656.364" type="appl" />
         <tli id="T971" time="656.792" type="appl" />
         <tli id="T972" time="657.22" type="appl" />
         <tli id="T973" time="657.72825" type="appl" />
         <tli id="T974" time="658.2365" type="appl" />
         <tli id="T975" time="658.74475" type="appl" />
         <tli id="T976" time="659.2529999999999" type="appl" />
         <tli id="T977" time="659.76125" type="appl" />
         <tli id="T978" time="660.2695" type="appl" />
         <tli id="T979" time="660.77775" type="appl" />
         <tli id="T980" time="661.286" type="appl" />
         <tli id="T981" time="661.683" type="appl" />
         <tli id="T982" time="662.0799999999999" type="appl" />
         <tli id="T983" time="662.477" type="appl" />
         <tli id="T984" time="662.8739999999999" type="appl" />
         <tli id="T985" time="663.271" type="appl" />
         <tli id="T986" time="663.668" type="appl" />
         <tli id="T987" time="664.0649999999999" type="appl" />
         <tli id="T988" time="664.462" type="appl" />
         <tli id="T989" time="664.8589999999999" type="appl" />
         <tli id="T990" time="665.256" type="appl" />
         <tli id="T991" time="665.5792" type="appl" />
         <tli id="T992" time="665.9024" type="appl" />
         <tli id="T993" time="666.2256" type="appl" />
         <tli id="T994" time="666.5487999999999" type="appl" />
         <tli id="T995" time="666.7372329048417" />
         <tli id="T996" time="667.1926666666666" type="appl" />
         <tli id="T997" time="667.5133333333333" type="appl" />
         <tli id="T998" time="667.8340000000001" type="appl" />
         <tli id="T999" time="668.1546666666667" type="appl" />
         <tli id="T1000" time="668.4753333333333" type="appl" />
         <tli id="T1001" time="668.7238913264717" />
         <tli id="T1002" time="669.2336428571429" type="appl" />
         <tli id="T1003" time="669.6712857142858" type="appl" />
         <tli id="T1004" time="670.1089285714286" type="appl" />
         <tli id="T1005" time="670.5465714285715" type="appl" />
         <tli id="T1006" time="670.9842142857143" type="appl" />
         <tli id="T1007" time="671.4218571428572" type="appl" />
         <tli id="T1008" time="671.8595" type="appl" />
         <tli id="T1009" time="672.2971428571428" type="appl" />
         <tli id="T1010" time="672.7347857142857" type="appl" />
         <tli id="T1011" time="673.1724285714286" type="appl" />
         <tli id="T1012" time="673.6100714285715" type="appl" />
         <tli id="T1013" time="674.0477142857143" type="appl" />
         <tli id="T1014" time="674.4853571428572" type="appl" />
         <tli id="T1015" time="674.923" type="appl" />
         <tli id="T1016" time="675.3235714285714" type="appl" />
         <tli id="T1017" time="675.7241428571429" type="appl" />
         <tli id="T1018" time="676.1247142857143" type="appl" />
         <tli id="T1019" time="676.5252857142857" type="appl" />
         <tli id="T1020" time="676.9258571428571" type="appl" />
         <tli id="T1021" time="677.3264285714286" type="appl" />
         <tli id="T1022" time="677.727" type="appl" />
         <tli id="T1023" time="678.2162857142857" type="appl" />
         <tli id="T1024" time="678.7055714285714" type="appl" />
         <tli id="T1025" time="679.1948571428571" type="appl" />
         <tli id="T1026" time="679.6841428571429" type="appl" />
         <tli id="T1027" time="680.1734285714286" type="appl" />
         <tli id="T1028" time="680.6627142857143" type="appl" />
         <tli id="T1029" time="681.152" type="appl" />
         <tli id="T1030" time="681.74725" type="appl" />
         <tli id="T1031" time="682.3425" type="appl" />
         <tli id="T1032" time="682.93775" type="appl" />
         <tli id="T1033" time="683.533" type="appl" />
         <tli id="T1034" time="684.16725" type="appl" />
         <tli id="T1035" time="684.8015" type="appl" />
         <tli id="T1036" time="685.4357500000001" type="appl" />
         <tli id="T1037" time="686.07" type="appl" />
         <tli id="T1038" time="686.6025000000001" type="appl" />
         <tli id="T1039" time="687.135" type="appl" />
         <tli id="T1040" time="687.6675" type="appl" />
         <tli id="T1041" time="688.2" type="appl" />
         <tli id="T1042" time="689.52" type="appl" />
         <tli id="T1043" time="689.976" type="appl" />
         <tli id="T1044" time="690.432" type="appl" />
         <tli id="T1045" time="690.8879999999999" type="appl" />
         <tli id="T1046" time="691.3439999999999" type="appl" />
         <tli id="T1047" time="691.8" type="appl" />
         <tli id="T1048" time="692.3356666666666" type="appl" />
         <tli id="T1049" time="692.8713333333333" type="appl" />
         <tli id="T1050" time="693.4069999999999" type="appl" />
         <tli id="T1051" time="693.9426666666667" type="appl" />
         <tli id="T1052" time="694.4783333333334" type="appl" />
         <tli id="T1053" time="695.014" type="appl" />
         <tli id="T1054" time="695.6766666666666" type="appl" />
         <tli id="T1055" time="696.3393333333333" type="appl" />
         <tli id="T1056" time="697.002" type="appl" />
         <tli id="T1057" time="742.42075" type="appl" />
         <tli id="T1058" time="787.8395" type="appl" />
         <tli id="T1059" time="833.25825" type="appl" />
         <tli id="T1060" time="878.677" type="appl" />
         <tli id="T1061" time="879.382375" type="appl" />
         <tli id="T1062" time="880.08775" type="appl" />
         <tli id="T1063" time="880.793125" type="appl" />
         <tli id="T1064" time="881.4985" type="appl" />
         <tli id="T1065" time="882.203875" type="appl" />
         <tli id="T1066" time="882.90925" type="appl" />
         <tli id="T1067" time="883.614625" type="appl" />
         <tli id="T1068" time="884.32" type="appl" />
         <tli id="T1069" time="884.7878333333333" type="appl" />
         <tli id="T1070" time="885.2556666666667" type="appl" />
         <tli id="T1071" time="885.7235000000001" type="appl" />
         <tli id="T1072" time="886.1913333333333" type="appl" />
         <tli id="T1073" time="886.6591666666666" type="appl" />
         <tli id="T1074" time="887.127" type="appl" />
         <tli id="T1075" time="887.7425" type="appl" />
         <tli id="T1076" time="888.358" type="appl" />
         <tli id="T1077" time="888.9735000000001" type="appl" />
         <tli id="T1078" time="889.589" type="appl" />
         <tli id="T1079" time="890.2045" type="appl" />
         <tli id="T1080" time="890.82" type="appl" />
         <tli id="T1081" time="891.8945000000001" type="appl" />
         <tli id="T1082" time="892.969" type="appl" />
         <tli id="T1083" time="894.0360000000001" type="appl" />
         <tli id="T1084" time="895.103" type="appl" />
         <tli id="T1085" time="896.17" type="appl" />
         <tli id="T1086" time="897.0973333333333" type="appl" />
         <tli id="T1087" time="898.0246666666667" type="appl" />
         <tli id="T1088" time="898.952" type="appl" />
         <tli id="T1089" time="899.63" type="appl" />
         <tli id="T1090" time="900.308" type="appl" />
         <tli id="T1091" time="900.986" type="appl" />
         <tli id="T1092" time="901.664" type="appl" />
         <tli id="T1093" time="902.342" type="appl" />
         <tli id="T1094" time="903.02" type="appl" />
         <tli id="T1095" time="903.8914" type="appl" />
         <tli id="T1096" time="904.7628" type="appl" />
         <tli id="T1097" time="905.6342" type="appl" />
         <tli id="T1098" time="906.5056" type="appl" />
         <tli id="T1099" time="907.377" type="appl" />
         <tli id="T1100" time="908.185" type="appl" />
         <tli id="T1101" time="908.993" type="appl" />
         <tli id="T1102" time="909.801" type="appl" />
         <tli id="T1103" time="910.9136666666667" type="appl" />
         <tli id="T1104" time="912.0263333333334" type="appl" />
         <tli id="T1105" time="913.139" type="appl" />
         <tli id="T1106" time="914.37925" type="appl" />
         <tli id="T1107" time="915.6195" type="appl" />
         <tli id="T1108" time="916.8597500000001" type="appl" />
         <tli id="T1109" time="918.1" type="appl" />
         <tli id="T1110" time="919.491" type="appl" />
         <tli id="T1111" time="920.8820000000001" type="appl" />
         <tli id="T1112" time="922.273" type="appl" />
         <tli id="T1113" time="923.664" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx-AkEE"
                      id="tx-AkEE"
                      speaker="AkEE"
                      type="t">
         <timeline-fork end="T634" start="T633">
            <tli id="T633.tx-AkEE.1" />
         </timeline-fork>
         <timeline-fork end="T814" start="T813">
            <tli id="T813.tx-AkEE.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-AkEE">
            <ts e="T24" id="Seg_0" n="sc" s="T1">
               <ts e="T9" id="Seg_2" n="HIAT:u" s="T1">
                  <nts id="Seg_3" n="HIAT:ip">–</nts>
                  <nts id="Seg_4" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_6" n="HIAT:w" s="T1">Jelʼena</ts>
                  <nts id="Seg_7" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_9" n="HIAT:w" s="T2">Trifanovna</ts>
                  <nts id="Seg_10" n="HIAT:ip">,</nts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">kɨratɨk</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">eː</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">kepseːŋ</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_21" n="HIAT:ip">(</nts>
                  <ts e="T7" id="Seg_23" n="HIAT:w" s="T6">bɨ-</ts>
                  <nts id="Seg_24" n="HIAT:ip">)</nts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_27" n="HIAT:w" s="T7">bejegit</ts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_30" n="HIAT:w" s="T8">tuskutunan</ts>
                  <nts id="Seg_31" n="HIAT:ip">.</nts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_34" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_36" n="HIAT:w" s="T9">Kanna</ts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_39" n="HIAT:w" s="T10">hirdeːkkitinij</ts>
                  <nts id="Seg_40" n="HIAT:ip">,</nts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_43" n="HIAT:w" s="T11">kanna</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_46" n="HIAT:w" s="T12">töröːbükküt</ts>
                  <nts id="Seg_47" n="HIAT:ip">,</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_50" n="HIAT:w" s="T13">kergenner</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_53" n="HIAT:w" s="T14">kimneːk</ts>
                  <nts id="Seg_54" n="HIAT:ip">?</nts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T24" id="Seg_57" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_59" n="HIAT:w" s="T15">Bejegit</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_62" n="HIAT:w" s="T16">tuhunan</ts>
                  <nts id="Seg_63" n="HIAT:ip">,</nts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_65" n="HIAT:ip">(</nts>
                  <ts e="T18" id="Seg_67" n="HIAT:w" s="T17">kerget-</ts>
                  <nts id="Seg_68" n="HIAT:ip">)</nts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_71" n="HIAT:w" s="T18">kergennergit</ts>
                  <nts id="Seg_72" n="HIAT:ip">,</nts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_75" n="HIAT:w" s="T19">ogolorgut</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_78" n="HIAT:w" s="T20">tustarɨnan</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_81" n="HIAT:w" s="T22">tu͡oktar</ts>
                  <nts id="Seg_82" n="HIAT:ip">.</nts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T202" id="Seg_84" n="sc" s="T187">
               <ts e="T194" id="Seg_86" n="HIAT:u" s="T187">
                  <nts id="Seg_87" n="HIAT:ip">–</nts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_90" n="HIAT:w" s="T187">Ehigi</ts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_93" n="HIAT:w" s="T189">sʼemʼjaːgɨt</ts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_96" n="HIAT:w" s="T191">ulakan</ts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_99" n="HIAT:w" s="T192">bagaj</ts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_102" n="HIAT:w" s="T193">ebit</ts>
                  <nts id="Seg_103" n="HIAT:ip">.</nts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T202" id="Seg_106" n="HIAT:u" s="T194">
                  <ts e="T195" id="Seg_108" n="HIAT:w" s="T194">Bu</ts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_111" n="HIAT:w" s="T195">ke</ts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_114" n="HIAT:w" s="T196">ogolorgut</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_117" n="HIAT:w" s="T197">ogoloro</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_120" n="HIAT:w" s="T198">eː</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_123" n="HIAT:w" s="T199">ehi͡eke</ts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_126" n="HIAT:w" s="T200">bu͡olaːččɨlar</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_129" n="HIAT:w" s="T201">duː</ts>
                  <nts id="Seg_130" n="HIAT:ip">?</nts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T299" id="Seg_132" n="sc" s="T277">
               <ts e="T291" id="Seg_134" n="HIAT:u" s="T277">
                  <nts id="Seg_135" n="HIAT:ip">–</nts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_138" n="HIAT:w" s="T277">Oččogo</ts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_141" n="HIAT:w" s="T278">ogolorgutugar</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_144" n="HIAT:w" s="T279">emi͡e</ts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_147" n="HIAT:w" s="T280">bu</ts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_150" n="HIAT:w" s="T281">ehigi</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_153" n="HIAT:w" s="T282">bilegit</ts>
                  <nts id="Seg_154" n="HIAT:ip">,</nts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_157" n="HIAT:w" s="T283">eː</ts>
                  <nts id="Seg_158" n="HIAT:ip">,</nts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_161" n="HIAT:w" s="T284">ügüs</ts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_164" n="HIAT:w" s="T285">bagajɨ</ts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_167" n="HIAT:w" s="T286">oloŋkoloru</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_170" n="HIAT:w" s="T287">da</ts>
                  <nts id="Seg_171" n="HIAT:ip">,</nts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_174" n="HIAT:w" s="T288">ɨrɨ͡alarɨ</ts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_177" n="HIAT:w" s="T289">da</ts>
                  <nts id="Seg_178" n="HIAT:ip">,</nts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_181" n="HIAT:w" s="T290">bɨlɨrgɨlarɨ</ts>
                  <nts id="Seg_182" n="HIAT:ip">.</nts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T299" id="Seg_185" n="HIAT:u" s="T291">
                  <ts e="T292" id="Seg_187" n="HIAT:w" s="T291">Ogolorgutugar</ts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_190" n="HIAT:w" s="T292">emi͡e</ts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_193" n="HIAT:w" s="T293">kepsiːgit</ts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_196" n="HIAT:w" s="T294">bu͡o</ts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_199" n="HIAT:w" s="T295">ginileri</ts>
                  <nts id="Seg_200" n="HIAT:ip">,</nts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_202" n="HIAT:ip">(</nts>
                  <ts e="T297" id="Seg_204" n="HIAT:w" s="T296">gin-</ts>
                  <nts id="Seg_205" n="HIAT:ip">)</nts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_208" n="HIAT:w" s="T297">bili͡ekterin</ts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_211" n="HIAT:w" s="T298">ogolorgut</ts>
                  <nts id="Seg_212" n="HIAT:ip">.</nts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T355" id="Seg_214" n="sc" s="T348">
               <ts e="T355" id="Seg_216" n="HIAT:u" s="T348">
                  <nts id="Seg_217" n="HIAT:ip">–</nts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_220" n="HIAT:w" s="T348">Bejegit</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_223" n="HIAT:w" s="T349">ke</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_226" n="HIAT:w" s="T350">bu</ts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_229" n="HIAT:w" s="T351">oloŋkoloru</ts>
                  <nts id="Seg_230" n="HIAT:ip">,</nts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_233" n="HIAT:w" s="T352">ɨrɨ͡alarɨ</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_236" n="HIAT:w" s="T353">kajtak</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_239" n="HIAT:w" s="T354">bilbikkitinij</ts>
                  <nts id="Seg_240" n="HIAT:ip">?</nts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T517" id="Seg_242" n="sc" s="T506">
               <ts e="T517" id="Seg_244" n="HIAT:u" s="T506">
                  <nts id="Seg_245" n="HIAT:ip">–</nts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507" id="Seg_248" n="HIAT:w" s="T506">De</ts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T508" id="Seg_251" n="HIAT:w" s="T507">kepseːŋ</ts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509" id="Seg_254" n="HIAT:w" s="T508">di͡e</ts>
                  <nts id="Seg_255" n="HIAT:ip">,</nts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T510" id="Seg_258" n="HIAT:w" s="T509">bu</ts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T511" id="Seg_261" n="HIAT:w" s="T510">kajtak</ts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T512" id="Seg_264" n="HIAT:w" s="T511">oːnnʼuːllar</ts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T513" id="Seg_267" n="HIAT:w" s="T512">ol</ts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T514" id="Seg_270" n="HIAT:w" s="T513">bu</ts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T515" id="Seg_273" n="HIAT:w" s="T514">anɨ</ts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T516" id="Seg_276" n="HIAT:w" s="T515">egelbit</ts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T517" id="Seg_279" n="HIAT:w" s="T516">oːnnʼuːgutunan</ts>
                  <nts id="Seg_280" n="HIAT:ip">?</nts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T558" id="Seg_282" n="sc" s="T540">
               <ts e="T547" id="Seg_284" n="HIAT:u" s="T540">
                  <nts id="Seg_285" n="HIAT:ip">–</nts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T541" id="Seg_288" n="HIAT:w" s="T540">Dʼe</ts>
                  <nts id="Seg_289" n="HIAT:ip">,</nts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T542" id="Seg_292" n="HIAT:w" s="T541">ɨllaː</ts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T543" id="Seg_295" n="HIAT:w" s="T542">dʼe</ts>
                  <nts id="Seg_296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T544" id="Seg_298" n="HIAT:w" s="T543">tu͡ok</ts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T545" id="Seg_301" n="HIAT:w" s="T544">emete</ts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T546" id="Seg_304" n="HIAT:w" s="T545">ɨrɨ͡atɨna</ts>
                  <nts id="Seg_305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T547" id="Seg_307" n="HIAT:w" s="T546">bu</ts>
                  <nts id="Seg_308" n="HIAT:ip">.</nts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T558" id="Seg_311" n="HIAT:u" s="T547">
                  <ts e="T548" id="Seg_313" n="HIAT:w" s="T547">Urut</ts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549" id="Seg_316" n="HIAT:w" s="T548">di͡ečči</ts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T550" id="Seg_319" n="HIAT:w" s="T549">etilere</ts>
                  <nts id="Seg_320" n="HIAT:ip">,</nts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T551" id="Seg_323" n="HIAT:w" s="T550">kɨrgɨttar</ts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T552" id="Seg_326" n="HIAT:w" s="T551">ɨllɨːllarɨ</ts>
                  <nts id="Seg_327" n="HIAT:ip">,</nts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T553" id="Seg_330" n="HIAT:w" s="T552">u͡olattar</ts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T554" id="Seg_333" n="HIAT:w" s="T553">ɨllɨːllarɨn</ts>
                  <nts id="Seg_334" n="HIAT:ip">,</nts>
                  <nts id="Seg_335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T555" id="Seg_337" n="HIAT:w" s="T554">onnuk</ts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T556" id="Seg_340" n="HIAT:w" s="T555">ɨrɨ͡alarɨ</ts>
                  <nts id="Seg_341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T557" id="Seg_343" n="HIAT:w" s="T556">bilegin</ts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T558" id="Seg_346" n="HIAT:w" s="T557">du͡o</ts>
                  <nts id="Seg_347" n="HIAT:ip">?</nts>
                  <nts id="Seg_348" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T610" id="Seg_349" n="sc" s="T608">
               <ts e="T610" id="Seg_351" n="HIAT:u" s="T608">
                  <nts id="Seg_352" n="HIAT:ip">–</nts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T609" id="Seg_355" n="HIAT:w" s="T608">Kim</ts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T610" id="Seg_358" n="HIAT:w" s="T609">ɨrɨ͡ataj</ts>
                  <nts id="Seg_359" n="HIAT:ip">?</nts>
                  <nts id="Seg_360" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T641" id="Seg_361" n="sc" s="T621">
               <ts e="T641" id="Seg_363" n="HIAT:u" s="T621">
                  <nts id="Seg_364" n="HIAT:ip">–</nts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T622" id="Seg_367" n="HIAT:w" s="T621">Onton</ts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T623" id="Seg_370" n="HIAT:w" s="T622">bu</ts>
                  <nts id="Seg_371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T624" id="Seg_373" n="HIAT:w" s="T623">ehigi</ts>
                  <nts id="Seg_374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T625" id="Seg_376" n="HIAT:w" s="T624">ügüs</ts>
                  <nts id="Seg_377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T626" id="Seg_379" n="HIAT:w" s="T625">kimi</ts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T627" id="Seg_382" n="HIAT:w" s="T626">bilegit</ts>
                  <nts id="Seg_383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T628" id="Seg_385" n="HIAT:w" s="T627">bɨhɨlaːk</ts>
                  <nts id="Seg_386" n="HIAT:ip">,</nts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T629" id="Seg_389" n="HIAT:w" s="T628">taːbɨrɨːnnarɨ</ts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T630" id="Seg_392" n="HIAT:w" s="T629">da</ts>
                  <nts id="Seg_393" n="HIAT:ip">,</nts>
                  <nts id="Seg_394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T631" id="Seg_396" n="HIAT:w" s="T630">onton</ts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T632" id="Seg_399" n="HIAT:w" s="T631">bu</ts>
                  <nts id="Seg_400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T633" id="Seg_402" n="HIAT:w" s="T632">kimneri</ts>
                  <nts id="Seg_403" n="HIAT:ip">,</nts>
                  <nts id="Seg_404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T633.tx-AkEE.1" id="Seg_406" n="HIAT:w" s="T633">pʼerʼexvat</ts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T634" id="Seg_409" n="HIAT:w" s="T633.tx-AkEE.1">dɨxaːnʼija</ts>
                  <nts id="Seg_410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T635" id="Seg_412" n="HIAT:w" s="T634">di͡en</ts>
                  <nts id="Seg_413" n="HIAT:ip">,</nts>
                  <nts id="Seg_414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T636" id="Seg_416" n="HIAT:w" s="T635">iti</ts>
                  <nts id="Seg_417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T637" id="Seg_419" n="HIAT:w" s="T636">ki͡e</ts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T638" id="Seg_422" n="HIAT:w" s="T637">hɨrsallar</ts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T639" id="Seg_425" n="HIAT:w" s="T638">duː</ts>
                  <nts id="Seg_426" n="HIAT:ip">,</nts>
                  <nts id="Seg_427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T640" id="Seg_429" n="HIAT:w" s="T639">tu͡ok</ts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T641" id="Seg_432" n="HIAT:w" s="T640">duː</ts>
                  <nts id="Seg_433" n="HIAT:ip">?</nts>
                  <nts id="Seg_434" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T727" id="Seg_435" n="sc" s="T716">
               <ts e="T727" id="Seg_437" n="HIAT:u" s="T716">
                  <nts id="Seg_438" n="HIAT:ip">–</nts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T717" id="Seg_441" n="HIAT:w" s="T716">Bu</ts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T718" id="Seg_444" n="HIAT:w" s="T717">Navarɨbnajga</ts>
                  <nts id="Seg_445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T719" id="Seg_447" n="HIAT:w" s="T718">Marʼina</ts>
                  <nts id="Seg_448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T720" id="Seg_450" n="HIAT:w" s="T719">di͡en</ts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T721" id="Seg_453" n="HIAT:w" s="T720">kɨːs</ts>
                  <nts id="Seg_454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T722" id="Seg_456" n="HIAT:w" s="T721">Bʼerʼegavaːja</ts>
                  <nts id="Seg_457" n="HIAT:ip">,</nts>
                  <nts id="Seg_458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T723" id="Seg_460" n="HIAT:w" s="T722">iti</ts>
                  <nts id="Seg_461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T724" id="Seg_463" n="HIAT:w" s="T723">ehigi</ts>
                  <nts id="Seg_464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T725" id="Seg_466" n="HIAT:w" s="T724">kɨːskɨt</ts>
                  <nts id="Seg_467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T726" id="Seg_469" n="HIAT:w" s="T725">bu͡oltak</ts>
                  <nts id="Seg_470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T727" id="Seg_472" n="HIAT:w" s="T726">du͡o</ts>
                  <nts id="Seg_473" n="HIAT:ip">?</nts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T760" id="Seg_475" n="sc" s="T731">
               <ts e="T737" id="Seg_477" n="HIAT:u" s="T731">
                  <nts id="Seg_478" n="HIAT:ip">–</nts>
                  <nts id="Seg_479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T732" id="Seg_481" n="HIAT:w" s="T731">E</ts>
                  <nts id="Seg_482" n="HIAT:ip">,</nts>
                  <nts id="Seg_483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T733" id="Seg_485" n="HIAT:w" s="T732">ol</ts>
                  <nts id="Seg_486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T734" id="Seg_488" n="HIAT:w" s="T733">ihin</ts>
                  <nts id="Seg_489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T735" id="Seg_491" n="HIAT:w" s="T734">oloŋkoloru</ts>
                  <nts id="Seg_492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T736" id="Seg_494" n="HIAT:w" s="T735">biler</ts>
                  <nts id="Seg_495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T737" id="Seg_497" n="HIAT:w" s="T736">ebit</ts>
                  <nts id="Seg_498" n="HIAT:ip">.</nts>
                  <nts id="Seg_499" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T750" id="Seg_501" n="HIAT:u" s="T737">
                  <ts e="T738" id="Seg_503" n="HIAT:w" s="T737">Munna</ts>
                  <nts id="Seg_504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T739" id="Seg_506" n="HIAT:w" s="T738">kahan</ts>
                  <nts id="Seg_507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T740" id="Seg_509" n="HIAT:w" s="T739">ere</ts>
                  <nts id="Seg_510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T741" id="Seg_512" n="HIAT:w" s="T740">kele</ts>
                  <nts id="Seg_513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T742" id="Seg_515" n="HIAT:w" s="T741">hɨldʼan</ts>
                  <nts id="Seg_516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T743" id="Seg_518" n="HIAT:w" s="T742">kepseːbite</ts>
                  <nts id="Seg_519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T744" id="Seg_521" n="HIAT:w" s="T743">biːr</ts>
                  <nts id="Seg_522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T745" id="Seg_524" n="HIAT:w" s="T744">oloŋkonu</ts>
                  <nts id="Seg_525" n="HIAT:ip">,</nts>
                  <nts id="Seg_526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T746" id="Seg_528" n="HIAT:w" s="T745">min</ts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T747" id="Seg_531" n="HIAT:w" s="T746">diːbin</ts>
                  <nts id="Seg_532" n="HIAT:ip">,</nts>
                  <nts id="Seg_533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T748" id="Seg_535" n="HIAT:w" s="T747">kantan</ts>
                  <nts id="Seg_536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T749" id="Seg_538" n="HIAT:w" s="T748">bilere</ts>
                  <nts id="Seg_539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T750" id="Seg_541" n="HIAT:w" s="T749">bu͡olu͡oj</ts>
                  <nts id="Seg_542" n="HIAT:ip">.</nts>
                  <nts id="Seg_543" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T760" id="Seg_545" n="HIAT:u" s="T750">
                  <ts e="T751" id="Seg_547" n="HIAT:w" s="T750">Dʼe</ts>
                  <nts id="Seg_548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T752" id="Seg_550" n="HIAT:w" s="T751">onto</ts>
                  <nts id="Seg_551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T753" id="Seg_553" n="HIAT:w" s="T752">en</ts>
                  <nts id="Seg_554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T754" id="Seg_556" n="HIAT:w" s="T753">baːr</ts>
                  <nts id="Seg_557" n="HIAT:ip">,</nts>
                  <nts id="Seg_558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T755" id="Seg_560" n="HIAT:w" s="T754">enigitten</ts>
                  <nts id="Seg_561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T756" id="Seg_563" n="HIAT:w" s="T755">isten</ts>
                  <nts id="Seg_564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T757" id="Seg_566" n="HIAT:w" s="T756">oččogo</ts>
                  <nts id="Seg_567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T758" id="Seg_569" n="HIAT:w" s="T757">biler</ts>
                  <nts id="Seg_570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T759" id="Seg_572" n="HIAT:w" s="T758">ebit</ts>
                  <nts id="Seg_573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T760" id="Seg_575" n="HIAT:w" s="T759">ol</ts>
                  <nts id="Seg_576" n="HIAT:ip">.</nts>
                  <nts id="Seg_577" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T792" id="Seg_578" n="sc" s="T789">
               <ts e="T792" id="Seg_580" n="HIAT:u" s="T789">
                  <nts id="Seg_581" n="HIAT:ip">–</nts>
                  <nts id="Seg_582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T790" id="Seg_584" n="HIAT:w" s="T789">Istibetegiŋ</ts>
                  <nts id="Seg_585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T791" id="Seg_587" n="HIAT:w" s="T790">du͡o</ts>
                  <nts id="Seg_588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T792" id="Seg_590" n="HIAT:w" s="T791">radioga</ts>
                  <nts id="Seg_591" n="HIAT:ip">?</nts>
                  <nts id="Seg_592" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T819" id="Seg_593" n="sc" s="T813">
               <ts e="T819" id="Seg_595" n="HIAT:u" s="T813">
                  <nts id="Seg_596" n="HIAT:ip">–</nts>
                  <nts id="Seg_597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T813.tx-AkEE.1" id="Seg_599" n="HIAT:w" s="T813">Vsʼo</ts>
                  <nts id="Seg_600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T814" id="Seg_602" n="HIAT:w" s="T813.tx-AkEE.1">ravno</ts>
                  <nts id="Seg_603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T815" id="Seg_605" n="HIAT:w" s="T814">otto</ts>
                  <nts id="Seg_606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T816" id="Seg_608" n="HIAT:w" s="T815">dʼüːlüːller</ts>
                  <nts id="Seg_609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T817" id="Seg_611" n="HIAT:w" s="T816">bu͡o</ts>
                  <nts id="Seg_612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T818" id="Seg_614" n="HIAT:w" s="T817">otto</ts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T819" id="Seg_617" n="HIAT:w" s="T818">kajtak</ts>
                  <nts id="Seg_618" n="HIAT:ip">.</nts>
                  <nts id="Seg_619" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T848" id="Seg_620" n="sc" s="T826">
               <ts e="T848" id="Seg_622" n="HIAT:u" s="T826">
                  <nts id="Seg_623" n="HIAT:ip">–</nts>
                  <nts id="Seg_624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T827" id="Seg_626" n="HIAT:w" s="T826">Jelʼena</ts>
                  <nts id="Seg_627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T828" id="Seg_629" n="HIAT:w" s="T827">Trʼifanovna</ts>
                  <nts id="Seg_630" n="HIAT:ip">,</nts>
                  <nts id="Seg_631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T829" id="Seg_633" n="HIAT:w" s="T828">bu</ts>
                  <nts id="Seg_634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T830" id="Seg_636" n="HIAT:w" s="T829">ehigitten</ts>
                  <nts id="Seg_637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T831" id="Seg_639" n="HIAT:w" s="T830">atɨn</ts>
                  <nts id="Seg_640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T832" id="Seg_642" n="HIAT:w" s="T831">kim</ts>
                  <nts id="Seg_643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T833" id="Seg_645" n="HIAT:w" s="T832">emete</ts>
                  <nts id="Seg_646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T834" id="Seg_648" n="HIAT:w" s="T833">baːr</ts>
                  <nts id="Seg_649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T835" id="Seg_651" n="HIAT:w" s="T834">du͡o</ts>
                  <nts id="Seg_652" n="HIAT:ip">,</nts>
                  <nts id="Seg_653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T836" id="Seg_655" n="HIAT:w" s="T835">er</ts>
                  <nts id="Seg_656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T837" id="Seg_658" n="HIAT:w" s="T836">kihi</ts>
                  <nts id="Seg_659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T838" id="Seg_661" n="HIAT:w" s="T837">duː</ts>
                  <nts id="Seg_662" n="HIAT:ip">,</nts>
                  <nts id="Seg_663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T839" id="Seg_665" n="HIAT:w" s="T838">dʼaktar</ts>
                  <nts id="Seg_666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T840" id="Seg_668" n="HIAT:w" s="T839">duː</ts>
                  <nts id="Seg_669" n="HIAT:ip">,</nts>
                  <nts id="Seg_670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T841" id="Seg_672" n="HIAT:w" s="T840">Novorɨbnɨjga</ts>
                  <nts id="Seg_673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T842" id="Seg_675" n="HIAT:w" s="T841">kim</ts>
                  <nts id="Seg_676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T843" id="Seg_678" n="HIAT:w" s="T842">biler</ts>
                  <nts id="Seg_679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T844" id="Seg_681" n="HIAT:w" s="T843">oloŋkonu</ts>
                  <nts id="Seg_682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T845" id="Seg_684" n="HIAT:w" s="T844">daː</ts>
                  <nts id="Seg_685" n="HIAT:ip">,</nts>
                  <nts id="Seg_686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T846" id="Seg_688" n="HIAT:w" s="T845">bɨlɨrgɨ</ts>
                  <nts id="Seg_689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T847" id="Seg_691" n="HIAT:w" s="T846">daː</ts>
                  <nts id="Seg_692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T848" id="Seg_694" n="HIAT:w" s="T847">ɨrɨ͡anɨ</ts>
                  <nts id="Seg_695" n="HIAT:ip">?</nts>
                  <nts id="Seg_696" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T887" id="Seg_697" n="sc" s="T883">
               <ts e="T887" id="Seg_699" n="HIAT:u" s="T883">
                  <nts id="Seg_700" n="HIAT:ip">–</nts>
                  <nts id="Seg_701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T884" id="Seg_703" n="HIAT:w" s="T883">Atɨn</ts>
                  <nts id="Seg_704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T885" id="Seg_706" n="HIAT:w" s="T884">kihiler</ts>
                  <nts id="Seg_707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T886" id="Seg_709" n="HIAT:w" s="T885">ke</ts>
                  <nts id="Seg_710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_711" n="HIAT:ip">(</nts>
                  <nts id="Seg_712" n="HIAT:ip">(</nts>
                  <ats e="T887" id="Seg_713" n="HIAT:non-pho" s="T886">…</ats>
                  <nts id="Seg_714" n="HIAT:ip">)</nts>
                  <nts id="Seg_715" n="HIAT:ip">)</nts>
                  <nts id="Seg_716" n="HIAT:ip">?</nts>
                  <nts id="Seg_717" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T932" id="Seg_718" n="sc" s="T918">
               <ts e="T932" id="Seg_720" n="HIAT:u" s="T918">
                  <nts id="Seg_721" n="HIAT:ip">–</nts>
                  <nts id="Seg_722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T919" id="Seg_724" n="HIAT:w" s="T918">Bu</ts>
                  <nts id="Seg_725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T920" id="Seg_727" n="HIAT:w" s="T919">bejegit</ts>
                  <nts id="Seg_728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T921" id="Seg_730" n="HIAT:w" s="T920">Navarɨbnɨjgɨtɨgar</ts>
                  <nts id="Seg_731" n="HIAT:ip">,</nts>
                  <nts id="Seg_732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T922" id="Seg_734" n="HIAT:w" s="T921">klubka</ts>
                  <nts id="Seg_735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T923" id="Seg_737" n="HIAT:w" s="T922">ehigi</ts>
                  <nts id="Seg_738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T924" id="Seg_740" n="HIAT:w" s="T923">bu</ts>
                  <nts id="Seg_741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T925" id="Seg_743" n="HIAT:w" s="T924">kihiler</ts>
                  <nts id="Seg_744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T926" id="Seg_746" n="HIAT:w" s="T925">innileriger</ts>
                  <nts id="Seg_747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T927" id="Seg_749" n="HIAT:w" s="T926">vɨstupajdaːn</ts>
                  <nts id="Seg_750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T928" id="Seg_752" n="HIAT:w" s="T927">kepseːččigit</ts>
                  <nts id="Seg_753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T929" id="Seg_755" n="HIAT:w" s="T928">duː</ts>
                  <nts id="Seg_756" n="HIAT:ip">,</nts>
                  <nts id="Seg_757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T930" id="Seg_759" n="HIAT:w" s="T929">ɨllaːččɨgɨt</ts>
                  <nts id="Seg_760" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T931" id="Seg_762" n="HIAT:w" s="T930">duː</ts>
                  <nts id="Seg_763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T932" id="Seg_765" n="HIAT:w" s="T931">oloŋkolorgutun</ts>
                  <nts id="Seg_766" n="HIAT:ip">?</nts>
                  <nts id="Seg_767" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T990" id="Seg_768" n="sc" s="T980">
               <ts e="T990" id="Seg_770" n="HIAT:u" s="T980">
                  <nts id="Seg_771" n="HIAT:ip">–</nts>
                  <nts id="Seg_772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T981" id="Seg_774" n="HIAT:w" s="T980">Ehigi</ts>
                  <nts id="Seg_775" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T982" id="Seg_777" n="HIAT:w" s="T981">ogolorgut</ts>
                  <nts id="Seg_778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T983" id="Seg_780" n="HIAT:w" s="T982">agaj</ts>
                  <nts id="Seg_781" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T984" id="Seg_783" n="HIAT:w" s="T983">bilellere</ts>
                  <nts id="Seg_784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T985" id="Seg_786" n="HIAT:w" s="T984">bu͡olu͡o</ts>
                  <nts id="Seg_787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T986" id="Seg_789" n="HIAT:w" s="T985">bu</ts>
                  <nts id="Seg_790" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T987" id="Seg_792" n="HIAT:w" s="T986">oloŋkoloru</ts>
                  <nts id="Seg_793" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T988" id="Seg_795" n="HIAT:w" s="T987">da</ts>
                  <nts id="Seg_796" n="HIAT:ip">,</nts>
                  <nts id="Seg_797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T989" id="Seg_799" n="HIAT:w" s="T988">ɨrɨ͡alarɨ</ts>
                  <nts id="Seg_800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T990" id="Seg_802" n="HIAT:w" s="T989">da</ts>
                  <nts id="Seg_803" n="HIAT:ip">.</nts>
                  <nts id="Seg_804" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1041" id="Seg_805" n="sc" s="T1022">
               <ts e="T1029" id="Seg_807" n="HIAT:u" s="T1022">
                  <nts id="Seg_808" n="HIAT:ip">–</nts>
                  <nts id="Seg_809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1023" id="Seg_811" n="HIAT:w" s="T1022">Anɨ</ts>
                  <nts id="Seg_812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1024" id="Seg_814" n="HIAT:w" s="T1023">tu͡ok</ts>
                  <nts id="Seg_815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1025" id="Seg_817" n="HIAT:w" s="T1024">emete</ts>
                  <nts id="Seg_818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1026" id="Seg_820" n="HIAT:w" s="T1025">oloŋkotuna</ts>
                  <nts id="Seg_821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1027" id="Seg_823" n="HIAT:w" s="T1026">kepseː</ts>
                  <nts id="Seg_824" n="HIAT:ip">,</nts>
                  <nts id="Seg_825" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1028" id="Seg_827" n="HIAT:w" s="T1027">bu</ts>
                  <nts id="Seg_828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1029" id="Seg_830" n="HIAT:w" s="T1028">oloŋko</ts>
                  <nts id="Seg_831" n="HIAT:ip">.</nts>
                  <nts id="Seg_832" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1033" id="Seg_834" n="HIAT:u" s="T1029">
                  <ts e="T1030" id="Seg_836" n="HIAT:w" s="T1029">Vaabšʼe-ta</ts>
                  <nts id="Seg_837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1031" id="Seg_839" n="HIAT:w" s="T1030">bu</ts>
                  <nts id="Seg_840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1032" id="Seg_842" n="HIAT:w" s="T1031">otto</ts>
                  <nts id="Seg_843" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1033" id="Seg_845" n="HIAT:w" s="T1032">barɨta</ts>
                  <nts id="Seg_846" n="HIAT:ip">…</nts>
                  <nts id="Seg_847" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1037" id="Seg_849" n="HIAT:u" s="T1033">
                  <ts e="T1034" id="Seg_851" n="HIAT:w" s="T1033">Kepseterbit</ts>
                  <nts id="Seg_852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1035" id="Seg_854" n="HIAT:w" s="T1034">bihi͡ene</ts>
                  <nts id="Seg_855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1036" id="Seg_857" n="HIAT:w" s="T1035">kaːlɨ͡aga</ts>
                  <nts id="Seg_858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1037" id="Seg_860" n="HIAT:w" s="T1036">bu͡o</ts>
                  <nts id="Seg_861" n="HIAT:ip">.</nts>
                  <nts id="Seg_862" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1041" id="Seg_864" n="HIAT:u" s="T1037">
                  <ts e="T1038" id="Seg_866" n="HIAT:w" s="T1037">Koju͡ot</ts>
                  <nts id="Seg_867" n="HIAT:ip">,</nts>
                  <nts id="Seg_868" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1039" id="Seg_870" n="HIAT:w" s="T1038">koju͡ot</ts>
                  <nts id="Seg_871" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1040" id="Seg_873" n="HIAT:w" s="T1039">ogoloruŋ</ts>
                  <nts id="Seg_874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1041" id="Seg_876" n="HIAT:w" s="T1040">isti͡ekterin</ts>
                  <nts id="Seg_877" n="HIAT:ip">.</nts>
                  <nts id="Seg_878" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1053" id="Seg_879" n="sc" s="T1042">
               <ts e="T1047" id="Seg_881" n="HIAT:u" s="T1042">
                  <nts id="Seg_882" n="HIAT:ip">–</nts>
                  <nts id="Seg_883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1043" id="Seg_885" n="HIAT:w" s="T1042">Kepseː</ts>
                  <nts id="Seg_886" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1044" id="Seg_888" n="HIAT:w" s="T1043">tu͡ok</ts>
                  <nts id="Seg_889" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1045" id="Seg_891" n="HIAT:w" s="T1044">emete</ts>
                  <nts id="Seg_892" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1046" id="Seg_894" n="HIAT:w" s="T1045">oloŋkotuna</ts>
                  <nts id="Seg_895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1047" id="Seg_897" n="HIAT:w" s="T1046">bu</ts>
                  <nts id="Seg_898" n="HIAT:ip">.</nts>
                  <nts id="Seg_899" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1053" id="Seg_901" n="HIAT:u" s="T1047">
                  <ts e="T1048" id="Seg_903" n="HIAT:w" s="T1047">Maːjdʼin</ts>
                  <nts id="Seg_904" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1049" id="Seg_906" n="HIAT:w" s="T1048">oloŋkotton</ts>
                  <nts id="Seg_907" n="HIAT:ip">,</nts>
                  <nts id="Seg_908" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1050" id="Seg_910" n="HIAT:w" s="T1049">e</ts>
                  <nts id="Seg_911" n="HIAT:ip">,</nts>
                  <nts id="Seg_912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1051" id="Seg_914" n="HIAT:w" s="T1050">kepseːbit</ts>
                  <nts id="Seg_915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1052" id="Seg_917" n="HIAT:w" s="T1051">oloŋkoguttan</ts>
                  <nts id="Seg_918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1053" id="Seg_920" n="HIAT:w" s="T1052">atɨnna</ts>
                  <nts id="Seg_921" n="HIAT:ip">.</nts>
                  <nts id="Seg_922" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1074" id="Seg_923" n="sc" s="T1060">
               <ts e="T1068" id="Seg_925" n="HIAT:u" s="T1060">
                  <nts id="Seg_926" n="HIAT:ip">–</nts>
                  <nts id="Seg_927" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1061" id="Seg_929" n="HIAT:w" s="T1060">Jelʼena</ts>
                  <nts id="Seg_930" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1062" id="Seg_932" n="HIAT:w" s="T1061">Trʼifanovna</ts>
                  <nts id="Seg_933" n="HIAT:ip">,</nts>
                  <nts id="Seg_934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1063" id="Seg_936" n="HIAT:w" s="T1062">ješʼo</ts>
                  <nts id="Seg_937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1064" id="Seg_939" n="HIAT:w" s="T1063">ehigini</ts>
                  <nts id="Seg_940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1065" id="Seg_942" n="HIAT:w" s="T1064">kohoːnnoru</ts>
                  <nts id="Seg_943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1066" id="Seg_945" n="HIAT:w" s="T1065">sačʼinʼajdɨːgɨn</ts>
                  <nts id="Seg_946" n="HIAT:ip">,</nts>
                  <nts id="Seg_947" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1067" id="Seg_949" n="HIAT:w" s="T1066">diːgin</ts>
                  <nts id="Seg_950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1068" id="Seg_952" n="HIAT:w" s="T1067">diː</ts>
                  <nts id="Seg_953" n="HIAT:ip">.</nts>
                  <nts id="Seg_954" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1074" id="Seg_956" n="HIAT:u" s="T1068">
                  <ts e="T1069" id="Seg_958" n="HIAT:w" s="T1068">Dʼe</ts>
                  <nts id="Seg_959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1070" id="Seg_961" n="HIAT:w" s="T1069">kepseː</ts>
                  <nts id="Seg_962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1071" id="Seg_964" n="HIAT:w" s="T1070">dʼe</ts>
                  <nts id="Seg_965" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1072" id="Seg_967" n="HIAT:w" s="T1071">tu͡ok</ts>
                  <nts id="Seg_968" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1073" id="Seg_970" n="HIAT:w" s="T1072">emete</ts>
                  <nts id="Seg_971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1074" id="Seg_973" n="HIAT:w" s="T1073">kohoːnun</ts>
                  <nts id="Seg_974" n="HIAT:ip">.</nts>
                  <nts id="Seg_975" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-AkEE">
            <ts e="T24" id="Seg_976" n="sc" s="T1">
               <ts e="T2" id="Seg_978" n="e" s="T1">– Jelʼena </ts>
               <ts e="T3" id="Seg_980" n="e" s="T2">Trifanovna, </ts>
               <ts e="T4" id="Seg_982" n="e" s="T3">kɨratɨk </ts>
               <ts e="T5" id="Seg_984" n="e" s="T4">eː </ts>
               <ts e="T6" id="Seg_986" n="e" s="T5">kepseːŋ </ts>
               <ts e="T7" id="Seg_988" n="e" s="T6">(bɨ-) </ts>
               <ts e="T8" id="Seg_990" n="e" s="T7">bejegit </ts>
               <ts e="T9" id="Seg_992" n="e" s="T8">tuskutunan. </ts>
               <ts e="T10" id="Seg_994" n="e" s="T9">Kanna </ts>
               <ts e="T11" id="Seg_996" n="e" s="T10">hirdeːkkitinij, </ts>
               <ts e="T12" id="Seg_998" n="e" s="T11">kanna </ts>
               <ts e="T13" id="Seg_1000" n="e" s="T12">töröːbükküt, </ts>
               <ts e="T14" id="Seg_1002" n="e" s="T13">kergenner </ts>
               <ts e="T15" id="Seg_1004" n="e" s="T14">kimneːk? </ts>
               <ts e="T16" id="Seg_1006" n="e" s="T15">Bejegit </ts>
               <ts e="T17" id="Seg_1008" n="e" s="T16">tuhunan, </ts>
               <ts e="T18" id="Seg_1010" n="e" s="T17">(kerget-) </ts>
               <ts e="T19" id="Seg_1012" n="e" s="T18">kergennergit, </ts>
               <ts e="T20" id="Seg_1014" n="e" s="T19">ogolorgut </ts>
               <ts e="T22" id="Seg_1016" n="e" s="T20">tustarɨnan </ts>
               <ts e="T24" id="Seg_1018" n="e" s="T22">tu͡oktar. </ts>
            </ts>
            <ts e="T202" id="Seg_1019" n="sc" s="T187">
               <ts e="T189" id="Seg_1021" n="e" s="T187">– Ehigi </ts>
               <ts e="T191" id="Seg_1023" n="e" s="T189">sʼemʼjaːgɨt </ts>
               <ts e="T192" id="Seg_1025" n="e" s="T191">ulakan </ts>
               <ts e="T193" id="Seg_1027" n="e" s="T192">bagaj </ts>
               <ts e="T194" id="Seg_1029" n="e" s="T193">ebit. </ts>
               <ts e="T195" id="Seg_1031" n="e" s="T194">Bu </ts>
               <ts e="T196" id="Seg_1033" n="e" s="T195">ke </ts>
               <ts e="T197" id="Seg_1035" n="e" s="T196">ogolorgut </ts>
               <ts e="T198" id="Seg_1037" n="e" s="T197">ogoloro </ts>
               <ts e="T199" id="Seg_1039" n="e" s="T198">eː </ts>
               <ts e="T200" id="Seg_1041" n="e" s="T199">ehi͡eke </ts>
               <ts e="T201" id="Seg_1043" n="e" s="T200">bu͡olaːččɨlar </ts>
               <ts e="T202" id="Seg_1045" n="e" s="T201">duː? </ts>
            </ts>
            <ts e="T299" id="Seg_1046" n="sc" s="T277">
               <ts e="T278" id="Seg_1048" n="e" s="T277">– Oččogo </ts>
               <ts e="T279" id="Seg_1050" n="e" s="T278">ogolorgutugar </ts>
               <ts e="T280" id="Seg_1052" n="e" s="T279">emi͡e </ts>
               <ts e="T281" id="Seg_1054" n="e" s="T280">bu </ts>
               <ts e="T282" id="Seg_1056" n="e" s="T281">ehigi </ts>
               <ts e="T283" id="Seg_1058" n="e" s="T282">bilegit, </ts>
               <ts e="T284" id="Seg_1060" n="e" s="T283">eː, </ts>
               <ts e="T285" id="Seg_1062" n="e" s="T284">ügüs </ts>
               <ts e="T286" id="Seg_1064" n="e" s="T285">bagajɨ </ts>
               <ts e="T287" id="Seg_1066" n="e" s="T286">oloŋkoloru </ts>
               <ts e="T288" id="Seg_1068" n="e" s="T287">da, </ts>
               <ts e="T289" id="Seg_1070" n="e" s="T288">ɨrɨ͡alarɨ </ts>
               <ts e="T290" id="Seg_1072" n="e" s="T289">da, </ts>
               <ts e="T291" id="Seg_1074" n="e" s="T290">bɨlɨrgɨlarɨ. </ts>
               <ts e="T292" id="Seg_1076" n="e" s="T291">Ogolorgutugar </ts>
               <ts e="T293" id="Seg_1078" n="e" s="T292">emi͡e </ts>
               <ts e="T294" id="Seg_1080" n="e" s="T293">kepsiːgit </ts>
               <ts e="T295" id="Seg_1082" n="e" s="T294">bu͡o </ts>
               <ts e="T296" id="Seg_1084" n="e" s="T295">ginileri, </ts>
               <ts e="T297" id="Seg_1086" n="e" s="T296">(gin-) </ts>
               <ts e="T298" id="Seg_1088" n="e" s="T297">bili͡ekterin </ts>
               <ts e="T299" id="Seg_1090" n="e" s="T298">ogolorgut. </ts>
            </ts>
            <ts e="T355" id="Seg_1091" n="sc" s="T348">
               <ts e="T349" id="Seg_1093" n="e" s="T348">– Bejegit </ts>
               <ts e="T350" id="Seg_1095" n="e" s="T349">ke </ts>
               <ts e="T351" id="Seg_1097" n="e" s="T350">bu </ts>
               <ts e="T352" id="Seg_1099" n="e" s="T351">oloŋkoloru, </ts>
               <ts e="T353" id="Seg_1101" n="e" s="T352">ɨrɨ͡alarɨ </ts>
               <ts e="T354" id="Seg_1103" n="e" s="T353">kajtak </ts>
               <ts e="T355" id="Seg_1105" n="e" s="T354">bilbikkitinij? </ts>
            </ts>
            <ts e="T517" id="Seg_1106" n="sc" s="T506">
               <ts e="T507" id="Seg_1108" n="e" s="T506">– De </ts>
               <ts e="T508" id="Seg_1110" n="e" s="T507">kepseːŋ </ts>
               <ts e="T509" id="Seg_1112" n="e" s="T508">di͡e, </ts>
               <ts e="T510" id="Seg_1114" n="e" s="T509">bu </ts>
               <ts e="T511" id="Seg_1116" n="e" s="T510">kajtak </ts>
               <ts e="T512" id="Seg_1118" n="e" s="T511">oːnnʼuːllar </ts>
               <ts e="T513" id="Seg_1120" n="e" s="T512">ol </ts>
               <ts e="T514" id="Seg_1122" n="e" s="T513">bu </ts>
               <ts e="T515" id="Seg_1124" n="e" s="T514">anɨ </ts>
               <ts e="T516" id="Seg_1126" n="e" s="T515">egelbit </ts>
               <ts e="T517" id="Seg_1128" n="e" s="T516">oːnnʼuːgutunan? </ts>
            </ts>
            <ts e="T558" id="Seg_1129" n="sc" s="T540">
               <ts e="T541" id="Seg_1131" n="e" s="T540">– Dʼe, </ts>
               <ts e="T542" id="Seg_1133" n="e" s="T541">ɨllaː </ts>
               <ts e="T543" id="Seg_1135" n="e" s="T542">dʼe </ts>
               <ts e="T544" id="Seg_1137" n="e" s="T543">tu͡ok </ts>
               <ts e="T545" id="Seg_1139" n="e" s="T544">emete </ts>
               <ts e="T546" id="Seg_1141" n="e" s="T545">ɨrɨ͡atɨna </ts>
               <ts e="T547" id="Seg_1143" n="e" s="T546">bu. </ts>
               <ts e="T548" id="Seg_1145" n="e" s="T547">Urut </ts>
               <ts e="T549" id="Seg_1147" n="e" s="T548">di͡ečči </ts>
               <ts e="T550" id="Seg_1149" n="e" s="T549">etilere, </ts>
               <ts e="T551" id="Seg_1151" n="e" s="T550">kɨrgɨttar </ts>
               <ts e="T552" id="Seg_1153" n="e" s="T551">ɨllɨːllarɨ, </ts>
               <ts e="T553" id="Seg_1155" n="e" s="T552">u͡olattar </ts>
               <ts e="T554" id="Seg_1157" n="e" s="T553">ɨllɨːllarɨn, </ts>
               <ts e="T555" id="Seg_1159" n="e" s="T554">onnuk </ts>
               <ts e="T556" id="Seg_1161" n="e" s="T555">ɨrɨ͡alarɨ </ts>
               <ts e="T557" id="Seg_1163" n="e" s="T556">bilegin </ts>
               <ts e="T558" id="Seg_1165" n="e" s="T557">du͡o? </ts>
            </ts>
            <ts e="T610" id="Seg_1166" n="sc" s="T608">
               <ts e="T609" id="Seg_1168" n="e" s="T608">– Kim </ts>
               <ts e="T610" id="Seg_1170" n="e" s="T609">ɨrɨ͡ataj? </ts>
            </ts>
            <ts e="T641" id="Seg_1171" n="sc" s="T621">
               <ts e="T622" id="Seg_1173" n="e" s="T621">– Onton </ts>
               <ts e="T623" id="Seg_1175" n="e" s="T622">bu </ts>
               <ts e="T624" id="Seg_1177" n="e" s="T623">ehigi </ts>
               <ts e="T625" id="Seg_1179" n="e" s="T624">ügüs </ts>
               <ts e="T626" id="Seg_1181" n="e" s="T625">kimi </ts>
               <ts e="T627" id="Seg_1183" n="e" s="T626">bilegit </ts>
               <ts e="T628" id="Seg_1185" n="e" s="T627">bɨhɨlaːk, </ts>
               <ts e="T629" id="Seg_1187" n="e" s="T628">taːbɨrɨːnnarɨ </ts>
               <ts e="T630" id="Seg_1189" n="e" s="T629">da, </ts>
               <ts e="T631" id="Seg_1191" n="e" s="T630">onton </ts>
               <ts e="T632" id="Seg_1193" n="e" s="T631">bu </ts>
               <ts e="T633" id="Seg_1195" n="e" s="T632">kimneri, </ts>
               <ts e="T634" id="Seg_1197" n="e" s="T633">pʼerʼexvat dɨxaːnʼija </ts>
               <ts e="T635" id="Seg_1199" n="e" s="T634">di͡en, </ts>
               <ts e="T636" id="Seg_1201" n="e" s="T635">iti </ts>
               <ts e="T637" id="Seg_1203" n="e" s="T636">ki͡e </ts>
               <ts e="T638" id="Seg_1205" n="e" s="T637">hɨrsallar </ts>
               <ts e="T639" id="Seg_1207" n="e" s="T638">duː, </ts>
               <ts e="T640" id="Seg_1209" n="e" s="T639">tu͡ok </ts>
               <ts e="T641" id="Seg_1211" n="e" s="T640">duː? </ts>
            </ts>
            <ts e="T727" id="Seg_1212" n="sc" s="T716">
               <ts e="T717" id="Seg_1214" n="e" s="T716">– Bu </ts>
               <ts e="T718" id="Seg_1216" n="e" s="T717">Navarɨbnajga </ts>
               <ts e="T719" id="Seg_1218" n="e" s="T718">Marʼina </ts>
               <ts e="T720" id="Seg_1220" n="e" s="T719">di͡en </ts>
               <ts e="T721" id="Seg_1222" n="e" s="T720">kɨːs </ts>
               <ts e="T722" id="Seg_1224" n="e" s="T721">Bʼerʼegavaːja, </ts>
               <ts e="T723" id="Seg_1226" n="e" s="T722">iti </ts>
               <ts e="T724" id="Seg_1228" n="e" s="T723">ehigi </ts>
               <ts e="T725" id="Seg_1230" n="e" s="T724">kɨːskɨt </ts>
               <ts e="T726" id="Seg_1232" n="e" s="T725">bu͡oltak </ts>
               <ts e="T727" id="Seg_1234" n="e" s="T726">du͡o? </ts>
            </ts>
            <ts e="T760" id="Seg_1235" n="sc" s="T731">
               <ts e="T732" id="Seg_1237" n="e" s="T731">– E, </ts>
               <ts e="T733" id="Seg_1239" n="e" s="T732">ol </ts>
               <ts e="T734" id="Seg_1241" n="e" s="T733">ihin </ts>
               <ts e="T735" id="Seg_1243" n="e" s="T734">oloŋkoloru </ts>
               <ts e="T736" id="Seg_1245" n="e" s="T735">biler </ts>
               <ts e="T737" id="Seg_1247" n="e" s="T736">ebit. </ts>
               <ts e="T738" id="Seg_1249" n="e" s="T737">Munna </ts>
               <ts e="T739" id="Seg_1251" n="e" s="T738">kahan </ts>
               <ts e="T740" id="Seg_1253" n="e" s="T739">ere </ts>
               <ts e="T741" id="Seg_1255" n="e" s="T740">kele </ts>
               <ts e="T742" id="Seg_1257" n="e" s="T741">hɨldʼan </ts>
               <ts e="T743" id="Seg_1259" n="e" s="T742">kepseːbite </ts>
               <ts e="T744" id="Seg_1261" n="e" s="T743">biːr </ts>
               <ts e="T745" id="Seg_1263" n="e" s="T744">oloŋkonu, </ts>
               <ts e="T746" id="Seg_1265" n="e" s="T745">min </ts>
               <ts e="T747" id="Seg_1267" n="e" s="T746">diːbin, </ts>
               <ts e="T748" id="Seg_1269" n="e" s="T747">kantan </ts>
               <ts e="T749" id="Seg_1271" n="e" s="T748">bilere </ts>
               <ts e="T750" id="Seg_1273" n="e" s="T749">bu͡olu͡oj. </ts>
               <ts e="T751" id="Seg_1275" n="e" s="T750">Dʼe </ts>
               <ts e="T752" id="Seg_1277" n="e" s="T751">onto </ts>
               <ts e="T753" id="Seg_1279" n="e" s="T752">en </ts>
               <ts e="T754" id="Seg_1281" n="e" s="T753">baːr, </ts>
               <ts e="T755" id="Seg_1283" n="e" s="T754">enigitten </ts>
               <ts e="T756" id="Seg_1285" n="e" s="T755">isten </ts>
               <ts e="T757" id="Seg_1287" n="e" s="T756">oččogo </ts>
               <ts e="T758" id="Seg_1289" n="e" s="T757">biler </ts>
               <ts e="T759" id="Seg_1291" n="e" s="T758">ebit </ts>
               <ts e="T760" id="Seg_1293" n="e" s="T759">ol. </ts>
            </ts>
            <ts e="T792" id="Seg_1294" n="sc" s="T789">
               <ts e="T790" id="Seg_1296" n="e" s="T789">– Istibetegiŋ </ts>
               <ts e="T791" id="Seg_1298" n="e" s="T790">du͡o </ts>
               <ts e="T792" id="Seg_1300" n="e" s="T791">radioga? </ts>
            </ts>
            <ts e="T819" id="Seg_1301" n="sc" s="T813">
               <ts e="T814" id="Seg_1303" n="e" s="T813">– Vsʼo ravno </ts>
               <ts e="T815" id="Seg_1305" n="e" s="T814">otto </ts>
               <ts e="T816" id="Seg_1307" n="e" s="T815">dʼüːlüːller </ts>
               <ts e="T817" id="Seg_1309" n="e" s="T816">bu͡o </ts>
               <ts e="T818" id="Seg_1311" n="e" s="T817">otto </ts>
               <ts e="T819" id="Seg_1313" n="e" s="T818">kajtak. </ts>
            </ts>
            <ts e="T848" id="Seg_1314" n="sc" s="T826">
               <ts e="T827" id="Seg_1316" n="e" s="T826">– Jelʼena </ts>
               <ts e="T828" id="Seg_1318" n="e" s="T827">Trʼifanovna, </ts>
               <ts e="T829" id="Seg_1320" n="e" s="T828">bu </ts>
               <ts e="T830" id="Seg_1322" n="e" s="T829">ehigitten </ts>
               <ts e="T831" id="Seg_1324" n="e" s="T830">atɨn </ts>
               <ts e="T832" id="Seg_1326" n="e" s="T831">kim </ts>
               <ts e="T833" id="Seg_1328" n="e" s="T832">emete </ts>
               <ts e="T834" id="Seg_1330" n="e" s="T833">baːr </ts>
               <ts e="T835" id="Seg_1332" n="e" s="T834">du͡o, </ts>
               <ts e="T836" id="Seg_1334" n="e" s="T835">er </ts>
               <ts e="T837" id="Seg_1336" n="e" s="T836">kihi </ts>
               <ts e="T838" id="Seg_1338" n="e" s="T837">duː, </ts>
               <ts e="T839" id="Seg_1340" n="e" s="T838">dʼaktar </ts>
               <ts e="T840" id="Seg_1342" n="e" s="T839">duː, </ts>
               <ts e="T841" id="Seg_1344" n="e" s="T840">Novorɨbnɨjga </ts>
               <ts e="T842" id="Seg_1346" n="e" s="T841">kim </ts>
               <ts e="T843" id="Seg_1348" n="e" s="T842">biler </ts>
               <ts e="T844" id="Seg_1350" n="e" s="T843">oloŋkonu </ts>
               <ts e="T845" id="Seg_1352" n="e" s="T844">daː, </ts>
               <ts e="T846" id="Seg_1354" n="e" s="T845">bɨlɨrgɨ </ts>
               <ts e="T847" id="Seg_1356" n="e" s="T846">daː </ts>
               <ts e="T848" id="Seg_1358" n="e" s="T847">ɨrɨ͡anɨ? </ts>
            </ts>
            <ts e="T887" id="Seg_1359" n="sc" s="T883">
               <ts e="T884" id="Seg_1361" n="e" s="T883">– Atɨn </ts>
               <ts e="T885" id="Seg_1363" n="e" s="T884">kihiler </ts>
               <ts e="T886" id="Seg_1365" n="e" s="T885">ke </ts>
               <ts e="T887" id="Seg_1367" n="e" s="T886">((…))? </ts>
            </ts>
            <ts e="T932" id="Seg_1368" n="sc" s="T918">
               <ts e="T919" id="Seg_1370" n="e" s="T918">– Bu </ts>
               <ts e="T920" id="Seg_1372" n="e" s="T919">bejegit </ts>
               <ts e="T921" id="Seg_1374" n="e" s="T920">Navarɨbnɨjgɨtɨgar, </ts>
               <ts e="T922" id="Seg_1376" n="e" s="T921">klubka </ts>
               <ts e="T923" id="Seg_1378" n="e" s="T922">ehigi </ts>
               <ts e="T924" id="Seg_1380" n="e" s="T923">bu </ts>
               <ts e="T925" id="Seg_1382" n="e" s="T924">kihiler </ts>
               <ts e="T926" id="Seg_1384" n="e" s="T925">innileriger </ts>
               <ts e="T927" id="Seg_1386" n="e" s="T926">vɨstupajdaːn </ts>
               <ts e="T928" id="Seg_1388" n="e" s="T927">kepseːččigit </ts>
               <ts e="T929" id="Seg_1390" n="e" s="T928">duː, </ts>
               <ts e="T930" id="Seg_1392" n="e" s="T929">ɨllaːččɨgɨt </ts>
               <ts e="T931" id="Seg_1394" n="e" s="T930">duː </ts>
               <ts e="T932" id="Seg_1396" n="e" s="T931">oloŋkolorgutun? </ts>
            </ts>
            <ts e="T990" id="Seg_1397" n="sc" s="T980">
               <ts e="T981" id="Seg_1399" n="e" s="T980">– Ehigi </ts>
               <ts e="T982" id="Seg_1401" n="e" s="T981">ogolorgut </ts>
               <ts e="T983" id="Seg_1403" n="e" s="T982">agaj </ts>
               <ts e="T984" id="Seg_1405" n="e" s="T983">bilellere </ts>
               <ts e="T985" id="Seg_1407" n="e" s="T984">bu͡olu͡o </ts>
               <ts e="T986" id="Seg_1409" n="e" s="T985">bu </ts>
               <ts e="T987" id="Seg_1411" n="e" s="T986">oloŋkoloru </ts>
               <ts e="T988" id="Seg_1413" n="e" s="T987">da, </ts>
               <ts e="T989" id="Seg_1415" n="e" s="T988">ɨrɨ͡alarɨ </ts>
               <ts e="T990" id="Seg_1417" n="e" s="T989">da. </ts>
            </ts>
            <ts e="T1041" id="Seg_1418" n="sc" s="T1022">
               <ts e="T1023" id="Seg_1420" n="e" s="T1022">– Anɨ </ts>
               <ts e="T1024" id="Seg_1422" n="e" s="T1023">tu͡ok </ts>
               <ts e="T1025" id="Seg_1424" n="e" s="T1024">emete </ts>
               <ts e="T1026" id="Seg_1426" n="e" s="T1025">oloŋkotuna </ts>
               <ts e="T1027" id="Seg_1428" n="e" s="T1026">kepseː, </ts>
               <ts e="T1028" id="Seg_1430" n="e" s="T1027">bu </ts>
               <ts e="T1029" id="Seg_1432" n="e" s="T1028">oloŋko. </ts>
               <ts e="T1030" id="Seg_1434" n="e" s="T1029">Vaabšʼe-ta </ts>
               <ts e="T1031" id="Seg_1436" n="e" s="T1030">bu </ts>
               <ts e="T1032" id="Seg_1438" n="e" s="T1031">otto </ts>
               <ts e="T1033" id="Seg_1440" n="e" s="T1032">barɨta… </ts>
               <ts e="T1034" id="Seg_1442" n="e" s="T1033">Kepseterbit </ts>
               <ts e="T1035" id="Seg_1444" n="e" s="T1034">bihi͡ene </ts>
               <ts e="T1036" id="Seg_1446" n="e" s="T1035">kaːlɨ͡aga </ts>
               <ts e="T1037" id="Seg_1448" n="e" s="T1036">bu͡o. </ts>
               <ts e="T1038" id="Seg_1450" n="e" s="T1037">Koju͡ot, </ts>
               <ts e="T1039" id="Seg_1452" n="e" s="T1038">koju͡ot </ts>
               <ts e="T1040" id="Seg_1454" n="e" s="T1039">ogoloruŋ </ts>
               <ts e="T1041" id="Seg_1456" n="e" s="T1040">isti͡ekterin. </ts>
            </ts>
            <ts e="T1053" id="Seg_1457" n="sc" s="T1042">
               <ts e="T1043" id="Seg_1459" n="e" s="T1042">– Kepseː </ts>
               <ts e="T1044" id="Seg_1461" n="e" s="T1043">tu͡ok </ts>
               <ts e="T1045" id="Seg_1463" n="e" s="T1044">emete </ts>
               <ts e="T1046" id="Seg_1465" n="e" s="T1045">oloŋkotuna </ts>
               <ts e="T1047" id="Seg_1467" n="e" s="T1046">bu. </ts>
               <ts e="T1048" id="Seg_1469" n="e" s="T1047">Maːjdʼin </ts>
               <ts e="T1049" id="Seg_1471" n="e" s="T1048">oloŋkotton, </ts>
               <ts e="T1050" id="Seg_1473" n="e" s="T1049">e, </ts>
               <ts e="T1051" id="Seg_1475" n="e" s="T1050">kepseːbit </ts>
               <ts e="T1052" id="Seg_1477" n="e" s="T1051">oloŋkoguttan </ts>
               <ts e="T1053" id="Seg_1479" n="e" s="T1052">atɨnna. </ts>
            </ts>
            <ts e="T1074" id="Seg_1480" n="sc" s="T1060">
               <ts e="T1061" id="Seg_1482" n="e" s="T1060">– Jelʼena </ts>
               <ts e="T1062" id="Seg_1484" n="e" s="T1061">Trʼifanovna, </ts>
               <ts e="T1063" id="Seg_1486" n="e" s="T1062">ješʼo </ts>
               <ts e="T1064" id="Seg_1488" n="e" s="T1063">ehigini </ts>
               <ts e="T1065" id="Seg_1490" n="e" s="T1064">kohoːnnoru </ts>
               <ts e="T1066" id="Seg_1492" n="e" s="T1065">sačʼinʼajdɨːgɨn, </ts>
               <ts e="T1067" id="Seg_1494" n="e" s="T1066">diːgin </ts>
               <ts e="T1068" id="Seg_1496" n="e" s="T1067">diː. </ts>
               <ts e="T1069" id="Seg_1498" n="e" s="T1068">Dʼe </ts>
               <ts e="T1070" id="Seg_1500" n="e" s="T1069">kepseː </ts>
               <ts e="T1071" id="Seg_1502" n="e" s="T1070">dʼe </ts>
               <ts e="T1072" id="Seg_1504" n="e" s="T1071">tu͡ok </ts>
               <ts e="T1073" id="Seg_1506" n="e" s="T1072">emete </ts>
               <ts e="T1074" id="Seg_1508" n="e" s="T1073">kohoːnun. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-AkEE">
            <ta e="T9" id="Seg_1509" s="T1">UkET_AkEE_19940424_SongsTales_conv.AkEE.001 (001.001)</ta>
            <ta e="T15" id="Seg_1510" s="T9">UkET_AkEE_19940424_SongsTales_conv.AkEE.002 (001.002)</ta>
            <ta e="T24" id="Seg_1511" s="T15">UkET_AkEE_19940424_SongsTales_conv.AkEE.003 (001.003)</ta>
            <ta e="T194" id="Seg_1512" s="T187">UkET_AkEE_19940424_SongsTales_conv.AkEE.004 (001.021)</ta>
            <ta e="T202" id="Seg_1513" s="T194">UkET_AkEE_19940424_SongsTales_conv.AkEE.005 (001.022)</ta>
            <ta e="T291" id="Seg_1514" s="T277">UkET_AkEE_19940424_SongsTales_conv.AkEE.006 (001.030)</ta>
            <ta e="T299" id="Seg_1515" s="T291">UkET_AkEE_19940424_SongsTales_conv.AkEE.007 (001.031)</ta>
            <ta e="T355" id="Seg_1516" s="T348">UkET_AkEE_19940424_SongsTales_conv.AkEE.008 (001.036)</ta>
            <ta e="T517" id="Seg_1517" s="T506">UkET_AkEE_19940424_SongsTales_conv.AkEE.009 (001.051)</ta>
            <ta e="T547" id="Seg_1518" s="T540">UkET_AkEE_19940424_SongsTales_conv.AkEE.010 (001.057)</ta>
            <ta e="T558" id="Seg_1519" s="T547">UkET_AkEE_19940424_SongsTales_conv.AkEE.011 (001.058)</ta>
            <ta e="T610" id="Seg_1520" s="T608">UkET_AkEE_19940424_SongsTales_conv.AkEE.012 (015.001)</ta>
            <ta e="T641" id="Seg_1521" s="T621">UkET_AkEE_19940424_SongsTales_conv.AkEE.013 (015.004)</ta>
            <ta e="T727" id="Seg_1522" s="T716">UkET_AkEE_19940424_SongsTales_conv.AkEE.014 (031.003)</ta>
            <ta e="T737" id="Seg_1523" s="T731">UkET_AkEE_19940424_SongsTales_conv.AkEE.015 (031.005)</ta>
            <ta e="T750" id="Seg_1524" s="T737">UkET_AkEE_19940424_SongsTales_conv.AkEE.016 (031.006)</ta>
            <ta e="T760" id="Seg_1525" s="T750">UkET_AkEE_19940424_SongsTales_conv.AkEE.017 (031.007)</ta>
            <ta e="T792" id="Seg_1526" s="T789">UkET_AkEE_19940424_SongsTales_conv.AkEE.018 (031.011)</ta>
            <ta e="T819" id="Seg_1527" s="T813">UkET_AkEE_19940424_SongsTales_conv.AkEE.019 (031.015)</ta>
            <ta e="T848" id="Seg_1528" s="T826">UkET_AkEE_19940424_SongsTales_conv.AkEE.020 (031.017)</ta>
            <ta e="T887" id="Seg_1529" s="T883">UkET_AkEE_19940424_SongsTales_conv.AkEE.021 (031.022)</ta>
            <ta e="T932" id="Seg_1530" s="T918">UkET_AkEE_19940424_SongsTales_conv.AkEE.022 (031.026)</ta>
            <ta e="T990" id="Seg_1531" s="T980">UkET_AkEE_19940424_SongsTales_conv.AkEE.023 (031.032)</ta>
            <ta e="T1029" id="Seg_1532" s="T1022">UkET_AkEE_19940424_SongsTales_conv.AkEE.024 (031.037)</ta>
            <ta e="T1033" id="Seg_1533" s="T1029">UkET_AkEE_19940424_SongsTales_conv.AkEE.025 (031.038)</ta>
            <ta e="T1037" id="Seg_1534" s="T1033">UkET_AkEE_19940424_SongsTales_conv.AkEE.026 (031.039)</ta>
            <ta e="T1041" id="Seg_1535" s="T1037">UkET_AkEE_19940424_SongsTales_conv.AkEE.027 (031.040)</ta>
            <ta e="T1047" id="Seg_1536" s="T1042">UkET_AkEE_19940424_SongsTales_conv.AkEE.028 (031.042)</ta>
            <ta e="T1053" id="Seg_1537" s="T1047">UkET_AkEE_19940424_SongsTales_conv.AkEE.029 (031.043)</ta>
            <ta e="T1068" id="Seg_1538" s="T1060">UkET_AkEE_19940424_SongsTales_conv.AkEE.030 (033.001)</ta>
            <ta e="T1074" id="Seg_1539" s="T1068">UkET_AkEE_19940424_SongsTales_conv.AkEE.031 (033.002)</ta>
         </annotation>
         <annotation name="st" tierref="st-AkEE">
            <ta e="T9" id="Seg_1540" s="T1">Аксенова: Елена Трифоновна, кыратык э–э кэпсээӈ бы (бу) бэйэгит тускутунан.</ta>
            <ta e="T15" id="Seg_1541" s="T9">Канна һирдээккитиний (һирдээккитий), канна төрөөбүккүт, кэргэннэр (кэргэӈӈит) кимнээк?</ta>
            <ta e="T24" id="Seg_1542" s="T15">А: Бэйэгит туһунан (туһунан), кэргэннэргит, оголоргут тустарынан туокта…</ta>
            <ta e="T194" id="Seg_1543" s="T187">А: Эһиги семьягыт улакан багай эбит.</ta>
            <ta e="T202" id="Seg_1544" s="T194">Бу киэ оголоргут оголоро э эһиэкэ буолаччылар дуу?</ta>
            <ta e="T291" id="Seg_1545" s="T277">А: Оччого оголоргутугар эмиэ бу эһиги билэгит, э-э, үгүс багайы олоӈколору да, ырыалары да – былыргылары.</ta>
            <ta e="T299" id="Seg_1546" s="T291">А: Оголоргутугар эмиэ кэпсиигит буо гинилэри, гин.. билиэктэрин оголоргут. </ta>
            <ta e="T355" id="Seg_1547" s="T348">А: Бэйэгит киэ бу олоӈколору, ыарыалары кайтак билбиккитиний?</ta>
            <ta e="T517" id="Seg_1548" s="T506">А: Дэ кэпсиэӈ диэ, бу кайтак оонньууллар ол бу аны эгэлбит онньургутунан?</ta>
            <ta e="T547" id="Seg_1549" s="T540">А: Дьэ, ыллаа дьэ туок эмэтэ ырыатына, бу…</ta>
            <ta e="T558" id="Seg_1550" s="T547">А: Урут диэччи этилэрэ, кыргыттар ыллыаллар (ыллыыллар), уолаттар ыллыыллар, оннук ырыалары билэгин дуо?</ta>
            <ta e="T610" id="Seg_1551" s="T608">А: Ким ырыатай?</ta>
            <ta e="T641" id="Seg_1552" s="T621">А: Онтон бу эһиги үгүс кими билэгит быһылаак – таабырыыннары да, онтон бу кимнэри, перехват дыхания диэн, ити киэ һырсаллар дуу, туок дуу?</ta>
            <ta e="T727" id="Seg_1553" s="T716">А: Бу Новорыбнайга Марина диэн кыыс Береговая, ити эһиги кыыскыт буолтак дуо?</ta>
            <ta e="T737" id="Seg_1554" s="T731">А: Э, ол иһин олоӈколору билэр эбит.</ta>
            <ta e="T750" id="Seg_1555" s="T737">А: Мунна каһан эрэ кэлэ һылдьан кэпсээбитэ биир олоӈкону, мин диибин, кантан билэрэ буолуой.</ta>
            <ta e="T760" id="Seg_1556" s="T750">А: Дьэ онто эн баар. Энигиттэн истэн оччого билэр эбит ол.</ta>
            <ta e="T792" id="Seg_1557" s="T789">А: Истибэтэгиӈ дуо радиога?</ta>
            <ta e="T819" id="Seg_1558" s="T813">А: Всё равно отто дьүлүллэр (дьүүллүүллэр) буо отто кайтак.</ta>
            <ta e="T848" id="Seg_1559" s="T826">А: Елена Трифоновна, бу эһигиттэн атын ким эмэ баар дуо: эр киһи дуу, дьактар дуу – Новорыбныйга ким билэр олоӈкону даа, былыргы даа ырыаны?</ta>
            <ta e="T887" id="Seg_1560" s="T883">A: Атын киһилер кэ ((…))?</ta>
            <ta e="T932" id="Seg_1561" s="T918">А: Бу бэйэгит Новорыбныйгытыгар, клубка эһиги бу киһилэр идилэригэр (иннилэригэр) выступайдаан кэпсэччигит дуу, ыллааччыгыт ду олоӈколоргутун?</ta>
            <ta e="T990" id="Seg_1562" s="T980">А: Эһиги оголоргут агай билэллэрэ буолуо бу олоӈколору да, ырыалары да.</ta>
            <ta e="T1029" id="Seg_1563" s="T1022">А: Аны туок эмэтэ олоӈкотуна кэпсээ, бу олоӈко.</ta>
            <ta e="T1033" id="Seg_1564" s="T1029">Вообще-то бу отто барыта…</ta>
            <ta e="T1037" id="Seg_1565" s="T1033">А: Кэпсэтэрбит биһиэнэ каалыага буо.</ta>
            <ta e="T1041" id="Seg_1566" s="T1037">А: Койуот, койуот оголоруӈ истиэктэрин.</ta>
            <ta e="T1047" id="Seg_1567" s="T1042">А: Кэпсээ туок эмэтэ олоӈкотуна бу.</ta>
            <ta e="T1053" id="Seg_1568" s="T1047">А: Маайдьин олоӈкоттон, э, кэпсээбит олоӈкогуттан атынна.</ta>
            <ta e="T1068" id="Seg_1569" s="T1060">А: Елена Трифоновна, ещё эһигини коһуоннору сочиняйдыыгын, диигин дии.</ta>
            <ta e="T1074" id="Seg_1570" s="T1068">А: Дьэ кэпсээ дьэ туок эмэтэ коһоонун.</ta>
         </annotation>
         <annotation name="ts" tierref="ts-AkEE">
            <ta e="T9" id="Seg_1571" s="T1">– Jelʼena Trifanovna, kɨratɨk eː kepseːŋ (bɨ-) bejegit tuskutunan. </ta>
            <ta e="T15" id="Seg_1572" s="T9">Kanna hirdeːkkitinij, kanna töröːbükküt, kergenner kimneːk? </ta>
            <ta e="T24" id="Seg_1573" s="T15">Bejegit tuhunan, (kerget-) kergennergit, ogolorgut tustarɨnan tu͡oktar. </ta>
            <ta e="T194" id="Seg_1574" s="T187">– Ehigi sʼemʼjaːgɨt ulakan bagaj ebit. </ta>
            <ta e="T202" id="Seg_1575" s="T194">Bu ke ogolorgut ogoloro eː ehi͡eke bu͡olaːččɨlar duː? </ta>
            <ta e="T291" id="Seg_1576" s="T277">– Oččogo ogolorgutugar emi͡e bu ehigi bilegit, eː, ügüs bagajɨ oloŋkoloru da, ɨrɨ͡alarɨ da, bɨlɨrgɨlarɨ. </ta>
            <ta e="T299" id="Seg_1577" s="T291">Ogolorgutugar emi͡e kepsiːgit bu͡o ginileri, (gin-) bili͡ekterin ogolorgut. </ta>
            <ta e="T355" id="Seg_1578" s="T348">– Bejegit ke bu oloŋkoloru, ɨrɨ͡alarɨ kajtak bilbikkitinij? </ta>
            <ta e="T517" id="Seg_1579" s="T506">– De kepseːŋ di͡e, bu kajtak oːnnʼuːllar ol bu anɨ egelbit oːnnʼuːgutunan? </ta>
            <ta e="T547" id="Seg_1580" s="T540">– Dʼe, ɨllaː dʼe tu͡ok emete ɨrɨ͡atɨna bu. </ta>
            <ta e="T558" id="Seg_1581" s="T547">Urut di͡ečči etilere, kɨrgɨttar ɨllɨːllarɨ, u͡olattar ɨllɨːllarɨn, onnuk ɨrɨ͡alarɨ bilegin du͡o? </ta>
            <ta e="T610" id="Seg_1582" s="T608">– Kim ɨrɨ͡ataj? </ta>
            <ta e="T641" id="Seg_1583" s="T621">– Onton bu ehigi ügüs kimi bilegit bɨhɨlaːk, taːbɨrɨːnnarɨ da, onton bu kimneri, pʼerʼexvat dɨxaːnʼija di͡en, iti ki͡e hɨrsallar duː, tu͡ok duː? </ta>
            <ta e="T727" id="Seg_1584" s="T716">– Bu Navarɨbnajga Marʼina di͡en kɨːs Bʼerʼegavaːja, iti ehigi kɨːskɨt bu͡oltak du͡o? </ta>
            <ta e="T737" id="Seg_1585" s="T731">– E, ol ihin oloŋkoloru biler ebit. </ta>
            <ta e="T750" id="Seg_1586" s="T737">Munna kahan ere kele hɨldʼan kepseːbite biːr oloŋkonu, min diːbin, kantan bilere bu͡olu͡oj. </ta>
            <ta e="T760" id="Seg_1587" s="T750">Dʼe onto en baːr, enigitten isten oččogo biler ebit ol. </ta>
            <ta e="T792" id="Seg_1588" s="T789">– Istibetegiŋ du͡o radioga? </ta>
            <ta e="T819" id="Seg_1589" s="T813">– Vsʼo ravno otto dʼüːlüːller bu͡o otto kajtak. </ta>
            <ta e="T848" id="Seg_1590" s="T826">– Jelʼena Trʼifanovna, bu ehigitten atɨn kim emete baːr du͡o, er kihi duː, dʼaktar duː, Novorɨbnɨjga kim biler oloŋkonu daː, bɨlɨrgɨ daː ɨrɨ͡anɨ? </ta>
            <ta e="T887" id="Seg_1591" s="T883">– Atɨn kihiler ke ((…))? </ta>
            <ta e="T932" id="Seg_1592" s="T918">– Bu bejegit Navarɨbnɨjgɨtɨgar, klubka ehigi bu kihiler innileriger vɨstupajdaːn kepseːččigit duː, ɨllaːččɨgɨt duː oloŋkolorgutun? </ta>
            <ta e="T990" id="Seg_1593" s="T980">– Ehigi ogolorgut agaj bilellere bu͡olu͡o bu oloŋkoloru da, ɨrɨ͡alarɨ da. </ta>
            <ta e="T1029" id="Seg_1594" s="T1022">– Anɨ tu͡ok emete oloŋkotuna kepseː, bu oloŋko. </ta>
            <ta e="T1033" id="Seg_1595" s="T1029">Vaabšʼe-ta bu otto barɨta… </ta>
            <ta e="T1037" id="Seg_1596" s="T1033">Kepseterbit bihi͡ene kaːlɨ͡aga bu͡o. </ta>
            <ta e="T1041" id="Seg_1597" s="T1037">Koju͡ot, koju͡ot ogoloruŋ isti͡ekterin. </ta>
            <ta e="T1047" id="Seg_1598" s="T1042">– Kepseː tu͡ok emete oloŋkotuna bu. </ta>
            <ta e="T1053" id="Seg_1599" s="T1047">Maːjdʼin oloŋkotton, e, kepseːbit oloŋkoguttan atɨnna. </ta>
            <ta e="T1068" id="Seg_1600" s="T1060">– Jelʼena Trʼifanovna, ješʼo ehigini kohoːnnoru sačʼinʼajdɨːgɨn, diːgin diː. </ta>
            <ta e="T1074" id="Seg_1601" s="T1068">Dʼe kepseː dʼe tu͡ok emete kohoːnun. </ta>
         </annotation>
         <annotation name="mb" tierref="mb-AkEE">
            <ta e="T2" id="Seg_1602" s="T1">Jelʼena</ta>
            <ta e="T3" id="Seg_1603" s="T2">Trifanovna</ta>
            <ta e="T4" id="Seg_1604" s="T3">kɨratɨk</ta>
            <ta e="T5" id="Seg_1605" s="T4">eː</ta>
            <ta e="T6" id="Seg_1606" s="T5">kepseː-ŋ</ta>
            <ta e="T8" id="Seg_1607" s="T7">beje-git</ta>
            <ta e="T9" id="Seg_1608" s="T8">tus-kutu-nan</ta>
            <ta e="T10" id="Seg_1609" s="T9">kanna</ta>
            <ta e="T11" id="Seg_1610" s="T10">hir-deːk-kitin=ij</ta>
            <ta e="T12" id="Seg_1611" s="T11">kanna</ta>
            <ta e="T13" id="Seg_1612" s="T12">töröː-bük-küt</ta>
            <ta e="T14" id="Seg_1613" s="T13">kergen-ner</ta>
            <ta e="T15" id="Seg_1614" s="T14">kim-neːk</ta>
            <ta e="T16" id="Seg_1615" s="T15">beje-git</ta>
            <ta e="T17" id="Seg_1616" s="T16">tuh-u-nan</ta>
            <ta e="T19" id="Seg_1617" s="T18">kergen-ner-git</ta>
            <ta e="T20" id="Seg_1618" s="T19">ogo-lor-gut</ta>
            <ta e="T22" id="Seg_1619" s="T20">tus-tarɨ-nan</ta>
            <ta e="T24" id="Seg_1620" s="T22">tu͡ok-tar</ta>
            <ta e="T189" id="Seg_1621" s="T187">ehigi</ta>
            <ta e="T191" id="Seg_1622" s="T189">sʼemʼjaː-gɨt</ta>
            <ta e="T192" id="Seg_1623" s="T191">ulakan</ta>
            <ta e="T193" id="Seg_1624" s="T192">bagaj</ta>
            <ta e="T194" id="Seg_1625" s="T193">e-bit</ta>
            <ta e="T195" id="Seg_1626" s="T194">bu</ta>
            <ta e="T196" id="Seg_1627" s="T195">ke</ta>
            <ta e="T197" id="Seg_1628" s="T196">ogo-lor-gut</ta>
            <ta e="T198" id="Seg_1629" s="T197">ogo-loro</ta>
            <ta e="T199" id="Seg_1630" s="T198">eː</ta>
            <ta e="T200" id="Seg_1631" s="T199">ehi͡e-ke</ta>
            <ta e="T201" id="Seg_1632" s="T200">bu͡ol-aːččɨ-lar</ta>
            <ta e="T202" id="Seg_1633" s="T201">duː</ta>
            <ta e="T278" id="Seg_1634" s="T277">oččogo</ta>
            <ta e="T279" id="Seg_1635" s="T278">ogo-lor-gutu-gar</ta>
            <ta e="T280" id="Seg_1636" s="T279">emi͡e</ta>
            <ta e="T281" id="Seg_1637" s="T280">bu</ta>
            <ta e="T282" id="Seg_1638" s="T281">ehigi</ta>
            <ta e="T283" id="Seg_1639" s="T282">bil-e-git</ta>
            <ta e="T284" id="Seg_1640" s="T283">eː</ta>
            <ta e="T285" id="Seg_1641" s="T284">ügüs</ta>
            <ta e="T286" id="Seg_1642" s="T285">bagajɨ</ta>
            <ta e="T287" id="Seg_1643" s="T286">oloŋko-lor-u</ta>
            <ta e="T288" id="Seg_1644" s="T287">da</ta>
            <ta e="T289" id="Seg_1645" s="T288">ɨrɨ͡a-lar-ɨ</ta>
            <ta e="T290" id="Seg_1646" s="T289">da</ta>
            <ta e="T291" id="Seg_1647" s="T290">bɨlɨrgɨ-lar-ɨ</ta>
            <ta e="T292" id="Seg_1648" s="T291">ogo-lor-gutu-gar</ta>
            <ta e="T293" id="Seg_1649" s="T292">emi͡e</ta>
            <ta e="T294" id="Seg_1650" s="T293">keps-iː-git</ta>
            <ta e="T295" id="Seg_1651" s="T294">bu͡o</ta>
            <ta e="T296" id="Seg_1652" s="T295">giniler-i</ta>
            <ta e="T298" id="Seg_1653" s="T297">bil-i͡ek-teri-n</ta>
            <ta e="T299" id="Seg_1654" s="T298">ogo-lor-gut</ta>
            <ta e="T349" id="Seg_1655" s="T348">beje-git</ta>
            <ta e="T350" id="Seg_1656" s="T349">ke</ta>
            <ta e="T351" id="Seg_1657" s="T350">bu</ta>
            <ta e="T352" id="Seg_1658" s="T351">oloŋko-lor-u</ta>
            <ta e="T353" id="Seg_1659" s="T352">ɨrɨ͡a-lar-ɨ</ta>
            <ta e="T354" id="Seg_1660" s="T353">kajtak</ta>
            <ta e="T355" id="Seg_1661" s="T354">bil-bik-kitin=ij</ta>
            <ta e="T507" id="Seg_1662" s="T506">de</ta>
            <ta e="T508" id="Seg_1663" s="T507">kepseː-ŋ</ta>
            <ta e="T509" id="Seg_1664" s="T508">di͡e</ta>
            <ta e="T510" id="Seg_1665" s="T509">bu</ta>
            <ta e="T511" id="Seg_1666" s="T510">kajtak</ta>
            <ta e="T512" id="Seg_1667" s="T511">oːnnʼuː-l-lar</ta>
            <ta e="T513" id="Seg_1668" s="T512">ol</ta>
            <ta e="T514" id="Seg_1669" s="T513">bu</ta>
            <ta e="T515" id="Seg_1670" s="T514">anɨ</ta>
            <ta e="T516" id="Seg_1671" s="T515">egel-bit</ta>
            <ta e="T517" id="Seg_1672" s="T516">oːnnʼ-uː-gutu-nan</ta>
            <ta e="T541" id="Seg_1673" s="T540">dʼe</ta>
            <ta e="T542" id="Seg_1674" s="T541">ɨllaː</ta>
            <ta e="T543" id="Seg_1675" s="T542">dʼe</ta>
            <ta e="T544" id="Seg_1676" s="T543">tu͡ok</ta>
            <ta e="T545" id="Seg_1677" s="T544">emete</ta>
            <ta e="T546" id="Seg_1678" s="T545">ɨrɨ͡a-tɨ-na</ta>
            <ta e="T547" id="Seg_1679" s="T546">bu</ta>
            <ta e="T548" id="Seg_1680" s="T547">urut</ta>
            <ta e="T549" id="Seg_1681" s="T548">di͡e-čči</ta>
            <ta e="T550" id="Seg_1682" s="T549">e-ti-lere</ta>
            <ta e="T551" id="Seg_1683" s="T550">kɨrgɨt-tar</ta>
            <ta e="T552" id="Seg_1684" s="T551">ɨllɨː-l-lar-ɨ</ta>
            <ta e="T553" id="Seg_1685" s="T552">u͡olat-tar</ta>
            <ta e="T554" id="Seg_1686" s="T553">ɨllɨː-l-larɨ-n</ta>
            <ta e="T555" id="Seg_1687" s="T554">onnuk</ta>
            <ta e="T556" id="Seg_1688" s="T555">ɨrɨ͡a-lar-ɨ</ta>
            <ta e="T557" id="Seg_1689" s="T556">bil-e-gin</ta>
            <ta e="T558" id="Seg_1690" s="T557">du͡o</ta>
            <ta e="T609" id="Seg_1691" s="T608">kim</ta>
            <ta e="T610" id="Seg_1692" s="T609">ɨrɨ͡a-ta=j</ta>
            <ta e="T622" id="Seg_1693" s="T621">onton</ta>
            <ta e="T623" id="Seg_1694" s="T622">bu</ta>
            <ta e="T624" id="Seg_1695" s="T623">ehigi</ta>
            <ta e="T625" id="Seg_1696" s="T624">ügüs</ta>
            <ta e="T626" id="Seg_1697" s="T625">kim-i</ta>
            <ta e="T627" id="Seg_1698" s="T626">bil-e-git</ta>
            <ta e="T628" id="Seg_1699" s="T627">bɨhɨlaːk</ta>
            <ta e="T629" id="Seg_1700" s="T628">taːbɨrɨːn-nar-ɨ</ta>
            <ta e="T630" id="Seg_1701" s="T629">da</ta>
            <ta e="T631" id="Seg_1702" s="T630">onton</ta>
            <ta e="T632" id="Seg_1703" s="T631">bu</ta>
            <ta e="T633" id="Seg_1704" s="T632">kim-ner-i</ta>
            <ta e="T634" id="Seg_1705" s="T633">pʼerʼexvat dɨxaːnʼija</ta>
            <ta e="T635" id="Seg_1706" s="T634">di͡e-n</ta>
            <ta e="T636" id="Seg_1707" s="T635">iti</ta>
            <ta e="T637" id="Seg_1708" s="T636">ki͡e</ta>
            <ta e="T638" id="Seg_1709" s="T637">hɨrs-al-lar</ta>
            <ta e="T639" id="Seg_1710" s="T638">duː</ta>
            <ta e="T640" id="Seg_1711" s="T639">tu͡ok</ta>
            <ta e="T641" id="Seg_1712" s="T640">duː</ta>
            <ta e="T717" id="Seg_1713" s="T716">bu</ta>
            <ta e="T718" id="Seg_1714" s="T717">Navarɨbnaj-ga</ta>
            <ta e="T719" id="Seg_1715" s="T718">Marʼina</ta>
            <ta e="T720" id="Seg_1716" s="T719">di͡e-n</ta>
            <ta e="T721" id="Seg_1717" s="T720">kɨːs</ta>
            <ta e="T722" id="Seg_1718" s="T721">Bʼerʼegavaːja</ta>
            <ta e="T723" id="Seg_1719" s="T722">iti</ta>
            <ta e="T724" id="Seg_1720" s="T723">ehigi</ta>
            <ta e="T725" id="Seg_1721" s="T724">kɨːs-kɨt</ta>
            <ta e="T726" id="Seg_1722" s="T725">bu͡ol-tak</ta>
            <ta e="T727" id="Seg_1723" s="T726">du͡o</ta>
            <ta e="T732" id="Seg_1724" s="T731">e</ta>
            <ta e="T733" id="Seg_1725" s="T732">ol</ta>
            <ta e="T734" id="Seg_1726" s="T733">ihin</ta>
            <ta e="T735" id="Seg_1727" s="T734">oloŋko-lor-u</ta>
            <ta e="T736" id="Seg_1728" s="T735">bil-er</ta>
            <ta e="T737" id="Seg_1729" s="T736">e-bit</ta>
            <ta e="T738" id="Seg_1730" s="T737">munna</ta>
            <ta e="T739" id="Seg_1731" s="T738">kahan</ta>
            <ta e="T740" id="Seg_1732" s="T739">ere</ta>
            <ta e="T741" id="Seg_1733" s="T740">kel-e</ta>
            <ta e="T742" id="Seg_1734" s="T741">hɨldʼ-an</ta>
            <ta e="T743" id="Seg_1735" s="T742">kepseː-bit-e</ta>
            <ta e="T744" id="Seg_1736" s="T743">biːr</ta>
            <ta e="T745" id="Seg_1737" s="T744">oloŋko-nu</ta>
            <ta e="T746" id="Seg_1738" s="T745">min</ta>
            <ta e="T747" id="Seg_1739" s="T746">d-iː-bin</ta>
            <ta e="T748" id="Seg_1740" s="T747">kantan</ta>
            <ta e="T749" id="Seg_1741" s="T748">bil-er-e</ta>
            <ta e="T750" id="Seg_1742" s="T749">bu͡ol-u͡o=j</ta>
            <ta e="T751" id="Seg_1743" s="T750">dʼe</ta>
            <ta e="T752" id="Seg_1744" s="T751">onto</ta>
            <ta e="T753" id="Seg_1745" s="T752">en</ta>
            <ta e="T754" id="Seg_1746" s="T753">baːr</ta>
            <ta e="T755" id="Seg_1747" s="T754">enigi-tten</ta>
            <ta e="T756" id="Seg_1748" s="T755">ist-en</ta>
            <ta e="T757" id="Seg_1749" s="T756">oččogo</ta>
            <ta e="T758" id="Seg_1750" s="T757">bil-er</ta>
            <ta e="T759" id="Seg_1751" s="T758">e-bit</ta>
            <ta e="T760" id="Seg_1752" s="T759">ol</ta>
            <ta e="T790" id="Seg_1753" s="T789">ist-i-beteg-i-ŋ</ta>
            <ta e="T791" id="Seg_1754" s="T790">du͡o</ta>
            <ta e="T792" id="Seg_1755" s="T791">radʼio-ga</ta>
            <ta e="T814" id="Seg_1756" s="T813">vsʼo ravno</ta>
            <ta e="T815" id="Seg_1757" s="T814">otto</ta>
            <ta e="T816" id="Seg_1758" s="T815">dʼüːlüː-l-ler</ta>
            <ta e="T817" id="Seg_1759" s="T816">bu͡o</ta>
            <ta e="T818" id="Seg_1760" s="T817">otto</ta>
            <ta e="T819" id="Seg_1761" s="T818">kajtak</ta>
            <ta e="T827" id="Seg_1762" s="T826">Jelʼena</ta>
            <ta e="T828" id="Seg_1763" s="T827">Trʼifanovna</ta>
            <ta e="T829" id="Seg_1764" s="T828">bu</ta>
            <ta e="T830" id="Seg_1765" s="T829">ehigi-tten</ta>
            <ta e="T831" id="Seg_1766" s="T830">atɨn</ta>
            <ta e="T832" id="Seg_1767" s="T831">kim</ta>
            <ta e="T833" id="Seg_1768" s="T832">emete</ta>
            <ta e="T834" id="Seg_1769" s="T833">baːr</ta>
            <ta e="T835" id="Seg_1770" s="T834">du͡o</ta>
            <ta e="T836" id="Seg_1771" s="T835">er</ta>
            <ta e="T837" id="Seg_1772" s="T836">kihi</ta>
            <ta e="T838" id="Seg_1773" s="T837">duː</ta>
            <ta e="T839" id="Seg_1774" s="T838">dʼaktar</ta>
            <ta e="T840" id="Seg_1775" s="T839">duː</ta>
            <ta e="T841" id="Seg_1776" s="T840">Novorɨbnɨj-ga</ta>
            <ta e="T842" id="Seg_1777" s="T841">kim</ta>
            <ta e="T843" id="Seg_1778" s="T842">bil-er</ta>
            <ta e="T844" id="Seg_1779" s="T843">oloŋko-nu</ta>
            <ta e="T845" id="Seg_1780" s="T844">daː</ta>
            <ta e="T846" id="Seg_1781" s="T845">bɨlɨr-gɨ</ta>
            <ta e="T847" id="Seg_1782" s="T846">daː</ta>
            <ta e="T848" id="Seg_1783" s="T847">ɨrɨ͡a-nɨ</ta>
            <ta e="T884" id="Seg_1784" s="T883">atɨn</ta>
            <ta e="T885" id="Seg_1785" s="T884">kihi-ler</ta>
            <ta e="T886" id="Seg_1786" s="T885">ke</ta>
            <ta e="T919" id="Seg_1787" s="T918">bu</ta>
            <ta e="T920" id="Seg_1788" s="T919">beje-git</ta>
            <ta e="T921" id="Seg_1789" s="T920">Navarɨbnɨj-gɨtɨ-gar</ta>
            <ta e="T922" id="Seg_1790" s="T921">klub-ka</ta>
            <ta e="T923" id="Seg_1791" s="T922">ehigi</ta>
            <ta e="T924" id="Seg_1792" s="T923">bu</ta>
            <ta e="T925" id="Seg_1793" s="T924">kihi-ler</ta>
            <ta e="T926" id="Seg_1794" s="T925">inni-leri-ger</ta>
            <ta e="T927" id="Seg_1795" s="T926">vɨstupaj-daː-n</ta>
            <ta e="T928" id="Seg_1796" s="T927">kepseː-čči-git</ta>
            <ta e="T929" id="Seg_1797" s="T928">duː</ta>
            <ta e="T930" id="Seg_1798" s="T929">ɨllaː-ččɨ-gɨt</ta>
            <ta e="T931" id="Seg_1799" s="T930">duː</ta>
            <ta e="T932" id="Seg_1800" s="T931">oloŋko-lor-gutu-n</ta>
            <ta e="T981" id="Seg_1801" s="T980">ehigi</ta>
            <ta e="T982" id="Seg_1802" s="T981">ogo-lor-gut</ta>
            <ta e="T983" id="Seg_1803" s="T982">agaj</ta>
            <ta e="T984" id="Seg_1804" s="T983">bil-el-lere</ta>
            <ta e="T985" id="Seg_1805" s="T984">bu͡ol-u͡o</ta>
            <ta e="T986" id="Seg_1806" s="T985">bu</ta>
            <ta e="T987" id="Seg_1807" s="T986">oloŋko-lor-u</ta>
            <ta e="T988" id="Seg_1808" s="T987">da</ta>
            <ta e="T989" id="Seg_1809" s="T988">ɨrɨ͡a-lar-ɨ</ta>
            <ta e="T990" id="Seg_1810" s="T989">da</ta>
            <ta e="T1023" id="Seg_1811" s="T1022">anɨ</ta>
            <ta e="T1024" id="Seg_1812" s="T1023">tu͡ok</ta>
            <ta e="T1025" id="Seg_1813" s="T1024">emete</ta>
            <ta e="T1026" id="Seg_1814" s="T1025">oloŋko-tu-na</ta>
            <ta e="T1027" id="Seg_1815" s="T1026">kepseː</ta>
            <ta e="T1028" id="Seg_1816" s="T1027">bu</ta>
            <ta e="T1029" id="Seg_1817" s="T1028">oloŋko</ta>
            <ta e="T1030" id="Seg_1818" s="T1029">vaabšʼe-ta</ta>
            <ta e="T1031" id="Seg_1819" s="T1030">bu</ta>
            <ta e="T1032" id="Seg_1820" s="T1031">otto</ta>
            <ta e="T1033" id="Seg_1821" s="T1032">barɨ-ta</ta>
            <ta e="T1034" id="Seg_1822" s="T1033">kepset-er-bit</ta>
            <ta e="T1035" id="Seg_1823" s="T1034">bihi͡ene</ta>
            <ta e="T1036" id="Seg_1824" s="T1035">kaːl-ɨ͡ag-a</ta>
            <ta e="T1037" id="Seg_1825" s="T1036">bu͡o</ta>
            <ta e="T1038" id="Seg_1826" s="T1037">koju͡ot</ta>
            <ta e="T1039" id="Seg_1827" s="T1038">koju͡ot</ta>
            <ta e="T1040" id="Seg_1828" s="T1039">ogo-lor-u-ŋ</ta>
            <ta e="T1041" id="Seg_1829" s="T1040">ist-i͡ek-teri-n</ta>
            <ta e="T1043" id="Seg_1830" s="T1042">kepseː</ta>
            <ta e="T1044" id="Seg_1831" s="T1043">tu͡ok</ta>
            <ta e="T1045" id="Seg_1832" s="T1044">emete</ta>
            <ta e="T1046" id="Seg_1833" s="T1045">oloŋko-tu-na</ta>
            <ta e="T1047" id="Seg_1834" s="T1046">bu</ta>
            <ta e="T1048" id="Seg_1835" s="T1047">maːjdʼin</ta>
            <ta e="T1049" id="Seg_1836" s="T1048">oloŋko-tton</ta>
            <ta e="T1050" id="Seg_1837" s="T1049">e</ta>
            <ta e="T1051" id="Seg_1838" s="T1050">kepseː-bit</ta>
            <ta e="T1052" id="Seg_1839" s="T1051">oloŋko-gu-ttan</ta>
            <ta e="T1053" id="Seg_1840" s="T1052">atɨn-na</ta>
            <ta e="T1061" id="Seg_1841" s="T1060">Jelʼena</ta>
            <ta e="T1062" id="Seg_1842" s="T1061">Trʼifanovna</ta>
            <ta e="T1063" id="Seg_1843" s="T1062">ješʼo</ta>
            <ta e="T1064" id="Seg_1844" s="T1063">ehigi-ni</ta>
            <ta e="T1065" id="Seg_1845" s="T1064">kohoːn-nor-u</ta>
            <ta e="T1066" id="Seg_1846" s="T1065">sačʼinʼaj-d-ɨː-gɨn</ta>
            <ta e="T1067" id="Seg_1847" s="T1066">d-iː-gin</ta>
            <ta e="T1068" id="Seg_1848" s="T1067">diː</ta>
            <ta e="T1069" id="Seg_1849" s="T1068">dʼe</ta>
            <ta e="T1070" id="Seg_1850" s="T1069">kepseː</ta>
            <ta e="T1071" id="Seg_1851" s="T1070">dʼe</ta>
            <ta e="T1072" id="Seg_1852" s="T1071">tu͡ok</ta>
            <ta e="T1073" id="Seg_1853" s="T1072">emete</ta>
            <ta e="T1074" id="Seg_1854" s="T1073">kohoːn-u-n</ta>
         </annotation>
         <annotation name="mp" tierref="mp-AkEE">
            <ta e="T2" id="Seg_1855" s="T1">Jelʼena</ta>
            <ta e="T3" id="Seg_1856" s="T2">Trifanovna</ta>
            <ta e="T4" id="Seg_1857" s="T3">kɨratɨk</ta>
            <ta e="T5" id="Seg_1858" s="T4">eː</ta>
            <ta e="T6" id="Seg_1859" s="T5">kepseː-ŋ</ta>
            <ta e="T8" id="Seg_1860" s="T7">beje-GIt</ta>
            <ta e="T9" id="Seg_1861" s="T8">tus-GItI-nAn</ta>
            <ta e="T10" id="Seg_1862" s="T9">kanna</ta>
            <ta e="T11" id="Seg_1863" s="T10">hir-LAːK-GIt=Ij</ta>
            <ta e="T12" id="Seg_1864" s="T11">kanna</ta>
            <ta e="T13" id="Seg_1865" s="T12">töröː-BIT-GIt</ta>
            <ta e="T14" id="Seg_1866" s="T13">kergen-LAr</ta>
            <ta e="T15" id="Seg_1867" s="T14">kim-LAːK</ta>
            <ta e="T16" id="Seg_1868" s="T15">beje-GIt</ta>
            <ta e="T17" id="Seg_1869" s="T16">tus-tI-nAn</ta>
            <ta e="T19" id="Seg_1870" s="T18">kergen-LAr-GIt</ta>
            <ta e="T20" id="Seg_1871" s="T19">ogo-LAr-GIt</ta>
            <ta e="T22" id="Seg_1872" s="T20">tus-LArI-nAn</ta>
            <ta e="T24" id="Seg_1873" s="T22">tu͡ok-LAr</ta>
            <ta e="T189" id="Seg_1874" s="T187">ehigi</ta>
            <ta e="T191" id="Seg_1875" s="T189">semja-GIt</ta>
            <ta e="T192" id="Seg_1876" s="T191">ulakan</ta>
            <ta e="T193" id="Seg_1877" s="T192">bagajɨ</ta>
            <ta e="T194" id="Seg_1878" s="T193">e-BIT</ta>
            <ta e="T195" id="Seg_1879" s="T194">bu</ta>
            <ta e="T196" id="Seg_1880" s="T195">ka</ta>
            <ta e="T197" id="Seg_1881" s="T196">ogo-LAr-GIt</ta>
            <ta e="T198" id="Seg_1882" s="T197">ogo-LArA</ta>
            <ta e="T199" id="Seg_1883" s="T198">eː</ta>
            <ta e="T200" id="Seg_1884" s="T199">ehigi-GA</ta>
            <ta e="T201" id="Seg_1885" s="T200">bu͡ol-AːččI-LAr</ta>
            <ta e="T202" id="Seg_1886" s="T201">du͡o</ta>
            <ta e="T278" id="Seg_1887" s="T277">oččogo</ta>
            <ta e="T279" id="Seg_1888" s="T278">ogo-LAr-GItI-GAr</ta>
            <ta e="T280" id="Seg_1889" s="T279">emi͡e</ta>
            <ta e="T281" id="Seg_1890" s="T280">bu</ta>
            <ta e="T282" id="Seg_1891" s="T281">ehigi</ta>
            <ta e="T283" id="Seg_1892" s="T282">bil-A-GIt</ta>
            <ta e="T284" id="Seg_1893" s="T283">eː</ta>
            <ta e="T285" id="Seg_1894" s="T284">ügüs</ta>
            <ta e="T286" id="Seg_1895" s="T285">bagajɨ</ta>
            <ta e="T287" id="Seg_1896" s="T286">oloŋko-LAr-nI</ta>
            <ta e="T288" id="Seg_1897" s="T287">da</ta>
            <ta e="T289" id="Seg_1898" s="T288">ɨrɨ͡a-LAr-nI</ta>
            <ta e="T290" id="Seg_1899" s="T289">da</ta>
            <ta e="T291" id="Seg_1900" s="T290">bɨlɨrgɨ-LAr-nI</ta>
            <ta e="T292" id="Seg_1901" s="T291">ogo-LAr-GItI-GAr</ta>
            <ta e="T293" id="Seg_1902" s="T292">emi͡e</ta>
            <ta e="T294" id="Seg_1903" s="T293">kepseː-A-GIt</ta>
            <ta e="T295" id="Seg_1904" s="T294">bu͡o</ta>
            <ta e="T296" id="Seg_1905" s="T295">giniler-nI</ta>
            <ta e="T298" id="Seg_1906" s="T297">bil-IAK-LArI-n</ta>
            <ta e="T299" id="Seg_1907" s="T298">ogo-LAr-GIt</ta>
            <ta e="T349" id="Seg_1908" s="T348">beje-GIt</ta>
            <ta e="T350" id="Seg_1909" s="T349">ka</ta>
            <ta e="T351" id="Seg_1910" s="T350">bu</ta>
            <ta e="T352" id="Seg_1911" s="T351">oloŋko-LAr-nI</ta>
            <ta e="T353" id="Seg_1912" s="T352">ɨrɨ͡a-LAr-nI</ta>
            <ta e="T354" id="Seg_1913" s="T353">kajdak</ta>
            <ta e="T355" id="Seg_1914" s="T354">bil-BIT-GIt=Ij</ta>
            <ta e="T507" id="Seg_1915" s="T506">dʼe</ta>
            <ta e="T508" id="Seg_1916" s="T507">kepseː-ŋ</ta>
            <ta e="T509" id="Seg_1917" s="T508">dʼe</ta>
            <ta e="T510" id="Seg_1918" s="T509">bu</ta>
            <ta e="T511" id="Seg_1919" s="T510">kajdak</ta>
            <ta e="T512" id="Seg_1920" s="T511">oːnnʼoː-Ar-LAr</ta>
            <ta e="T513" id="Seg_1921" s="T512">ol</ta>
            <ta e="T514" id="Seg_1922" s="T513">bu</ta>
            <ta e="T515" id="Seg_1923" s="T514">anɨ</ta>
            <ta e="T516" id="Seg_1924" s="T515">egel-BIT</ta>
            <ta e="T517" id="Seg_1925" s="T516">oːnnʼoː-Iː-GItI-nAn</ta>
            <ta e="T541" id="Seg_1926" s="T540">dʼe</ta>
            <ta e="T542" id="Seg_1927" s="T541">ɨllaː</ta>
            <ta e="T543" id="Seg_1928" s="T542">dʼe</ta>
            <ta e="T544" id="Seg_1929" s="T543">tu͡ok</ta>
            <ta e="T545" id="Seg_1930" s="T544">eme</ta>
            <ta e="T546" id="Seg_1931" s="T545">ɨrɨ͡a-tI-nA</ta>
            <ta e="T547" id="Seg_1932" s="T546">bu</ta>
            <ta e="T548" id="Seg_1933" s="T547">urut</ta>
            <ta e="T549" id="Seg_1934" s="T548">di͡e-AːččI</ta>
            <ta e="T550" id="Seg_1935" s="T549">e-TI-LArA</ta>
            <ta e="T551" id="Seg_1936" s="T550">kɨːs-LAr</ta>
            <ta e="T552" id="Seg_1937" s="T551">ɨllaː-Ar-LAr-nI</ta>
            <ta e="T553" id="Seg_1938" s="T552">u͡ol-LAr</ta>
            <ta e="T554" id="Seg_1939" s="T553">ɨllaː-Ar-LArI-n</ta>
            <ta e="T555" id="Seg_1940" s="T554">onnuk</ta>
            <ta e="T556" id="Seg_1941" s="T555">ɨrɨ͡a-LAr-nI</ta>
            <ta e="T557" id="Seg_1942" s="T556">bil-A-GIn</ta>
            <ta e="T558" id="Seg_1943" s="T557">du͡o</ta>
            <ta e="T609" id="Seg_1944" s="T608">kim</ta>
            <ta e="T610" id="Seg_1945" s="T609">ɨrɨ͡a-tA=Ij</ta>
            <ta e="T622" id="Seg_1946" s="T621">onton</ta>
            <ta e="T623" id="Seg_1947" s="T622">bu</ta>
            <ta e="T624" id="Seg_1948" s="T623">ehigi</ta>
            <ta e="T625" id="Seg_1949" s="T624">ügüs</ta>
            <ta e="T626" id="Seg_1950" s="T625">kim-nI</ta>
            <ta e="T627" id="Seg_1951" s="T626">bil-A-GIt</ta>
            <ta e="T628" id="Seg_1952" s="T627">bɨhɨːlaːk</ta>
            <ta e="T629" id="Seg_1953" s="T628">taːburun-LAr-nI</ta>
            <ta e="T630" id="Seg_1954" s="T629">da</ta>
            <ta e="T631" id="Seg_1955" s="T630">onton</ta>
            <ta e="T632" id="Seg_1956" s="T631">bu</ta>
            <ta e="T633" id="Seg_1957" s="T632">kim-LAr-nI</ta>
            <ta e="T634" id="Seg_1958" s="T633">pʼerʼexvat dɨxaːnʼija</ta>
            <ta e="T635" id="Seg_1959" s="T634">di͡e-An</ta>
            <ta e="T636" id="Seg_1960" s="T635">iti</ta>
            <ta e="T637" id="Seg_1961" s="T636">keː</ta>
            <ta e="T638" id="Seg_1962" s="T637">hɨrɨs-Ar-LAr</ta>
            <ta e="T639" id="Seg_1963" s="T638">du͡o</ta>
            <ta e="T640" id="Seg_1964" s="T639">tu͡ok</ta>
            <ta e="T641" id="Seg_1965" s="T640">du͡o</ta>
            <ta e="T717" id="Seg_1966" s="T716">bu</ta>
            <ta e="T718" id="Seg_1967" s="T717">Rɨmnaj-GA</ta>
            <ta e="T719" id="Seg_1968" s="T718">Marɨːna</ta>
            <ta e="T720" id="Seg_1969" s="T719">di͡e-An</ta>
            <ta e="T721" id="Seg_1970" s="T720">kɨːs</ta>
            <ta e="T722" id="Seg_1971" s="T721">Bʼerʼegavaːja</ta>
            <ta e="T723" id="Seg_1972" s="T722">iti</ta>
            <ta e="T724" id="Seg_1973" s="T723">ehigi</ta>
            <ta e="T725" id="Seg_1974" s="T724">kɨːs-GIt</ta>
            <ta e="T726" id="Seg_1975" s="T725">bu͡ol-BAtAK</ta>
            <ta e="T727" id="Seg_1976" s="T726">du͡o</ta>
            <ta e="T732" id="Seg_1977" s="T731">eː</ta>
            <ta e="T733" id="Seg_1978" s="T732">ol</ta>
            <ta e="T734" id="Seg_1979" s="T733">ihin</ta>
            <ta e="T735" id="Seg_1980" s="T734">oloŋko-LAr-nI</ta>
            <ta e="T736" id="Seg_1981" s="T735">bil-Ar</ta>
            <ta e="T737" id="Seg_1982" s="T736">e-BIT</ta>
            <ta e="T738" id="Seg_1983" s="T737">manna</ta>
            <ta e="T739" id="Seg_1984" s="T738">kahan</ta>
            <ta e="T740" id="Seg_1985" s="T739">ere</ta>
            <ta e="T741" id="Seg_1986" s="T740">kel-A</ta>
            <ta e="T742" id="Seg_1987" s="T741">hɨrɨt-An</ta>
            <ta e="T743" id="Seg_1988" s="T742">kepseː-BIT-tA</ta>
            <ta e="T744" id="Seg_1989" s="T743">biːr</ta>
            <ta e="T745" id="Seg_1990" s="T744">oloŋko-nI</ta>
            <ta e="T746" id="Seg_1991" s="T745">min</ta>
            <ta e="T747" id="Seg_1992" s="T746">di͡e-A-BIn</ta>
            <ta e="T748" id="Seg_1993" s="T747">kantan</ta>
            <ta e="T749" id="Seg_1994" s="T748">bil-Ar-tA</ta>
            <ta e="T750" id="Seg_1995" s="T749">bu͡ol-IAK.[tA]=Ij</ta>
            <ta e="T751" id="Seg_1996" s="T750">dʼe</ta>
            <ta e="T752" id="Seg_1997" s="T751">onton</ta>
            <ta e="T753" id="Seg_1998" s="T752">en</ta>
            <ta e="T754" id="Seg_1999" s="T753">baːr</ta>
            <ta e="T755" id="Seg_2000" s="T754">en-ttAn</ta>
            <ta e="T756" id="Seg_2001" s="T755">ihit-An</ta>
            <ta e="T757" id="Seg_2002" s="T756">oččogo</ta>
            <ta e="T758" id="Seg_2003" s="T757">bil-Ar</ta>
            <ta e="T759" id="Seg_2004" s="T758">e-BIT</ta>
            <ta e="T760" id="Seg_2005" s="T759">ol</ta>
            <ta e="T790" id="Seg_2006" s="T789">ihit-I-BAtAK-I-ŋ</ta>
            <ta e="T791" id="Seg_2007" s="T790">du͡o</ta>
            <ta e="T792" id="Seg_2008" s="T791">radʼio-GA</ta>
            <ta e="T814" id="Seg_2009" s="T813">vsʼo ravno</ta>
            <ta e="T815" id="Seg_2010" s="T814">onton</ta>
            <ta e="T816" id="Seg_2011" s="T815">dʼüːlleː-Ar-LAr</ta>
            <ta e="T817" id="Seg_2012" s="T816">bu͡o</ta>
            <ta e="T818" id="Seg_2013" s="T817">onton</ta>
            <ta e="T819" id="Seg_2014" s="T818">kajdak</ta>
            <ta e="T827" id="Seg_2015" s="T826">Jelʼena</ta>
            <ta e="T828" id="Seg_2016" s="T827">Trifanovna</ta>
            <ta e="T829" id="Seg_2017" s="T828">bu</ta>
            <ta e="T830" id="Seg_2018" s="T829">ehigi-ttAn</ta>
            <ta e="T831" id="Seg_2019" s="T830">atɨn</ta>
            <ta e="T832" id="Seg_2020" s="T831">kim</ta>
            <ta e="T833" id="Seg_2021" s="T832">eme</ta>
            <ta e="T834" id="Seg_2022" s="T833">baːr</ta>
            <ta e="T835" id="Seg_2023" s="T834">du͡o</ta>
            <ta e="T836" id="Seg_2024" s="T835">er</ta>
            <ta e="T837" id="Seg_2025" s="T836">kihi</ta>
            <ta e="T838" id="Seg_2026" s="T837">du͡o</ta>
            <ta e="T839" id="Seg_2027" s="T838">dʼaktar</ta>
            <ta e="T840" id="Seg_2028" s="T839">du͡o</ta>
            <ta e="T841" id="Seg_2029" s="T840">Rɨmnaj-GA</ta>
            <ta e="T842" id="Seg_2030" s="T841">kim</ta>
            <ta e="T843" id="Seg_2031" s="T842">bil-Ar</ta>
            <ta e="T844" id="Seg_2032" s="T843">oloŋko-nI</ta>
            <ta e="T845" id="Seg_2033" s="T844">da</ta>
            <ta e="T846" id="Seg_2034" s="T845">bɨlɨr-GI</ta>
            <ta e="T847" id="Seg_2035" s="T846">da</ta>
            <ta e="T848" id="Seg_2036" s="T847">ɨrɨ͡a-nI</ta>
            <ta e="T884" id="Seg_2037" s="T883">atɨn</ta>
            <ta e="T885" id="Seg_2038" s="T884">kihi-LAr</ta>
            <ta e="T886" id="Seg_2039" s="T885">ka</ta>
            <ta e="T919" id="Seg_2040" s="T918">bu</ta>
            <ta e="T920" id="Seg_2041" s="T919">beje-GIt</ta>
            <ta e="T921" id="Seg_2042" s="T920">Rɨmnaj-GItI-GAr</ta>
            <ta e="T922" id="Seg_2043" s="T921">kuluːb-GA</ta>
            <ta e="T923" id="Seg_2044" s="T922">ehigi</ta>
            <ta e="T924" id="Seg_2045" s="T923">bu</ta>
            <ta e="T925" id="Seg_2046" s="T924">kihi-LAr</ta>
            <ta e="T926" id="Seg_2047" s="T925">ilin-LArI-GAr</ta>
            <ta e="T927" id="Seg_2048" s="T926">vɨstupaj-LAː-An</ta>
            <ta e="T928" id="Seg_2049" s="T927">kepseː-AːččI-GIt</ta>
            <ta e="T929" id="Seg_2050" s="T928">du͡o</ta>
            <ta e="T930" id="Seg_2051" s="T929">ɨllaː-AːččI-GIt</ta>
            <ta e="T931" id="Seg_2052" s="T930">du͡o</ta>
            <ta e="T932" id="Seg_2053" s="T931">oloŋko-LAr-GItI-n</ta>
            <ta e="T981" id="Seg_2054" s="T980">ehigi</ta>
            <ta e="T982" id="Seg_2055" s="T981">ogo-LAr-GIt</ta>
            <ta e="T983" id="Seg_2056" s="T982">agaj</ta>
            <ta e="T984" id="Seg_2057" s="T983">bil-Ar-LArA</ta>
            <ta e="T985" id="Seg_2058" s="T984">bu͡ol-IAK.[tA]</ta>
            <ta e="T986" id="Seg_2059" s="T985">bu</ta>
            <ta e="T987" id="Seg_2060" s="T986">oloŋko-LAr-nI</ta>
            <ta e="T988" id="Seg_2061" s="T987">da</ta>
            <ta e="T989" id="Seg_2062" s="T988">ɨrɨ͡a-LAr-nI</ta>
            <ta e="T990" id="Seg_2063" s="T989">da</ta>
            <ta e="T1023" id="Seg_2064" s="T1022">anɨ</ta>
            <ta e="T1024" id="Seg_2065" s="T1023">tu͡ok</ta>
            <ta e="T1025" id="Seg_2066" s="T1024">eme</ta>
            <ta e="T1026" id="Seg_2067" s="T1025">oloŋko-tI-nA</ta>
            <ta e="T1027" id="Seg_2068" s="T1026">kepseː</ta>
            <ta e="T1028" id="Seg_2069" s="T1027">bu</ta>
            <ta e="T1029" id="Seg_2070" s="T1028">oloŋko</ta>
            <ta e="T1030" id="Seg_2071" s="T1029">vaabšʼe-ta</ta>
            <ta e="T1031" id="Seg_2072" s="T1030">bu</ta>
            <ta e="T1032" id="Seg_2073" s="T1031">onton</ta>
            <ta e="T1033" id="Seg_2074" s="T1032">barɨ-tA</ta>
            <ta e="T1034" id="Seg_2075" s="T1033">kepset-Ar-BIt</ta>
            <ta e="T1035" id="Seg_2076" s="T1034">bihi͡ene</ta>
            <ta e="T1036" id="Seg_2077" s="T1035">kaːl-IAK-tA</ta>
            <ta e="T1037" id="Seg_2078" s="T1036">bu͡o</ta>
            <ta e="T1038" id="Seg_2079" s="T1037">kojut</ta>
            <ta e="T1039" id="Seg_2080" s="T1038">kojut</ta>
            <ta e="T1040" id="Seg_2081" s="T1039">ogo-LAr-I-ŋ</ta>
            <ta e="T1041" id="Seg_2082" s="T1040">ihit-IAK-LArI-n</ta>
            <ta e="T1043" id="Seg_2083" s="T1042">kepseː</ta>
            <ta e="T1044" id="Seg_2084" s="T1043">tu͡ok</ta>
            <ta e="T1045" id="Seg_2085" s="T1044">eme</ta>
            <ta e="T1046" id="Seg_2086" s="T1045">oloŋko-tI-nA</ta>
            <ta e="T1047" id="Seg_2087" s="T1046">bu</ta>
            <ta e="T1048" id="Seg_2088" s="T1047">maːjdiːn</ta>
            <ta e="T1049" id="Seg_2089" s="T1048">oloŋko-ttAn</ta>
            <ta e="T1050" id="Seg_2090" s="T1049">e</ta>
            <ta e="T1051" id="Seg_2091" s="T1050">kepseː-BIT</ta>
            <ta e="T1052" id="Seg_2092" s="T1051">oloŋko-GI-ttAn</ta>
            <ta e="T1053" id="Seg_2093" s="T1052">atɨn-nA</ta>
            <ta e="T1061" id="Seg_2094" s="T1060">Jelʼena</ta>
            <ta e="T1062" id="Seg_2095" s="T1061">Trifanovna</ta>
            <ta e="T1063" id="Seg_2096" s="T1062">össü͡ö</ta>
            <ta e="T1064" id="Seg_2097" s="T1063">ehigi-nI</ta>
            <ta e="T1065" id="Seg_2098" s="T1064">kohoːn-LAr-nI</ta>
            <ta e="T1066" id="Seg_2099" s="T1065">sačʼinʼaj-LAː-A-GIn</ta>
            <ta e="T1067" id="Seg_2100" s="T1066">di͡e-A-GIn</ta>
            <ta e="T1068" id="Seg_2101" s="T1067">diː</ta>
            <ta e="T1069" id="Seg_2102" s="T1068">dʼe</ta>
            <ta e="T1070" id="Seg_2103" s="T1069">kepseː</ta>
            <ta e="T1071" id="Seg_2104" s="T1070">dʼe</ta>
            <ta e="T1072" id="Seg_2105" s="T1071">tu͡ok</ta>
            <ta e="T1073" id="Seg_2106" s="T1072">eme</ta>
            <ta e="T1074" id="Seg_2107" s="T1073">kohoːn-tI-n</ta>
         </annotation>
         <annotation name="ge" tierref="ge-AkEE">
            <ta e="T2" id="Seg_2108" s="T1">Elena.[NOM]</ta>
            <ta e="T3" id="Seg_2109" s="T2">Trifonovna.[NOM]</ta>
            <ta e="T4" id="Seg_2110" s="T3">few</ta>
            <ta e="T5" id="Seg_2111" s="T4">eh</ta>
            <ta e="T6" id="Seg_2112" s="T5">tell-IMP.2PL</ta>
            <ta e="T8" id="Seg_2113" s="T7">self-2PL.[NOM]</ta>
            <ta e="T9" id="Seg_2114" s="T8">direction-2PL-INSTR</ta>
            <ta e="T10" id="Seg_2115" s="T9">where</ta>
            <ta e="T11" id="Seg_2116" s="T10">earth-PROPR-2PL=Q</ta>
            <ta e="T12" id="Seg_2117" s="T11">where</ta>
            <ta e="T13" id="Seg_2118" s="T12">be.born-PST2-2PL</ta>
            <ta e="T14" id="Seg_2119" s="T13">parents-PL.[NOM]</ta>
            <ta e="T15" id="Seg_2120" s="T14">who-PROPR.[NOM]</ta>
            <ta e="T16" id="Seg_2121" s="T15">self-2PL.[NOM]</ta>
            <ta e="T17" id="Seg_2122" s="T16">side-3SG-INSTR</ta>
            <ta e="T19" id="Seg_2123" s="T18">parents-PL-2PL.[NOM]</ta>
            <ta e="T20" id="Seg_2124" s="T19">child-PL-2PL.[NOM]</ta>
            <ta e="T22" id="Seg_2125" s="T20">side-3PL-INSTR</ta>
            <ta e="T24" id="Seg_2126" s="T22">what-PL.[NOM]</ta>
            <ta e="T189" id="Seg_2127" s="T187">2PL.[NOM]</ta>
            <ta e="T191" id="Seg_2128" s="T189">family-2PL.[NOM]</ta>
            <ta e="T192" id="Seg_2129" s="T191">big.[NOM]</ta>
            <ta e="T193" id="Seg_2130" s="T192">very</ta>
            <ta e="T194" id="Seg_2131" s="T193">be-PST2.[3SG]</ta>
            <ta e="T195" id="Seg_2132" s="T194">this</ta>
            <ta e="T196" id="Seg_2133" s="T195">well</ta>
            <ta e="T197" id="Seg_2134" s="T196">child-PL-2PL.[NOM]</ta>
            <ta e="T198" id="Seg_2135" s="T197">child-3PL.[NOM]</ta>
            <ta e="T199" id="Seg_2136" s="T198">eh</ta>
            <ta e="T200" id="Seg_2137" s="T199">2PL-DAT/LOC</ta>
            <ta e="T201" id="Seg_2138" s="T200">be-HAB-3PL</ta>
            <ta e="T202" id="Seg_2139" s="T201">Q</ta>
            <ta e="T278" id="Seg_2140" s="T277">then</ta>
            <ta e="T279" id="Seg_2141" s="T278">child-PL-2PL-DAT/LOC</ta>
            <ta e="T280" id="Seg_2142" s="T279">also</ta>
            <ta e="T281" id="Seg_2143" s="T280">this</ta>
            <ta e="T282" id="Seg_2144" s="T281">2PL.[NOM]</ta>
            <ta e="T283" id="Seg_2145" s="T282">know-PRS-2PL</ta>
            <ta e="T284" id="Seg_2146" s="T283">eh</ta>
            <ta e="T285" id="Seg_2147" s="T284">many</ta>
            <ta e="T286" id="Seg_2148" s="T285">very</ta>
            <ta e="T287" id="Seg_2149" s="T286">tale-PL-ACC</ta>
            <ta e="T288" id="Seg_2150" s="T287">and</ta>
            <ta e="T289" id="Seg_2151" s="T288">song-PL-ACC</ta>
            <ta e="T290" id="Seg_2152" s="T289">and</ta>
            <ta e="T291" id="Seg_2153" s="T290">olden.time-PL-ACC</ta>
            <ta e="T292" id="Seg_2154" s="T291">child-PL-2PL-DAT/LOC</ta>
            <ta e="T293" id="Seg_2155" s="T292">also</ta>
            <ta e="T294" id="Seg_2156" s="T293">tell-PRS-2PL</ta>
            <ta e="T295" id="Seg_2157" s="T294">EMPH</ta>
            <ta e="T296" id="Seg_2158" s="T295">3PL-ACC</ta>
            <ta e="T298" id="Seg_2159" s="T297">get.to.know-PTCP.FUT-3PL-ACC</ta>
            <ta e="T299" id="Seg_2160" s="T298">child-PL-2PL.[NOM]</ta>
            <ta e="T349" id="Seg_2161" s="T348">self-2PL.[NOM]</ta>
            <ta e="T350" id="Seg_2162" s="T349">well</ta>
            <ta e="T351" id="Seg_2163" s="T350">this</ta>
            <ta e="T352" id="Seg_2164" s="T351">tale-PL-ACC</ta>
            <ta e="T353" id="Seg_2165" s="T352">song-PL-ACC</ta>
            <ta e="T354" id="Seg_2166" s="T353">how</ta>
            <ta e="T355" id="Seg_2167" s="T354">know-PST2-2PL=Q</ta>
            <ta e="T507" id="Seg_2168" s="T506">well</ta>
            <ta e="T508" id="Seg_2169" s="T507">tell-IMP.2PL</ta>
            <ta e="T509" id="Seg_2170" s="T508">well</ta>
            <ta e="T510" id="Seg_2171" s="T509">this</ta>
            <ta e="T511" id="Seg_2172" s="T510">how</ta>
            <ta e="T512" id="Seg_2173" s="T511">play-PRS-3PL</ta>
            <ta e="T513" id="Seg_2174" s="T512">that</ta>
            <ta e="T514" id="Seg_2175" s="T513">this</ta>
            <ta e="T515" id="Seg_2176" s="T514">now</ta>
            <ta e="T516" id="Seg_2177" s="T515">bring-PTCP.PST</ta>
            <ta e="T517" id="Seg_2178" s="T516">play-NMNZ-2PL-INSTR</ta>
            <ta e="T541" id="Seg_2179" s="T540">well</ta>
            <ta e="T542" id="Seg_2180" s="T541">sing.[IMP.2SG]</ta>
            <ta e="T543" id="Seg_2181" s="T542">well</ta>
            <ta e="T544" id="Seg_2182" s="T543">what.[NOM]</ta>
            <ta e="T545" id="Seg_2183" s="T544">INDEF</ta>
            <ta e="T546" id="Seg_2184" s="T545">song-3SG-PART</ta>
            <ta e="T547" id="Seg_2185" s="T546">this</ta>
            <ta e="T548" id="Seg_2186" s="T547">before</ta>
            <ta e="T549" id="Seg_2187" s="T548">say-PTCP.HAB</ta>
            <ta e="T550" id="Seg_2188" s="T549">be-PST1-3PL</ta>
            <ta e="T551" id="Seg_2189" s="T550">girl-PL.[NOM]</ta>
            <ta e="T552" id="Seg_2190" s="T551">sing-PTCP.PRS-PL-ACC</ta>
            <ta e="T553" id="Seg_2191" s="T552">boy-PL.[NOM]</ta>
            <ta e="T554" id="Seg_2192" s="T553">sing-PTCP.PRS-3PL-ACC</ta>
            <ta e="T555" id="Seg_2193" s="T554">such</ta>
            <ta e="T556" id="Seg_2194" s="T555">song-PL-ACC</ta>
            <ta e="T557" id="Seg_2195" s="T556">know-PRS-2SG</ta>
            <ta e="T558" id="Seg_2196" s="T557">Q</ta>
            <ta e="T609" id="Seg_2197" s="T608">who.[NOM]</ta>
            <ta e="T610" id="Seg_2198" s="T609">song-3SG.[NOM]=Q</ta>
            <ta e="T622" id="Seg_2199" s="T621">then</ta>
            <ta e="T623" id="Seg_2200" s="T622">this</ta>
            <ta e="T624" id="Seg_2201" s="T623">2PL.[NOM]</ta>
            <ta e="T625" id="Seg_2202" s="T624">many</ta>
            <ta e="T626" id="Seg_2203" s="T625">who-ACC</ta>
            <ta e="T627" id="Seg_2204" s="T626">know-PRS-2PL</ta>
            <ta e="T628" id="Seg_2205" s="T627">apparently</ta>
            <ta e="T629" id="Seg_2206" s="T628">riddle-PL-ACC</ta>
            <ta e="T630" id="Seg_2207" s="T629">and</ta>
            <ta e="T631" id="Seg_2208" s="T630">then</ta>
            <ta e="T632" id="Seg_2209" s="T631">this</ta>
            <ta e="T633" id="Seg_2210" s="T632">who-PL-ACC</ta>
            <ta e="T634" id="Seg_2211" s="T633">holding.ones.breath</ta>
            <ta e="T635" id="Seg_2212" s="T634">say-CVB.SEQ</ta>
            <ta e="T636" id="Seg_2213" s="T635">that.[NOM]</ta>
            <ta e="T637" id="Seg_2214" s="T636">EMPH</ta>
            <ta e="T638" id="Seg_2215" s="T637">compete-PRS-3PL</ta>
            <ta e="T639" id="Seg_2216" s="T638">Q</ta>
            <ta e="T640" id="Seg_2217" s="T639">what.[NOM]</ta>
            <ta e="T641" id="Seg_2218" s="T640">Q</ta>
            <ta e="T717" id="Seg_2219" s="T716">this</ta>
            <ta e="T718" id="Seg_2220" s="T717">Novorybnoe-DAT/LOC</ta>
            <ta e="T719" id="Seg_2221" s="T718">Marina</ta>
            <ta e="T720" id="Seg_2222" s="T719">say-CVB.SEQ</ta>
            <ta e="T721" id="Seg_2223" s="T720">girl.[NOM]</ta>
            <ta e="T722" id="Seg_2224" s="T721">Beregovaya</ta>
            <ta e="T723" id="Seg_2225" s="T722">that.[NOM]</ta>
            <ta e="T724" id="Seg_2226" s="T723">2PL.[NOM]</ta>
            <ta e="T725" id="Seg_2227" s="T724">daughter-2PL.[NOM]</ta>
            <ta e="T726" id="Seg_2228" s="T725">be-PST2.NEG.[3SG]</ta>
            <ta e="T727" id="Seg_2229" s="T726">Q</ta>
            <ta e="T732" id="Seg_2230" s="T731">AFFIRM</ta>
            <ta e="T733" id="Seg_2231" s="T732">that.[NOM]</ta>
            <ta e="T734" id="Seg_2232" s="T733">because.of</ta>
            <ta e="T735" id="Seg_2233" s="T734">tale-PL-ACC</ta>
            <ta e="T736" id="Seg_2234" s="T735">know-PRS.[3SG]</ta>
            <ta e="T737" id="Seg_2235" s="T736">be-PST2.[3SG]</ta>
            <ta e="T738" id="Seg_2236" s="T737">hither</ta>
            <ta e="T739" id="Seg_2237" s="T738">when</ta>
            <ta e="T740" id="Seg_2238" s="T739">INDEF</ta>
            <ta e="T741" id="Seg_2239" s="T740">come-CVB.SIM</ta>
            <ta e="T742" id="Seg_2240" s="T741">go-CVB.SEQ</ta>
            <ta e="T743" id="Seg_2241" s="T742">tell-PST2-3SG</ta>
            <ta e="T744" id="Seg_2242" s="T743">one</ta>
            <ta e="T745" id="Seg_2243" s="T744">tale-ACC</ta>
            <ta e="T746" id="Seg_2244" s="T745">1SG.[NOM]</ta>
            <ta e="T747" id="Seg_2245" s="T746">think-PRS-1SG</ta>
            <ta e="T748" id="Seg_2246" s="T747">where.from</ta>
            <ta e="T749" id="Seg_2247" s="T748">know-PTCP.PRS-3SG</ta>
            <ta e="T750" id="Seg_2248" s="T749">be-FUT.[3SG]=Q</ta>
            <ta e="T751" id="Seg_2249" s="T750">well</ta>
            <ta e="T752" id="Seg_2250" s="T751">then</ta>
            <ta e="T753" id="Seg_2251" s="T752">2SG.[NOM]</ta>
            <ta e="T754" id="Seg_2252" s="T753">there.is</ta>
            <ta e="T755" id="Seg_2253" s="T754">2SG-ABL</ta>
            <ta e="T756" id="Seg_2254" s="T755">hear-CVB.SEQ</ta>
            <ta e="T757" id="Seg_2255" s="T756">then</ta>
            <ta e="T758" id="Seg_2256" s="T757">know-PRS.[3SG]</ta>
            <ta e="T759" id="Seg_2257" s="T758">be-PST2.[3SG]</ta>
            <ta e="T760" id="Seg_2258" s="T759">that</ta>
            <ta e="T790" id="Seg_2259" s="T789">hear-EP-PST2.NEG-EP-2SG</ta>
            <ta e="T791" id="Seg_2260" s="T790">Q</ta>
            <ta e="T792" id="Seg_2261" s="T791">radio-DAT/LOC</ta>
            <ta e="T814" id="Seg_2262" s="T813">however</ta>
            <ta e="T815" id="Seg_2263" s="T814">then</ta>
            <ta e="T816" id="Seg_2264" s="T815">notice-PRS-3PL</ta>
            <ta e="T817" id="Seg_2265" s="T816">EMPH</ta>
            <ta e="T818" id="Seg_2266" s="T817">then</ta>
            <ta e="T819" id="Seg_2267" s="T818">how</ta>
            <ta e="T827" id="Seg_2268" s="T826">Elena.[NOM]</ta>
            <ta e="T828" id="Seg_2269" s="T827">Trifonovna.[NOM]</ta>
            <ta e="T829" id="Seg_2270" s="T828">this</ta>
            <ta e="T830" id="Seg_2271" s="T829">2PL-ABL</ta>
            <ta e="T831" id="Seg_2272" s="T830">different.[NOM]</ta>
            <ta e="T832" id="Seg_2273" s="T831">who.[NOM]</ta>
            <ta e="T833" id="Seg_2274" s="T832">INDEF</ta>
            <ta e="T834" id="Seg_2275" s="T833">there.is</ta>
            <ta e="T835" id="Seg_2276" s="T834">Q</ta>
            <ta e="T836" id="Seg_2277" s="T835">man.[NOM]</ta>
            <ta e="T837" id="Seg_2278" s="T836">human.being.[NOM]</ta>
            <ta e="T838" id="Seg_2279" s="T837">Q</ta>
            <ta e="T839" id="Seg_2280" s="T838">woman.[NOM]</ta>
            <ta e="T840" id="Seg_2281" s="T839">Q</ta>
            <ta e="T841" id="Seg_2282" s="T840">Novorybnoe-DAT/LOC</ta>
            <ta e="T842" id="Seg_2283" s="T841">who.[NOM]</ta>
            <ta e="T843" id="Seg_2284" s="T842">know-PRS.[3SG]</ta>
            <ta e="T844" id="Seg_2285" s="T843">tale-ACC</ta>
            <ta e="T845" id="Seg_2286" s="T844">and</ta>
            <ta e="T846" id="Seg_2287" s="T845">long.ago-ADJZ</ta>
            <ta e="T847" id="Seg_2288" s="T846">and</ta>
            <ta e="T848" id="Seg_2289" s="T847">song-ACC</ta>
            <ta e="T884" id="Seg_2290" s="T883">different.[NOM]</ta>
            <ta e="T885" id="Seg_2291" s="T884">human.being-PL.[NOM]</ta>
            <ta e="T886" id="Seg_2292" s="T885">well</ta>
            <ta e="T919" id="Seg_2293" s="T918">this</ta>
            <ta e="T920" id="Seg_2294" s="T919">self-2PL.[NOM]</ta>
            <ta e="T921" id="Seg_2295" s="T920">Novorybnoe-2PL-DAT/LOC</ta>
            <ta e="T922" id="Seg_2296" s="T921">club-DAT/LOC</ta>
            <ta e="T923" id="Seg_2297" s="T922">2PL.[NOM]</ta>
            <ta e="T924" id="Seg_2298" s="T923">this</ta>
            <ta e="T925" id="Seg_2299" s="T924">human.being-PL.[NOM]</ta>
            <ta e="T926" id="Seg_2300" s="T925">front-3PL-DAT/LOC</ta>
            <ta e="T927" id="Seg_2301" s="T926">perform-VBZ-CVB.SEQ</ta>
            <ta e="T928" id="Seg_2302" s="T927">tell-HAB-2PL</ta>
            <ta e="T929" id="Seg_2303" s="T928">Q</ta>
            <ta e="T930" id="Seg_2304" s="T929">sing-HAB-2PL</ta>
            <ta e="T931" id="Seg_2305" s="T930">Q</ta>
            <ta e="T932" id="Seg_2306" s="T931">tale-PL-2PL-ACC</ta>
            <ta e="T981" id="Seg_2307" s="T980">2PL.[NOM]</ta>
            <ta e="T982" id="Seg_2308" s="T981">child-PL-2PL.[NOM]</ta>
            <ta e="T983" id="Seg_2309" s="T982">only</ta>
            <ta e="T984" id="Seg_2310" s="T983">know-PRS-3PL</ta>
            <ta e="T985" id="Seg_2311" s="T984">be-FUT.[3SG]</ta>
            <ta e="T986" id="Seg_2312" s="T985">this</ta>
            <ta e="T987" id="Seg_2313" s="T986">tale-PL-ACC</ta>
            <ta e="T988" id="Seg_2314" s="T987">and</ta>
            <ta e="T989" id="Seg_2315" s="T988">song-PL-ACC</ta>
            <ta e="T990" id="Seg_2316" s="T989">and</ta>
            <ta e="T1023" id="Seg_2317" s="T1022">now</ta>
            <ta e="T1024" id="Seg_2318" s="T1023">what.[NOM]</ta>
            <ta e="T1025" id="Seg_2319" s="T1024">INDEF</ta>
            <ta e="T1026" id="Seg_2320" s="T1025">tale-3SG-PART</ta>
            <ta e="T1027" id="Seg_2321" s="T1026">tell.[IMP.2SG]</ta>
            <ta e="T1028" id="Seg_2322" s="T1027">this</ta>
            <ta e="T1029" id="Seg_2323" s="T1028">tale.[NOM]</ta>
            <ta e="T1030" id="Seg_2324" s="T1029">at.all-EMPH</ta>
            <ta e="T1031" id="Seg_2325" s="T1030">this</ta>
            <ta e="T1032" id="Seg_2326" s="T1031">then</ta>
            <ta e="T1033" id="Seg_2327" s="T1032">every-3SG.[NOM]</ta>
            <ta e="T1034" id="Seg_2328" s="T1033">chat-PTCP.PRS-1PL.[NOM]</ta>
            <ta e="T1035" id="Seg_2329" s="T1034">our</ta>
            <ta e="T1036" id="Seg_2330" s="T1035">stay-FUT-3SG</ta>
            <ta e="T1037" id="Seg_2331" s="T1036">EMPH</ta>
            <ta e="T1038" id="Seg_2332" s="T1037">later</ta>
            <ta e="T1039" id="Seg_2333" s="T1038">later</ta>
            <ta e="T1040" id="Seg_2334" s="T1039">child-PL-EP-2SG.[NOM]</ta>
            <ta e="T1041" id="Seg_2335" s="T1040">hear-PTCP.FUT-3PL-ACC</ta>
            <ta e="T1043" id="Seg_2336" s="T1042">tell.[IMP.2SG]</ta>
            <ta e="T1044" id="Seg_2337" s="T1043">what.[NOM]</ta>
            <ta e="T1045" id="Seg_2338" s="T1044">INDEF</ta>
            <ta e="T1046" id="Seg_2339" s="T1045">tale-3SG-PART</ta>
            <ta e="T1047" id="Seg_2340" s="T1046">this</ta>
            <ta e="T1048" id="Seg_2341" s="T1047">not.long.ago</ta>
            <ta e="T1049" id="Seg_2342" s="T1048">tale-ABL</ta>
            <ta e="T1050" id="Seg_2343" s="T1049">eh</ta>
            <ta e="T1051" id="Seg_2344" s="T1050">tell-PTCP.PST</ta>
            <ta e="T1052" id="Seg_2345" s="T1051">tale-2SG-ABL</ta>
            <ta e="T1053" id="Seg_2346" s="T1052">different-PART</ta>
            <ta e="T1061" id="Seg_2347" s="T1060">Elena.[NOM]</ta>
            <ta e="T1062" id="Seg_2348" s="T1061">Trifonovna.[NOM]</ta>
            <ta e="T1063" id="Seg_2349" s="T1062">still</ta>
            <ta e="T1064" id="Seg_2350" s="T1063">2PL-ACC</ta>
            <ta e="T1065" id="Seg_2351" s="T1064">poem-PL-ACC</ta>
            <ta e="T1066" id="Seg_2352" s="T1065">compose-VBZ-PRS-2SG</ta>
            <ta e="T1067" id="Seg_2353" s="T1066">say-PRS-2SG</ta>
            <ta e="T1068" id="Seg_2354" s="T1067">EMPH</ta>
            <ta e="T1069" id="Seg_2355" s="T1068">well</ta>
            <ta e="T1070" id="Seg_2356" s="T1069">tell.[IMP.2SG]</ta>
            <ta e="T1071" id="Seg_2357" s="T1070">well</ta>
            <ta e="T1072" id="Seg_2358" s="T1071">what.[NOM]</ta>
            <ta e="T1073" id="Seg_2359" s="T1072">INDEF</ta>
            <ta e="T1074" id="Seg_2360" s="T1073">poem-3SG-ACC</ta>
         </annotation>
         <annotation name="gg" tierref="gg-AkEE">
            <ta e="T2" id="Seg_2361" s="T1">Elena.[NOM]</ta>
            <ta e="T3" id="Seg_2362" s="T2">Trifonovna.[NOM]</ta>
            <ta e="T4" id="Seg_2363" s="T3">wenig</ta>
            <ta e="T5" id="Seg_2364" s="T4">äh</ta>
            <ta e="T6" id="Seg_2365" s="T5">erzählen-IMP.2PL</ta>
            <ta e="T8" id="Seg_2366" s="T7">selbst-2PL.[NOM]</ta>
            <ta e="T9" id="Seg_2367" s="T8">Richtung-2PL-INSTR</ta>
            <ta e="T10" id="Seg_2368" s="T9">wo</ta>
            <ta e="T11" id="Seg_2369" s="T10">Erde-PROPR-2PL=Q</ta>
            <ta e="T12" id="Seg_2370" s="T11">wo</ta>
            <ta e="T13" id="Seg_2371" s="T12">geboren.werden-PST2-2PL</ta>
            <ta e="T14" id="Seg_2372" s="T13">Eltern-PL.[NOM]</ta>
            <ta e="T15" id="Seg_2373" s="T14">wer-PROPR.[NOM]</ta>
            <ta e="T16" id="Seg_2374" s="T15">selbst-2PL.[NOM]</ta>
            <ta e="T17" id="Seg_2375" s="T16">Seite-3SG-INSTR</ta>
            <ta e="T19" id="Seg_2376" s="T18">Eltern-PL-2PL.[NOM]</ta>
            <ta e="T20" id="Seg_2377" s="T19">Kind-PL-2PL.[NOM]</ta>
            <ta e="T22" id="Seg_2378" s="T20">Seite-3PL-INSTR</ta>
            <ta e="T24" id="Seg_2379" s="T22">was-PL.[NOM]</ta>
            <ta e="T189" id="Seg_2380" s="T187">2PL.[NOM]</ta>
            <ta e="T191" id="Seg_2381" s="T189">Familie-2PL.[NOM]</ta>
            <ta e="T192" id="Seg_2382" s="T191">groß.[NOM]</ta>
            <ta e="T193" id="Seg_2383" s="T192">sehr</ta>
            <ta e="T194" id="Seg_2384" s="T193">sein-PST2.[3SG]</ta>
            <ta e="T195" id="Seg_2385" s="T194">dieses</ta>
            <ta e="T196" id="Seg_2386" s="T195">nun</ta>
            <ta e="T197" id="Seg_2387" s="T196">Kind-PL-2PL.[NOM]</ta>
            <ta e="T198" id="Seg_2388" s="T197">Kind-3PL.[NOM]</ta>
            <ta e="T199" id="Seg_2389" s="T198">äh</ta>
            <ta e="T200" id="Seg_2390" s="T199">2PL-DAT/LOC</ta>
            <ta e="T201" id="Seg_2391" s="T200">sein-HAB-3PL</ta>
            <ta e="T202" id="Seg_2392" s="T201">Q</ta>
            <ta e="T278" id="Seg_2393" s="T277">dann</ta>
            <ta e="T279" id="Seg_2394" s="T278">Kind-PL-2PL-DAT/LOC</ta>
            <ta e="T280" id="Seg_2395" s="T279">auch</ta>
            <ta e="T281" id="Seg_2396" s="T280">dieses</ta>
            <ta e="T282" id="Seg_2397" s="T281">2PL.[NOM]</ta>
            <ta e="T283" id="Seg_2398" s="T282">wissen-PRS-2PL</ta>
            <ta e="T284" id="Seg_2399" s="T283">äh</ta>
            <ta e="T285" id="Seg_2400" s="T284">viel</ta>
            <ta e="T286" id="Seg_2401" s="T285">sehr</ta>
            <ta e="T287" id="Seg_2402" s="T286">Märchen-PL-ACC</ta>
            <ta e="T288" id="Seg_2403" s="T287">und</ta>
            <ta e="T289" id="Seg_2404" s="T288">Lied-PL-ACC</ta>
            <ta e="T290" id="Seg_2405" s="T289">und</ta>
            <ta e="T291" id="Seg_2406" s="T290">alte.Zeiten-PL-ACC</ta>
            <ta e="T292" id="Seg_2407" s="T291">Kind-PL-2PL-DAT/LOC</ta>
            <ta e="T293" id="Seg_2408" s="T292">auch</ta>
            <ta e="T294" id="Seg_2409" s="T293">erzählen-PRS-2PL</ta>
            <ta e="T295" id="Seg_2410" s="T294">EMPH</ta>
            <ta e="T296" id="Seg_2411" s="T295">3PL-ACC</ta>
            <ta e="T298" id="Seg_2412" s="T297">erfahren-PTCP.FUT-3PL-ACC</ta>
            <ta e="T299" id="Seg_2413" s="T298">Kind-PL-2PL.[NOM]</ta>
            <ta e="T349" id="Seg_2414" s="T348">selbst-2PL.[NOM]</ta>
            <ta e="T350" id="Seg_2415" s="T349">nun</ta>
            <ta e="T351" id="Seg_2416" s="T350">dieses</ta>
            <ta e="T352" id="Seg_2417" s="T351">Märchen-PL-ACC</ta>
            <ta e="T353" id="Seg_2418" s="T352">Lied-PL-ACC</ta>
            <ta e="T354" id="Seg_2419" s="T353">wie</ta>
            <ta e="T355" id="Seg_2420" s="T354">wissen-PST2-2PL=Q</ta>
            <ta e="T507" id="Seg_2421" s="T506">doch</ta>
            <ta e="T508" id="Seg_2422" s="T507">erzählen-IMP.2PL</ta>
            <ta e="T509" id="Seg_2423" s="T508">doch</ta>
            <ta e="T510" id="Seg_2424" s="T509">dieses</ta>
            <ta e="T511" id="Seg_2425" s="T510">wie</ta>
            <ta e="T512" id="Seg_2426" s="T511">spielen-PRS-3PL</ta>
            <ta e="T513" id="Seg_2427" s="T512">jenes</ta>
            <ta e="T514" id="Seg_2428" s="T513">dieses</ta>
            <ta e="T515" id="Seg_2429" s="T514">jetzt</ta>
            <ta e="T516" id="Seg_2430" s="T515">bringen-PTCP.PST</ta>
            <ta e="T517" id="Seg_2431" s="T516">spielen-NMNZ-2PL-INSTR</ta>
            <ta e="T541" id="Seg_2432" s="T540">doch</ta>
            <ta e="T542" id="Seg_2433" s="T541">singen.[IMP.2SG]</ta>
            <ta e="T543" id="Seg_2434" s="T542">doch</ta>
            <ta e="T544" id="Seg_2435" s="T543">was.[NOM]</ta>
            <ta e="T545" id="Seg_2436" s="T544">INDEF</ta>
            <ta e="T546" id="Seg_2437" s="T545">Lied-3SG-PART</ta>
            <ta e="T547" id="Seg_2438" s="T546">dieses</ta>
            <ta e="T548" id="Seg_2439" s="T547">früher</ta>
            <ta e="T549" id="Seg_2440" s="T548">sagen-PTCP.HAB</ta>
            <ta e="T550" id="Seg_2441" s="T549">sein-PST1-3PL</ta>
            <ta e="T551" id="Seg_2442" s="T550">Mädchen-PL.[NOM]</ta>
            <ta e="T552" id="Seg_2443" s="T551">singen-PTCP.PRS-PL-ACC</ta>
            <ta e="T553" id="Seg_2444" s="T552">Junge-PL.[NOM]</ta>
            <ta e="T554" id="Seg_2445" s="T553">singen-PTCP.PRS-3PL-ACC</ta>
            <ta e="T555" id="Seg_2446" s="T554">solch</ta>
            <ta e="T556" id="Seg_2447" s="T555">Lied-PL-ACC</ta>
            <ta e="T557" id="Seg_2448" s="T556">wissen-PRS-2SG</ta>
            <ta e="T558" id="Seg_2449" s="T557">Q</ta>
            <ta e="T609" id="Seg_2450" s="T608">wer.[NOM]</ta>
            <ta e="T610" id="Seg_2451" s="T609">Lied-3SG.[NOM]=Q</ta>
            <ta e="T622" id="Seg_2452" s="T621">dann</ta>
            <ta e="T623" id="Seg_2453" s="T622">dieses</ta>
            <ta e="T624" id="Seg_2454" s="T623">2PL.[NOM]</ta>
            <ta e="T625" id="Seg_2455" s="T624">viel</ta>
            <ta e="T626" id="Seg_2456" s="T625">wer-ACC</ta>
            <ta e="T627" id="Seg_2457" s="T626">wissen-PRS-2PL</ta>
            <ta e="T628" id="Seg_2458" s="T627">offenbar</ta>
            <ta e="T629" id="Seg_2459" s="T628">Rätsel-PL-ACC</ta>
            <ta e="T630" id="Seg_2460" s="T629">und</ta>
            <ta e="T631" id="Seg_2461" s="T630">dann</ta>
            <ta e="T632" id="Seg_2462" s="T631">dieses</ta>
            <ta e="T633" id="Seg_2463" s="T632">wer-PL-ACC</ta>
            <ta e="T634" id="Seg_2464" s="T633">Luft.anhalten</ta>
            <ta e="T635" id="Seg_2465" s="T634">sagen-CVB.SEQ</ta>
            <ta e="T636" id="Seg_2466" s="T635">dieses.[NOM]</ta>
            <ta e="T637" id="Seg_2467" s="T636">EMPH</ta>
            <ta e="T638" id="Seg_2468" s="T637">sich.messen-PRS-3PL</ta>
            <ta e="T639" id="Seg_2469" s="T638">Q</ta>
            <ta e="T640" id="Seg_2470" s="T639">was.[NOM]</ta>
            <ta e="T641" id="Seg_2471" s="T640">Q</ta>
            <ta e="T717" id="Seg_2472" s="T716">dieses</ta>
            <ta e="T718" id="Seg_2473" s="T717">Novorybnoe-DAT/LOC</ta>
            <ta e="T719" id="Seg_2474" s="T718">Marina</ta>
            <ta e="T720" id="Seg_2475" s="T719">sagen-CVB.SEQ</ta>
            <ta e="T721" id="Seg_2476" s="T720">Mädchen.[NOM]</ta>
            <ta e="T722" id="Seg_2477" s="T721">Beregovaja</ta>
            <ta e="T723" id="Seg_2478" s="T722">dieses.[NOM]</ta>
            <ta e="T724" id="Seg_2479" s="T723">2PL.[NOM]</ta>
            <ta e="T725" id="Seg_2480" s="T724">Tochter-2PL.[NOM]</ta>
            <ta e="T726" id="Seg_2481" s="T725">sein-PST2.NEG.[3SG]</ta>
            <ta e="T727" id="Seg_2482" s="T726">Q</ta>
            <ta e="T732" id="Seg_2483" s="T731">AFFIRM</ta>
            <ta e="T733" id="Seg_2484" s="T732">jenes.[NOM]</ta>
            <ta e="T734" id="Seg_2485" s="T733">wegen</ta>
            <ta e="T735" id="Seg_2486" s="T734">Märchen-PL-ACC</ta>
            <ta e="T736" id="Seg_2487" s="T735">wissen-PRS.[3SG]</ta>
            <ta e="T737" id="Seg_2488" s="T736">sein-PST2.[3SG]</ta>
            <ta e="T738" id="Seg_2489" s="T737">hierher</ta>
            <ta e="T739" id="Seg_2490" s="T738">wann</ta>
            <ta e="T740" id="Seg_2491" s="T739">INDEF</ta>
            <ta e="T741" id="Seg_2492" s="T740">kommen-CVB.SIM</ta>
            <ta e="T742" id="Seg_2493" s="T741">gehen-CVB.SEQ</ta>
            <ta e="T743" id="Seg_2494" s="T742">erzählen-PST2-3SG</ta>
            <ta e="T744" id="Seg_2495" s="T743">eins</ta>
            <ta e="T745" id="Seg_2496" s="T744">Märchen-ACC</ta>
            <ta e="T746" id="Seg_2497" s="T745">1SG.[NOM]</ta>
            <ta e="T747" id="Seg_2498" s="T746">denken-PRS-1SG</ta>
            <ta e="T748" id="Seg_2499" s="T747">woher</ta>
            <ta e="T749" id="Seg_2500" s="T748">wissen-PTCP.PRS-3SG</ta>
            <ta e="T750" id="Seg_2501" s="T749">sein-FUT.[3SG]=Q</ta>
            <ta e="T751" id="Seg_2502" s="T750">doch</ta>
            <ta e="T752" id="Seg_2503" s="T751">dann</ta>
            <ta e="T753" id="Seg_2504" s="T752">2SG.[NOM]</ta>
            <ta e="T754" id="Seg_2505" s="T753">es.gibt</ta>
            <ta e="T755" id="Seg_2506" s="T754">2SG-ABL</ta>
            <ta e="T756" id="Seg_2507" s="T755">hören-CVB.SEQ</ta>
            <ta e="T757" id="Seg_2508" s="T756">dann</ta>
            <ta e="T758" id="Seg_2509" s="T757">wissen-PRS.[3SG]</ta>
            <ta e="T759" id="Seg_2510" s="T758">sein-PST2.[3SG]</ta>
            <ta e="T760" id="Seg_2511" s="T759">jenes</ta>
            <ta e="T790" id="Seg_2512" s="T789">hören-EP-PST2.NEG-EP-2SG</ta>
            <ta e="T791" id="Seg_2513" s="T790">Q</ta>
            <ta e="T792" id="Seg_2514" s="T791">radio-DAT/LOC</ta>
            <ta e="T814" id="Seg_2515" s="T813">gleichwohl</ta>
            <ta e="T815" id="Seg_2516" s="T814">dann</ta>
            <ta e="T816" id="Seg_2517" s="T815">bemerken-PRS-3PL</ta>
            <ta e="T817" id="Seg_2518" s="T816">EMPH</ta>
            <ta e="T818" id="Seg_2519" s="T817">dann</ta>
            <ta e="T819" id="Seg_2520" s="T818">wie</ta>
            <ta e="T827" id="Seg_2521" s="T826">Elena.[NOM]</ta>
            <ta e="T828" id="Seg_2522" s="T827">Trifonovna.[NOM]</ta>
            <ta e="T829" id="Seg_2523" s="T828">dieses</ta>
            <ta e="T830" id="Seg_2524" s="T829">2PL-ABL</ta>
            <ta e="T831" id="Seg_2525" s="T830">anders.[NOM]</ta>
            <ta e="T832" id="Seg_2526" s="T831">wer.[NOM]</ta>
            <ta e="T833" id="Seg_2527" s="T832">INDEF</ta>
            <ta e="T834" id="Seg_2528" s="T833">es.gibt</ta>
            <ta e="T835" id="Seg_2529" s="T834">Q</ta>
            <ta e="T836" id="Seg_2530" s="T835">Mann.[NOM]</ta>
            <ta e="T837" id="Seg_2531" s="T836">Mensch.[NOM]</ta>
            <ta e="T838" id="Seg_2532" s="T837">Q</ta>
            <ta e="T839" id="Seg_2533" s="T838">Frau.[NOM]</ta>
            <ta e="T840" id="Seg_2534" s="T839">Q</ta>
            <ta e="T841" id="Seg_2535" s="T840">Novorybnoe-DAT/LOC</ta>
            <ta e="T842" id="Seg_2536" s="T841">wer.[NOM]</ta>
            <ta e="T843" id="Seg_2537" s="T842">wissen-PRS.[3SG]</ta>
            <ta e="T844" id="Seg_2538" s="T843">Märchen-ACC</ta>
            <ta e="T845" id="Seg_2539" s="T844">und</ta>
            <ta e="T846" id="Seg_2540" s="T845">vor.langer.Zeit-ADJZ</ta>
            <ta e="T847" id="Seg_2541" s="T846">und</ta>
            <ta e="T848" id="Seg_2542" s="T847">Lied-ACC</ta>
            <ta e="T884" id="Seg_2543" s="T883">anders.[NOM]</ta>
            <ta e="T885" id="Seg_2544" s="T884">Mensch-PL.[NOM]</ta>
            <ta e="T886" id="Seg_2545" s="T885">nun</ta>
            <ta e="T919" id="Seg_2546" s="T918">dieses</ta>
            <ta e="T920" id="Seg_2547" s="T919">selbst-2PL.[NOM]</ta>
            <ta e="T921" id="Seg_2548" s="T920">Novorybnoe-2PL-DAT/LOC</ta>
            <ta e="T922" id="Seg_2549" s="T921">Klub-DAT/LOC</ta>
            <ta e="T923" id="Seg_2550" s="T922">2PL.[NOM]</ta>
            <ta e="T924" id="Seg_2551" s="T923">dieses</ta>
            <ta e="T925" id="Seg_2552" s="T924">Mensch-PL.[NOM]</ta>
            <ta e="T926" id="Seg_2553" s="T925">Vorderteil-3PL-DAT/LOC</ta>
            <ta e="T927" id="Seg_2554" s="T926">auftreten-VBZ-CVB.SEQ</ta>
            <ta e="T928" id="Seg_2555" s="T927">erzählen-HAB-2PL</ta>
            <ta e="T929" id="Seg_2556" s="T928">Q</ta>
            <ta e="T930" id="Seg_2557" s="T929">singen-HAB-2PL</ta>
            <ta e="T931" id="Seg_2558" s="T930">Q</ta>
            <ta e="T932" id="Seg_2559" s="T931">Märchen-PL-2PL-ACC</ta>
            <ta e="T981" id="Seg_2560" s="T980">2PL.[NOM]</ta>
            <ta e="T982" id="Seg_2561" s="T981">Kind-PL-2PL.[NOM]</ta>
            <ta e="T983" id="Seg_2562" s="T982">nur</ta>
            <ta e="T984" id="Seg_2563" s="T983">wissen-PRS-3PL</ta>
            <ta e="T985" id="Seg_2564" s="T984">sein-FUT.[3SG]</ta>
            <ta e="T986" id="Seg_2565" s="T985">dieses</ta>
            <ta e="T987" id="Seg_2566" s="T986">Märchen-PL-ACC</ta>
            <ta e="T988" id="Seg_2567" s="T987">und</ta>
            <ta e="T989" id="Seg_2568" s="T988">Lied-PL-ACC</ta>
            <ta e="T990" id="Seg_2569" s="T989">und</ta>
            <ta e="T1023" id="Seg_2570" s="T1022">jetzt</ta>
            <ta e="T1024" id="Seg_2571" s="T1023">was.[NOM]</ta>
            <ta e="T1025" id="Seg_2572" s="T1024">INDEF</ta>
            <ta e="T1026" id="Seg_2573" s="T1025">Märchen-3SG-PART</ta>
            <ta e="T1027" id="Seg_2574" s="T1026">erzählen.[IMP.2SG]</ta>
            <ta e="T1028" id="Seg_2575" s="T1027">dieses</ta>
            <ta e="T1029" id="Seg_2576" s="T1028">Märchen.[NOM]</ta>
            <ta e="T1030" id="Seg_2577" s="T1029">überhaupt-EMPH</ta>
            <ta e="T1031" id="Seg_2578" s="T1030">dieses</ta>
            <ta e="T1032" id="Seg_2579" s="T1031">dann</ta>
            <ta e="T1033" id="Seg_2580" s="T1032">jeder-3SG.[NOM]</ta>
            <ta e="T1034" id="Seg_2581" s="T1033">sich.unterhalten-PTCP.PRS-1PL.[NOM]</ta>
            <ta e="T1035" id="Seg_2582" s="T1034">unser</ta>
            <ta e="T1036" id="Seg_2583" s="T1035">bleiben-FUT-3SG</ta>
            <ta e="T1037" id="Seg_2584" s="T1036">EMPH</ta>
            <ta e="T1038" id="Seg_2585" s="T1037">später</ta>
            <ta e="T1039" id="Seg_2586" s="T1038">später</ta>
            <ta e="T1040" id="Seg_2587" s="T1039">Kind-PL-EP-2SG.[NOM]</ta>
            <ta e="T1041" id="Seg_2588" s="T1040">hören-PTCP.FUT-3PL-ACC</ta>
            <ta e="T1043" id="Seg_2589" s="T1042">erzählen.[IMP.2SG]</ta>
            <ta e="T1044" id="Seg_2590" s="T1043">was.[NOM]</ta>
            <ta e="T1045" id="Seg_2591" s="T1044">INDEF</ta>
            <ta e="T1046" id="Seg_2592" s="T1045">Märchen-3SG-PART</ta>
            <ta e="T1047" id="Seg_2593" s="T1046">dieses</ta>
            <ta e="T1048" id="Seg_2594" s="T1047">vor.Kurzem</ta>
            <ta e="T1049" id="Seg_2595" s="T1048">Märchen-ABL</ta>
            <ta e="T1050" id="Seg_2596" s="T1049">äh</ta>
            <ta e="T1051" id="Seg_2597" s="T1050">erzählen-PTCP.PST</ta>
            <ta e="T1052" id="Seg_2598" s="T1051">Märchen-2SG-ABL</ta>
            <ta e="T1053" id="Seg_2599" s="T1052">anders-PART</ta>
            <ta e="T1061" id="Seg_2600" s="T1060">Elena.[NOM]</ta>
            <ta e="T1062" id="Seg_2601" s="T1061">Trifonovna.[NOM]</ta>
            <ta e="T1063" id="Seg_2602" s="T1062">noch</ta>
            <ta e="T1064" id="Seg_2603" s="T1063">2PL-ACC</ta>
            <ta e="T1065" id="Seg_2604" s="T1064">Gedicht-PL-ACC</ta>
            <ta e="T1066" id="Seg_2605" s="T1065">verfassen-VBZ-PRS-2SG</ta>
            <ta e="T1067" id="Seg_2606" s="T1066">sagen-PRS-2SG</ta>
            <ta e="T1068" id="Seg_2607" s="T1067">EMPH</ta>
            <ta e="T1069" id="Seg_2608" s="T1068">doch</ta>
            <ta e="T1070" id="Seg_2609" s="T1069">erzählen.[IMP.2SG]</ta>
            <ta e="T1071" id="Seg_2610" s="T1070">doch</ta>
            <ta e="T1072" id="Seg_2611" s="T1071">was.[NOM]</ta>
            <ta e="T1073" id="Seg_2612" s="T1072">INDEF</ta>
            <ta e="T1074" id="Seg_2613" s="T1073">Gedicht-3SG-ACC</ta>
         </annotation>
         <annotation name="gr" tierref="gr-AkEE">
            <ta e="T2" id="Seg_2614" s="T1">Елена.[NOM]</ta>
            <ta e="T3" id="Seg_2615" s="T2">Трифоновна.[NOM]</ta>
            <ta e="T4" id="Seg_2616" s="T3">мало</ta>
            <ta e="T5" id="Seg_2617" s="T4">ээ</ta>
            <ta e="T6" id="Seg_2618" s="T5">рассказывать-IMP.2PL</ta>
            <ta e="T8" id="Seg_2619" s="T7">сам-2PL.[NOM]</ta>
            <ta e="T9" id="Seg_2620" s="T8">направление-2PL-INSTR</ta>
            <ta e="T10" id="Seg_2621" s="T9">где</ta>
            <ta e="T11" id="Seg_2622" s="T10">земля-PROPR-2PL=Q</ta>
            <ta e="T12" id="Seg_2623" s="T11">где</ta>
            <ta e="T13" id="Seg_2624" s="T12">родиться-PST2-2PL</ta>
            <ta e="T14" id="Seg_2625" s="T13">родители-PL.[NOM]</ta>
            <ta e="T15" id="Seg_2626" s="T14">кто-PROPR.[NOM]</ta>
            <ta e="T16" id="Seg_2627" s="T15">сам-2PL.[NOM]</ta>
            <ta e="T17" id="Seg_2628" s="T16">сторона-3SG-INSTR</ta>
            <ta e="T19" id="Seg_2629" s="T18">родители-PL-2PL.[NOM]</ta>
            <ta e="T20" id="Seg_2630" s="T19">ребенок-PL-2PL.[NOM]</ta>
            <ta e="T22" id="Seg_2631" s="T20">сторона-3PL-INSTR</ta>
            <ta e="T24" id="Seg_2632" s="T22">что-PL.[NOM]</ta>
            <ta e="T189" id="Seg_2633" s="T187">2PL.[NOM]</ta>
            <ta e="T191" id="Seg_2634" s="T189">семья-2PL.[NOM]</ta>
            <ta e="T192" id="Seg_2635" s="T191">большой.[NOM]</ta>
            <ta e="T193" id="Seg_2636" s="T192">очень</ta>
            <ta e="T194" id="Seg_2637" s="T193">быть-PST2.[3SG]</ta>
            <ta e="T195" id="Seg_2638" s="T194">этот</ta>
            <ta e="T196" id="Seg_2639" s="T195">вот</ta>
            <ta e="T197" id="Seg_2640" s="T196">ребенок-PL-2PL.[NOM]</ta>
            <ta e="T198" id="Seg_2641" s="T197">ребенок-3PL.[NOM]</ta>
            <ta e="T199" id="Seg_2642" s="T198">ээ</ta>
            <ta e="T200" id="Seg_2643" s="T199">2PL-DAT/LOC</ta>
            <ta e="T201" id="Seg_2644" s="T200">быть-HAB-3PL</ta>
            <ta e="T202" id="Seg_2645" s="T201">Q</ta>
            <ta e="T278" id="Seg_2646" s="T277">тогда</ta>
            <ta e="T279" id="Seg_2647" s="T278">ребенок-PL-2PL-DAT/LOC</ta>
            <ta e="T280" id="Seg_2648" s="T279">тоже</ta>
            <ta e="T281" id="Seg_2649" s="T280">этот</ta>
            <ta e="T282" id="Seg_2650" s="T281">2PL.[NOM]</ta>
            <ta e="T283" id="Seg_2651" s="T282">знать-PRS-2PL</ta>
            <ta e="T284" id="Seg_2652" s="T283">ээ</ta>
            <ta e="T285" id="Seg_2653" s="T284">много</ta>
            <ta e="T286" id="Seg_2654" s="T285">очень</ta>
            <ta e="T287" id="Seg_2655" s="T286">сказка-PL-ACC</ta>
            <ta e="T288" id="Seg_2656" s="T287">да</ta>
            <ta e="T289" id="Seg_2657" s="T288">песня-PL-ACC</ta>
            <ta e="T290" id="Seg_2658" s="T289">да</ta>
            <ta e="T291" id="Seg_2659" s="T290">старина-PL-ACC</ta>
            <ta e="T292" id="Seg_2660" s="T291">ребенок-PL-2PL-DAT/LOC</ta>
            <ta e="T293" id="Seg_2661" s="T292">тоже</ta>
            <ta e="T294" id="Seg_2662" s="T293">рассказывать-PRS-2PL</ta>
            <ta e="T295" id="Seg_2663" s="T294">EMPH</ta>
            <ta e="T296" id="Seg_2664" s="T295">3PL-ACC</ta>
            <ta e="T298" id="Seg_2665" s="T297">узнавать-PTCP.FUT-3PL-ACC</ta>
            <ta e="T299" id="Seg_2666" s="T298">ребенок-PL-2PL.[NOM]</ta>
            <ta e="T349" id="Seg_2667" s="T348">сам-2PL.[NOM]</ta>
            <ta e="T350" id="Seg_2668" s="T349">вот</ta>
            <ta e="T351" id="Seg_2669" s="T350">этот</ta>
            <ta e="T352" id="Seg_2670" s="T351">сказка-PL-ACC</ta>
            <ta e="T353" id="Seg_2671" s="T352">песня-PL-ACC</ta>
            <ta e="T354" id="Seg_2672" s="T353">как</ta>
            <ta e="T355" id="Seg_2673" s="T354">знать-PST2-2PL=Q</ta>
            <ta e="T507" id="Seg_2674" s="T506">вот</ta>
            <ta e="T508" id="Seg_2675" s="T507">рассказывать-IMP.2PL</ta>
            <ta e="T509" id="Seg_2676" s="T508">вот</ta>
            <ta e="T510" id="Seg_2677" s="T509">этот</ta>
            <ta e="T511" id="Seg_2678" s="T510">как</ta>
            <ta e="T512" id="Seg_2679" s="T511">играть-PRS-3PL</ta>
            <ta e="T513" id="Seg_2680" s="T512">тот</ta>
            <ta e="T514" id="Seg_2681" s="T513">этот</ta>
            <ta e="T515" id="Seg_2682" s="T514">теперь</ta>
            <ta e="T516" id="Seg_2683" s="T515">принести-PTCP.PST</ta>
            <ta e="T517" id="Seg_2684" s="T516">играть-NMNZ-2PL-INSTR</ta>
            <ta e="T541" id="Seg_2685" s="T540">вот</ta>
            <ta e="T542" id="Seg_2686" s="T541">петь.[IMP.2SG]</ta>
            <ta e="T543" id="Seg_2687" s="T542">вот</ta>
            <ta e="T544" id="Seg_2688" s="T543">что.[NOM]</ta>
            <ta e="T545" id="Seg_2689" s="T544">INDEF</ta>
            <ta e="T546" id="Seg_2690" s="T545">песня-3SG-PART</ta>
            <ta e="T547" id="Seg_2691" s="T546">этот</ta>
            <ta e="T548" id="Seg_2692" s="T547">раньше</ta>
            <ta e="T549" id="Seg_2693" s="T548">говорить-PTCP.HAB</ta>
            <ta e="T550" id="Seg_2694" s="T549">быть-PST1-3PL</ta>
            <ta e="T551" id="Seg_2695" s="T550">девушка-PL.[NOM]</ta>
            <ta e="T552" id="Seg_2696" s="T551">петь-PTCP.PRS-PL-ACC</ta>
            <ta e="T553" id="Seg_2697" s="T552">мальчик-PL.[NOM]</ta>
            <ta e="T554" id="Seg_2698" s="T553">петь-PTCP.PRS-3PL-ACC</ta>
            <ta e="T555" id="Seg_2699" s="T554">такой</ta>
            <ta e="T556" id="Seg_2700" s="T555">песня-PL-ACC</ta>
            <ta e="T557" id="Seg_2701" s="T556">знать-PRS-2SG</ta>
            <ta e="T558" id="Seg_2702" s="T557">Q</ta>
            <ta e="T609" id="Seg_2703" s="T608">кто.[NOM]</ta>
            <ta e="T610" id="Seg_2704" s="T609">песня-3SG.[NOM]=Q</ta>
            <ta e="T622" id="Seg_2705" s="T621">потом</ta>
            <ta e="T623" id="Seg_2706" s="T622">этот</ta>
            <ta e="T624" id="Seg_2707" s="T623">2PL.[NOM]</ta>
            <ta e="T625" id="Seg_2708" s="T624">много</ta>
            <ta e="T626" id="Seg_2709" s="T625">кто-ACC</ta>
            <ta e="T627" id="Seg_2710" s="T626">знать-PRS-2PL</ta>
            <ta e="T628" id="Seg_2711" s="T627">наверное</ta>
            <ta e="T629" id="Seg_2712" s="T628">загадка-PL-ACC</ta>
            <ta e="T630" id="Seg_2713" s="T629">да</ta>
            <ta e="T631" id="Seg_2714" s="T630">потом</ta>
            <ta e="T632" id="Seg_2715" s="T631">этот</ta>
            <ta e="T633" id="Seg_2716" s="T632">кто-PL-ACC</ta>
            <ta e="T634" id="Seg_2717" s="T633">перехват.дыхания</ta>
            <ta e="T635" id="Seg_2718" s="T634">говорить-CVB.SEQ</ta>
            <ta e="T636" id="Seg_2719" s="T635">тот.[NOM]</ta>
            <ta e="T637" id="Seg_2720" s="T636">EMPH</ta>
            <ta e="T638" id="Seg_2721" s="T637">соревновать-PRS-3PL</ta>
            <ta e="T639" id="Seg_2722" s="T638">Q</ta>
            <ta e="T640" id="Seg_2723" s="T639">что.[NOM]</ta>
            <ta e="T641" id="Seg_2724" s="T640">Q</ta>
            <ta e="T717" id="Seg_2725" s="T716">этот</ta>
            <ta e="T718" id="Seg_2726" s="T717">Новорыбное-DAT/LOC</ta>
            <ta e="T719" id="Seg_2727" s="T718">Марина</ta>
            <ta e="T720" id="Seg_2728" s="T719">говорить-CVB.SEQ</ta>
            <ta e="T721" id="Seg_2729" s="T720">девушка.[NOM]</ta>
            <ta e="T722" id="Seg_2730" s="T721">Береговая</ta>
            <ta e="T723" id="Seg_2731" s="T722">тот.[NOM]</ta>
            <ta e="T724" id="Seg_2732" s="T723">2PL.[NOM]</ta>
            <ta e="T725" id="Seg_2733" s="T724">дочь-2PL.[NOM]</ta>
            <ta e="T726" id="Seg_2734" s="T725">быть-PST2.NEG.[3SG]</ta>
            <ta e="T727" id="Seg_2735" s="T726">Q</ta>
            <ta e="T732" id="Seg_2736" s="T731">AFFIRM</ta>
            <ta e="T733" id="Seg_2737" s="T732">тот.[NOM]</ta>
            <ta e="T734" id="Seg_2738" s="T733">из_за</ta>
            <ta e="T735" id="Seg_2739" s="T734">сказка-PL-ACC</ta>
            <ta e="T736" id="Seg_2740" s="T735">знать-PRS.[3SG]</ta>
            <ta e="T737" id="Seg_2741" s="T736">быть-PST2.[3SG]</ta>
            <ta e="T738" id="Seg_2742" s="T737">сюда</ta>
            <ta e="T739" id="Seg_2743" s="T738">когда</ta>
            <ta e="T740" id="Seg_2744" s="T739">INDEF</ta>
            <ta e="T741" id="Seg_2745" s="T740">приходить-CVB.SIM</ta>
            <ta e="T742" id="Seg_2746" s="T741">идти-CVB.SEQ</ta>
            <ta e="T743" id="Seg_2747" s="T742">рассказывать-PST2-3SG</ta>
            <ta e="T744" id="Seg_2748" s="T743">один</ta>
            <ta e="T745" id="Seg_2749" s="T744">сказка-ACC</ta>
            <ta e="T746" id="Seg_2750" s="T745">1SG.[NOM]</ta>
            <ta e="T747" id="Seg_2751" s="T746">думать-PRS-1SG</ta>
            <ta e="T748" id="Seg_2752" s="T747">откуда</ta>
            <ta e="T749" id="Seg_2753" s="T748">знать-PTCP.PRS-3SG</ta>
            <ta e="T750" id="Seg_2754" s="T749">быть-FUT.[3SG]=Q</ta>
            <ta e="T751" id="Seg_2755" s="T750">вот</ta>
            <ta e="T752" id="Seg_2756" s="T751">потом</ta>
            <ta e="T753" id="Seg_2757" s="T752">2SG.[NOM]</ta>
            <ta e="T754" id="Seg_2758" s="T753">есть</ta>
            <ta e="T755" id="Seg_2759" s="T754">2SG-ABL</ta>
            <ta e="T756" id="Seg_2760" s="T755">слышать-CVB.SEQ</ta>
            <ta e="T757" id="Seg_2761" s="T756">тогда</ta>
            <ta e="T758" id="Seg_2762" s="T757">знать-PRS.[3SG]</ta>
            <ta e="T759" id="Seg_2763" s="T758">быть-PST2.[3SG]</ta>
            <ta e="T760" id="Seg_2764" s="T759">тот</ta>
            <ta e="T790" id="Seg_2765" s="T789">слышать-EP-PST2.NEG-EP-2SG</ta>
            <ta e="T791" id="Seg_2766" s="T790">Q</ta>
            <ta e="T792" id="Seg_2767" s="T791">радио-DAT/LOC</ta>
            <ta e="T814" id="Seg_2768" s="T813">все.равно</ta>
            <ta e="T815" id="Seg_2769" s="T814">потом</ta>
            <ta e="T816" id="Seg_2770" s="T815">замечать-PRS-3PL</ta>
            <ta e="T817" id="Seg_2771" s="T816">EMPH</ta>
            <ta e="T818" id="Seg_2772" s="T817">потом</ta>
            <ta e="T819" id="Seg_2773" s="T818">как</ta>
            <ta e="T827" id="Seg_2774" s="T826">Елена.[NOM]</ta>
            <ta e="T828" id="Seg_2775" s="T827">Трифоновна.[NOM]</ta>
            <ta e="T829" id="Seg_2776" s="T828">этот</ta>
            <ta e="T830" id="Seg_2777" s="T829">2PL-ABL</ta>
            <ta e="T831" id="Seg_2778" s="T830">другой.[NOM]</ta>
            <ta e="T832" id="Seg_2779" s="T831">кто.[NOM]</ta>
            <ta e="T833" id="Seg_2780" s="T832">INDEF</ta>
            <ta e="T834" id="Seg_2781" s="T833">есть</ta>
            <ta e="T835" id="Seg_2782" s="T834">Q</ta>
            <ta e="T836" id="Seg_2783" s="T835">мужчина.[NOM]</ta>
            <ta e="T837" id="Seg_2784" s="T836">человек.[NOM]</ta>
            <ta e="T838" id="Seg_2785" s="T837">Q</ta>
            <ta e="T839" id="Seg_2786" s="T838">жена.[NOM]</ta>
            <ta e="T840" id="Seg_2787" s="T839">Q</ta>
            <ta e="T841" id="Seg_2788" s="T840">Новорыбное-DAT/LOC</ta>
            <ta e="T842" id="Seg_2789" s="T841">кто.[NOM]</ta>
            <ta e="T843" id="Seg_2790" s="T842">знать-PRS.[3SG]</ta>
            <ta e="T844" id="Seg_2791" s="T843">сказка-ACC</ta>
            <ta e="T845" id="Seg_2792" s="T844">да</ta>
            <ta e="T846" id="Seg_2793" s="T845">давно-ADJZ</ta>
            <ta e="T847" id="Seg_2794" s="T846">да</ta>
            <ta e="T848" id="Seg_2795" s="T847">песня-ACC</ta>
            <ta e="T884" id="Seg_2796" s="T883">другой.[NOM]</ta>
            <ta e="T885" id="Seg_2797" s="T884">человек-PL.[NOM]</ta>
            <ta e="T886" id="Seg_2798" s="T885">вот</ta>
            <ta e="T919" id="Seg_2799" s="T918">этот</ta>
            <ta e="T920" id="Seg_2800" s="T919">сам-2PL.[NOM]</ta>
            <ta e="T921" id="Seg_2801" s="T920">Новорыбное-2PL-DAT/LOC</ta>
            <ta e="T922" id="Seg_2802" s="T921">клуб-DAT/LOC</ta>
            <ta e="T923" id="Seg_2803" s="T922">2PL.[NOM]</ta>
            <ta e="T924" id="Seg_2804" s="T923">этот</ta>
            <ta e="T925" id="Seg_2805" s="T924">человек-PL.[NOM]</ta>
            <ta e="T926" id="Seg_2806" s="T925">передняя.часть-3PL-DAT/LOC</ta>
            <ta e="T927" id="Seg_2807" s="T926">выступать-VBZ-CVB.SEQ</ta>
            <ta e="T928" id="Seg_2808" s="T927">рассказывать-HAB-2PL</ta>
            <ta e="T929" id="Seg_2809" s="T928">Q</ta>
            <ta e="T930" id="Seg_2810" s="T929">петь-HAB-2PL</ta>
            <ta e="T931" id="Seg_2811" s="T930">Q</ta>
            <ta e="T932" id="Seg_2812" s="T931">сказка-PL-2PL-ACC</ta>
            <ta e="T981" id="Seg_2813" s="T980">2PL.[NOM]</ta>
            <ta e="T982" id="Seg_2814" s="T981">ребенок-PL-2PL.[NOM]</ta>
            <ta e="T983" id="Seg_2815" s="T982">только</ta>
            <ta e="T984" id="Seg_2816" s="T983">знать-PRS-3PL</ta>
            <ta e="T985" id="Seg_2817" s="T984">быть-FUT.[3SG]</ta>
            <ta e="T986" id="Seg_2818" s="T985">этот</ta>
            <ta e="T987" id="Seg_2819" s="T986">сказка-PL-ACC</ta>
            <ta e="T988" id="Seg_2820" s="T987">да</ta>
            <ta e="T989" id="Seg_2821" s="T988">песня-PL-ACC</ta>
            <ta e="T990" id="Seg_2822" s="T989">да</ta>
            <ta e="T1023" id="Seg_2823" s="T1022">теперь</ta>
            <ta e="T1024" id="Seg_2824" s="T1023">что.[NOM]</ta>
            <ta e="T1025" id="Seg_2825" s="T1024">INDEF</ta>
            <ta e="T1026" id="Seg_2826" s="T1025">сказка-3SG-PART</ta>
            <ta e="T1027" id="Seg_2827" s="T1026">рассказывать.[IMP.2SG]</ta>
            <ta e="T1028" id="Seg_2828" s="T1027">этот</ta>
            <ta e="T1029" id="Seg_2829" s="T1028">сказка.[NOM]</ta>
            <ta e="T1030" id="Seg_2830" s="T1029">вообще-EMPH</ta>
            <ta e="T1031" id="Seg_2831" s="T1030">этот</ta>
            <ta e="T1032" id="Seg_2832" s="T1031">потом</ta>
            <ta e="T1033" id="Seg_2833" s="T1032">каждый-3SG.[NOM]</ta>
            <ta e="T1034" id="Seg_2834" s="T1033">разговаривать-PTCP.PRS-1PL.[NOM]</ta>
            <ta e="T1035" id="Seg_2835" s="T1034">наш</ta>
            <ta e="T1036" id="Seg_2836" s="T1035">оставаться-FUT-3SG</ta>
            <ta e="T1037" id="Seg_2837" s="T1036">EMPH</ta>
            <ta e="T1038" id="Seg_2838" s="T1037">позже</ta>
            <ta e="T1039" id="Seg_2839" s="T1038">позже</ta>
            <ta e="T1040" id="Seg_2840" s="T1039">ребенок-PL-EP-2SG.[NOM]</ta>
            <ta e="T1041" id="Seg_2841" s="T1040">слышать-PTCP.FUT-3PL-ACC</ta>
            <ta e="T1043" id="Seg_2842" s="T1042">рассказывать.[IMP.2SG]</ta>
            <ta e="T1044" id="Seg_2843" s="T1043">что.[NOM]</ta>
            <ta e="T1045" id="Seg_2844" s="T1044">INDEF</ta>
            <ta e="T1046" id="Seg_2845" s="T1045">сказка-3SG-PART</ta>
            <ta e="T1047" id="Seg_2846" s="T1046">этот</ta>
            <ta e="T1048" id="Seg_2847" s="T1047">недавно</ta>
            <ta e="T1049" id="Seg_2848" s="T1048">сказка-ABL</ta>
            <ta e="T1050" id="Seg_2849" s="T1049">э</ta>
            <ta e="T1051" id="Seg_2850" s="T1050">рассказывать-PTCP.PST</ta>
            <ta e="T1052" id="Seg_2851" s="T1051">сказка-2SG-ABL</ta>
            <ta e="T1053" id="Seg_2852" s="T1052">другой-PART</ta>
            <ta e="T1061" id="Seg_2853" s="T1060">Елена.[NOM]</ta>
            <ta e="T1062" id="Seg_2854" s="T1061">Трифоновна.[NOM]</ta>
            <ta e="T1063" id="Seg_2855" s="T1062">еще</ta>
            <ta e="T1064" id="Seg_2856" s="T1063">2PL-ACC</ta>
            <ta e="T1065" id="Seg_2857" s="T1064">стих-PL-ACC</ta>
            <ta e="T1066" id="Seg_2858" s="T1065">сочинать-VBZ-PRS-2SG</ta>
            <ta e="T1067" id="Seg_2859" s="T1066">говорить-PRS-2SG</ta>
            <ta e="T1068" id="Seg_2860" s="T1067">EMPH</ta>
            <ta e="T1069" id="Seg_2861" s="T1068">вот</ta>
            <ta e="T1070" id="Seg_2862" s="T1069">рассказывать.[IMP.2SG]</ta>
            <ta e="T1071" id="Seg_2863" s="T1070">вот</ta>
            <ta e="T1072" id="Seg_2864" s="T1071">что.[NOM]</ta>
            <ta e="T1073" id="Seg_2865" s="T1072">INDEF</ta>
            <ta e="T1074" id="Seg_2866" s="T1073">стих-3SG-ACC</ta>
         </annotation>
         <annotation name="mc" tierref="mc-AkEE">
            <ta e="T2" id="Seg_2867" s="T1">propr.[n:case]</ta>
            <ta e="T3" id="Seg_2868" s="T2">propr.[n:case]</ta>
            <ta e="T4" id="Seg_2869" s="T3">adv</ta>
            <ta e="T5" id="Seg_2870" s="T4">interj</ta>
            <ta e="T6" id="Seg_2871" s="T5">v-v:mood.pn</ta>
            <ta e="T8" id="Seg_2872" s="T7">emphpro-pro:(poss).[pro:case]</ta>
            <ta e="T9" id="Seg_2873" s="T8">n-n:poss-n:case</ta>
            <ta e="T10" id="Seg_2874" s="T9">que</ta>
            <ta e="T11" id="Seg_2875" s="T10">n-n&gt;adj-n:(pred.pn)=ptcl</ta>
            <ta e="T12" id="Seg_2876" s="T11">que</ta>
            <ta e="T13" id="Seg_2877" s="T12">v-v:tense-v:pred.pn</ta>
            <ta e="T14" id="Seg_2878" s="T13">n-n:(num).[n:case]</ta>
            <ta e="T15" id="Seg_2879" s="T14">que-que&gt;adj.[pro:case]</ta>
            <ta e="T16" id="Seg_2880" s="T15">emphpro-pro:(poss).[pro:case]</ta>
            <ta e="T17" id="Seg_2881" s="T16">n-n:poss-n:case</ta>
            <ta e="T19" id="Seg_2882" s="T18">n-n:(num)-n:(poss).[n:case]</ta>
            <ta e="T20" id="Seg_2883" s="T19">n-n:(num)-n:(poss).[n:case]</ta>
            <ta e="T22" id="Seg_2884" s="T20">n-n:poss-n:case</ta>
            <ta e="T24" id="Seg_2885" s="T22">que-pro:(num).[pro:case]</ta>
            <ta e="T189" id="Seg_2886" s="T187">pers.[pro:case]</ta>
            <ta e="T191" id="Seg_2887" s="T189">n-n:(poss).[n:case]</ta>
            <ta e="T192" id="Seg_2888" s="T191">adj.[n:case]</ta>
            <ta e="T193" id="Seg_2889" s="T192">ptcl</ta>
            <ta e="T194" id="Seg_2890" s="T193">v-v:tense.[v:pred.pn]</ta>
            <ta e="T195" id="Seg_2891" s="T194">dempro</ta>
            <ta e="T196" id="Seg_2892" s="T195">ptcl</ta>
            <ta e="T197" id="Seg_2893" s="T196">n-n:(num)-n:(poss).[n:case]</ta>
            <ta e="T198" id="Seg_2894" s="T197">n-n:(poss).[n:case]</ta>
            <ta e="T199" id="Seg_2895" s="T198">interj</ta>
            <ta e="T200" id="Seg_2896" s="T199">pers-pro:case</ta>
            <ta e="T201" id="Seg_2897" s="T200">v-v:mood-v:pred.pn</ta>
            <ta e="T202" id="Seg_2898" s="T201">ptcl</ta>
            <ta e="T278" id="Seg_2899" s="T277">adv</ta>
            <ta e="T279" id="Seg_2900" s="T278">n-n:(num)-n:poss-n:case</ta>
            <ta e="T280" id="Seg_2901" s="T279">ptcl</ta>
            <ta e="T281" id="Seg_2902" s="T280">dempro</ta>
            <ta e="T282" id="Seg_2903" s="T281">pers.[pro:case]</ta>
            <ta e="T283" id="Seg_2904" s="T282">v-v:tense-v:pred.pn</ta>
            <ta e="T284" id="Seg_2905" s="T283">interj</ta>
            <ta e="T285" id="Seg_2906" s="T284">quant</ta>
            <ta e="T286" id="Seg_2907" s="T285">ptcl</ta>
            <ta e="T287" id="Seg_2908" s="T286">n-n:(num)-n:case</ta>
            <ta e="T288" id="Seg_2909" s="T287">conj</ta>
            <ta e="T289" id="Seg_2910" s="T288">n-n:(num)-n:case</ta>
            <ta e="T290" id="Seg_2911" s="T289">conj</ta>
            <ta e="T291" id="Seg_2912" s="T290">n-n:(num)-n:case</ta>
            <ta e="T292" id="Seg_2913" s="T291">n-n:(num)-n:poss-n:case</ta>
            <ta e="T293" id="Seg_2914" s="T292">ptcl</ta>
            <ta e="T294" id="Seg_2915" s="T293">v-v:tense-v:pred.pn</ta>
            <ta e="T295" id="Seg_2916" s="T294">ptcl</ta>
            <ta e="T296" id="Seg_2917" s="T295">pers-pro:case</ta>
            <ta e="T298" id="Seg_2918" s="T297">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T299" id="Seg_2919" s="T298">n-n:(num)-n:(poss).[n:case]</ta>
            <ta e="T349" id="Seg_2920" s="T348">emphpro-pro:(poss).[pro:case]</ta>
            <ta e="T350" id="Seg_2921" s="T349">ptcl</ta>
            <ta e="T351" id="Seg_2922" s="T350">dempro</ta>
            <ta e="T352" id="Seg_2923" s="T351">n-n:(num)-n:case</ta>
            <ta e="T353" id="Seg_2924" s="T352">n-n:(num)-n:case</ta>
            <ta e="T354" id="Seg_2925" s="T353">que</ta>
            <ta e="T355" id="Seg_2926" s="T354">v-v:tense-v:pred.pn=ptcl</ta>
            <ta e="T507" id="Seg_2927" s="T506">ptcl</ta>
            <ta e="T508" id="Seg_2928" s="T507">v-v:mood.pn</ta>
            <ta e="T509" id="Seg_2929" s="T508">ptcl</ta>
            <ta e="T510" id="Seg_2930" s="T509">dempro</ta>
            <ta e="T511" id="Seg_2931" s="T510">que</ta>
            <ta e="T512" id="Seg_2932" s="T511">v-v:tense-v:pred.pn</ta>
            <ta e="T513" id="Seg_2933" s="T512">dempro</ta>
            <ta e="T514" id="Seg_2934" s="T513">dempro</ta>
            <ta e="T515" id="Seg_2935" s="T514">adv</ta>
            <ta e="T516" id="Seg_2936" s="T515">v-v:ptcp</ta>
            <ta e="T517" id="Seg_2937" s="T516">v-v&gt;n-n:poss-n:case</ta>
            <ta e="T541" id="Seg_2938" s="T540">ptcl</ta>
            <ta e="T542" id="Seg_2939" s="T541">v.[v:mood.pn]</ta>
            <ta e="T543" id="Seg_2940" s="T542">ptcl</ta>
            <ta e="T544" id="Seg_2941" s="T543">que.[pro:case]</ta>
            <ta e="T545" id="Seg_2942" s="T544">ptcl</ta>
            <ta e="T546" id="Seg_2943" s="T545">n-n:poss-n:case</ta>
            <ta e="T547" id="Seg_2944" s="T546">dempro</ta>
            <ta e="T548" id="Seg_2945" s="T547">adv</ta>
            <ta e="T549" id="Seg_2946" s="T548">v-v:ptcp</ta>
            <ta e="T550" id="Seg_2947" s="T549">v-v:tense-v:poss.pn</ta>
            <ta e="T551" id="Seg_2948" s="T550">n-n:(num).[n:case]</ta>
            <ta e="T552" id="Seg_2949" s="T551">v-v:ptcp-v:(num)-v:(case)</ta>
            <ta e="T553" id="Seg_2950" s="T552">n-n:(num).[n:case]</ta>
            <ta e="T554" id="Seg_2951" s="T553">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T555" id="Seg_2952" s="T554">dempro</ta>
            <ta e="T556" id="Seg_2953" s="T555">n-n:(num)-n:case</ta>
            <ta e="T557" id="Seg_2954" s="T556">v-v:tense-v:pred.pn</ta>
            <ta e="T558" id="Seg_2955" s="T557">ptcl</ta>
            <ta e="T609" id="Seg_2956" s="T608">que.[pro:case]</ta>
            <ta e="T610" id="Seg_2957" s="T609">n-n:(poss).[n:case]=ptcl</ta>
            <ta e="T622" id="Seg_2958" s="T621">adv</ta>
            <ta e="T623" id="Seg_2959" s="T622">dempro</ta>
            <ta e="T624" id="Seg_2960" s="T623">pers.[pro:case]</ta>
            <ta e="T625" id="Seg_2961" s="T624">quant</ta>
            <ta e="T626" id="Seg_2962" s="T625">que-pro:case</ta>
            <ta e="T627" id="Seg_2963" s="T626">v-v:tense-v:pred.pn</ta>
            <ta e="T628" id="Seg_2964" s="T627">adv</ta>
            <ta e="T629" id="Seg_2965" s="T628">n-n:(num)-n:case</ta>
            <ta e="T630" id="Seg_2966" s="T629">conj</ta>
            <ta e="T631" id="Seg_2967" s="T630">adv</ta>
            <ta e="T632" id="Seg_2968" s="T631">dempro</ta>
            <ta e="T633" id="Seg_2969" s="T632">que-pro:(num)-pro:case</ta>
            <ta e="T634" id="Seg_2970" s="T633">n</ta>
            <ta e="T635" id="Seg_2971" s="T634">v-v:cvb</ta>
            <ta e="T636" id="Seg_2972" s="T635">dempro.[pro:case]</ta>
            <ta e="T637" id="Seg_2973" s="T636">ptcl</ta>
            <ta e="T638" id="Seg_2974" s="T637">v-v:tense-v:pred.pn</ta>
            <ta e="T639" id="Seg_2975" s="T638">ptcl</ta>
            <ta e="T640" id="Seg_2976" s="T639">que.[pro:case]</ta>
            <ta e="T641" id="Seg_2977" s="T640">ptcl</ta>
            <ta e="T717" id="Seg_2978" s="T716">dempro</ta>
            <ta e="T718" id="Seg_2979" s="T717">propr-n:case</ta>
            <ta e="T719" id="Seg_2980" s="T718">propr</ta>
            <ta e="T720" id="Seg_2981" s="T719">v-v:cvb</ta>
            <ta e="T721" id="Seg_2982" s="T720">n.[n:case]</ta>
            <ta e="T722" id="Seg_2983" s="T721">propr</ta>
            <ta e="T723" id="Seg_2984" s="T722">dempro.[pro:case]</ta>
            <ta e="T724" id="Seg_2985" s="T723">pers.[pro:case]</ta>
            <ta e="T725" id="Seg_2986" s="T724">n-n:(poss).[n:case]</ta>
            <ta e="T726" id="Seg_2987" s="T725">v-v:neg.[v:pred.pn]</ta>
            <ta e="T727" id="Seg_2988" s="T726">ptcl</ta>
            <ta e="T732" id="Seg_2989" s="T731">ptcl</ta>
            <ta e="T733" id="Seg_2990" s="T732">dempro.[pro:case]</ta>
            <ta e="T734" id="Seg_2991" s="T733">post</ta>
            <ta e="T735" id="Seg_2992" s="T734">n-n:(num)-n:case</ta>
            <ta e="T736" id="Seg_2993" s="T735">v-v:tense.[v:pred.pn]</ta>
            <ta e="T737" id="Seg_2994" s="T736">v-v:tense.[v:pred.pn]</ta>
            <ta e="T738" id="Seg_2995" s="T737">adv</ta>
            <ta e="T739" id="Seg_2996" s="T738">que</ta>
            <ta e="T740" id="Seg_2997" s="T739">ptcl</ta>
            <ta e="T741" id="Seg_2998" s="T740">v-v:cvb</ta>
            <ta e="T742" id="Seg_2999" s="T741">v-v:cvb</ta>
            <ta e="T743" id="Seg_3000" s="T742">v-v:tense-v:poss.pn</ta>
            <ta e="T744" id="Seg_3001" s="T743">cardnum</ta>
            <ta e="T745" id="Seg_3002" s="T744">n-n:case</ta>
            <ta e="T746" id="Seg_3003" s="T745">pers.[pro:case]</ta>
            <ta e="T747" id="Seg_3004" s="T746">v-v:tense-v:pred.pn</ta>
            <ta e="T748" id="Seg_3005" s="T747">que</ta>
            <ta e="T749" id="Seg_3006" s="T748">v-v:ptcp-v:(poss)</ta>
            <ta e="T750" id="Seg_3007" s="T749">v-v:tense.[v:poss.pn]=ptcl</ta>
            <ta e="T751" id="Seg_3008" s="T750">ptcl</ta>
            <ta e="T752" id="Seg_3009" s="T751">adv</ta>
            <ta e="T753" id="Seg_3010" s="T752">pers.[pro:case]</ta>
            <ta e="T754" id="Seg_3011" s="T753">ptcl</ta>
            <ta e="T755" id="Seg_3012" s="T754">pers-pro:case</ta>
            <ta e="T756" id="Seg_3013" s="T755">v-v:cvb</ta>
            <ta e="T757" id="Seg_3014" s="T756">adv</ta>
            <ta e="T758" id="Seg_3015" s="T757">v-v:tense.[v:pred.pn]</ta>
            <ta e="T759" id="Seg_3016" s="T758">v-v:tense.[v:pred.pn]</ta>
            <ta e="T760" id="Seg_3017" s="T759">dempro</ta>
            <ta e="T790" id="Seg_3018" s="T789">v-v:(ins)-v:neg-v:(ins)-v:poss.pn</ta>
            <ta e="T791" id="Seg_3019" s="T790">ptcl</ta>
            <ta e="T792" id="Seg_3020" s="T791">n-n:case</ta>
            <ta e="T814" id="Seg_3021" s="T813">adv</ta>
            <ta e="T815" id="Seg_3022" s="T814">adv</ta>
            <ta e="T816" id="Seg_3023" s="T815">v-v:tense-v:pred.pn</ta>
            <ta e="T817" id="Seg_3024" s="T816">ptcl</ta>
            <ta e="T818" id="Seg_3025" s="T817">adv</ta>
            <ta e="T819" id="Seg_3026" s="T818">que</ta>
            <ta e="T827" id="Seg_3027" s="T826">propr.[n:case]</ta>
            <ta e="T828" id="Seg_3028" s="T827">propr.[n:case]</ta>
            <ta e="T829" id="Seg_3029" s="T828">dempro</ta>
            <ta e="T830" id="Seg_3030" s="T829">pers-pro:case</ta>
            <ta e="T831" id="Seg_3031" s="T830">adj.[n:case]</ta>
            <ta e="T832" id="Seg_3032" s="T831">que.[pro:case]</ta>
            <ta e="T833" id="Seg_3033" s="T832">ptcl</ta>
            <ta e="T834" id="Seg_3034" s="T833">ptcl</ta>
            <ta e="T835" id="Seg_3035" s="T834">ptcl</ta>
            <ta e="T836" id="Seg_3036" s="T835">n.[n:case]</ta>
            <ta e="T837" id="Seg_3037" s="T836">n.[n:case]</ta>
            <ta e="T838" id="Seg_3038" s="T837">ptcl</ta>
            <ta e="T839" id="Seg_3039" s="T838">n.[n:case]</ta>
            <ta e="T840" id="Seg_3040" s="T839">ptcl</ta>
            <ta e="T841" id="Seg_3041" s="T840">propr-n:case</ta>
            <ta e="T842" id="Seg_3042" s="T841">que.[pro:case]</ta>
            <ta e="T843" id="Seg_3043" s="T842">v-v:tense.[v:pred.pn]</ta>
            <ta e="T844" id="Seg_3044" s="T843">n-n:case</ta>
            <ta e="T845" id="Seg_3045" s="T844">conj</ta>
            <ta e="T846" id="Seg_3046" s="T845">adv-adv&gt;adj</ta>
            <ta e="T847" id="Seg_3047" s="T846">conj</ta>
            <ta e="T848" id="Seg_3048" s="T847">n-n:case</ta>
            <ta e="T884" id="Seg_3049" s="T883">adj.[n:case]</ta>
            <ta e="T885" id="Seg_3050" s="T884">n-n:(num).[n:case]</ta>
            <ta e="T886" id="Seg_3051" s="T885">ptcl</ta>
            <ta e="T919" id="Seg_3052" s="T918">dempro</ta>
            <ta e="T920" id="Seg_3053" s="T919">emphpro-pro:(poss).[pro:case]</ta>
            <ta e="T921" id="Seg_3054" s="T920">propr-n:poss-n:case</ta>
            <ta e="T922" id="Seg_3055" s="T921">n-n:case</ta>
            <ta e="T923" id="Seg_3056" s="T922">pers.[pro:case]</ta>
            <ta e="T924" id="Seg_3057" s="T923">dempro</ta>
            <ta e="T925" id="Seg_3058" s="T924">n-n:(num).[n:case]</ta>
            <ta e="T926" id="Seg_3059" s="T925">n-n:poss-n:case</ta>
            <ta e="T927" id="Seg_3060" s="T926">v-v&gt;v-v:cvb</ta>
            <ta e="T928" id="Seg_3061" s="T927">v-v:mood-v:pred.pn</ta>
            <ta e="T929" id="Seg_3062" s="T928">ptcl</ta>
            <ta e="T930" id="Seg_3063" s="T929">v-v:mood-v:pred.pn</ta>
            <ta e="T931" id="Seg_3064" s="T930">ptcl</ta>
            <ta e="T932" id="Seg_3065" s="T931">n-n:(num)-n:poss-n:case</ta>
            <ta e="T981" id="Seg_3066" s="T980">pers.[pro:case]</ta>
            <ta e="T982" id="Seg_3067" s="T981">n-n:(num)-n:(poss).[n:case]</ta>
            <ta e="T983" id="Seg_3068" s="T982">ptcl</ta>
            <ta e="T984" id="Seg_3069" s="T983">v-v:tense-v:poss.pn</ta>
            <ta e="T985" id="Seg_3070" s="T984">v-v:tense.[v:poss.pn]</ta>
            <ta e="T986" id="Seg_3071" s="T985">dempro</ta>
            <ta e="T987" id="Seg_3072" s="T986">n-n:(num)-n:case</ta>
            <ta e="T988" id="Seg_3073" s="T987">conj</ta>
            <ta e="T989" id="Seg_3074" s="T988">n-n:(num)-n:case</ta>
            <ta e="T990" id="Seg_3075" s="T989">conj</ta>
            <ta e="T1023" id="Seg_3076" s="T1022">adv</ta>
            <ta e="T1024" id="Seg_3077" s="T1023">que.[pro:case]</ta>
            <ta e="T1025" id="Seg_3078" s="T1024">ptcl</ta>
            <ta e="T1026" id="Seg_3079" s="T1025">n-n:poss-n:case</ta>
            <ta e="T1027" id="Seg_3080" s="T1026">v.[v:mood.pn]</ta>
            <ta e="T1028" id="Seg_3081" s="T1027">dempro</ta>
            <ta e="T1029" id="Seg_3082" s="T1028">n.[n:case]</ta>
            <ta e="T1030" id="Seg_3083" s="T1029">adv-ptcl</ta>
            <ta e="T1031" id="Seg_3084" s="T1030">dempro</ta>
            <ta e="T1032" id="Seg_3085" s="T1031">adv</ta>
            <ta e="T1033" id="Seg_3086" s="T1032">adj-n:(poss).[n:case]</ta>
            <ta e="T1034" id="Seg_3087" s="T1033">v-v:ptcp-v:(poss).[v:(case)]</ta>
            <ta e="T1035" id="Seg_3088" s="T1034">posspr</ta>
            <ta e="T1036" id="Seg_3089" s="T1035">v-v:tense-v:poss.pn</ta>
            <ta e="T1037" id="Seg_3090" s="T1036">ptcl</ta>
            <ta e="T1038" id="Seg_3091" s="T1037">adv</ta>
            <ta e="T1039" id="Seg_3092" s="T1038">adv</ta>
            <ta e="T1040" id="Seg_3093" s="T1039">n-n:(num)-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T1041" id="Seg_3094" s="T1040">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T1043" id="Seg_3095" s="T1042">v.[v:mood.pn]</ta>
            <ta e="T1044" id="Seg_3096" s="T1043">que.[pro:case]</ta>
            <ta e="T1045" id="Seg_3097" s="T1044">ptcl</ta>
            <ta e="T1046" id="Seg_3098" s="T1045">n-n:poss-n:case</ta>
            <ta e="T1047" id="Seg_3099" s="T1046">dempro</ta>
            <ta e="T1048" id="Seg_3100" s="T1047">adv</ta>
            <ta e="T1049" id="Seg_3101" s="T1048">n-n:case</ta>
            <ta e="T1050" id="Seg_3102" s="T1049">interj</ta>
            <ta e="T1051" id="Seg_3103" s="T1050">v-v:ptcp</ta>
            <ta e="T1052" id="Seg_3104" s="T1051">n-n:poss-n:case</ta>
            <ta e="T1053" id="Seg_3105" s="T1052">adj-n:case</ta>
            <ta e="T1061" id="Seg_3106" s="T1060">propr.[n:case]</ta>
            <ta e="T1062" id="Seg_3107" s="T1061">propr.[n:case]</ta>
            <ta e="T1063" id="Seg_3108" s="T1062">adv</ta>
            <ta e="T1064" id="Seg_3109" s="T1063">pers-pro:case</ta>
            <ta e="T1065" id="Seg_3110" s="T1064">n-n:(num)-n:case</ta>
            <ta e="T1066" id="Seg_3111" s="T1065">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T1067" id="Seg_3112" s="T1066">v-v:tense-v:pred.pn</ta>
            <ta e="T1068" id="Seg_3113" s="T1067">ptcl</ta>
            <ta e="T1069" id="Seg_3114" s="T1068">ptcl</ta>
            <ta e="T1070" id="Seg_3115" s="T1069">v.[v:mood.pn]</ta>
            <ta e="T1071" id="Seg_3116" s="T1070">ptcl</ta>
            <ta e="T1072" id="Seg_3117" s="T1071">que.[pro:case]</ta>
            <ta e="T1073" id="Seg_3118" s="T1072">ptcl</ta>
            <ta e="T1074" id="Seg_3119" s="T1073">n-n:poss-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps-AkEE">
            <ta e="T2" id="Seg_3120" s="T1">propr</ta>
            <ta e="T3" id="Seg_3121" s="T2">propr</ta>
            <ta e="T4" id="Seg_3122" s="T3">adv</ta>
            <ta e="T5" id="Seg_3123" s="T4">interj</ta>
            <ta e="T6" id="Seg_3124" s="T5">v</ta>
            <ta e="T8" id="Seg_3125" s="T7">emphpro</ta>
            <ta e="T9" id="Seg_3126" s="T8">n</ta>
            <ta e="T10" id="Seg_3127" s="T9">que</ta>
            <ta e="T11" id="Seg_3128" s="T10">adj</ta>
            <ta e="T12" id="Seg_3129" s="T11">que</ta>
            <ta e="T13" id="Seg_3130" s="T12">v</ta>
            <ta e="T14" id="Seg_3131" s="T13">n</ta>
            <ta e="T15" id="Seg_3132" s="T14">que</ta>
            <ta e="T16" id="Seg_3133" s="T15">emphpro</ta>
            <ta e="T17" id="Seg_3134" s="T16">n</ta>
            <ta e="T19" id="Seg_3135" s="T18">n</ta>
            <ta e="T20" id="Seg_3136" s="T19">n</ta>
            <ta e="T22" id="Seg_3137" s="T20">n</ta>
            <ta e="T24" id="Seg_3138" s="T22">que</ta>
            <ta e="T189" id="Seg_3139" s="T187">pers</ta>
            <ta e="T191" id="Seg_3140" s="T189">n</ta>
            <ta e="T192" id="Seg_3141" s="T191">adj</ta>
            <ta e="T193" id="Seg_3142" s="T192">ptcl</ta>
            <ta e="T194" id="Seg_3143" s="T193">aux</ta>
            <ta e="T195" id="Seg_3144" s="T194">dempro</ta>
            <ta e="T196" id="Seg_3145" s="T195">ptcl</ta>
            <ta e="T197" id="Seg_3146" s="T196">n</ta>
            <ta e="T198" id="Seg_3147" s="T197">n</ta>
            <ta e="T199" id="Seg_3148" s="T198">interj</ta>
            <ta e="T200" id="Seg_3149" s="T199">pers</ta>
            <ta e="T201" id="Seg_3150" s="T200">cop</ta>
            <ta e="T202" id="Seg_3151" s="T201">ptcl</ta>
            <ta e="T278" id="Seg_3152" s="T277">adv</ta>
            <ta e="T279" id="Seg_3153" s="T278">n</ta>
            <ta e="T280" id="Seg_3154" s="T279">ptcl</ta>
            <ta e="T281" id="Seg_3155" s="T280">dempro</ta>
            <ta e="T282" id="Seg_3156" s="T281">pers</ta>
            <ta e="T283" id="Seg_3157" s="T282">v</ta>
            <ta e="T284" id="Seg_3158" s="T283">interj</ta>
            <ta e="T285" id="Seg_3159" s="T284">quant</ta>
            <ta e="T286" id="Seg_3160" s="T285">ptcl</ta>
            <ta e="T287" id="Seg_3161" s="T286">n</ta>
            <ta e="T288" id="Seg_3162" s="T287">conj</ta>
            <ta e="T289" id="Seg_3163" s="T288">n</ta>
            <ta e="T290" id="Seg_3164" s="T289">conj</ta>
            <ta e="T291" id="Seg_3165" s="T290">n</ta>
            <ta e="T292" id="Seg_3166" s="T291">n</ta>
            <ta e="T293" id="Seg_3167" s="T292">ptcl</ta>
            <ta e="T294" id="Seg_3168" s="T293">v</ta>
            <ta e="T295" id="Seg_3169" s="T294">ptcl</ta>
            <ta e="T296" id="Seg_3170" s="T295">pers</ta>
            <ta e="T298" id="Seg_3171" s="T297">v</ta>
            <ta e="T299" id="Seg_3172" s="T298">n</ta>
            <ta e="T349" id="Seg_3173" s="T348">emphpro</ta>
            <ta e="T350" id="Seg_3174" s="T349">ptcl</ta>
            <ta e="T351" id="Seg_3175" s="T350">dempro</ta>
            <ta e="T352" id="Seg_3176" s="T351">n</ta>
            <ta e="T353" id="Seg_3177" s="T352">n</ta>
            <ta e="T354" id="Seg_3178" s="T353">que</ta>
            <ta e="T355" id="Seg_3179" s="T354">v</ta>
            <ta e="T507" id="Seg_3180" s="T506">ptcl</ta>
            <ta e="T508" id="Seg_3181" s="T507">v</ta>
            <ta e="T509" id="Seg_3182" s="T508">ptcl</ta>
            <ta e="T510" id="Seg_3183" s="T509">dempro</ta>
            <ta e="T511" id="Seg_3184" s="T510">que</ta>
            <ta e="T512" id="Seg_3185" s="T511">v</ta>
            <ta e="T513" id="Seg_3186" s="T512">dempro</ta>
            <ta e="T514" id="Seg_3187" s="T513">dempro</ta>
            <ta e="T515" id="Seg_3188" s="T514">adv</ta>
            <ta e="T516" id="Seg_3189" s="T515">v</ta>
            <ta e="T517" id="Seg_3190" s="T516">n</ta>
            <ta e="T541" id="Seg_3191" s="T540">ptcl</ta>
            <ta e="T542" id="Seg_3192" s="T541">v</ta>
            <ta e="T543" id="Seg_3193" s="T542">ptcl</ta>
            <ta e="T544" id="Seg_3194" s="T543">que</ta>
            <ta e="T545" id="Seg_3195" s="T544">ptcl</ta>
            <ta e="T546" id="Seg_3196" s="T545">n</ta>
            <ta e="T547" id="Seg_3197" s="T546">dempro</ta>
            <ta e="T548" id="Seg_3198" s="T547">adv</ta>
            <ta e="T549" id="Seg_3199" s="T548">v</ta>
            <ta e="T550" id="Seg_3200" s="T549">aux</ta>
            <ta e="T551" id="Seg_3201" s="T550">n</ta>
            <ta e="T552" id="Seg_3202" s="T551">v</ta>
            <ta e="T553" id="Seg_3203" s="T552">n</ta>
            <ta e="T554" id="Seg_3204" s="T553">v</ta>
            <ta e="T555" id="Seg_3205" s="T554">dempro</ta>
            <ta e="T556" id="Seg_3206" s="T555">n</ta>
            <ta e="T557" id="Seg_3207" s="T556">v</ta>
            <ta e="T558" id="Seg_3208" s="T557">ptcl</ta>
            <ta e="T609" id="Seg_3209" s="T608">que</ta>
            <ta e="T610" id="Seg_3210" s="T609">n</ta>
            <ta e="T622" id="Seg_3211" s="T621">adv</ta>
            <ta e="T623" id="Seg_3212" s="T622">dempro</ta>
            <ta e="T624" id="Seg_3213" s="T623">pers</ta>
            <ta e="T625" id="Seg_3214" s="T624">quant</ta>
            <ta e="T626" id="Seg_3215" s="T625">que</ta>
            <ta e="T627" id="Seg_3216" s="T626">v</ta>
            <ta e="T628" id="Seg_3217" s="T627">adv</ta>
            <ta e="T629" id="Seg_3218" s="T628">n</ta>
            <ta e="T630" id="Seg_3219" s="T629">conj</ta>
            <ta e="T631" id="Seg_3220" s="T630">adv</ta>
            <ta e="T632" id="Seg_3221" s="T631">dempro</ta>
            <ta e="T633" id="Seg_3222" s="T632">que</ta>
            <ta e="T634" id="Seg_3223" s="T633">n</ta>
            <ta e="T635" id="Seg_3224" s="T634">v</ta>
            <ta e="T636" id="Seg_3225" s="T635">dempro</ta>
            <ta e="T637" id="Seg_3226" s="T636">ptcl</ta>
            <ta e="T638" id="Seg_3227" s="T637">v</ta>
            <ta e="T639" id="Seg_3228" s="T638">ptcl</ta>
            <ta e="T640" id="Seg_3229" s="T639">que</ta>
            <ta e="T641" id="Seg_3230" s="T640">ptcl</ta>
            <ta e="T717" id="Seg_3231" s="T716">dempro</ta>
            <ta e="T718" id="Seg_3232" s="T717">propr</ta>
            <ta e="T719" id="Seg_3233" s="T718">propr</ta>
            <ta e="T720" id="Seg_3234" s="T719">v</ta>
            <ta e="T721" id="Seg_3235" s="T720">n</ta>
            <ta e="T722" id="Seg_3236" s="T721">propr</ta>
            <ta e="T723" id="Seg_3237" s="T722">dempro</ta>
            <ta e="T724" id="Seg_3238" s="T723">pers</ta>
            <ta e="T725" id="Seg_3239" s="T724">n</ta>
            <ta e="T726" id="Seg_3240" s="T725">cop</ta>
            <ta e="T727" id="Seg_3241" s="T726">ptcl</ta>
            <ta e="T732" id="Seg_3242" s="T731">ptcl</ta>
            <ta e="T733" id="Seg_3243" s="T732">dempro</ta>
            <ta e="T734" id="Seg_3244" s="T733">post</ta>
            <ta e="T735" id="Seg_3245" s="T734">n</ta>
            <ta e="T736" id="Seg_3246" s="T735">v</ta>
            <ta e="T737" id="Seg_3247" s="T736">aux</ta>
            <ta e="T738" id="Seg_3248" s="T737">adv</ta>
            <ta e="T739" id="Seg_3249" s="T738">que</ta>
            <ta e="T740" id="Seg_3250" s="T739">ptcl</ta>
            <ta e="T741" id="Seg_3251" s="T740">v</ta>
            <ta e="T742" id="Seg_3252" s="T741">v</ta>
            <ta e="T743" id="Seg_3253" s="T742">v</ta>
            <ta e="T744" id="Seg_3254" s="T743">cardnum</ta>
            <ta e="T745" id="Seg_3255" s="T744">n</ta>
            <ta e="T746" id="Seg_3256" s="T745">pers</ta>
            <ta e="T747" id="Seg_3257" s="T746">v</ta>
            <ta e="T748" id="Seg_3258" s="T747">que</ta>
            <ta e="T749" id="Seg_3259" s="T748">v</ta>
            <ta e="T750" id="Seg_3260" s="T749">cop</ta>
            <ta e="T751" id="Seg_3261" s="T750">ptcl</ta>
            <ta e="T752" id="Seg_3262" s="T751">adv</ta>
            <ta e="T753" id="Seg_3263" s="T752">pers</ta>
            <ta e="T754" id="Seg_3264" s="T753">ptcl</ta>
            <ta e="T755" id="Seg_3265" s="T754">pers</ta>
            <ta e="T756" id="Seg_3266" s="T755">v</ta>
            <ta e="T757" id="Seg_3267" s="T756">adv</ta>
            <ta e="T758" id="Seg_3268" s="T757">v</ta>
            <ta e="T759" id="Seg_3269" s="T758">aux</ta>
            <ta e="T760" id="Seg_3270" s="T759">dempro</ta>
            <ta e="T790" id="Seg_3271" s="T789">v</ta>
            <ta e="T791" id="Seg_3272" s="T790">ptcl</ta>
            <ta e="T792" id="Seg_3273" s="T791">n</ta>
            <ta e="T814" id="Seg_3274" s="T813">adv</ta>
            <ta e="T815" id="Seg_3275" s="T814">adv</ta>
            <ta e="T816" id="Seg_3276" s="T815">v</ta>
            <ta e="T817" id="Seg_3277" s="T816">ptcl</ta>
            <ta e="T818" id="Seg_3278" s="T817">adv</ta>
            <ta e="T819" id="Seg_3279" s="T818">que</ta>
            <ta e="T827" id="Seg_3280" s="T826">propr</ta>
            <ta e="T828" id="Seg_3281" s="T827">propr</ta>
            <ta e="T829" id="Seg_3282" s="T828">dempro</ta>
            <ta e="T830" id="Seg_3283" s="T829">pers</ta>
            <ta e="T831" id="Seg_3284" s="T830">adj</ta>
            <ta e="T832" id="Seg_3285" s="T831">que</ta>
            <ta e="T833" id="Seg_3286" s="T832">ptcl</ta>
            <ta e="T834" id="Seg_3287" s="T833">ptcl</ta>
            <ta e="T835" id="Seg_3288" s="T834">ptcl</ta>
            <ta e="T836" id="Seg_3289" s="T835">n</ta>
            <ta e="T837" id="Seg_3290" s="T836">n</ta>
            <ta e="T838" id="Seg_3291" s="T837">ptcl</ta>
            <ta e="T839" id="Seg_3292" s="T838">n</ta>
            <ta e="T840" id="Seg_3293" s="T839">ptcl</ta>
            <ta e="T841" id="Seg_3294" s="T840">propr</ta>
            <ta e="T842" id="Seg_3295" s="T841">que</ta>
            <ta e="T843" id="Seg_3296" s="T842">v</ta>
            <ta e="T844" id="Seg_3297" s="T843">n</ta>
            <ta e="T845" id="Seg_3298" s="T844">conj</ta>
            <ta e="T846" id="Seg_3299" s="T845">adj</ta>
            <ta e="T847" id="Seg_3300" s="T846">conj</ta>
            <ta e="T848" id="Seg_3301" s="T847">n</ta>
            <ta e="T884" id="Seg_3302" s="T883">adj</ta>
            <ta e="T885" id="Seg_3303" s="T884">n</ta>
            <ta e="T886" id="Seg_3304" s="T885">ptcl</ta>
            <ta e="T919" id="Seg_3305" s="T918">dempro</ta>
            <ta e="T920" id="Seg_3306" s="T919">emphpro</ta>
            <ta e="T921" id="Seg_3307" s="T920">propr</ta>
            <ta e="T922" id="Seg_3308" s="T921">n</ta>
            <ta e="T923" id="Seg_3309" s="T922">pers</ta>
            <ta e="T924" id="Seg_3310" s="T923">dempro</ta>
            <ta e="T925" id="Seg_3311" s="T924">n</ta>
            <ta e="T926" id="Seg_3312" s="T925">n</ta>
            <ta e="T927" id="Seg_3313" s="T926">v</ta>
            <ta e="T928" id="Seg_3314" s="T927">v</ta>
            <ta e="T929" id="Seg_3315" s="T928">ptcl</ta>
            <ta e="T930" id="Seg_3316" s="T929">v</ta>
            <ta e="T931" id="Seg_3317" s="T930">ptcl</ta>
            <ta e="T932" id="Seg_3318" s="T931">n</ta>
            <ta e="T981" id="Seg_3319" s="T980">pers</ta>
            <ta e="T982" id="Seg_3320" s="T981">n</ta>
            <ta e="T983" id="Seg_3321" s="T982">ptcl</ta>
            <ta e="T984" id="Seg_3322" s="T983">v</ta>
            <ta e="T985" id="Seg_3323" s="T984">cop</ta>
            <ta e="T986" id="Seg_3324" s="T985">dempro</ta>
            <ta e="T987" id="Seg_3325" s="T986">n</ta>
            <ta e="T988" id="Seg_3326" s="T987">conj</ta>
            <ta e="T989" id="Seg_3327" s="T988">n</ta>
            <ta e="T990" id="Seg_3328" s="T989">conj</ta>
            <ta e="T1023" id="Seg_3329" s="T1022">adv</ta>
            <ta e="T1024" id="Seg_3330" s="T1023">que</ta>
            <ta e="T1025" id="Seg_3331" s="T1024">ptcl</ta>
            <ta e="T1026" id="Seg_3332" s="T1025">n</ta>
            <ta e="T1027" id="Seg_3333" s="T1026">v</ta>
            <ta e="T1028" id="Seg_3334" s="T1027">dempro</ta>
            <ta e="T1029" id="Seg_3335" s="T1028">n</ta>
            <ta e="T1030" id="Seg_3336" s="T1029">adv</ta>
            <ta e="T1031" id="Seg_3337" s="T1030">dempro</ta>
            <ta e="T1032" id="Seg_3338" s="T1031">adv</ta>
            <ta e="T1033" id="Seg_3339" s="T1032">adj</ta>
            <ta e="T1034" id="Seg_3340" s="T1033">n</ta>
            <ta e="T1035" id="Seg_3341" s="T1034">posspr</ta>
            <ta e="T1036" id="Seg_3342" s="T1035">v</ta>
            <ta e="T1037" id="Seg_3343" s="T1036">ptcl</ta>
            <ta e="T1038" id="Seg_3344" s="T1037">adv</ta>
            <ta e="T1039" id="Seg_3345" s="T1038">adv</ta>
            <ta e="T1040" id="Seg_3346" s="T1039">n</ta>
            <ta e="T1041" id="Seg_3347" s="T1040">v</ta>
            <ta e="T1043" id="Seg_3348" s="T1042">v</ta>
            <ta e="T1044" id="Seg_3349" s="T1043">que</ta>
            <ta e="T1045" id="Seg_3350" s="T1044">ptcl</ta>
            <ta e="T1046" id="Seg_3351" s="T1045">n</ta>
            <ta e="T1047" id="Seg_3352" s="T1046">dempro</ta>
            <ta e="T1048" id="Seg_3353" s="T1047">adv</ta>
            <ta e="T1049" id="Seg_3354" s="T1048">n</ta>
            <ta e="T1050" id="Seg_3355" s="T1049">interj</ta>
            <ta e="T1051" id="Seg_3356" s="T1050">v</ta>
            <ta e="T1052" id="Seg_3357" s="T1051">n</ta>
            <ta e="T1053" id="Seg_3358" s="T1052">n</ta>
            <ta e="T1061" id="Seg_3359" s="T1060">propr</ta>
            <ta e="T1062" id="Seg_3360" s="T1061">propr</ta>
            <ta e="T1063" id="Seg_3361" s="T1062">adv</ta>
            <ta e="T1064" id="Seg_3362" s="T1063">pers</ta>
            <ta e="T1065" id="Seg_3363" s="T1064">n</ta>
            <ta e="T1066" id="Seg_3364" s="T1065">v</ta>
            <ta e="T1067" id="Seg_3365" s="T1066">v</ta>
            <ta e="T1068" id="Seg_3366" s="T1067">ptcl</ta>
            <ta e="T1069" id="Seg_3367" s="T1068">ptcl</ta>
            <ta e="T1070" id="Seg_3368" s="T1069">v</ta>
            <ta e="T1071" id="Seg_3369" s="T1070">ptcl</ta>
            <ta e="T1072" id="Seg_3370" s="T1071">que</ta>
            <ta e="T1073" id="Seg_3371" s="T1072">ptcl</ta>
            <ta e="T1074" id="Seg_3372" s="T1073">n</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-AkEE" />
         <annotation name="SyF" tierref="SyF-AkEE" />
         <annotation name="IST" tierref="IST-AkEE" />
         <annotation name="Top" tierref="Top-AkEE" />
         <annotation name="Foc" tierref="Foc-AkEE" />
         <annotation name="BOR" tierref="BOR-AkEE">
            <ta e="T2" id="Seg_3373" s="T1">RUS:cult</ta>
            <ta e="T3" id="Seg_3374" s="T2">RUS:cult</ta>
            <ta e="T191" id="Seg_3375" s="T189">RUS:core</ta>
            <ta e="T288" id="Seg_3376" s="T287">RUS:gram</ta>
            <ta e="T290" id="Seg_3377" s="T289">RUS:gram</ta>
            <ta e="T630" id="Seg_3378" s="T629">RUS:gram</ta>
            <ta e="T634" id="Seg_3379" s="T633">RUS:cult</ta>
            <ta e="T718" id="Seg_3380" s="T717">RUS:cult</ta>
            <ta e="T719" id="Seg_3381" s="T718">RUS:cult</ta>
            <ta e="T722" id="Seg_3382" s="T721">RUS:cult</ta>
            <ta e="T792" id="Seg_3383" s="T791">RUS:cult</ta>
            <ta e="T814" id="Seg_3384" s="T813">RUS:mod</ta>
            <ta e="T827" id="Seg_3385" s="T826">RUS:cult</ta>
            <ta e="T828" id="Seg_3386" s="T827">RUS:cult</ta>
            <ta e="T841" id="Seg_3387" s="T840">RUS:cult</ta>
            <ta e="T845" id="Seg_3388" s="T844">RUS:gram</ta>
            <ta e="T847" id="Seg_3389" s="T846">RUS:gram</ta>
            <ta e="T921" id="Seg_3390" s="T920">RUS:cult</ta>
            <ta e="T922" id="Seg_3391" s="T921">RUS:cult</ta>
            <ta e="T927" id="Seg_3392" s="T926">RUS:cult</ta>
            <ta e="T988" id="Seg_3393" s="T987">RUS:gram</ta>
            <ta e="T990" id="Seg_3394" s="T989">RUS:gram</ta>
            <ta e="T1030" id="Seg_3395" s="T1029">RUS:modRUS:disc</ta>
            <ta e="T1061" id="Seg_3396" s="T1060">RUS:cult</ta>
            <ta e="T1062" id="Seg_3397" s="T1061">RUS:cult</ta>
            <ta e="T1063" id="Seg_3398" s="T1062">RUS:mod</ta>
            <ta e="T1066" id="Seg_3399" s="T1065">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-AkEE" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-AkEE" />
         <annotation name="CS" tierref="CS-AkEE" />
         <annotation name="fe" tierref="fe-AkEE">
            <ta e="T9" id="Seg_3400" s="T1">– Elena Triforovna, tell a bit about Yourself. </ta>
            <ta e="T15" id="Seg_3401" s="T9">Where do You live, where were You born, who are Your parents?</ta>
            <ta e="T24" id="Seg_3402" s="T15">About Yourself, about Your parents and Your children, something. </ta>
            <ta e="T194" id="Seg_3403" s="T187">– Your family is very big, apparently. </ta>
            <ta e="T202" id="Seg_3404" s="T194">And are the children of Your children now with You?</ta>
            <ta e="T291" id="Seg_3405" s="T277">– Then You know a lot of tales and songs for Your children, very old ones.</ta>
            <ta e="T299" id="Seg_3406" s="T291">And You are telling Your children those, so that Your children get to know them.</ta>
            <ta e="T355" id="Seg_3407" s="T348">– And how did You Yourself get to know these tales and songs?</ta>
            <ta e="T517" id="Seg_3408" s="T506">– Well, now tell, how does one play the game that You have brought?</ta>
            <ta e="T547" id="Seg_3409" s="T540">– Well, sing some song then. </ta>
            <ta e="T558" id="Seg_3410" s="T547">Earlier they said, [songs] which the girls sing, [songs] which the boys sing, do you know such songs?</ta>
            <ta e="T610" id="Seg_3411" s="T608">– Whose song is it?</ta>
            <ta e="T641" id="Seg_3412" s="T621">– Then You apparently know many whatchamacallit, riddles, then these whatchamacallit, holding one's breath, they are competing in it, aren't they?</ta>
            <ta e="T727" id="Seg_3413" s="T716">– And in Novorybnoe a girl is named Marina Beregovaya, isn't it Your daughter?</ta>
            <ta e="T737" id="Seg_3414" s="T731">– Ah, therefore she knows tales, apparently.</ta>
            <ta e="T750" id="Seg_3415" s="T737">She came here at some point and told a tale, I am thinking, where does she know it from?</ta>
            <ta e="T760" id="Seg_3416" s="T750">Well, then you are there, she heard it apparently from you and now she knows it.</ta>
            <ta e="T792" id="Seg_3417" s="T789">– Didn't you here it in the radio?</ta>
            <ta e="T819" id="Seg_3418" s="T813">– In any case they recognixed it.</ta>
            <ta e="T848" id="Seg_3419" s="T826">– Elena Trifonovna, is there anybody apart from You, a man or a woman, who knows tales or old songs in Novorybnoe?</ta>
            <ta e="T887" id="Seg_3420" s="T883">– And other people (…)?</ta>
            <ta e="T932" id="Seg_3421" s="T918">– In Novorybnoe in the club you did perform and told and sang your tales?</ta>
            <ta e="T990" id="Seg_3422" s="T980">– Only Your children probably know these tales and songs.</ta>
            <ta e="T1029" id="Seg_3423" s="T1022">– Tell now some tale, this tale.</ta>
            <ta e="T1033" id="Seg_3424" s="T1029">All that in general…</ta>
            <ta e="T1037" id="Seg_3425" s="T1033">Our talk will stay alive.</ta>
            <ta e="T1041" id="Seg_3426" s="T1037">For that the children hear it later.</ta>
            <ta e="T1047" id="Seg_3427" s="T1042">– Tell some tale.</ta>
            <ta e="T1053" id="Seg_3428" s="T1047">Another than that which you just told.</ta>
            <ta e="T1068" id="Seg_3429" s="T1060">– Elena Triforovna, You say that You also compose poems.</ta>
            <ta e="T1074" id="Seg_3430" s="T1068">Well, tell some poem.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-AkEE">
            <ta e="T9" id="Seg_3431" s="T1">– Elena Triforovna, erzählen Sie ein Bisschen von sich.</ta>
            <ta e="T15" id="Seg_3432" s="T9">Wo wohnen Sie, wo wurden Sie geboren, wer sind Ihre Eltern?</ta>
            <ta e="T24" id="Seg_3433" s="T15">Über sich selbst, über Ihre Eltern und Ihre Kinder, etwas.</ta>
            <ta e="T194" id="Seg_3434" s="T187">– Ihre Familie ist offenbar sehr groß.</ta>
            <ta e="T202" id="Seg_3435" s="T194">Und sind die Kinder Ihrer Kinder jetzt bei Ihnen?</ta>
            <ta e="T291" id="Seg_3436" s="T277">– Dann kennen Sie viele Märchen und Lieder für Ihre Kinder, ganz alte.</ta>
            <ta e="T299" id="Seg_3437" s="T291">Und sie erzählen sie Ihren Kindern, damit sie sie kennenlernen.</ta>
            <ta e="T355" id="Seg_3438" s="T348">– Und wie haben Sie selbst diese Märchen und Lieder kennengelernt?</ta>
            <ta e="T517" id="Seg_3439" s="T506">– Nun, erzählen Sie, wie spielt man das Spiel, das Sie mitgebracht haben?</ta>
            <ta e="T547" id="Seg_3440" s="T540">– Nun, sing doch irgendein Lied.</ta>
            <ta e="T558" id="Seg_3441" s="T547">Früher sagte man, welche, die die Mädchen singen, welche, die die Jungen singen, kennst du solche Lieder?</ta>
            <ta e="T610" id="Seg_3442" s="T608">– Wessen Lied ist das?</ta>
            <ta e="T641" id="Seg_3443" s="T621">– Dann kennen Sie offenbar viele Dings, Rätsel, dann diese Dings, das Luftanhalten, da misst man sich miteinander oder?</ta>
            <ta e="T727" id="Seg_3444" s="T716">– Und in Novorybnoe heißt ein Mädchen Marina Beregovaja, ist das nicht Ihre Tochter?</ta>
            <ta e="T737" id="Seg_3445" s="T731">– Ah, deshalb kennt sie offensichtlich Märchen.</ta>
            <ta e="T750" id="Seg_3446" s="T737">Sie kam irgendwann hierher und erzählte ein Märchen, ich denke, woher kennt sie das wohl?</ta>
            <ta e="T760" id="Seg_3447" s="T750">Nun, dann bist du da, von dir hat sie es wohl gehört und kennt es jetzt.</ta>
            <ta e="T792" id="Seg_3448" s="T789">– Hast du es nicht im Radio gehört?</ta>
            <ta e="T819" id="Seg_3449" s="T813">– Jedenfalls haben sie es erkannt.</ta>
            <ta e="T848" id="Seg_3450" s="T826">– Jelena Trifonovna, gibt es außer Ihnen noch jemanden, einen Mann oder eine Frau, der in Novorybnoe Märchen oder alte Lieder kennt?</ta>
            <ta e="T887" id="Seg_3451" s="T883">– Und andere Leute (…)?</ta>
            <ta e="T932" id="Seg_3452" s="T918">– Bei euch in Novorybnoe im Klub seid ihr vor Leuten aufgetreten und habt eure Märchen erzählt und gesungen?</ta>
            <ta e="T990" id="Seg_3453" s="T980">– Nur Ihre Kinder kennen wohl diese Märchen und Lieder.</ta>
            <ta e="T1029" id="Seg_3454" s="T1022">– Erzähl jetzt irgendein Märchen, dieses Märchen.</ta>
            <ta e="T1033" id="Seg_3455" s="T1029">Überhaupt das alles…</ta>
            <ta e="T1037" id="Seg_3456" s="T1033">Unser Gespräch wird bleiben.</ta>
            <ta e="T1041" id="Seg_3457" s="T1037">Damit die Kinder es später hören.</ta>
            <ta e="T1047" id="Seg_3458" s="T1042">– Erzähl irgendein Märchen.</ta>
            <ta e="T1053" id="Seg_3459" s="T1047">Ein anderes als was du eben erzählt hast.</ta>
            <ta e="T1068" id="Seg_3460" s="T1060">– Jelena Triforovna, Sie sagen, dass Sie noch Gedichte verfassen.</ta>
            <ta e="T1074" id="Seg_3461" s="T1068">Erzähl doch irgendein Gedicht.</ta>
         </annotation>
         <annotation name="fr" tierref="fr-AkEE" />
         <annotation name="ltr" tierref="ltr-AkEE">
            <ta e="T9" id="Seg_3462" s="T1">А: Елена Трифоновна, немного э–э расскажите вот о себе.</ta>
            <ta e="T15" id="Seg_3463" s="T9">Где проживаете, где родились, семья кто?</ta>
            <ta e="T24" id="Seg_3464" s="T15">А: О себе, о родителях, о детях что ..</ta>
            <ta e="T194" id="Seg_3465" s="T187">А: Ваша семья большая очень, оказывается.</ta>
            <ta e="T202" id="Seg_3466" s="T194">А вот дети ваших детей (внуки) у вас бывают да?</ta>
            <ta e="T291" id="Seg_3467" s="T277">А: Тогда для детей вы знаете много очень сказок и песни – старинные.</ta>
            <ta e="T299" id="Seg_3468" s="T291">А: Детям тоже рассказываете, наверно, им.. чтобы знали дети.</ta>
            <ta e="T355" id="Seg_3469" s="T348">А: Сами-то эти сказки, песни как узнали?</ta>
            <ta e="T517" id="Seg_3470" s="T506">А: Расскажите, как играют вот сейчас в игру, которую привезли?</ta>
            <ta e="T547" id="Seg_3471" s="T540">А: Вот, спой-ка какую-нибудь песню, ведь…</ta>
            <ta e="T558" id="Seg_3472" s="T547">А: Раньше говорили, девушки поют, парни поют, такие песни знаешь?</ta>
            <ta e="T610" id="Seg_3473" s="T608">А: Чья песня?</ta>
            <ta e="T641" id="Seg_3474" s="T621">А: Затем вот вы много этих знаете, кажется, и загадок, затем вот эти, перехват дыхания называются, это вот или соревнуются, или что?</ta>
            <ta e="T727" id="Seg_3475" s="T716">А: А вот в Новорыбном Марина зовут девушку Береговая, это не ваша дочь?</ta>
            <ta e="T737" id="Seg_3476" s="T731">А: А, поэтому сказки знает, оказывается.</ta>
            <ta e="T750" id="Seg_3477" s="T737">А: Как-то приезжала, рассказывала одну сказку, я думаю, откуда знать может.</ta>
            <ta e="T760" id="Seg_3478" s="T750">А: И вот, ты есть. От тебя услышав тогда знает, оказывается.</ta>
            <ta e="T792" id="Seg_3479" s="T789">А: Не слышала ты по радио?</ta>
            <ta e="T819" id="Seg_3480" s="T813">А: Всё равно вот узнают же ведь как.</ta>
            <ta e="T848" id="Seg_3481" s="T826">А: Елена Трифоновна, вот от вас кто-нибудь есть: или мужчина, или женщина – в Новорыбной кто знает и сказку, и старинную песню?</ta>
            <ta e="T887" id="Seg_3482" s="T883">A: Другие люди же ((…))?</ta>
            <ta e="T932" id="Seg_3483" s="T918">А: Тут у себя в Новорыбном в клубе вы перед людьми в выступлениях рассказывали или пели сказки?</ta>
            <ta e="T990" id="Seg_3484" s="T980">А: Ваши дети только знают наверно эти сказки да и песни.</ta>
            <ta e="T1029" id="Seg_3485" s="T1022">А: Сейчас какую-нибудь сказку расскажи, вот сказку.</ta>
            <ta e="T1033" id="Seg_3486" s="T1029">Вообще-то это всё….</ta>
            <ta e="T1037" id="Seg_3487" s="T1033">А: Разговор наш останется ведь.</ta>
            <ta e="T1041" id="Seg_3488" s="T1037">А: Потом, потом дети чтобы услышали.</ta>
            <ta e="T1047" id="Seg_3489" s="T1042">А: Расскажи какую-нибудь сказку.</ta>
            <ta e="T1053" id="Seg_3490" s="T1047">А: От той сказки которую рассказала другую.</ta>
            <ta e="T1068" id="Seg_3491" s="T1060">А: Елена Трифоновна, ещё вы стихи сочиняете говорите.</ta>
            <ta e="T1074" id="Seg_3492" s="T1068">А: Расскажите какой-нибудь стих.</ta>
         </annotation>
         <annotation name="nt" tierref="nt-AkEE" />
      </segmented-tier>
      <segmented-tier category="tx"
                      display-name="tx-UkET"
                      id="tx-UkET"
                      speaker="UkET"
                      type="t">
         <timeline-fork end="T657" start="T656">
            <tli id="T656.tx-UkET.1" />
         </timeline-fork>
         <timeline-fork end="T820" start="T819">
            <tli id="T819.tx-UkET.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-UkET">
            <ts e="T190" id="Seg_3493" n="sc" s="T21">
               <ts e="T43" id="Seg_3495" n="HIAT:u" s="T21">
                  <nts id="Seg_3496" n="HIAT:ip">–</nts>
                  <nts id="Seg_3497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_3499" n="HIAT:w" s="T21">Min</ts>
                  <nts id="Seg_3500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_3502" n="HIAT:w" s="T23">iː</ts>
                  <nts id="Seg_3503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_3505" n="HIAT:w" s="T25">bejem</ts>
                  <nts id="Seg_3506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_3508" n="HIAT:w" s="T26">töröːbüt</ts>
                  <nts id="Seg_3509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_3511" n="HIAT:w" s="T27">hirim</ts>
                  <nts id="Seg_3512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_3514" n="HIAT:w" s="T28">bi</ts>
                  <nts id="Seg_3515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_3517" n="HIAT:w" s="T29">di͡ek</ts>
                  <nts id="Seg_3518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_3520" n="HIAT:w" s="T30">ete</ts>
                  <nts id="Seg_3521" n="HIAT:ip">,</nts>
                  <nts id="Seg_3522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_3524" n="HIAT:w" s="T31">mu͡ora</ts>
                  <nts id="Seg_3525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_3527" n="HIAT:w" s="T32">di͡ek</ts>
                  <nts id="Seg_3528" n="HIAT:ip">,</nts>
                  <nts id="Seg_3529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_3531" n="HIAT:w" s="T33">ol</ts>
                  <nts id="Seg_3532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_3534" n="HIAT:w" s="T34">di͡ek</ts>
                  <nts id="Seg_3535" n="HIAT:ip">,</nts>
                  <nts id="Seg_3536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3537" n="HIAT:ip">(</nts>
                  <ts e="T36" id="Seg_3539" n="HIAT:w" s="T35">Nos-</ts>
                  <nts id="Seg_3540" n="HIAT:ip">)</nts>
                  <nts id="Seg_3541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_3543" n="HIAT:w" s="T36">kim</ts>
                  <nts id="Seg_3544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_3546" n="HIAT:w" s="T37">di͡ek</ts>
                  <nts id="Seg_3547" n="HIAT:ip">,</nts>
                  <nts id="Seg_3548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_3550" n="HIAT:w" s="T38">eː</ts>
                  <nts id="Seg_3551" n="HIAT:ip">,</nts>
                  <nts id="Seg_3552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_3554" n="HIAT:w" s="T39">onton</ts>
                  <nts id="Seg_3555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_3557" n="HIAT:w" s="T40">ɨrɨːmnaj</ts>
                  <nts id="Seg_3558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_3560" n="HIAT:w" s="T41">oŋu͡orgutugar</ts>
                  <nts id="Seg_3561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_3563" n="HIAT:w" s="T42">iti</ts>
                  <nts id="Seg_3564" n="HIAT:ip">.</nts>
                  <nts id="Seg_3565" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T52" id="Seg_3567" n="HIAT:u" s="T43">
                  <ts e="T44" id="Seg_3569" n="HIAT:w" s="T43">Mu͡ora</ts>
                  <nts id="Seg_3570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_3572" n="HIAT:w" s="T44">di͡ek</ts>
                  <nts id="Seg_3573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_3575" n="HIAT:w" s="T45">töröːbütüm</ts>
                  <nts id="Seg_3576" n="HIAT:ip">,</nts>
                  <nts id="Seg_3577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_3579" n="HIAT:w" s="T46">onton</ts>
                  <nts id="Seg_3580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_3582" n="HIAT:w" s="T47">erge</ts>
                  <nts id="Seg_3583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_3585" n="HIAT:w" s="T48">barammɨn</ts>
                  <nts id="Seg_3586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_3588" n="HIAT:w" s="T49">taːs</ts>
                  <nts id="Seg_3589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_3591" n="HIAT:w" s="T50">di͡ek</ts>
                  <nts id="Seg_3592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_3594" n="HIAT:w" s="T51">hirdemmitim</ts>
                  <nts id="Seg_3595" n="HIAT:ip">.</nts>
                  <nts id="Seg_3596" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T55" id="Seg_3598" n="HIAT:u" s="T52">
                  <ts e="T53" id="Seg_3600" n="HIAT:w" s="T52">Ogolorum</ts>
                  <nts id="Seg_3601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_3603" n="HIAT:w" s="T53">da</ts>
                  <nts id="Seg_3604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_3606" n="HIAT:w" s="T54">tu͡ok</ts>
                  <nts id="Seg_3607" n="HIAT:ip">…</nts>
                  <nts id="Seg_3608" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T68" id="Seg_3610" n="HIAT:u" s="T55">
                  <ts e="T56" id="Seg_3612" n="HIAT:w" s="T55">Kajdak</ts>
                  <nts id="Seg_3613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_3615" n="HIAT:w" s="T56">kepseːn</ts>
                  <nts id="Seg_3616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_3618" n="HIAT:w" s="T57">da</ts>
                  <nts id="Seg_3619" n="HIAT:ip">,</nts>
                  <nts id="Seg_3620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_3622" n="HIAT:w" s="T58">tugu</ts>
                  <nts id="Seg_3623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_3625" n="HIAT:w" s="T59">kepsi͡emij</ts>
                  <nts id="Seg_3626" n="HIAT:ip">,</nts>
                  <nts id="Seg_3627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_3629" n="HIAT:w" s="T60">biːr</ts>
                  <nts id="Seg_3630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_3632" n="HIAT:w" s="T61">ogom</ts>
                  <nts id="Seg_3633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_3635" n="HIAT:w" s="T62">iti</ts>
                  <nts id="Seg_3636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_3638" n="HIAT:w" s="T63">ušku͡olaga</ts>
                  <nts id="Seg_3639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_3641" n="HIAT:w" s="T64">üleliːr</ts>
                  <nts id="Seg_3642" n="HIAT:ip">,</nts>
                  <nts id="Seg_3643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_3645" n="HIAT:w" s="T65">biːr</ts>
                  <nts id="Seg_3646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_3648" n="HIAT:w" s="T66">kɨːhɨm</ts>
                  <nts id="Seg_3649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_3651" n="HIAT:w" s="T67">dʼirʼektardiːr</ts>
                  <nts id="Seg_3652" n="HIAT:ip">.</nts>
                  <nts id="Seg_3653" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T81" id="Seg_3655" n="HIAT:u" s="T68">
                  <ts e="T69" id="Seg_3657" n="HIAT:w" s="T68">Biːr</ts>
                  <nts id="Seg_3658" n="HIAT:ip">,</nts>
                  <nts id="Seg_3659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_3661" n="HIAT:w" s="T69">e</ts>
                  <nts id="Seg_3662" n="HIAT:ip">,</nts>
                  <nts id="Seg_3663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_3665" n="HIAT:w" s="T70">biːr</ts>
                  <nts id="Seg_3666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_3668" n="HIAT:w" s="T71">ogom</ts>
                  <nts id="Seg_3669" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_3671" n="HIAT:w" s="T72">iti</ts>
                  <nts id="Seg_3672" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_3674" n="HIAT:w" s="T73">Nosku͡oga</ts>
                  <nts id="Seg_3675" n="HIAT:ip">,</nts>
                  <nts id="Seg_3676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3677" n="HIAT:ip">(</nts>
                  <ts e="T75" id="Seg_3679" n="HIAT:w" s="T74">ula-</ts>
                  <nts id="Seg_3680" n="HIAT:ip">)</nts>
                  <nts id="Seg_3681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_3683" n="HIAT:w" s="T75">muŋ</ts>
                  <nts id="Seg_3684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_3686" n="HIAT:w" s="T76">ulakan</ts>
                  <nts id="Seg_3687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_3689" n="HIAT:w" s="T77">kɨːhɨm</ts>
                  <nts id="Seg_3690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_3692" n="HIAT:w" s="T78">Nosku͡oga</ts>
                  <nts id="Seg_3693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_3695" n="HIAT:w" s="T79">ušku͡olaga</ts>
                  <nts id="Seg_3696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_3698" n="HIAT:w" s="T80">doktuːrduːr</ts>
                  <nts id="Seg_3699" n="HIAT:ip">.</nts>
                  <nts id="Seg_3700" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T82" id="Seg_3702" n="HIAT:u" s="T81">
                  <ts e="T82" id="Seg_3704" n="HIAT:w" s="T81">Nʼiːna</ts>
                  <nts id="Seg_3705" n="HIAT:ip">.</nts>
                  <nts id="Seg_3706" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T87" id="Seg_3708" n="HIAT:u" s="T82">
                  <nts id="Seg_3709" n="HIAT:ip">(</nts>
                  <ts e="T83" id="Seg_3711" n="HIAT:w" s="T82">Biː-</ts>
                  <nts id="Seg_3712" n="HIAT:ip">)</nts>
                  <nts id="Seg_3713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_3715" n="HIAT:w" s="T83">ol</ts>
                  <nts id="Seg_3716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_3718" n="HIAT:w" s="T84">ulakan</ts>
                  <nts id="Seg_3719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_3721" n="HIAT:w" s="T85">kɨːhɨm</ts>
                  <nts id="Seg_3722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_3724" n="HIAT:w" s="T86">aːta</ts>
                  <nts id="Seg_3725" n="HIAT:ip">.</nts>
                  <nts id="Seg_3726" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T91" id="Seg_3728" n="HIAT:u" s="T87">
                  <ts e="T88" id="Seg_3730" n="HIAT:w" s="T87">Kuččuguj</ts>
                  <nts id="Seg_3731" n="HIAT:ip">,</nts>
                  <nts id="Seg_3732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_3734" n="HIAT:w" s="T88">ontum</ts>
                  <nts id="Seg_3735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_3737" n="HIAT:w" s="T89">annɨta</ts>
                  <nts id="Seg_3738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_3740" n="HIAT:w" s="T90">Keːtʼe</ts>
                  <nts id="Seg_3741" n="HIAT:ip">.</nts>
                  <nts id="Seg_3742" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T94" id="Seg_3744" n="HIAT:u" s="T91">
                  <ts e="T92" id="Seg_3746" n="HIAT:w" s="T91">Ol</ts>
                  <nts id="Seg_3747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_3749" n="HIAT:w" s="T92">ušku͡olaga</ts>
                  <nts id="Seg_3750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_3752" n="HIAT:w" s="T93">dʼirʼekterdiːr</ts>
                  <nts id="Seg_3753" n="HIAT:ip">.</nts>
                  <nts id="Seg_3754" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T109" id="Seg_3756" n="HIAT:u" s="T94">
                  <ts e="T95" id="Seg_3758" n="HIAT:w" s="T94">Biːr</ts>
                  <nts id="Seg_3759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_3761" n="HIAT:w" s="T95">kɨːhɨm</ts>
                  <nts id="Seg_3762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_3764" n="HIAT:w" s="T96">haːdikka</ts>
                  <nts id="Seg_3765" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_3767" n="HIAT:w" s="T97">üleleːčči</ts>
                  <nts id="Seg_3768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_3770" n="HIAT:w" s="T98">ete</ts>
                  <nts id="Seg_3771" n="HIAT:ip">,</nts>
                  <nts id="Seg_3772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_3774" n="HIAT:w" s="T99">ontum</ts>
                  <nts id="Seg_3775" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_3777" n="HIAT:w" s="T100">eː</ts>
                  <nts id="Seg_3778" n="HIAT:ip">,</nts>
                  <nts id="Seg_3779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_3781" n="HIAT:w" s="T101">bu</ts>
                  <nts id="Seg_3782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_3784" n="HIAT:w" s="T102">haːdiktara</ts>
                  <nts id="Seg_3785" n="HIAT:ip">,</nts>
                  <nts id="Seg_3786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_3788" n="HIAT:w" s="T103">tu͡oktara</ts>
                  <nts id="Seg_3789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_3791" n="HIAT:w" s="T104">aldʼanan</ts>
                  <nts id="Seg_3792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_3794" n="HIAT:w" s="T105">anaːn</ts>
                  <nts id="Seg_3795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_3797" n="HIAT:w" s="T106">da</ts>
                  <nts id="Seg_3798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_3800" n="HIAT:w" s="T107">ogolonon</ts>
                  <nts id="Seg_3801" n="HIAT:ip">,</nts>
                  <nts id="Seg_3802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_3804" n="HIAT:w" s="T108">kanʼaːn</ts>
                  <nts id="Seg_3805" n="HIAT:ip">.</nts>
                  <nts id="Seg_3806" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T120" id="Seg_3808" n="HIAT:u" s="T109">
                  <ts e="T110" id="Seg_3810" n="HIAT:w" s="T109">Anɨ</ts>
                  <nts id="Seg_3811" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_3813" n="HIAT:w" s="T110">ɨ͡aldʼan</ts>
                  <nts id="Seg_3814" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_3816" n="HIAT:w" s="T111">Dudʼinka</ts>
                  <nts id="Seg_3817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_3819" n="HIAT:w" s="T112">di͡ek</ts>
                  <nts id="Seg_3820" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_3822" n="HIAT:w" s="T113">hɨldʼar</ts>
                  <nts id="Seg_3823" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_3825" n="HIAT:w" s="T114">bu</ts>
                  <nts id="Seg_3826" n="HIAT:ip">,</nts>
                  <nts id="Seg_3827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_3829" n="HIAT:w" s="T115">eː</ts>
                  <nts id="Seg_3830" n="HIAT:ip">,</nts>
                  <nts id="Seg_3831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_3833" n="HIAT:w" s="T116">Narilʼskajga</ts>
                  <nts id="Seg_3834" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_3836" n="HIAT:w" s="T117">emtete</ts>
                  <nts id="Seg_3837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_3839" n="HIAT:w" s="T118">hɨtar</ts>
                  <nts id="Seg_3840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_3842" n="HIAT:w" s="T119">anɨ</ts>
                  <nts id="Seg_3843" n="HIAT:ip">.</nts>
                  <nts id="Seg_3844" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T125" id="Seg_3846" n="HIAT:u" s="T120">
                  <ts e="T121" id="Seg_3848" n="HIAT:w" s="T120">Onton</ts>
                  <nts id="Seg_3849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_3851" n="HIAT:w" s="T121">biːr</ts>
                  <nts id="Seg_3852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_3854" n="HIAT:w" s="T122">u͡olum</ts>
                  <nts id="Seg_3855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_3857" n="HIAT:w" s="T123">axoːtnʼik</ts>
                  <nts id="Seg_3858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_3860" n="HIAT:w" s="T124">bu͡olla</ts>
                  <nts id="Seg_3861" n="HIAT:ip">.</nts>
                  <nts id="Seg_3862" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T139" id="Seg_3864" n="HIAT:u" s="T125">
                  <ts e="T126" id="Seg_3866" n="HIAT:w" s="T125">Biːr</ts>
                  <nts id="Seg_3867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_3869" n="HIAT:w" s="T126">u͡olum</ts>
                  <nts id="Seg_3870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_3872" n="HIAT:w" s="T127">iti</ts>
                  <nts id="Seg_3873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_3875" n="HIAT:w" s="T128">hamalʼottarɨ</ts>
                  <nts id="Seg_3876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3877" n="HIAT:ip">(</nts>
                  <ts e="T130" id="Seg_3879" n="HIAT:w" s="T129">ɨːk-</ts>
                  <nts id="Seg_3880" n="HIAT:ip">)</nts>
                  <nts id="Seg_3881" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_3883" n="HIAT:w" s="T130">ittelerin</ts>
                  <nts id="Seg_3884" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3885" n="HIAT:ip">(</nts>
                  <ts e="T132" id="Seg_3887" n="HIAT:w" s="T131">ɨːt-</ts>
                  <nts id="Seg_3888" n="HIAT:ip">)</nts>
                  <nts id="Seg_3889" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_3891" n="HIAT:w" s="T132">tu͡oktuːr</ts>
                  <nts id="Seg_3892" n="HIAT:ip">,</nts>
                  <nts id="Seg_3893" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_3895" n="HIAT:w" s="T133">oŋoror</ts>
                  <nts id="Seg_3896" n="HIAT:ip">,</nts>
                  <nts id="Seg_3897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_3899" n="HIAT:w" s="T134">hamalʼottarɨ</ts>
                  <nts id="Seg_3900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_3902" n="HIAT:w" s="T135">iːter</ts>
                  <nts id="Seg_3903" n="HIAT:ip">,</nts>
                  <nts id="Seg_3904" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_3906" n="HIAT:w" s="T136">kötüter</ts>
                  <nts id="Seg_3907" n="HIAT:ip">,</nts>
                  <nts id="Seg_3908" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_3910" n="HIAT:w" s="T137">itinne</ts>
                  <nts id="Seg_3911" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_3913" n="HIAT:w" s="T138">baːr</ts>
                  <nts id="Seg_3914" n="HIAT:ip">.</nts>
                  <nts id="Seg_3915" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T155" id="Seg_3917" n="HIAT:u" s="T139">
                  <ts e="T140" id="Seg_3919" n="HIAT:w" s="T139">Biːr</ts>
                  <nts id="Seg_3920" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_3922" n="HIAT:w" s="T140">kɨːhɨm</ts>
                  <nts id="Seg_3923" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3924" n="HIAT:ip">(</nts>
                  <ts e="T142" id="Seg_3926" n="HIAT:w" s="T141">ini-</ts>
                  <nts id="Seg_3927" n="HIAT:ip">)</nts>
                  <nts id="Seg_3928" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_3930" n="HIAT:w" s="T142">tuːgu</ts>
                  <nts id="Seg_3931" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_3933" n="HIAT:w" s="T143">büppüt</ts>
                  <nts id="Seg_3934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_3936" n="HIAT:w" s="T144">ete</ts>
                  <nts id="Seg_3937" n="HIAT:ip">,</nts>
                  <nts id="Seg_3938" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_3940" n="HIAT:w" s="T145">haːdik</ts>
                  <nts id="Seg_3941" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_3943" n="HIAT:w" s="T146">ü͡öregin</ts>
                  <nts id="Seg_3944" n="HIAT:ip">,</nts>
                  <nts id="Seg_3945" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_3947" n="HIAT:w" s="T147">anɨ</ts>
                  <nts id="Seg_3948" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_3950" n="HIAT:w" s="T148">bejem</ts>
                  <nts id="Seg_3951" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_3953" n="HIAT:w" s="T149">kɨrdʼammɨn</ts>
                  <nts id="Seg_3954" n="HIAT:ip">,</nts>
                  <nts id="Seg_3955" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_3957" n="HIAT:w" s="T150">ontubun</ts>
                  <nts id="Seg_3958" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_3960" n="HIAT:w" s="T151">bejeber</ts>
                  <nts id="Seg_3961" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_3963" n="HIAT:w" s="T152">üleleteːribin</ts>
                  <nts id="Seg_3964" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_3966" n="HIAT:w" s="T153">tɨ͡aga</ts>
                  <nts id="Seg_3967" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_3969" n="HIAT:w" s="T154">tutabɨn</ts>
                  <nts id="Seg_3970" n="HIAT:ip">.</nts>
                  <nts id="Seg_3971" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T169" id="Seg_3973" n="HIAT:u" s="T155">
                  <ts e="T156" id="Seg_3975" n="HIAT:w" s="T155">Ogonnʼorum</ts>
                  <nts id="Seg_3976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_3978" n="HIAT:w" s="T156">urut</ts>
                  <nts id="Seg_3979" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_3981" n="HIAT:w" s="T157">bulčut</ts>
                  <nts id="Seg_3982" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_3984" n="HIAT:w" s="T158">ete</ts>
                  <nts id="Seg_3985" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_3987" n="HIAT:w" s="T159">hin</ts>
                  <nts id="Seg_3988" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3989" n="HIAT:ip">(</nts>
                  <ts e="T161" id="Seg_3991" n="HIAT:w" s="T160">kihi͡e-</ts>
                  <nts id="Seg_3992" n="HIAT:ip">)</nts>
                  <nts id="Seg_3993" n="HIAT:ip">,</nts>
                  <nts id="Seg_3994" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_3996" n="HIAT:w" s="T161">kihitten</ts>
                  <nts id="Seg_3997" n="HIAT:ip">,</nts>
                  <nts id="Seg_3998" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_4000" n="HIAT:w" s="T162">eː</ts>
                  <nts id="Seg_4001" n="HIAT:ip">,</nts>
                  <nts id="Seg_4002" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_4004" n="HIAT:w" s="T163">ɨrɨmnajga</ts>
                  <nts id="Seg_4005" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_4007" n="HIAT:w" s="T164">otton</ts>
                  <nts id="Seg_4008" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_4010" n="HIAT:w" s="T165">hapku͡oska</ts>
                  <nts id="Seg_4011" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_4013" n="HIAT:w" s="T166">peredavojɨnan</ts>
                  <nts id="Seg_4014" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_4016" n="HIAT:w" s="T167">bultaːččɨ</ts>
                  <nts id="Seg_4017" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_4019" n="HIAT:w" s="T168">ete</ts>
                  <nts id="Seg_4020" n="HIAT:ip">.</nts>
                  <nts id="Seg_4021" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T183" id="Seg_4023" n="HIAT:u" s="T169">
                  <ts e="T170" id="Seg_4025" n="HIAT:w" s="T169">Anɨ</ts>
                  <nts id="Seg_4026" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_4028" n="HIAT:w" s="T170">ɨ͡aldʼan</ts>
                  <nts id="Seg_4029" n="HIAT:ip">,</nts>
                  <nts id="Seg_4030" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_4032" n="HIAT:w" s="T171">iliːte</ts>
                  <nts id="Seg_4033" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_4035" n="HIAT:w" s="T172">ɨ͡aldʼan</ts>
                  <nts id="Seg_4036" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_4038" n="HIAT:w" s="T173">ol</ts>
                  <nts id="Seg_4039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_4041" n="HIAT:w" s="T174">bulduttan</ts>
                  <nts id="Seg_4042" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_4044" n="HIAT:w" s="T175">da</ts>
                  <nts id="Seg_4045" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_4047" n="HIAT:w" s="T176">tagɨsta</ts>
                  <nts id="Seg_4048" n="HIAT:ip">,</nts>
                  <nts id="Seg_4049" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_4051" n="HIAT:w" s="T177">onuga</ts>
                  <nts id="Seg_4052" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_4054" n="HIAT:w" s="T178">ebiːleːn</ts>
                  <nts id="Seg_4055" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_4057" n="HIAT:w" s="T179">anɨ</ts>
                  <nts id="Seg_4058" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4059" n="HIAT:ip">(</nts>
                  <ts e="T181" id="Seg_4061" n="HIAT:w" s="T180">aːktan</ts>
                  <nts id="Seg_4062" n="HIAT:ip">)</nts>
                  <nts id="Seg_4063" n="HIAT:ip">,</nts>
                  <nts id="Seg_4064" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_4066" n="HIAT:w" s="T181">pʼensʼijaga</ts>
                  <nts id="Seg_4067" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_4069" n="HIAT:w" s="T182">kiːrde</ts>
                  <nts id="Seg_4070" n="HIAT:ip">.</nts>
                  <nts id="Seg_4071" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T190" id="Seg_4073" n="HIAT:u" s="T183">
                  <ts e="T184" id="Seg_4075" n="HIAT:w" s="T183">Bu</ts>
                  <nts id="Seg_4076" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_4078" n="HIAT:w" s="T184">anɨ</ts>
                  <nts id="Seg_4079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_4081" n="HIAT:w" s="T185">bu</ts>
                  <nts id="Seg_4082" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_4084" n="HIAT:w" s="T186">dʼɨllarga</ts>
                  <nts id="Seg_4085" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_4087" n="HIAT:w" s="T188">kiːrbite</ts>
                  <nts id="Seg_4088" n="HIAT:ip">.</nts>
                  <nts id="Seg_4089" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T277" id="Seg_4090" n="sc" s="T202">
               <ts e="T209" id="Seg_4092" n="HIAT:u" s="T202">
                  <nts id="Seg_4093" n="HIAT:ip">–</nts>
                  <nts id="Seg_4094" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_4096" n="HIAT:w" s="T202">Heː</ts>
                  <nts id="Seg_4097" n="HIAT:ip">,</nts>
                  <nts id="Seg_4098" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_4100" n="HIAT:w" s="T203">anɨ</ts>
                  <nts id="Seg_4101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_4103" n="HIAT:w" s="T204">bu</ts>
                  <nts id="Seg_4104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_4106" n="HIAT:w" s="T205">ogolorum</ts>
                  <nts id="Seg_4107" n="HIAT:ip">,</nts>
                  <nts id="Seg_4108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_4110" n="HIAT:w" s="T206">ogom</ts>
                  <nts id="Seg_4111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_4113" n="HIAT:w" s="T207">ogolorun</ts>
                  <nts id="Seg_4114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_4116" n="HIAT:w" s="T208">karajammɨn</ts>
                  <nts id="Seg_4117" n="HIAT:ip">.</nts>
                  <nts id="Seg_4118" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T226" id="Seg_4120" n="HIAT:u" s="T209">
                  <ts e="T210" id="Seg_4122" n="HIAT:w" s="T209">Ol</ts>
                  <nts id="Seg_4123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_4125" n="HIAT:w" s="T210">emŋe</ts>
                  <nts id="Seg_4126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_4128" n="HIAT:w" s="T211">keleːčči</ts>
                  <nts id="Seg_4129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_4131" n="HIAT:w" s="T212">ogom</ts>
                  <nts id="Seg_4132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_4134" n="HIAT:w" s="T213">gi͡ene</ts>
                  <nts id="Seg_4135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_4137" n="HIAT:w" s="T214">üs</ts>
                  <nts id="Seg_4138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_4140" n="HIAT:w" s="T215">ogoloːk</ts>
                  <nts id="Seg_4141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_4143" n="HIAT:w" s="T216">bu͡olla</ts>
                  <nts id="Seg_4144" n="HIAT:ip">,</nts>
                  <nts id="Seg_4145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_4147" n="HIAT:w" s="T217">ontularɨm</ts>
                  <nts id="Seg_4148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_4150" n="HIAT:w" s="T218">ogotun</ts>
                  <nts id="Seg_4151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_4153" n="HIAT:w" s="T219">karaja</ts>
                  <nts id="Seg_4154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_4156" n="HIAT:w" s="T220">hɨtar</ts>
                  <nts id="Seg_4157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_4159" n="HIAT:w" s="T221">etim</ts>
                  <nts id="Seg_4160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_4162" n="HIAT:w" s="T222">bu͡o</ts>
                  <nts id="Seg_4163" n="HIAT:ip">,</nts>
                  <nts id="Seg_4164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_4166" n="HIAT:w" s="T223">anɨ</ts>
                  <nts id="Seg_4167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_4169" n="HIAT:w" s="T224">bu͡ollagɨna</ts>
                  <nts id="Seg_4170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_4172" n="HIAT:w" s="T225">bu</ts>
                  <nts id="Seg_4173" n="HIAT:ip">.</nts>
                  <nts id="Seg_4174" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T233" id="Seg_4176" n="HIAT:u" s="T226">
                  <ts e="T227" id="Seg_4178" n="HIAT:w" s="T226">Ogom</ts>
                  <nts id="Seg_4179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_4181" n="HIAT:w" s="T227">ol</ts>
                  <nts id="Seg_4182" n="HIAT:ip">,</nts>
                  <nts id="Seg_4183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_4185" n="HIAT:w" s="T228">Maːčam</ts>
                  <nts id="Seg_4186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_4188" n="HIAT:w" s="T229">kelen</ts>
                  <nts id="Seg_4189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_4191" n="HIAT:w" s="T230">kellim</ts>
                  <nts id="Seg_4192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_4194" n="HIAT:w" s="T231">ol</ts>
                  <nts id="Seg_4195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_4197" n="HIAT:w" s="T232">tɨ͡attan</ts>
                  <nts id="Seg_4198" n="HIAT:ip">.</nts>
                  <nts id="Seg_4199" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T251" id="Seg_4201" n="HIAT:u" s="T233">
                  <ts e="T234" id="Seg_4203" n="HIAT:w" s="T233">Tɨ͡aga</ts>
                  <nts id="Seg_4204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_4206" n="HIAT:w" s="T234">tuta</ts>
                  <nts id="Seg_4207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_4209" n="HIAT:w" s="T235">hɨtabɨn</ts>
                  <nts id="Seg_4210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_4212" n="HIAT:w" s="T236">bu͡olla</ts>
                  <nts id="Seg_4213" n="HIAT:ip">,</nts>
                  <nts id="Seg_4214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_4216" n="HIAT:w" s="T237">ol</ts>
                  <nts id="Seg_4217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_4219" n="HIAT:w" s="T238">üs</ts>
                  <nts id="Seg_4220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_4222" n="HIAT:w" s="T239">ogobun</ts>
                  <nts id="Seg_4223" n="HIAT:ip">,</nts>
                  <nts id="Seg_4224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_4226" n="HIAT:w" s="T240">ol</ts>
                  <nts id="Seg_4227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_4229" n="HIAT:w" s="T241">maːmata</ts>
                  <nts id="Seg_4230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_4232" n="HIAT:w" s="T242">bu</ts>
                  <nts id="Seg_4233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_4235" n="HIAT:w" s="T243">di͡ek</ts>
                  <nts id="Seg_4236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_4238" n="HIAT:w" s="T244">baːr</ts>
                  <nts id="Seg_4239" n="HIAT:ip">,</nts>
                  <nts id="Seg_4240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_4242" n="HIAT:w" s="T245">teːtelere</ts>
                  <nts id="Seg_4243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_4245" n="HIAT:w" s="T246">arakpɨta</ts>
                  <nts id="Seg_4246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_4248" n="HIAT:w" s="T247">bu͡olla</ts>
                  <nts id="Seg_4249" n="HIAT:ip">,</nts>
                  <nts id="Seg_4250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_4252" n="HIAT:w" s="T248">ol</ts>
                  <nts id="Seg_4253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_4255" n="HIAT:w" s="T249">dʼuraːk</ts>
                  <nts id="Seg_4256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_4258" n="HIAT:w" s="T250">ete</ts>
                  <nts id="Seg_4259" n="HIAT:ip">.</nts>
                  <nts id="Seg_4260" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T253" id="Seg_4262" n="HIAT:u" s="T251">
                  <ts e="T252" id="Seg_4264" n="HIAT:w" s="T251">Ontuta</ts>
                  <nts id="Seg_4265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_4267" n="HIAT:w" s="T252">arakpɨta</ts>
                  <nts id="Seg_4268" n="HIAT:ip">.</nts>
                  <nts id="Seg_4269" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T273" id="Seg_4271" n="HIAT:u" s="T253">
                  <ts e="T254" id="Seg_4273" n="HIAT:w" s="T253">Ol</ts>
                  <nts id="Seg_4274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_4276" n="HIAT:w" s="T254">ihin</ts>
                  <nts id="Seg_4277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_4279" n="HIAT:w" s="T255">ol</ts>
                  <nts id="Seg_4280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_4282" n="HIAT:w" s="T256">kantan</ts>
                  <nts id="Seg_4283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_4285" n="HIAT:w" s="T257">biːrten</ts>
                  <nts id="Seg_4286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_4288" n="HIAT:w" s="T258">biːr</ts>
                  <nts id="Seg_4289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_4291" n="HIAT:w" s="T259">ogo</ts>
                  <nts id="Seg_4292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_4294" n="HIAT:w" s="T260">bu͡olla</ts>
                  <nts id="Seg_4295" n="HIAT:ip">,</nts>
                  <nts id="Seg_4296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_4298" n="HIAT:w" s="T261">ogolorum</ts>
                  <nts id="Seg_4299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_4301" n="HIAT:w" s="T262">ogoloro</ts>
                  <nts id="Seg_4302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_4304" n="HIAT:w" s="T263">kaččaga</ts>
                  <nts id="Seg_4305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_4307" n="HIAT:w" s="T264">da</ts>
                  <nts id="Seg_4308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_4310" n="HIAT:w" s="T265">arappattar</ts>
                  <nts id="Seg_4311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_4313" n="HIAT:w" s="T266">bu͡o</ts>
                  <nts id="Seg_4314" n="HIAT:ip">,</nts>
                  <nts id="Seg_4315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_4317" n="HIAT:w" s="T267">hajɨn</ts>
                  <nts id="Seg_4318" n="HIAT:ip">,</nts>
                  <nts id="Seg_4319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_4321" n="HIAT:w" s="T268">hajɨnnarɨ</ts>
                  <nts id="Seg_4322" n="HIAT:ip">,</nts>
                  <nts id="Seg_4323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_4325" n="HIAT:w" s="T269">kɨhɨnnarɨ</ts>
                  <nts id="Seg_4326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_4328" n="HIAT:w" s="T270">huptu</ts>
                  <nts id="Seg_4329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_4331" n="HIAT:w" s="T271">ogoloːk</ts>
                  <nts id="Seg_4332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_4334" n="HIAT:w" s="T272">bu͡olaːččɨbɨn</ts>
                  <nts id="Seg_4335" n="HIAT:ip">.</nts>
                  <nts id="Seg_4336" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T277" id="Seg_4338" n="HIAT:u" s="T273">
                  <ts e="T274" id="Seg_4340" n="HIAT:w" s="T273">Vnuktarɨm</ts>
                  <nts id="Seg_4341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_4343" n="HIAT:w" s="T274">bi͡ek</ts>
                  <nts id="Seg_4344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_4346" n="HIAT:w" s="T275">baːr</ts>
                  <nts id="Seg_4347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_4349" n="HIAT:w" s="T276">bu͡olallar</ts>
                  <nts id="Seg_4350" n="HIAT:ip">.</nts>
                  <nts id="Seg_4351" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T348" id="Seg_4352" n="sc" s="T299">
               <ts e="T315" id="Seg_4354" n="HIAT:u" s="T299">
                  <nts id="Seg_4355" n="HIAT:ip">–</nts>
                  <nts id="Seg_4356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_4358" n="HIAT:w" s="T299">Heː</ts>
                  <nts id="Seg_4359" n="HIAT:ip">,</nts>
                  <nts id="Seg_4360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_4362" n="HIAT:w" s="T300">kepseːččibit</ts>
                  <nts id="Seg_4363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_4365" n="HIAT:w" s="T301">ittekeːni</ts>
                  <nts id="Seg_4366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_4368" n="HIAT:w" s="T302">ki͡e</ts>
                  <nts id="Seg_4369" n="HIAT:ip">,</nts>
                  <nts id="Seg_4370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_4372" n="HIAT:w" s="T303">anɨ</ts>
                  <nts id="Seg_4373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_4375" n="HIAT:w" s="T304">ulakannɨk</ts>
                  <nts id="Seg_4376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_4378" n="HIAT:w" s="T305">da</ts>
                  <nts id="Seg_4379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_4381" n="HIAT:w" s="T306">tu͡olkulana</ts>
                  <nts id="Seg_4382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_4384" n="HIAT:w" s="T307">ilikter</ts>
                  <nts id="Seg_4385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_4387" n="HIAT:w" s="T308">eːt</ts>
                  <nts id="Seg_4388" n="HIAT:ip">,</nts>
                  <nts id="Seg_4389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_4391" n="HIAT:w" s="T309">ulakan</ts>
                  <nts id="Seg_4392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_4394" n="HIAT:w" s="T310">kɨːhɨm</ts>
                  <nts id="Seg_4395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_4397" n="HIAT:w" s="T311">ere</ts>
                  <nts id="Seg_4398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_4400" n="HIAT:w" s="T312">ogoloro</ts>
                  <nts id="Seg_4401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_4403" n="HIAT:w" s="T313">usku͡olaga</ts>
                  <nts id="Seg_4404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_4406" n="HIAT:w" s="T314">barbɨttara</ts>
                  <nts id="Seg_4407" n="HIAT:ip">.</nts>
                  <nts id="Seg_4408" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T321" id="Seg_4410" n="HIAT:u" s="T315">
                  <ts e="T316" id="Seg_4412" n="HIAT:w" s="T315">Kuččuguj</ts>
                  <nts id="Seg_4413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_4415" n="HIAT:w" s="T316">ogolorum</ts>
                  <nts id="Seg_4416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_4418" n="HIAT:w" s="T317">ogoloro</ts>
                  <nts id="Seg_4419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_4421" n="HIAT:w" s="T318">kiːre</ts>
                  <nts id="Seg_4422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_4424" n="HIAT:w" s="T319">ilikter</ts>
                  <nts id="Seg_4425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_4427" n="HIAT:w" s="T320">usku͡olaga</ts>
                  <nts id="Seg_4428" n="HIAT:ip">.</nts>
                  <nts id="Seg_4429" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T340" id="Seg_4431" n="HIAT:u" s="T321">
                  <ts e="T322" id="Seg_4433" n="HIAT:w" s="T321">Olorgo</ts>
                  <nts id="Seg_4434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_4436" n="HIAT:w" s="T322">ol</ts>
                  <nts id="Seg_4437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_4439" n="HIAT:w" s="T323">ittekeːni</ts>
                  <nts id="Seg_4440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_4442" n="HIAT:w" s="T324">kepsiːbin</ts>
                  <nts id="Seg_4443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_4445" n="HIAT:w" s="T325">bu͡o</ts>
                  <nts id="Seg_4446" n="HIAT:ip">,</nts>
                  <nts id="Seg_4447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_4449" n="HIAT:w" s="T326">itteni-bütteni</ts>
                  <nts id="Seg_4450" n="HIAT:ip">,</nts>
                  <nts id="Seg_4451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4452" n="HIAT:ip">"</nts>
                  <ts e="T328" id="Seg_4454" n="HIAT:w" s="T327">ebeː</ts>
                  <nts id="Seg_4455" n="HIAT:ip">"</nts>
                  <nts id="Seg_4456" n="HIAT:ip">,</nts>
                  <nts id="Seg_4457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_4459" n="HIAT:w" s="T328">di͡en</ts>
                  <nts id="Seg_4460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_4462" n="HIAT:w" s="T329">ergiččije</ts>
                  <nts id="Seg_4463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_4465" n="HIAT:w" s="T330">hɨldʼaːččɨlar</ts>
                  <nts id="Seg_4466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_4468" n="HIAT:w" s="T331">bu͡o</ts>
                  <nts id="Seg_4469" n="HIAT:ip">,</nts>
                  <nts id="Seg_4470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4471" n="HIAT:ip">"</nts>
                  <ts e="T333" id="Seg_4473" n="HIAT:w" s="T332">ontugun</ts>
                  <nts id="Seg_4474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_4476" n="HIAT:w" s="T333">kepseː</ts>
                  <nts id="Seg_4477" n="HIAT:ip">,</nts>
                  <nts id="Seg_4478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_4480" n="HIAT:w" s="T334">onu</ts>
                  <nts id="Seg_4481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_4483" n="HIAT:w" s="T335">oloŋkoloː</ts>
                  <nts id="Seg_4484" n="HIAT:ip">,</nts>
                  <nts id="Seg_4485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_4487" n="HIAT:w" s="T336">munu</ts>
                  <nts id="Seg_4488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_4490" n="HIAT:w" s="T337">kepseː</ts>
                  <nts id="Seg_4491" n="HIAT:ip">,</nts>
                  <nts id="Seg_4492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_4494" n="HIAT:w" s="T338">itini</ts>
                  <nts id="Seg_4495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_4497" n="HIAT:w" s="T339">ɨllaː</ts>
                  <nts id="Seg_4498" n="HIAT:ip">"</nts>
                  <nts id="Seg_4499" n="HIAT:ip">.</nts>
                  <nts id="Seg_4500" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T348" id="Seg_4502" n="HIAT:u" s="T340">
                  <ts e="T341" id="Seg_4504" n="HIAT:w" s="T340">Oloru</ts>
                  <nts id="Seg_4505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_4507" n="HIAT:w" s="T341">haːtataːrɨgɨn</ts>
                  <nts id="Seg_4508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_4510" n="HIAT:w" s="T342">ke</ts>
                  <nts id="Seg_4511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_4513" n="HIAT:w" s="T343">ɨllanɨŋ</ts>
                  <nts id="Seg_4514" n="HIAT:ip">,</nts>
                  <nts id="Seg_4515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_4517" n="HIAT:w" s="T344">kepsetiŋ</ts>
                  <nts id="Seg_4518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4519" n="HIAT:ip">–</nts>
                  <nts id="Seg_4520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_4522" n="HIAT:w" s="T345">hin</ts>
                  <nts id="Seg_4523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_4525" n="HIAT:w" s="T346">kojuː</ts>
                  <nts id="Seg_4526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_4528" n="HIAT:w" s="T347">bu͡olla</ts>
                  <nts id="Seg_4529" n="HIAT:ip">.</nts>
                  <nts id="Seg_4530" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T506" id="Seg_4531" n="sc" s="T355">
               <ts e="T361" id="Seg_4533" n="HIAT:u" s="T355">
                  <nts id="Seg_4534" n="HIAT:ip">–</nts>
                  <nts id="Seg_4535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_4537" n="HIAT:w" s="T355">Kaja</ts>
                  <nts id="Seg_4538" n="HIAT:ip">,</nts>
                  <nts id="Seg_4539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_4541" n="HIAT:w" s="T356">mini͡ettere</ts>
                  <nts id="Seg_4542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_4544" n="HIAT:w" s="T357">pirʼedkalarɨm</ts>
                  <nts id="Seg_4545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_4547" n="HIAT:w" s="T358">barɨta</ts>
                  <nts id="Seg_4548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_4550" n="HIAT:w" s="T359">oloŋkohut</ts>
                  <nts id="Seg_4551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_4553" n="HIAT:w" s="T360">etiler</ts>
                  <nts id="Seg_4554" n="HIAT:ip">.</nts>
                  <nts id="Seg_4555" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T384" id="Seg_4557" n="HIAT:u" s="T361">
                  <ts e="T362" id="Seg_4559" n="HIAT:w" s="T361">ɨllaːččɨ</ts>
                  <nts id="Seg_4560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_4562" n="HIAT:w" s="T362">da</ts>
                  <nts id="Seg_4563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_4565" n="HIAT:w" s="T363">etiler</ts>
                  <nts id="Seg_4566" n="HIAT:ip">,</nts>
                  <nts id="Seg_4567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_4569" n="HIAT:w" s="T364">ɨrɨ͡a</ts>
                  <nts id="Seg_4570" n="HIAT:ip">,</nts>
                  <nts id="Seg_4571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4572" n="HIAT:ip">(</nts>
                  <ts e="T366" id="Seg_4574" n="HIAT:w" s="T365">ɨlla-</ts>
                  <nts id="Seg_4575" n="HIAT:ip">)</nts>
                  <nts id="Seg_4576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_4578" n="HIAT:w" s="T366">ɨllana</ts>
                  <nts id="Seg_4579" n="HIAT:ip">,</nts>
                  <nts id="Seg_4580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_4582" n="HIAT:w" s="T367">ɨrɨ͡alaːk</ts>
                  <nts id="Seg_4583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_4585" n="HIAT:w" s="T368">daː</ts>
                  <nts id="Seg_4586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_4588" n="HIAT:w" s="T369">oloŋkonu</ts>
                  <nts id="Seg_4589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_4591" n="HIAT:w" s="T370">kepseːčči</ts>
                  <nts id="Seg_4592" n="HIAT:ip">,</nts>
                  <nts id="Seg_4593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_4595" n="HIAT:w" s="T371">meːne</ts>
                  <nts id="Seg_4596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_4598" n="HIAT:w" s="T372">da</ts>
                  <nts id="Seg_4599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_4601" n="HIAT:w" s="T373">oloŋkonu</ts>
                  <nts id="Seg_4602" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4603" n="HIAT:ip">(</nts>
                  <ts e="T375" id="Seg_4605" n="HIAT:w" s="T374">kepse-</ts>
                  <nts id="Seg_4606" n="HIAT:ip">)</nts>
                  <nts id="Seg_4607" n="HIAT:ip">,</nts>
                  <nts id="Seg_4608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_4610" n="HIAT:w" s="T375">oččogo</ts>
                  <nts id="Seg_4611" n="HIAT:ip">,</nts>
                  <nts id="Seg_4612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_4614" n="HIAT:w" s="T376">bɨlɨrgɨ</ts>
                  <nts id="Seg_4615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_4617" n="HIAT:w" s="T377">üjege</ts>
                  <nts id="Seg_4618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_4620" n="HIAT:w" s="T378">bu͡ollagɨnan</ts>
                  <nts id="Seg_4621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_4623" n="HIAT:w" s="T379">kinoːnɨ</ts>
                  <nts id="Seg_4624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_4626" n="HIAT:w" s="T380">körögün</ts>
                  <nts id="Seg_4627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_4629" n="HIAT:w" s="T381">duː</ts>
                  <nts id="Seg_4630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_4632" n="HIAT:w" s="T382">oččo</ts>
                  <nts id="Seg_4633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_4635" n="HIAT:w" s="T383">daːgɨnɨ</ts>
                  <nts id="Seg_4636" n="HIAT:ip">.</nts>
                  <nts id="Seg_4637" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T387" id="Seg_4639" n="HIAT:u" s="T384">
                  <nts id="Seg_4640" n="HIAT:ip">"</nts>
                  <ts e="T385" id="Seg_4642" n="HIAT:w" s="T384">Ispidoːlanɨ</ts>
                  <nts id="Seg_4643" n="HIAT:ip">"</nts>
                  <nts id="Seg_4644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_4646" n="HIAT:w" s="T385">ihilliːgin</ts>
                  <nts id="Seg_4647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_4649" n="HIAT:w" s="T386">duː</ts>
                  <nts id="Seg_4650" n="HIAT:ip">.</nts>
                  <nts id="Seg_4651" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T395" id="Seg_4653" n="HIAT:u" s="T387">
                  <ts e="T388" id="Seg_4655" n="HIAT:w" s="T387">Mas</ts>
                  <nts id="Seg_4656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_4658" n="HIAT:w" s="T388">oːnnʼuːrunan</ts>
                  <nts id="Seg_4659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_4661" n="HIAT:w" s="T389">onnʼuːgun</ts>
                  <nts id="Seg_4662" n="HIAT:ip">,</nts>
                  <nts id="Seg_4663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_4665" n="HIAT:w" s="T390">čɨːčaːktarɨnan</ts>
                  <nts id="Seg_4666" n="HIAT:ip">,</nts>
                  <nts id="Seg_4667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_4669" n="HIAT:w" s="T391">iti</ts>
                  <nts id="Seg_4670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_4672" n="HIAT:w" s="T392">hɨrga</ts>
                  <nts id="Seg_4673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_4675" n="HIAT:w" s="T393">oŋosto</ts>
                  <nts id="Seg_4676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_4678" n="HIAT:w" s="T394">oŋostogun</ts>
                  <nts id="Seg_4679" n="HIAT:ip">.</nts>
                  <nts id="Seg_4680" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T401" id="Seg_4682" n="HIAT:u" s="T395">
                  <ts e="T396" id="Seg_4684" n="HIAT:w" s="T395">Tahaːra</ts>
                  <nts id="Seg_4685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_4687" n="HIAT:w" s="T396">tagɨstakkɨnan</ts>
                  <nts id="Seg_4688" n="HIAT:ip">,</nts>
                  <nts id="Seg_4689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_4691" n="HIAT:w" s="T397">mu͡os</ts>
                  <nts id="Seg_4692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_4694" n="HIAT:w" s="T398">tamnaːn</ts>
                  <nts id="Seg_4695" n="HIAT:ip">,</nts>
                  <nts id="Seg_4696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_4698" n="HIAT:w" s="T399">mu͡ohunan</ts>
                  <nts id="Seg_4699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_4701" n="HIAT:w" s="T400">oːnnʼu͡oŋ</ts>
                  <nts id="Seg_4702" n="HIAT:ip">.</nts>
                  <nts id="Seg_4703" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T406" id="Seg_4705" n="HIAT:u" s="T401">
                  <ts e="T402" id="Seg_4707" n="HIAT:w" s="T401">U͡ol</ts>
                  <nts id="Seg_4708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_4710" n="HIAT:w" s="T402">ogo</ts>
                  <nts id="Seg_4711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_4713" n="HIAT:w" s="T403">bu͡olla</ts>
                  <nts id="Seg_4714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_4716" n="HIAT:w" s="T404">maːmɨttana</ts>
                  <nts id="Seg_4717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_4719" n="HIAT:w" s="T405">oːnnʼu͡oga</ts>
                  <nts id="Seg_4720" n="HIAT:ip">.</nts>
                  <nts id="Seg_4721" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T413" id="Seg_4723" n="HIAT:u" s="T406">
                  <ts e="T407" id="Seg_4725" n="HIAT:w" s="T406">Hajɨn</ts>
                  <nts id="Seg_4726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_4728" n="HIAT:w" s="T407">kajdak</ts>
                  <nts id="Seg_4729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_4731" n="HIAT:w" s="T408">hɨldʼargɨn</ts>
                  <nts id="Seg_4732" n="HIAT:ip">,</nts>
                  <nts id="Seg_4733" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_4735" n="HIAT:w" s="T409">taːhɨ</ts>
                  <nts id="Seg_4736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_4738" n="HIAT:w" s="T410">munnʼunaŋŋɨn</ts>
                  <nts id="Seg_4739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_4741" n="HIAT:w" s="T411">ölböküːn</ts>
                  <nts id="Seg_4742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413" id="Seg_4744" n="HIAT:w" s="T412">oːnnʼu͡oŋ</ts>
                  <nts id="Seg_4745" n="HIAT:ip">.</nts>
                  <nts id="Seg_4746" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T423" id="Seg_4748" n="HIAT:u" s="T413">
                  <ts e="T414" id="Seg_4750" n="HIAT:w" s="T413">Hol</ts>
                  <nts id="Seg_4751" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_4753" n="HIAT:w" s="T414">kördükkeːčen</ts>
                  <nts id="Seg_4754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_4756" n="HIAT:w" s="T415">künü</ts>
                  <nts id="Seg_4757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_4759" n="HIAT:w" s="T416">bɨhagɨn</ts>
                  <nts id="Seg_4760" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_4762" n="HIAT:w" s="T417">bu͡olla</ts>
                  <nts id="Seg_4763" n="HIAT:ip">,</nts>
                  <nts id="Seg_4764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_4766" n="HIAT:w" s="T418">bɨlɨrgɨ</ts>
                  <nts id="Seg_4767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_4769" n="HIAT:w" s="T419">ogo</ts>
                  <nts id="Seg_4770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_4772" n="HIAT:w" s="T420">bɨlɨrgɨlɨː</ts>
                  <nts id="Seg_4773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_4775" n="HIAT:w" s="T421">oːnnʼuːr</ts>
                  <nts id="Seg_4776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_4778" n="HIAT:w" s="T422">bu͡o</ts>
                  <nts id="Seg_4779" n="HIAT:ip">.</nts>
                  <nts id="Seg_4780" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T439" id="Seg_4782" n="HIAT:u" s="T423">
                  <ts e="T424" id="Seg_4784" n="HIAT:w" s="T423">Ol</ts>
                  <nts id="Seg_4785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_4787" n="HIAT:w" s="T424">bɨːhɨːtɨnan</ts>
                  <nts id="Seg_4788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_4790" n="HIAT:w" s="T425">anɨ</ts>
                  <nts id="Seg_4791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_4793" n="HIAT:w" s="T426">bɨlɨrgɨ</ts>
                  <nts id="Seg_4794" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_4796" n="HIAT:w" s="T427">oːnnʼuːru</ts>
                  <nts id="Seg_4797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_4799" n="HIAT:w" s="T428">da</ts>
                  <nts id="Seg_4800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_4802" n="HIAT:w" s="T429">anɨ</ts>
                  <nts id="Seg_4803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_4805" n="HIAT:w" s="T430">da</ts>
                  <nts id="Seg_4806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_4808" n="HIAT:w" s="T431">körü͡ökkütün</ts>
                  <nts id="Seg_4809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_4811" n="HIAT:w" s="T432">da</ts>
                  <nts id="Seg_4812" n="HIAT:ip">,</nts>
                  <nts id="Seg_4813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_4815" n="HIAT:w" s="T433">čaŋkarkaːtaːk</ts>
                  <nts id="Seg_4816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_4818" n="HIAT:w" s="T434">oːnnʼuːru</ts>
                  <nts id="Seg_4819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_4821" n="HIAT:w" s="T435">egelbitim</ts>
                  <nts id="Seg_4822" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_4824" n="HIAT:w" s="T436">bu͡olluŋ</ts>
                  <nts id="Seg_4825" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_4827" n="HIAT:w" s="T437">bu</ts>
                  <nts id="Seg_4828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_4830" n="HIAT:w" s="T438">dojduga</ts>
                  <nts id="Seg_4831" n="HIAT:ip">.</nts>
                  <nts id="Seg_4832" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T445" id="Seg_4834" n="HIAT:u" s="T439">
                  <ts e="T440" id="Seg_4836" n="HIAT:w" s="T439">Hin</ts>
                  <nts id="Seg_4837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_4839" n="HIAT:w" s="T440">köllörü͡öm</ts>
                  <nts id="Seg_4840" n="HIAT:ip">,</nts>
                  <nts id="Seg_4841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_4843" n="HIAT:w" s="T441">ol</ts>
                  <nts id="Seg_4844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_4846" n="HIAT:w" s="T442">oːnnʼuː</ts>
                  <nts id="Seg_4847" n="HIAT:ip">,</nts>
                  <nts id="Seg_4848" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_4850" n="HIAT:w" s="T443">kajdak</ts>
                  <nts id="Seg_4851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_4853" n="HIAT:w" s="T444">oːnnʼuːllarɨn</ts>
                  <nts id="Seg_4854" n="HIAT:ip">.</nts>
                  <nts id="Seg_4855" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T465" id="Seg_4857" n="HIAT:u" s="T445">
                  <ts e="T446" id="Seg_4859" n="HIAT:w" s="T445">Aŋar</ts>
                  <nts id="Seg_4860" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_4862" n="HIAT:w" s="T446">oːnnʼuːrdarɨ</ts>
                  <nts id="Seg_4863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4864" n="HIAT:ip">(</nts>
                  <ts e="T448" id="Seg_4866" n="HIAT:w" s="T447">oŋor-</ts>
                  <nts id="Seg_4867" n="HIAT:ip">)</nts>
                  <nts id="Seg_4868" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T449" id="Seg_4870" n="HIAT:w" s="T448">oŋoron</ts>
                  <nts id="Seg_4871" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_4873" n="HIAT:w" s="T449">egeli͡ekpin</ts>
                  <nts id="Seg_4874" n="HIAT:ip">,</nts>
                  <nts id="Seg_4875" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_4877" n="HIAT:w" s="T450">kihite</ts>
                  <nts id="Seg_4878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_4880" n="HIAT:w" s="T451">hu͡oppun</ts>
                  <nts id="Seg_4881" n="HIAT:ip">,</nts>
                  <nts id="Seg_4882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_4884" n="HIAT:w" s="T452">ogonnʼorum</ts>
                  <nts id="Seg_4885" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T454" id="Seg_4887" n="HIAT:w" s="T453">ol</ts>
                  <nts id="Seg_4888" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_4890" n="HIAT:w" s="T454">balʼnʼisaga</ts>
                  <nts id="Seg_4891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_4893" n="HIAT:w" s="T455">hɨtar</ts>
                  <nts id="Seg_4894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_4896" n="HIAT:w" s="T456">ete</ts>
                  <nts id="Seg_4897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T458" id="Seg_4899" n="HIAT:w" s="T457">bu͡olla</ts>
                  <nts id="Seg_4900" n="HIAT:ip">,</nts>
                  <nts id="Seg_4901" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_4903" n="HIAT:w" s="T458">biːr</ts>
                  <nts id="Seg_4904" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_4906" n="HIAT:w" s="T459">u͡olɨm</ts>
                  <nts id="Seg_4907" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_4909" n="HIAT:w" s="T460">hin</ts>
                  <nts id="Seg_4910" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T462" id="Seg_4912" n="HIAT:w" s="T461">ɨ͡aldʼan</ts>
                  <nts id="Seg_4913" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T463" id="Seg_4915" n="HIAT:w" s="T462">balʼnʼisattan</ts>
                  <nts id="Seg_4916" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_4918" n="HIAT:w" s="T463">taksaːt</ts>
                  <nts id="Seg_4919" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T465" id="Seg_4921" n="HIAT:w" s="T464">ke</ts>
                  <nts id="Seg_4922" n="HIAT:ip">…</nts>
                  <nts id="Seg_4923" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T487" id="Seg_4925" n="HIAT:u" s="T465">
                  <ts e="T466" id="Seg_4927" n="HIAT:w" s="T465">Kelbitim</ts>
                  <nts id="Seg_4928" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_4930" n="HIAT:w" s="T466">bu͡olla</ts>
                  <nts id="Seg_4931" n="HIAT:ip">,</nts>
                  <nts id="Seg_4932" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468" id="Seg_4934" n="HIAT:w" s="T467">ontum</ts>
                  <nts id="Seg_4935" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T469" id="Seg_4937" n="HIAT:w" s="T468">min</ts>
                  <nts id="Seg_4938" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_4940" n="HIAT:w" s="T469">tɨ͡aga</ts>
                  <nts id="Seg_4941" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_4943" n="HIAT:w" s="T470">baːr</ts>
                  <nts id="Seg_4944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_4946" n="HIAT:w" s="T471">ete</ts>
                  <nts id="Seg_4947" n="HIAT:ip">,</nts>
                  <nts id="Seg_4948" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_4950" n="HIAT:w" s="T472">ontum</ts>
                  <nts id="Seg_4951" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_4953" n="HIAT:w" s="T473">ebege</ts>
                  <nts id="Seg_4954" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_4956" n="HIAT:w" s="T474">kiːren</ts>
                  <nts id="Seg_4957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T476" id="Seg_4959" n="HIAT:w" s="T475">ɨ͡aldʼɨbɨta</ts>
                  <nts id="Seg_4960" n="HIAT:ip">,</nts>
                  <nts id="Seg_4961" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_4963" n="HIAT:w" s="T476">onton</ts>
                  <nts id="Seg_4964" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T478" id="Seg_4966" n="HIAT:w" s="T477">kojut</ts>
                  <nts id="Seg_4967" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T479" id="Seg_4969" n="HIAT:w" s="T478">taksɨbɨta</ts>
                  <nts id="Seg_4970" n="HIAT:ip">,</nts>
                  <nts id="Seg_4971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_4973" n="HIAT:w" s="T479">kelerbit</ts>
                  <nts id="Seg_4974" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T481" id="Seg_4976" n="HIAT:w" s="T480">da</ts>
                  <nts id="Seg_4977" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T482" id="Seg_4979" n="HIAT:w" s="T481">onu</ts>
                  <nts id="Seg_4980" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4981" n="HIAT:ip">–</nts>
                  <nts id="Seg_4982" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T483" id="Seg_4984" n="HIAT:w" s="T482">purgaːlar</ts>
                  <nts id="Seg_4985" n="HIAT:ip">,</nts>
                  <nts id="Seg_4986" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_4988" n="HIAT:w" s="T483">tu͡oktar</ts>
                  <nts id="Seg_4989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T485" id="Seg_4991" n="HIAT:w" s="T484">bu͡o</ts>
                  <nts id="Seg_4992" n="HIAT:ip">,</nts>
                  <nts id="Seg_4993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_4995" n="HIAT:w" s="T485">meheːjdeːn</ts>
                  <nts id="Seg_4996" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_4998" n="HIAT:w" s="T486">honon</ts>
                  <nts id="Seg_4999" n="HIAT:ip">.</nts>
                  <nts id="Seg_5000" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T500" id="Seg_5002" n="HIAT:u" s="T487">
                  <ts e="T488" id="Seg_5004" n="HIAT:w" s="T487">Tu͡ok</ts>
                  <nts id="Seg_5005" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T489" id="Seg_5007" n="HIAT:w" s="T488">elbek</ts>
                  <nts id="Seg_5008" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_5010" n="HIAT:w" s="T489">oːnnʼuːr</ts>
                  <nts id="Seg_5011" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_5013" n="HIAT:w" s="T490">egelbetegim</ts>
                  <nts id="Seg_5014" n="HIAT:ip">,</nts>
                  <nts id="Seg_5015" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T492" id="Seg_5017" n="HIAT:w" s="T491">urukku</ts>
                  <nts id="Seg_5018" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493" id="Seg_5020" n="HIAT:w" s="T492">ogo</ts>
                  <nts id="Seg_5021" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T494" id="Seg_5023" n="HIAT:w" s="T493">oːnnʼuːrun</ts>
                  <nts id="Seg_5024" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T495" id="Seg_5026" n="HIAT:w" s="T494">hin</ts>
                  <nts id="Seg_5027" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_5029" n="HIAT:w" s="T495">onton</ts>
                  <nts id="Seg_5030" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T497" id="Seg_5032" n="HIAT:w" s="T496">atɨn</ts>
                  <nts id="Seg_5033" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T498" id="Seg_5035" n="HIAT:w" s="T497">köllörüːge</ts>
                  <nts id="Seg_5036" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T499" id="Seg_5038" n="HIAT:w" s="T498">egeli͡ek</ts>
                  <nts id="Seg_5039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T500" id="Seg_5041" n="HIAT:w" s="T499">etim</ts>
                  <nts id="Seg_5042" n="HIAT:ip">.</nts>
                  <nts id="Seg_5043" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T506" id="Seg_5045" n="HIAT:u" s="T500">
                  <ts e="T501" id="Seg_5047" n="HIAT:w" s="T500">Urut</ts>
                  <nts id="Seg_5048" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T502" id="Seg_5050" n="HIAT:w" s="T501">bejem</ts>
                  <nts id="Seg_5051" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503" id="Seg_5053" n="HIAT:w" s="T502">oːnnʼoːn</ts>
                  <nts id="Seg_5054" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T504" id="Seg_5056" n="HIAT:w" s="T503">ü͡öskeːbippin</ts>
                  <nts id="Seg_5057" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T505" id="Seg_5059" n="HIAT:w" s="T504">ke</ts>
                  <nts id="Seg_5060" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T506" id="Seg_5062" n="HIAT:w" s="T505">onton</ts>
                  <nts id="Seg_5063" n="HIAT:ip">.</nts>
                  <nts id="Seg_5064" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T540" id="Seg_5065" n="sc" s="T517">
               <ts e="T522" id="Seg_5067" n="HIAT:u" s="T517">
                  <nts id="Seg_5068" n="HIAT:ip">–</nts>
                  <nts id="Seg_5069" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T518" id="Seg_5071" n="HIAT:w" s="T517">Te</ts>
                  <nts id="Seg_5072" n="HIAT:ip">,</nts>
                  <nts id="Seg_5073" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T519" id="Seg_5075" n="HIAT:w" s="T518">mu͡otunan</ts>
                  <nts id="Seg_5076" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T520" id="Seg_5078" n="HIAT:w" s="T519">hörüːbüt</ts>
                  <nts id="Seg_5079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T521" id="Seg_5081" n="HIAT:w" s="T520">üːtteːk</ts>
                  <nts id="Seg_5082" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T522" id="Seg_5084" n="HIAT:w" s="T521">maska</ts>
                  <nts id="Seg_5085" n="HIAT:ip">.</nts>
                  <nts id="Seg_5086" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T526" id="Seg_5088" n="HIAT:u" s="T522">
                  <ts e="T523" id="Seg_5090" n="HIAT:w" s="T522">Bahɨnan</ts>
                  <nts id="Seg_5091" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T524" id="Seg_5093" n="HIAT:w" s="T523">taŋnarɨ</ts>
                  <nts id="Seg_5094" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T525" id="Seg_5096" n="HIAT:w" s="T524">turan</ts>
                  <nts id="Seg_5097" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T526" id="Seg_5099" n="HIAT:w" s="T525">ergijer</ts>
                  <nts id="Seg_5100" n="HIAT:ip">.</nts>
                  <nts id="Seg_5101" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T536" id="Seg_5103" n="HIAT:u" s="T526">
                  <ts e="T527" id="Seg_5105" n="HIAT:w" s="T526">Onu</ts>
                  <nts id="Seg_5106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T528" id="Seg_5108" n="HIAT:w" s="T527">kördökkünen</ts>
                  <nts id="Seg_5109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T529" id="Seg_5111" n="HIAT:w" s="T528">ere</ts>
                  <nts id="Seg_5112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T530" id="Seg_5114" n="HIAT:w" s="T529">bili͡eŋ</ts>
                  <nts id="Seg_5115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T531" id="Seg_5117" n="HIAT:w" s="T530">bu͡olla</ts>
                  <nts id="Seg_5118" n="HIAT:ip">,</nts>
                  <nts id="Seg_5119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T532" id="Seg_5121" n="HIAT:w" s="T531">kajdak</ts>
                  <nts id="Seg_5122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T533" id="Seg_5124" n="HIAT:w" s="T532">anɨ</ts>
                  <nts id="Seg_5125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T534" id="Seg_5127" n="HIAT:w" s="T533">kepseːmmin</ts>
                  <nts id="Seg_5128" n="HIAT:ip">,</nts>
                  <nts id="Seg_5129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T535" id="Seg_5131" n="HIAT:w" s="T534">kajdak</ts>
                  <nts id="Seg_5132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T536" id="Seg_5134" n="HIAT:w" s="T535">kepsi͡emij</ts>
                  <nts id="Seg_5135" n="HIAT:ip">?</nts>
                  <nts id="Seg_5136" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T537" id="Seg_5138" n="HIAT:u" s="T536">
                  <ts e="T537" id="Seg_5140" n="HIAT:w" s="T536">Köllörü͡öm</ts>
                  <nts id="Seg_5141" n="HIAT:ip">.</nts>
                  <nts id="Seg_5142" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T540" id="Seg_5144" n="HIAT:u" s="T537">
                  <ts e="T538" id="Seg_5146" n="HIAT:w" s="T537">Ol</ts>
                  <nts id="Seg_5147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539" id="Seg_5149" n="HIAT:w" s="T538">kördökküne</ts>
                  <nts id="Seg_5150" n="HIAT:ip">,</nts>
                  <nts id="Seg_5151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T540" id="Seg_5153" n="HIAT:w" s="T539">bileːr</ts>
                  <nts id="Seg_5154" n="HIAT:ip">.</nts>
                  <nts id="Seg_5155" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T608" id="Seg_5156" n="sc" s="T558">
               <ts e="T561" id="Seg_5158" n="HIAT:u" s="T558">
                  <nts id="Seg_5159" n="HIAT:ip">–</nts>
                  <nts id="Seg_5160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T559" id="Seg_5162" n="HIAT:w" s="T558">Heː</ts>
                  <nts id="Seg_5163" n="HIAT:ip">,</nts>
                  <nts id="Seg_5164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T560" id="Seg_5166" n="HIAT:w" s="T559">bilebin</ts>
                  <nts id="Seg_5167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T561" id="Seg_5169" n="HIAT:w" s="T560">onnuktarɨ</ts>
                  <nts id="Seg_5170" n="HIAT:ip">.</nts>
                  <nts id="Seg_5171" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T569" id="Seg_5173" n="HIAT:u" s="T561">
                  <nts id="Seg_5174" n="HIAT:ip">(</nts>
                  <ts e="T562" id="Seg_5176" n="HIAT:w" s="T561">Onnuk-</ts>
                  <nts id="Seg_5177" n="HIAT:ip">)</nts>
                  <nts id="Seg_5178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T563" id="Seg_5180" n="HIAT:w" s="T562">onnukta</ts>
                  <nts id="Seg_5181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T564" id="Seg_5183" n="HIAT:w" s="T563">meːne</ts>
                  <nts id="Seg_5184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565" id="Seg_5186" n="HIAT:w" s="T564">lʼuboj</ts>
                  <nts id="Seg_5187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T566" id="Seg_5189" n="HIAT:w" s="T565">ɨrɨ͡ata</ts>
                  <nts id="Seg_5190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T567" id="Seg_5192" n="HIAT:w" s="T566">ɨllɨ͡am</ts>
                  <nts id="Seg_5193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T568" id="Seg_5195" n="HIAT:w" s="T567">du͡o</ts>
                  <nts id="Seg_5196" n="HIAT:ip">,</nts>
                  <nts id="Seg_5197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T569" id="Seg_5199" n="HIAT:w" s="T568">lʼubojda</ts>
                  <nts id="Seg_5200" n="HIAT:ip">?</nts>
                  <nts id="Seg_5201" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T573" id="Seg_5203" n="HIAT:u" s="T569">
                  <ts e="T570" id="Seg_5205" n="HIAT:w" s="T569">Lʼuboj</ts>
                  <nts id="Seg_5206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T571" id="Seg_5208" n="HIAT:w" s="T570">ɨrɨ͡ata</ts>
                  <nts id="Seg_5209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T572" id="Seg_5211" n="HIAT:w" s="T571">ɨllɨ͡am</ts>
                  <nts id="Seg_5212" n="HIAT:ip">,</nts>
                  <nts id="Seg_5213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T573" id="Seg_5215" n="HIAT:w" s="T572">beːbe</ts>
                  <nts id="Seg_5216" n="HIAT:ip">.</nts>
                  <nts id="Seg_5217" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T576" id="Seg_5219" n="HIAT:u" s="T573">
                  <ts e="T574" id="Seg_5221" n="HIAT:w" s="T573">Kele</ts>
                  <nts id="Seg_5222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T575" id="Seg_5224" n="HIAT:w" s="T574">tarpat</ts>
                  <nts id="Seg_5225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T576" id="Seg_5227" n="HIAT:w" s="T575">itintiŋ</ts>
                  <nts id="Seg_5228" n="HIAT:ip">.</nts>
                  <nts id="Seg_5229" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T583" id="Seg_5231" n="HIAT:u" s="T576">
                  <nts id="Seg_5232" n="HIAT:ip">(</nts>
                  <ts e="T577" id="Seg_5234" n="HIAT:w" s="T576">Keli͡e-</ts>
                  <nts id="Seg_5235" n="HIAT:ip">)</nts>
                  <nts id="Seg_5236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_5237" n="HIAT:ip">(</nts>
                  <ts e="T578" id="Seg_5239" n="HIAT:w" s="T577">keli͡ene</ts>
                  <nts id="Seg_5240" n="HIAT:ip">)</nts>
                  <nts id="Seg_5241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T579" id="Seg_5243" n="HIAT:w" s="T578">hu͡ok</ts>
                  <nts id="Seg_5244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T580" id="Seg_5246" n="HIAT:w" s="T579">bu͡olaːččɨ</ts>
                  <nts id="Seg_5247" n="HIAT:ip">,</nts>
                  <nts id="Seg_5248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T581" id="Seg_5250" n="HIAT:w" s="T580">kü͡ömejderin</ts>
                  <nts id="Seg_5251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T582" id="Seg_5253" n="HIAT:w" s="T581">öjdöːböppün</ts>
                  <nts id="Seg_5254" n="HIAT:ip">,</nts>
                  <nts id="Seg_5255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T583" id="Seg_5257" n="HIAT:w" s="T582">umnubuppun</ts>
                  <nts id="Seg_5258" n="HIAT:ip">.</nts>
                  <nts id="Seg_5259" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T595" id="Seg_5261" n="HIAT:u" s="T583">
                  <ts e="T584" id="Seg_5263" n="HIAT:w" s="T583">Urut</ts>
                  <nts id="Seg_5264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585" id="Seg_5266" n="HIAT:w" s="T584">bu͡ollagɨna</ts>
                  <nts id="Seg_5267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T586" id="Seg_5269" n="HIAT:w" s="T585">gininen</ts>
                  <nts id="Seg_5270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T587" id="Seg_5272" n="HIAT:w" s="T586">zanʼimaːtsalɨ͡am</ts>
                  <nts id="Seg_5273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T588" id="Seg_5275" n="HIAT:w" s="T587">di͡emmin</ts>
                  <nts id="Seg_5276" n="HIAT:ip">,</nts>
                  <nts id="Seg_5277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T589" id="Seg_5279" n="HIAT:w" s="T588">olus</ts>
                  <nts id="Seg_5280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T590" id="Seg_5282" n="HIAT:w" s="T589">dʼelajdaːbakka</ts>
                  <nts id="Seg_5283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T591" id="Seg_5285" n="HIAT:w" s="T590">hɨldʼɨbɨtɨm</ts>
                  <nts id="Seg_5286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T592" id="Seg_5288" n="HIAT:w" s="T591">bu͡olla</ts>
                  <nts id="Seg_5289" n="HIAT:ip">,</nts>
                  <nts id="Seg_5290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T593" id="Seg_5292" n="HIAT:w" s="T592">ol</ts>
                  <nts id="Seg_5293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T594" id="Seg_5295" n="HIAT:w" s="T593">ihin</ts>
                  <nts id="Seg_5296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T595" id="Seg_5298" n="HIAT:w" s="T594">umnabɨn</ts>
                  <nts id="Seg_5299" n="HIAT:ip">.</nts>
                  <nts id="Seg_5300" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T596" id="Seg_5302" n="HIAT:u" s="T595">
                  <ts e="T596" id="Seg_5304" n="HIAT:w" s="T595">SONG</ts>
                  <nts id="Seg_5305" n="HIAT:ip">.</nts>
                  <nts id="Seg_5306" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T597" id="Seg_5308" n="HIAT:u" s="T596">
                  <ts e="T597" id="Seg_5310" n="HIAT:w" s="T596">SONG</ts>
                  <nts id="Seg_5311" n="HIAT:ip">.</nts>
                  <nts id="Seg_5312" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T598" id="Seg_5314" n="HIAT:u" s="T597">
                  <ts e="T598" id="Seg_5316" n="HIAT:w" s="T597">SONG</ts>
                  <nts id="Seg_5317" n="HIAT:ip">.</nts>
                  <nts id="Seg_5318" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T599" id="Seg_5320" n="HIAT:u" s="T598">
                  <ts e="T599" id="Seg_5322" n="HIAT:w" s="T598">SONG</ts>
                  <nts id="Seg_5323" n="HIAT:ip">.</nts>
                  <nts id="Seg_5324" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T600" id="Seg_5326" n="HIAT:u" s="T599">
                  <ts e="T600" id="Seg_5328" n="HIAT:w" s="T599">SONG</ts>
                  <nts id="Seg_5329" n="HIAT:ip">.</nts>
                  <nts id="Seg_5330" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T601" id="Seg_5332" n="HIAT:u" s="T600">
                  <ts e="T601" id="Seg_5334" n="HIAT:w" s="T600">SONG</ts>
                  <nts id="Seg_5335" n="HIAT:ip">.</nts>
                  <nts id="Seg_5336" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T602" id="Seg_5338" n="HIAT:u" s="T601">
                  <ts e="T602" id="Seg_5340" n="HIAT:w" s="T601">SONG</ts>
                  <nts id="Seg_5341" n="HIAT:ip">.</nts>
                  <nts id="Seg_5342" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T603" id="Seg_5344" n="HIAT:u" s="T602">
                  <ts e="T603" id="Seg_5346" n="HIAT:w" s="T602">SONG</ts>
                  <nts id="Seg_5347" n="HIAT:ip">.</nts>
                  <nts id="Seg_5348" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T604" id="Seg_5350" n="HIAT:u" s="T603">
                  <ts e="T604" id="Seg_5352" n="HIAT:w" s="T603">SONG</ts>
                  <nts id="Seg_5353" n="HIAT:ip">.</nts>
                  <nts id="Seg_5354" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T605" id="Seg_5356" n="HIAT:u" s="T604">
                  <ts e="T605" id="Seg_5358" n="HIAT:w" s="T604">SONG</ts>
                  <nts id="Seg_5359" n="HIAT:ip">.</nts>
                  <nts id="Seg_5360" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T606" id="Seg_5362" n="HIAT:u" s="T605">
                  <ts e="T606" id="Seg_5364" n="HIAT:w" s="T605">SONG</ts>
                  <nts id="Seg_5365" n="HIAT:ip">.</nts>
                  <nts id="Seg_5366" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T607" id="Seg_5368" n="HIAT:u" s="T606">
                  <ts e="T607" id="Seg_5370" n="HIAT:w" s="T606">SONG</ts>
                  <nts id="Seg_5371" n="HIAT:ip">.</nts>
                  <nts id="Seg_5372" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T608" id="Seg_5374" n="HIAT:u" s="T607">
                  <ts e="T608" id="Seg_5376" n="HIAT:w" s="T607">SONG</ts>
                  <nts id="Seg_5377" n="HIAT:ip">.</nts>
                  <nts id="Seg_5378" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T621" id="Seg_5379" n="sc" s="T610">
               <ts e="T617" id="Seg_5381" n="HIAT:u" s="T610">
                  <nts id="Seg_5382" n="HIAT:ip">–</nts>
                  <nts id="Seg_5383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T611" id="Seg_5385" n="HIAT:w" s="T610">Iti</ts>
                  <nts id="Seg_5386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T612" id="Seg_5388" n="HIAT:w" s="T611">ölbüte</ts>
                  <nts id="Seg_5389" n="HIAT:ip">,</nts>
                  <nts id="Seg_5390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T613" id="Seg_5392" n="HIAT:w" s="T612">iti</ts>
                  <nts id="Seg_5393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T614" id="Seg_5395" n="HIAT:w" s="T613">Baja</ts>
                  <nts id="Seg_5396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T615" id="Seg_5398" n="HIAT:w" s="T614">di͡en</ts>
                  <nts id="Seg_5399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T616" id="Seg_5401" n="HIAT:w" s="T615">kɨːs</ts>
                  <nts id="Seg_5402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T617" id="Seg_5404" n="HIAT:w" s="T616">ɨllaːbɨta</ts>
                  <nts id="Seg_5405" n="HIAT:ip">.</nts>
                  <nts id="Seg_5406" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T621" id="Seg_5408" n="HIAT:u" s="T617">
                  <ts e="T618" id="Seg_5410" n="HIAT:w" s="T617">ɨllaːbɨt</ts>
                  <nts id="Seg_5411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T619" id="Seg_5413" n="HIAT:w" s="T618">u͡ola</ts>
                  <nts id="Seg_5414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T620" id="Seg_5416" n="HIAT:w" s="T619">hin</ts>
                  <nts id="Seg_5417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T621" id="Seg_5419" n="HIAT:w" s="T620">ölbüte</ts>
                  <nts id="Seg_5420" n="HIAT:ip">.</nts>
                  <nts id="Seg_5421" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T716" id="Seg_5422" n="sc" s="T641">
               <ts e="T652" id="Seg_5424" n="HIAT:u" s="T641">
                  <nts id="Seg_5425" n="HIAT:ip">–</nts>
                  <nts id="Seg_5426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T642" id="Seg_5428" n="HIAT:w" s="T641">Heː</ts>
                  <nts id="Seg_5429" n="HIAT:ip">,</nts>
                  <nts id="Seg_5430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T643" id="Seg_5432" n="HIAT:w" s="T642">onnuktarɨnan</ts>
                  <nts id="Seg_5433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T644" id="Seg_5435" n="HIAT:w" s="T643">Toːnʼanɨ</ts>
                  <nts id="Seg_5436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T645" id="Seg_5438" n="HIAT:w" s="T644">gɨtta</ts>
                  <nts id="Seg_5439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T646" id="Seg_5441" n="HIAT:w" s="T645">hin</ts>
                  <nts id="Seg_5442" n="HIAT:ip">,</nts>
                  <nts id="Seg_5443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T647" id="Seg_5445" n="HIAT:w" s="T646">eː</ts>
                  <nts id="Seg_5446" n="HIAT:ip">,</nts>
                  <nts id="Seg_5447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T648" id="Seg_5449" n="HIAT:w" s="T647">tu͡oktu͡opput</ts>
                  <nts id="Seg_5450" n="HIAT:ip">,</nts>
                  <nts id="Seg_5451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T649" id="Seg_5453" n="HIAT:w" s="T648">hɨrsarɨ</ts>
                  <nts id="Seg_5454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T650" id="Seg_5456" n="HIAT:w" s="T649">hin</ts>
                  <nts id="Seg_5457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T651" id="Seg_5459" n="HIAT:w" s="T650">ɨllɨ͡appɨt</ts>
                  <nts id="Seg_5460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T652" id="Seg_5462" n="HIAT:w" s="T651">bu͡olla</ts>
                  <nts id="Seg_5463" n="HIAT:ip">.</nts>
                  <nts id="Seg_5464" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T666" id="Seg_5466" n="HIAT:u" s="T652">
                  <ts e="T653" id="Seg_5468" n="HIAT:w" s="T652">Meːne</ts>
                  <nts id="Seg_5469" n="HIAT:ip">,</nts>
                  <nts id="Seg_5470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654" id="Seg_5472" n="HIAT:w" s="T653">tɨːna</ts>
                  <nts id="Seg_5473" n="HIAT:ip">,</nts>
                  <nts id="Seg_5474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T655" id="Seg_5476" n="HIAT:w" s="T654">tɨːna</ts>
                  <nts id="Seg_5477" n="HIAT:ip">,</nts>
                  <nts id="Seg_5478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T656" id="Seg_5480" n="HIAT:w" s="T655">eː</ts>
                  <nts id="Seg_5481" n="HIAT:ip">,</nts>
                  <nts id="Seg_5482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T656.tx-UkET.1" id="Seg_5484" n="HIAT:w" s="T656">pʼerʼexvat</ts>
                  <nts id="Seg_5485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T657" id="Seg_5487" n="HIAT:w" s="T656.tx-UkET.1">dɨxanʼijanɨ</ts>
                  <nts id="Seg_5488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_5489" n="HIAT:ip">–</nts>
                  <nts id="Seg_5490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T658" id="Seg_5492" n="HIAT:w" s="T657">tɨːna</ts>
                  <nts id="Seg_5493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T659" id="Seg_5495" n="HIAT:w" s="T658">tɨːnagɨn</ts>
                  <nts id="Seg_5496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T660" id="Seg_5498" n="HIAT:w" s="T659">bu͡olla</ts>
                  <nts id="Seg_5499" n="HIAT:ip">,</nts>
                  <nts id="Seg_5500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T661" id="Seg_5502" n="HIAT:w" s="T660">da</ts>
                  <nts id="Seg_5503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T662" id="Seg_5505" n="HIAT:w" s="T661">kanca</ts>
                  <nts id="Seg_5506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T663" id="Seg_5508" n="HIAT:w" s="T662">ɨllaː</ts>
                  <nts id="Seg_5509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T664" id="Seg_5511" n="HIAT:w" s="T663">di͡etekkine</ts>
                  <nts id="Seg_5512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T665" id="Seg_5514" n="HIAT:w" s="T664">ɨllɨ͡am</ts>
                  <nts id="Seg_5515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T666" id="Seg_5517" n="HIAT:w" s="T665">meːne</ts>
                  <nts id="Seg_5518" n="HIAT:ip">.</nts>
                  <nts id="Seg_5519" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T669" id="Seg_5521" n="HIAT:u" s="T666">
                  <ts e="T667" id="Seg_5523" n="HIAT:w" s="T666">Tɨːn</ts>
                  <nts id="Seg_5524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T668" id="Seg_5526" n="HIAT:w" s="T667">bɨldʼahar</ts>
                  <nts id="Seg_5527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T669" id="Seg_5529" n="HIAT:w" s="T668">kohoːn</ts>
                  <nts id="Seg_5530" n="HIAT:ip">.</nts>
                  <nts id="Seg_5531" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T670" id="Seg_5533" n="HIAT:u" s="T669">
                  <ts e="T670" id="Seg_5535" n="HIAT:w" s="T669">SONG</ts>
                  <nts id="Seg_5536" n="HIAT:ip">.</nts>
                  <nts id="Seg_5537" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T671" id="Seg_5539" n="HIAT:u" s="T670">
                  <ts e="T671" id="Seg_5541" n="HIAT:w" s="T670">SONG</ts>
                  <nts id="Seg_5542" n="HIAT:ip">.</nts>
                  <nts id="Seg_5543" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T672" id="Seg_5545" n="HIAT:u" s="T671">
                  <ts e="T672" id="Seg_5547" n="HIAT:w" s="T671">SONG</ts>
                  <nts id="Seg_5548" n="HIAT:ip">.</nts>
                  <nts id="Seg_5549" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T673" id="Seg_5551" n="HIAT:u" s="T672">
                  <ts e="T673" id="Seg_5553" n="HIAT:w" s="T672">SONG</ts>
                  <nts id="Seg_5554" n="HIAT:ip">.</nts>
                  <nts id="Seg_5555" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T674" id="Seg_5557" n="HIAT:u" s="T673">
                  <ts e="T674" id="Seg_5559" n="HIAT:w" s="T673">SONG</ts>
                  <nts id="Seg_5560" n="HIAT:ip">.</nts>
                  <nts id="Seg_5561" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T675" id="Seg_5563" n="HIAT:u" s="T674">
                  <ts e="T675" id="Seg_5565" n="HIAT:w" s="T674">SONG</ts>
                  <nts id="Seg_5566" n="HIAT:ip">.</nts>
                  <nts id="Seg_5567" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T676" id="Seg_5569" n="HIAT:u" s="T675">
                  <ts e="T676" id="Seg_5571" n="HIAT:w" s="T675">SONG</ts>
                  <nts id="Seg_5572" n="HIAT:ip">.</nts>
                  <nts id="Seg_5573" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T677" id="Seg_5575" n="HIAT:u" s="T676">
                  <ts e="T677" id="Seg_5577" n="HIAT:w" s="T676">SONG</ts>
                  <nts id="Seg_5578" n="HIAT:ip">.</nts>
                  <nts id="Seg_5579" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T678" id="Seg_5581" n="HIAT:u" s="T677">
                  <ts e="T678" id="Seg_5583" n="HIAT:w" s="T677">SONG</ts>
                  <nts id="Seg_5584" n="HIAT:ip">.</nts>
                  <nts id="Seg_5585" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T679" id="Seg_5587" n="HIAT:u" s="T678">
                  <ts e="T679" id="Seg_5589" n="HIAT:w" s="T678">SONG</ts>
                  <nts id="Seg_5590" n="HIAT:ip">.</nts>
                  <nts id="Seg_5591" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T680" id="Seg_5593" n="HIAT:u" s="T679">
                  <ts e="T680" id="Seg_5595" n="HIAT:w" s="T679">SONG</ts>
                  <nts id="Seg_5596" n="HIAT:ip">.</nts>
                  <nts id="Seg_5597" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T681" id="Seg_5599" n="HIAT:u" s="T680">
                  <ts e="T681" id="Seg_5601" n="HIAT:w" s="T680">SONG</ts>
                  <nts id="Seg_5602" n="HIAT:ip">.</nts>
                  <nts id="Seg_5603" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T682" id="Seg_5605" n="HIAT:u" s="T681">
                  <ts e="T682" id="Seg_5607" n="HIAT:w" s="T681">SONG</ts>
                  <nts id="Seg_5608" n="HIAT:ip">.</nts>
                  <nts id="Seg_5609" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T683" id="Seg_5611" n="HIAT:u" s="T682">
                  <ts e="T683" id="Seg_5613" n="HIAT:w" s="T682">SONG</ts>
                  <nts id="Seg_5614" n="HIAT:ip">.</nts>
                  <nts id="Seg_5615" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T684" id="Seg_5617" n="HIAT:u" s="T683">
                  <ts e="T684" id="Seg_5619" n="HIAT:w" s="T683">SONG</ts>
                  <nts id="Seg_5620" n="HIAT:ip">.</nts>
                  <nts id="Seg_5621" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T705" id="Seg_5623" n="HIAT:u" s="T684">
                  <nts id="Seg_5624" n="HIAT:ip">–</nts>
                  <nts id="Seg_5625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T685" id="Seg_5627" n="HIAT:w" s="T684">Itini</ts>
                  <nts id="Seg_5628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T686" id="Seg_5630" n="HIAT:w" s="T685">bu͡olla</ts>
                  <nts id="Seg_5631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T687" id="Seg_5633" n="HIAT:w" s="T686">hogotok</ts>
                  <nts id="Seg_5634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T688" id="Seg_5636" n="HIAT:w" s="T687">tɨːnnan</ts>
                  <nts id="Seg_5637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T689" id="Seg_5639" n="HIAT:w" s="T688">ɨllaːččɨlar</ts>
                  <nts id="Seg_5640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T690" id="Seg_5642" n="HIAT:w" s="T689">ühü</ts>
                  <nts id="Seg_5643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T691" id="Seg_5645" n="HIAT:w" s="T690">bɨlɨrgɨlar</ts>
                  <nts id="Seg_5646" n="HIAT:ip">,</nts>
                  <nts id="Seg_5647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T692" id="Seg_5649" n="HIAT:w" s="T691">hogotok</ts>
                  <nts id="Seg_5650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T693" id="Seg_5652" n="HIAT:w" s="T692">tɨːnnan</ts>
                  <nts id="Seg_5653" n="HIAT:ip">,</nts>
                  <nts id="Seg_5654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T694" id="Seg_5656" n="HIAT:w" s="T693">tɨːn</ts>
                  <nts id="Seg_5657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T695" id="Seg_5659" n="HIAT:w" s="T694">bɨldʼahannar</ts>
                  <nts id="Seg_5660" n="HIAT:ip">,</nts>
                  <nts id="Seg_5661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T696" id="Seg_5663" n="HIAT:w" s="T695">dʼe</ts>
                  <nts id="Seg_5664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T697" id="Seg_5666" n="HIAT:w" s="T696">ginner</ts>
                  <nts id="Seg_5667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T698" id="Seg_5669" n="HIAT:w" s="T697">oːnnʼuːrdara</ts>
                  <nts id="Seg_5670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T699" id="Seg_5672" n="HIAT:w" s="T698">ol</ts>
                  <nts id="Seg_5673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T700" id="Seg_5675" n="HIAT:w" s="T699">bu͡olla</ts>
                  <nts id="Seg_5676" n="HIAT:ip">,</nts>
                  <nts id="Seg_5677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T701" id="Seg_5679" n="HIAT:w" s="T700">bu</ts>
                  <nts id="Seg_5680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T702" id="Seg_5682" n="HIAT:w" s="T701">artʼistar</ts>
                  <nts id="Seg_5683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T703" id="Seg_5685" n="HIAT:w" s="T702">anɨ</ts>
                  <nts id="Seg_5686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T704" id="Seg_5688" n="HIAT:w" s="T703">oːnnʼuːllarɨn</ts>
                  <nts id="Seg_5689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T705" id="Seg_5691" n="HIAT:w" s="T704">kördük</ts>
                  <nts id="Seg_5692" n="HIAT:ip">.</nts>
                  <nts id="Seg_5693" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T716" id="Seg_5695" n="HIAT:u" s="T705">
                  <ts e="T706" id="Seg_5697" n="HIAT:w" s="T705">Hiti</ts>
                  <nts id="Seg_5698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T707" id="Seg_5700" n="HIAT:w" s="T706">hogotok</ts>
                  <nts id="Seg_5701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T708" id="Seg_5703" n="HIAT:w" s="T707">tɨːnnan</ts>
                  <nts id="Seg_5704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T709" id="Seg_5706" n="HIAT:w" s="T708">tijeːčči</ts>
                  <nts id="Seg_5707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T710" id="Seg_5709" n="HIAT:w" s="T709">ete</ts>
                  <nts id="Seg_5710" n="HIAT:ip">,</nts>
                  <nts id="Seg_5711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T711" id="Seg_5713" n="HIAT:w" s="T710">ühü</ts>
                  <nts id="Seg_5714" n="HIAT:ip">,</nts>
                  <nts id="Seg_5715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T712" id="Seg_5717" n="HIAT:w" s="T711">Iniki͡en</ts>
                  <nts id="Seg_5718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T713" id="Seg_5720" n="HIAT:w" s="T712">di͡en</ts>
                  <nts id="Seg_5721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T714" id="Seg_5723" n="HIAT:w" s="T713">kihi</ts>
                  <nts id="Seg_5724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_5725" n="HIAT:ip">–</nts>
                  <nts id="Seg_5726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T715" id="Seg_5728" n="HIAT:w" s="T714">iti</ts>
                  <nts id="Seg_5729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T716" id="Seg_5731" n="HIAT:w" s="T715">itinige</ts>
                  <nts id="Seg_5732" n="HIAT:ip">.</nts>
                  <nts id="Seg_5733" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T731" id="Seg_5734" n="sc" s="T727">
               <ts e="T731" id="Seg_5736" n="HIAT:u" s="T727">
                  <nts id="Seg_5737" n="HIAT:ip">–</nts>
                  <nts id="Seg_5738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T728" id="Seg_5740" n="HIAT:w" s="T727">Iti</ts>
                  <nts id="Seg_5741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T729" id="Seg_5743" n="HIAT:w" s="T728">mini͡ene</ts>
                  <nts id="Seg_5744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T730" id="Seg_5746" n="HIAT:w" s="T729">kɨːhɨm</ts>
                  <nts id="Seg_5747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T731" id="Seg_5749" n="HIAT:w" s="T730">iti</ts>
                  <nts id="Seg_5750" n="HIAT:ip">.</nts>
                  <nts id="Seg_5751" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T789" id="Seg_5752" n="sc" s="T760">
               <ts e="T769" id="Seg_5754" n="HIAT:u" s="T760">
                  <nts id="Seg_5755" n="HIAT:ip">–</nts>
                  <nts id="Seg_5756" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T761" id="Seg_5758" n="HIAT:w" s="T760">Heː</ts>
                  <nts id="Seg_5759" n="HIAT:ip">,</nts>
                  <nts id="Seg_5760" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T762" id="Seg_5762" n="HIAT:w" s="T761">dʼe</ts>
                  <nts id="Seg_5763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T763" id="Seg_5765" n="HIAT:w" s="T762">ol</ts>
                  <nts id="Seg_5766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T764" id="Seg_5768" n="HIAT:w" s="T763">min</ts>
                  <nts id="Seg_5769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T765" id="Seg_5771" n="HIAT:w" s="T764">kepseːbit</ts>
                  <nts id="Seg_5772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T766" id="Seg_5774" n="HIAT:w" s="T765">oloŋkobun</ts>
                  <nts id="Seg_5775" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_5776" n="HIAT:ip">"</nts>
                  <ts e="T767" id="Seg_5778" n="HIAT:w" s="T766">Koptoːk</ts>
                  <nts id="Seg_5779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T768" id="Seg_5781" n="HIAT:w" s="T767">Taiskanɨ</ts>
                  <nts id="Seg_5782" n="HIAT:ip">"</nts>
                  <nts id="Seg_5783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T769" id="Seg_5785" n="HIAT:w" s="T768">kepseːbitter</ts>
                  <nts id="Seg_5786" n="HIAT:ip">.</nts>
                  <nts id="Seg_5787" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T779" id="Seg_5789" n="HIAT:u" s="T769">
                  <ts e="T770" id="Seg_5791" n="HIAT:w" s="T769">Minigin</ts>
                  <nts id="Seg_5792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T771" id="Seg_5794" n="HIAT:w" s="T770">kepseten</ts>
                  <nts id="Seg_5795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T772" id="Seg_5797" n="HIAT:w" s="T771">baraːn</ts>
                  <nts id="Seg_5798" n="HIAT:ip">,</nts>
                  <nts id="Seg_5799" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T773" id="Seg_5801" n="HIAT:w" s="T772">kelbittere</ts>
                  <nts id="Seg_5802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T774" id="Seg_5804" n="HIAT:w" s="T773">manna</ts>
                  <nts id="Seg_5805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T775" id="Seg_5807" n="HIAT:w" s="T774">keleːriler</ts>
                  <nts id="Seg_5808" n="HIAT:ip">,</nts>
                  <nts id="Seg_5809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_5810" n="HIAT:ip">(</nts>
                  <ts e="T776" id="Seg_5812" n="HIAT:w" s="T775">kutta</ts>
                  <nts id="Seg_5813" n="HIAT:ip">)</nts>
                  <nts id="Seg_5814" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T777" id="Seg_5816" n="HIAT:w" s="T776">dʼɨlga</ts>
                  <nts id="Seg_5817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T778" id="Seg_5819" n="HIAT:w" s="T777">kelbittere</ts>
                  <nts id="Seg_5820" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T779" id="Seg_5822" n="HIAT:w" s="T778">oːnnʼoːrular</ts>
                  <nts id="Seg_5823" n="HIAT:ip">.</nts>
                  <nts id="Seg_5824" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T789" id="Seg_5826" n="HIAT:u" s="T779">
                  <ts e="T780" id="Seg_5828" n="HIAT:w" s="T779">Ontularɨn</ts>
                  <nts id="Seg_5829" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T781" id="Seg_5831" n="HIAT:w" s="T780">daːgɨnɨ</ts>
                  <nts id="Seg_5832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T782" id="Seg_5834" n="HIAT:w" s="T781">iti</ts>
                  <nts id="Seg_5835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T783" id="Seg_5837" n="HIAT:w" s="T782">kajdak</ts>
                  <nts id="Seg_5838" n="HIAT:ip">,</nts>
                  <nts id="Seg_5839" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T784" id="Seg_5841" n="HIAT:w" s="T783">kajdak</ts>
                  <nts id="Seg_5842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T785" id="Seg_5844" n="HIAT:w" s="T784">kepseːn</ts>
                  <nts id="Seg_5845" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T786" id="Seg_5847" n="HIAT:w" s="T785">keːspit</ts>
                  <nts id="Seg_5848" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T787" id="Seg_5850" n="HIAT:w" s="T786">etilere</ts>
                  <nts id="Seg_5851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T788" id="Seg_5853" n="HIAT:w" s="T787">onton</ts>
                  <nts id="Seg_5854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T789" id="Seg_5856" n="HIAT:w" s="T788">obu͡oja</ts>
                  <nts id="Seg_5857" n="HIAT:ip">.</nts>
                  <nts id="Seg_5858" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T813" id="Seg_5859" n="sc" s="T792">
               <ts e="T799" id="Seg_5861" n="HIAT:u" s="T792">
                  <nts id="Seg_5862" n="HIAT:ip">–</nts>
                  <nts id="Seg_5863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T793" id="Seg_5865" n="HIAT:w" s="T792">Eː</ts>
                  <nts id="Seg_5866" n="HIAT:ip">,</nts>
                  <nts id="Seg_5867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T794" id="Seg_5869" n="HIAT:w" s="T793">istibitim</ts>
                  <nts id="Seg_5870" n="HIAT:ip">,</nts>
                  <nts id="Seg_5871" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T795" id="Seg_5873" n="HIAT:w" s="T794">onu</ts>
                  <nts id="Seg_5874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T796" id="Seg_5876" n="HIAT:w" s="T795">istemmin</ts>
                  <nts id="Seg_5877" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T797" id="Seg_5879" n="HIAT:w" s="T796">gɨnabɨn</ts>
                  <nts id="Seg_5880" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T798" id="Seg_5882" n="HIAT:w" s="T797">iti</ts>
                  <nts id="Seg_5883" n="HIAT:ip">,</nts>
                  <nts id="Seg_5884" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_5885" n="HIAT:ip">(</nts>
                  <nts id="Seg_5886" n="HIAT:ip">(</nts>
                  <ats e="T799" id="Seg_5887" n="HIAT:non-pho" s="T798">LAUGH</ats>
                  <nts id="Seg_5888" n="HIAT:ip">)</nts>
                  <nts id="Seg_5889" n="HIAT:ip">)</nts>
                  <nts id="Seg_5890" n="HIAT:ip">.</nts>
                  <nts id="Seg_5891" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T805" id="Seg_5893" n="HIAT:u" s="T799">
                  <nts id="Seg_5894" n="HIAT:ip">(</nts>
                  <ts e="T800" id="Seg_5896" n="HIAT:w" s="T799">Oloru</ts>
                  <nts id="Seg_5897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T801" id="Seg_5899" n="HIAT:w" s="T800">ke-</ts>
                  <nts id="Seg_5900" n="HIAT:ip">)</nts>
                  <nts id="Seg_5901" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T802" id="Seg_5903" n="HIAT:w" s="T801">Oloru</ts>
                  <nts id="Seg_5904" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T803" id="Seg_5906" n="HIAT:w" s="T802">keleller</ts>
                  <nts id="Seg_5907" n="HIAT:ip">,</nts>
                  <nts id="Seg_5908" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T804" id="Seg_5910" n="HIAT:w" s="T803">oloru</ts>
                  <nts id="Seg_5911" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T805" id="Seg_5913" n="HIAT:w" s="T804">hemeliːbin</ts>
                  <nts id="Seg_5914" n="HIAT:ip">:</nts>
                  <nts id="Seg_5915" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T813" id="Seg_5917" n="HIAT:u" s="T805">
                  <nts id="Seg_5918" n="HIAT:ip">"</nts>
                  <ts e="T806" id="Seg_5920" n="HIAT:w" s="T805">Togo</ts>
                  <nts id="Seg_5921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T807" id="Seg_5923" n="HIAT:w" s="T806">ke</ts>
                  <nts id="Seg_5924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T808" id="Seg_5926" n="HIAT:w" s="T807">iti</ts>
                  <nts id="Seg_5927" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T809" id="Seg_5929" n="HIAT:w" s="T808">onon-manan</ts>
                  <nts id="Seg_5930" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T810" id="Seg_5932" n="HIAT:w" s="T809">kepsiːgit</ts>
                  <nts id="Seg_5933" n="HIAT:ip">,</nts>
                  <nts id="Seg_5934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T811" id="Seg_5936" n="HIAT:w" s="T810">ileliː</ts>
                  <nts id="Seg_5937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T812" id="Seg_5939" n="HIAT:w" s="T811">kepseːbekkit</ts>
                  <nts id="Seg_5940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T813" id="Seg_5942" n="HIAT:w" s="T812">oloŋkogutun</ts>
                  <nts id="Seg_5943" n="HIAT:ip">?</nts>
                  <nts id="Seg_5944" n="HIAT:ip">"</nts>
                  <nts id="Seg_5945" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T826" id="Seg_5946" n="sc" s="T819">
               <ts e="T826" id="Seg_5948" n="HIAT:u" s="T819">
                  <nts id="Seg_5949" n="HIAT:ip">–</nts>
                  <nts id="Seg_5950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T819.tx-UkET.1" id="Seg_5952" n="HIAT:w" s="T819">Vsʼo</ts>
                  <nts id="Seg_5953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T820" id="Seg_5955" n="HIAT:w" s="T819.tx-UkET.1">ravno</ts>
                  <nts id="Seg_5956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T821" id="Seg_5958" n="HIAT:w" s="T820">hin</ts>
                  <nts id="Seg_5959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T822" id="Seg_5961" n="HIAT:w" s="T821">dʼüːllener</ts>
                  <nts id="Seg_5962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T823" id="Seg_5964" n="HIAT:w" s="T822">onton</ts>
                  <nts id="Seg_5965" n="HIAT:ip">,</nts>
                  <nts id="Seg_5966" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T824" id="Seg_5968" n="HIAT:w" s="T823">asnavnojɨ</ts>
                  <nts id="Seg_5969" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T825" id="Seg_5971" n="HIAT:w" s="T824">hin</ts>
                  <nts id="Seg_5972" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T826" id="Seg_5974" n="HIAT:w" s="T825">kepsiːller</ts>
                  <nts id="Seg_5975" n="HIAT:ip">.</nts>
                  <nts id="Seg_5976" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T883" id="Seg_5977" n="sc" s="T848">
               <ts e="T851" id="Seg_5979" n="HIAT:u" s="T848">
                  <nts id="Seg_5980" n="HIAT:ip">–</nts>
                  <nts id="Seg_5981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T849" id="Seg_5983" n="HIAT:w" s="T848">Eː</ts>
                  <nts id="Seg_5984" n="HIAT:ip">,</nts>
                  <nts id="Seg_5985" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T850" id="Seg_5987" n="HIAT:w" s="T849">baːr</ts>
                  <nts id="Seg_5988" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T851" id="Seg_5990" n="HIAT:w" s="T850">eːt</ts>
                  <nts id="Seg_5991" n="HIAT:ip">.</nts>
                  <nts id="Seg_5992" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T864" id="Seg_5994" n="HIAT:u" s="T851">
                  <ts e="T852" id="Seg_5996" n="HIAT:w" s="T851">Ol</ts>
                  <nts id="Seg_5997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T853" id="Seg_5999" n="HIAT:w" s="T852">oloŋkonu</ts>
                  <nts id="Seg_6000" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T854" id="Seg_6002" n="HIAT:w" s="T853">biler</ts>
                  <nts id="Seg_6003" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T855" id="Seg_6005" n="HIAT:w" s="T854">min</ts>
                  <nts id="Seg_6006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T856" id="Seg_6008" n="HIAT:w" s="T855">ubajɨm</ts>
                  <nts id="Seg_6009" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T857" id="Seg_6011" n="HIAT:w" s="T856">baːr</ts>
                  <nts id="Seg_6012" n="HIAT:ip">,</nts>
                  <nts id="Seg_6013" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T858" id="Seg_6015" n="HIAT:w" s="T857">ɨbajdarɨm</ts>
                  <nts id="Seg_6016" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T859" id="Seg_6018" n="HIAT:w" s="T858">onton</ts>
                  <nts id="Seg_6019" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T860" id="Seg_6021" n="HIAT:w" s="T859">ikki͡en</ts>
                  <nts id="Seg_6022" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T861" id="Seg_6024" n="HIAT:w" s="T860">da</ts>
                  <nts id="Seg_6025" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T862" id="Seg_6027" n="HIAT:w" s="T861">bileller</ts>
                  <nts id="Seg_6028" n="HIAT:ip">,</nts>
                  <nts id="Seg_6029" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T863" id="Seg_6031" n="HIAT:w" s="T862">ikki</ts>
                  <nts id="Seg_6032" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T864" id="Seg_6034" n="HIAT:w" s="T863">ubajdaːkpɨn</ts>
                  <nts id="Seg_6035" n="HIAT:ip">.</nts>
                  <nts id="Seg_6036" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T873" id="Seg_6038" n="HIAT:u" s="T864">
                  <ts e="T865" id="Seg_6040" n="HIAT:w" s="T864">Ikki͡en</ts>
                  <nts id="Seg_6041" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T866" id="Seg_6043" n="HIAT:w" s="T865">da</ts>
                  <nts id="Seg_6044" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_6045" n="HIAT:ip">(</nts>
                  <ts e="T867" id="Seg_6047" n="HIAT:w" s="T866">bile-</ts>
                  <nts id="Seg_6048" n="HIAT:ip">)</nts>
                  <nts id="Seg_6049" n="HIAT:ip">,</nts>
                  <nts id="Seg_6050" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T868" id="Seg_6052" n="HIAT:w" s="T867">biːrgehe</ts>
                  <nts id="Seg_6053" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T869" id="Seg_6055" n="HIAT:w" s="T868">kɨrdžagas</ts>
                  <nts id="Seg_6056" n="HIAT:ip">,</nts>
                  <nts id="Seg_6057" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T870" id="Seg_6059" n="HIAT:w" s="T869">biːrgehe</ts>
                  <nts id="Seg_6060" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T871" id="Seg_6062" n="HIAT:w" s="T870">miniginneːger</ts>
                  <nts id="Seg_6063" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T872" id="Seg_6065" n="HIAT:w" s="T871">hin</ts>
                  <nts id="Seg_6066" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T873" id="Seg_6068" n="HIAT:w" s="T872">ubaj</ts>
                  <nts id="Seg_6069" n="HIAT:ip">.</nts>
                  <nts id="Seg_6070" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T883" id="Seg_6072" n="HIAT:u" s="T873">
                  <ts e="T874" id="Seg_6074" n="HIAT:w" s="T873">Bileller</ts>
                  <nts id="Seg_6075" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T875" id="Seg_6077" n="HIAT:w" s="T874">olor</ts>
                  <nts id="Seg_6078" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T876" id="Seg_6080" n="HIAT:w" s="T875">oloŋkoloru</ts>
                  <nts id="Seg_6081" n="HIAT:ip">,</nts>
                  <nts id="Seg_6082" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T877" id="Seg_6084" n="HIAT:w" s="T876">ɨrɨ͡alaːktarɨ</ts>
                  <nts id="Seg_6085" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T878" id="Seg_6087" n="HIAT:w" s="T877">daː</ts>
                  <nts id="Seg_6088" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T879" id="Seg_6090" n="HIAT:w" s="T878">bileller</ts>
                  <nts id="Seg_6091" n="HIAT:ip">,</nts>
                  <nts id="Seg_6092" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T880" id="Seg_6094" n="HIAT:w" s="T879">ɨrɨ͡ata</ts>
                  <nts id="Seg_6095" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T881" id="Seg_6097" n="HIAT:w" s="T880">daː</ts>
                  <nts id="Seg_6098" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T882" id="Seg_6100" n="HIAT:w" s="T881">hu͡ogu</ts>
                  <nts id="Seg_6101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_6102" n="HIAT:ip">(</nts>
                  <ts e="T883" id="Seg_6104" n="HIAT:w" s="T882">bile-</ts>
                  <nts id="Seg_6105" n="HIAT:ip">)</nts>
                  <nts id="Seg_6106" n="HIAT:ip">.</nts>
                  <nts id="Seg_6107" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T918" id="Seg_6108" n="sc" s="T887">
               <ts e="T903" id="Seg_6110" n="HIAT:u" s="T887">
                  <nts id="Seg_6111" n="HIAT:ip">–</nts>
                  <nts id="Seg_6112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T888" id="Seg_6114" n="HIAT:w" s="T887">Meːne</ts>
                  <nts id="Seg_6115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T889" id="Seg_6117" n="HIAT:w" s="T888">kihiler</ts>
                  <nts id="Seg_6118" n="HIAT:ip">,</nts>
                  <nts id="Seg_6119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T890" id="Seg_6121" n="HIAT:w" s="T889">iti</ts>
                  <nts id="Seg_6122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T891" id="Seg_6124" n="HIAT:w" s="T890">urut</ts>
                  <nts id="Seg_6125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T892" id="Seg_6127" n="HIAT:w" s="T891">oloŋkohut</ts>
                  <nts id="Seg_6128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T893" id="Seg_6130" n="HIAT:w" s="T892">kihilerbit</ts>
                  <nts id="Seg_6131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T894" id="Seg_6133" n="HIAT:w" s="T893">barannɨlar</ts>
                  <nts id="Seg_6134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T895" id="Seg_6136" n="HIAT:w" s="T894">bu͡olla</ts>
                  <nts id="Seg_6137" n="HIAT:ip">,</nts>
                  <nts id="Seg_6138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T896" id="Seg_6140" n="HIAT:w" s="T895">anɨ</ts>
                  <nts id="Seg_6141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T897" id="Seg_6143" n="HIAT:w" s="T896">ile</ts>
                  <nts id="Seg_6144" n="HIAT:ip">,</nts>
                  <nts id="Seg_6145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T898" id="Seg_6147" n="HIAT:w" s="T897">ile</ts>
                  <nts id="Seg_6148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T899" id="Seg_6150" n="HIAT:w" s="T898">min</ts>
                  <nts id="Seg_6151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T900" id="Seg_6153" n="HIAT:w" s="T899">biler</ts>
                  <nts id="Seg_6154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T901" id="Seg_6156" n="HIAT:w" s="T900">oloŋkohut</ts>
                  <nts id="Seg_6157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T902" id="Seg_6159" n="HIAT:w" s="T901">kihilerim</ts>
                  <nts id="Seg_6160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T903" id="Seg_6162" n="HIAT:w" s="T902">barannɨlar</ts>
                  <nts id="Seg_6163" n="HIAT:ip">.</nts>
                  <nts id="Seg_6164" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T909" id="Seg_6166" n="HIAT:u" s="T903">
                  <ts e="T904" id="Seg_6168" n="HIAT:w" s="T903">Kim</ts>
                  <nts id="Seg_6169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T905" id="Seg_6171" n="HIAT:w" s="T904">baːra</ts>
                  <nts id="Seg_6172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T906" id="Seg_6174" n="HIAT:w" s="T905">bu͡olu͡oj</ts>
                  <nts id="Seg_6175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T907" id="Seg_6177" n="HIAT:w" s="T906">anɨ</ts>
                  <nts id="Seg_6178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T908" id="Seg_6180" n="HIAT:w" s="T907">oloŋkohut</ts>
                  <nts id="Seg_6181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T909" id="Seg_6183" n="HIAT:w" s="T908">kihi</ts>
                  <nts id="Seg_6184" n="HIAT:ip">?</nts>
                  <nts id="Seg_6185" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T918" id="Seg_6187" n="HIAT:u" s="T909">
                  <ts e="T910" id="Seg_6189" n="HIAT:w" s="T909">Kimi</ts>
                  <nts id="Seg_6190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T911" id="Seg_6192" n="HIAT:w" s="T910">da</ts>
                  <nts id="Seg_6193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T912" id="Seg_6195" n="HIAT:w" s="T911">bilbeppin</ts>
                  <nts id="Seg_6196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T913" id="Seg_6198" n="HIAT:w" s="T912">anɨ</ts>
                  <nts id="Seg_6199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T914" id="Seg_6201" n="HIAT:w" s="T913">oloŋkohut</ts>
                  <nts id="Seg_6202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T915" id="Seg_6204" n="HIAT:w" s="T914">kihini</ts>
                  <nts id="Seg_6205" n="HIAT:ip">,</nts>
                  <nts id="Seg_6206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T916" id="Seg_6208" n="HIAT:w" s="T915">ile</ts>
                  <nts id="Seg_6209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T917" id="Seg_6211" n="HIAT:w" s="T916">oloŋkohut</ts>
                  <nts id="Seg_6212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T918" id="Seg_6214" n="HIAT:w" s="T917">kihini</ts>
                  <nts id="Seg_6215" n="HIAT:ip">.</nts>
                  <nts id="Seg_6216" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T980" id="Seg_6217" n="sc" s="T932">
               <ts e="T943" id="Seg_6219" n="HIAT:u" s="T932">
                  <nts id="Seg_6220" n="HIAT:ip">–</nts>
                  <nts id="Seg_6221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T933" id="Seg_6223" n="HIAT:w" s="T932">Hu͡ok</ts>
                  <nts id="Seg_6224" n="HIAT:ip">,</nts>
                  <nts id="Seg_6225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T934" id="Seg_6227" n="HIAT:w" s="T933">hu͡ok</ts>
                  <nts id="Seg_6228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T935" id="Seg_6230" n="HIAT:w" s="T934">urut</ts>
                  <nts id="Seg_6231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T936" id="Seg_6233" n="HIAT:w" s="T935">bu͡olla</ts>
                  <nts id="Seg_6234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T937" id="Seg_6236" n="HIAT:w" s="T936">kimnere</ts>
                  <nts id="Seg_6237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T938" id="Seg_6239" n="HIAT:w" s="T937">daːganɨ</ts>
                  <nts id="Seg_6240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T939" id="Seg_6242" n="HIAT:w" s="T938">itigirdikeːčen</ts>
                  <nts id="Seg_6243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T940" id="Seg_6245" n="HIAT:w" s="T939">gɨmmatɨlar</ts>
                  <nts id="Seg_6246" n="HIAT:ip">,</nts>
                  <nts id="Seg_6247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T941" id="Seg_6249" n="HIAT:w" s="T940">bu</ts>
                  <nts id="Seg_6250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T942" id="Seg_6252" n="HIAT:w" s="T941">anɨ</ts>
                  <nts id="Seg_6253" n="HIAT:ip">,</nts>
                  <nts id="Seg_6254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T943" id="Seg_6256" n="HIAT:w" s="T942">bu</ts>
                  <nts id="Seg_6257" n="HIAT:ip">.</nts>
                  <nts id="Seg_6258" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T963" id="Seg_6260" n="HIAT:u" s="T943">
                  <ts e="T944" id="Seg_6262" n="HIAT:w" s="T943">Bɨlɨrɨːŋŋɨttan</ts>
                  <nts id="Seg_6263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T945" id="Seg_6265" n="HIAT:w" s="T944">bettek</ts>
                  <nts id="Seg_6266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T946" id="Seg_6268" n="HIAT:w" s="T945">bu</ts>
                  <nts id="Seg_6269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T947" id="Seg_6271" n="HIAT:w" s="T946">ide</ts>
                  <nts id="Seg_6272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T948" id="Seg_6274" n="HIAT:w" s="T947">bullular</ts>
                  <nts id="Seg_6275" n="HIAT:ip">,</nts>
                  <nts id="Seg_6276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T949" id="Seg_6278" n="HIAT:w" s="T948">bu</ts>
                  <nts id="Seg_6279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T950" id="Seg_6281" n="HIAT:w" s="T949">urukku</ts>
                  <nts id="Seg_6282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T951" id="Seg_6284" n="HIAT:w" s="T950">oloŋko</ts>
                  <nts id="Seg_6285" n="HIAT:ip">,</nts>
                  <nts id="Seg_6286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T952" id="Seg_6288" n="HIAT:w" s="T951">ol</ts>
                  <nts id="Seg_6289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T953" id="Seg_6291" n="HIAT:w" s="T952">ihin</ts>
                  <nts id="Seg_6292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T954" id="Seg_6294" n="HIAT:w" s="T953">bihigi</ts>
                  <nts id="Seg_6295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T955" id="Seg_6297" n="HIAT:w" s="T954">tahɨččɨ</ts>
                  <nts id="Seg_6298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T956" id="Seg_6300" n="HIAT:w" s="T955">umnubupput</ts>
                  <nts id="Seg_6301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T957" id="Seg_6303" n="HIAT:w" s="T956">onu</ts>
                  <nts id="Seg_6304" n="HIAT:ip">,</nts>
                  <nts id="Seg_6305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T958" id="Seg_6307" n="HIAT:w" s="T957">oloŋkolorbutun</ts>
                  <nts id="Seg_6308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T959" id="Seg_6310" n="HIAT:w" s="T958">barɨtɨn</ts>
                  <nts id="Seg_6311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T960" id="Seg_6313" n="HIAT:w" s="T959">umnan</ts>
                  <nts id="Seg_6314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T961" id="Seg_6316" n="HIAT:w" s="T960">ihebit</ts>
                  <nts id="Seg_6317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T962" id="Seg_6319" n="HIAT:w" s="T961">bu͡o</ts>
                  <nts id="Seg_6320" n="HIAT:ip">,</nts>
                  <nts id="Seg_6321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T963" id="Seg_6323" n="HIAT:w" s="T962">barɨtɨn</ts>
                  <nts id="Seg_6324" n="HIAT:ip">.</nts>
                  <nts id="Seg_6325" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T966" id="Seg_6327" n="HIAT:u" s="T963">
                  <ts e="T964" id="Seg_6329" n="HIAT:w" s="T963">Kaččaga</ts>
                  <nts id="Seg_6330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T965" id="Seg_6332" n="HIAT:w" s="T964">da</ts>
                  <nts id="Seg_6333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T966" id="Seg_6335" n="HIAT:w" s="T965">kepseːbekkin</ts>
                  <nts id="Seg_6336" n="HIAT:ip">.</nts>
                  <nts id="Seg_6337" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T972" id="Seg_6339" n="HIAT:u" s="T966">
                  <ts e="T967" id="Seg_6341" n="HIAT:w" s="T966">ɨrɨ͡anɨ</ts>
                  <nts id="Seg_6342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T968" id="Seg_6344" n="HIAT:w" s="T967">da</ts>
                  <nts id="Seg_6345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T969" id="Seg_6347" n="HIAT:w" s="T968">umnagɨn</ts>
                  <nts id="Seg_6348" n="HIAT:ip">,</nts>
                  <nts id="Seg_6349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T970" id="Seg_6351" n="HIAT:w" s="T969">oloŋkonu</ts>
                  <nts id="Seg_6352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T971" id="Seg_6354" n="HIAT:w" s="T970">da</ts>
                  <nts id="Seg_6355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T972" id="Seg_6357" n="HIAT:w" s="T971">umnagɨn</ts>
                  <nts id="Seg_6358" n="HIAT:ip">.</nts>
                  <nts id="Seg_6359" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T980" id="Seg_6361" n="HIAT:u" s="T972">
                  <ts e="T973" id="Seg_6363" n="HIAT:w" s="T972">Ustorijanɨ</ts>
                  <nts id="Seg_6364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T974" id="Seg_6366" n="HIAT:w" s="T973">kepsiːre</ts>
                  <nts id="Seg_6367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T975" id="Seg_6369" n="HIAT:w" s="T974">bu͡olla</ts>
                  <nts id="Seg_6370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T976" id="Seg_6372" n="HIAT:w" s="T975">Onʼiːsim</ts>
                  <nts id="Seg_6373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T977" id="Seg_6375" n="HIAT:w" s="T976">ogonnʼor</ts>
                  <nts id="Seg_6376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T978" id="Seg_6378" n="HIAT:w" s="T977">baːr</ts>
                  <nts id="Seg_6379" n="HIAT:ip">,</nts>
                  <nts id="Seg_6380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T979" id="Seg_6382" n="HIAT:w" s="T978">oloŋkohut</ts>
                  <nts id="Seg_6383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T980" id="Seg_6385" n="HIAT:w" s="T979">emi͡e</ts>
                  <nts id="Seg_6386" n="HIAT:ip">.</nts>
                  <nts id="Seg_6387" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1022" id="Seg_6388" n="sc" s="T990">
               <ts e="T995" id="Seg_6390" n="HIAT:u" s="T990">
                  <nts id="Seg_6391" n="HIAT:ip">–</nts>
                  <nts id="Seg_6392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T991" id="Seg_6394" n="HIAT:w" s="T990">Tu͡ok</ts>
                  <nts id="Seg_6395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T992" id="Seg_6397" n="HIAT:w" s="T991">bili͡ej</ts>
                  <nts id="Seg_6398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T993" id="Seg_6400" n="HIAT:w" s="T992">da</ts>
                  <nts id="Seg_6401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T994" id="Seg_6403" n="HIAT:w" s="T993">ol</ts>
                  <nts id="Seg_6404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T995" id="Seg_6406" n="HIAT:w" s="T994">ginileri</ts>
                  <nts id="Seg_6407" n="HIAT:ip">.</nts>
                  <nts id="Seg_6408" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1001" id="Seg_6410" n="HIAT:u" s="T995">
                  <ts e="T996" id="Seg_6412" n="HIAT:w" s="T995">Oloŋko</ts>
                  <nts id="Seg_6413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T997" id="Seg_6415" n="HIAT:w" s="T996">iŋere</ts>
                  <nts id="Seg_6416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T998" id="Seg_6418" n="HIAT:w" s="T997">dʼürü</ts>
                  <nts id="Seg_6419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T999" id="Seg_6421" n="HIAT:w" s="T998">ginilerge</ts>
                  <nts id="Seg_6422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1000" id="Seg_6424" n="HIAT:w" s="T999">tu͡ok</ts>
                  <nts id="Seg_6425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_6426" n="HIAT:ip">(</nts>
                  <ts e="T1001" id="Seg_6428" n="HIAT:w" s="T1000">bil-</ts>
                  <nts id="Seg_6429" n="HIAT:ip">)</nts>
                  <nts id="Seg_6430" n="HIAT:ip">?</nts>
                  <nts id="Seg_6431" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1015" id="Seg_6433" n="HIAT:u" s="T1001">
                  <ts e="T1002" id="Seg_6435" n="HIAT:w" s="T1001">Čubu-čubu</ts>
                  <nts id="Seg_6436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1003" id="Seg_6438" n="HIAT:w" s="T1002">anɨ</ts>
                  <nts id="Seg_6439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1004" id="Seg_6441" n="HIAT:w" s="T1003">oloŋkolospopput</ts>
                  <nts id="Seg_6442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1005" id="Seg_6444" n="HIAT:w" s="T1004">bu͡o</ts>
                  <nts id="Seg_6445" n="HIAT:ip">,</nts>
                  <nts id="Seg_6446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_6447" n="HIAT:ip">"</nts>
                  <ts e="T1006" id="Seg_6449" n="HIAT:w" s="T1005">meːne</ts>
                  <nts id="Seg_6450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1007" id="Seg_6452" n="HIAT:w" s="T1006">kepseː</ts>
                  <nts id="Seg_6453" n="HIAT:ip">"</nts>
                  <nts id="Seg_6454" n="HIAT:ip">,</nts>
                  <nts id="Seg_6455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1008" id="Seg_6457" n="HIAT:w" s="T1007">ere</ts>
                  <nts id="Seg_6458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1009" id="Seg_6460" n="HIAT:w" s="T1008">di͡etekterine</ts>
                  <nts id="Seg_6461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1010" id="Seg_6463" n="HIAT:w" s="T1009">kepsiːbin</ts>
                  <nts id="Seg_6464" n="HIAT:ip">,</nts>
                  <nts id="Seg_6465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1011" id="Seg_6467" n="HIAT:w" s="T1010">naːdadaktarɨnan</ts>
                  <nts id="Seg_6468" n="HIAT:ip">,</nts>
                  <nts id="Seg_6469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1012" id="Seg_6471" n="HIAT:w" s="T1011">kepsettekterine</ts>
                  <nts id="Seg_6472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1013" id="Seg_6474" n="HIAT:w" s="T1012">ere</ts>
                  <nts id="Seg_6475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1014" id="Seg_6477" n="HIAT:w" s="T1013">kepsiːgin</ts>
                  <nts id="Seg_6478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1015" id="Seg_6480" n="HIAT:w" s="T1014">bu͡o</ts>
                  <nts id="Seg_6481" n="HIAT:ip">.</nts>
                  <nts id="Seg_6482" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1022" id="Seg_6484" n="HIAT:u" s="T1015">
                  <ts e="T1016" id="Seg_6486" n="HIAT:w" s="T1015">Taːk</ts>
                  <nts id="Seg_6487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1017" id="Seg_6489" n="HIAT:w" s="T1016">da</ts>
                  <nts id="Seg_6490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1018" id="Seg_6492" n="HIAT:w" s="T1017">meːne</ts>
                  <nts id="Seg_6493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1019" id="Seg_6495" n="HIAT:w" s="T1018">kaččaga</ts>
                  <nts id="Seg_6496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1020" id="Seg_6498" n="HIAT:w" s="T1019">da</ts>
                  <nts id="Seg_6499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1021" id="Seg_6501" n="HIAT:w" s="T1020">oloŋkolospopput</ts>
                  <nts id="Seg_6502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1022" id="Seg_6504" n="HIAT:w" s="T1021">bu͡o</ts>
                  <nts id="Seg_6505" n="HIAT:ip">.</nts>
                  <nts id="Seg_6506" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1042" id="Seg_6507" n="sc" s="T1041">
               <ts e="T1042" id="Seg_6509" n="HIAT:u" s="T1041">
                  <nts id="Seg_6510" n="HIAT:ip">–</nts>
                  <nts id="Seg_6511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1042" id="Seg_6513" n="HIAT:w" s="T1041">Heː</ts>
                  <nts id="Seg_6514" n="HIAT:ip">.</nts>
                  <nts id="Seg_6515" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1060" id="Seg_6516" n="sc" s="T1053">
               <ts e="T1056" id="Seg_6518" n="HIAT:u" s="T1053">
                  <nts id="Seg_6519" n="HIAT:ip">–</nts>
                  <nts id="Seg_6520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1054" id="Seg_6522" n="HIAT:w" s="T1053">Kaja</ts>
                  <nts id="Seg_6523" n="HIAT:ip">,</nts>
                  <nts id="Seg_6524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1055" id="Seg_6526" n="HIAT:w" s="T1054">atɨnna</ts>
                  <nts id="Seg_6527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1056" id="Seg_6529" n="HIAT:w" s="T1055">bu͡olumna</ts>
                  <nts id="Seg_6530" n="HIAT:ip">.</nts>
                  <nts id="Seg_6531" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1060" id="Seg_6533" n="HIAT:u" s="T1056">
                  <nts id="Seg_6534" n="HIAT:ip">(</nts>
                  <nts id="Seg_6535" n="HIAT:ip">(</nts>
                  <ats e="T1060" id="Seg_6536" n="HIAT:non-pho" s="T1056">TALE; UkET_19940424_OldWomanTaal_flk</ats>
                  <nts id="Seg_6537" n="HIAT:ip">)</nts>
                  <nts id="Seg_6538" n="HIAT:ip">)</nts>
                  <nts id="Seg_6539" n="HIAT:ip">.</nts>
                  <nts id="Seg_6540" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1113" id="Seg_6541" n="sc" s="T1074">
               <ts e="T1080" id="Seg_6543" n="HIAT:u" s="T1074">
                  <nts id="Seg_6544" n="HIAT:ip">–</nts>
                  <nts id="Seg_6545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1075" id="Seg_6547" n="HIAT:w" s="T1074">Ogom</ts>
                  <nts id="Seg_6548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1076" id="Seg_6550" n="HIAT:w" s="T1075">tuhunan</ts>
                  <nts id="Seg_6551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1077" id="Seg_6553" n="HIAT:w" s="T1076">küččüküːkeːn</ts>
                  <nts id="Seg_6554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1078" id="Seg_6556" n="HIAT:w" s="T1077">stʼix</ts>
                  <nts id="Seg_6557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1079" id="Seg_6559" n="HIAT:w" s="T1078">obu͡ojun</ts>
                  <nts id="Seg_6560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1080" id="Seg_6562" n="HIAT:w" s="T1079">oŋorbutum</ts>
                  <nts id="Seg_6563" n="HIAT:ip">.</nts>
                  <nts id="Seg_6564" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1082" id="Seg_6566" n="HIAT:u" s="T1080">
                  <ts e="T1081" id="Seg_6568" n="HIAT:w" s="T1080">Ajsülüː</ts>
                  <nts id="Seg_6569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1082" id="Seg_6571" n="HIAT:w" s="T1081">tuhunan</ts>
                  <nts id="Seg_6572" n="HIAT:ip">.</nts>
                  <nts id="Seg_6573" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1085" id="Seg_6575" n="HIAT:u" s="T1082">
                  <ts e="T1083" id="Seg_6577" n="HIAT:w" s="T1082">Sulustar</ts>
                  <nts id="Seg_6578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1084" id="Seg_6580" n="HIAT:w" s="T1083">kallaːŋŋa</ts>
                  <nts id="Seg_6581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1085" id="Seg_6583" n="HIAT:w" s="T1084">köstöllör</ts>
                  <nts id="Seg_6584" n="HIAT:ip">.</nts>
                  <nts id="Seg_6585" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1088" id="Seg_6587" n="HIAT:u" s="T1085">
                  <ts e="T1086" id="Seg_6589" n="HIAT:w" s="T1085">Utuj</ts>
                  <nts id="Seg_6590" n="HIAT:ip">,</nts>
                  <nts id="Seg_6591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1087" id="Seg_6593" n="HIAT:w" s="T1086">utuj</ts>
                  <nts id="Seg_6594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1088" id="Seg_6596" n="HIAT:w" s="T1087">Ajsülüː</ts>
                  <nts id="Seg_6597" n="HIAT:ip">.</nts>
                  <nts id="Seg_6598" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1094" id="Seg_6600" n="HIAT:u" s="T1088">
                  <ts e="T1089" id="Seg_6602" n="HIAT:w" s="T1088">Üjebit</ts>
                  <nts id="Seg_6603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1090" id="Seg_6605" n="HIAT:w" s="T1089">ularɨjda</ts>
                  <nts id="Seg_6606" n="HIAT:ip">,</nts>
                  <nts id="Seg_6607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1091" id="Seg_6609" n="HIAT:w" s="T1090">oduː</ts>
                  <nts id="Seg_6610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1092" id="Seg_6612" n="HIAT:w" s="T1091">atɨn</ts>
                  <nts id="Seg_6613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1093" id="Seg_6615" n="HIAT:w" s="T1092">kem</ts>
                  <nts id="Seg_6616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1094" id="Seg_6618" n="HIAT:w" s="T1093">kelle</ts>
                  <nts id="Seg_6619" n="HIAT:ip">.</nts>
                  <nts id="Seg_6620" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1099" id="Seg_6622" n="HIAT:u" s="T1094">
                  <ts e="T1095" id="Seg_6624" n="HIAT:w" s="T1094">Türgenkeːnnik</ts>
                  <nts id="Seg_6625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1096" id="Seg_6627" n="HIAT:w" s="T1095">ulaːtaːr</ts>
                  <nts id="Seg_6628" n="HIAT:ip">,</nts>
                  <nts id="Seg_6629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1097" id="Seg_6631" n="HIAT:w" s="T1096">sappɨn</ts>
                  <nts id="Seg_6632" n="HIAT:ip">,</nts>
                  <nts id="Seg_6633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1098" id="Seg_6635" n="HIAT:w" s="T1097">innebin</ts>
                  <nts id="Seg_6636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1099" id="Seg_6638" n="HIAT:w" s="T1098">tutaːr</ts>
                  <nts id="Seg_6639" n="HIAT:ip">.</nts>
                  <nts id="Seg_6640" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1102" id="Seg_6642" n="HIAT:u" s="T1099">
                  <ts e="T1100" id="Seg_6644" n="HIAT:w" s="T1099">Min</ts>
                  <nts id="Seg_6645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1101" id="Seg_6647" n="HIAT:w" s="T1100">üjebin</ts>
                  <nts id="Seg_6648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1102" id="Seg_6650" n="HIAT:w" s="T1101">üjeleneːr</ts>
                  <nts id="Seg_6651" n="HIAT:ip">.</nts>
                  <nts id="Seg_6652" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1105" id="Seg_6654" n="HIAT:u" s="T1102">
                  <ts e="T1103" id="Seg_6656" n="HIAT:w" s="T1102">Kihi͡eke</ts>
                  <nts id="Seg_6657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1104" id="Seg_6659" n="HIAT:w" s="T1103">kihi</ts>
                  <nts id="Seg_6660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1105" id="Seg_6662" n="HIAT:w" s="T1104">deteːr</ts>
                  <nts id="Seg_6663" n="HIAT:ip">.</nts>
                  <nts id="Seg_6664" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1109" id="Seg_6666" n="HIAT:u" s="T1105">
                  <ts e="T1106" id="Seg_6668" n="HIAT:w" s="T1105">Ogokoːnum</ts>
                  <nts id="Seg_6669" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1107" id="Seg_6671" n="HIAT:w" s="T1106">ogotun</ts>
                  <nts id="Seg_6672" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1108" id="Seg_6674" n="HIAT:w" s="T1107">bihigiger</ts>
                  <nts id="Seg_6675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1109" id="Seg_6677" n="HIAT:w" s="T1108">biliːbin</ts>
                  <nts id="Seg_6678" n="HIAT:ip">.</nts>
                  <nts id="Seg_6679" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1113" id="Seg_6681" n="HIAT:u" s="T1109">
                  <ts e="T1110" id="Seg_6683" n="HIAT:w" s="T1109">Attɨtɨgar</ts>
                  <nts id="Seg_6684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1111" id="Seg_6686" n="HIAT:w" s="T1110">olorobun</ts>
                  <nts id="Seg_6687" n="HIAT:ip">,</nts>
                  <nts id="Seg_6688" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1112" id="Seg_6690" n="HIAT:w" s="T1111">orgujakaːn</ts>
                  <nts id="Seg_6691" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1113" id="Seg_6693" n="HIAT:w" s="T1112">ɨllannɨm</ts>
                  <nts id="Seg_6694" n="HIAT:ip">.</nts>
                  <nts id="Seg_6695" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-UkET">
            <ts e="T190" id="Seg_6696" n="sc" s="T21">
               <ts e="T23" id="Seg_6698" n="e" s="T21">– Min </ts>
               <ts e="T25" id="Seg_6700" n="e" s="T23">iː </ts>
               <ts e="T26" id="Seg_6702" n="e" s="T25">bejem </ts>
               <ts e="T27" id="Seg_6704" n="e" s="T26">töröːbüt </ts>
               <ts e="T28" id="Seg_6706" n="e" s="T27">hirim </ts>
               <ts e="T29" id="Seg_6708" n="e" s="T28">bi </ts>
               <ts e="T30" id="Seg_6710" n="e" s="T29">di͡ek </ts>
               <ts e="T31" id="Seg_6712" n="e" s="T30">ete, </ts>
               <ts e="T32" id="Seg_6714" n="e" s="T31">mu͡ora </ts>
               <ts e="T33" id="Seg_6716" n="e" s="T32">di͡ek, </ts>
               <ts e="T34" id="Seg_6718" n="e" s="T33">ol </ts>
               <ts e="T35" id="Seg_6720" n="e" s="T34">di͡ek, </ts>
               <ts e="T36" id="Seg_6722" n="e" s="T35">(Nos-) </ts>
               <ts e="T37" id="Seg_6724" n="e" s="T36">kim </ts>
               <ts e="T38" id="Seg_6726" n="e" s="T37">di͡ek, </ts>
               <ts e="T39" id="Seg_6728" n="e" s="T38">eː, </ts>
               <ts e="T40" id="Seg_6730" n="e" s="T39">onton </ts>
               <ts e="T41" id="Seg_6732" n="e" s="T40">ɨrɨːmnaj </ts>
               <ts e="T42" id="Seg_6734" n="e" s="T41">oŋu͡orgutugar </ts>
               <ts e="T43" id="Seg_6736" n="e" s="T42">iti. </ts>
               <ts e="T44" id="Seg_6738" n="e" s="T43">Mu͡ora </ts>
               <ts e="T45" id="Seg_6740" n="e" s="T44">di͡ek </ts>
               <ts e="T46" id="Seg_6742" n="e" s="T45">töröːbütüm, </ts>
               <ts e="T47" id="Seg_6744" n="e" s="T46">onton </ts>
               <ts e="T48" id="Seg_6746" n="e" s="T47">erge </ts>
               <ts e="T49" id="Seg_6748" n="e" s="T48">barammɨn </ts>
               <ts e="T50" id="Seg_6750" n="e" s="T49">taːs </ts>
               <ts e="T51" id="Seg_6752" n="e" s="T50">di͡ek </ts>
               <ts e="T52" id="Seg_6754" n="e" s="T51">hirdemmitim. </ts>
               <ts e="T53" id="Seg_6756" n="e" s="T52">Ogolorum </ts>
               <ts e="T54" id="Seg_6758" n="e" s="T53">da </ts>
               <ts e="T55" id="Seg_6760" n="e" s="T54">tu͡ok… </ts>
               <ts e="T56" id="Seg_6762" n="e" s="T55">Kajdak </ts>
               <ts e="T57" id="Seg_6764" n="e" s="T56">kepseːn </ts>
               <ts e="T58" id="Seg_6766" n="e" s="T57">da, </ts>
               <ts e="T59" id="Seg_6768" n="e" s="T58">tugu </ts>
               <ts e="T60" id="Seg_6770" n="e" s="T59">kepsi͡emij, </ts>
               <ts e="T61" id="Seg_6772" n="e" s="T60">biːr </ts>
               <ts e="T62" id="Seg_6774" n="e" s="T61">ogom </ts>
               <ts e="T63" id="Seg_6776" n="e" s="T62">iti </ts>
               <ts e="T64" id="Seg_6778" n="e" s="T63">ušku͡olaga </ts>
               <ts e="T65" id="Seg_6780" n="e" s="T64">üleliːr, </ts>
               <ts e="T66" id="Seg_6782" n="e" s="T65">biːr </ts>
               <ts e="T67" id="Seg_6784" n="e" s="T66">kɨːhɨm </ts>
               <ts e="T68" id="Seg_6786" n="e" s="T67">dʼirʼektardiːr. </ts>
               <ts e="T69" id="Seg_6788" n="e" s="T68">Biːr, </ts>
               <ts e="T70" id="Seg_6790" n="e" s="T69">e, </ts>
               <ts e="T71" id="Seg_6792" n="e" s="T70">biːr </ts>
               <ts e="T72" id="Seg_6794" n="e" s="T71">ogom </ts>
               <ts e="T73" id="Seg_6796" n="e" s="T72">iti </ts>
               <ts e="T74" id="Seg_6798" n="e" s="T73">Nosku͡oga, </ts>
               <ts e="T75" id="Seg_6800" n="e" s="T74">(ula-) </ts>
               <ts e="T76" id="Seg_6802" n="e" s="T75">muŋ </ts>
               <ts e="T77" id="Seg_6804" n="e" s="T76">ulakan </ts>
               <ts e="T78" id="Seg_6806" n="e" s="T77">kɨːhɨm </ts>
               <ts e="T79" id="Seg_6808" n="e" s="T78">Nosku͡oga </ts>
               <ts e="T80" id="Seg_6810" n="e" s="T79">ušku͡olaga </ts>
               <ts e="T81" id="Seg_6812" n="e" s="T80">doktuːrduːr. </ts>
               <ts e="T82" id="Seg_6814" n="e" s="T81">Nʼiːna. </ts>
               <ts e="T83" id="Seg_6816" n="e" s="T82">(Biː-) </ts>
               <ts e="T84" id="Seg_6818" n="e" s="T83">ol </ts>
               <ts e="T85" id="Seg_6820" n="e" s="T84">ulakan </ts>
               <ts e="T86" id="Seg_6822" n="e" s="T85">kɨːhɨm </ts>
               <ts e="T87" id="Seg_6824" n="e" s="T86">aːta. </ts>
               <ts e="T88" id="Seg_6826" n="e" s="T87">Kuččuguj, </ts>
               <ts e="T89" id="Seg_6828" n="e" s="T88">ontum </ts>
               <ts e="T90" id="Seg_6830" n="e" s="T89">annɨta </ts>
               <ts e="T91" id="Seg_6832" n="e" s="T90">Keːtʼe. </ts>
               <ts e="T92" id="Seg_6834" n="e" s="T91">Ol </ts>
               <ts e="T93" id="Seg_6836" n="e" s="T92">ušku͡olaga </ts>
               <ts e="T94" id="Seg_6838" n="e" s="T93">dʼirʼekterdiːr. </ts>
               <ts e="T95" id="Seg_6840" n="e" s="T94">Biːr </ts>
               <ts e="T96" id="Seg_6842" n="e" s="T95">kɨːhɨm </ts>
               <ts e="T97" id="Seg_6844" n="e" s="T96">haːdikka </ts>
               <ts e="T98" id="Seg_6846" n="e" s="T97">üleleːčči </ts>
               <ts e="T99" id="Seg_6848" n="e" s="T98">ete, </ts>
               <ts e="T100" id="Seg_6850" n="e" s="T99">ontum </ts>
               <ts e="T101" id="Seg_6852" n="e" s="T100">eː, </ts>
               <ts e="T102" id="Seg_6854" n="e" s="T101">bu </ts>
               <ts e="T103" id="Seg_6856" n="e" s="T102">haːdiktara, </ts>
               <ts e="T104" id="Seg_6858" n="e" s="T103">tu͡oktara </ts>
               <ts e="T105" id="Seg_6860" n="e" s="T104">aldʼanan </ts>
               <ts e="T106" id="Seg_6862" n="e" s="T105">anaːn </ts>
               <ts e="T107" id="Seg_6864" n="e" s="T106">da </ts>
               <ts e="T108" id="Seg_6866" n="e" s="T107">ogolonon, </ts>
               <ts e="T109" id="Seg_6868" n="e" s="T108">kanʼaːn. </ts>
               <ts e="T110" id="Seg_6870" n="e" s="T109">Anɨ </ts>
               <ts e="T111" id="Seg_6872" n="e" s="T110">ɨ͡aldʼan </ts>
               <ts e="T112" id="Seg_6874" n="e" s="T111">Dudʼinka </ts>
               <ts e="T113" id="Seg_6876" n="e" s="T112">di͡ek </ts>
               <ts e="T114" id="Seg_6878" n="e" s="T113">hɨldʼar </ts>
               <ts e="T115" id="Seg_6880" n="e" s="T114">bu, </ts>
               <ts e="T116" id="Seg_6882" n="e" s="T115">eː, </ts>
               <ts e="T117" id="Seg_6884" n="e" s="T116">Narilʼskajga </ts>
               <ts e="T118" id="Seg_6886" n="e" s="T117">emtete </ts>
               <ts e="T119" id="Seg_6888" n="e" s="T118">hɨtar </ts>
               <ts e="T120" id="Seg_6890" n="e" s="T119">anɨ. </ts>
               <ts e="T121" id="Seg_6892" n="e" s="T120">Onton </ts>
               <ts e="T122" id="Seg_6894" n="e" s="T121">biːr </ts>
               <ts e="T123" id="Seg_6896" n="e" s="T122">u͡olum </ts>
               <ts e="T124" id="Seg_6898" n="e" s="T123">axoːtnʼik </ts>
               <ts e="T125" id="Seg_6900" n="e" s="T124">bu͡olla. </ts>
               <ts e="T126" id="Seg_6902" n="e" s="T125">Biːr </ts>
               <ts e="T127" id="Seg_6904" n="e" s="T126">u͡olum </ts>
               <ts e="T128" id="Seg_6906" n="e" s="T127">iti </ts>
               <ts e="T129" id="Seg_6908" n="e" s="T128">hamalʼottarɨ </ts>
               <ts e="T130" id="Seg_6910" n="e" s="T129">(ɨːk-) </ts>
               <ts e="T131" id="Seg_6912" n="e" s="T130">ittelerin </ts>
               <ts e="T132" id="Seg_6914" n="e" s="T131">(ɨːt-) </ts>
               <ts e="T133" id="Seg_6916" n="e" s="T132">tu͡oktuːr, </ts>
               <ts e="T134" id="Seg_6918" n="e" s="T133">oŋoror, </ts>
               <ts e="T135" id="Seg_6920" n="e" s="T134">hamalʼottarɨ </ts>
               <ts e="T136" id="Seg_6922" n="e" s="T135">iːter, </ts>
               <ts e="T137" id="Seg_6924" n="e" s="T136">kötüter, </ts>
               <ts e="T138" id="Seg_6926" n="e" s="T137">itinne </ts>
               <ts e="T139" id="Seg_6928" n="e" s="T138">baːr. </ts>
               <ts e="T140" id="Seg_6930" n="e" s="T139">Biːr </ts>
               <ts e="T141" id="Seg_6932" n="e" s="T140">kɨːhɨm </ts>
               <ts e="T142" id="Seg_6934" n="e" s="T141">(ini-) </ts>
               <ts e="T143" id="Seg_6936" n="e" s="T142">tuːgu </ts>
               <ts e="T144" id="Seg_6938" n="e" s="T143">büppüt </ts>
               <ts e="T145" id="Seg_6940" n="e" s="T144">ete, </ts>
               <ts e="T146" id="Seg_6942" n="e" s="T145">haːdik </ts>
               <ts e="T147" id="Seg_6944" n="e" s="T146">ü͡öregin, </ts>
               <ts e="T148" id="Seg_6946" n="e" s="T147">anɨ </ts>
               <ts e="T149" id="Seg_6948" n="e" s="T148">bejem </ts>
               <ts e="T150" id="Seg_6950" n="e" s="T149">kɨrdʼammɨn, </ts>
               <ts e="T151" id="Seg_6952" n="e" s="T150">ontubun </ts>
               <ts e="T152" id="Seg_6954" n="e" s="T151">bejeber </ts>
               <ts e="T153" id="Seg_6956" n="e" s="T152">üleleteːribin </ts>
               <ts e="T154" id="Seg_6958" n="e" s="T153">tɨ͡aga </ts>
               <ts e="T155" id="Seg_6960" n="e" s="T154">tutabɨn. </ts>
               <ts e="T156" id="Seg_6962" n="e" s="T155">Ogonnʼorum </ts>
               <ts e="T157" id="Seg_6964" n="e" s="T156">urut </ts>
               <ts e="T158" id="Seg_6966" n="e" s="T157">bulčut </ts>
               <ts e="T159" id="Seg_6968" n="e" s="T158">ete </ts>
               <ts e="T160" id="Seg_6970" n="e" s="T159">hin </ts>
               <ts e="T161" id="Seg_6972" n="e" s="T160">(kihi͡e-), </ts>
               <ts e="T162" id="Seg_6974" n="e" s="T161">kihitten, </ts>
               <ts e="T163" id="Seg_6976" n="e" s="T162">eː, </ts>
               <ts e="T164" id="Seg_6978" n="e" s="T163">ɨrɨmnajga </ts>
               <ts e="T165" id="Seg_6980" n="e" s="T164">otton </ts>
               <ts e="T166" id="Seg_6982" n="e" s="T165">hapku͡oska </ts>
               <ts e="T167" id="Seg_6984" n="e" s="T166">peredavojɨnan </ts>
               <ts e="T168" id="Seg_6986" n="e" s="T167">bultaːččɨ </ts>
               <ts e="T169" id="Seg_6988" n="e" s="T168">ete. </ts>
               <ts e="T170" id="Seg_6990" n="e" s="T169">Anɨ </ts>
               <ts e="T171" id="Seg_6992" n="e" s="T170">ɨ͡aldʼan, </ts>
               <ts e="T172" id="Seg_6994" n="e" s="T171">iliːte </ts>
               <ts e="T173" id="Seg_6996" n="e" s="T172">ɨ͡aldʼan </ts>
               <ts e="T174" id="Seg_6998" n="e" s="T173">ol </ts>
               <ts e="T175" id="Seg_7000" n="e" s="T174">bulduttan </ts>
               <ts e="T176" id="Seg_7002" n="e" s="T175">da </ts>
               <ts e="T177" id="Seg_7004" n="e" s="T176">tagɨsta, </ts>
               <ts e="T178" id="Seg_7006" n="e" s="T177">onuga </ts>
               <ts e="T179" id="Seg_7008" n="e" s="T178">ebiːleːn </ts>
               <ts e="T180" id="Seg_7010" n="e" s="T179">anɨ </ts>
               <ts e="T181" id="Seg_7012" n="e" s="T180">(aːktan), </ts>
               <ts e="T182" id="Seg_7014" n="e" s="T181">pʼensʼijaga </ts>
               <ts e="T183" id="Seg_7016" n="e" s="T182">kiːrde. </ts>
               <ts e="T184" id="Seg_7018" n="e" s="T183">Bu </ts>
               <ts e="T185" id="Seg_7020" n="e" s="T184">anɨ </ts>
               <ts e="T186" id="Seg_7022" n="e" s="T185">bu </ts>
               <ts e="T188" id="Seg_7024" n="e" s="T186">dʼɨllarga </ts>
               <ts e="T190" id="Seg_7026" n="e" s="T188">kiːrbite. </ts>
            </ts>
            <ts e="T277" id="Seg_7027" n="sc" s="T202">
               <ts e="T203" id="Seg_7029" n="e" s="T202">– Heː, </ts>
               <ts e="T204" id="Seg_7031" n="e" s="T203">anɨ </ts>
               <ts e="T205" id="Seg_7033" n="e" s="T204">bu </ts>
               <ts e="T206" id="Seg_7035" n="e" s="T205">ogolorum, </ts>
               <ts e="T207" id="Seg_7037" n="e" s="T206">ogom </ts>
               <ts e="T208" id="Seg_7039" n="e" s="T207">ogolorun </ts>
               <ts e="T209" id="Seg_7041" n="e" s="T208">karajammɨn. </ts>
               <ts e="T210" id="Seg_7043" n="e" s="T209">Ol </ts>
               <ts e="T211" id="Seg_7045" n="e" s="T210">emŋe </ts>
               <ts e="T212" id="Seg_7047" n="e" s="T211">keleːčči </ts>
               <ts e="T213" id="Seg_7049" n="e" s="T212">ogom </ts>
               <ts e="T214" id="Seg_7051" n="e" s="T213">gi͡ene </ts>
               <ts e="T215" id="Seg_7053" n="e" s="T214">üs </ts>
               <ts e="T216" id="Seg_7055" n="e" s="T215">ogoloːk </ts>
               <ts e="T217" id="Seg_7057" n="e" s="T216">bu͡olla, </ts>
               <ts e="T218" id="Seg_7059" n="e" s="T217">ontularɨm </ts>
               <ts e="T219" id="Seg_7061" n="e" s="T218">ogotun </ts>
               <ts e="T220" id="Seg_7063" n="e" s="T219">karaja </ts>
               <ts e="T221" id="Seg_7065" n="e" s="T220">hɨtar </ts>
               <ts e="T222" id="Seg_7067" n="e" s="T221">etim </ts>
               <ts e="T223" id="Seg_7069" n="e" s="T222">bu͡o, </ts>
               <ts e="T224" id="Seg_7071" n="e" s="T223">anɨ </ts>
               <ts e="T225" id="Seg_7073" n="e" s="T224">bu͡ollagɨna </ts>
               <ts e="T226" id="Seg_7075" n="e" s="T225">bu. </ts>
               <ts e="T227" id="Seg_7077" n="e" s="T226">Ogom </ts>
               <ts e="T228" id="Seg_7079" n="e" s="T227">ol, </ts>
               <ts e="T229" id="Seg_7081" n="e" s="T228">Maːčam </ts>
               <ts e="T230" id="Seg_7083" n="e" s="T229">kelen </ts>
               <ts e="T231" id="Seg_7085" n="e" s="T230">kellim </ts>
               <ts e="T232" id="Seg_7087" n="e" s="T231">ol </ts>
               <ts e="T233" id="Seg_7089" n="e" s="T232">tɨ͡attan. </ts>
               <ts e="T234" id="Seg_7091" n="e" s="T233">Tɨ͡aga </ts>
               <ts e="T235" id="Seg_7093" n="e" s="T234">tuta </ts>
               <ts e="T236" id="Seg_7095" n="e" s="T235">hɨtabɨn </ts>
               <ts e="T237" id="Seg_7097" n="e" s="T236">bu͡olla, </ts>
               <ts e="T238" id="Seg_7099" n="e" s="T237">ol </ts>
               <ts e="T239" id="Seg_7101" n="e" s="T238">üs </ts>
               <ts e="T240" id="Seg_7103" n="e" s="T239">ogobun, </ts>
               <ts e="T241" id="Seg_7105" n="e" s="T240">ol </ts>
               <ts e="T242" id="Seg_7107" n="e" s="T241">maːmata </ts>
               <ts e="T243" id="Seg_7109" n="e" s="T242">bu </ts>
               <ts e="T244" id="Seg_7111" n="e" s="T243">di͡ek </ts>
               <ts e="T245" id="Seg_7113" n="e" s="T244">baːr, </ts>
               <ts e="T246" id="Seg_7115" n="e" s="T245">teːtelere </ts>
               <ts e="T247" id="Seg_7117" n="e" s="T246">arakpɨta </ts>
               <ts e="T248" id="Seg_7119" n="e" s="T247">bu͡olla, </ts>
               <ts e="T249" id="Seg_7121" n="e" s="T248">ol </ts>
               <ts e="T250" id="Seg_7123" n="e" s="T249">dʼuraːk </ts>
               <ts e="T251" id="Seg_7125" n="e" s="T250">ete. </ts>
               <ts e="T252" id="Seg_7127" n="e" s="T251">Ontuta </ts>
               <ts e="T253" id="Seg_7129" n="e" s="T252">arakpɨta. </ts>
               <ts e="T254" id="Seg_7131" n="e" s="T253">Ol </ts>
               <ts e="T255" id="Seg_7133" n="e" s="T254">ihin </ts>
               <ts e="T256" id="Seg_7135" n="e" s="T255">ol </ts>
               <ts e="T257" id="Seg_7137" n="e" s="T256">kantan </ts>
               <ts e="T258" id="Seg_7139" n="e" s="T257">biːrten </ts>
               <ts e="T259" id="Seg_7141" n="e" s="T258">biːr </ts>
               <ts e="T260" id="Seg_7143" n="e" s="T259">ogo </ts>
               <ts e="T261" id="Seg_7145" n="e" s="T260">bu͡olla, </ts>
               <ts e="T262" id="Seg_7147" n="e" s="T261">ogolorum </ts>
               <ts e="T263" id="Seg_7149" n="e" s="T262">ogoloro </ts>
               <ts e="T264" id="Seg_7151" n="e" s="T263">kaččaga </ts>
               <ts e="T265" id="Seg_7153" n="e" s="T264">da </ts>
               <ts e="T266" id="Seg_7155" n="e" s="T265">arappattar </ts>
               <ts e="T267" id="Seg_7157" n="e" s="T266">bu͡o, </ts>
               <ts e="T268" id="Seg_7159" n="e" s="T267">hajɨn, </ts>
               <ts e="T269" id="Seg_7161" n="e" s="T268">hajɨnnarɨ, </ts>
               <ts e="T270" id="Seg_7163" n="e" s="T269">kɨhɨnnarɨ </ts>
               <ts e="T271" id="Seg_7165" n="e" s="T270">huptu </ts>
               <ts e="T272" id="Seg_7167" n="e" s="T271">ogoloːk </ts>
               <ts e="T273" id="Seg_7169" n="e" s="T272">bu͡olaːččɨbɨn. </ts>
               <ts e="T274" id="Seg_7171" n="e" s="T273">Vnuktarɨm </ts>
               <ts e="T275" id="Seg_7173" n="e" s="T274">bi͡ek </ts>
               <ts e="T276" id="Seg_7175" n="e" s="T275">baːr </ts>
               <ts e="T277" id="Seg_7177" n="e" s="T276">bu͡olallar. </ts>
            </ts>
            <ts e="T348" id="Seg_7178" n="sc" s="T299">
               <ts e="T300" id="Seg_7180" n="e" s="T299">– Heː, </ts>
               <ts e="T301" id="Seg_7182" n="e" s="T300">kepseːččibit </ts>
               <ts e="T302" id="Seg_7184" n="e" s="T301">ittekeːni </ts>
               <ts e="T303" id="Seg_7186" n="e" s="T302">ki͡e, </ts>
               <ts e="T304" id="Seg_7188" n="e" s="T303">anɨ </ts>
               <ts e="T305" id="Seg_7190" n="e" s="T304">ulakannɨk </ts>
               <ts e="T306" id="Seg_7192" n="e" s="T305">da </ts>
               <ts e="T307" id="Seg_7194" n="e" s="T306">tu͡olkulana </ts>
               <ts e="T308" id="Seg_7196" n="e" s="T307">ilikter </ts>
               <ts e="T309" id="Seg_7198" n="e" s="T308">eːt, </ts>
               <ts e="T310" id="Seg_7200" n="e" s="T309">ulakan </ts>
               <ts e="T311" id="Seg_7202" n="e" s="T310">kɨːhɨm </ts>
               <ts e="T312" id="Seg_7204" n="e" s="T311">ere </ts>
               <ts e="T313" id="Seg_7206" n="e" s="T312">ogoloro </ts>
               <ts e="T314" id="Seg_7208" n="e" s="T313">usku͡olaga </ts>
               <ts e="T315" id="Seg_7210" n="e" s="T314">barbɨttara. </ts>
               <ts e="T316" id="Seg_7212" n="e" s="T315">Kuččuguj </ts>
               <ts e="T317" id="Seg_7214" n="e" s="T316">ogolorum </ts>
               <ts e="T318" id="Seg_7216" n="e" s="T317">ogoloro </ts>
               <ts e="T319" id="Seg_7218" n="e" s="T318">kiːre </ts>
               <ts e="T320" id="Seg_7220" n="e" s="T319">ilikter </ts>
               <ts e="T321" id="Seg_7222" n="e" s="T320">usku͡olaga. </ts>
               <ts e="T322" id="Seg_7224" n="e" s="T321">Olorgo </ts>
               <ts e="T323" id="Seg_7226" n="e" s="T322">ol </ts>
               <ts e="T324" id="Seg_7228" n="e" s="T323">ittekeːni </ts>
               <ts e="T325" id="Seg_7230" n="e" s="T324">kepsiːbin </ts>
               <ts e="T326" id="Seg_7232" n="e" s="T325">bu͡o, </ts>
               <ts e="T327" id="Seg_7234" n="e" s="T326">itteni-bütteni, </ts>
               <ts e="T328" id="Seg_7236" n="e" s="T327">"ebeː", </ts>
               <ts e="T329" id="Seg_7238" n="e" s="T328">di͡en </ts>
               <ts e="T330" id="Seg_7240" n="e" s="T329">ergiččije </ts>
               <ts e="T331" id="Seg_7242" n="e" s="T330">hɨldʼaːččɨlar </ts>
               <ts e="T332" id="Seg_7244" n="e" s="T331">bu͡o, </ts>
               <ts e="T333" id="Seg_7246" n="e" s="T332">"ontugun </ts>
               <ts e="T334" id="Seg_7248" n="e" s="T333">kepseː, </ts>
               <ts e="T335" id="Seg_7250" n="e" s="T334">onu </ts>
               <ts e="T336" id="Seg_7252" n="e" s="T335">oloŋkoloː, </ts>
               <ts e="T337" id="Seg_7254" n="e" s="T336">munu </ts>
               <ts e="T338" id="Seg_7256" n="e" s="T337">kepseː, </ts>
               <ts e="T339" id="Seg_7258" n="e" s="T338">itini </ts>
               <ts e="T340" id="Seg_7260" n="e" s="T339">ɨllaː". </ts>
               <ts e="T341" id="Seg_7262" n="e" s="T340">Oloru </ts>
               <ts e="T342" id="Seg_7264" n="e" s="T341">haːtataːrɨgɨn </ts>
               <ts e="T343" id="Seg_7266" n="e" s="T342">ke </ts>
               <ts e="T344" id="Seg_7268" n="e" s="T343">ɨllanɨŋ, </ts>
               <ts e="T345" id="Seg_7270" n="e" s="T344">kepsetiŋ – </ts>
               <ts e="T346" id="Seg_7272" n="e" s="T345">hin </ts>
               <ts e="T347" id="Seg_7274" n="e" s="T346">kojuː </ts>
               <ts e="T348" id="Seg_7276" n="e" s="T347">bu͡olla. </ts>
            </ts>
            <ts e="T506" id="Seg_7277" n="sc" s="T355">
               <ts e="T356" id="Seg_7279" n="e" s="T355">– Kaja, </ts>
               <ts e="T357" id="Seg_7281" n="e" s="T356">mini͡ettere </ts>
               <ts e="T358" id="Seg_7283" n="e" s="T357">pirʼedkalarɨm </ts>
               <ts e="T359" id="Seg_7285" n="e" s="T358">barɨta </ts>
               <ts e="T360" id="Seg_7287" n="e" s="T359">oloŋkohut </ts>
               <ts e="T361" id="Seg_7289" n="e" s="T360">etiler. </ts>
               <ts e="T362" id="Seg_7291" n="e" s="T361">ɨllaːččɨ </ts>
               <ts e="T363" id="Seg_7293" n="e" s="T362">da </ts>
               <ts e="T364" id="Seg_7295" n="e" s="T363">etiler, </ts>
               <ts e="T365" id="Seg_7297" n="e" s="T364">ɨrɨ͡a, </ts>
               <ts e="T366" id="Seg_7299" n="e" s="T365">(ɨlla-) </ts>
               <ts e="T367" id="Seg_7301" n="e" s="T366">ɨllana, </ts>
               <ts e="T368" id="Seg_7303" n="e" s="T367">ɨrɨ͡alaːk </ts>
               <ts e="T369" id="Seg_7305" n="e" s="T368">daː </ts>
               <ts e="T370" id="Seg_7307" n="e" s="T369">oloŋkonu </ts>
               <ts e="T371" id="Seg_7309" n="e" s="T370">kepseːčči, </ts>
               <ts e="T372" id="Seg_7311" n="e" s="T371">meːne </ts>
               <ts e="T373" id="Seg_7313" n="e" s="T372">da </ts>
               <ts e="T374" id="Seg_7315" n="e" s="T373">oloŋkonu </ts>
               <ts e="T375" id="Seg_7317" n="e" s="T374">(kepse-), </ts>
               <ts e="T376" id="Seg_7319" n="e" s="T375">oččogo, </ts>
               <ts e="T377" id="Seg_7321" n="e" s="T376">bɨlɨrgɨ </ts>
               <ts e="T378" id="Seg_7323" n="e" s="T377">üjege </ts>
               <ts e="T379" id="Seg_7325" n="e" s="T378">bu͡ollagɨnan </ts>
               <ts e="T380" id="Seg_7327" n="e" s="T379">kinoːnɨ </ts>
               <ts e="T381" id="Seg_7329" n="e" s="T380">körögün </ts>
               <ts e="T382" id="Seg_7331" n="e" s="T381">duː </ts>
               <ts e="T383" id="Seg_7333" n="e" s="T382">oččo </ts>
               <ts e="T384" id="Seg_7335" n="e" s="T383">daːgɨnɨ. </ts>
               <ts e="T385" id="Seg_7337" n="e" s="T384">"Ispidoːlanɨ" </ts>
               <ts e="T386" id="Seg_7339" n="e" s="T385">ihilliːgin </ts>
               <ts e="T387" id="Seg_7341" n="e" s="T386">duː. </ts>
               <ts e="T388" id="Seg_7343" n="e" s="T387">Mas </ts>
               <ts e="T389" id="Seg_7345" n="e" s="T388">oːnnʼuːrunan </ts>
               <ts e="T390" id="Seg_7347" n="e" s="T389">onnʼuːgun, </ts>
               <ts e="T391" id="Seg_7349" n="e" s="T390">čɨːčaːktarɨnan, </ts>
               <ts e="T392" id="Seg_7351" n="e" s="T391">iti </ts>
               <ts e="T393" id="Seg_7353" n="e" s="T392">hɨrga </ts>
               <ts e="T394" id="Seg_7355" n="e" s="T393">oŋosto </ts>
               <ts e="T395" id="Seg_7357" n="e" s="T394">oŋostogun. </ts>
               <ts e="T396" id="Seg_7359" n="e" s="T395">Tahaːra </ts>
               <ts e="T397" id="Seg_7361" n="e" s="T396">tagɨstakkɨnan, </ts>
               <ts e="T398" id="Seg_7363" n="e" s="T397">mu͡os </ts>
               <ts e="T399" id="Seg_7365" n="e" s="T398">tamnaːn, </ts>
               <ts e="T400" id="Seg_7367" n="e" s="T399">mu͡ohunan </ts>
               <ts e="T401" id="Seg_7369" n="e" s="T400">oːnnʼu͡oŋ. </ts>
               <ts e="T402" id="Seg_7371" n="e" s="T401">U͡ol </ts>
               <ts e="T403" id="Seg_7373" n="e" s="T402">ogo </ts>
               <ts e="T404" id="Seg_7375" n="e" s="T403">bu͡olla </ts>
               <ts e="T405" id="Seg_7377" n="e" s="T404">maːmɨttana </ts>
               <ts e="T406" id="Seg_7379" n="e" s="T405">oːnnʼu͡oga. </ts>
               <ts e="T407" id="Seg_7381" n="e" s="T406">Hajɨn </ts>
               <ts e="T408" id="Seg_7383" n="e" s="T407">kajdak </ts>
               <ts e="T409" id="Seg_7385" n="e" s="T408">hɨldʼargɨn, </ts>
               <ts e="T410" id="Seg_7387" n="e" s="T409">taːhɨ </ts>
               <ts e="T411" id="Seg_7389" n="e" s="T410">munnʼunaŋŋɨn </ts>
               <ts e="T412" id="Seg_7391" n="e" s="T411">ölböküːn </ts>
               <ts e="T413" id="Seg_7393" n="e" s="T412">oːnnʼu͡oŋ. </ts>
               <ts e="T414" id="Seg_7395" n="e" s="T413">Hol </ts>
               <ts e="T415" id="Seg_7397" n="e" s="T414">kördükkeːčen </ts>
               <ts e="T416" id="Seg_7399" n="e" s="T415">künü </ts>
               <ts e="T417" id="Seg_7401" n="e" s="T416">bɨhagɨn </ts>
               <ts e="T418" id="Seg_7403" n="e" s="T417">bu͡olla, </ts>
               <ts e="T419" id="Seg_7405" n="e" s="T418">bɨlɨrgɨ </ts>
               <ts e="T420" id="Seg_7407" n="e" s="T419">ogo </ts>
               <ts e="T421" id="Seg_7409" n="e" s="T420">bɨlɨrgɨlɨː </ts>
               <ts e="T422" id="Seg_7411" n="e" s="T421">oːnnʼuːr </ts>
               <ts e="T423" id="Seg_7413" n="e" s="T422">bu͡o. </ts>
               <ts e="T424" id="Seg_7415" n="e" s="T423">Ol </ts>
               <ts e="T425" id="Seg_7417" n="e" s="T424">bɨːhɨːtɨnan </ts>
               <ts e="T426" id="Seg_7419" n="e" s="T425">anɨ </ts>
               <ts e="T427" id="Seg_7421" n="e" s="T426">bɨlɨrgɨ </ts>
               <ts e="T428" id="Seg_7423" n="e" s="T427">oːnnʼuːru </ts>
               <ts e="T429" id="Seg_7425" n="e" s="T428">da </ts>
               <ts e="T430" id="Seg_7427" n="e" s="T429">anɨ </ts>
               <ts e="T431" id="Seg_7429" n="e" s="T430">da </ts>
               <ts e="T432" id="Seg_7431" n="e" s="T431">körü͡ökkütün </ts>
               <ts e="T433" id="Seg_7433" n="e" s="T432">da, </ts>
               <ts e="T434" id="Seg_7435" n="e" s="T433">čaŋkarkaːtaːk </ts>
               <ts e="T435" id="Seg_7437" n="e" s="T434">oːnnʼuːru </ts>
               <ts e="T436" id="Seg_7439" n="e" s="T435">egelbitim </ts>
               <ts e="T437" id="Seg_7441" n="e" s="T436">bu͡olluŋ </ts>
               <ts e="T438" id="Seg_7443" n="e" s="T437">bu </ts>
               <ts e="T439" id="Seg_7445" n="e" s="T438">dojduga. </ts>
               <ts e="T440" id="Seg_7447" n="e" s="T439">Hin </ts>
               <ts e="T441" id="Seg_7449" n="e" s="T440">köllörü͡öm, </ts>
               <ts e="T442" id="Seg_7451" n="e" s="T441">ol </ts>
               <ts e="T443" id="Seg_7453" n="e" s="T442">oːnnʼuː, </ts>
               <ts e="T444" id="Seg_7455" n="e" s="T443">kajdak </ts>
               <ts e="T445" id="Seg_7457" n="e" s="T444">oːnnʼuːllarɨn. </ts>
               <ts e="T446" id="Seg_7459" n="e" s="T445">Aŋar </ts>
               <ts e="T447" id="Seg_7461" n="e" s="T446">oːnnʼuːrdarɨ </ts>
               <ts e="T448" id="Seg_7463" n="e" s="T447">(oŋor-) </ts>
               <ts e="T449" id="Seg_7465" n="e" s="T448">oŋoron </ts>
               <ts e="T450" id="Seg_7467" n="e" s="T449">egeli͡ekpin, </ts>
               <ts e="T451" id="Seg_7469" n="e" s="T450">kihite </ts>
               <ts e="T452" id="Seg_7471" n="e" s="T451">hu͡oppun, </ts>
               <ts e="T453" id="Seg_7473" n="e" s="T452">ogonnʼorum </ts>
               <ts e="T454" id="Seg_7475" n="e" s="T453">ol </ts>
               <ts e="T455" id="Seg_7477" n="e" s="T454">balʼnʼisaga </ts>
               <ts e="T456" id="Seg_7479" n="e" s="T455">hɨtar </ts>
               <ts e="T457" id="Seg_7481" n="e" s="T456">ete </ts>
               <ts e="T458" id="Seg_7483" n="e" s="T457">bu͡olla, </ts>
               <ts e="T459" id="Seg_7485" n="e" s="T458">biːr </ts>
               <ts e="T460" id="Seg_7487" n="e" s="T459">u͡olɨm </ts>
               <ts e="T461" id="Seg_7489" n="e" s="T460">hin </ts>
               <ts e="T462" id="Seg_7491" n="e" s="T461">ɨ͡aldʼan </ts>
               <ts e="T463" id="Seg_7493" n="e" s="T462">balʼnʼisattan </ts>
               <ts e="T464" id="Seg_7495" n="e" s="T463">taksaːt </ts>
               <ts e="T465" id="Seg_7497" n="e" s="T464">ke… </ts>
               <ts e="T466" id="Seg_7499" n="e" s="T465">Kelbitim </ts>
               <ts e="T467" id="Seg_7501" n="e" s="T466">bu͡olla, </ts>
               <ts e="T468" id="Seg_7503" n="e" s="T467">ontum </ts>
               <ts e="T469" id="Seg_7505" n="e" s="T468">min </ts>
               <ts e="T470" id="Seg_7507" n="e" s="T469">tɨ͡aga </ts>
               <ts e="T471" id="Seg_7509" n="e" s="T470">baːr </ts>
               <ts e="T472" id="Seg_7511" n="e" s="T471">ete, </ts>
               <ts e="T473" id="Seg_7513" n="e" s="T472">ontum </ts>
               <ts e="T474" id="Seg_7515" n="e" s="T473">ebege </ts>
               <ts e="T475" id="Seg_7517" n="e" s="T474">kiːren </ts>
               <ts e="T476" id="Seg_7519" n="e" s="T475">ɨ͡aldʼɨbɨta, </ts>
               <ts e="T477" id="Seg_7521" n="e" s="T476">onton </ts>
               <ts e="T478" id="Seg_7523" n="e" s="T477">kojut </ts>
               <ts e="T479" id="Seg_7525" n="e" s="T478">taksɨbɨta, </ts>
               <ts e="T480" id="Seg_7527" n="e" s="T479">kelerbit </ts>
               <ts e="T481" id="Seg_7529" n="e" s="T480">da </ts>
               <ts e="T482" id="Seg_7531" n="e" s="T481">onu – </ts>
               <ts e="T483" id="Seg_7533" n="e" s="T482">purgaːlar, </ts>
               <ts e="T484" id="Seg_7535" n="e" s="T483">tu͡oktar </ts>
               <ts e="T485" id="Seg_7537" n="e" s="T484">bu͡o, </ts>
               <ts e="T486" id="Seg_7539" n="e" s="T485">meheːjdeːn </ts>
               <ts e="T487" id="Seg_7541" n="e" s="T486">honon. </ts>
               <ts e="T488" id="Seg_7543" n="e" s="T487">Tu͡ok </ts>
               <ts e="T489" id="Seg_7545" n="e" s="T488">elbek </ts>
               <ts e="T490" id="Seg_7547" n="e" s="T489">oːnnʼuːr </ts>
               <ts e="T491" id="Seg_7549" n="e" s="T490">egelbetegim, </ts>
               <ts e="T492" id="Seg_7551" n="e" s="T491">urukku </ts>
               <ts e="T493" id="Seg_7553" n="e" s="T492">ogo </ts>
               <ts e="T494" id="Seg_7555" n="e" s="T493">oːnnʼuːrun </ts>
               <ts e="T495" id="Seg_7557" n="e" s="T494">hin </ts>
               <ts e="T496" id="Seg_7559" n="e" s="T495">onton </ts>
               <ts e="T497" id="Seg_7561" n="e" s="T496">atɨn </ts>
               <ts e="T498" id="Seg_7563" n="e" s="T497">köllörüːge </ts>
               <ts e="T499" id="Seg_7565" n="e" s="T498">egeli͡ek </ts>
               <ts e="T500" id="Seg_7567" n="e" s="T499">etim. </ts>
               <ts e="T501" id="Seg_7569" n="e" s="T500">Urut </ts>
               <ts e="T502" id="Seg_7571" n="e" s="T501">bejem </ts>
               <ts e="T503" id="Seg_7573" n="e" s="T502">oːnnʼoːn </ts>
               <ts e="T504" id="Seg_7575" n="e" s="T503">ü͡öskeːbippin </ts>
               <ts e="T505" id="Seg_7577" n="e" s="T504">ke </ts>
               <ts e="T506" id="Seg_7579" n="e" s="T505">onton. </ts>
            </ts>
            <ts e="T540" id="Seg_7580" n="sc" s="T517">
               <ts e="T518" id="Seg_7582" n="e" s="T517">– Te, </ts>
               <ts e="T519" id="Seg_7584" n="e" s="T518">mu͡otunan </ts>
               <ts e="T520" id="Seg_7586" n="e" s="T519">hörüːbüt </ts>
               <ts e="T521" id="Seg_7588" n="e" s="T520">üːtteːk </ts>
               <ts e="T522" id="Seg_7590" n="e" s="T521">maska. </ts>
               <ts e="T523" id="Seg_7592" n="e" s="T522">Bahɨnan </ts>
               <ts e="T524" id="Seg_7594" n="e" s="T523">taŋnarɨ </ts>
               <ts e="T525" id="Seg_7596" n="e" s="T524">turan </ts>
               <ts e="T526" id="Seg_7598" n="e" s="T525">ergijer. </ts>
               <ts e="T527" id="Seg_7600" n="e" s="T526">Onu </ts>
               <ts e="T528" id="Seg_7602" n="e" s="T527">kördökkünen </ts>
               <ts e="T529" id="Seg_7604" n="e" s="T528">ere </ts>
               <ts e="T530" id="Seg_7606" n="e" s="T529">bili͡eŋ </ts>
               <ts e="T531" id="Seg_7608" n="e" s="T530">bu͡olla, </ts>
               <ts e="T532" id="Seg_7610" n="e" s="T531">kajdak </ts>
               <ts e="T533" id="Seg_7612" n="e" s="T532">anɨ </ts>
               <ts e="T534" id="Seg_7614" n="e" s="T533">kepseːmmin, </ts>
               <ts e="T535" id="Seg_7616" n="e" s="T534">kajdak </ts>
               <ts e="T536" id="Seg_7618" n="e" s="T535">kepsi͡emij? </ts>
               <ts e="T537" id="Seg_7620" n="e" s="T536">Köllörü͡öm. </ts>
               <ts e="T538" id="Seg_7622" n="e" s="T537">Ol </ts>
               <ts e="T539" id="Seg_7624" n="e" s="T538">kördökküne, </ts>
               <ts e="T540" id="Seg_7626" n="e" s="T539">bileːr. </ts>
            </ts>
            <ts e="T608" id="Seg_7627" n="sc" s="T558">
               <ts e="T559" id="Seg_7629" n="e" s="T558">– Heː, </ts>
               <ts e="T560" id="Seg_7631" n="e" s="T559">bilebin </ts>
               <ts e="T561" id="Seg_7633" n="e" s="T560">onnuktarɨ. </ts>
               <ts e="T562" id="Seg_7635" n="e" s="T561">(Onnuk-) </ts>
               <ts e="T563" id="Seg_7637" n="e" s="T562">onnukta </ts>
               <ts e="T564" id="Seg_7639" n="e" s="T563">meːne </ts>
               <ts e="T565" id="Seg_7641" n="e" s="T564">lʼuboj </ts>
               <ts e="T566" id="Seg_7643" n="e" s="T565">ɨrɨ͡ata </ts>
               <ts e="T567" id="Seg_7645" n="e" s="T566">ɨllɨ͡am </ts>
               <ts e="T568" id="Seg_7647" n="e" s="T567">du͡o, </ts>
               <ts e="T569" id="Seg_7649" n="e" s="T568">lʼubojda? </ts>
               <ts e="T570" id="Seg_7651" n="e" s="T569">Lʼuboj </ts>
               <ts e="T571" id="Seg_7653" n="e" s="T570">ɨrɨ͡ata </ts>
               <ts e="T572" id="Seg_7655" n="e" s="T571">ɨllɨ͡am, </ts>
               <ts e="T573" id="Seg_7657" n="e" s="T572">beːbe. </ts>
               <ts e="T574" id="Seg_7659" n="e" s="T573">Kele </ts>
               <ts e="T575" id="Seg_7661" n="e" s="T574">tarpat </ts>
               <ts e="T576" id="Seg_7663" n="e" s="T575">itintiŋ. </ts>
               <ts e="T577" id="Seg_7665" n="e" s="T576">(Keli͡e-) </ts>
               <ts e="T578" id="Seg_7667" n="e" s="T577">(keli͡ene) </ts>
               <ts e="T579" id="Seg_7669" n="e" s="T578">hu͡ok </ts>
               <ts e="T580" id="Seg_7671" n="e" s="T579">bu͡olaːččɨ, </ts>
               <ts e="T581" id="Seg_7673" n="e" s="T580">kü͡ömejderin </ts>
               <ts e="T582" id="Seg_7675" n="e" s="T581">öjdöːböppün, </ts>
               <ts e="T583" id="Seg_7677" n="e" s="T582">umnubuppun. </ts>
               <ts e="T584" id="Seg_7679" n="e" s="T583">Urut </ts>
               <ts e="T585" id="Seg_7681" n="e" s="T584">bu͡ollagɨna </ts>
               <ts e="T586" id="Seg_7683" n="e" s="T585">gininen </ts>
               <ts e="T587" id="Seg_7685" n="e" s="T586">zanʼimaːtsalɨ͡am </ts>
               <ts e="T588" id="Seg_7687" n="e" s="T587">di͡emmin, </ts>
               <ts e="T589" id="Seg_7689" n="e" s="T588">olus </ts>
               <ts e="T590" id="Seg_7691" n="e" s="T589">dʼelajdaːbakka </ts>
               <ts e="T591" id="Seg_7693" n="e" s="T590">hɨldʼɨbɨtɨm </ts>
               <ts e="T592" id="Seg_7695" n="e" s="T591">bu͡olla, </ts>
               <ts e="T593" id="Seg_7697" n="e" s="T592">ol </ts>
               <ts e="T594" id="Seg_7699" n="e" s="T593">ihin </ts>
               <ts e="T595" id="Seg_7701" n="e" s="T594">umnabɨn. </ts>
               <ts e="T596" id="Seg_7703" n="e" s="T595">SONG. </ts>
               <ts e="T597" id="Seg_7705" n="e" s="T596">SONG. </ts>
               <ts e="T598" id="Seg_7707" n="e" s="T597">SONG. </ts>
               <ts e="T599" id="Seg_7709" n="e" s="T598">SONG. </ts>
               <ts e="T600" id="Seg_7711" n="e" s="T599">SONG. </ts>
               <ts e="T601" id="Seg_7713" n="e" s="T600">SONG. </ts>
               <ts e="T602" id="Seg_7715" n="e" s="T601">SONG. </ts>
               <ts e="T603" id="Seg_7717" n="e" s="T602">SONG. </ts>
               <ts e="T604" id="Seg_7719" n="e" s="T603">SONG. </ts>
               <ts e="T605" id="Seg_7721" n="e" s="T604">SONG. </ts>
               <ts e="T606" id="Seg_7723" n="e" s="T605">SONG. </ts>
               <ts e="T607" id="Seg_7725" n="e" s="T606">SONG. </ts>
               <ts e="T608" id="Seg_7727" n="e" s="T607">SONG. </ts>
            </ts>
            <ts e="T621" id="Seg_7728" n="sc" s="T610">
               <ts e="T611" id="Seg_7730" n="e" s="T610">– Iti </ts>
               <ts e="T612" id="Seg_7732" n="e" s="T611">ölbüte, </ts>
               <ts e="T613" id="Seg_7734" n="e" s="T612">iti </ts>
               <ts e="T614" id="Seg_7736" n="e" s="T613">Baja </ts>
               <ts e="T615" id="Seg_7738" n="e" s="T614">di͡en </ts>
               <ts e="T616" id="Seg_7740" n="e" s="T615">kɨːs </ts>
               <ts e="T617" id="Seg_7742" n="e" s="T616">ɨllaːbɨta. </ts>
               <ts e="T618" id="Seg_7744" n="e" s="T617">ɨllaːbɨt </ts>
               <ts e="T619" id="Seg_7746" n="e" s="T618">u͡ola </ts>
               <ts e="T620" id="Seg_7748" n="e" s="T619">hin </ts>
               <ts e="T621" id="Seg_7750" n="e" s="T620">ölbüte. </ts>
            </ts>
            <ts e="T716" id="Seg_7751" n="sc" s="T641">
               <ts e="T642" id="Seg_7753" n="e" s="T641">– Heː, </ts>
               <ts e="T643" id="Seg_7755" n="e" s="T642">onnuktarɨnan </ts>
               <ts e="T644" id="Seg_7757" n="e" s="T643">Toːnʼanɨ </ts>
               <ts e="T645" id="Seg_7759" n="e" s="T644">gɨtta </ts>
               <ts e="T646" id="Seg_7761" n="e" s="T645">hin, </ts>
               <ts e="T647" id="Seg_7763" n="e" s="T646">eː, </ts>
               <ts e="T648" id="Seg_7765" n="e" s="T647">tu͡oktu͡opput, </ts>
               <ts e="T649" id="Seg_7767" n="e" s="T648">hɨrsarɨ </ts>
               <ts e="T650" id="Seg_7769" n="e" s="T649">hin </ts>
               <ts e="T651" id="Seg_7771" n="e" s="T650">ɨllɨ͡appɨt </ts>
               <ts e="T652" id="Seg_7773" n="e" s="T651">bu͡olla. </ts>
               <ts e="T653" id="Seg_7775" n="e" s="T652">Meːne, </ts>
               <ts e="T654" id="Seg_7777" n="e" s="T653">tɨːna, </ts>
               <ts e="T655" id="Seg_7779" n="e" s="T654">tɨːna, </ts>
               <ts e="T656" id="Seg_7781" n="e" s="T655">eː, </ts>
               <ts e="T657" id="Seg_7783" n="e" s="T656">pʼerʼexvat dɨxanʼijanɨ – </ts>
               <ts e="T658" id="Seg_7785" n="e" s="T657">tɨːna </ts>
               <ts e="T659" id="Seg_7787" n="e" s="T658">tɨːnagɨn </ts>
               <ts e="T660" id="Seg_7789" n="e" s="T659">bu͡olla, </ts>
               <ts e="T661" id="Seg_7791" n="e" s="T660">da </ts>
               <ts e="T662" id="Seg_7793" n="e" s="T661">kanca </ts>
               <ts e="T663" id="Seg_7795" n="e" s="T662">ɨllaː </ts>
               <ts e="T664" id="Seg_7797" n="e" s="T663">di͡etekkine </ts>
               <ts e="T665" id="Seg_7799" n="e" s="T664">ɨllɨ͡am </ts>
               <ts e="T666" id="Seg_7801" n="e" s="T665">meːne. </ts>
               <ts e="T667" id="Seg_7803" n="e" s="T666">Tɨːn </ts>
               <ts e="T668" id="Seg_7805" n="e" s="T667">bɨldʼahar </ts>
               <ts e="T669" id="Seg_7807" n="e" s="T668">kohoːn. </ts>
               <ts e="T670" id="Seg_7809" n="e" s="T669">SONG. </ts>
               <ts e="T671" id="Seg_7811" n="e" s="T670">SONG. </ts>
               <ts e="T672" id="Seg_7813" n="e" s="T671">SONG. </ts>
               <ts e="T673" id="Seg_7815" n="e" s="T672">SONG. </ts>
               <ts e="T674" id="Seg_7817" n="e" s="T673">SONG. </ts>
               <ts e="T675" id="Seg_7819" n="e" s="T674">SONG. </ts>
               <ts e="T676" id="Seg_7821" n="e" s="T675">SONG. </ts>
               <ts e="T677" id="Seg_7823" n="e" s="T676">SONG. </ts>
               <ts e="T678" id="Seg_7825" n="e" s="T677">SONG. </ts>
               <ts e="T679" id="Seg_7827" n="e" s="T678">SONG. </ts>
               <ts e="T680" id="Seg_7829" n="e" s="T679">SONG. </ts>
               <ts e="T681" id="Seg_7831" n="e" s="T680">SONG. </ts>
               <ts e="T682" id="Seg_7833" n="e" s="T681">SONG. </ts>
               <ts e="T683" id="Seg_7835" n="e" s="T682">SONG. </ts>
               <ts e="T684" id="Seg_7837" n="e" s="T683">SONG. </ts>
               <ts e="T685" id="Seg_7839" n="e" s="T684">– Itini </ts>
               <ts e="T686" id="Seg_7841" n="e" s="T685">bu͡olla </ts>
               <ts e="T687" id="Seg_7843" n="e" s="T686">hogotok </ts>
               <ts e="T688" id="Seg_7845" n="e" s="T687">tɨːnnan </ts>
               <ts e="T689" id="Seg_7847" n="e" s="T688">ɨllaːččɨlar </ts>
               <ts e="T690" id="Seg_7849" n="e" s="T689">ühü </ts>
               <ts e="T691" id="Seg_7851" n="e" s="T690">bɨlɨrgɨlar, </ts>
               <ts e="T692" id="Seg_7853" n="e" s="T691">hogotok </ts>
               <ts e="T693" id="Seg_7855" n="e" s="T692">tɨːnnan, </ts>
               <ts e="T694" id="Seg_7857" n="e" s="T693">tɨːn </ts>
               <ts e="T695" id="Seg_7859" n="e" s="T694">bɨldʼahannar, </ts>
               <ts e="T696" id="Seg_7861" n="e" s="T695">dʼe </ts>
               <ts e="T697" id="Seg_7863" n="e" s="T696">ginner </ts>
               <ts e="T698" id="Seg_7865" n="e" s="T697">oːnnʼuːrdara </ts>
               <ts e="T699" id="Seg_7867" n="e" s="T698">ol </ts>
               <ts e="T700" id="Seg_7869" n="e" s="T699">bu͡olla, </ts>
               <ts e="T701" id="Seg_7871" n="e" s="T700">bu </ts>
               <ts e="T702" id="Seg_7873" n="e" s="T701">artʼistar </ts>
               <ts e="T703" id="Seg_7875" n="e" s="T702">anɨ </ts>
               <ts e="T704" id="Seg_7877" n="e" s="T703">oːnnʼuːllarɨn </ts>
               <ts e="T705" id="Seg_7879" n="e" s="T704">kördük. </ts>
               <ts e="T706" id="Seg_7881" n="e" s="T705">Hiti </ts>
               <ts e="T707" id="Seg_7883" n="e" s="T706">hogotok </ts>
               <ts e="T708" id="Seg_7885" n="e" s="T707">tɨːnnan </ts>
               <ts e="T709" id="Seg_7887" n="e" s="T708">tijeːčči </ts>
               <ts e="T710" id="Seg_7889" n="e" s="T709">ete, </ts>
               <ts e="T711" id="Seg_7891" n="e" s="T710">ühü, </ts>
               <ts e="T712" id="Seg_7893" n="e" s="T711">Iniki͡en </ts>
               <ts e="T713" id="Seg_7895" n="e" s="T712">di͡en </ts>
               <ts e="T714" id="Seg_7897" n="e" s="T713">kihi – </ts>
               <ts e="T715" id="Seg_7899" n="e" s="T714">iti </ts>
               <ts e="T716" id="Seg_7901" n="e" s="T715">itinige. </ts>
            </ts>
            <ts e="T731" id="Seg_7902" n="sc" s="T727">
               <ts e="T728" id="Seg_7904" n="e" s="T727">– Iti </ts>
               <ts e="T729" id="Seg_7906" n="e" s="T728">mini͡ene </ts>
               <ts e="T730" id="Seg_7908" n="e" s="T729">kɨːhɨm </ts>
               <ts e="T731" id="Seg_7910" n="e" s="T730">iti. </ts>
            </ts>
            <ts e="T789" id="Seg_7911" n="sc" s="T760">
               <ts e="T761" id="Seg_7913" n="e" s="T760">– Heː, </ts>
               <ts e="T762" id="Seg_7915" n="e" s="T761">dʼe </ts>
               <ts e="T763" id="Seg_7917" n="e" s="T762">ol </ts>
               <ts e="T764" id="Seg_7919" n="e" s="T763">min </ts>
               <ts e="T765" id="Seg_7921" n="e" s="T764">kepseːbit </ts>
               <ts e="T766" id="Seg_7923" n="e" s="T765">oloŋkobun </ts>
               <ts e="T767" id="Seg_7925" n="e" s="T766">"Koptoːk </ts>
               <ts e="T768" id="Seg_7927" n="e" s="T767">Taiskanɨ" </ts>
               <ts e="T769" id="Seg_7929" n="e" s="T768">kepseːbitter. </ts>
               <ts e="T770" id="Seg_7931" n="e" s="T769">Minigin </ts>
               <ts e="T771" id="Seg_7933" n="e" s="T770">kepseten </ts>
               <ts e="T772" id="Seg_7935" n="e" s="T771">baraːn, </ts>
               <ts e="T773" id="Seg_7937" n="e" s="T772">kelbittere </ts>
               <ts e="T774" id="Seg_7939" n="e" s="T773">manna </ts>
               <ts e="T775" id="Seg_7941" n="e" s="T774">keleːriler, </ts>
               <ts e="T776" id="Seg_7943" n="e" s="T775">(kutta) </ts>
               <ts e="T777" id="Seg_7945" n="e" s="T776">dʼɨlga </ts>
               <ts e="T778" id="Seg_7947" n="e" s="T777">kelbittere </ts>
               <ts e="T779" id="Seg_7949" n="e" s="T778">oːnnʼoːrular. </ts>
               <ts e="T780" id="Seg_7951" n="e" s="T779">Ontularɨn </ts>
               <ts e="T781" id="Seg_7953" n="e" s="T780">daːgɨnɨ </ts>
               <ts e="T782" id="Seg_7955" n="e" s="T781">iti </ts>
               <ts e="T783" id="Seg_7957" n="e" s="T782">kajdak, </ts>
               <ts e="T784" id="Seg_7959" n="e" s="T783">kajdak </ts>
               <ts e="T785" id="Seg_7961" n="e" s="T784">kepseːn </ts>
               <ts e="T786" id="Seg_7963" n="e" s="T785">keːspit </ts>
               <ts e="T787" id="Seg_7965" n="e" s="T786">etilere </ts>
               <ts e="T788" id="Seg_7967" n="e" s="T787">onton </ts>
               <ts e="T789" id="Seg_7969" n="e" s="T788">obu͡oja. </ts>
            </ts>
            <ts e="T813" id="Seg_7970" n="sc" s="T792">
               <ts e="T793" id="Seg_7972" n="e" s="T792">– Eː, </ts>
               <ts e="T794" id="Seg_7974" n="e" s="T793">istibitim, </ts>
               <ts e="T795" id="Seg_7976" n="e" s="T794">onu </ts>
               <ts e="T796" id="Seg_7978" n="e" s="T795">istemmin </ts>
               <ts e="T797" id="Seg_7980" n="e" s="T796">gɨnabɨn </ts>
               <ts e="T798" id="Seg_7982" n="e" s="T797">iti, </ts>
               <ts e="T799" id="Seg_7984" n="e" s="T798">((LAUGH)). </ts>
               <ts e="T800" id="Seg_7986" n="e" s="T799">(Oloru </ts>
               <ts e="T801" id="Seg_7988" n="e" s="T800">ke-) </ts>
               <ts e="T802" id="Seg_7990" n="e" s="T801">Oloru </ts>
               <ts e="T803" id="Seg_7992" n="e" s="T802">keleller, </ts>
               <ts e="T804" id="Seg_7994" n="e" s="T803">oloru </ts>
               <ts e="T805" id="Seg_7996" n="e" s="T804">hemeliːbin: </ts>
               <ts e="T806" id="Seg_7998" n="e" s="T805">"Togo </ts>
               <ts e="T807" id="Seg_8000" n="e" s="T806">ke </ts>
               <ts e="T808" id="Seg_8002" n="e" s="T807">iti </ts>
               <ts e="T809" id="Seg_8004" n="e" s="T808">onon-manan </ts>
               <ts e="T810" id="Seg_8006" n="e" s="T809">kepsiːgit, </ts>
               <ts e="T811" id="Seg_8008" n="e" s="T810">ileliː </ts>
               <ts e="T812" id="Seg_8010" n="e" s="T811">kepseːbekkit </ts>
               <ts e="T813" id="Seg_8012" n="e" s="T812">oloŋkogutun?" </ts>
            </ts>
            <ts e="T826" id="Seg_8013" n="sc" s="T819">
               <ts e="T820" id="Seg_8015" n="e" s="T819">– Vsʼo ravno </ts>
               <ts e="T821" id="Seg_8017" n="e" s="T820">hin </ts>
               <ts e="T822" id="Seg_8019" n="e" s="T821">dʼüːllener </ts>
               <ts e="T823" id="Seg_8021" n="e" s="T822">onton, </ts>
               <ts e="T824" id="Seg_8023" n="e" s="T823">asnavnojɨ </ts>
               <ts e="T825" id="Seg_8025" n="e" s="T824">hin </ts>
               <ts e="T826" id="Seg_8027" n="e" s="T825">kepsiːller. </ts>
            </ts>
            <ts e="T883" id="Seg_8028" n="sc" s="T848">
               <ts e="T849" id="Seg_8030" n="e" s="T848">– Eː, </ts>
               <ts e="T850" id="Seg_8032" n="e" s="T849">baːr </ts>
               <ts e="T851" id="Seg_8034" n="e" s="T850">eːt. </ts>
               <ts e="T852" id="Seg_8036" n="e" s="T851">Ol </ts>
               <ts e="T853" id="Seg_8038" n="e" s="T852">oloŋkonu </ts>
               <ts e="T854" id="Seg_8040" n="e" s="T853">biler </ts>
               <ts e="T855" id="Seg_8042" n="e" s="T854">min </ts>
               <ts e="T856" id="Seg_8044" n="e" s="T855">ubajɨm </ts>
               <ts e="T857" id="Seg_8046" n="e" s="T856">baːr, </ts>
               <ts e="T858" id="Seg_8048" n="e" s="T857">ɨbajdarɨm </ts>
               <ts e="T859" id="Seg_8050" n="e" s="T858">onton </ts>
               <ts e="T860" id="Seg_8052" n="e" s="T859">ikki͡en </ts>
               <ts e="T861" id="Seg_8054" n="e" s="T860">da </ts>
               <ts e="T862" id="Seg_8056" n="e" s="T861">bileller, </ts>
               <ts e="T863" id="Seg_8058" n="e" s="T862">ikki </ts>
               <ts e="T864" id="Seg_8060" n="e" s="T863">ubajdaːkpɨn. </ts>
               <ts e="T865" id="Seg_8062" n="e" s="T864">Ikki͡en </ts>
               <ts e="T866" id="Seg_8064" n="e" s="T865">da </ts>
               <ts e="T867" id="Seg_8066" n="e" s="T866">(bile-), </ts>
               <ts e="T868" id="Seg_8068" n="e" s="T867">biːrgehe </ts>
               <ts e="T869" id="Seg_8070" n="e" s="T868">kɨrdžagas, </ts>
               <ts e="T870" id="Seg_8072" n="e" s="T869">biːrgehe </ts>
               <ts e="T871" id="Seg_8074" n="e" s="T870">miniginneːger </ts>
               <ts e="T872" id="Seg_8076" n="e" s="T871">hin </ts>
               <ts e="T873" id="Seg_8078" n="e" s="T872">ubaj. </ts>
               <ts e="T874" id="Seg_8080" n="e" s="T873">Bileller </ts>
               <ts e="T875" id="Seg_8082" n="e" s="T874">olor </ts>
               <ts e="T876" id="Seg_8084" n="e" s="T875">oloŋkoloru, </ts>
               <ts e="T877" id="Seg_8086" n="e" s="T876">ɨrɨ͡alaːktarɨ </ts>
               <ts e="T878" id="Seg_8088" n="e" s="T877">daː </ts>
               <ts e="T879" id="Seg_8090" n="e" s="T878">bileller, </ts>
               <ts e="T880" id="Seg_8092" n="e" s="T879">ɨrɨ͡ata </ts>
               <ts e="T881" id="Seg_8094" n="e" s="T880">daː </ts>
               <ts e="T882" id="Seg_8096" n="e" s="T881">hu͡ogu </ts>
               <ts e="T883" id="Seg_8098" n="e" s="T882">(bile-). </ts>
            </ts>
            <ts e="T918" id="Seg_8099" n="sc" s="T887">
               <ts e="T888" id="Seg_8101" n="e" s="T887">– Meːne </ts>
               <ts e="T889" id="Seg_8103" n="e" s="T888">kihiler, </ts>
               <ts e="T890" id="Seg_8105" n="e" s="T889">iti </ts>
               <ts e="T891" id="Seg_8107" n="e" s="T890">urut </ts>
               <ts e="T892" id="Seg_8109" n="e" s="T891">oloŋkohut </ts>
               <ts e="T893" id="Seg_8111" n="e" s="T892">kihilerbit </ts>
               <ts e="T894" id="Seg_8113" n="e" s="T893">barannɨlar </ts>
               <ts e="T895" id="Seg_8115" n="e" s="T894">bu͡olla, </ts>
               <ts e="T896" id="Seg_8117" n="e" s="T895">anɨ </ts>
               <ts e="T897" id="Seg_8119" n="e" s="T896">ile, </ts>
               <ts e="T898" id="Seg_8121" n="e" s="T897">ile </ts>
               <ts e="T899" id="Seg_8123" n="e" s="T898">min </ts>
               <ts e="T900" id="Seg_8125" n="e" s="T899">biler </ts>
               <ts e="T901" id="Seg_8127" n="e" s="T900">oloŋkohut </ts>
               <ts e="T902" id="Seg_8129" n="e" s="T901">kihilerim </ts>
               <ts e="T903" id="Seg_8131" n="e" s="T902">barannɨlar. </ts>
               <ts e="T904" id="Seg_8133" n="e" s="T903">Kim </ts>
               <ts e="T905" id="Seg_8135" n="e" s="T904">baːra </ts>
               <ts e="T906" id="Seg_8137" n="e" s="T905">bu͡olu͡oj </ts>
               <ts e="T907" id="Seg_8139" n="e" s="T906">anɨ </ts>
               <ts e="T908" id="Seg_8141" n="e" s="T907">oloŋkohut </ts>
               <ts e="T909" id="Seg_8143" n="e" s="T908">kihi? </ts>
               <ts e="T910" id="Seg_8145" n="e" s="T909">Kimi </ts>
               <ts e="T911" id="Seg_8147" n="e" s="T910">da </ts>
               <ts e="T912" id="Seg_8149" n="e" s="T911">bilbeppin </ts>
               <ts e="T913" id="Seg_8151" n="e" s="T912">anɨ </ts>
               <ts e="T914" id="Seg_8153" n="e" s="T913">oloŋkohut </ts>
               <ts e="T915" id="Seg_8155" n="e" s="T914">kihini, </ts>
               <ts e="T916" id="Seg_8157" n="e" s="T915">ile </ts>
               <ts e="T917" id="Seg_8159" n="e" s="T916">oloŋkohut </ts>
               <ts e="T918" id="Seg_8161" n="e" s="T917">kihini. </ts>
            </ts>
            <ts e="T980" id="Seg_8162" n="sc" s="T932">
               <ts e="T933" id="Seg_8164" n="e" s="T932">– Hu͡ok, </ts>
               <ts e="T934" id="Seg_8166" n="e" s="T933">hu͡ok </ts>
               <ts e="T935" id="Seg_8168" n="e" s="T934">urut </ts>
               <ts e="T936" id="Seg_8170" n="e" s="T935">bu͡olla </ts>
               <ts e="T937" id="Seg_8172" n="e" s="T936">kimnere </ts>
               <ts e="T938" id="Seg_8174" n="e" s="T937">daːganɨ </ts>
               <ts e="T939" id="Seg_8176" n="e" s="T938">itigirdikeːčen </ts>
               <ts e="T940" id="Seg_8178" n="e" s="T939">gɨmmatɨlar, </ts>
               <ts e="T941" id="Seg_8180" n="e" s="T940">bu </ts>
               <ts e="T942" id="Seg_8182" n="e" s="T941">anɨ, </ts>
               <ts e="T943" id="Seg_8184" n="e" s="T942">bu. </ts>
               <ts e="T944" id="Seg_8186" n="e" s="T943">Bɨlɨrɨːŋŋɨttan </ts>
               <ts e="T945" id="Seg_8188" n="e" s="T944">bettek </ts>
               <ts e="T946" id="Seg_8190" n="e" s="T945">bu </ts>
               <ts e="T947" id="Seg_8192" n="e" s="T946">ide </ts>
               <ts e="T948" id="Seg_8194" n="e" s="T947">bullular, </ts>
               <ts e="T949" id="Seg_8196" n="e" s="T948">bu </ts>
               <ts e="T950" id="Seg_8198" n="e" s="T949">urukku </ts>
               <ts e="T951" id="Seg_8200" n="e" s="T950">oloŋko, </ts>
               <ts e="T952" id="Seg_8202" n="e" s="T951">ol </ts>
               <ts e="T953" id="Seg_8204" n="e" s="T952">ihin </ts>
               <ts e="T954" id="Seg_8206" n="e" s="T953">bihigi </ts>
               <ts e="T955" id="Seg_8208" n="e" s="T954">tahɨččɨ </ts>
               <ts e="T956" id="Seg_8210" n="e" s="T955">umnubupput </ts>
               <ts e="T957" id="Seg_8212" n="e" s="T956">onu, </ts>
               <ts e="T958" id="Seg_8214" n="e" s="T957">oloŋkolorbutun </ts>
               <ts e="T959" id="Seg_8216" n="e" s="T958">barɨtɨn </ts>
               <ts e="T960" id="Seg_8218" n="e" s="T959">umnan </ts>
               <ts e="T961" id="Seg_8220" n="e" s="T960">ihebit </ts>
               <ts e="T962" id="Seg_8222" n="e" s="T961">bu͡o, </ts>
               <ts e="T963" id="Seg_8224" n="e" s="T962">barɨtɨn. </ts>
               <ts e="T964" id="Seg_8226" n="e" s="T963">Kaččaga </ts>
               <ts e="T965" id="Seg_8228" n="e" s="T964">da </ts>
               <ts e="T966" id="Seg_8230" n="e" s="T965">kepseːbekkin. </ts>
               <ts e="T967" id="Seg_8232" n="e" s="T966">ɨrɨ͡anɨ </ts>
               <ts e="T968" id="Seg_8234" n="e" s="T967">da </ts>
               <ts e="T969" id="Seg_8236" n="e" s="T968">umnagɨn, </ts>
               <ts e="T970" id="Seg_8238" n="e" s="T969">oloŋkonu </ts>
               <ts e="T971" id="Seg_8240" n="e" s="T970">da </ts>
               <ts e="T972" id="Seg_8242" n="e" s="T971">umnagɨn. </ts>
               <ts e="T973" id="Seg_8244" n="e" s="T972">Ustorijanɨ </ts>
               <ts e="T974" id="Seg_8246" n="e" s="T973">kepsiːre </ts>
               <ts e="T975" id="Seg_8248" n="e" s="T974">bu͡olla </ts>
               <ts e="T976" id="Seg_8250" n="e" s="T975">Onʼiːsim </ts>
               <ts e="T977" id="Seg_8252" n="e" s="T976">ogonnʼor </ts>
               <ts e="T978" id="Seg_8254" n="e" s="T977">baːr, </ts>
               <ts e="T979" id="Seg_8256" n="e" s="T978">oloŋkohut </ts>
               <ts e="T980" id="Seg_8258" n="e" s="T979">emi͡e. </ts>
            </ts>
            <ts e="T1022" id="Seg_8259" n="sc" s="T990">
               <ts e="T991" id="Seg_8261" n="e" s="T990">– Tu͡ok </ts>
               <ts e="T992" id="Seg_8263" n="e" s="T991">bili͡ej </ts>
               <ts e="T993" id="Seg_8265" n="e" s="T992">da </ts>
               <ts e="T994" id="Seg_8267" n="e" s="T993">ol </ts>
               <ts e="T995" id="Seg_8269" n="e" s="T994">ginileri. </ts>
               <ts e="T996" id="Seg_8271" n="e" s="T995">Oloŋko </ts>
               <ts e="T997" id="Seg_8273" n="e" s="T996">iŋere </ts>
               <ts e="T998" id="Seg_8275" n="e" s="T997">dʼürü </ts>
               <ts e="T999" id="Seg_8277" n="e" s="T998">ginilerge </ts>
               <ts e="T1000" id="Seg_8279" n="e" s="T999">tu͡ok </ts>
               <ts e="T1001" id="Seg_8281" n="e" s="T1000">(bil-)? </ts>
               <ts e="T1002" id="Seg_8283" n="e" s="T1001">Čubu-čubu </ts>
               <ts e="T1003" id="Seg_8285" n="e" s="T1002">anɨ </ts>
               <ts e="T1004" id="Seg_8287" n="e" s="T1003">oloŋkolospopput </ts>
               <ts e="T1005" id="Seg_8289" n="e" s="T1004">bu͡o, </ts>
               <ts e="T1006" id="Seg_8291" n="e" s="T1005">"meːne </ts>
               <ts e="T1007" id="Seg_8293" n="e" s="T1006">kepseː", </ts>
               <ts e="T1008" id="Seg_8295" n="e" s="T1007">ere </ts>
               <ts e="T1009" id="Seg_8297" n="e" s="T1008">di͡etekterine </ts>
               <ts e="T1010" id="Seg_8299" n="e" s="T1009">kepsiːbin, </ts>
               <ts e="T1011" id="Seg_8301" n="e" s="T1010">naːdadaktarɨnan, </ts>
               <ts e="T1012" id="Seg_8303" n="e" s="T1011">kepsettekterine </ts>
               <ts e="T1013" id="Seg_8305" n="e" s="T1012">ere </ts>
               <ts e="T1014" id="Seg_8307" n="e" s="T1013">kepsiːgin </ts>
               <ts e="T1015" id="Seg_8309" n="e" s="T1014">bu͡o. </ts>
               <ts e="T1016" id="Seg_8311" n="e" s="T1015">Taːk </ts>
               <ts e="T1017" id="Seg_8313" n="e" s="T1016">da </ts>
               <ts e="T1018" id="Seg_8315" n="e" s="T1017">meːne </ts>
               <ts e="T1019" id="Seg_8317" n="e" s="T1018">kaččaga </ts>
               <ts e="T1020" id="Seg_8319" n="e" s="T1019">da </ts>
               <ts e="T1021" id="Seg_8321" n="e" s="T1020">oloŋkolospopput </ts>
               <ts e="T1022" id="Seg_8323" n="e" s="T1021">bu͡o. </ts>
            </ts>
            <ts e="T1042" id="Seg_8324" n="sc" s="T1041">
               <ts e="T1042" id="Seg_8326" n="e" s="T1041">– Heː. </ts>
            </ts>
            <ts e="T1060" id="Seg_8327" n="sc" s="T1053">
               <ts e="T1054" id="Seg_8329" n="e" s="T1053">– Kaja, </ts>
               <ts e="T1055" id="Seg_8331" n="e" s="T1054">atɨnna </ts>
               <ts e="T1056" id="Seg_8333" n="e" s="T1055">bu͡olumna. </ts>
               <ts e="T1060" id="Seg_8335" n="e" s="T1056">((TALE; UkET_19940424_OldWomanTaal_flk)). </ts>
            </ts>
            <ts e="T1113" id="Seg_8336" n="sc" s="T1074">
               <ts e="T1075" id="Seg_8338" n="e" s="T1074">– Ogom </ts>
               <ts e="T1076" id="Seg_8340" n="e" s="T1075">tuhunan </ts>
               <ts e="T1077" id="Seg_8342" n="e" s="T1076">küččüküːkeːn </ts>
               <ts e="T1078" id="Seg_8344" n="e" s="T1077">stʼix </ts>
               <ts e="T1079" id="Seg_8346" n="e" s="T1078">obu͡ojun </ts>
               <ts e="T1080" id="Seg_8348" n="e" s="T1079">oŋorbutum. </ts>
               <ts e="T1081" id="Seg_8350" n="e" s="T1080">Ajsülüː </ts>
               <ts e="T1082" id="Seg_8352" n="e" s="T1081">tuhunan. </ts>
               <ts e="T1083" id="Seg_8354" n="e" s="T1082">Sulustar </ts>
               <ts e="T1084" id="Seg_8356" n="e" s="T1083">kallaːŋŋa </ts>
               <ts e="T1085" id="Seg_8358" n="e" s="T1084">köstöllör. </ts>
               <ts e="T1086" id="Seg_8360" n="e" s="T1085">Utuj, </ts>
               <ts e="T1087" id="Seg_8362" n="e" s="T1086">utuj </ts>
               <ts e="T1088" id="Seg_8364" n="e" s="T1087">Ajsülüː. </ts>
               <ts e="T1089" id="Seg_8366" n="e" s="T1088">Üjebit </ts>
               <ts e="T1090" id="Seg_8368" n="e" s="T1089">ularɨjda, </ts>
               <ts e="T1091" id="Seg_8370" n="e" s="T1090">oduː </ts>
               <ts e="T1092" id="Seg_8372" n="e" s="T1091">atɨn </ts>
               <ts e="T1093" id="Seg_8374" n="e" s="T1092">kem </ts>
               <ts e="T1094" id="Seg_8376" n="e" s="T1093">kelle. </ts>
               <ts e="T1095" id="Seg_8378" n="e" s="T1094">Türgenkeːnnik </ts>
               <ts e="T1096" id="Seg_8380" n="e" s="T1095">ulaːtaːr, </ts>
               <ts e="T1097" id="Seg_8382" n="e" s="T1096">sappɨn, </ts>
               <ts e="T1098" id="Seg_8384" n="e" s="T1097">innebin </ts>
               <ts e="T1099" id="Seg_8386" n="e" s="T1098">tutaːr. </ts>
               <ts e="T1100" id="Seg_8388" n="e" s="T1099">Min </ts>
               <ts e="T1101" id="Seg_8390" n="e" s="T1100">üjebin </ts>
               <ts e="T1102" id="Seg_8392" n="e" s="T1101">üjeleneːr. </ts>
               <ts e="T1103" id="Seg_8394" n="e" s="T1102">Kihi͡eke </ts>
               <ts e="T1104" id="Seg_8396" n="e" s="T1103">kihi </ts>
               <ts e="T1105" id="Seg_8398" n="e" s="T1104">deteːr. </ts>
               <ts e="T1106" id="Seg_8400" n="e" s="T1105">Ogokoːnum </ts>
               <ts e="T1107" id="Seg_8402" n="e" s="T1106">ogotun </ts>
               <ts e="T1108" id="Seg_8404" n="e" s="T1107">bihigiger </ts>
               <ts e="T1109" id="Seg_8406" n="e" s="T1108">biliːbin. </ts>
               <ts e="T1110" id="Seg_8408" n="e" s="T1109">Attɨtɨgar </ts>
               <ts e="T1111" id="Seg_8410" n="e" s="T1110">olorobun, </ts>
               <ts e="T1112" id="Seg_8412" n="e" s="T1111">orgujakaːn </ts>
               <ts e="T1113" id="Seg_8414" n="e" s="T1112">ɨllannɨm. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-UkET">
            <ta e="T43" id="Seg_8415" s="T21">UkET_AkEE_19940424_SongsTales_conv.UkET.001 (001.004)</ta>
            <ta e="T52" id="Seg_8416" s="T43">UkET_AkEE_19940424_SongsTales_conv.UkET.002 (001.005)</ta>
            <ta e="T55" id="Seg_8417" s="T52">UkET_AkEE_19940424_SongsTales_conv.UkET.003 (001.006)</ta>
            <ta e="T68" id="Seg_8418" s="T55">UkET_AkEE_19940424_SongsTales_conv.UkET.004 (001.007)</ta>
            <ta e="T81" id="Seg_8419" s="T68">UkET_AkEE_19940424_SongsTales_conv.UkET.005 (001.008)</ta>
            <ta e="T82" id="Seg_8420" s="T81">UkET_AkEE_19940424_SongsTales_conv.UkET.006 (001.009)</ta>
            <ta e="T87" id="Seg_8421" s="T82">UkET_AkEE_19940424_SongsTales_conv.UkET.007 (001.010)</ta>
            <ta e="T91" id="Seg_8422" s="T87">UkET_AkEE_19940424_SongsTales_conv.UkET.008 (001.011)</ta>
            <ta e="T94" id="Seg_8423" s="T91">UkET_AkEE_19940424_SongsTales_conv.UkET.009 (001.012)</ta>
            <ta e="T109" id="Seg_8424" s="T94">UkET_AkEE_19940424_SongsTales_conv.UkET.010 (001.013)</ta>
            <ta e="T120" id="Seg_8425" s="T109">UkET_AkEE_19940424_SongsTales_conv.UkET.011 (001.014)</ta>
            <ta e="T125" id="Seg_8426" s="T120">UkET_AkEE_19940424_SongsTales_conv.UkET.012 (001.015)</ta>
            <ta e="T139" id="Seg_8427" s="T125">UkET_AkEE_19940424_SongsTales_conv.UkET.013 (001.016)</ta>
            <ta e="T155" id="Seg_8428" s="T139">UkET_AkEE_19940424_SongsTales_conv.UkET.014 (001.017)</ta>
            <ta e="T169" id="Seg_8429" s="T155">UkET_AkEE_19940424_SongsTales_conv.UkET.015 (001.018)</ta>
            <ta e="T183" id="Seg_8430" s="T169">UkET_AkEE_19940424_SongsTales_conv.UkET.016 (001.019)</ta>
            <ta e="T190" id="Seg_8431" s="T183">UkET_AkEE_19940424_SongsTales_conv.UkET.017 (001.020)</ta>
            <ta e="T209" id="Seg_8432" s="T202">UkET_AkEE_19940424_SongsTales_conv.UkET.018 (001.023)</ta>
            <ta e="T226" id="Seg_8433" s="T209">UkET_AkEE_19940424_SongsTales_conv.UkET.019 (001.024)</ta>
            <ta e="T233" id="Seg_8434" s="T226">UkET_AkEE_19940424_SongsTales_conv.UkET.020 (001.025)</ta>
            <ta e="T251" id="Seg_8435" s="T233">UkET_AkEE_19940424_SongsTales_conv.UkET.021 (001.026)</ta>
            <ta e="T253" id="Seg_8436" s="T251">UkET_AkEE_19940424_SongsTales_conv.UkET.022 (001.027)</ta>
            <ta e="T273" id="Seg_8437" s="T253">UkET_AkEE_19940424_SongsTales_conv.UkET.023 (001.028)</ta>
            <ta e="T277" id="Seg_8438" s="T273">UkET_AkEE_19940424_SongsTales_conv.UkET.024 (001.029)</ta>
            <ta e="T315" id="Seg_8439" s="T299">UkET_AkEE_19940424_SongsTales_conv.UkET.025 (001.032)</ta>
            <ta e="T321" id="Seg_8440" s="T315">UkET_AkEE_19940424_SongsTales_conv.UkET.026 (001.033)</ta>
            <ta e="T340" id="Seg_8441" s="T321">UkET_AkEE_19940424_SongsTales_conv.UkET.027 (001.034)</ta>
            <ta e="T348" id="Seg_8442" s="T340">UkET_AkEE_19940424_SongsTales_conv.UkET.028 (001.035)</ta>
            <ta e="T361" id="Seg_8443" s="T355">UkET_AkEE_19940424_SongsTales_conv.UkET.029 (001.037)</ta>
            <ta e="T384" id="Seg_8444" s="T361">UkET_AkEE_19940424_SongsTales_conv.UkET.030 (001.038)</ta>
            <ta e="T387" id="Seg_8445" s="T384">UkET_AkEE_19940424_SongsTales_conv.UkET.031 (001.039)</ta>
            <ta e="T395" id="Seg_8446" s="T387">UkET_AkEE_19940424_SongsTales_conv.UkET.032 (001.040)</ta>
            <ta e="T401" id="Seg_8447" s="T395">UkET_AkEE_19940424_SongsTales_conv.UkET.033 (001.041)</ta>
            <ta e="T406" id="Seg_8448" s="T401">UkET_AkEE_19940424_SongsTales_conv.UkET.034 (001.042)</ta>
            <ta e="T413" id="Seg_8449" s="T406">UkET_AkEE_19940424_SongsTales_conv.UkET.035 (001.043)</ta>
            <ta e="T423" id="Seg_8450" s="T413">UkET_AkEE_19940424_SongsTales_conv.UkET.036 (001.044)</ta>
            <ta e="T439" id="Seg_8451" s="T423">UkET_AkEE_19940424_SongsTales_conv.UkET.037 (001.045)</ta>
            <ta e="T445" id="Seg_8452" s="T439">UkET_AkEE_19940424_SongsTales_conv.UkET.038 (001.046)</ta>
            <ta e="T465" id="Seg_8453" s="T445">UkET_AkEE_19940424_SongsTales_conv.UkET.039 (001.047)</ta>
            <ta e="T487" id="Seg_8454" s="T465">UkET_AkEE_19940424_SongsTales_conv.UkET.040 (001.048)</ta>
            <ta e="T500" id="Seg_8455" s="T487">UkET_AkEE_19940424_SongsTales_conv.UkET.041 (001.049)</ta>
            <ta e="T506" id="Seg_8456" s="T500">UkET_AkEE_19940424_SongsTales_conv.UkET.042 (001.050)</ta>
            <ta e="T522" id="Seg_8457" s="T517">UkET_AkEE_19940424_SongsTales_conv.UkET.043 (001.052)</ta>
            <ta e="T526" id="Seg_8458" s="T522">UkET_AkEE_19940424_SongsTales_conv.UkET.044 (001.053)</ta>
            <ta e="T536" id="Seg_8459" s="T526">UkET_AkEE_19940424_SongsTales_conv.UkET.045 (001.054)</ta>
            <ta e="T537" id="Seg_8460" s="T536">UkET_AkEE_19940424_SongsTales_conv.UkET.046 (001.055)</ta>
            <ta e="T540" id="Seg_8461" s="T537">UkET_AkEE_19940424_SongsTales_conv.UkET.047 (001.056)</ta>
            <ta e="T561" id="Seg_8462" s="T558">UkET_AkEE_19940424_SongsTales_conv.UkET.048 (001.059)</ta>
            <ta e="T569" id="Seg_8463" s="T561">UkET_AkEE_19940424_SongsTales_conv.UkET.049 (001.060)</ta>
            <ta e="T573" id="Seg_8464" s="T569">UkET_AkEE_19940424_SongsTales_conv.UkET.050 (001.061)</ta>
            <ta e="T576" id="Seg_8465" s="T573">UkET_AkEE_19940424_SongsTales_conv.UkET.051 (001.062)</ta>
            <ta e="T583" id="Seg_8466" s="T576">UkET_AkEE_19940424_SongsTales_conv.UkET.052 (001.063)</ta>
            <ta e="T595" id="Seg_8467" s="T583">UkET_AkEE_19940424_SongsTales_conv.UkET.053 (001.064)</ta>
            <ta e="T596" id="Seg_8468" s="T595">UkET_AkEE_19940424_SongsTales_conv.UkET.054 (002)</ta>
            <ta e="T597" id="Seg_8469" s="T596">UkET_AkEE_19940424_SongsTales_conv.UkET.055 (003)</ta>
            <ta e="T598" id="Seg_8470" s="T597">UkET_AkEE_19940424_SongsTales_conv.UkET.056 (004)</ta>
            <ta e="T599" id="Seg_8471" s="T598">UkET_AkEE_19940424_SongsTales_conv.UkET.057 (005)</ta>
            <ta e="T600" id="Seg_8472" s="T599">UkET_AkEE_19940424_SongsTales_conv.UkET.058 (006)</ta>
            <ta e="T601" id="Seg_8473" s="T600">UkET_AkEE_19940424_SongsTales_conv.UkET.059 (007)</ta>
            <ta e="T602" id="Seg_8474" s="T601">UkET_AkEE_19940424_SongsTales_conv.UkET.060 (008)</ta>
            <ta e="T603" id="Seg_8475" s="T602">UkET_AkEE_19940424_SongsTales_conv.UkET.061 (009)</ta>
            <ta e="T604" id="Seg_8476" s="T603">UkET_AkEE_19940424_SongsTales_conv.UkET.062 (010)</ta>
            <ta e="T605" id="Seg_8477" s="T604">UkET_AkEE_19940424_SongsTales_conv.UkET.063 (011)</ta>
            <ta e="T606" id="Seg_8478" s="T605">UkET_AkEE_19940424_SongsTales_conv.UkET.064 (012)</ta>
            <ta e="T607" id="Seg_8479" s="T606">UkET_AkEE_19940424_SongsTales_conv.UkET.065 (013)</ta>
            <ta e="T608" id="Seg_8480" s="T607">UkET_AkEE_19940424_SongsTales_conv.UkET.066 (014)</ta>
            <ta e="T617" id="Seg_8481" s="T610">UkET_AkEE_19940424_SongsTales_conv.UkET.067 (015.002)</ta>
            <ta e="T621" id="Seg_8482" s="T617">UkET_AkEE_19940424_SongsTales_conv.UkET.068 (015.003)</ta>
            <ta e="T652" id="Seg_8483" s="T641">UkET_AkEE_19940424_SongsTales_conv.UkET.069 (015.005)</ta>
            <ta e="T666" id="Seg_8484" s="T652">UkET_AkEE_19940424_SongsTales_conv.UkET.070 (015.006)</ta>
            <ta e="T669" id="Seg_8485" s="T666">UkET_AkEE_19940424_SongsTales_conv.UkET.071 (015.007)</ta>
            <ta e="T670" id="Seg_8486" s="T669">UkET_AkEE_19940424_SongsTales_conv.UkET.072 (016)</ta>
            <ta e="T671" id="Seg_8487" s="T670">UkET_AkEE_19940424_SongsTales_conv.UkET.073 (017)</ta>
            <ta e="T672" id="Seg_8488" s="T671">UkET_AkEE_19940424_SongsTales_conv.UkET.074 (018)</ta>
            <ta e="T673" id="Seg_8489" s="T672">UkET_AkEE_19940424_SongsTales_conv.UkET.075 (019)</ta>
            <ta e="T674" id="Seg_8490" s="T673">UkET_AkEE_19940424_SongsTales_conv.UkET.076 (020)</ta>
            <ta e="T675" id="Seg_8491" s="T674">UkET_AkEE_19940424_SongsTales_conv.UkET.077 (021)</ta>
            <ta e="T676" id="Seg_8492" s="T675">UkET_AkEE_19940424_SongsTales_conv.UkET.078 (022)</ta>
            <ta e="T677" id="Seg_8493" s="T676">UkET_AkEE_19940424_SongsTales_conv.UkET.079 (023)</ta>
            <ta e="T678" id="Seg_8494" s="T677">UkET_AkEE_19940424_SongsTales_conv.UkET.080 (024)</ta>
            <ta e="T679" id="Seg_8495" s="T678">UkET_AkEE_19940424_SongsTales_conv.UkET.081 (025)</ta>
            <ta e="T680" id="Seg_8496" s="T679">UkET_AkEE_19940424_SongsTales_conv.UkET.082 (026)</ta>
            <ta e="T681" id="Seg_8497" s="T680">UkET_AkEE_19940424_SongsTales_conv.UkET.083 (027)</ta>
            <ta e="T682" id="Seg_8498" s="T681">UkET_AkEE_19940424_SongsTales_conv.UkET.084 (028)</ta>
            <ta e="T683" id="Seg_8499" s="T682">UkET_AkEE_19940424_SongsTales_conv.UkET.085 (029)</ta>
            <ta e="T684" id="Seg_8500" s="T683">UkET_AkEE_19940424_SongsTales_conv.UkET.086 (030)</ta>
            <ta e="T705" id="Seg_8501" s="T684">UkET_AkEE_19940424_SongsTales_conv.UkET.087 (031.001)</ta>
            <ta e="T716" id="Seg_8502" s="T705">UkET_AkEE_19940424_SongsTales_conv.UkET.088 (031.002)</ta>
            <ta e="T731" id="Seg_8503" s="T727">UkET_AkEE_19940424_SongsTales_conv.UkET.089 (031.004)</ta>
            <ta e="T769" id="Seg_8504" s="T760">UkET_AkEE_19940424_SongsTales_conv.UkET.090 (031.008)</ta>
            <ta e="T779" id="Seg_8505" s="T769">UkET_AkEE_19940424_SongsTales_conv.UkET.091 (031.009)</ta>
            <ta e="T789" id="Seg_8506" s="T779">UkET_AkEE_19940424_SongsTales_conv.UkET.092 (031.010)</ta>
            <ta e="T799" id="Seg_8507" s="T792">UkET_AkEE_19940424_SongsTales_conv.UkET.093 (031.012)</ta>
            <ta e="T805" id="Seg_8508" s="T799">UkET_AkEE_19940424_SongsTales_conv.UkET.094 (031.013)</ta>
            <ta e="T813" id="Seg_8509" s="T805">UkET_AkEE_19940424_SongsTales_conv.UkET.095 (031.014)</ta>
            <ta e="T826" id="Seg_8510" s="T819">UkET_AkEE_19940424_SongsTales_conv.UkET.096 (031.016)</ta>
            <ta e="T851" id="Seg_8511" s="T848">UkET_AkEE_19940424_SongsTales_conv.UkET.097 (031.018)</ta>
            <ta e="T864" id="Seg_8512" s="T851">UkET_AkEE_19940424_SongsTales_conv.UkET.098 (031.019)</ta>
            <ta e="T873" id="Seg_8513" s="T864">UkET_AkEE_19940424_SongsTales_conv.UkET.099 (031.020)</ta>
            <ta e="T883" id="Seg_8514" s="T873">UkET_AkEE_19940424_SongsTales_conv.UkET.100 (031.021)</ta>
            <ta e="T903" id="Seg_8515" s="T887">UkET_AkEE_19940424_SongsTales_conv.UkET.101 (031.023)</ta>
            <ta e="T909" id="Seg_8516" s="T903">UkET_AkEE_19940424_SongsTales_conv.UkET.102 (031.024)</ta>
            <ta e="T918" id="Seg_8517" s="T909">UkET_AkEE_19940424_SongsTales_conv.UkET.103 (031.025)</ta>
            <ta e="T943" id="Seg_8518" s="T932">UkET_AkEE_19940424_SongsTales_conv.UkET.104 (031.027)</ta>
            <ta e="T963" id="Seg_8519" s="T943">UkET_AkEE_19940424_SongsTales_conv.UkET.105 (031.028)</ta>
            <ta e="T966" id="Seg_8520" s="T963">UkET_AkEE_19940424_SongsTales_conv.UkET.106 (031.029)</ta>
            <ta e="T972" id="Seg_8521" s="T966">UkET_AkEE_19940424_SongsTales_conv.UkET.107 (031.030)</ta>
            <ta e="T980" id="Seg_8522" s="T972">UkET_AkEE_19940424_SongsTales_conv.UkET.108 (031.031)</ta>
            <ta e="T995" id="Seg_8523" s="T990">UkET_AkEE_19940424_SongsTales_conv.UkET.109 (031.033)</ta>
            <ta e="T1001" id="Seg_8524" s="T995">UkET_AkEE_19940424_SongsTales_conv.UkET.110 (031.034)</ta>
            <ta e="T1015" id="Seg_8525" s="T1001">UkET_AkEE_19940424_SongsTales_conv.UkET.111 (031.035)</ta>
            <ta e="T1022" id="Seg_8526" s="T1015">UkET_AkEE_19940424_SongsTales_conv.UkET.112 (031.036)</ta>
            <ta e="T1042" id="Seg_8527" s="T1041">UkET_AkEE_19940424_SongsTales_conv.UkET.113 (031.041)</ta>
            <ta e="T1056" id="Seg_8528" s="T1053">UkET_AkEE_19940424_SongsTales_conv.UkET.114 (031.044)</ta>
            <ta e="T1060" id="Seg_8529" s="T1056">UkET_AkEE_19940424_SongsTales_conv.UkET.115 (032)</ta>
            <ta e="T1080" id="Seg_8530" s="T1074">UkET_AkEE_19940424_SongsTales_conv.UkET.116 (033.003)</ta>
            <ta e="T1082" id="Seg_8531" s="T1080">UkET_AkEE_19940424_SongsTales_conv.UkET.117 (033.004)</ta>
            <ta e="T1085" id="Seg_8532" s="T1082">UkET_AkEE_19940424_SongsTales_conv.UkET.118 (034)</ta>
            <ta e="T1088" id="Seg_8533" s="T1085">UkET_AkEE_19940424_SongsTales_conv.UkET.119 (035)</ta>
            <ta e="T1094" id="Seg_8534" s="T1088">UkET_AkEE_19940424_SongsTales_conv.UkET.120 (036)</ta>
            <ta e="T1099" id="Seg_8535" s="T1094">UkET_AkEE_19940424_SongsTales_conv.UkET.121 (037)</ta>
            <ta e="T1102" id="Seg_8536" s="T1099">UkET_AkEE_19940424_SongsTales_conv.UkET.122 (038)</ta>
            <ta e="T1105" id="Seg_8537" s="T1102">UkET_AkEE_19940424_SongsTales_conv.UkET.123 (039)</ta>
            <ta e="T1109" id="Seg_8538" s="T1105">UkET_AkEE_19940424_SongsTales_conv.UkET.124 (040)</ta>
            <ta e="T1113" id="Seg_8539" s="T1109">UkET_AkEE_19940424_SongsTales_conv.UkET.125 (041)</ta>
         </annotation>
         <annotation name="st" tierref="st-UkET">
            <ta e="T43" id="Seg_8540" s="T21">Елена Трифоновна Уксусникова: Мин и-и бэйэм төрөөбүт һирим би (бу) диэк этэ, муора диэк, ол диэк, Нос..(Носкуо) ким диэк, э-э, онтон Ырыымнай оӈуоргутугар ити.</ta>
            <ta e="T52" id="Seg_8541" s="T43">ЕТ: Муора диэк төрөөбүтүм, онтон эргэ бараммын таас диэк һирдэммитим.</ta>
            <ta e="T55" id="Seg_8542" s="T52">ЕТ: Оголорум да туок…</ta>
            <ta e="T68" id="Seg_8543" s="T55">Кайдак кэпсээн да, тугу кэпсиэмий: Биир огом ити ушкуолага (ускуолага) үлэлиир, биир кыыһым директордиир.</ta>
            <ta e="T81" id="Seg_8544" s="T68">ЕТ: Биир, э, биир огом ити Носкуога, ула.. муӈ улакан кыыһым Носкуога ушкуолага (ускуолага) доктуурдуур.</ta>
            <ta e="T82" id="Seg_8545" s="T81">Ниина.</ta>
            <ta e="T87" id="Seg_8546" s="T82">ЕТ: Би-и.. ол улакан кыыһым аата.</ta>
            <ta e="T91" id="Seg_8547" s="T87">Куччугуй, онтум анныта Кээтэ.</ta>
            <ta e="T94" id="Seg_8548" s="T91">Ол ускуолага директэрдиир.</ta>
            <ta e="T109" id="Seg_8549" s="T94">ЕТ: Биир кыыһым һаадикка үлэлээччи этэ, онтум э-э, бу һадиктара, туоктара алдьанан анаан да оголонон, каньаан…</ta>
            <ta e="T120" id="Seg_8550" s="T109">ЕТ: Аны ыалдьан Дудинка диэк һылдьар бу, э-э, Норильскайга эмтэтэ һытар аны.</ta>
            <ta e="T125" id="Seg_8551" s="T120">ЕТ: Онтон биир уолум охотник буолла.</ta>
            <ta e="T139" id="Seg_8552" s="T125">ЕТ: Биир уолум ити һамолёттары ыык.. иттэлэрин ыыт.. туоктуур, оӈорор, һамолёттары иитэр, көтүтэр, итиннэ баар.</ta>
            <ta e="T155" id="Seg_8553" s="T139">ЕТ: Биир кыыһым ини.. туугу бүппүт этэ, һаадик үөрэгин, аны бэйэм кырдьаммын, онтубун бэйэбэр үлэлэтээрибин тыага тутабын. </ta>
            <ta e="T169" id="Seg_8554" s="T155">ЕТ:Огонньорум урут булчут этэ һин киһиэ.., киһиттэн, э-э, Ырымнайга оттон (онтон) һопкуоска передовойынан бултааччы этэ.</ta>
            <ta e="T183" id="Seg_8555" s="T169">ЕТ: Аны ыалдьан, илиитэ ыалдьан ол булдуттан да тагыста, онуга эбиилээн аны "аактага", пенсияга киирдэ. </ta>
            <ta e="T190" id="Seg_8556" s="T183">Бу аны бу дьылларга киирбитэ.</ta>
            <ta e="T209" id="Seg_8557" s="T202">ЕТ: Һэ-э, аны бу оголорум, огом оголорун карайаммын…</ta>
            <ta e="T226" id="Seg_8558" s="T209">Ол эмӈэ кэлээччи огом гиэнэ үс оголоок буолла, онтуларым оготун карайа һытар этим буо, аны буоллагына бу…</ta>
            <ta e="T233" id="Seg_8559" s="T226">ET: Oгом ол, Маачам кэлэн кэллим, ол тыаттан.</ta>
            <ta e="T251" id="Seg_8560" s="T233">ЕТ: Тыага тута һытабын буолла, ол үс огобун, ол маамата бу диэк баар, тээтэлэрэ аракпыта буолла, ол дьураак этэ.</ta>
            <ta e="T253" id="Seg_8561" s="T251">ЕТ: Онтута аракпыта.</ta>
            <ta e="T273" id="Seg_8562" s="T253">ЕТ: Ол иһин ол кантан бииртэн биир ого буолла, оголорум оголоро каччага да араппаттар буо, һайын, һайыннары, кыһыннары һупту оголоок буолааччыбын.</ta>
            <ta e="T277" id="Seg_8563" s="T273">ЕТ: Внуктарым биэк баар буолаллар.</ta>
            <ta e="T315" id="Seg_8564" s="T299">ЕТ: Һэ-э, кэпсээччибит буо иттэкээни киэ, аны улаканнык да туолкулана иликтэр ээт, улакан кыыһым эрэ оголоро ускуолага барбыттара.</ta>
            <ta e="T321" id="Seg_8565" s="T315">ЕТ: Куччугуй оголорум оголоро киирэл иликтэр ускуолага.</ta>
            <ta e="T340" id="Seg_8566" s="T321">ЕТ: Олорго ол иттэкээни кэпсиибин буо: иттэни- бүттэни, эбээ, диэн эргиччийэ һылдьааччылар буо – онтугун кэпсээ, ону олоӈколоо, муну кэпсээ, итини ыллаа.</ta>
            <ta e="T348" id="Seg_8567" s="T340">ЕТ: Олору һаататаарыгын киэ ылланыӈ, кэпсэтиӈ – һин койуу буолла.</ta>
            <ta e="T361" id="Seg_8568" s="T355">ЕТ: Кайа, миниэттэрэ пиредкаларым барыта олоӈкоһут этилэрэ.</ta>
            <ta e="T384" id="Seg_8569" s="T361">ЕТ: Ыллааччы да этилэр, ырыа, ылла.. ыллана, ырыалаак даа олоӈкону кэпсээччи, мээнэ да олоӈкону кэпсэ.. оччого, былыргы үйэгэ буоллагынан кинооны көрөгүн дуу оччо даагыны.</ta>
            <ta e="T387" id="Seg_8570" s="T384">ET: "Испидооланы" иһиллиигин дуу.</ta>
            <ta e="T395" id="Seg_8571" s="T387">ЕТ: Мас оонньуурунан онньуугун: чыычаактарынан, ити һырга оӈосто оӈостогун.</ta>
            <ta e="T401" id="Seg_8572" s="T395">ЕТ: Таһаара тагыстаккынан, муос тамнаан, муоһунан оонньуоӈ.</ta>
            <ta e="T406" id="Seg_8573" s="T401">ЕТ: Уол ого буолла маабыттана оонньуога.</ta>
            <ta e="T413" id="Seg_8574" s="T406">ЕТ: Һайын кайдак һылдьаргын, тааһы мунньунаӈӈын өлбөкүүн оонньуоӈ.</ta>
            <ta e="T423" id="Seg_8575" s="T413">ЕТ: Һол көрдүккээчэн күнү быһаагын буолла, былыргы ого былыргылыы оонньур буо.</ta>
            <ta e="T439" id="Seg_8576" s="T423">ЕТ: Ол быыһыытынан аны былыргы оонньууру да аны да көрүөккүтүн да, чаӈкаркаатаак оонньууру эгэлбитим буоллуӈ бу дойдуга.</ta>
            <ta e="T445" id="Seg_8577" s="T439">ЕТ: Һин көллөрүөм, ол оонньуу, кайдак оонньуулларын.</ta>
            <ta e="T465" id="Seg_8578" s="T445">ЕТ: Аӈар онньурдары оӈор.. оӈорон эгэлиэкпин, киһитэ һуоппун, огонньорум ол больницага һытар этэ буолла, биир уолым һин ыалдьан больницаттан таксыат кэ…</ta>
            <ta e="T487" id="Seg_8579" s="T465">ЕТ: Кэлбитим буолла, онтум (мин) тыага баар этэ, онтум эбэгэ киирэн ыалдьыбыта, онтон койут таксыбыта, кэлэрбит да ону – пургаалар, туоктар буо, мэһээйдээн һонон..</ta>
            <ta e="T500" id="Seg_8580" s="T487">ЕТ: Туок элбэк оонньур эгэлбэтэгим, урукку ого оонньурун һин онтон атын көллөрүүгэ эгэлиэк этим.</ta>
            <ta e="T506" id="Seg_8581" s="T500">ЕТ: Урут бэйэм оонньоон үөскээбиппин киэ онтон.</ta>
            <ta e="T522" id="Seg_8582" s="T517">ЕТ: Тэ, муотунан һөрүүбүт үүттээк маска.</ta>
            <ta e="T526" id="Seg_8583" s="T522">ЕТ: Баһынан таӈнары туран эргийэр.</ta>
            <ta e="T536" id="Seg_8584" s="T526">ЕТ: Ону көрдөккүнэн эрэ билиэӈ буолла, кайдак аны кэпсээммин, кайдак кэпсиэбий? </ta>
            <ta e="T537" id="Seg_8585" s="T536">ЕТ: Көллөрүөм.</ta>
            <ta e="T540" id="Seg_8586" s="T537">ЕТ: Ол көрдөккүнэ, билээр.</ta>
            <ta e="T561" id="Seg_8587" s="T558">ЕТ: Һэ-э, билэбин оннуктары.</ta>
            <ta e="T569" id="Seg_8588" s="T561">ЕТ: Оннук.. оннукта мээнэ любой ырыата ыллыам дуо, любойда?</ta>
            <ta e="T573" id="Seg_8589" s="T569">ЕТ: Любой ырыата ыллыам, бээбэ.</ta>
            <ta e="T576" id="Seg_8590" s="T573">ЕТ: Кэлэ таарпат итинтиӈ.</ta>
            <ta e="T583" id="Seg_8591" s="T576">ЕТ: Кэлиэ.. кэлиэнэ (кэлигэнэ) һуок буолаачы, күөмэйдэрин өйдөөбөппүн, умнубуппун.</ta>
            <ta e="T595" id="Seg_8592" s="T583">ЕТ: Урут буоллагына гининэн заниматьсалыам диэммин, олус делайдаабакка һылдьыбытым буолла, ол иһин умнабын.</ta>
            <ta e="T617" id="Seg_8593" s="T610">ЕТ: Итии өлбүтэ, ити Байа диэн кыыс ыллаабыта.</ta>
            <ta e="T621" id="Seg_8594" s="T617">ЕТ: Ыллаабыт уола һин өлбүтэ.</ta>
            <ta e="T652" id="Seg_8595" s="T641">ЕТ: Һэ-э, оннуктарынан Тоняны гытта һин, э-э, туоктуокпут, һырсары һин ыллыакпыт буолла.</ta>
            <ta e="T666" id="Seg_8596" s="T652">ЕТ: Мээнэ, тыына, тыына, э-э, перехват дыханияны – тыына тыынагын буолла, до конца ыллаа диэтэккинэ ыллыам мээнэ. </ta>
            <ta e="T669" id="Seg_8597" s="T666">ЕТ: Тыын былдьаһар коһооно.</ta>
            <ta e="T705" id="Seg_8598" s="T684">ЕТ: Итини буолла һоготок тыыннан ыллаaччылар үһү былыргылар, һоготок тыыннан (тыынынан), тыын былдьаһаннан, дьэ гиннэр оонньуурдара ол буолла, бу артистар ыны оонньуулларын көрдүк. </ta>
            <ta e="T716" id="Seg_8599" s="T705">ЕТ: Һити һоготок тыыннан (тыынынан) тийээччи этэ, үһү, Иникиэн диэн киһи -- ити итинигэ.</ta>
            <ta e="T731" id="Seg_8600" s="T727">ЕТ: Ити миниэнэ кыыһым ити.</ta>
            <ta e="T769" id="Seg_8601" s="T760">ЕТ: Һэ-э, дьэ ол мин кэпсээбит олоӈкобун "Коптоок Таисканы" кэпсээбиттэр.</ta>
            <ta e="T779" id="Seg_8602" s="T769">ЕТ: Минигин кэпсэтэн бараан, кэлбиттэрэ манна кэлээрилэр, (кутта?) дьылга кэлбиттэрэ оонньоорулар.</ta>
            <ta e="T789" id="Seg_8603" s="T779">ЕТ: Онтуларын даагыны (дааганы) ити кайдак, кайдак кэпсээн кээспит этилэрэ онтон обуойа. Һэ-һэ.</ta>
            <ta e="T799" id="Seg_8604" s="T792">ЕТ: Э-э, истибитим. Ону истэммин гынабын ити, һэ-һэ-һэ…..</ta>
            <ta e="T805" id="Seg_8605" s="T799">ЕТ: .. Олору кэлэллэр, олору һэмэлиибин:</ta>
            <ta e="T813" id="Seg_8606" s="T805">"Того киэ ити онон-манан кэпсиигит, илэлии кэпсээбэккит олоӈкогутун?"</ta>
            <ta e="T826" id="Seg_8607" s="T819">ЕТ: (Всё равно) һин дьүллэнэр онтон, основнойы һин кэпсииллэр.</ta>
            <ta e="T851" id="Seg_8608" s="T848">ЕТ: Э-э, баар эт.</ta>
            <ta e="T864" id="Seg_8609" s="T851">ЕТ: Ол олоӈкону билэр мин убайым баар, ыбайдарым (убайдарым) онтон иккиэн да билэллэр, икки убайдаакпын.</ta>
            <ta e="T873" id="Seg_8610" s="T864">ЕТ: Иккиэн да билэ.. бииргэһэ кырдьагас, бииргэһэ минигиннээгэр һин убай.</ta>
            <ta e="T883" id="Seg_8611" s="T873">ЕТ: Билэллэр олор олоӈколору. Ырыалаактары даа билэллэр, ырыата даа һуогу билэ..</ta>
            <ta e="T903" id="Seg_8612" s="T887">ЕТ: Мээнэ киһилэр, ити урут олоӈкоһут киһилэрбит бараннылар буолла, aны илэ, илэ мин билэр олоӈкоһут киһилэрим бараннылар.</ta>
            <ta e="T909" id="Seg_8613" s="T903">ЕТ: Ким баара буолой аны олоӈкоһут киһи?</ta>
            <ta e="T918" id="Seg_8614" s="T909">ЕТ: Кими да билбэппин аны олоӈкоһут киһини, илэ олоӈкоһут киһини. </ta>
            <ta e="T943" id="Seg_8615" s="T932">ЕТ: Һуок, һуок урут буолла кимнэрэ дааганы итигирдикээчэн гымматылар, бу аны, бу….</ta>
            <ta e="T963" id="Seg_8616" s="T943">ЕТ: былырыыӈӈыттан бэттэк бу идэ буллулар: бу урукку олоӈко, ол иһин биһиги таһыччы умнубуппут ону, олоӈколорбутун барытын умнан иһэбит буо, барытын. </ta>
            <ta e="T966" id="Seg_8617" s="T963">ЕТ: каччага да кэпсээбэккин.</ta>
            <ta e="T972" id="Seg_8618" s="T966">ЕТ: Ырыаны да умнагын, олоӈкону да умнагын.</ta>
            <ta e="T980" id="Seg_8619" s="T972">ЕТ: Усторияны кэпсиирэ буолла Ониисим огонньор, баар олоӈкоһут эмиэ.</ta>
            <ta e="T995" id="Seg_8620" s="T990">ЕТ: Туок билиэй да ол гиннилэри.</ta>
            <ta e="T1001" id="Seg_8621" s="T995">ЕТ: Олоӈко иӈэрэ дьүрү гиннилэргэ туок бил..?</ta>
            <ta e="T1015" id="Seg_8622" s="T1001">Чубу- чубу аны олоӈколоспоппут буо, мээнэ кэпсээ, эрэ диэтэктэринэ кэпсиибин. Наадырдактарынан, кэпсэттэктэринэ эрэ кэпсиигин буол..</ta>
            <ta e="T1022" id="Seg_8623" s="T1015">ЕТ: Таак да мээнэ каччага да олоӈколоспоппут буо.</ta>
            <ta e="T1042" id="Seg_8624" s="T1041">ЕТ: Һэ-э.</ta>
            <ta e="T1056" id="Seg_8625" s="T1053">ЕТ: Кайа, атынна буолумна.</ta>
            <ta e="T1080" id="Seg_8626" s="T1074">ЕТ: Огом туһунан куччукүүкээн стих обойун оӈуорбутум. </ta>
            <ta e="T1082" id="Seg_8627" s="T1080">ЕТ: Айсүлүү туһунан.</ta>
            <ta e="T1085" id="Seg_8628" s="T1082">ЕТ: Сулустар каллаӈӈа көстөллөр.</ta>
            <ta e="T1088" id="Seg_8629" s="T1085">ЕТ: Утуй, утуй Айсүлүү.</ta>
            <ta e="T1094" id="Seg_8630" s="T1088">ЕТ: Үйэбит уларыйда, одуу атын кэм кэллэ.</ta>
            <ta e="T1099" id="Seg_8631" s="T1094">ЕТ: Түргэнкээнник улаатаар, саппын, иннэбин тутаар. </ta>
            <ta e="T1102" id="Seg_8632" s="T1099">ЕТ: Мин үйэбин үйэлэнээр.</ta>
            <ta e="T1105" id="Seg_8633" s="T1102">ЕТ: Киһиэкэ киһи дэтээр.</ta>
            <ta e="T1109" id="Seg_8634" s="T1105">ЕТ: Огокоонум оготун биһигигэр билиибин.</ta>
            <ta e="T1113" id="Seg_8635" s="T1109">ЕТ: Аттытыгар олоробун, оргуйакаан ылланным.</ta>
         </annotation>
         <annotation name="ts" tierref="ts-UkET">
            <ta e="T43" id="Seg_8636" s="T21">– Min iː bejem töröːbüt hirim bi di͡ek ete, mu͡ora di͡ek, ol di͡ek, (Nos-) kim di͡ek, eː, onton ɨrɨːmnaj oŋu͡orgutugar iti. </ta>
            <ta e="T52" id="Seg_8637" s="T43">Mu͡ora di͡ek töröːbütüm, onton erge barammɨn taːs di͡ek hirdemmitim. </ta>
            <ta e="T55" id="Seg_8638" s="T52">Ogolorum da tu͡ok… </ta>
            <ta e="T68" id="Seg_8639" s="T55">Kajdak kepseːn da, tugu kepsi͡emij, biːr ogom iti ušku͡olaga üleliːr, biːr kɨːhɨm dʼirʼektardiːr. </ta>
            <ta e="T81" id="Seg_8640" s="T68">Biːr, e, biːr ogom iti Nosku͡oga, (ula-) muŋ ulakan kɨːhɨm Nosku͡oga ušku͡olaga doktuːrduːr. </ta>
            <ta e="T82" id="Seg_8641" s="T81">Nʼiːna. </ta>
            <ta e="T87" id="Seg_8642" s="T82">(Biː-) ol ulakan kɨːhɨm aːta. </ta>
            <ta e="T91" id="Seg_8643" s="T87">Kuččuguj, ontum annɨta Keːtʼe. </ta>
            <ta e="T94" id="Seg_8644" s="T91">Ol ušku͡olaga dʼirʼekterdiːr. </ta>
            <ta e="T109" id="Seg_8645" s="T94">Biːr kɨːhɨm haːdikka üleleːčči ete, ontum eː, bu haːdiktara, tu͡oktara aldʼanan anaːn da ogolonon, kanʼaːn. </ta>
            <ta e="T120" id="Seg_8646" s="T109">Anɨ ɨ͡aldʼan Dudʼinka di͡ek hɨldʼar bu, eː, Narilʼskajga emtete hɨtar anɨ. </ta>
            <ta e="T125" id="Seg_8647" s="T120">Onton biːr u͡olum axoːtnʼik bu͡olla. </ta>
            <ta e="T139" id="Seg_8648" s="T125">Biːr u͡olum iti hamalʼottarɨ (ɨːk-) ittelerin (ɨːt-) tu͡oktuːr, oŋoror, hamalʼottarɨ iːter, kötüter, itinne baːr. </ta>
            <ta e="T155" id="Seg_8649" s="T139">Biːr kɨːhɨm (ini-) tuːgu büppüt ete, haːdik ü͡öregin, anɨ bejem kɨrdʼammɨn, ontubun bejeber üleleteːribin tɨ͡aga tutabɨn. </ta>
            <ta e="T169" id="Seg_8650" s="T155">Ogonnʼorum urut bulčut ete hin (kihi͡e-), kihitten, eː, ɨrɨmnajga otton hapku͡oska peredavojɨnan bultaːččɨ ete. </ta>
            <ta e="T183" id="Seg_8651" s="T169">Anɨ ɨ͡aldʼan, iliːte ɨ͡aldʼan ol bulduttan da tagɨsta, onuga ebiːleːn anɨ (aːktan), pʼensʼijaga kiːrde. </ta>
            <ta e="T190" id="Seg_8652" s="T183">Bu anɨ bu dʼɨllarga kiːrbite. </ta>
            <ta e="T209" id="Seg_8653" s="T202">– Heː, anɨ bu ogolorum, ogom ogolorun karajammɨn. </ta>
            <ta e="T226" id="Seg_8654" s="T209">Ol emŋe keleːčči ogom gi͡ene üs ogoloːk bu͡olla, ontularɨm ogotun karaja hɨtar etim bu͡o, anɨ bu͡ollagɨna bu. </ta>
            <ta e="T233" id="Seg_8655" s="T226">Ogom ol, Maːčam kelen kellim ol tɨ͡attan. </ta>
            <ta e="T251" id="Seg_8656" s="T233">Tɨ͡aga tuta hɨtabɨn bu͡olla, ol üs ogobun, ol maːmata bu di͡ek baːr, teːtelere arakpɨta bu͡olla, ol dʼuraːk ete. </ta>
            <ta e="T253" id="Seg_8657" s="T251">Ontuta arakpɨta. </ta>
            <ta e="T273" id="Seg_8658" s="T253">Ol ihin ol kantan biːrten biːr ogo bu͡olla, ogolorum ogoloro kaččaga da arappattar bu͡o, hajɨn, hajɨnnarɨ, kɨhɨnnarɨ huptu ogoloːk bu͡olaːččɨbɨn. </ta>
            <ta e="T277" id="Seg_8659" s="T273">Vnuktarɨm bi͡ek baːr bu͡olallar. </ta>
            <ta e="T315" id="Seg_8660" s="T299">– Heː, kepseːččibit ittekeːni ki͡e, anɨ ulakannɨk da tu͡olkulana ilikter eːt, ulakan kɨːhɨm ere ogoloro usku͡olaga barbɨttara. </ta>
            <ta e="T321" id="Seg_8661" s="T315">Kuččuguj ogolorum ogoloro kiːre ilikter usku͡olaga. </ta>
            <ta e="T340" id="Seg_8662" s="T321">Olorgo ol ittekeːni kepsiːbin bu͡o, itteni-bütteni, "ebeː", di͡en ergiččije hɨldʼaːččɨlar bu͡o, "ontugun kepseː, onu oloŋkoloː, munu kepseː, itini ɨllaː". </ta>
            <ta e="T348" id="Seg_8663" s="T340">Oloru haːtataːrɨgɨn ke ɨllanɨŋ, kepsetiŋ – hin kojuː bu͡olla. </ta>
            <ta e="T361" id="Seg_8664" s="T355">– Kaja, mini͡ettere pirʼedkalarɨm barɨta oloŋkohut etiler. </ta>
            <ta e="T384" id="Seg_8665" s="T361">ɨllaːččɨ da etiler, ɨrɨ͡a, (ɨlla-) ɨllana, ɨrɨ͡alaːk daː oloŋkonu kepseːčči, meːne da oloŋkonu (kepse-), oččogo, bɨlɨrgɨ üjege bu͡ollagɨnan kinoːnɨ körögün duː oččo daːgɨnɨ. </ta>
            <ta e="T387" id="Seg_8666" s="T384">"Ispidoːlanɨ" ihilliːgin duː. </ta>
            <ta e="T395" id="Seg_8667" s="T387">Mas oːnnʼuːrunan onnʼuːgun, čɨːčaːktarɨnan, iti hɨrga oŋosto oŋostogun. </ta>
            <ta e="T401" id="Seg_8668" s="T395">Tahaːra tagɨstakkɨnan, mu͡os tamnaːn, mu͡ohunan oːnnʼu͡oŋ. </ta>
            <ta e="T406" id="Seg_8669" s="T401">U͡ol ogo bu͡olla maːmɨttana oːnnʼu͡oga. </ta>
            <ta e="T413" id="Seg_8670" s="T406">Hajɨn kajdak hɨldʼargɨn, taːhɨ munnʼunaŋŋɨn ölböküːn oːnnʼu͡oŋ. </ta>
            <ta e="T423" id="Seg_8671" s="T413">Hol kördükkeːčen künü bɨhagɨn bu͡olla, bɨlɨrgɨ ogo bɨlɨrgɨlɨː oːnnʼuːr bu͡o. </ta>
            <ta e="T439" id="Seg_8672" s="T423">Ol bɨːhɨːtɨnan anɨ bɨlɨrgɨ oːnnʼuːru da anɨ da körü͡ökkütün da, čaŋkarkaːtaːk oːnnʼuːru egelbitim bu͡olluŋ bu dojduga. </ta>
            <ta e="T445" id="Seg_8673" s="T439">Hin köllörü͡öm, ol oːnnʼuː, kajdak oːnnʼuːllarɨn. </ta>
            <ta e="T465" id="Seg_8674" s="T445">Aŋar oːnnʼuːrdarɨ (oŋor-) oŋoron egeli͡ekpin, kihite hu͡oppun, ogonnʼorum ol balʼnʼisaga hɨtar ete bu͡olla, biːr u͡olɨm hin ɨ͡aldʼan balʼnʼisattan taksaːt ke… </ta>
            <ta e="T487" id="Seg_8675" s="T465">Kelbitim bu͡olla, ontum min tɨ͡aga baːr ete, ontum ebege kiːren ɨ͡aldʼɨbɨta, onton kojut taksɨbɨta, kelerbit da onu – purgaːlar, tu͡oktar bu͡o, meheːjdeːn honon. </ta>
            <ta e="T500" id="Seg_8676" s="T487">Tu͡ok elbek oːnnʼuːr egelbetegim, urukku ogo oːnnʼuːrun hin onton atɨn köllörüːge egeli͡ek etim. </ta>
            <ta e="T506" id="Seg_8677" s="T500">Urut bejem oːnnʼoːn ü͡öskeːbippin ke onton. </ta>
            <ta e="T522" id="Seg_8678" s="T517">– Te, mu͡otunan hörüːbüt üːtteːk maska. </ta>
            <ta e="T526" id="Seg_8679" s="T522">Bahɨnan taŋnarɨ turan ergijer. </ta>
            <ta e="T536" id="Seg_8680" s="T526">Onu kördökkünen ere bili͡eŋ bu͡olla, kajdak anɨ kepseːmmin, kajdak kepsi͡emij? </ta>
            <ta e="T537" id="Seg_8681" s="T536">Köllörü͡öm. </ta>
            <ta e="T540" id="Seg_8682" s="T537">Ol kördökküne, bileːr. </ta>
            <ta e="T561" id="Seg_8683" s="T558">– Heː, bilebin onnuktarɨ. </ta>
            <ta e="T569" id="Seg_8684" s="T561">(Onnuk-) onnukta meːne lʼuboj ɨrɨ͡ata ɨllɨ͡am du͡o, lʼubojda? </ta>
            <ta e="T573" id="Seg_8685" s="T569">Lʼuboj ɨrɨ͡ata ɨllɨ͡am, beːbe. </ta>
            <ta e="T576" id="Seg_8686" s="T573">Kele tarpat itintiŋ. </ta>
            <ta e="T583" id="Seg_8687" s="T576">(Keli͡e-) (keli͡ene) hu͡ok bu͡olaːččɨ, kü͡ömejderin öjdöːböppün, umnubuppun. </ta>
            <ta e="T595" id="Seg_8688" s="T583">Urut bu͡ollagɨna gininen zanʼimaːtsalɨ͡am di͡emmin, olus dʼelajdaːbakka hɨldʼɨbɨtɨm bu͡olla, ol ihin umnabɨn. </ta>
            <ta e="T596" id="Seg_8689" s="T595">SONG </ta>
            <ta e="T597" id="Seg_8690" s="T596">SONG </ta>
            <ta e="T598" id="Seg_8691" s="T597">SONG </ta>
            <ta e="T599" id="Seg_8692" s="T598">SONG </ta>
            <ta e="T600" id="Seg_8693" s="T599">SONG </ta>
            <ta e="T601" id="Seg_8694" s="T600">SONG </ta>
            <ta e="T602" id="Seg_8695" s="T601">SONG </ta>
            <ta e="T603" id="Seg_8696" s="T602">SONG </ta>
            <ta e="T604" id="Seg_8697" s="T603">SONG </ta>
            <ta e="T605" id="Seg_8698" s="T604">SONG </ta>
            <ta e="T606" id="Seg_8699" s="T605">SONG </ta>
            <ta e="T607" id="Seg_8700" s="T606">SONG </ta>
            <ta e="T608" id="Seg_8701" s="T607">SONG </ta>
            <ta e="T617" id="Seg_8702" s="T610">– Iti ölbüte, iti Baja di͡en kɨːs ɨllaːbɨta. </ta>
            <ta e="T621" id="Seg_8703" s="T617">ɨllaːbɨt u͡ola hin ölbüte. </ta>
            <ta e="T652" id="Seg_8704" s="T641">– Heː, onnuktarɨnan Toːnʼanɨ gɨtta hin, eː, tu͡oktu͡opput, hɨrsarɨ hin ɨllɨ͡appɨt bu͡olla. </ta>
            <ta e="T666" id="Seg_8705" s="T652">Meːne, tɨːna, tɨːna, eː, pʼerʼexvat dɨxanʼijanɨ – tɨːna tɨːnagɨn bu͡olla, da kanca ɨllaː di͡etekkine ɨllɨ͡am meːne. </ta>
            <ta e="T669" id="Seg_8706" s="T666">Tɨːn bɨldʼahar kohoːn. </ta>
            <ta e="T670" id="Seg_8707" s="T669">SONG </ta>
            <ta e="T671" id="Seg_8708" s="T670">SONG </ta>
            <ta e="T672" id="Seg_8709" s="T671">SONG </ta>
            <ta e="T673" id="Seg_8710" s="T672">SONG </ta>
            <ta e="T674" id="Seg_8711" s="T673">SONG </ta>
            <ta e="T675" id="Seg_8712" s="T674">SONG </ta>
            <ta e="T676" id="Seg_8713" s="T675">SONG </ta>
            <ta e="T677" id="Seg_8714" s="T676">SONG </ta>
            <ta e="T678" id="Seg_8715" s="T677">SONG </ta>
            <ta e="T679" id="Seg_8716" s="T678">SONG </ta>
            <ta e="T680" id="Seg_8717" s="T679">SONG </ta>
            <ta e="T681" id="Seg_8718" s="T680">SONG </ta>
            <ta e="T682" id="Seg_8719" s="T681">SONG </ta>
            <ta e="T683" id="Seg_8720" s="T682">SONG </ta>
            <ta e="T684" id="Seg_8721" s="T683">SONG </ta>
            <ta e="T705" id="Seg_8722" s="T684">– Itini bu͡olla hogotok tɨːnnan ɨllaːččɨlar ühü bɨlɨrgɨlar, hogotok tɨːnnan, tɨːn bɨldʼahannar, dʼe ginner oːnnʼuːrdara ol bu͡olla, bu artʼistar anɨ oːnnʼuːllarɨn kördük. </ta>
            <ta e="T716" id="Seg_8723" s="T705">Hiti hogotok tɨːnnan tijeːčči ete, ühü, Iniki͡en di͡en kihi – iti itinige. </ta>
            <ta e="T731" id="Seg_8724" s="T727">– Iti mini͡ene kɨːhɨm iti. </ta>
            <ta e="T769" id="Seg_8725" s="T760">– Heː, dʼe ol min kepseːbit oloŋkobun "Koptoːk Taiskanɨ" kepseːbitter. </ta>
            <ta e="T779" id="Seg_8726" s="T769">Minigin kepseten baraːn, kelbittere manna keleːriler, (kutta) dʼɨlga kelbittere oːnnʼoːrular. </ta>
            <ta e="T789" id="Seg_8727" s="T779">Ontularɨn daːgɨnɨ iti kajdak, kajdak kepseːn keːspit etilere onton obu͡oja. </ta>
            <ta e="T799" id="Seg_8728" s="T792">– Eː, istibitim, onu istemmin gɨnabɨn iti, ((LAUGH)). </ta>
            <ta e="T805" id="Seg_8729" s="T799">(Oloru ke-) Oloru keleller, oloru hemeliːbin: </ta>
            <ta e="T813" id="Seg_8730" s="T805">"Togo ke iti onon-manan kepsiːgit, ileliː kepseːbekkit oloŋkogutun?" </ta>
            <ta e="T826" id="Seg_8731" s="T819">– Vsʼo ravno hin dʼüːllener onton, asnavnojɨ hin kepsiːller. </ta>
            <ta e="T851" id="Seg_8732" s="T848">– Eː, baːr eːt. </ta>
            <ta e="T864" id="Seg_8733" s="T851">Ol oloŋkonu biler min ubajɨm baːr, ɨbajdarɨm onton ikki͡en da bileller, ikki ubajdaːkpɨn. </ta>
            <ta e="T873" id="Seg_8734" s="T864">Ikki͡en da (bile-), biːrgehe kɨrdžagas, biːrgehe miniginneːger hin ubaj. </ta>
            <ta e="T883" id="Seg_8735" s="T873">Bileller olor oloŋkoloru, ɨrɨ͡alaːktarɨ daː bileller, ɨrɨ͡ata daː hu͡ogu (bile-). </ta>
            <ta e="T903" id="Seg_8736" s="T887">– Meːne kihiler, iti urut oloŋkohut kihilerbit barannɨlar bu͡olla, anɨ ile, ile min biler oloŋkohut kihilerim barannɨlar. </ta>
            <ta e="T909" id="Seg_8737" s="T903">Kim baːra bu͡olu͡oj anɨ oloŋkohut kihi? </ta>
            <ta e="T918" id="Seg_8738" s="T909">Kimi da bilbeppin anɨ oloŋkohut kihini, ile oloŋkohut kihini. </ta>
            <ta e="T943" id="Seg_8739" s="T932">– Hu͡ok, hu͡ok urut bu͡olla kimnere daːganɨ itigirdikeːčen gɨmmatɨlar, bu anɨ, bu. </ta>
            <ta e="T963" id="Seg_8740" s="T943">Bɨlɨrɨːŋŋɨttan bettek bu ide bullular, bu urukku oloŋko, ol ihin bihigi tahɨččɨ umnubupput onu, oloŋkolorbutun barɨtɨn umnan ihebit bu͡o, barɨtɨn. </ta>
            <ta e="T966" id="Seg_8741" s="T963">Kaččaga da kepseːbekkin. </ta>
            <ta e="T972" id="Seg_8742" s="T966">ɨrɨ͡anɨ da umnagɨn, oloŋkonu da umnagɨn. </ta>
            <ta e="T980" id="Seg_8743" s="T972">Ustorijanɨ kepsiːre bu͡olla Onʼiːsim ogonnʼor baːr, oloŋkohut emi͡e. </ta>
            <ta e="T995" id="Seg_8744" s="T990">– Tu͡ok bili͡ej da ol ginileri. </ta>
            <ta e="T1001" id="Seg_8745" s="T995">Oloŋko iŋere dʼürü ginilerge tu͡ok (bil-)? </ta>
            <ta e="T1015" id="Seg_8746" s="T1001">Čubu-čubu anɨ oloŋkolospopput bu͡o, "meːne kepseː", ere di͡etekterine kepsiːbin, naːdadaktarɨnan, kepsettekterine ere kepsiːgin bu͡o. </ta>
            <ta e="T1022" id="Seg_8747" s="T1015">Taːk da meːne kaččaga da oloŋkolospopput bu͡o. </ta>
            <ta e="T1042" id="Seg_8748" s="T1041">– Heː. </ta>
            <ta e="T1056" id="Seg_8749" s="T1053">– Kaja, atɨnna bu͡olumna. </ta>
            <ta e="T1060" id="Seg_8750" s="T1056">((TALE; UkET_19940424_OldWomanTaal_flk)). </ta>
            <ta e="T1080" id="Seg_8751" s="T1074">– Ogom tuhunan küččüküːkeːn stʼix obu͡ojun oŋorbutum. </ta>
            <ta e="T1082" id="Seg_8752" s="T1080">Ajsülüː tuhunan. </ta>
            <ta e="T1085" id="Seg_8753" s="T1082">Sulustar kallaːŋŋa köstöllör. </ta>
            <ta e="T1088" id="Seg_8754" s="T1085">Utuj, utuj Ajsülüː. </ta>
            <ta e="T1094" id="Seg_8755" s="T1088">Üjebit ularɨjda, oduː atɨn kem kelle. </ta>
            <ta e="T1099" id="Seg_8756" s="T1094">Türgenkeːnnik ulaːtaːr, sappɨn, innebin tutaːr. </ta>
            <ta e="T1102" id="Seg_8757" s="T1099">Min üjebin üjeleneːr. </ta>
            <ta e="T1105" id="Seg_8758" s="T1102">Kihi͡eke kihi deteːr. </ta>
            <ta e="T1109" id="Seg_8759" s="T1105">Ogokoːnum ogotun bihigiger biliːbin. </ta>
            <ta e="T1113" id="Seg_8760" s="T1109">Attɨtɨgar olorobun, orgujakaːn ɨllannɨm. </ta>
         </annotation>
         <annotation name="mb" tierref="mb-UkET">
            <ta e="T23" id="Seg_8761" s="T21">min</ta>
            <ta e="T25" id="Seg_8762" s="T23">iː</ta>
            <ta e="T26" id="Seg_8763" s="T25">beje-m</ta>
            <ta e="T27" id="Seg_8764" s="T26">töröː-büt</ta>
            <ta e="T28" id="Seg_8765" s="T27">hir-i-m</ta>
            <ta e="T29" id="Seg_8766" s="T28">bi</ta>
            <ta e="T30" id="Seg_8767" s="T29">di͡ek</ta>
            <ta e="T31" id="Seg_8768" s="T30">e-t-e</ta>
            <ta e="T32" id="Seg_8769" s="T31">mu͡ora</ta>
            <ta e="T33" id="Seg_8770" s="T32">di͡ek</ta>
            <ta e="T34" id="Seg_8771" s="T33">ol</ta>
            <ta e="T35" id="Seg_8772" s="T34">di͡ek</ta>
            <ta e="T37" id="Seg_8773" s="T36">kim</ta>
            <ta e="T38" id="Seg_8774" s="T37">di͡ek</ta>
            <ta e="T39" id="Seg_8775" s="T38">eː</ta>
            <ta e="T40" id="Seg_8776" s="T39">onton</ta>
            <ta e="T41" id="Seg_8777" s="T40">ɨrɨːmnaj</ta>
            <ta e="T42" id="Seg_8778" s="T41">oŋu͡or-gutu-gar</ta>
            <ta e="T43" id="Seg_8779" s="T42">iti</ta>
            <ta e="T44" id="Seg_8780" s="T43">mu͡ora</ta>
            <ta e="T45" id="Seg_8781" s="T44">di͡ek</ta>
            <ta e="T46" id="Seg_8782" s="T45">töröː-büt-ü-m</ta>
            <ta e="T47" id="Seg_8783" s="T46">onton</ta>
            <ta e="T48" id="Seg_8784" s="T47">er-ge</ta>
            <ta e="T49" id="Seg_8785" s="T48">bar-am-mɨn</ta>
            <ta e="T50" id="Seg_8786" s="T49">taːs</ta>
            <ta e="T51" id="Seg_8787" s="T50">di͡ek</ta>
            <ta e="T52" id="Seg_8788" s="T51">hir-dem-mit-i-m</ta>
            <ta e="T53" id="Seg_8789" s="T52">ogo-lor-u-m</ta>
            <ta e="T54" id="Seg_8790" s="T53">da</ta>
            <ta e="T55" id="Seg_8791" s="T54">tu͡ok</ta>
            <ta e="T56" id="Seg_8792" s="T55">kajdak</ta>
            <ta e="T57" id="Seg_8793" s="T56">kepseː-n</ta>
            <ta e="T58" id="Seg_8794" s="T57">da</ta>
            <ta e="T59" id="Seg_8795" s="T58">tug-u</ta>
            <ta e="T60" id="Seg_8796" s="T59">keps-i͡e-m=ij</ta>
            <ta e="T61" id="Seg_8797" s="T60">biːr</ta>
            <ta e="T62" id="Seg_8798" s="T61">ogo-m</ta>
            <ta e="T63" id="Seg_8799" s="T62">iti</ta>
            <ta e="T64" id="Seg_8800" s="T63">ušku͡ola-ga</ta>
            <ta e="T65" id="Seg_8801" s="T64">üleliː-r</ta>
            <ta e="T66" id="Seg_8802" s="T65">biːr</ta>
            <ta e="T67" id="Seg_8803" s="T66">kɨːh-ɨ-m</ta>
            <ta e="T68" id="Seg_8804" s="T67">dʼirʼektar-diː-r</ta>
            <ta e="T69" id="Seg_8805" s="T68">biːr</ta>
            <ta e="T70" id="Seg_8806" s="T69">e</ta>
            <ta e="T71" id="Seg_8807" s="T70">biːr</ta>
            <ta e="T72" id="Seg_8808" s="T71">ogo-m</ta>
            <ta e="T73" id="Seg_8809" s="T72">iti</ta>
            <ta e="T74" id="Seg_8810" s="T73">Nosku͡o-ga</ta>
            <ta e="T76" id="Seg_8811" s="T75">muŋ</ta>
            <ta e="T77" id="Seg_8812" s="T76">ulakan</ta>
            <ta e="T78" id="Seg_8813" s="T77">kɨːh-ɨ-m</ta>
            <ta e="T79" id="Seg_8814" s="T78">Nosku͡o-ga</ta>
            <ta e="T80" id="Seg_8815" s="T79">ušku͡ola-ga</ta>
            <ta e="T81" id="Seg_8816" s="T80">doktuːr-duː-r</ta>
            <ta e="T82" id="Seg_8817" s="T81">Nʼiːna</ta>
            <ta e="T84" id="Seg_8818" s="T83">ol</ta>
            <ta e="T85" id="Seg_8819" s="T84">ulakan</ta>
            <ta e="T86" id="Seg_8820" s="T85">kɨːh-ɨ-m</ta>
            <ta e="T87" id="Seg_8821" s="T86">aːt-a</ta>
            <ta e="T88" id="Seg_8822" s="T87">kuččuguj</ta>
            <ta e="T89" id="Seg_8823" s="T88">on-tu-m</ta>
            <ta e="T90" id="Seg_8824" s="T89">ann-ɨ-ta</ta>
            <ta e="T91" id="Seg_8825" s="T90">Keːtʼe</ta>
            <ta e="T92" id="Seg_8826" s="T91">ol</ta>
            <ta e="T93" id="Seg_8827" s="T92">ušku͡ola-ga</ta>
            <ta e="T94" id="Seg_8828" s="T93">dʼirʼekter-diː-r</ta>
            <ta e="T95" id="Seg_8829" s="T94">biːr</ta>
            <ta e="T96" id="Seg_8830" s="T95">kɨːh-ɨ-m</ta>
            <ta e="T97" id="Seg_8831" s="T96">haːdik-ka</ta>
            <ta e="T98" id="Seg_8832" s="T97">üleleː-čči</ta>
            <ta e="T99" id="Seg_8833" s="T98">e-t-e</ta>
            <ta e="T100" id="Seg_8834" s="T99">on-tu-m</ta>
            <ta e="T101" id="Seg_8835" s="T100">eː</ta>
            <ta e="T102" id="Seg_8836" s="T101">bu</ta>
            <ta e="T103" id="Seg_8837" s="T102">haːdik-tara</ta>
            <ta e="T104" id="Seg_8838" s="T103">tu͡ok-tara</ta>
            <ta e="T105" id="Seg_8839" s="T104">aldʼan-an</ta>
            <ta e="T106" id="Seg_8840" s="T105">anaːn</ta>
            <ta e="T107" id="Seg_8841" s="T106">da</ta>
            <ta e="T108" id="Seg_8842" s="T107">ogo-lon-on</ta>
            <ta e="T109" id="Seg_8843" s="T108">kanʼaː-n</ta>
            <ta e="T110" id="Seg_8844" s="T109">anɨ</ta>
            <ta e="T111" id="Seg_8845" s="T110">ɨ͡aldʼ-an</ta>
            <ta e="T112" id="Seg_8846" s="T111">Dudʼinka</ta>
            <ta e="T113" id="Seg_8847" s="T112">di͡ek</ta>
            <ta e="T114" id="Seg_8848" s="T113">hɨldʼ-ar</ta>
            <ta e="T115" id="Seg_8849" s="T114">bu</ta>
            <ta e="T116" id="Seg_8850" s="T115">eː</ta>
            <ta e="T117" id="Seg_8851" s="T116">Narilʼskaj-ga</ta>
            <ta e="T118" id="Seg_8852" s="T117">emt-e-t-e</ta>
            <ta e="T119" id="Seg_8853" s="T118">hɨt-ar</ta>
            <ta e="T120" id="Seg_8854" s="T119">anɨ</ta>
            <ta e="T121" id="Seg_8855" s="T120">onton</ta>
            <ta e="T122" id="Seg_8856" s="T121">biːr</ta>
            <ta e="T123" id="Seg_8857" s="T122">u͡ol-u-m</ta>
            <ta e="T124" id="Seg_8858" s="T123">axoːtnʼik</ta>
            <ta e="T125" id="Seg_8859" s="T124">bu͡ol-l-a</ta>
            <ta e="T126" id="Seg_8860" s="T125">biːr</ta>
            <ta e="T127" id="Seg_8861" s="T126">u͡ol-u-m</ta>
            <ta e="T128" id="Seg_8862" s="T127">iti</ta>
            <ta e="T129" id="Seg_8863" s="T128">hamalʼot-tar-ɨ</ta>
            <ta e="T131" id="Seg_8864" s="T130">itte-leri-n</ta>
            <ta e="T132" id="Seg_8865" s="T131">ɨːt</ta>
            <ta e="T133" id="Seg_8866" s="T132">tu͡ok-tuː-r</ta>
            <ta e="T134" id="Seg_8867" s="T133">oŋor-or</ta>
            <ta e="T135" id="Seg_8868" s="T134">hamalʼot-tar-ɨ</ta>
            <ta e="T136" id="Seg_8869" s="T135">iːt-er</ta>
            <ta e="T137" id="Seg_8870" s="T136">köt-ü-t-er</ta>
            <ta e="T138" id="Seg_8871" s="T137">itinne</ta>
            <ta e="T139" id="Seg_8872" s="T138">baːr</ta>
            <ta e="T140" id="Seg_8873" s="T139">biːr</ta>
            <ta e="T141" id="Seg_8874" s="T140">kɨːh-ɨ-m</ta>
            <ta e="T143" id="Seg_8875" s="T142">tuːg-u</ta>
            <ta e="T144" id="Seg_8876" s="T143">büp-püt</ta>
            <ta e="T145" id="Seg_8877" s="T144">e-t-e</ta>
            <ta e="T146" id="Seg_8878" s="T145">haːdik</ta>
            <ta e="T147" id="Seg_8879" s="T146">ü͡öreg-i-n</ta>
            <ta e="T148" id="Seg_8880" s="T147">anɨ</ta>
            <ta e="T149" id="Seg_8881" s="T148">beje-m</ta>
            <ta e="T150" id="Seg_8882" s="T149">kɨrdʼ-am-mɨn</ta>
            <ta e="T151" id="Seg_8883" s="T150">on-tu-bu-n</ta>
            <ta e="T152" id="Seg_8884" s="T151">beje-be-r</ta>
            <ta e="T153" id="Seg_8885" s="T152">ülel-e-t-eːri-bin</ta>
            <ta e="T154" id="Seg_8886" s="T153">tɨ͡a-ga</ta>
            <ta e="T155" id="Seg_8887" s="T154">tut-a-bɨn</ta>
            <ta e="T156" id="Seg_8888" s="T155">ogonnʼor-u-m</ta>
            <ta e="T157" id="Seg_8889" s="T156">urut</ta>
            <ta e="T158" id="Seg_8890" s="T157">bulčut</ta>
            <ta e="T159" id="Seg_8891" s="T158">e-t-e</ta>
            <ta e="T160" id="Seg_8892" s="T159">hin</ta>
            <ta e="T162" id="Seg_8893" s="T161">kihi-tten</ta>
            <ta e="T163" id="Seg_8894" s="T162">eː</ta>
            <ta e="T164" id="Seg_8895" s="T163">ɨrɨmnaj-ga</ta>
            <ta e="T165" id="Seg_8896" s="T164">otton</ta>
            <ta e="T166" id="Seg_8897" s="T165">hapku͡os-ka</ta>
            <ta e="T167" id="Seg_8898" s="T166">peredavoj-ɨ-nan</ta>
            <ta e="T168" id="Seg_8899" s="T167">bultaː-ččɨ</ta>
            <ta e="T169" id="Seg_8900" s="T168">e-t-e</ta>
            <ta e="T170" id="Seg_8901" s="T169">anɨ</ta>
            <ta e="T171" id="Seg_8902" s="T170">ɨ͡aldʼ-an</ta>
            <ta e="T172" id="Seg_8903" s="T171">iliː-te</ta>
            <ta e="T173" id="Seg_8904" s="T172">ɨ͡aldʼ-an</ta>
            <ta e="T174" id="Seg_8905" s="T173">ol</ta>
            <ta e="T175" id="Seg_8906" s="T174">buld-u-ttan</ta>
            <ta e="T176" id="Seg_8907" s="T175">da</ta>
            <ta e="T177" id="Seg_8908" s="T176">tagɨs-t-a</ta>
            <ta e="T178" id="Seg_8909" s="T177">onu-ga</ta>
            <ta e="T179" id="Seg_8910" s="T178">ebiː-leː-n</ta>
            <ta e="T180" id="Seg_8911" s="T179">anɨ</ta>
            <ta e="T181" id="Seg_8912" s="T180">aːk-t-an</ta>
            <ta e="T182" id="Seg_8913" s="T181">pʼensʼija-ga</ta>
            <ta e="T183" id="Seg_8914" s="T182">kiːr-d-e</ta>
            <ta e="T184" id="Seg_8915" s="T183">bu</ta>
            <ta e="T185" id="Seg_8916" s="T184">anɨ</ta>
            <ta e="T186" id="Seg_8917" s="T185">bu</ta>
            <ta e="T188" id="Seg_8918" s="T186">dʼɨl-lar-ga</ta>
            <ta e="T190" id="Seg_8919" s="T188">kiːr-bit-e</ta>
            <ta e="T203" id="Seg_8920" s="T202">heː</ta>
            <ta e="T204" id="Seg_8921" s="T203">anɨ</ta>
            <ta e="T205" id="Seg_8922" s="T204">bu</ta>
            <ta e="T206" id="Seg_8923" s="T205">ogo-lor-u-m</ta>
            <ta e="T207" id="Seg_8924" s="T206">ogo-m</ta>
            <ta e="T208" id="Seg_8925" s="T207">ogo-lor-u-n</ta>
            <ta e="T209" id="Seg_8926" s="T208">karaj-am-mɨn</ta>
            <ta e="T210" id="Seg_8927" s="T209">ol</ta>
            <ta e="T211" id="Seg_8928" s="T210">em-ŋe</ta>
            <ta e="T212" id="Seg_8929" s="T211">kel-eːčči</ta>
            <ta e="T213" id="Seg_8930" s="T212">ogo-m</ta>
            <ta e="T214" id="Seg_8931" s="T213">gi͡en-e</ta>
            <ta e="T215" id="Seg_8932" s="T214">üs</ta>
            <ta e="T216" id="Seg_8933" s="T215">ogo-loːk</ta>
            <ta e="T217" id="Seg_8934" s="T216">bu͡olla</ta>
            <ta e="T218" id="Seg_8935" s="T217">on-tu-lar-ɨ-m</ta>
            <ta e="T219" id="Seg_8936" s="T218">ogo-tu-n</ta>
            <ta e="T220" id="Seg_8937" s="T219">karaj-a</ta>
            <ta e="T221" id="Seg_8938" s="T220">hɨt-ar</ta>
            <ta e="T222" id="Seg_8939" s="T221">e-ti-m</ta>
            <ta e="T223" id="Seg_8940" s="T222">bu͡o</ta>
            <ta e="T224" id="Seg_8941" s="T223">anɨ</ta>
            <ta e="T225" id="Seg_8942" s="T224">bu͡ollagɨna</ta>
            <ta e="T226" id="Seg_8943" s="T225">bu</ta>
            <ta e="T227" id="Seg_8944" s="T226">ogo-m</ta>
            <ta e="T228" id="Seg_8945" s="T227">ol</ta>
            <ta e="T229" id="Seg_8946" s="T228">Maːča-m</ta>
            <ta e="T230" id="Seg_8947" s="T229">kel-en</ta>
            <ta e="T231" id="Seg_8948" s="T230">kel-li-m</ta>
            <ta e="T232" id="Seg_8949" s="T231">ol</ta>
            <ta e="T233" id="Seg_8950" s="T232">tɨ͡a-ttan</ta>
            <ta e="T234" id="Seg_8951" s="T233">tɨ͡a-ga</ta>
            <ta e="T235" id="Seg_8952" s="T234">tut-a</ta>
            <ta e="T236" id="Seg_8953" s="T235">hɨt-a-bɨn</ta>
            <ta e="T237" id="Seg_8954" s="T236">bu͡olla</ta>
            <ta e="T238" id="Seg_8955" s="T237">ol</ta>
            <ta e="T239" id="Seg_8956" s="T238">üs</ta>
            <ta e="T240" id="Seg_8957" s="T239">ogo-bu-n</ta>
            <ta e="T241" id="Seg_8958" s="T240">ol</ta>
            <ta e="T242" id="Seg_8959" s="T241">maːma-ta</ta>
            <ta e="T243" id="Seg_8960" s="T242">bu</ta>
            <ta e="T244" id="Seg_8961" s="T243">di͡ek</ta>
            <ta e="T245" id="Seg_8962" s="T244">baːr</ta>
            <ta e="T246" id="Seg_8963" s="T245">teːte-lere</ta>
            <ta e="T247" id="Seg_8964" s="T246">arak-pɨt-a</ta>
            <ta e="T248" id="Seg_8965" s="T247">bu͡olla</ta>
            <ta e="T249" id="Seg_8966" s="T248">ol</ta>
            <ta e="T250" id="Seg_8967" s="T249">dʼuraːk</ta>
            <ta e="T251" id="Seg_8968" s="T250">e-t-e</ta>
            <ta e="T252" id="Seg_8969" s="T251">on-tu-ta</ta>
            <ta e="T253" id="Seg_8970" s="T252">arak-pɨt-a</ta>
            <ta e="T254" id="Seg_8971" s="T253">ol</ta>
            <ta e="T255" id="Seg_8972" s="T254">ihin</ta>
            <ta e="T256" id="Seg_8973" s="T255">ol</ta>
            <ta e="T257" id="Seg_8974" s="T256">kantan</ta>
            <ta e="T258" id="Seg_8975" s="T257">biːr-ten</ta>
            <ta e="T259" id="Seg_8976" s="T258">biːr</ta>
            <ta e="T260" id="Seg_8977" s="T259">ogo</ta>
            <ta e="T261" id="Seg_8978" s="T260">bu͡olla</ta>
            <ta e="T262" id="Seg_8979" s="T261">ogo-lor-u-m</ta>
            <ta e="T263" id="Seg_8980" s="T262">ogo-loro</ta>
            <ta e="T264" id="Seg_8981" s="T263">kaččaga</ta>
            <ta e="T265" id="Seg_8982" s="T264">da</ta>
            <ta e="T266" id="Seg_8983" s="T265">arap-pat-tar</ta>
            <ta e="T267" id="Seg_8984" s="T266">bu͡o</ta>
            <ta e="T268" id="Seg_8985" s="T267">hajɨn</ta>
            <ta e="T269" id="Seg_8986" s="T268">hajɨn-nar-ɨ</ta>
            <ta e="T270" id="Seg_8987" s="T269">kɨhɨn-nar-ɨ</ta>
            <ta e="T271" id="Seg_8988" s="T270">huptu</ta>
            <ta e="T272" id="Seg_8989" s="T271">ogo-loːk</ta>
            <ta e="T273" id="Seg_8990" s="T272">bu͡ol-aːččɨ-bɨn</ta>
            <ta e="T274" id="Seg_8991" s="T273">vnuk-tar-ɨ-m</ta>
            <ta e="T275" id="Seg_8992" s="T274">bi͡ek</ta>
            <ta e="T276" id="Seg_8993" s="T275">baːr</ta>
            <ta e="T277" id="Seg_8994" s="T276">bu͡ol-al-lar</ta>
            <ta e="T300" id="Seg_8995" s="T299">heː</ta>
            <ta e="T301" id="Seg_8996" s="T300">kepseː-čči-bit</ta>
            <ta e="T302" id="Seg_8997" s="T301">itte-keːn-i</ta>
            <ta e="T303" id="Seg_8998" s="T302">ki͡e</ta>
            <ta e="T304" id="Seg_8999" s="T303">anɨ</ta>
            <ta e="T305" id="Seg_9000" s="T304">ulakan-nɨk</ta>
            <ta e="T306" id="Seg_9001" s="T305">da</ta>
            <ta e="T307" id="Seg_9002" s="T306">tu͡olkul-a-n-a</ta>
            <ta e="T308" id="Seg_9003" s="T307">ilik-ter</ta>
            <ta e="T309" id="Seg_9004" s="T308">eːt</ta>
            <ta e="T310" id="Seg_9005" s="T309">ulakan</ta>
            <ta e="T311" id="Seg_9006" s="T310">kɨːh-ɨ-m</ta>
            <ta e="T312" id="Seg_9007" s="T311">ere</ta>
            <ta e="T313" id="Seg_9008" s="T312">ogo-lor-o</ta>
            <ta e="T314" id="Seg_9009" s="T313">usku͡ola-ga</ta>
            <ta e="T315" id="Seg_9010" s="T314">bar-bɨt-tara</ta>
            <ta e="T316" id="Seg_9011" s="T315">kuččuguj</ta>
            <ta e="T317" id="Seg_9012" s="T316">ogo-lor-u-m</ta>
            <ta e="T318" id="Seg_9013" s="T317">ogo-loro</ta>
            <ta e="T319" id="Seg_9014" s="T318">kiːr-e</ta>
            <ta e="T320" id="Seg_9015" s="T319">ilik-ter</ta>
            <ta e="T321" id="Seg_9016" s="T320">usku͡ola-ga</ta>
            <ta e="T322" id="Seg_9017" s="T321">o-lor-go</ta>
            <ta e="T323" id="Seg_9018" s="T322">ol</ta>
            <ta e="T324" id="Seg_9019" s="T323">itte-keːn-i</ta>
            <ta e="T325" id="Seg_9020" s="T324">keps-iː-bin</ta>
            <ta e="T326" id="Seg_9021" s="T325">bu͡o</ta>
            <ta e="T327" id="Seg_9022" s="T326">itte-ni-bütte-ni</ta>
            <ta e="T328" id="Seg_9023" s="T327">ebeː</ta>
            <ta e="T329" id="Seg_9024" s="T328">di͡e-n</ta>
            <ta e="T330" id="Seg_9025" s="T329">ergiččij-e</ta>
            <ta e="T331" id="Seg_9026" s="T330">hɨldʼ-aːččɨ-lar</ta>
            <ta e="T332" id="Seg_9027" s="T331">bu͡o</ta>
            <ta e="T333" id="Seg_9028" s="T332">on-tu-gu-n</ta>
            <ta e="T334" id="Seg_9029" s="T333">kepseː</ta>
            <ta e="T335" id="Seg_9030" s="T334">o-nu</ta>
            <ta e="T336" id="Seg_9031" s="T335">oloŋko-loː</ta>
            <ta e="T337" id="Seg_9032" s="T336">mu-nu</ta>
            <ta e="T338" id="Seg_9033" s="T337">kepseː</ta>
            <ta e="T339" id="Seg_9034" s="T338">iti-ni</ta>
            <ta e="T340" id="Seg_9035" s="T339">ɨllaː</ta>
            <ta e="T341" id="Seg_9036" s="T340">o-lor-u</ta>
            <ta e="T342" id="Seg_9037" s="T341">haːtat-aːrɨ-gɨn</ta>
            <ta e="T343" id="Seg_9038" s="T342">ke</ta>
            <ta e="T344" id="Seg_9039" s="T343">ɨll-a-n-ɨ-ŋ</ta>
            <ta e="T345" id="Seg_9040" s="T344">keps-e-t-i-ŋ</ta>
            <ta e="T346" id="Seg_9041" s="T345">hin</ta>
            <ta e="T347" id="Seg_9042" s="T346">kojuː</ta>
            <ta e="T348" id="Seg_9043" s="T347">bu͡olla</ta>
            <ta e="T356" id="Seg_9044" s="T355">kaja</ta>
            <ta e="T357" id="Seg_9045" s="T356">mini͡ettere</ta>
            <ta e="T358" id="Seg_9046" s="T357">pirʼedka-lar-ɨ-m</ta>
            <ta e="T359" id="Seg_9047" s="T358">barɨ-ta</ta>
            <ta e="T360" id="Seg_9048" s="T359">oloŋko-hut</ta>
            <ta e="T361" id="Seg_9049" s="T360">e-ti-ler</ta>
            <ta e="T362" id="Seg_9050" s="T361">ɨllaː-ččɨ</ta>
            <ta e="T363" id="Seg_9051" s="T362">da</ta>
            <ta e="T364" id="Seg_9052" s="T363">e-ti-ler</ta>
            <ta e="T365" id="Seg_9053" s="T364">ɨrɨ͡a</ta>
            <ta e="T366" id="Seg_9054" s="T365">ɨll-a</ta>
            <ta e="T367" id="Seg_9055" s="T366">ɨll-a-n-a</ta>
            <ta e="T368" id="Seg_9056" s="T367">ɨrɨ͡a-laːk</ta>
            <ta e="T369" id="Seg_9057" s="T368">daː</ta>
            <ta e="T370" id="Seg_9058" s="T369">oloŋko-nu</ta>
            <ta e="T371" id="Seg_9059" s="T370">kepseː-čči</ta>
            <ta e="T372" id="Seg_9060" s="T371">meːne</ta>
            <ta e="T373" id="Seg_9061" s="T372">da</ta>
            <ta e="T374" id="Seg_9062" s="T373">oloŋko-nu</ta>
            <ta e="T375" id="Seg_9063" s="T374">keps-e</ta>
            <ta e="T376" id="Seg_9064" s="T375">oččogo</ta>
            <ta e="T377" id="Seg_9065" s="T376">bɨlɨr-gɨ</ta>
            <ta e="T378" id="Seg_9066" s="T377">üje-ge</ta>
            <ta e="T379" id="Seg_9067" s="T378">bu͡ollagɨnan</ta>
            <ta e="T380" id="Seg_9068" s="T379">kinoː-nɨ</ta>
            <ta e="T381" id="Seg_9069" s="T380">kör-ö-gün</ta>
            <ta e="T382" id="Seg_9070" s="T381">duː</ta>
            <ta e="T383" id="Seg_9071" s="T382">oččo</ta>
            <ta e="T384" id="Seg_9072" s="T383">daːgɨnɨ</ta>
            <ta e="T385" id="Seg_9073" s="T384">Ispidoːla-nɨ</ta>
            <ta e="T386" id="Seg_9074" s="T385">ihill-iː-gin</ta>
            <ta e="T387" id="Seg_9075" s="T386">duː</ta>
            <ta e="T388" id="Seg_9076" s="T387">mas</ta>
            <ta e="T389" id="Seg_9077" s="T388">oːnnʼuːr-u-nan</ta>
            <ta e="T390" id="Seg_9078" s="T389">onnʼ-uː-gun</ta>
            <ta e="T391" id="Seg_9079" s="T390">čɨːčaːk-tar-ɨ-nan</ta>
            <ta e="T392" id="Seg_9080" s="T391">iti</ta>
            <ta e="T393" id="Seg_9081" s="T392">hɨrga</ta>
            <ta e="T394" id="Seg_9082" s="T393">oŋost-o</ta>
            <ta e="T395" id="Seg_9083" s="T394">oŋost-o-gun</ta>
            <ta e="T396" id="Seg_9084" s="T395">tahaːra</ta>
            <ta e="T397" id="Seg_9085" s="T396">tagɨs-tak-kɨnan</ta>
            <ta e="T398" id="Seg_9086" s="T397">mu͡os</ta>
            <ta e="T399" id="Seg_9087" s="T398">tamnaː-n</ta>
            <ta e="T400" id="Seg_9088" s="T399">mu͡oh-u-nan</ta>
            <ta e="T401" id="Seg_9089" s="T400">oːnnʼ-u͡o-ŋ</ta>
            <ta e="T402" id="Seg_9090" s="T401">u͡ol</ta>
            <ta e="T403" id="Seg_9091" s="T402">ogo</ta>
            <ta e="T404" id="Seg_9092" s="T403">bu͡olla</ta>
            <ta e="T405" id="Seg_9093" s="T404">maːmɨt-tan-a</ta>
            <ta e="T406" id="Seg_9094" s="T405">oːnnʼ-u͡og-a</ta>
            <ta e="T407" id="Seg_9095" s="T406">hajɨn</ta>
            <ta e="T408" id="Seg_9096" s="T407">kajdak</ta>
            <ta e="T409" id="Seg_9097" s="T408">hɨldʼ-ar-gɨ-n</ta>
            <ta e="T410" id="Seg_9098" s="T409">taːh-ɨ</ta>
            <ta e="T411" id="Seg_9099" s="T410">munnʼ-u-n-aŋ-ŋɨn</ta>
            <ta e="T412" id="Seg_9100" s="T411">ölböküːn</ta>
            <ta e="T413" id="Seg_9101" s="T412">oːnnʼ-u͡o-ŋ</ta>
            <ta e="T414" id="Seg_9102" s="T413">hol</ta>
            <ta e="T415" id="Seg_9103" s="T414">kördük-keːčen</ta>
            <ta e="T416" id="Seg_9104" s="T415">kün-ü</ta>
            <ta e="T417" id="Seg_9105" s="T416">bɨh-a-gɨn</ta>
            <ta e="T418" id="Seg_9106" s="T417">bu͡olla</ta>
            <ta e="T419" id="Seg_9107" s="T418">bɨlɨr-gɨ</ta>
            <ta e="T420" id="Seg_9108" s="T419">ogo</ta>
            <ta e="T421" id="Seg_9109" s="T420">bɨlɨrgɨ-lɨː</ta>
            <ta e="T422" id="Seg_9110" s="T421">oːnnʼuː-r</ta>
            <ta e="T423" id="Seg_9111" s="T422">bu͡o</ta>
            <ta e="T424" id="Seg_9112" s="T423">ol</ta>
            <ta e="T425" id="Seg_9113" s="T424">bɨːh-ɨː-tɨ-nan</ta>
            <ta e="T426" id="Seg_9114" s="T425">anɨ</ta>
            <ta e="T427" id="Seg_9115" s="T426">bɨlɨr-gɨ</ta>
            <ta e="T428" id="Seg_9116" s="T427">oːnnʼuːr-u</ta>
            <ta e="T429" id="Seg_9117" s="T428">da</ta>
            <ta e="T430" id="Seg_9118" s="T429">anɨ</ta>
            <ta e="T431" id="Seg_9119" s="T430">da</ta>
            <ta e="T432" id="Seg_9120" s="T431">kör-ü͡ök-kütü-n</ta>
            <ta e="T433" id="Seg_9121" s="T432">da</ta>
            <ta e="T434" id="Seg_9122" s="T433">čaŋkarkaːtaːk</ta>
            <ta e="T435" id="Seg_9123" s="T434">oːnnʼuːr-u</ta>
            <ta e="T436" id="Seg_9124" s="T435">egel-bit-i-m</ta>
            <ta e="T437" id="Seg_9125" s="T436">bu͡olluŋ</ta>
            <ta e="T438" id="Seg_9126" s="T437">bu</ta>
            <ta e="T439" id="Seg_9127" s="T438">dojdu-ga</ta>
            <ta e="T440" id="Seg_9128" s="T439">hin</ta>
            <ta e="T441" id="Seg_9129" s="T440">köllör-ü͡ö-m</ta>
            <ta e="T442" id="Seg_9130" s="T441">ol</ta>
            <ta e="T443" id="Seg_9131" s="T442">oːnnʼ-uː</ta>
            <ta e="T444" id="Seg_9132" s="T443">kajdak</ta>
            <ta e="T445" id="Seg_9133" s="T444">oːnnʼuː-l-larɨ-n</ta>
            <ta e="T446" id="Seg_9134" s="T445">aŋar</ta>
            <ta e="T447" id="Seg_9135" s="T446">oːnnʼuːr-dar-ɨ</ta>
            <ta e="T448" id="Seg_9136" s="T447">oŋor</ta>
            <ta e="T449" id="Seg_9137" s="T448">oŋor-on</ta>
            <ta e="T450" id="Seg_9138" s="T449">egel-i͡ek-pi-n</ta>
            <ta e="T451" id="Seg_9139" s="T450">kihi-te</ta>
            <ta e="T452" id="Seg_9140" s="T451">hu͡op-pun</ta>
            <ta e="T453" id="Seg_9141" s="T452">ogonnʼor-u-m</ta>
            <ta e="T454" id="Seg_9142" s="T453">ol</ta>
            <ta e="T455" id="Seg_9143" s="T454">balʼnʼisa-ga</ta>
            <ta e="T456" id="Seg_9144" s="T455">hɨt-ar</ta>
            <ta e="T457" id="Seg_9145" s="T456">e-t-e</ta>
            <ta e="T458" id="Seg_9146" s="T457">bu͡olla</ta>
            <ta e="T459" id="Seg_9147" s="T458">biːr</ta>
            <ta e="T460" id="Seg_9148" s="T459">u͡ol-ɨ-m</ta>
            <ta e="T461" id="Seg_9149" s="T460">hin</ta>
            <ta e="T462" id="Seg_9150" s="T461">ɨ͡aldʼ-an</ta>
            <ta e="T463" id="Seg_9151" s="T462">balʼnʼisa-ttan</ta>
            <ta e="T464" id="Seg_9152" s="T463">taks-aːt</ta>
            <ta e="T465" id="Seg_9153" s="T464">ke</ta>
            <ta e="T466" id="Seg_9154" s="T465">kel-bit-i-m</ta>
            <ta e="T467" id="Seg_9155" s="T466">bu͡olla</ta>
            <ta e="T468" id="Seg_9156" s="T467">on-tu-m</ta>
            <ta e="T469" id="Seg_9157" s="T468">min</ta>
            <ta e="T470" id="Seg_9158" s="T469">tɨ͡a-ga</ta>
            <ta e="T471" id="Seg_9159" s="T470">baːr</ta>
            <ta e="T472" id="Seg_9160" s="T471">e-t-e</ta>
            <ta e="T473" id="Seg_9161" s="T472">on-tu-m</ta>
            <ta e="T474" id="Seg_9162" s="T473">ebe-ge</ta>
            <ta e="T475" id="Seg_9163" s="T474">kiːr-en</ta>
            <ta e="T476" id="Seg_9164" s="T475">ɨ͡aldʼ-ɨ-bɨt-a</ta>
            <ta e="T477" id="Seg_9165" s="T476">onton</ta>
            <ta e="T478" id="Seg_9166" s="T477">kojut</ta>
            <ta e="T479" id="Seg_9167" s="T478">taks-ɨ-bɨt-a</ta>
            <ta e="T480" id="Seg_9168" s="T479">kel-er-bit</ta>
            <ta e="T481" id="Seg_9169" s="T480">da</ta>
            <ta e="T482" id="Seg_9170" s="T481">o-nu</ta>
            <ta e="T483" id="Seg_9171" s="T482">purgaː-lar</ta>
            <ta e="T484" id="Seg_9172" s="T483">tu͡ok-tar</ta>
            <ta e="T485" id="Seg_9173" s="T484">bu͡o</ta>
            <ta e="T486" id="Seg_9174" s="T485">meheːj-deː-n</ta>
            <ta e="T487" id="Seg_9175" s="T486">h-onon</ta>
            <ta e="T488" id="Seg_9176" s="T487">tu͡ok</ta>
            <ta e="T489" id="Seg_9177" s="T488">elbek</ta>
            <ta e="T490" id="Seg_9178" s="T489">oːnnʼuːr</ta>
            <ta e="T491" id="Seg_9179" s="T490">egel-beteg-i-m</ta>
            <ta e="T492" id="Seg_9180" s="T491">urukku</ta>
            <ta e="T493" id="Seg_9181" s="T492">ogo</ta>
            <ta e="T494" id="Seg_9182" s="T493">oːnnʼuːr-u-n</ta>
            <ta e="T495" id="Seg_9183" s="T494">hin</ta>
            <ta e="T496" id="Seg_9184" s="T495">onton</ta>
            <ta e="T497" id="Seg_9185" s="T496">atɨn</ta>
            <ta e="T498" id="Seg_9186" s="T497">köllör-üː-ge</ta>
            <ta e="T499" id="Seg_9187" s="T498">egel-i͡ek</ta>
            <ta e="T500" id="Seg_9188" s="T499">e-ti-m</ta>
            <ta e="T501" id="Seg_9189" s="T500">urut</ta>
            <ta e="T502" id="Seg_9190" s="T501">beje-m</ta>
            <ta e="T503" id="Seg_9191" s="T502">oːnnʼoː-n</ta>
            <ta e="T504" id="Seg_9192" s="T503">ü͡öskeː-bip-pin</ta>
            <ta e="T505" id="Seg_9193" s="T504">ke</ta>
            <ta e="T506" id="Seg_9194" s="T505">onton</ta>
            <ta e="T518" id="Seg_9195" s="T517">te</ta>
            <ta e="T519" id="Seg_9196" s="T518">mu͡ot-u-nan</ta>
            <ta e="T520" id="Seg_9197" s="T519">hör-üː-büt</ta>
            <ta e="T521" id="Seg_9198" s="T520">üːt-teːk</ta>
            <ta e="T522" id="Seg_9199" s="T521">mas-ka</ta>
            <ta e="T523" id="Seg_9200" s="T522">bah-ɨ-nan</ta>
            <ta e="T524" id="Seg_9201" s="T523">taŋnarɨ</ta>
            <ta e="T525" id="Seg_9202" s="T524">tur-an</ta>
            <ta e="T526" id="Seg_9203" s="T525">ergij-er</ta>
            <ta e="T527" id="Seg_9204" s="T526">o-nu</ta>
            <ta e="T528" id="Seg_9205" s="T527">kör-dök-künen</ta>
            <ta e="T529" id="Seg_9206" s="T528">ere</ta>
            <ta e="T530" id="Seg_9207" s="T529">bil-i͡e-ŋ</ta>
            <ta e="T531" id="Seg_9208" s="T530">bu͡olla</ta>
            <ta e="T532" id="Seg_9209" s="T531">kajdak</ta>
            <ta e="T533" id="Seg_9210" s="T532">anɨ</ta>
            <ta e="T534" id="Seg_9211" s="T533">kepseː-m-min</ta>
            <ta e="T535" id="Seg_9212" s="T534">kajdak</ta>
            <ta e="T536" id="Seg_9213" s="T535">keps-i͡e-m=ij</ta>
            <ta e="T537" id="Seg_9214" s="T536">köllör-ü͡ö-m</ta>
            <ta e="T538" id="Seg_9215" s="T537">ol</ta>
            <ta e="T539" id="Seg_9216" s="T538">kör-dök-küne</ta>
            <ta e="T540" id="Seg_9217" s="T539">bil-eːr</ta>
            <ta e="T559" id="Seg_9218" s="T558">heː</ta>
            <ta e="T560" id="Seg_9219" s="T559">bil-e-bin</ta>
            <ta e="T561" id="Seg_9220" s="T560">onnuk-tar-ɨ</ta>
            <ta e="T562" id="Seg_9221" s="T561">onnuk</ta>
            <ta e="T563" id="Seg_9222" s="T562">onnuk-ta</ta>
            <ta e="T564" id="Seg_9223" s="T563">meːne</ta>
            <ta e="T565" id="Seg_9224" s="T564">lʼuboj</ta>
            <ta e="T566" id="Seg_9225" s="T565">ɨrɨ͡a-ta</ta>
            <ta e="T567" id="Seg_9226" s="T566">ɨll-ɨ͡a-m</ta>
            <ta e="T568" id="Seg_9227" s="T567">du͡o</ta>
            <ta e="T569" id="Seg_9228" s="T568">lʼuboj-da</ta>
            <ta e="T570" id="Seg_9229" s="T569">lʼuboj</ta>
            <ta e="T571" id="Seg_9230" s="T570">ɨrɨ͡a-ta</ta>
            <ta e="T572" id="Seg_9231" s="T571">ɨll-ɨ͡a-m</ta>
            <ta e="T573" id="Seg_9232" s="T572">beːbe</ta>
            <ta e="T574" id="Seg_9233" s="T573">kel-e</ta>
            <ta e="T575" id="Seg_9234" s="T574">tar-pat</ta>
            <ta e="T576" id="Seg_9235" s="T575">itin-ti-ŋ</ta>
            <ta e="T577" id="Seg_9236" s="T576">kel-i͡e</ta>
            <ta e="T578" id="Seg_9237" s="T577">kel-i͡e-ne</ta>
            <ta e="T579" id="Seg_9238" s="T578">hu͡ok</ta>
            <ta e="T580" id="Seg_9239" s="T579">bu͡ol-aːččɨ</ta>
            <ta e="T581" id="Seg_9240" s="T580">kü͡ömej-deri-n</ta>
            <ta e="T582" id="Seg_9241" s="T581">öjdöː-böp-pün</ta>
            <ta e="T583" id="Seg_9242" s="T582">umn-u-bup-pun</ta>
            <ta e="T584" id="Seg_9243" s="T583">urut</ta>
            <ta e="T585" id="Seg_9244" s="T584">bu͡ollagɨna</ta>
            <ta e="T586" id="Seg_9245" s="T585">gini-nen</ta>
            <ta e="T587" id="Seg_9246" s="T586">zanʼimaːtsa-l-ɨ͡a-m</ta>
            <ta e="T588" id="Seg_9247" s="T587">di͡e-m-min</ta>
            <ta e="T589" id="Seg_9248" s="T588">olus</ta>
            <ta e="T590" id="Seg_9249" s="T589">dʼelaj-daː-bakka</ta>
            <ta e="T591" id="Seg_9250" s="T590">hɨldʼ-ɨ-bɨt-ɨ-m</ta>
            <ta e="T592" id="Seg_9251" s="T591">bu͡olla</ta>
            <ta e="T593" id="Seg_9252" s="T592">ol</ta>
            <ta e="T594" id="Seg_9253" s="T593">ihin</ta>
            <ta e="T595" id="Seg_9254" s="T594">umn-a-bɨn</ta>
            <ta e="T611" id="Seg_9255" s="T610">iti</ta>
            <ta e="T612" id="Seg_9256" s="T611">öl-büt-e</ta>
            <ta e="T613" id="Seg_9257" s="T612">iti</ta>
            <ta e="T614" id="Seg_9258" s="T613">Baja</ta>
            <ta e="T615" id="Seg_9259" s="T614">di͡e-n</ta>
            <ta e="T616" id="Seg_9260" s="T615">kɨːs</ta>
            <ta e="T617" id="Seg_9261" s="T616">ɨllaː-bɨt-a</ta>
            <ta e="T618" id="Seg_9262" s="T617">ɨllaː-bɨt</ta>
            <ta e="T619" id="Seg_9263" s="T618">u͡ol-a</ta>
            <ta e="T620" id="Seg_9264" s="T619">hin</ta>
            <ta e="T621" id="Seg_9265" s="T620">öl-büt-e</ta>
            <ta e="T642" id="Seg_9266" s="T641">heː</ta>
            <ta e="T643" id="Seg_9267" s="T642">onnuk-tarɨ-nan</ta>
            <ta e="T644" id="Seg_9268" s="T643">Toːnʼa-nɨ</ta>
            <ta e="T645" id="Seg_9269" s="T644">gɨtta</ta>
            <ta e="T646" id="Seg_9270" s="T645">hin</ta>
            <ta e="T647" id="Seg_9271" s="T646">eː</ta>
            <ta e="T648" id="Seg_9272" s="T647">tu͡ok-t-u͡op-put</ta>
            <ta e="T649" id="Seg_9273" s="T648">hɨrs-ar-ɨ</ta>
            <ta e="T650" id="Seg_9274" s="T649">hin</ta>
            <ta e="T651" id="Seg_9275" s="T650">ɨll-ɨ͡ap-pɨt</ta>
            <ta e="T652" id="Seg_9276" s="T651">bu͡olla</ta>
            <ta e="T653" id="Seg_9277" s="T652">meːne</ta>
            <ta e="T654" id="Seg_9278" s="T653">tɨːn-a</ta>
            <ta e="T655" id="Seg_9279" s="T654">tɨːn-a</ta>
            <ta e="T656" id="Seg_9280" s="T655">eː</ta>
            <ta e="T657" id="Seg_9281" s="T656">pʼerʼexvat dɨxanʼija-nɨ</ta>
            <ta e="T658" id="Seg_9282" s="T657">tɨːn-a</ta>
            <ta e="T659" id="Seg_9283" s="T658">tɨːn-a-gɨn</ta>
            <ta e="T660" id="Seg_9284" s="T659">bu͡olla</ta>
            <ta e="T663" id="Seg_9285" s="T662">ɨllaː</ta>
            <ta e="T664" id="Seg_9286" s="T663">di͡e-tek-kine</ta>
            <ta e="T665" id="Seg_9287" s="T664">ɨll-ɨ͡a-m</ta>
            <ta e="T666" id="Seg_9288" s="T665">meːne</ta>
            <ta e="T667" id="Seg_9289" s="T666">tɨːn</ta>
            <ta e="T668" id="Seg_9290" s="T667">bɨldʼ-a-h-ar</ta>
            <ta e="T669" id="Seg_9291" s="T668">kohoːn</ta>
            <ta e="T685" id="Seg_9292" s="T684">iti-ni</ta>
            <ta e="T686" id="Seg_9293" s="T685">bu͡olla</ta>
            <ta e="T687" id="Seg_9294" s="T686">hogotok</ta>
            <ta e="T688" id="Seg_9295" s="T687">tɨːn-nan</ta>
            <ta e="T689" id="Seg_9296" s="T688">ɨllaː-ččɨ-lar</ta>
            <ta e="T690" id="Seg_9297" s="T689">ühü</ta>
            <ta e="T691" id="Seg_9298" s="T690">bɨlɨrgɨ-lar</ta>
            <ta e="T692" id="Seg_9299" s="T691">hogotok</ta>
            <ta e="T693" id="Seg_9300" s="T692">tɨːn-nan</ta>
            <ta e="T694" id="Seg_9301" s="T693">tɨːn</ta>
            <ta e="T695" id="Seg_9302" s="T694">bɨldʼ-a-h-an-nar</ta>
            <ta e="T696" id="Seg_9303" s="T695">dʼe</ta>
            <ta e="T697" id="Seg_9304" s="T696">ginner</ta>
            <ta e="T698" id="Seg_9305" s="T697">oːnnʼuːr-dara</ta>
            <ta e="T699" id="Seg_9306" s="T698">ol</ta>
            <ta e="T700" id="Seg_9307" s="T699">bu͡ol-l-a</ta>
            <ta e="T701" id="Seg_9308" s="T700">bu</ta>
            <ta e="T702" id="Seg_9309" s="T701">artʼis-tar</ta>
            <ta e="T703" id="Seg_9310" s="T702">anɨ</ta>
            <ta e="T704" id="Seg_9311" s="T703">oːnnʼuː-l-larɨ-n</ta>
            <ta e="T705" id="Seg_9312" s="T704">kördük</ta>
            <ta e="T706" id="Seg_9313" s="T705">hiti</ta>
            <ta e="T707" id="Seg_9314" s="T706">hogotok</ta>
            <ta e="T708" id="Seg_9315" s="T707">tɨːn-nan</ta>
            <ta e="T709" id="Seg_9316" s="T708">tij-eːčči</ta>
            <ta e="T710" id="Seg_9317" s="T709">e-t-e</ta>
            <ta e="T711" id="Seg_9318" s="T710">ühü</ta>
            <ta e="T712" id="Seg_9319" s="T711">Iniki͡en</ta>
            <ta e="T713" id="Seg_9320" s="T712">di͡e-n</ta>
            <ta e="T714" id="Seg_9321" s="T713">kihi</ta>
            <ta e="T715" id="Seg_9322" s="T714">iti</ta>
            <ta e="T716" id="Seg_9323" s="T715">itini-ge</ta>
            <ta e="T728" id="Seg_9324" s="T727">iti</ta>
            <ta e="T729" id="Seg_9325" s="T728">mini͡ene</ta>
            <ta e="T730" id="Seg_9326" s="T729">kɨːh-ɨ-m</ta>
            <ta e="T731" id="Seg_9327" s="T730">iti</ta>
            <ta e="T761" id="Seg_9328" s="T760">heː</ta>
            <ta e="T762" id="Seg_9329" s="T761">dʼe</ta>
            <ta e="T763" id="Seg_9330" s="T762">ol</ta>
            <ta e="T764" id="Seg_9331" s="T763">min</ta>
            <ta e="T765" id="Seg_9332" s="T764">kepseː-bit</ta>
            <ta e="T766" id="Seg_9333" s="T765">oloŋko-bu-n</ta>
            <ta e="T767" id="Seg_9334" s="T766">koptoːk</ta>
            <ta e="T768" id="Seg_9335" s="T767">Taiska-nɨ</ta>
            <ta e="T769" id="Seg_9336" s="T768">kepseː-bit-ter</ta>
            <ta e="T770" id="Seg_9337" s="T769">minigi-n</ta>
            <ta e="T771" id="Seg_9338" s="T770">keps-e-t-en</ta>
            <ta e="T772" id="Seg_9339" s="T771">baraːn</ta>
            <ta e="T773" id="Seg_9340" s="T772">kel-bit-tere</ta>
            <ta e="T774" id="Seg_9341" s="T773">manna</ta>
            <ta e="T775" id="Seg_9342" s="T774">kel-eːri-ler</ta>
            <ta e="T776" id="Seg_9343" s="T775">kutta</ta>
            <ta e="T777" id="Seg_9344" s="T776">dʼɨl-ga</ta>
            <ta e="T778" id="Seg_9345" s="T777">kel-bit-tere</ta>
            <ta e="T779" id="Seg_9346" s="T778">oːnnʼ-oːru-lar</ta>
            <ta e="T780" id="Seg_9347" s="T779">on-tu-larɨ-n</ta>
            <ta e="T781" id="Seg_9348" s="T780">daːgɨnɨ</ta>
            <ta e="T782" id="Seg_9349" s="T781">iti</ta>
            <ta e="T783" id="Seg_9350" s="T782">kajdak</ta>
            <ta e="T784" id="Seg_9351" s="T783">kajdak</ta>
            <ta e="T785" id="Seg_9352" s="T784">kepseː-n</ta>
            <ta e="T786" id="Seg_9353" s="T785">keːs-pit</ta>
            <ta e="T787" id="Seg_9354" s="T786">e-ti-lere</ta>
            <ta e="T788" id="Seg_9355" s="T787">onton</ta>
            <ta e="T789" id="Seg_9356" s="T788">obu͡oj-a</ta>
            <ta e="T793" id="Seg_9357" s="T792">eː</ta>
            <ta e="T794" id="Seg_9358" s="T793">ist-i-bit-i-m</ta>
            <ta e="T795" id="Seg_9359" s="T794">o-nu</ta>
            <ta e="T796" id="Seg_9360" s="T795">ist-em-min</ta>
            <ta e="T797" id="Seg_9361" s="T796">gɨn-a-bɨn</ta>
            <ta e="T798" id="Seg_9362" s="T797">iti</ta>
            <ta e="T800" id="Seg_9363" s="T799">o-lor-u</ta>
            <ta e="T802" id="Seg_9364" s="T801">o-lor-u</ta>
            <ta e="T803" id="Seg_9365" s="T802">kel-el-ler</ta>
            <ta e="T804" id="Seg_9366" s="T803">o-lor-u</ta>
            <ta e="T805" id="Seg_9367" s="T804">hemel-iː-bin</ta>
            <ta e="T806" id="Seg_9368" s="T805">togo</ta>
            <ta e="T807" id="Seg_9369" s="T806">ke</ta>
            <ta e="T808" id="Seg_9370" s="T807">iti</ta>
            <ta e="T809" id="Seg_9371" s="T808">o-non-ma-nan</ta>
            <ta e="T810" id="Seg_9372" s="T809">keps-iː-git</ta>
            <ta e="T811" id="Seg_9373" s="T810">ile-liː</ta>
            <ta e="T812" id="Seg_9374" s="T811">kepseː-bek-kit</ta>
            <ta e="T813" id="Seg_9375" s="T812">oloŋko-gutu-n</ta>
            <ta e="T820" id="Seg_9376" s="T819">vsʼo ravno</ta>
            <ta e="T821" id="Seg_9377" s="T820">hin</ta>
            <ta e="T822" id="Seg_9378" s="T821">dʼüːll-e-n-er</ta>
            <ta e="T823" id="Seg_9379" s="T822">onton</ta>
            <ta e="T824" id="Seg_9380" s="T823">asnavnoj-ɨ</ta>
            <ta e="T825" id="Seg_9381" s="T824">hin</ta>
            <ta e="T826" id="Seg_9382" s="T825">kepsiː-l-ler</ta>
            <ta e="T849" id="Seg_9383" s="T848">eː</ta>
            <ta e="T850" id="Seg_9384" s="T849">baːr</ta>
            <ta e="T851" id="Seg_9385" s="T850">eːt</ta>
            <ta e="T852" id="Seg_9386" s="T851">ol</ta>
            <ta e="T853" id="Seg_9387" s="T852">oloŋko-nu</ta>
            <ta e="T854" id="Seg_9388" s="T853">bil-er</ta>
            <ta e="T855" id="Seg_9389" s="T854">min</ta>
            <ta e="T856" id="Seg_9390" s="T855">ubaj-ɨ-m</ta>
            <ta e="T857" id="Seg_9391" s="T856">baːr</ta>
            <ta e="T858" id="Seg_9392" s="T857">ɨbaj-dar-ɨ-m</ta>
            <ta e="T859" id="Seg_9393" s="T858">onton</ta>
            <ta e="T860" id="Seg_9394" s="T859">ikk-i͡en</ta>
            <ta e="T861" id="Seg_9395" s="T860">da</ta>
            <ta e="T862" id="Seg_9396" s="T861">bil-el-ler</ta>
            <ta e="T863" id="Seg_9397" s="T862">ikki</ta>
            <ta e="T864" id="Seg_9398" s="T863">ubaj-daːk-pɨn</ta>
            <ta e="T865" id="Seg_9399" s="T864">ikk-i͡en</ta>
            <ta e="T866" id="Seg_9400" s="T865">da</ta>
            <ta e="T867" id="Seg_9401" s="T866">bil-e</ta>
            <ta e="T868" id="Seg_9402" s="T867">biːrgeh-e</ta>
            <ta e="T869" id="Seg_9403" s="T868">kɨrdžagas</ta>
            <ta e="T870" id="Seg_9404" s="T869">biːrgeh-e</ta>
            <ta e="T871" id="Seg_9405" s="T870">minigi-nneːger</ta>
            <ta e="T872" id="Seg_9406" s="T871">hin</ta>
            <ta e="T873" id="Seg_9407" s="T872">ubaj</ta>
            <ta e="T874" id="Seg_9408" s="T873">bil-el-ler</ta>
            <ta e="T875" id="Seg_9409" s="T874">o-lor</ta>
            <ta e="T876" id="Seg_9410" s="T875">oloŋko-lor-u</ta>
            <ta e="T877" id="Seg_9411" s="T876">ɨrɨ͡a-laːk-tar-ɨ</ta>
            <ta e="T878" id="Seg_9412" s="T877">daː</ta>
            <ta e="T879" id="Seg_9413" s="T878">bil-el-ler</ta>
            <ta e="T880" id="Seg_9414" s="T879">ɨrɨ͡a-ta</ta>
            <ta e="T881" id="Seg_9415" s="T880">daː</ta>
            <ta e="T882" id="Seg_9416" s="T881">hu͡og-u</ta>
            <ta e="T883" id="Seg_9417" s="T882">bil-e</ta>
            <ta e="T888" id="Seg_9418" s="T887">meːne</ta>
            <ta e="T889" id="Seg_9419" s="T888">kihi-ler</ta>
            <ta e="T890" id="Seg_9420" s="T889">iti</ta>
            <ta e="T891" id="Seg_9421" s="T890">urut</ta>
            <ta e="T892" id="Seg_9422" s="T891">oloŋko-hut</ta>
            <ta e="T893" id="Seg_9423" s="T892">kihi-ler-bit</ta>
            <ta e="T894" id="Seg_9424" s="T893">baran-nɨ-lar</ta>
            <ta e="T895" id="Seg_9425" s="T894">bu͡olla</ta>
            <ta e="T896" id="Seg_9426" s="T895">anɨ</ta>
            <ta e="T897" id="Seg_9427" s="T896">ile</ta>
            <ta e="T898" id="Seg_9428" s="T897">ile</ta>
            <ta e="T899" id="Seg_9429" s="T898">min</ta>
            <ta e="T900" id="Seg_9430" s="T899">bil-er</ta>
            <ta e="T901" id="Seg_9431" s="T900">oloŋko-hut</ta>
            <ta e="T902" id="Seg_9432" s="T901">kihi-ler-i-m</ta>
            <ta e="T903" id="Seg_9433" s="T902">baran-nɨ-lar</ta>
            <ta e="T904" id="Seg_9434" s="T903">kim</ta>
            <ta e="T905" id="Seg_9435" s="T904">baːr-a</ta>
            <ta e="T906" id="Seg_9436" s="T905">bu͡ol-u͡o=j</ta>
            <ta e="T907" id="Seg_9437" s="T906">anɨ</ta>
            <ta e="T908" id="Seg_9438" s="T907">oloŋko-hut</ta>
            <ta e="T909" id="Seg_9439" s="T908">kihi</ta>
            <ta e="T910" id="Seg_9440" s="T909">kim-i</ta>
            <ta e="T911" id="Seg_9441" s="T910">da</ta>
            <ta e="T912" id="Seg_9442" s="T911">bil-bep-pin</ta>
            <ta e="T913" id="Seg_9443" s="T912">anɨ</ta>
            <ta e="T914" id="Seg_9444" s="T913">oloŋko-hut</ta>
            <ta e="T915" id="Seg_9445" s="T914">kihi-ni</ta>
            <ta e="T916" id="Seg_9446" s="T915">ile</ta>
            <ta e="T917" id="Seg_9447" s="T916">oloŋko-hut</ta>
            <ta e="T918" id="Seg_9448" s="T917">kihi-ni</ta>
            <ta e="T933" id="Seg_9449" s="T932">hu͡ok</ta>
            <ta e="T934" id="Seg_9450" s="T933">hu͡ok</ta>
            <ta e="T935" id="Seg_9451" s="T934">urut</ta>
            <ta e="T936" id="Seg_9452" s="T935">bu͡olla</ta>
            <ta e="T937" id="Seg_9453" s="T936">kim-nere</ta>
            <ta e="T938" id="Seg_9454" s="T937">daːganɨ</ta>
            <ta e="T939" id="Seg_9455" s="T938">itigirdi-keːčen</ta>
            <ta e="T940" id="Seg_9456" s="T939">gɨm-ma-tɨ-lar</ta>
            <ta e="T941" id="Seg_9457" s="T940">bu</ta>
            <ta e="T942" id="Seg_9458" s="T941">anɨ</ta>
            <ta e="T943" id="Seg_9459" s="T942">bu</ta>
            <ta e="T944" id="Seg_9460" s="T943">bɨlɨrɨːŋ-ŋɨ-ttan</ta>
            <ta e="T945" id="Seg_9461" s="T944">bettek</ta>
            <ta e="T946" id="Seg_9462" s="T945">bu</ta>
            <ta e="T947" id="Seg_9463" s="T946">ide</ta>
            <ta e="T948" id="Seg_9464" s="T947">bul-lu-lar</ta>
            <ta e="T949" id="Seg_9465" s="T948">bu</ta>
            <ta e="T950" id="Seg_9466" s="T949">urukku</ta>
            <ta e="T951" id="Seg_9467" s="T950">oloŋko</ta>
            <ta e="T952" id="Seg_9468" s="T951">ol</ta>
            <ta e="T953" id="Seg_9469" s="T952">ihin</ta>
            <ta e="T954" id="Seg_9470" s="T953">bihigi</ta>
            <ta e="T955" id="Seg_9471" s="T954">tahɨččɨ</ta>
            <ta e="T956" id="Seg_9472" s="T955">umn-u-bup-put</ta>
            <ta e="T957" id="Seg_9473" s="T956">o-nu</ta>
            <ta e="T958" id="Seg_9474" s="T957">oloŋko-lor-butu-n</ta>
            <ta e="T959" id="Seg_9475" s="T958">barɨ-tɨ-n</ta>
            <ta e="T960" id="Seg_9476" s="T959">umn-an</ta>
            <ta e="T961" id="Seg_9477" s="T960">ih-e-bit</ta>
            <ta e="T962" id="Seg_9478" s="T961">bu͡o</ta>
            <ta e="T963" id="Seg_9479" s="T962">barɨ-tɨ-n</ta>
            <ta e="T964" id="Seg_9480" s="T963">kaččaga</ta>
            <ta e="T965" id="Seg_9481" s="T964">da</ta>
            <ta e="T966" id="Seg_9482" s="T965">kepseː-bek-kin</ta>
            <ta e="T967" id="Seg_9483" s="T966">ɨrɨ͡a-nɨ</ta>
            <ta e="T968" id="Seg_9484" s="T967">da</ta>
            <ta e="T969" id="Seg_9485" s="T968">umn-a-gɨn</ta>
            <ta e="T970" id="Seg_9486" s="T969">oloŋko-nu</ta>
            <ta e="T971" id="Seg_9487" s="T970">da</ta>
            <ta e="T972" id="Seg_9488" s="T971">umn-a-gɨn</ta>
            <ta e="T973" id="Seg_9489" s="T972">ustorija-nɨ</ta>
            <ta e="T974" id="Seg_9490" s="T973">kepsiː-r-e</ta>
            <ta e="T975" id="Seg_9491" s="T974">bu͡olla</ta>
            <ta e="T976" id="Seg_9492" s="T975">Onʼiːsim</ta>
            <ta e="T977" id="Seg_9493" s="T976">ogonnʼor</ta>
            <ta e="T978" id="Seg_9494" s="T977">baːr</ta>
            <ta e="T979" id="Seg_9495" s="T978">oloŋko-hut</ta>
            <ta e="T980" id="Seg_9496" s="T979">emi͡e</ta>
            <ta e="T991" id="Seg_9497" s="T990">tu͡ok</ta>
            <ta e="T992" id="Seg_9498" s="T991">bil-i͡e=j</ta>
            <ta e="T993" id="Seg_9499" s="T992">da</ta>
            <ta e="T994" id="Seg_9500" s="T993">ol</ta>
            <ta e="T995" id="Seg_9501" s="T994">giniler-i</ta>
            <ta e="T996" id="Seg_9502" s="T995">oloŋko</ta>
            <ta e="T997" id="Seg_9503" s="T996">iŋ-er-e</ta>
            <ta e="T998" id="Seg_9504" s="T997">dʼürü</ta>
            <ta e="T999" id="Seg_9505" s="T998">giniler-ge</ta>
            <ta e="T1000" id="Seg_9506" s="T999">tu͡ok</ta>
            <ta e="T1001" id="Seg_9507" s="T1000">bil</ta>
            <ta e="T1002" id="Seg_9508" s="T1001">čubu-čubu</ta>
            <ta e="T1003" id="Seg_9509" s="T1002">anɨ</ta>
            <ta e="T1004" id="Seg_9510" s="T1003">oloŋko-l-o-s-pop-put</ta>
            <ta e="T1005" id="Seg_9511" s="T1004">bu͡o</ta>
            <ta e="T1006" id="Seg_9512" s="T1005">meːne</ta>
            <ta e="T1007" id="Seg_9513" s="T1006">kepseː</ta>
            <ta e="T1008" id="Seg_9514" s="T1007">ere</ta>
            <ta e="T1009" id="Seg_9515" s="T1008">di͡e-tek-terine</ta>
            <ta e="T1010" id="Seg_9516" s="T1009">keps-iː-bin</ta>
            <ta e="T1011" id="Seg_9517" s="T1010">naːda-dak-tarɨnan</ta>
            <ta e="T1012" id="Seg_9518" s="T1011">keps-e-t-tek-terine</ta>
            <ta e="T1013" id="Seg_9519" s="T1012">ere</ta>
            <ta e="T1014" id="Seg_9520" s="T1013">keps-iː-gin</ta>
            <ta e="T1015" id="Seg_9521" s="T1014">bu͡o</ta>
            <ta e="T1016" id="Seg_9522" s="T1015">taːk</ta>
            <ta e="T1017" id="Seg_9523" s="T1016">da</ta>
            <ta e="T1018" id="Seg_9524" s="T1017">meːne</ta>
            <ta e="T1019" id="Seg_9525" s="T1018">kaččaga</ta>
            <ta e="T1020" id="Seg_9526" s="T1019">da</ta>
            <ta e="T1021" id="Seg_9527" s="T1020">oloŋko-l-o-s-pop-put</ta>
            <ta e="T1022" id="Seg_9528" s="T1021">bu͡o</ta>
            <ta e="T1042" id="Seg_9529" s="T1041">heː</ta>
            <ta e="T1054" id="Seg_9530" s="T1053">kaja</ta>
            <ta e="T1055" id="Seg_9531" s="T1054">atɨn-na</ta>
            <ta e="T1056" id="Seg_9532" s="T1055">bu͡ol-u-mna</ta>
            <ta e="T1075" id="Seg_9533" s="T1074">ogo-m</ta>
            <ta e="T1076" id="Seg_9534" s="T1075">tuh-u-nan</ta>
            <ta e="T1077" id="Seg_9535" s="T1076">küččüküːkeːn</ta>
            <ta e="T1078" id="Seg_9536" s="T1077">stʼix</ta>
            <ta e="T1079" id="Seg_9537" s="T1078">obu͡oj-u-n</ta>
            <ta e="T1080" id="Seg_9538" s="T1079">oŋor-but-u-m</ta>
            <ta e="T1081" id="Seg_9539" s="T1080">Ajsülüː</ta>
            <ta e="T1082" id="Seg_9540" s="T1081">tuh-u-nan</ta>
            <ta e="T1083" id="Seg_9541" s="T1082">sulus-tar</ta>
            <ta e="T1084" id="Seg_9542" s="T1083">kallaːŋ-ŋa</ta>
            <ta e="T1085" id="Seg_9543" s="T1084">köst-öl-lör</ta>
            <ta e="T1086" id="Seg_9544" s="T1085">utuj</ta>
            <ta e="T1087" id="Seg_9545" s="T1086">utuj</ta>
            <ta e="T1088" id="Seg_9546" s="T1087">Ajsülüː</ta>
            <ta e="T1089" id="Seg_9547" s="T1088">üje-bit</ta>
            <ta e="T1090" id="Seg_9548" s="T1089">ularɨj-d-a</ta>
            <ta e="T1091" id="Seg_9549" s="T1090">oduː</ta>
            <ta e="T1092" id="Seg_9550" s="T1091">atɨn</ta>
            <ta e="T1093" id="Seg_9551" s="T1092">kem</ta>
            <ta e="T1094" id="Seg_9552" s="T1093">kel-l-e</ta>
            <ta e="T1095" id="Seg_9553" s="T1094">türgen-keːn-nik</ta>
            <ta e="T1096" id="Seg_9554" s="T1095">ulaːt-aːr</ta>
            <ta e="T1097" id="Seg_9555" s="T1096">sap-pɨ-n</ta>
            <ta e="T1098" id="Seg_9556" s="T1097">inne-bi-n</ta>
            <ta e="T1099" id="Seg_9557" s="T1098">tut-aːr</ta>
            <ta e="T1100" id="Seg_9558" s="T1099">min</ta>
            <ta e="T1101" id="Seg_9559" s="T1100">üje-bi-n</ta>
            <ta e="T1102" id="Seg_9560" s="T1101">üje-len-eːr</ta>
            <ta e="T1103" id="Seg_9561" s="T1102">kihi͡e-ke</ta>
            <ta e="T1104" id="Seg_9562" s="T1103">kihi</ta>
            <ta e="T1105" id="Seg_9563" s="T1104">d-e-t-eːr</ta>
            <ta e="T1106" id="Seg_9564" s="T1105">ogo-koːn-u-m</ta>
            <ta e="T1107" id="Seg_9565" s="T1106">ogo-tu-n</ta>
            <ta e="T1108" id="Seg_9566" s="T1107">bihig-i-ger</ta>
            <ta e="T1109" id="Seg_9567" s="T1108">bil-iː-bin</ta>
            <ta e="T1110" id="Seg_9568" s="T1109">attɨ-tɨ-gar</ta>
            <ta e="T1111" id="Seg_9569" s="T1110">olor-o-bun</ta>
            <ta e="T1112" id="Seg_9570" s="T1111">orgujakaːn</ta>
            <ta e="T1113" id="Seg_9571" s="T1112">ɨll-a-n-nɨ-m</ta>
         </annotation>
         <annotation name="mp" tierref="mp-UkET">
            <ta e="T23" id="Seg_9572" s="T21">min</ta>
            <ta e="T25" id="Seg_9573" s="T23">i</ta>
            <ta e="T26" id="Seg_9574" s="T25">beje-m</ta>
            <ta e="T27" id="Seg_9575" s="T26">töröː-BIT</ta>
            <ta e="T28" id="Seg_9576" s="T27">hir-I-m</ta>
            <ta e="T29" id="Seg_9577" s="T28">bu</ta>
            <ta e="T30" id="Seg_9578" s="T29">dek</ta>
            <ta e="T31" id="Seg_9579" s="T30">e-TI-tA</ta>
            <ta e="T32" id="Seg_9580" s="T31">mu͡ora</ta>
            <ta e="T33" id="Seg_9581" s="T32">dek</ta>
            <ta e="T34" id="Seg_9582" s="T33">ol</ta>
            <ta e="T35" id="Seg_9583" s="T34">dek</ta>
            <ta e="T37" id="Seg_9584" s="T36">kim</ta>
            <ta e="T38" id="Seg_9585" s="T37">dek</ta>
            <ta e="T39" id="Seg_9586" s="T38">eː</ta>
            <ta e="T40" id="Seg_9587" s="T39">onton</ta>
            <ta e="T41" id="Seg_9588" s="T40">Rɨmnaj</ta>
            <ta e="T42" id="Seg_9589" s="T41">onu͡or-GItI-GAr</ta>
            <ta e="T43" id="Seg_9590" s="T42">iti</ta>
            <ta e="T44" id="Seg_9591" s="T43">mu͡ora</ta>
            <ta e="T45" id="Seg_9592" s="T44">dek</ta>
            <ta e="T46" id="Seg_9593" s="T45">töröː-BIT-I-m</ta>
            <ta e="T47" id="Seg_9594" s="T46">onton</ta>
            <ta e="T48" id="Seg_9595" s="T47">er-GA</ta>
            <ta e="T49" id="Seg_9596" s="T48">bar-An-BIn</ta>
            <ta e="T50" id="Seg_9597" s="T49">taːs</ta>
            <ta e="T51" id="Seg_9598" s="T50">dek</ta>
            <ta e="T52" id="Seg_9599" s="T51">hir-LAN-BIT-I-m</ta>
            <ta e="T53" id="Seg_9600" s="T52">ogo-LAr-I-m</ta>
            <ta e="T54" id="Seg_9601" s="T53">da</ta>
            <ta e="T55" id="Seg_9602" s="T54">tu͡ok</ta>
            <ta e="T56" id="Seg_9603" s="T55">kajdak</ta>
            <ta e="T57" id="Seg_9604" s="T56">kepseː-An</ta>
            <ta e="T58" id="Seg_9605" s="T57">da</ta>
            <ta e="T59" id="Seg_9606" s="T58">tu͡ok-nI</ta>
            <ta e="T60" id="Seg_9607" s="T59">kepseː-IAK-m=Ij</ta>
            <ta e="T61" id="Seg_9608" s="T60">biːr</ta>
            <ta e="T62" id="Seg_9609" s="T61">ogo-m</ta>
            <ta e="T63" id="Seg_9610" s="T62">iti</ta>
            <ta e="T64" id="Seg_9611" s="T63">usku͡ola-GA</ta>
            <ta e="T65" id="Seg_9612" s="T64">üleleː-Ar</ta>
            <ta e="T66" id="Seg_9613" s="T65">biːr</ta>
            <ta e="T67" id="Seg_9614" s="T66">kɨːs-I-m</ta>
            <ta e="T68" id="Seg_9615" s="T67">dʼirʼektar-LAː-Ar</ta>
            <ta e="T69" id="Seg_9616" s="T68">biːr</ta>
            <ta e="T70" id="Seg_9617" s="T69">e</ta>
            <ta e="T71" id="Seg_9618" s="T70">biːr</ta>
            <ta e="T72" id="Seg_9619" s="T71">ogo-m</ta>
            <ta e="T73" id="Seg_9620" s="T72">iti</ta>
            <ta e="T74" id="Seg_9621" s="T73">Nosku͡o-GA</ta>
            <ta e="T76" id="Seg_9622" s="T75">muŋ</ta>
            <ta e="T77" id="Seg_9623" s="T76">ulakan</ta>
            <ta e="T78" id="Seg_9624" s="T77">kɨːs-I-m</ta>
            <ta e="T79" id="Seg_9625" s="T78">Nosku͡o-GA</ta>
            <ta e="T80" id="Seg_9626" s="T79">usku͡ola-GA</ta>
            <ta e="T81" id="Seg_9627" s="T80">du͡oktuːr-LAː-Ar</ta>
            <ta e="T82" id="Seg_9628" s="T81">Nʼina</ta>
            <ta e="T84" id="Seg_9629" s="T83">ol</ta>
            <ta e="T85" id="Seg_9630" s="T84">ulakan</ta>
            <ta e="T86" id="Seg_9631" s="T85">kɨːs-I-m</ta>
            <ta e="T87" id="Seg_9632" s="T86">aːt-tA</ta>
            <ta e="T88" id="Seg_9633" s="T87">küččügüj</ta>
            <ta e="T89" id="Seg_9634" s="T88">ol-tI-m</ta>
            <ta e="T90" id="Seg_9635" s="T89">alɨn-I-tA</ta>
            <ta e="T91" id="Seg_9636" s="T90">Kaːtʼa</ta>
            <ta e="T92" id="Seg_9637" s="T91">ol</ta>
            <ta e="T93" id="Seg_9638" s="T92">usku͡ola-GA</ta>
            <ta e="T94" id="Seg_9639" s="T93">dʼirʼektar-LAː-Ar</ta>
            <ta e="T95" id="Seg_9640" s="T94">biːr</ta>
            <ta e="T96" id="Seg_9641" s="T95">kɨːs-I-m</ta>
            <ta e="T97" id="Seg_9642" s="T96">saːdik-GA</ta>
            <ta e="T98" id="Seg_9643" s="T97">üleleː-AːččI</ta>
            <ta e="T99" id="Seg_9644" s="T98">e-TI-tA</ta>
            <ta e="T100" id="Seg_9645" s="T99">ol-tI-m</ta>
            <ta e="T101" id="Seg_9646" s="T100">eː</ta>
            <ta e="T102" id="Seg_9647" s="T101">bu</ta>
            <ta e="T103" id="Seg_9648" s="T102">saːdik-LArA</ta>
            <ta e="T104" id="Seg_9649" s="T103">tu͡ok-LArA</ta>
            <ta e="T105" id="Seg_9650" s="T104">aldʼan-An</ta>
            <ta e="T106" id="Seg_9651" s="T105">anaːn</ta>
            <ta e="T107" id="Seg_9652" s="T106">da</ta>
            <ta e="T108" id="Seg_9653" s="T107">ogo-LAN-An</ta>
            <ta e="T109" id="Seg_9654" s="T108">kanʼaː-An</ta>
            <ta e="T110" id="Seg_9655" s="T109">anɨ</ta>
            <ta e="T111" id="Seg_9656" s="T110">ɨ͡arɨj-An</ta>
            <ta e="T112" id="Seg_9657" s="T111">Dudʼinka</ta>
            <ta e="T113" id="Seg_9658" s="T112">dek</ta>
            <ta e="T114" id="Seg_9659" s="T113">hɨrɨt-Ar</ta>
            <ta e="T115" id="Seg_9660" s="T114">bu</ta>
            <ta e="T116" id="Seg_9661" s="T115">eː</ta>
            <ta e="T117" id="Seg_9662" s="T116">Norilskaj-GA</ta>
            <ta e="T118" id="Seg_9663" s="T117">emteː-A-t-A</ta>
            <ta e="T119" id="Seg_9664" s="T118">hɨt-Ar</ta>
            <ta e="T120" id="Seg_9665" s="T119">anɨ</ta>
            <ta e="T121" id="Seg_9666" s="T120">onton</ta>
            <ta e="T122" id="Seg_9667" s="T121">biːr</ta>
            <ta e="T123" id="Seg_9668" s="T122">u͡ol-I-m</ta>
            <ta e="T124" id="Seg_9669" s="T123">axoːtnʼik</ta>
            <ta e="T125" id="Seg_9670" s="T124">bu͡ol-TI-tA</ta>
            <ta e="T126" id="Seg_9671" s="T125">biːr</ta>
            <ta e="T127" id="Seg_9672" s="T126">u͡ol-I-m</ta>
            <ta e="T128" id="Seg_9673" s="T127">iti</ta>
            <ta e="T129" id="Seg_9674" s="T128">samalʼot-LAr-nI</ta>
            <ta e="T131" id="Seg_9675" s="T130">itte-LArI-n</ta>
            <ta e="T132" id="Seg_9676" s="T131">ɨːt</ta>
            <ta e="T133" id="Seg_9677" s="T132">tu͡ok-LAː-Ar</ta>
            <ta e="T134" id="Seg_9678" s="T133">oŋor-Ar</ta>
            <ta e="T135" id="Seg_9679" s="T134">samalʼot-LAr-nI</ta>
            <ta e="T136" id="Seg_9680" s="T135">iːt-Ar</ta>
            <ta e="T137" id="Seg_9681" s="T136">köt-I-t-Ar</ta>
            <ta e="T138" id="Seg_9682" s="T137">itinne</ta>
            <ta e="T139" id="Seg_9683" s="T138">baːr</ta>
            <ta e="T140" id="Seg_9684" s="T139">biːr</ta>
            <ta e="T141" id="Seg_9685" s="T140">kɨːs-I-m</ta>
            <ta e="T143" id="Seg_9686" s="T142">tu͡ok-nI</ta>
            <ta e="T144" id="Seg_9687" s="T143">büt-BIT</ta>
            <ta e="T145" id="Seg_9688" s="T144">e-TI-tA</ta>
            <ta e="T146" id="Seg_9689" s="T145">saːdik</ta>
            <ta e="T147" id="Seg_9690" s="T146">ü͡örek-tI-n</ta>
            <ta e="T148" id="Seg_9691" s="T147">anɨ</ta>
            <ta e="T149" id="Seg_9692" s="T148">beje-m</ta>
            <ta e="T150" id="Seg_9693" s="T149">kɨrɨj-An-BIn</ta>
            <ta e="T151" id="Seg_9694" s="T150">ol-tI-BI-n</ta>
            <ta e="T152" id="Seg_9695" s="T151">beje-BA-r</ta>
            <ta e="T153" id="Seg_9696" s="T152">üleleː-A-t-AːrI-BIn</ta>
            <ta e="T154" id="Seg_9697" s="T153">tɨ͡a-GA</ta>
            <ta e="T155" id="Seg_9698" s="T154">tut-A-BIn</ta>
            <ta e="T156" id="Seg_9699" s="T155">ogonnʼor-I-m</ta>
            <ta e="T157" id="Seg_9700" s="T156">urut</ta>
            <ta e="T158" id="Seg_9701" s="T157">bulčut</ta>
            <ta e="T159" id="Seg_9702" s="T158">e-TI-tA</ta>
            <ta e="T160" id="Seg_9703" s="T159">hin</ta>
            <ta e="T162" id="Seg_9704" s="T161">kihi-ttAn</ta>
            <ta e="T163" id="Seg_9705" s="T162">eː</ta>
            <ta e="T164" id="Seg_9706" s="T163">Rɨmnaj-GA</ta>
            <ta e="T165" id="Seg_9707" s="T164">onton</ta>
            <ta e="T166" id="Seg_9708" s="T165">hopku͡os-GA</ta>
            <ta e="T167" id="Seg_9709" s="T166">peredavoj-tI-nAn</ta>
            <ta e="T168" id="Seg_9710" s="T167">bultaː-AːččI</ta>
            <ta e="T169" id="Seg_9711" s="T168">e-TI-tA</ta>
            <ta e="T170" id="Seg_9712" s="T169">anɨ</ta>
            <ta e="T171" id="Seg_9713" s="T170">ɨ͡arɨj-An</ta>
            <ta e="T172" id="Seg_9714" s="T171">iliː-tA</ta>
            <ta e="T173" id="Seg_9715" s="T172">ɨ͡arɨj-An</ta>
            <ta e="T174" id="Seg_9716" s="T173">ol</ta>
            <ta e="T175" id="Seg_9717" s="T174">bult-I-ttAn</ta>
            <ta e="T176" id="Seg_9718" s="T175">da</ta>
            <ta e="T177" id="Seg_9719" s="T176">tagɨs-TI-tA</ta>
            <ta e="T178" id="Seg_9720" s="T177">ol-GA</ta>
            <ta e="T179" id="Seg_9721" s="T178">ebiː-LAː-An</ta>
            <ta e="T180" id="Seg_9722" s="T179">anɨ</ta>
            <ta e="T181" id="Seg_9723" s="T180">aːk-t-An</ta>
            <ta e="T182" id="Seg_9724" s="T181">pʼensija-GA</ta>
            <ta e="T183" id="Seg_9725" s="T182">kiːr-TI-tA</ta>
            <ta e="T184" id="Seg_9726" s="T183">bu</ta>
            <ta e="T185" id="Seg_9727" s="T184">anɨ</ta>
            <ta e="T186" id="Seg_9728" s="T185">bu</ta>
            <ta e="T188" id="Seg_9729" s="T186">dʼɨl-LAr-GA</ta>
            <ta e="T190" id="Seg_9730" s="T188">kiːr-BIT-tA</ta>
            <ta e="T203" id="Seg_9731" s="T202">eː</ta>
            <ta e="T204" id="Seg_9732" s="T203">anɨ</ta>
            <ta e="T205" id="Seg_9733" s="T204">bu</ta>
            <ta e="T206" id="Seg_9734" s="T205">ogo-LAr-I-m</ta>
            <ta e="T207" id="Seg_9735" s="T206">ogo-m</ta>
            <ta e="T208" id="Seg_9736" s="T207">ogo-LAr-tI-n</ta>
            <ta e="T209" id="Seg_9737" s="T208">karaj-An-BIn</ta>
            <ta e="T210" id="Seg_9738" s="T209">ol</ta>
            <ta e="T211" id="Seg_9739" s="T210">em-GA</ta>
            <ta e="T212" id="Seg_9740" s="T211">kel-AːččI</ta>
            <ta e="T213" id="Seg_9741" s="T212">ogo-m</ta>
            <ta e="T214" id="Seg_9742" s="T213">gi͡en-tA</ta>
            <ta e="T215" id="Seg_9743" s="T214">üs</ta>
            <ta e="T216" id="Seg_9744" s="T215">ogo-LAːK</ta>
            <ta e="T217" id="Seg_9745" s="T216">bu͡olla</ta>
            <ta e="T218" id="Seg_9746" s="T217">ol-tI-LAr-I-m</ta>
            <ta e="T219" id="Seg_9747" s="T218">ogo-tI-n</ta>
            <ta e="T220" id="Seg_9748" s="T219">karaj-A</ta>
            <ta e="T221" id="Seg_9749" s="T220">hɨt-Ar</ta>
            <ta e="T222" id="Seg_9750" s="T221">e-TI-m</ta>
            <ta e="T223" id="Seg_9751" s="T222">bu͡o</ta>
            <ta e="T224" id="Seg_9752" s="T223">anɨ</ta>
            <ta e="T225" id="Seg_9753" s="T224">bu͡ollagɨna</ta>
            <ta e="T226" id="Seg_9754" s="T225">bu</ta>
            <ta e="T227" id="Seg_9755" s="T226">ogo-m</ta>
            <ta e="T228" id="Seg_9756" s="T227">ol</ta>
            <ta e="T229" id="Seg_9757" s="T228">Maːča-m</ta>
            <ta e="T230" id="Seg_9758" s="T229">kel-An</ta>
            <ta e="T231" id="Seg_9759" s="T230">kel-TI-m</ta>
            <ta e="T232" id="Seg_9760" s="T231">ol</ta>
            <ta e="T233" id="Seg_9761" s="T232">tɨ͡a-ttAn</ta>
            <ta e="T234" id="Seg_9762" s="T233">tɨ͡a-GA</ta>
            <ta e="T235" id="Seg_9763" s="T234">tut-A</ta>
            <ta e="T236" id="Seg_9764" s="T235">hɨt-A-BIn</ta>
            <ta e="T237" id="Seg_9765" s="T236">bu͡olla</ta>
            <ta e="T238" id="Seg_9766" s="T237">ol</ta>
            <ta e="T239" id="Seg_9767" s="T238">üs</ta>
            <ta e="T240" id="Seg_9768" s="T239">ogo-BI-n</ta>
            <ta e="T241" id="Seg_9769" s="T240">ol</ta>
            <ta e="T242" id="Seg_9770" s="T241">maːma-tA</ta>
            <ta e="T243" id="Seg_9771" s="T242">bu</ta>
            <ta e="T244" id="Seg_9772" s="T243">dek</ta>
            <ta e="T245" id="Seg_9773" s="T244">baːr</ta>
            <ta e="T246" id="Seg_9774" s="T245">teːte-LArA</ta>
            <ta e="T247" id="Seg_9775" s="T246">araːk-BIT-tA</ta>
            <ta e="T248" id="Seg_9776" s="T247">bu͡olla</ta>
            <ta e="T249" id="Seg_9777" s="T248">ol</ta>
            <ta e="T250" id="Seg_9778" s="T249">dʼuraːk</ta>
            <ta e="T251" id="Seg_9779" s="T250">e-TI-tA</ta>
            <ta e="T252" id="Seg_9780" s="T251">ol-tI-tA</ta>
            <ta e="T253" id="Seg_9781" s="T252">araːk-BIT-tA</ta>
            <ta e="T254" id="Seg_9782" s="T253">ol</ta>
            <ta e="T255" id="Seg_9783" s="T254">ihin</ta>
            <ta e="T256" id="Seg_9784" s="T255">ol</ta>
            <ta e="T257" id="Seg_9785" s="T256">kantan</ta>
            <ta e="T258" id="Seg_9786" s="T257">biːr-ttAn</ta>
            <ta e="T259" id="Seg_9787" s="T258">biːr</ta>
            <ta e="T260" id="Seg_9788" s="T259">ogo</ta>
            <ta e="T261" id="Seg_9789" s="T260">bu͡olla</ta>
            <ta e="T262" id="Seg_9790" s="T261">ogo-LAr-I-m</ta>
            <ta e="T263" id="Seg_9791" s="T262">ogo-LArA</ta>
            <ta e="T264" id="Seg_9792" s="T263">kaččaga</ta>
            <ta e="T265" id="Seg_9793" s="T264">da</ta>
            <ta e="T266" id="Seg_9794" s="T265">araːk-BAT-LAr</ta>
            <ta e="T267" id="Seg_9795" s="T266">bu͡o</ta>
            <ta e="T268" id="Seg_9796" s="T267">hajɨn</ta>
            <ta e="T269" id="Seg_9797" s="T268">hajɨn-LAr-nI</ta>
            <ta e="T270" id="Seg_9798" s="T269">kɨhɨn-LAr-nI</ta>
            <ta e="T271" id="Seg_9799" s="T270">huptu</ta>
            <ta e="T272" id="Seg_9800" s="T271">ogo-LAːK</ta>
            <ta e="T273" id="Seg_9801" s="T272">bu͡ol-AːččI-BIn</ta>
            <ta e="T274" id="Seg_9802" s="T273">vnuk-LAr-I-m</ta>
            <ta e="T275" id="Seg_9803" s="T274">bi͡ek</ta>
            <ta e="T276" id="Seg_9804" s="T275">baːr</ta>
            <ta e="T277" id="Seg_9805" s="T276">bu͡ol-Ar-LAr</ta>
            <ta e="T300" id="Seg_9806" s="T299">eː</ta>
            <ta e="T301" id="Seg_9807" s="T300">kepseː-AːččI-BIt</ta>
            <ta e="T302" id="Seg_9808" s="T301">itte-kAːN-nI</ta>
            <ta e="T303" id="Seg_9809" s="T302">keː</ta>
            <ta e="T304" id="Seg_9810" s="T303">anɨ</ta>
            <ta e="T305" id="Seg_9811" s="T304">ulakan-LIk</ta>
            <ta e="T306" id="Seg_9812" s="T305">da</ta>
            <ta e="T307" id="Seg_9813" s="T306">tu͡olkulaː-A-n-A</ta>
            <ta e="T308" id="Seg_9814" s="T307">ilik-LAr</ta>
            <ta e="T309" id="Seg_9815" s="T308">eːt</ta>
            <ta e="T310" id="Seg_9816" s="T309">ulakan</ta>
            <ta e="T311" id="Seg_9817" s="T310">kɨːs-I-m</ta>
            <ta e="T312" id="Seg_9818" s="T311">ere</ta>
            <ta e="T313" id="Seg_9819" s="T312">ogo-LAr-tA</ta>
            <ta e="T314" id="Seg_9820" s="T313">usku͡ola-GA</ta>
            <ta e="T315" id="Seg_9821" s="T314">bar-BIT-LArA</ta>
            <ta e="T316" id="Seg_9822" s="T315">küččügüj</ta>
            <ta e="T317" id="Seg_9823" s="T316">ogo-LAr-I-m</ta>
            <ta e="T318" id="Seg_9824" s="T317">ogo-LArA</ta>
            <ta e="T319" id="Seg_9825" s="T318">kiːr-A</ta>
            <ta e="T320" id="Seg_9826" s="T319">ilik-LAr</ta>
            <ta e="T321" id="Seg_9827" s="T320">usku͡ola-GA</ta>
            <ta e="T322" id="Seg_9828" s="T321">ol-LAr-GA</ta>
            <ta e="T323" id="Seg_9829" s="T322">ol</ta>
            <ta e="T324" id="Seg_9830" s="T323">itte-kAːN-nI</ta>
            <ta e="T325" id="Seg_9831" s="T324">kepseː-A-BIn</ta>
            <ta e="T326" id="Seg_9832" s="T325">bu͡o</ta>
            <ta e="T327" id="Seg_9833" s="T326">itte-nI-bütte-nI</ta>
            <ta e="T328" id="Seg_9834" s="T327">ebe</ta>
            <ta e="T329" id="Seg_9835" s="T328">di͡e-An</ta>
            <ta e="T330" id="Seg_9836" s="T329">ergičij-A</ta>
            <ta e="T331" id="Seg_9837" s="T330">hɨrɨt-AːččI-LAr</ta>
            <ta e="T332" id="Seg_9838" s="T331">bu͡o</ta>
            <ta e="T333" id="Seg_9839" s="T332">ol-tI-GI-n</ta>
            <ta e="T334" id="Seg_9840" s="T333">kepseː</ta>
            <ta e="T335" id="Seg_9841" s="T334">ol-nI</ta>
            <ta e="T336" id="Seg_9842" s="T335">oloŋko-LAː</ta>
            <ta e="T337" id="Seg_9843" s="T336">bu-nI</ta>
            <ta e="T338" id="Seg_9844" s="T337">kepseː</ta>
            <ta e="T339" id="Seg_9845" s="T338">iti-nI</ta>
            <ta e="T340" id="Seg_9846" s="T339">ɨllaː</ta>
            <ta e="T341" id="Seg_9847" s="T340">ol-LAr-nI</ta>
            <ta e="T342" id="Seg_9848" s="T341">haːtat-AːrI-GIn</ta>
            <ta e="T343" id="Seg_9849" s="T342">ka</ta>
            <ta e="T344" id="Seg_9850" s="T343">ɨllaː-A-n-I-ŋ</ta>
            <ta e="T345" id="Seg_9851" s="T344">kepseː-A-t-I-ŋ</ta>
            <ta e="T346" id="Seg_9852" s="T345">hin</ta>
            <ta e="T347" id="Seg_9853" s="T346">kojuː</ta>
            <ta e="T348" id="Seg_9854" s="T347">bu͡olla</ta>
            <ta e="T356" id="Seg_9855" s="T355">kaja</ta>
            <ta e="T357" id="Seg_9856" s="T356">mini͡ettere</ta>
            <ta e="T358" id="Seg_9857" s="T357">pirʼedka-LAr-I-m</ta>
            <ta e="T359" id="Seg_9858" s="T358">barɨ-tA</ta>
            <ta e="T360" id="Seg_9859" s="T359">oloŋko-ČIt</ta>
            <ta e="T361" id="Seg_9860" s="T360">e-TI-LAr</ta>
            <ta e="T362" id="Seg_9861" s="T361">ɨllaː-AːččI</ta>
            <ta e="T363" id="Seg_9862" s="T362">da</ta>
            <ta e="T364" id="Seg_9863" s="T363">e-TI-LAr</ta>
            <ta e="T365" id="Seg_9864" s="T364">ɨrɨ͡a</ta>
            <ta e="T366" id="Seg_9865" s="T365">ɨllaː-A</ta>
            <ta e="T367" id="Seg_9866" s="T366">ɨllaː-A-n-A</ta>
            <ta e="T368" id="Seg_9867" s="T367">ɨrɨ͡a-LAːK</ta>
            <ta e="T369" id="Seg_9868" s="T368">da</ta>
            <ta e="T370" id="Seg_9869" s="T369">oloŋko-nI</ta>
            <ta e="T371" id="Seg_9870" s="T370">kepseː-AːččI</ta>
            <ta e="T372" id="Seg_9871" s="T371">meːne</ta>
            <ta e="T373" id="Seg_9872" s="T372">da</ta>
            <ta e="T374" id="Seg_9873" s="T373">oloŋko-nI</ta>
            <ta e="T375" id="Seg_9874" s="T374">kepseː-A</ta>
            <ta e="T376" id="Seg_9875" s="T375">oččogo</ta>
            <ta e="T377" id="Seg_9876" s="T376">bɨlɨr-GI</ta>
            <ta e="T378" id="Seg_9877" s="T377">üje-GA</ta>
            <ta e="T379" id="Seg_9878" s="T378">bu͡ollagɨna</ta>
            <ta e="T380" id="Seg_9879" s="T379">kinoː-nI</ta>
            <ta e="T381" id="Seg_9880" s="T380">kör-A-GIn</ta>
            <ta e="T382" id="Seg_9881" s="T381">du͡o</ta>
            <ta e="T383" id="Seg_9882" s="T382">oččogo</ta>
            <ta e="T384" id="Seg_9883" s="T383">daːganɨ</ta>
            <ta e="T385" id="Seg_9884" s="T384">Spʼidola-nI</ta>
            <ta e="T386" id="Seg_9885" s="T385">ihilin-A-GIn</ta>
            <ta e="T387" id="Seg_9886" s="T386">du͡o</ta>
            <ta e="T388" id="Seg_9887" s="T387">mas</ta>
            <ta e="T389" id="Seg_9888" s="T388">oːnnʼuːr-I-nAn</ta>
            <ta e="T390" id="Seg_9889" s="T389">oːnnʼoː-A-GIn</ta>
            <ta e="T391" id="Seg_9890" s="T390">čɨːčaːk-LAr-I-nAn</ta>
            <ta e="T392" id="Seg_9891" s="T391">iti</ta>
            <ta e="T393" id="Seg_9892" s="T392">hɨrga</ta>
            <ta e="T394" id="Seg_9893" s="T393">oŋohun-A</ta>
            <ta e="T395" id="Seg_9894" s="T394">oŋohun-A-GIn</ta>
            <ta e="T396" id="Seg_9895" s="T395">tahaːra</ta>
            <ta e="T397" id="Seg_9896" s="T396">tagɨs-TAK-GInA</ta>
            <ta e="T398" id="Seg_9897" s="T397">mu͡os</ta>
            <ta e="T399" id="Seg_9898" s="T398">tamnaː-An</ta>
            <ta e="T400" id="Seg_9899" s="T399">mu͡os-I-nAn</ta>
            <ta e="T401" id="Seg_9900" s="T400">oːnnʼoː-IAK-ŋ</ta>
            <ta e="T402" id="Seg_9901" s="T401">u͡ol</ta>
            <ta e="T403" id="Seg_9902" s="T402">ogo</ta>
            <ta e="T404" id="Seg_9903" s="T403">bu͡olla</ta>
            <ta e="T405" id="Seg_9904" s="T404">maːbɨt-LAN-A</ta>
            <ta e="T406" id="Seg_9905" s="T405">oːnnʼoː-IAK-tA</ta>
            <ta e="T407" id="Seg_9906" s="T406">hajɨn</ta>
            <ta e="T408" id="Seg_9907" s="T407">kajdak</ta>
            <ta e="T409" id="Seg_9908" s="T408">hɨrɨt-Ar-GI-n</ta>
            <ta e="T410" id="Seg_9909" s="T409">taːs-nI</ta>
            <ta e="T411" id="Seg_9910" s="T410">mus-I-n-An-GIn</ta>
            <ta e="T412" id="Seg_9911" s="T411">ölgöbüːn</ta>
            <ta e="T413" id="Seg_9912" s="T412">oːnnʼoː-IAK-ŋ</ta>
            <ta e="T414" id="Seg_9913" s="T413">hol</ta>
            <ta e="T415" id="Seg_9914" s="T414">kördük-keːčeːn</ta>
            <ta e="T416" id="Seg_9915" s="T415">kün-nI</ta>
            <ta e="T417" id="Seg_9916" s="T416">bɨs-A-GIn</ta>
            <ta e="T418" id="Seg_9917" s="T417">bu͡olla</ta>
            <ta e="T419" id="Seg_9918" s="T418">bɨlɨr-GI</ta>
            <ta e="T420" id="Seg_9919" s="T419">ogo</ta>
            <ta e="T421" id="Seg_9920" s="T420">bɨlɨrgɨ-LIː</ta>
            <ta e="T422" id="Seg_9921" s="T421">oːnnʼoː-Ar</ta>
            <ta e="T423" id="Seg_9922" s="T422">bu͡o</ta>
            <ta e="T424" id="Seg_9923" s="T423">ol</ta>
            <ta e="T425" id="Seg_9924" s="T424">bɨs-Iː-tI-nAn</ta>
            <ta e="T426" id="Seg_9925" s="T425">anɨ</ta>
            <ta e="T427" id="Seg_9926" s="T426">bɨlɨr-GI</ta>
            <ta e="T428" id="Seg_9927" s="T427">oːnnʼuːr-nI</ta>
            <ta e="T429" id="Seg_9928" s="T428">da</ta>
            <ta e="T430" id="Seg_9929" s="T429">anɨ</ta>
            <ta e="T431" id="Seg_9930" s="T430">da</ta>
            <ta e="T432" id="Seg_9931" s="T431">kör-IAK-GItI-n</ta>
            <ta e="T433" id="Seg_9932" s="T432">da</ta>
            <ta e="T434" id="Seg_9933" s="T433">čaŋkarkaːtaːk</ta>
            <ta e="T435" id="Seg_9934" s="T434">oːnnʼuːr-nI</ta>
            <ta e="T436" id="Seg_9935" s="T435">egel-BIT-I-m</ta>
            <ta e="T437" id="Seg_9936" s="T436">bu͡olluŋ</ta>
            <ta e="T438" id="Seg_9937" s="T437">bu</ta>
            <ta e="T439" id="Seg_9938" s="T438">dojdu-GA</ta>
            <ta e="T440" id="Seg_9939" s="T439">hin</ta>
            <ta e="T441" id="Seg_9940" s="T440">köllör-IAK-m</ta>
            <ta e="T442" id="Seg_9941" s="T441">ol</ta>
            <ta e="T443" id="Seg_9942" s="T442">oːnnʼoː-Iː</ta>
            <ta e="T444" id="Seg_9943" s="T443">kajdak</ta>
            <ta e="T445" id="Seg_9944" s="T444">oːnnʼoː-Ar-LArI-n</ta>
            <ta e="T446" id="Seg_9945" s="T445">aŋar</ta>
            <ta e="T447" id="Seg_9946" s="T446">oːnnʼuːr-LAr-nI</ta>
            <ta e="T448" id="Seg_9947" s="T447">oŋor</ta>
            <ta e="T449" id="Seg_9948" s="T448">oŋor-An</ta>
            <ta e="T450" id="Seg_9949" s="T449">egel-IAK-BI-n</ta>
            <ta e="T451" id="Seg_9950" s="T450">kihi-tA</ta>
            <ta e="T452" id="Seg_9951" s="T451">hu͡ok-BIn</ta>
            <ta e="T453" id="Seg_9952" s="T452">ogonnʼor-I-m</ta>
            <ta e="T454" id="Seg_9953" s="T453">ol</ta>
            <ta e="T455" id="Seg_9954" s="T454">bolʼnica-GA</ta>
            <ta e="T456" id="Seg_9955" s="T455">hɨt-Ar</ta>
            <ta e="T457" id="Seg_9956" s="T456">e-TI-tA</ta>
            <ta e="T458" id="Seg_9957" s="T457">bu͡olla</ta>
            <ta e="T459" id="Seg_9958" s="T458">biːr</ta>
            <ta e="T460" id="Seg_9959" s="T459">u͡ol-I-m</ta>
            <ta e="T461" id="Seg_9960" s="T460">hin</ta>
            <ta e="T462" id="Seg_9961" s="T461">ɨ͡arɨj-An</ta>
            <ta e="T463" id="Seg_9962" s="T462">bolʼnica-ttAn</ta>
            <ta e="T464" id="Seg_9963" s="T463">tagɨs-AːT</ta>
            <ta e="T465" id="Seg_9964" s="T464">ka</ta>
            <ta e="T466" id="Seg_9965" s="T465">kel-BIT-I-m</ta>
            <ta e="T467" id="Seg_9966" s="T466">bu͡olla</ta>
            <ta e="T468" id="Seg_9967" s="T467">ol-tI-m</ta>
            <ta e="T469" id="Seg_9968" s="T468">min</ta>
            <ta e="T470" id="Seg_9969" s="T469">tɨ͡a-GA</ta>
            <ta e="T471" id="Seg_9970" s="T470">baːr</ta>
            <ta e="T472" id="Seg_9971" s="T471">e-TI-tA</ta>
            <ta e="T473" id="Seg_9972" s="T472">ol-tI-m</ta>
            <ta e="T474" id="Seg_9973" s="T473">ebe-GA</ta>
            <ta e="T475" id="Seg_9974" s="T474">kiːr-An</ta>
            <ta e="T476" id="Seg_9975" s="T475">ɨ͡arɨj-I-BIT-tA</ta>
            <ta e="T477" id="Seg_9976" s="T476">onton</ta>
            <ta e="T478" id="Seg_9977" s="T477">kojut</ta>
            <ta e="T479" id="Seg_9978" s="T478">tagɨs-I-BIT-tA</ta>
            <ta e="T480" id="Seg_9979" s="T479">kel-Ar-BIt</ta>
            <ta e="T481" id="Seg_9980" s="T480">da</ta>
            <ta e="T482" id="Seg_9981" s="T481">ol-nI</ta>
            <ta e="T483" id="Seg_9982" s="T482">purgaː-LAr</ta>
            <ta e="T484" id="Seg_9983" s="T483">tu͡ok-LAr</ta>
            <ta e="T485" id="Seg_9984" s="T484">bu͡o</ta>
            <ta e="T486" id="Seg_9985" s="T485">meheːj-LAː-An</ta>
            <ta e="T487" id="Seg_9986" s="T486">h-onon</ta>
            <ta e="T488" id="Seg_9987" s="T487">tu͡ok</ta>
            <ta e="T489" id="Seg_9988" s="T488">elbek</ta>
            <ta e="T490" id="Seg_9989" s="T489">oːnnʼuːr</ta>
            <ta e="T491" id="Seg_9990" s="T490">egel-BAtAK-I-m</ta>
            <ta e="T492" id="Seg_9991" s="T491">urukku</ta>
            <ta e="T493" id="Seg_9992" s="T492">ogo</ta>
            <ta e="T494" id="Seg_9993" s="T493">oːnnʼuːr-tI-n</ta>
            <ta e="T495" id="Seg_9994" s="T494">hin</ta>
            <ta e="T496" id="Seg_9995" s="T495">onton</ta>
            <ta e="T497" id="Seg_9996" s="T496">atɨn</ta>
            <ta e="T498" id="Seg_9997" s="T497">köllör-Iː-GA</ta>
            <ta e="T499" id="Seg_9998" s="T498">egel-IAK</ta>
            <ta e="T500" id="Seg_9999" s="T499">e-TI-m</ta>
            <ta e="T501" id="Seg_10000" s="T500">urut</ta>
            <ta e="T502" id="Seg_10001" s="T501">beje-m</ta>
            <ta e="T503" id="Seg_10002" s="T502">oːnnʼoː-An</ta>
            <ta e="T504" id="Seg_10003" s="T503">ü͡öskeː-BIT-BIn</ta>
            <ta e="T505" id="Seg_10004" s="T504">ka</ta>
            <ta e="T506" id="Seg_10005" s="T505">onton</ta>
            <ta e="T518" id="Seg_10006" s="T517">dʼe</ta>
            <ta e="T519" id="Seg_10007" s="T518">mu͡ot-I-nAn</ta>
            <ta e="T520" id="Seg_10008" s="T519">höröː-A-BIt</ta>
            <ta e="T521" id="Seg_10009" s="T520">üːt-LAːK</ta>
            <ta e="T522" id="Seg_10010" s="T521">mas-GA</ta>
            <ta e="T523" id="Seg_10011" s="T522">bas-I-nAn</ta>
            <ta e="T524" id="Seg_10012" s="T523">taŋnarɨ</ta>
            <ta e="T525" id="Seg_10013" s="T524">tur-An</ta>
            <ta e="T526" id="Seg_10014" s="T525">ergij-Ar</ta>
            <ta e="T527" id="Seg_10015" s="T526">ol-nI</ta>
            <ta e="T528" id="Seg_10016" s="T527">kör-TAK-GInA</ta>
            <ta e="T529" id="Seg_10017" s="T528">ere</ta>
            <ta e="T530" id="Seg_10018" s="T529">bil-IAK-ŋ</ta>
            <ta e="T531" id="Seg_10019" s="T530">bu͡olla</ta>
            <ta e="T532" id="Seg_10020" s="T531">kajdak</ta>
            <ta e="T533" id="Seg_10021" s="T532">anɨ</ta>
            <ta e="T534" id="Seg_10022" s="T533">kepseː-An-BIn</ta>
            <ta e="T535" id="Seg_10023" s="T534">kajdak</ta>
            <ta e="T536" id="Seg_10024" s="T535">kepseː-IAK-m=Ij</ta>
            <ta e="T537" id="Seg_10025" s="T536">köllör-IAK-m</ta>
            <ta e="T538" id="Seg_10026" s="T537">ol</ta>
            <ta e="T539" id="Seg_10027" s="T538">kör-TAK-GInA</ta>
            <ta e="T540" id="Seg_10028" s="T539">bil-Aːr</ta>
            <ta e="T559" id="Seg_10029" s="T558">eː</ta>
            <ta e="T560" id="Seg_10030" s="T559">bil-A-BIn</ta>
            <ta e="T561" id="Seg_10031" s="T560">onnuk-LAr-nI</ta>
            <ta e="T562" id="Seg_10032" s="T561">onnuk</ta>
            <ta e="T563" id="Seg_10033" s="T562">onnuk-TA</ta>
            <ta e="T564" id="Seg_10034" s="T563">meːne</ta>
            <ta e="T565" id="Seg_10035" s="T564">lʼuboj</ta>
            <ta e="T566" id="Seg_10036" s="T565">ɨrɨ͡a-TA</ta>
            <ta e="T567" id="Seg_10037" s="T566">ɨllaː-IAK-m</ta>
            <ta e="T568" id="Seg_10038" s="T567">du͡o</ta>
            <ta e="T569" id="Seg_10039" s="T568">lʼuboj-TA</ta>
            <ta e="T570" id="Seg_10040" s="T569">lʼuboj</ta>
            <ta e="T571" id="Seg_10041" s="T570">ɨrɨ͡a-TA</ta>
            <ta e="T572" id="Seg_10042" s="T571">ɨllaː-IAK-m</ta>
            <ta e="T573" id="Seg_10043" s="T572">beːbe</ta>
            <ta e="T574" id="Seg_10044" s="T573">kel-A</ta>
            <ta e="T575" id="Seg_10045" s="T574">tart-BAT</ta>
            <ta e="T576" id="Seg_10046" s="T575">iti-tI-ŋ</ta>
            <ta e="T577" id="Seg_10047" s="T576">kel-IAK</ta>
            <ta e="T578" id="Seg_10048" s="T577">kel-IAK-TA</ta>
            <ta e="T579" id="Seg_10049" s="T578">hu͡ok</ta>
            <ta e="T580" id="Seg_10050" s="T579">bu͡ol-AːččI</ta>
            <ta e="T581" id="Seg_10051" s="T580">kü͡ömej-LArI-n</ta>
            <ta e="T582" id="Seg_10052" s="T581">öjdöː-BAT-BIn</ta>
            <ta e="T583" id="Seg_10053" s="T582">umun-I-BIT-BIn</ta>
            <ta e="T584" id="Seg_10054" s="T583">urut</ta>
            <ta e="T585" id="Seg_10055" s="T584">bu͡ollagɨna</ta>
            <ta e="T586" id="Seg_10056" s="T585">gini-nAn</ta>
            <ta e="T587" id="Seg_10057" s="T586">zanʼimatsa-LAː-IAK-m</ta>
            <ta e="T588" id="Seg_10058" s="T587">di͡e-An-BIn</ta>
            <ta e="T589" id="Seg_10059" s="T588">olus</ta>
            <ta e="T590" id="Seg_10060" s="T589">dʼelaj-LAː-BAkkA</ta>
            <ta e="T591" id="Seg_10061" s="T590">hɨrɨt-I-BIT-I-m</ta>
            <ta e="T592" id="Seg_10062" s="T591">bu͡olla</ta>
            <ta e="T593" id="Seg_10063" s="T592">ol</ta>
            <ta e="T594" id="Seg_10064" s="T593">ihin</ta>
            <ta e="T595" id="Seg_10065" s="T594">umun-A-BIn</ta>
            <ta e="T611" id="Seg_10066" s="T610">iti</ta>
            <ta e="T612" id="Seg_10067" s="T611">öl-BIT-tA</ta>
            <ta e="T613" id="Seg_10068" s="T612">iti</ta>
            <ta e="T614" id="Seg_10069" s="T613">Baja</ta>
            <ta e="T615" id="Seg_10070" s="T614">di͡e-An</ta>
            <ta e="T616" id="Seg_10071" s="T615">kɨːs</ta>
            <ta e="T617" id="Seg_10072" s="T616">ɨllaː-BIT-tA</ta>
            <ta e="T618" id="Seg_10073" s="T617">ɨllaː-BIT</ta>
            <ta e="T619" id="Seg_10074" s="T618">u͡ol-tA</ta>
            <ta e="T620" id="Seg_10075" s="T619">hin</ta>
            <ta e="T621" id="Seg_10076" s="T620">öl-BIT-tA</ta>
            <ta e="T642" id="Seg_10077" s="T641">eː</ta>
            <ta e="T643" id="Seg_10078" s="T642">onnuk-LArI-nAn</ta>
            <ta e="T644" id="Seg_10079" s="T643">Toːnʼa-nI</ta>
            <ta e="T645" id="Seg_10080" s="T644">kɨtta</ta>
            <ta e="T646" id="Seg_10081" s="T645">hin</ta>
            <ta e="T647" id="Seg_10082" s="T646">eː</ta>
            <ta e="T648" id="Seg_10083" s="T647">tu͡ok-LAː-IAK-BIt</ta>
            <ta e="T649" id="Seg_10084" s="T648">hɨrɨs-Ar-nI</ta>
            <ta e="T650" id="Seg_10085" s="T649">hin</ta>
            <ta e="T651" id="Seg_10086" s="T650">ɨllaː-IAK-BIt</ta>
            <ta e="T652" id="Seg_10087" s="T651">bu͡olla</ta>
            <ta e="T653" id="Seg_10088" s="T652">meːne</ta>
            <ta e="T654" id="Seg_10089" s="T653">tɨːn-A</ta>
            <ta e="T655" id="Seg_10090" s="T654">tɨːn-A</ta>
            <ta e="T656" id="Seg_10091" s="T655">eː</ta>
            <ta e="T657" id="Seg_10092" s="T656">pʼerʼexvat dɨxaːnʼija-nI</ta>
            <ta e="T658" id="Seg_10093" s="T657">tɨːn-A</ta>
            <ta e="T659" id="Seg_10094" s="T658">tɨːn-A-GIn</ta>
            <ta e="T660" id="Seg_10095" s="T659">bu͡olla</ta>
            <ta e="T663" id="Seg_10096" s="T662">ɨllaː</ta>
            <ta e="T664" id="Seg_10097" s="T663">di͡e-TAK-GInA</ta>
            <ta e="T665" id="Seg_10098" s="T664">ɨllaː-IAK-m</ta>
            <ta e="T666" id="Seg_10099" s="T665">meːne</ta>
            <ta e="T667" id="Seg_10100" s="T666">tɨːn</ta>
            <ta e="T668" id="Seg_10101" s="T667">bɨldʼaː-A-s-Ar</ta>
            <ta e="T669" id="Seg_10102" s="T668">kohoːn</ta>
            <ta e="T685" id="Seg_10103" s="T684">iti-nI</ta>
            <ta e="T686" id="Seg_10104" s="T685">bu͡olla</ta>
            <ta e="T687" id="Seg_10105" s="T686">čogotok</ta>
            <ta e="T688" id="Seg_10106" s="T687">tɨːn-nAn</ta>
            <ta e="T689" id="Seg_10107" s="T688">ɨllaː-AːččI-LAr</ta>
            <ta e="T690" id="Seg_10108" s="T689">ühü</ta>
            <ta e="T691" id="Seg_10109" s="T690">bɨlɨrgɨ-LAr</ta>
            <ta e="T692" id="Seg_10110" s="T691">čogotok</ta>
            <ta e="T693" id="Seg_10111" s="T692">tɨːn-nAn</ta>
            <ta e="T694" id="Seg_10112" s="T693">tɨːn</ta>
            <ta e="T695" id="Seg_10113" s="T694">bɨldʼaː-A-s-An-LAr</ta>
            <ta e="T696" id="Seg_10114" s="T695">dʼe</ta>
            <ta e="T697" id="Seg_10115" s="T696">giniler</ta>
            <ta e="T698" id="Seg_10116" s="T697">oːnnʼuːr-LArA</ta>
            <ta e="T699" id="Seg_10117" s="T698">ol</ta>
            <ta e="T700" id="Seg_10118" s="T699">bu͡ol-TI-tA</ta>
            <ta e="T701" id="Seg_10119" s="T700">bu</ta>
            <ta e="T702" id="Seg_10120" s="T701">artʼist-LAr</ta>
            <ta e="T703" id="Seg_10121" s="T702">anɨ</ta>
            <ta e="T704" id="Seg_10122" s="T703">oːnnʼoː-Ar-LArI-n</ta>
            <ta e="T705" id="Seg_10123" s="T704">kördük</ta>
            <ta e="T706" id="Seg_10124" s="T705">hiti</ta>
            <ta e="T707" id="Seg_10125" s="T706">čogotok</ta>
            <ta e="T708" id="Seg_10126" s="T707">tɨːn-nAn</ta>
            <ta e="T709" id="Seg_10127" s="T708">tij-AːččI</ta>
            <ta e="T710" id="Seg_10128" s="T709">e-TI-tA</ta>
            <ta e="T711" id="Seg_10129" s="T710">ühü</ta>
            <ta e="T712" id="Seg_10130" s="T711">Inokʼentʼij</ta>
            <ta e="T713" id="Seg_10131" s="T712">di͡e-An</ta>
            <ta e="T714" id="Seg_10132" s="T713">kihi</ta>
            <ta e="T715" id="Seg_10133" s="T714">iti</ta>
            <ta e="T716" id="Seg_10134" s="T715">iti-GA</ta>
            <ta e="T728" id="Seg_10135" s="T727">iti</ta>
            <ta e="T729" id="Seg_10136" s="T728">mini͡ene</ta>
            <ta e="T730" id="Seg_10137" s="T729">kɨːs-I-m</ta>
            <ta e="T731" id="Seg_10138" s="T730">iti</ta>
            <ta e="T761" id="Seg_10139" s="T760">eː</ta>
            <ta e="T762" id="Seg_10140" s="T761">dʼe</ta>
            <ta e="T763" id="Seg_10141" s="T762">ol</ta>
            <ta e="T764" id="Seg_10142" s="T763">min</ta>
            <ta e="T765" id="Seg_10143" s="T764">kepseː-BIT</ta>
            <ta e="T766" id="Seg_10144" s="T765">oloŋko-BI-n</ta>
            <ta e="T767" id="Seg_10145" s="T766">koptoːk</ta>
            <ta e="T768" id="Seg_10146" s="T767">Taiska-nI</ta>
            <ta e="T769" id="Seg_10147" s="T768">kepseː-BIT-LAr</ta>
            <ta e="T770" id="Seg_10148" s="T769">min-n</ta>
            <ta e="T771" id="Seg_10149" s="T770">kepseː-A-t-An</ta>
            <ta e="T772" id="Seg_10150" s="T771">baran</ta>
            <ta e="T773" id="Seg_10151" s="T772">kel-BIT-LArA</ta>
            <ta e="T774" id="Seg_10152" s="T773">manna</ta>
            <ta e="T775" id="Seg_10153" s="T774">kel-AːrI-LAr</ta>
            <ta e="T776" id="Seg_10154" s="T775">kutta</ta>
            <ta e="T777" id="Seg_10155" s="T776">dʼɨl-GA</ta>
            <ta e="T778" id="Seg_10156" s="T777">kel-BIT-LArA</ta>
            <ta e="T779" id="Seg_10157" s="T778">oːnnʼoː-AːrI-LAr</ta>
            <ta e="T780" id="Seg_10158" s="T779">ol-tI-LArI-n</ta>
            <ta e="T781" id="Seg_10159" s="T780">daːganɨ</ta>
            <ta e="T782" id="Seg_10160" s="T781">iti</ta>
            <ta e="T783" id="Seg_10161" s="T782">kajdak</ta>
            <ta e="T784" id="Seg_10162" s="T783">kajdak</ta>
            <ta e="T785" id="Seg_10163" s="T784">kepseː-An</ta>
            <ta e="T786" id="Seg_10164" s="T785">keːs-BIT</ta>
            <ta e="T787" id="Seg_10165" s="T786">e-TI-LArA</ta>
            <ta e="T788" id="Seg_10166" s="T787">onton</ta>
            <ta e="T789" id="Seg_10167" s="T788">obu͡oj-tA</ta>
            <ta e="T793" id="Seg_10168" s="T792">eː</ta>
            <ta e="T794" id="Seg_10169" s="T793">ihit-I-BIT-I-m</ta>
            <ta e="T795" id="Seg_10170" s="T794">ol-nI</ta>
            <ta e="T796" id="Seg_10171" s="T795">ihit-An-BIn</ta>
            <ta e="T797" id="Seg_10172" s="T796">gɨn-A-BIn</ta>
            <ta e="T798" id="Seg_10173" s="T797">iti</ta>
            <ta e="T800" id="Seg_10174" s="T799">ol-LAr-nI</ta>
            <ta e="T802" id="Seg_10175" s="T801">ol-LAr-nI</ta>
            <ta e="T803" id="Seg_10176" s="T802">kel-Ar-LAr</ta>
            <ta e="T804" id="Seg_10177" s="T803">ol-LAr-nI</ta>
            <ta e="T805" id="Seg_10178" s="T804">hemeleː-A-BIn</ta>
            <ta e="T806" id="Seg_10179" s="T805">togo</ta>
            <ta e="T807" id="Seg_10180" s="T806">ka</ta>
            <ta e="T808" id="Seg_10181" s="T807">iti</ta>
            <ta e="T809" id="Seg_10182" s="T808">ol-nAn-bu-nAn</ta>
            <ta e="T810" id="Seg_10183" s="T809">kepseː-A-GIt</ta>
            <ta e="T811" id="Seg_10184" s="T810">ile-LIː</ta>
            <ta e="T812" id="Seg_10185" s="T811">kepseː-BAT-GIt</ta>
            <ta e="T813" id="Seg_10186" s="T812">oloŋko-GItI-n</ta>
            <ta e="T820" id="Seg_10187" s="T819">vsʼo ravno</ta>
            <ta e="T821" id="Seg_10188" s="T820">hin</ta>
            <ta e="T822" id="Seg_10189" s="T821">dʼüːlleː-A-n-Ar</ta>
            <ta e="T823" id="Seg_10190" s="T822">onton</ta>
            <ta e="T824" id="Seg_10191" s="T823">asnavnoj-nI</ta>
            <ta e="T825" id="Seg_10192" s="T824">hin</ta>
            <ta e="T826" id="Seg_10193" s="T825">kepseː-Ar-LAr</ta>
            <ta e="T849" id="Seg_10194" s="T848">eː</ta>
            <ta e="T850" id="Seg_10195" s="T849">baːr</ta>
            <ta e="T851" id="Seg_10196" s="T850">eːt</ta>
            <ta e="T852" id="Seg_10197" s="T851">ol</ta>
            <ta e="T853" id="Seg_10198" s="T852">oloŋko-nI</ta>
            <ta e="T854" id="Seg_10199" s="T853">bil-Ar</ta>
            <ta e="T855" id="Seg_10200" s="T854">min</ta>
            <ta e="T856" id="Seg_10201" s="T855">ubaj-I-m</ta>
            <ta e="T857" id="Seg_10202" s="T856">baːr</ta>
            <ta e="T858" id="Seg_10203" s="T857">ubaj-LAr-I-m</ta>
            <ta e="T859" id="Seg_10204" s="T858">onton</ta>
            <ta e="T860" id="Seg_10205" s="T859">ikki-IAn</ta>
            <ta e="T861" id="Seg_10206" s="T860">da</ta>
            <ta e="T862" id="Seg_10207" s="T861">bil-Ar-LAr</ta>
            <ta e="T863" id="Seg_10208" s="T862">ikki</ta>
            <ta e="T864" id="Seg_10209" s="T863">ubaj-LAːK-BIn</ta>
            <ta e="T865" id="Seg_10210" s="T864">ikki-IAn</ta>
            <ta e="T866" id="Seg_10211" s="T865">da</ta>
            <ta e="T867" id="Seg_10212" s="T866">bil-A</ta>
            <ta e="T868" id="Seg_10213" s="T867">biːrges-tA</ta>
            <ta e="T869" id="Seg_10214" s="T868">kɨrdʼagas</ta>
            <ta e="T870" id="Seg_10215" s="T869">biːrges-tA</ta>
            <ta e="T871" id="Seg_10216" s="T870">min-TAːgAr</ta>
            <ta e="T872" id="Seg_10217" s="T871">hin</ta>
            <ta e="T873" id="Seg_10218" s="T872">ubaj</ta>
            <ta e="T874" id="Seg_10219" s="T873">bil-Ar-LAr</ta>
            <ta e="T875" id="Seg_10220" s="T874">ol-LAr</ta>
            <ta e="T876" id="Seg_10221" s="T875">oloŋko-LAr-nI</ta>
            <ta e="T877" id="Seg_10222" s="T876">ɨrɨ͡a-LAːK-LAr-nI</ta>
            <ta e="T878" id="Seg_10223" s="T877">da</ta>
            <ta e="T879" id="Seg_10224" s="T878">bil-Ar-LAr</ta>
            <ta e="T880" id="Seg_10225" s="T879">ɨrɨ͡a-tA</ta>
            <ta e="T881" id="Seg_10226" s="T880">da</ta>
            <ta e="T882" id="Seg_10227" s="T881">hu͡ok-nI</ta>
            <ta e="T883" id="Seg_10228" s="T882">bil-A</ta>
            <ta e="T888" id="Seg_10229" s="T887">meːne</ta>
            <ta e="T889" id="Seg_10230" s="T888">kihi-LAr</ta>
            <ta e="T890" id="Seg_10231" s="T889">iti</ta>
            <ta e="T891" id="Seg_10232" s="T890">urut</ta>
            <ta e="T892" id="Seg_10233" s="T891">oloŋko-ČIt</ta>
            <ta e="T893" id="Seg_10234" s="T892">kihi-LAr-BIt</ta>
            <ta e="T894" id="Seg_10235" s="T893">baran-TI-LAr</ta>
            <ta e="T895" id="Seg_10236" s="T894">bu͡olla</ta>
            <ta e="T896" id="Seg_10237" s="T895">anɨ</ta>
            <ta e="T897" id="Seg_10238" s="T896">ile</ta>
            <ta e="T898" id="Seg_10239" s="T897">ile</ta>
            <ta e="T899" id="Seg_10240" s="T898">min</ta>
            <ta e="T900" id="Seg_10241" s="T899">bil-Ar</ta>
            <ta e="T901" id="Seg_10242" s="T900">oloŋko-ČIt</ta>
            <ta e="T902" id="Seg_10243" s="T901">kihi-LAr-I-m</ta>
            <ta e="T903" id="Seg_10244" s="T902">baran-TI-LAr</ta>
            <ta e="T904" id="Seg_10245" s="T903">kim</ta>
            <ta e="T905" id="Seg_10246" s="T904">baːr-tA</ta>
            <ta e="T906" id="Seg_10247" s="T905">bu͡ol-IAK.[tA]=Ij</ta>
            <ta e="T907" id="Seg_10248" s="T906">anɨ</ta>
            <ta e="T908" id="Seg_10249" s="T907">oloŋko-ČIt</ta>
            <ta e="T909" id="Seg_10250" s="T908">kihi</ta>
            <ta e="T910" id="Seg_10251" s="T909">kim-nI</ta>
            <ta e="T911" id="Seg_10252" s="T910">da</ta>
            <ta e="T912" id="Seg_10253" s="T911">bil-BAT-BIn</ta>
            <ta e="T913" id="Seg_10254" s="T912">anɨ</ta>
            <ta e="T914" id="Seg_10255" s="T913">oloŋko-ČIt</ta>
            <ta e="T915" id="Seg_10256" s="T914">kihi-nI</ta>
            <ta e="T916" id="Seg_10257" s="T915">ile</ta>
            <ta e="T917" id="Seg_10258" s="T916">oloŋko-ČIt</ta>
            <ta e="T918" id="Seg_10259" s="T917">kihi-nI</ta>
            <ta e="T933" id="Seg_10260" s="T932">hu͡ok</ta>
            <ta e="T934" id="Seg_10261" s="T933">hu͡ok</ta>
            <ta e="T935" id="Seg_10262" s="T934">urut</ta>
            <ta e="T936" id="Seg_10263" s="T935">bu͡olla</ta>
            <ta e="T937" id="Seg_10264" s="T936">kim-LArA</ta>
            <ta e="T938" id="Seg_10265" s="T937">daːganɨ</ta>
            <ta e="T939" id="Seg_10266" s="T938">itigirdik-keːčeːn</ta>
            <ta e="T940" id="Seg_10267" s="T939">gɨn-BA-TI-LAr</ta>
            <ta e="T941" id="Seg_10268" s="T940">bu</ta>
            <ta e="T942" id="Seg_10269" s="T941">anɨ</ta>
            <ta e="T943" id="Seg_10270" s="T942">bu</ta>
            <ta e="T944" id="Seg_10271" s="T943">bɨlɨrɨːn-GI-ttAn</ta>
            <ta e="T945" id="Seg_10272" s="T944">bettek</ta>
            <ta e="T946" id="Seg_10273" s="T945">bu</ta>
            <ta e="T947" id="Seg_10274" s="T946">ide</ta>
            <ta e="T948" id="Seg_10275" s="T947">bul-TI-LAr</ta>
            <ta e="T949" id="Seg_10276" s="T948">bu</ta>
            <ta e="T950" id="Seg_10277" s="T949">urukku</ta>
            <ta e="T951" id="Seg_10278" s="T950">oloŋko</ta>
            <ta e="T952" id="Seg_10279" s="T951">ol</ta>
            <ta e="T953" id="Seg_10280" s="T952">ihin</ta>
            <ta e="T954" id="Seg_10281" s="T953">bihigi</ta>
            <ta e="T955" id="Seg_10282" s="T954">tahɨččɨ</ta>
            <ta e="T956" id="Seg_10283" s="T955">umun-I-BIT-BIt</ta>
            <ta e="T957" id="Seg_10284" s="T956">ol-nI</ta>
            <ta e="T958" id="Seg_10285" s="T957">oloŋko-LAr-BItI-n</ta>
            <ta e="T959" id="Seg_10286" s="T958">barɨ-tI-n</ta>
            <ta e="T960" id="Seg_10287" s="T959">umun-An</ta>
            <ta e="T961" id="Seg_10288" s="T960">is-A-BIt</ta>
            <ta e="T962" id="Seg_10289" s="T961">bu͡o</ta>
            <ta e="T963" id="Seg_10290" s="T962">barɨ-tI-n</ta>
            <ta e="T964" id="Seg_10291" s="T963">kaččaga</ta>
            <ta e="T965" id="Seg_10292" s="T964">da</ta>
            <ta e="T966" id="Seg_10293" s="T965">kepseː-BAT-GIn</ta>
            <ta e="T967" id="Seg_10294" s="T966">ɨrɨ͡a-nI</ta>
            <ta e="T968" id="Seg_10295" s="T967">da</ta>
            <ta e="T969" id="Seg_10296" s="T968">umun-A-GIn</ta>
            <ta e="T970" id="Seg_10297" s="T969">oloŋko-nI</ta>
            <ta e="T971" id="Seg_10298" s="T970">da</ta>
            <ta e="T972" id="Seg_10299" s="T971">umun-A-GIn</ta>
            <ta e="T973" id="Seg_10300" s="T972">istorʼija-nI</ta>
            <ta e="T974" id="Seg_10301" s="T973">kepseː-Ar-tA</ta>
            <ta e="T975" id="Seg_10302" s="T974">bu͡olla</ta>
            <ta e="T976" id="Seg_10303" s="T975">Anʼiːsʼim</ta>
            <ta e="T977" id="Seg_10304" s="T976">ogonnʼor</ta>
            <ta e="T978" id="Seg_10305" s="T977">baːr</ta>
            <ta e="T979" id="Seg_10306" s="T978">oloŋko-ČIt</ta>
            <ta e="T980" id="Seg_10307" s="T979">emi͡e</ta>
            <ta e="T991" id="Seg_10308" s="T990">tu͡ok</ta>
            <ta e="T992" id="Seg_10309" s="T991">bil-IAK.[tA]=Ij</ta>
            <ta e="T993" id="Seg_10310" s="T992">da</ta>
            <ta e="T994" id="Seg_10311" s="T993">ol</ta>
            <ta e="T995" id="Seg_10312" s="T994">giniler-nI</ta>
            <ta e="T996" id="Seg_10313" s="T995">oloŋko</ta>
            <ta e="T997" id="Seg_10314" s="T996">iŋ-Ar-tA</ta>
            <ta e="T998" id="Seg_10315" s="T997">dʼürü</ta>
            <ta e="T999" id="Seg_10316" s="T998">giniler-GA</ta>
            <ta e="T1000" id="Seg_10317" s="T999">tu͡ok</ta>
            <ta e="T1001" id="Seg_10318" s="T1000">bil</ta>
            <ta e="T1002" id="Seg_10319" s="T1001">čubu-čubu</ta>
            <ta e="T1003" id="Seg_10320" s="T1002">anɨ</ta>
            <ta e="T1004" id="Seg_10321" s="T1003">oloŋko-LAː-A-s-BAT-BIt</ta>
            <ta e="T1005" id="Seg_10322" s="T1004">bu͡o</ta>
            <ta e="T1006" id="Seg_10323" s="T1005">meːne</ta>
            <ta e="T1007" id="Seg_10324" s="T1006">kepseː</ta>
            <ta e="T1008" id="Seg_10325" s="T1007">ere</ta>
            <ta e="T1009" id="Seg_10326" s="T1008">di͡e-TAK-TArInA</ta>
            <ta e="T1010" id="Seg_10327" s="T1009">kepseː-A-BIn</ta>
            <ta e="T1011" id="Seg_10328" s="T1010">naːda-TAK-TArInA</ta>
            <ta e="T1012" id="Seg_10329" s="T1011">kepseː-A-t-TAK-TArInA</ta>
            <ta e="T1013" id="Seg_10330" s="T1012">ere</ta>
            <ta e="T1014" id="Seg_10331" s="T1013">kepseː-A-GIn</ta>
            <ta e="T1015" id="Seg_10332" s="T1014">bu͡o</ta>
            <ta e="T1016" id="Seg_10333" s="T1015">taːk</ta>
            <ta e="T1017" id="Seg_10334" s="T1016">da</ta>
            <ta e="T1018" id="Seg_10335" s="T1017">meːne</ta>
            <ta e="T1019" id="Seg_10336" s="T1018">kaččaga</ta>
            <ta e="T1020" id="Seg_10337" s="T1019">da</ta>
            <ta e="T1021" id="Seg_10338" s="T1020">oloŋko-LAː-A-s-BAT-BIt</ta>
            <ta e="T1022" id="Seg_10339" s="T1021">bu͡o</ta>
            <ta e="T1042" id="Seg_10340" s="T1041">eː</ta>
            <ta e="T1054" id="Seg_10341" s="T1053">kaja</ta>
            <ta e="T1055" id="Seg_10342" s="T1054">atɨn-nA</ta>
            <ta e="T1056" id="Seg_10343" s="T1055">bu͡ol-I-mInA</ta>
            <ta e="T1075" id="Seg_10344" s="T1074">ogo-m</ta>
            <ta e="T1076" id="Seg_10345" s="T1075">tus-tI-nAn</ta>
            <ta e="T1077" id="Seg_10346" s="T1076">küččükkeːn</ta>
            <ta e="T1078" id="Seg_10347" s="T1077">stʼix</ta>
            <ta e="T1079" id="Seg_10348" s="T1078">obu͡oj-tI-n</ta>
            <ta e="T1080" id="Seg_10349" s="T1079">oŋor-BIT-I-m</ta>
            <ta e="T1081" id="Seg_10350" s="T1080">Ajsülüː</ta>
            <ta e="T1082" id="Seg_10351" s="T1081">tus-tI-nAn</ta>
            <ta e="T1083" id="Seg_10352" s="T1082">hulus-LAr</ta>
            <ta e="T1084" id="Seg_10353" s="T1083">kallaːn-GA</ta>
            <ta e="T1085" id="Seg_10354" s="T1084">köhün-Ar-LAr</ta>
            <ta e="T1086" id="Seg_10355" s="T1085">utuj</ta>
            <ta e="T1087" id="Seg_10356" s="T1086">utuj</ta>
            <ta e="T1088" id="Seg_10357" s="T1087">Ajsülüː</ta>
            <ta e="T1089" id="Seg_10358" s="T1088">üje-BIt</ta>
            <ta e="T1090" id="Seg_10359" s="T1089">ularɨj-TI-tA</ta>
            <ta e="T1091" id="Seg_10360" s="T1090">oduː</ta>
            <ta e="T1092" id="Seg_10361" s="T1091">atɨn</ta>
            <ta e="T1093" id="Seg_10362" s="T1092">kem</ta>
            <ta e="T1094" id="Seg_10363" s="T1093">kel-TI-tA</ta>
            <ta e="T1095" id="Seg_10364" s="T1094">türgen-kAːN-LIk</ta>
            <ta e="T1096" id="Seg_10365" s="T1095">ulaːt-Aːr</ta>
            <ta e="T1097" id="Seg_10366" s="T1096">hap-BI-n</ta>
            <ta e="T1098" id="Seg_10367" s="T1097">iŋne-BI-n</ta>
            <ta e="T1099" id="Seg_10368" s="T1098">tut-Aːr</ta>
            <ta e="T1100" id="Seg_10369" s="T1099">min</ta>
            <ta e="T1101" id="Seg_10370" s="T1100">üje-BI-n</ta>
            <ta e="T1102" id="Seg_10371" s="T1101">üje-LAN-Aːr</ta>
            <ta e="T1103" id="Seg_10372" s="T1102">kihi-GA</ta>
            <ta e="T1104" id="Seg_10373" s="T1103">kihi</ta>
            <ta e="T1105" id="Seg_10374" s="T1104">di͡e-A-t-Aːr</ta>
            <ta e="T1106" id="Seg_10375" s="T1105">ogo-kAːN-I-m</ta>
            <ta e="T1107" id="Seg_10376" s="T1106">ogo-tI-n</ta>
            <ta e="T1108" id="Seg_10377" s="T1107">bihik-tI-GAr</ta>
            <ta e="T1109" id="Seg_10378" s="T1108">bigeː-A-BIn</ta>
            <ta e="T1110" id="Seg_10379" s="T1109">attɨ-tI-GAr</ta>
            <ta e="T1111" id="Seg_10380" s="T1110">olor-A-BIn</ta>
            <ta e="T1112" id="Seg_10381" s="T1111">orgujakaːn</ta>
            <ta e="T1113" id="Seg_10382" s="T1112">ɨllaː-A-n-TI-m</ta>
         </annotation>
         <annotation name="ge" tierref="ge-UkET">
            <ta e="T23" id="Seg_10383" s="T21">1SG.[NOM]</ta>
            <ta e="T25" id="Seg_10384" s="T23">and</ta>
            <ta e="T26" id="Seg_10385" s="T25">self-1SG.[NOM]</ta>
            <ta e="T27" id="Seg_10386" s="T26">be.born-PTCP.PST</ta>
            <ta e="T28" id="Seg_10387" s="T27">earth-EP-1SG.[NOM]</ta>
            <ta e="T29" id="Seg_10388" s="T28">this.[NOM]</ta>
            <ta e="T30" id="Seg_10389" s="T29">to</ta>
            <ta e="T31" id="Seg_10390" s="T30">be-PST1-3SG</ta>
            <ta e="T32" id="Seg_10391" s="T31">north.[NOM]</ta>
            <ta e="T33" id="Seg_10392" s="T32">to</ta>
            <ta e="T34" id="Seg_10393" s="T33">that.[NOM]</ta>
            <ta e="T35" id="Seg_10394" s="T34">to</ta>
            <ta e="T37" id="Seg_10395" s="T36">who.[NOM]</ta>
            <ta e="T38" id="Seg_10396" s="T37">to</ta>
            <ta e="T39" id="Seg_10397" s="T38">eh</ta>
            <ta e="T40" id="Seg_10398" s="T39">then</ta>
            <ta e="T41" id="Seg_10399" s="T40">Novorybnoe.[NOM]</ta>
            <ta e="T42" id="Seg_10400" s="T41">on.the.other.shore-2PL-DAT/LOC</ta>
            <ta e="T43" id="Seg_10401" s="T42">that.[NOM]</ta>
            <ta e="T44" id="Seg_10402" s="T43">north.[NOM]</ta>
            <ta e="T45" id="Seg_10403" s="T44">to</ta>
            <ta e="T46" id="Seg_10404" s="T45">be.born-PST2-EP-1SG</ta>
            <ta e="T47" id="Seg_10405" s="T46">then</ta>
            <ta e="T48" id="Seg_10406" s="T47">husband-DAT/LOC</ta>
            <ta e="T49" id="Seg_10407" s="T48">go-CVB.SEQ-1SG</ta>
            <ta e="T50" id="Seg_10408" s="T49">south.[NOM]</ta>
            <ta e="T51" id="Seg_10409" s="T50">to</ta>
            <ta e="T52" id="Seg_10410" s="T51">earth-VBZ-PST2-EP-1SG</ta>
            <ta e="T53" id="Seg_10411" s="T52">child-PL-EP-1SG.[NOM]</ta>
            <ta e="T54" id="Seg_10412" s="T53">EMPH</ta>
            <ta e="T55" id="Seg_10413" s="T54">what.[NOM]</ta>
            <ta e="T56" id="Seg_10414" s="T55">how</ta>
            <ta e="T57" id="Seg_10415" s="T56">tell-CVB.SEQ</ta>
            <ta e="T58" id="Seg_10416" s="T57">EMPH</ta>
            <ta e="T59" id="Seg_10417" s="T58">what-ACC</ta>
            <ta e="T60" id="Seg_10418" s="T59">tell-FUT-1SG=Q</ta>
            <ta e="T61" id="Seg_10419" s="T60">one</ta>
            <ta e="T62" id="Seg_10420" s="T61">child-1SG.[NOM]</ta>
            <ta e="T63" id="Seg_10421" s="T62">that.[NOM]</ta>
            <ta e="T64" id="Seg_10422" s="T63">school-DAT/LOC</ta>
            <ta e="T65" id="Seg_10423" s="T64">work-PRS.[3SG]</ta>
            <ta e="T66" id="Seg_10424" s="T65">one</ta>
            <ta e="T67" id="Seg_10425" s="T66">daughter-EP-1SG.[NOM]</ta>
            <ta e="T68" id="Seg_10426" s="T67">director-VBZ-PRS.[3SG]</ta>
            <ta e="T69" id="Seg_10427" s="T68">one</ta>
            <ta e="T70" id="Seg_10428" s="T69">eh</ta>
            <ta e="T71" id="Seg_10429" s="T70">one</ta>
            <ta e="T72" id="Seg_10430" s="T71">child-1SG.[NOM]</ta>
            <ta e="T73" id="Seg_10431" s="T72">that.[NOM]</ta>
            <ta e="T74" id="Seg_10432" s="T73">Khatanga-DAT/LOC</ta>
            <ta e="T76" id="Seg_10433" s="T75">most</ta>
            <ta e="T77" id="Seg_10434" s="T76">big.[NOM]</ta>
            <ta e="T78" id="Seg_10435" s="T77">daughter-EP-1SG.[NOM]</ta>
            <ta e="T79" id="Seg_10436" s="T78">Khatanga-DAT/LOC</ta>
            <ta e="T80" id="Seg_10437" s="T79">school-DAT/LOC</ta>
            <ta e="T81" id="Seg_10438" s="T80">doctor-VBZ-PRS.[3SG]</ta>
            <ta e="T82" id="Seg_10439" s="T81">Nina</ta>
            <ta e="T84" id="Seg_10440" s="T83">that</ta>
            <ta e="T85" id="Seg_10441" s="T84">big</ta>
            <ta e="T86" id="Seg_10442" s="T85">daughter-EP-1SG.[NOM]</ta>
            <ta e="T87" id="Seg_10443" s="T86">name-3SG.[NOM]</ta>
            <ta e="T88" id="Seg_10444" s="T87">small.[NOM]</ta>
            <ta e="T89" id="Seg_10445" s="T88">that-3SG-1SG.[NOM]</ta>
            <ta e="T90" id="Seg_10446" s="T89">lower.part-EP-3SG.[NOM]</ta>
            <ta e="T91" id="Seg_10447" s="T90">Katya</ta>
            <ta e="T92" id="Seg_10448" s="T91">that</ta>
            <ta e="T93" id="Seg_10449" s="T92">school-DAT/LOC</ta>
            <ta e="T94" id="Seg_10450" s="T93">director-VBZ-PRS.[3SG]</ta>
            <ta e="T95" id="Seg_10451" s="T94">one</ta>
            <ta e="T96" id="Seg_10452" s="T95">daughter-EP-1SG.[NOM]</ta>
            <ta e="T97" id="Seg_10453" s="T96">kindergarten-DAT/LOC</ta>
            <ta e="T98" id="Seg_10454" s="T97">work-PTCP.HAB</ta>
            <ta e="T99" id="Seg_10455" s="T98">be-PST1-3SG</ta>
            <ta e="T100" id="Seg_10456" s="T99">that-3SG-1SG.[NOM]</ta>
            <ta e="T101" id="Seg_10457" s="T100">eh</ta>
            <ta e="T102" id="Seg_10458" s="T101">this</ta>
            <ta e="T103" id="Seg_10459" s="T102">kindergarten-3PL.[NOM]</ta>
            <ta e="T104" id="Seg_10460" s="T103">what-3PL.[NOM]</ta>
            <ta e="T105" id="Seg_10461" s="T104">break-CVB.SEQ</ta>
            <ta e="T106" id="Seg_10462" s="T105">especially</ta>
            <ta e="T107" id="Seg_10463" s="T106">EMPH</ta>
            <ta e="T108" id="Seg_10464" s="T107">child-VBZ-CVB.SEQ</ta>
            <ta e="T109" id="Seg_10465" s="T108">and.so.on-CVB.SEQ</ta>
            <ta e="T110" id="Seg_10466" s="T109">now</ta>
            <ta e="T111" id="Seg_10467" s="T110">be.sick-CVB.SEQ</ta>
            <ta e="T112" id="Seg_10468" s="T111">Dudinka.[NOM]</ta>
            <ta e="T113" id="Seg_10469" s="T112">to</ta>
            <ta e="T114" id="Seg_10470" s="T113">be-PRS.[3SG]</ta>
            <ta e="T115" id="Seg_10471" s="T114">this</ta>
            <ta e="T116" id="Seg_10472" s="T115">eh</ta>
            <ta e="T117" id="Seg_10473" s="T116">Norilsk-DAT/LOC</ta>
            <ta e="T118" id="Seg_10474" s="T117">heal-EP-CAUS-CVB.SIM</ta>
            <ta e="T119" id="Seg_10475" s="T118">lie-PRS.[3SG]</ta>
            <ta e="T120" id="Seg_10476" s="T119">now</ta>
            <ta e="T121" id="Seg_10477" s="T120">then</ta>
            <ta e="T122" id="Seg_10478" s="T121">one</ta>
            <ta e="T123" id="Seg_10479" s="T122">son-EP-1SG.[NOM]</ta>
            <ta e="T124" id="Seg_10480" s="T123">hunter.[NOM]</ta>
            <ta e="T125" id="Seg_10481" s="T124">be-PST1-3SG</ta>
            <ta e="T126" id="Seg_10482" s="T125">one</ta>
            <ta e="T127" id="Seg_10483" s="T126">boy-EP-1SG.[NOM]</ta>
            <ta e="T128" id="Seg_10484" s="T127">that.[NOM]</ta>
            <ta e="T129" id="Seg_10485" s="T128">airplane-PL-ACC</ta>
            <ta e="T131" id="Seg_10486" s="T130">different-3PL-ACC</ta>
            <ta e="T132" id="Seg_10487" s="T131">send</ta>
            <ta e="T133" id="Seg_10488" s="T132">what-VBZ-PRS.[3SG]</ta>
            <ta e="T134" id="Seg_10489" s="T133">make-PRS.[3SG]</ta>
            <ta e="T135" id="Seg_10490" s="T134">airplane-PL-ACC</ta>
            <ta e="T136" id="Seg_10491" s="T135">load-PRS.[3SG]</ta>
            <ta e="T137" id="Seg_10492" s="T136">fly-EP-CAUS-PRS.[3SG]</ta>
            <ta e="T138" id="Seg_10493" s="T137">there</ta>
            <ta e="T139" id="Seg_10494" s="T138">there.is</ta>
            <ta e="T140" id="Seg_10495" s="T139">one</ta>
            <ta e="T141" id="Seg_10496" s="T140">daughter-EP-1SG.[NOM]</ta>
            <ta e="T143" id="Seg_10497" s="T142">what-ACC</ta>
            <ta e="T144" id="Seg_10498" s="T143">stop-PTCP.PST</ta>
            <ta e="T145" id="Seg_10499" s="T144">be-PST1-3SG</ta>
            <ta e="T146" id="Seg_10500" s="T145">kindergarten.[NOM]</ta>
            <ta e="T147" id="Seg_10501" s="T146">education-3SG-ACC</ta>
            <ta e="T148" id="Seg_10502" s="T147">now</ta>
            <ta e="T149" id="Seg_10503" s="T148">self-1SG.[NOM]</ta>
            <ta e="T150" id="Seg_10504" s="T149">age-CVB.SEQ-1SG</ta>
            <ta e="T151" id="Seg_10505" s="T150">that-3SG-1SG-ACC</ta>
            <ta e="T152" id="Seg_10506" s="T151">self-1SG-DAT/LOC</ta>
            <ta e="T153" id="Seg_10507" s="T152">work-EP-CAUS-CVB.PURP-1SG</ta>
            <ta e="T154" id="Seg_10508" s="T153">tundra-DAT/LOC</ta>
            <ta e="T155" id="Seg_10509" s="T154">hold-PRS-1SG</ta>
            <ta e="T156" id="Seg_10510" s="T155">old.man-EP-1SG.[NOM]</ta>
            <ta e="T157" id="Seg_10511" s="T156">before</ta>
            <ta e="T158" id="Seg_10512" s="T157">hunter.[NOM]</ta>
            <ta e="T159" id="Seg_10513" s="T158">be-PST1-3SG</ta>
            <ta e="T160" id="Seg_10514" s="T159">however</ta>
            <ta e="T162" id="Seg_10515" s="T161">human.being-ABL</ta>
            <ta e="T163" id="Seg_10516" s="T162">eh</ta>
            <ta e="T164" id="Seg_10517" s="T163">Novorybnoe-DAT/LOC</ta>
            <ta e="T165" id="Seg_10518" s="T164">then</ta>
            <ta e="T166" id="Seg_10519" s="T165">sovkhoz-DAT/LOC</ta>
            <ta e="T167" id="Seg_10520" s="T166">leading-3SG-INSTR</ta>
            <ta e="T168" id="Seg_10521" s="T167">hunt-PTCP.HAB</ta>
            <ta e="T169" id="Seg_10522" s="T168">be-PST1-3SG</ta>
            <ta e="T170" id="Seg_10523" s="T169">now</ta>
            <ta e="T171" id="Seg_10524" s="T170">be.sick-CVB.SEQ</ta>
            <ta e="T172" id="Seg_10525" s="T171">hand-3SG.[NOM]</ta>
            <ta e="T173" id="Seg_10526" s="T172">be.sick-CVB.SEQ</ta>
            <ta e="T174" id="Seg_10527" s="T173">that</ta>
            <ta e="T175" id="Seg_10528" s="T174">hunt-EP-ABL</ta>
            <ta e="T176" id="Seg_10529" s="T175">EMPH</ta>
            <ta e="T177" id="Seg_10530" s="T176">go.out-PST1-3SG</ta>
            <ta e="T178" id="Seg_10531" s="T177">that-DAT/LOC</ta>
            <ta e="T179" id="Seg_10532" s="T178">additional-VBZ-CVB.SEQ</ta>
            <ta e="T180" id="Seg_10533" s="T179">now</ta>
            <ta e="T181" id="Seg_10534" s="T180">read-CAUS-CVB.SEQ</ta>
            <ta e="T182" id="Seg_10535" s="T181">retirement-DAT/LOC</ta>
            <ta e="T183" id="Seg_10536" s="T182">go.in-PST1-3SG</ta>
            <ta e="T184" id="Seg_10537" s="T183">this</ta>
            <ta e="T185" id="Seg_10538" s="T184">now</ta>
            <ta e="T186" id="Seg_10539" s="T185">this</ta>
            <ta e="T188" id="Seg_10540" s="T186">year-PL-DAT/LOC</ta>
            <ta e="T190" id="Seg_10541" s="T188">go.in-PST2-3SG</ta>
            <ta e="T203" id="Seg_10542" s="T202">AFFIRM</ta>
            <ta e="T204" id="Seg_10543" s="T203">now</ta>
            <ta e="T205" id="Seg_10544" s="T204">this</ta>
            <ta e="T206" id="Seg_10545" s="T205">child-PL-EP-1SG.[NOM]</ta>
            <ta e="T207" id="Seg_10546" s="T206">child-1SG.[NOM]</ta>
            <ta e="T208" id="Seg_10547" s="T207">child-PL-3SG-ACC</ta>
            <ta e="T209" id="Seg_10548" s="T208">care.about-CVB.SEQ-1SG</ta>
            <ta e="T210" id="Seg_10549" s="T209">that</ta>
            <ta e="T211" id="Seg_10550" s="T210">medicament-DAT/LOC</ta>
            <ta e="T212" id="Seg_10551" s="T211">come-PTCP.HAB</ta>
            <ta e="T213" id="Seg_10552" s="T212">child-1SG.[NOM]</ta>
            <ta e="T214" id="Seg_10553" s="T213">own-3SG</ta>
            <ta e="T215" id="Seg_10554" s="T214">three</ta>
            <ta e="T216" id="Seg_10555" s="T215">child-PROPR.[NOM]</ta>
            <ta e="T217" id="Seg_10556" s="T216">MOD</ta>
            <ta e="T218" id="Seg_10557" s="T217">that-3SG-PL-EP-1SG.[NOM]</ta>
            <ta e="T219" id="Seg_10558" s="T218">child-3SG-ACC</ta>
            <ta e="T220" id="Seg_10559" s="T219">care.about-CVB.SIM</ta>
            <ta e="T221" id="Seg_10560" s="T220">lie-PTCP.PRS</ta>
            <ta e="T222" id="Seg_10561" s="T221">be-PST1-1SG</ta>
            <ta e="T223" id="Seg_10562" s="T222">EMPH</ta>
            <ta e="T224" id="Seg_10563" s="T223">now</ta>
            <ta e="T225" id="Seg_10564" s="T224">though</ta>
            <ta e="T226" id="Seg_10565" s="T225">this</ta>
            <ta e="T227" id="Seg_10566" s="T226">child-1SG.[NOM]</ta>
            <ta e="T228" id="Seg_10567" s="T227">that</ta>
            <ta e="T229" id="Seg_10568" s="T228">Masha-1SG.[NOM]</ta>
            <ta e="T230" id="Seg_10569" s="T229">come-CVB.SEQ</ta>
            <ta e="T231" id="Seg_10570" s="T230">come-PST1-1SG</ta>
            <ta e="T232" id="Seg_10571" s="T231">that</ta>
            <ta e="T233" id="Seg_10572" s="T232">tundra-ABL</ta>
            <ta e="T234" id="Seg_10573" s="T233">tundra-DAT/LOC</ta>
            <ta e="T235" id="Seg_10574" s="T234">hold-CVB.SIM</ta>
            <ta e="T236" id="Seg_10575" s="T235">lie-PRS-1SG</ta>
            <ta e="T237" id="Seg_10576" s="T236">MOD</ta>
            <ta e="T238" id="Seg_10577" s="T237">that</ta>
            <ta e="T239" id="Seg_10578" s="T238">three</ta>
            <ta e="T240" id="Seg_10579" s="T239">child-1SG-ACC</ta>
            <ta e="T241" id="Seg_10580" s="T240">that</ta>
            <ta e="T242" id="Seg_10581" s="T241">mum-3SG.[NOM]</ta>
            <ta e="T243" id="Seg_10582" s="T242">this.[NOM]</ta>
            <ta e="T244" id="Seg_10583" s="T243">to</ta>
            <ta e="T245" id="Seg_10584" s="T244">there.is</ta>
            <ta e="T246" id="Seg_10585" s="T245">father-3PL.[NOM]</ta>
            <ta e="T247" id="Seg_10586" s="T246">go.away-PST2-3SG</ta>
            <ta e="T248" id="Seg_10587" s="T247">MOD</ta>
            <ta e="T249" id="Seg_10588" s="T248">that.[NOM]</ta>
            <ta e="T250" id="Seg_10589" s="T249">Nenets.[NOM]</ta>
            <ta e="T251" id="Seg_10590" s="T250">be-PST1-3SG</ta>
            <ta e="T252" id="Seg_10591" s="T251">that-3SG-3SG.[NOM]</ta>
            <ta e="T253" id="Seg_10592" s="T252">go.away-PST2-3SG</ta>
            <ta e="T254" id="Seg_10593" s="T253">that.[NOM]</ta>
            <ta e="T255" id="Seg_10594" s="T254">because.of</ta>
            <ta e="T256" id="Seg_10595" s="T255">that</ta>
            <ta e="T257" id="Seg_10596" s="T256">where.from</ta>
            <ta e="T258" id="Seg_10597" s="T257">one-ABL</ta>
            <ta e="T259" id="Seg_10598" s="T258">one</ta>
            <ta e="T260" id="Seg_10599" s="T259">child.[NOM]</ta>
            <ta e="T261" id="Seg_10600" s="T260">MOD</ta>
            <ta e="T262" id="Seg_10601" s="T261">child-PL-EP-1SG.[NOM]</ta>
            <ta e="T263" id="Seg_10602" s="T262">child-3PL.[NOM]</ta>
            <ta e="T264" id="Seg_10603" s="T263">when</ta>
            <ta e="T265" id="Seg_10604" s="T264">NEG</ta>
            <ta e="T266" id="Seg_10605" s="T265">go.away-NEG-3PL</ta>
            <ta e="T267" id="Seg_10606" s="T266">EMPH</ta>
            <ta e="T268" id="Seg_10607" s="T267">in.summer</ta>
            <ta e="T269" id="Seg_10608" s="T268">summer-PL-ACC</ta>
            <ta e="T270" id="Seg_10609" s="T269">winter-PL-ACC</ta>
            <ta e="T271" id="Seg_10610" s="T270">through</ta>
            <ta e="T272" id="Seg_10611" s="T271">child-PROPR.[NOM]</ta>
            <ta e="T273" id="Seg_10612" s="T272">be-HAB-1SG</ta>
            <ta e="T274" id="Seg_10613" s="T273">grandchild-PL-EP-1SG.[NOM]</ta>
            <ta e="T275" id="Seg_10614" s="T274">always</ta>
            <ta e="T276" id="Seg_10615" s="T275">there.is</ta>
            <ta e="T277" id="Seg_10616" s="T276">be-PRS-3PL</ta>
            <ta e="T300" id="Seg_10617" s="T299">AFFIRM</ta>
            <ta e="T301" id="Seg_10618" s="T300">tell-HAB-1PL</ta>
            <ta e="T302" id="Seg_10619" s="T301">different-DIM-ACC</ta>
            <ta e="T303" id="Seg_10620" s="T302">EMPH</ta>
            <ta e="T304" id="Seg_10621" s="T303">now</ta>
            <ta e="T305" id="Seg_10622" s="T304">big-ADVZ</ta>
            <ta e="T306" id="Seg_10623" s="T305">NEG</ta>
            <ta e="T307" id="Seg_10624" s="T306">understand-EP-MED-CVB.SIM</ta>
            <ta e="T308" id="Seg_10625" s="T307">not.yet-3PL</ta>
            <ta e="T309" id="Seg_10626" s="T308">EVID</ta>
            <ta e="T310" id="Seg_10627" s="T309">big</ta>
            <ta e="T311" id="Seg_10628" s="T310">daughter-EP-1SG.[NOM]</ta>
            <ta e="T312" id="Seg_10629" s="T311">just</ta>
            <ta e="T313" id="Seg_10630" s="T312">child-PL-3SG.[NOM]</ta>
            <ta e="T314" id="Seg_10631" s="T313">school-DAT/LOC</ta>
            <ta e="T315" id="Seg_10632" s="T314">go-PST2-3PL</ta>
            <ta e="T316" id="Seg_10633" s="T315">small.[NOM]</ta>
            <ta e="T317" id="Seg_10634" s="T316">child-PL-EP-1SG.[NOM]</ta>
            <ta e="T318" id="Seg_10635" s="T317">child-3PL.[NOM]</ta>
            <ta e="T319" id="Seg_10636" s="T318">go.in-CVB.SIM</ta>
            <ta e="T320" id="Seg_10637" s="T319">not.yet-3PL</ta>
            <ta e="T321" id="Seg_10638" s="T320">school-DAT/LOC</ta>
            <ta e="T322" id="Seg_10639" s="T321">that-PL-DAT/LOC</ta>
            <ta e="T323" id="Seg_10640" s="T322">that</ta>
            <ta e="T324" id="Seg_10641" s="T323">different-DIM-ACC</ta>
            <ta e="T325" id="Seg_10642" s="T324">tell-PRS-1SG</ta>
            <ta e="T326" id="Seg_10643" s="T325">EMPH</ta>
            <ta e="T327" id="Seg_10644" s="T326">different-ACC-various-ACC</ta>
            <ta e="T328" id="Seg_10645" s="T327">grandmother.[NOM]</ta>
            <ta e="T329" id="Seg_10646" s="T328">say-CVB.SEQ</ta>
            <ta e="T330" id="Seg_10647" s="T329">turn-CVB.SIM</ta>
            <ta e="T331" id="Seg_10648" s="T330">go-HAB-3PL</ta>
            <ta e="T332" id="Seg_10649" s="T331">EMPH</ta>
            <ta e="T333" id="Seg_10650" s="T332">that-3SG-2SG-ACC</ta>
            <ta e="T334" id="Seg_10651" s="T333">tell.[IMP.2SG]</ta>
            <ta e="T335" id="Seg_10652" s="T334">that-ACC</ta>
            <ta e="T336" id="Seg_10653" s="T335">tale-VBZ.[IMP.2SG]</ta>
            <ta e="T337" id="Seg_10654" s="T336">this-ACC</ta>
            <ta e="T338" id="Seg_10655" s="T337">tell.[IMP.2SG]</ta>
            <ta e="T339" id="Seg_10656" s="T338">that-ACC</ta>
            <ta e="T340" id="Seg_10657" s="T339">sing.[IMP.2SG]</ta>
            <ta e="T341" id="Seg_10658" s="T340">that-PL-ACC</ta>
            <ta e="T342" id="Seg_10659" s="T341">entertain-CVB.PURP-2SG</ta>
            <ta e="T343" id="Seg_10660" s="T342">well</ta>
            <ta e="T344" id="Seg_10661" s="T343">sing-EP-MED-EP-IMP.2PL</ta>
            <ta e="T345" id="Seg_10662" s="T344">tell-EP-CAUS-EP-IMP.2PL</ta>
            <ta e="T346" id="Seg_10663" s="T345">however</ta>
            <ta e="T347" id="Seg_10664" s="T346">viscous.[NOM]</ta>
            <ta e="T348" id="Seg_10665" s="T347">MOD</ta>
            <ta e="T356" id="Seg_10666" s="T355">well</ta>
            <ta e="T357" id="Seg_10667" s="T356">my</ta>
            <ta e="T358" id="Seg_10668" s="T357">ancestor-PL-EP-1SG.[NOM]</ta>
            <ta e="T359" id="Seg_10669" s="T358">every-3SG.[NOM]</ta>
            <ta e="T360" id="Seg_10670" s="T359">tale-AG.[NOM]</ta>
            <ta e="T361" id="Seg_10671" s="T360">be-PST1-3PL</ta>
            <ta e="T362" id="Seg_10672" s="T361">sing-PTCP.HAB</ta>
            <ta e="T363" id="Seg_10673" s="T362">and</ta>
            <ta e="T364" id="Seg_10674" s="T363">be-PST1-3PL</ta>
            <ta e="T365" id="Seg_10675" s="T364">song.[NOM]</ta>
            <ta e="T366" id="Seg_10676" s="T365">sing-EP</ta>
            <ta e="T367" id="Seg_10677" s="T366">sing-EP-MED-CVB.SIM</ta>
            <ta e="T368" id="Seg_10678" s="T367">song-PROPR.[NOM]</ta>
            <ta e="T369" id="Seg_10679" s="T368">and</ta>
            <ta e="T370" id="Seg_10680" s="T369">tale-ACC</ta>
            <ta e="T371" id="Seg_10681" s="T370">tell-HAB.[3SG]</ta>
            <ta e="T372" id="Seg_10682" s="T371">simply</ta>
            <ta e="T373" id="Seg_10683" s="T372">and</ta>
            <ta e="T374" id="Seg_10684" s="T373">tale-ACC</ta>
            <ta e="T375" id="Seg_10685" s="T374">tell-EP</ta>
            <ta e="T376" id="Seg_10686" s="T375">then</ta>
            <ta e="T377" id="Seg_10687" s="T376">long.ago-ADJZ</ta>
            <ta e="T378" id="Seg_10688" s="T377">time-DAT/LOC</ta>
            <ta e="T379" id="Seg_10689" s="T378">though</ta>
            <ta e="T380" id="Seg_10690" s="T379">cinema-ACC</ta>
            <ta e="T381" id="Seg_10691" s="T380">see-PRS-2SG</ta>
            <ta e="T382" id="Seg_10692" s="T381">Q</ta>
            <ta e="T383" id="Seg_10693" s="T382">then</ta>
            <ta e="T384" id="Seg_10694" s="T383">EMPH</ta>
            <ta e="T385" id="Seg_10695" s="T384">Spidola-ACC</ta>
            <ta e="T386" id="Seg_10696" s="T385">be.heard-PRS-2SG</ta>
            <ta e="T387" id="Seg_10697" s="T386">MOD</ta>
            <ta e="T388" id="Seg_10698" s="T387">wood.[NOM]</ta>
            <ta e="T389" id="Seg_10699" s="T388">toys-EP-INSTR</ta>
            <ta e="T390" id="Seg_10700" s="T389">play-PRS-2SG</ta>
            <ta e="T391" id="Seg_10701" s="T390">small.bird-PL-EP-INSTR</ta>
            <ta e="T392" id="Seg_10702" s="T391">that.[NOM]</ta>
            <ta e="T393" id="Seg_10703" s="T392">sledge.[NOM]</ta>
            <ta e="T394" id="Seg_10704" s="T393">make-CVB.SIM</ta>
            <ta e="T395" id="Seg_10705" s="T394">make-PRS-2SG</ta>
            <ta e="T396" id="Seg_10706" s="T395">out</ta>
            <ta e="T397" id="Seg_10707" s="T396">go.out-TEMP-2SG</ta>
            <ta e="T398" id="Seg_10708" s="T397">horn.[NOM]</ta>
            <ta e="T399" id="Seg_10709" s="T398">throw-CVB.SEQ</ta>
            <ta e="T400" id="Seg_10710" s="T399">horn-EP-INSTR</ta>
            <ta e="T401" id="Seg_10711" s="T400">play-FUT-2SG</ta>
            <ta e="T402" id="Seg_10712" s="T401">boy.[NOM]</ta>
            <ta e="T403" id="Seg_10713" s="T402">child.[NOM]</ta>
            <ta e="T404" id="Seg_10714" s="T403">MOD</ta>
            <ta e="T405" id="Seg_10715" s="T404">noose-VBZ-CVB.SIM</ta>
            <ta e="T406" id="Seg_10716" s="T405">play-FUT-3SG</ta>
            <ta e="T407" id="Seg_10717" s="T406">in.summer</ta>
            <ta e="T408" id="Seg_10718" s="T407">how</ta>
            <ta e="T409" id="Seg_10719" s="T408">go-PTCP.PRS-2SG-ACC</ta>
            <ta e="T410" id="Seg_10720" s="T409">stone-ACC</ta>
            <ta e="T411" id="Seg_10721" s="T410">gather-EP-MED-CVB.SEQ-2SG</ta>
            <ta e="T412" id="Seg_10722" s="T411">caravan.[NOM]</ta>
            <ta e="T413" id="Seg_10723" s="T412">play-FUT-2SG</ta>
            <ta e="T414" id="Seg_10724" s="T413">that.EMPH.[NOM]</ta>
            <ta e="T415" id="Seg_10725" s="T414">like-INTNS</ta>
            <ta e="T416" id="Seg_10726" s="T415">day-ACC</ta>
            <ta e="T417" id="Seg_10727" s="T416">cut-PRS-2SG</ta>
            <ta e="T418" id="Seg_10728" s="T417">MOD</ta>
            <ta e="T419" id="Seg_10729" s="T418">long.ago-ADJZ</ta>
            <ta e="T420" id="Seg_10730" s="T419">child.[NOM]</ta>
            <ta e="T421" id="Seg_10731" s="T420">olden.time-SIM</ta>
            <ta e="T422" id="Seg_10732" s="T421">play-PRS.[3SG]</ta>
            <ta e="T423" id="Seg_10733" s="T422">EMPH</ta>
            <ta e="T424" id="Seg_10734" s="T423">that</ta>
            <ta e="T425" id="Seg_10735" s="T424">cut-NMNZ-3SG-INSTR</ta>
            <ta e="T426" id="Seg_10736" s="T425">now</ta>
            <ta e="T427" id="Seg_10737" s="T426">long.ago-ADJZ</ta>
            <ta e="T428" id="Seg_10738" s="T427">toys-ACC</ta>
            <ta e="T429" id="Seg_10739" s="T428">EMPH</ta>
            <ta e="T430" id="Seg_10740" s="T429">now</ta>
            <ta e="T431" id="Seg_10741" s="T430">EMPH</ta>
            <ta e="T432" id="Seg_10742" s="T431">see-PTCP.FUT-2PL-ACC</ta>
            <ta e="T433" id="Seg_10743" s="T432">EMPH</ta>
            <ta e="T434" id="Seg_10744" s="T433">round.and.thick</ta>
            <ta e="T435" id="Seg_10745" s="T434">toys-ACC</ta>
            <ta e="T436" id="Seg_10746" s="T435">bring-PST2-EP-1SG</ta>
            <ta e="T437" id="Seg_10747" s="T436">MOD</ta>
            <ta e="T438" id="Seg_10748" s="T437">this</ta>
            <ta e="T439" id="Seg_10749" s="T438">place-DAT/LOC</ta>
            <ta e="T440" id="Seg_10750" s="T439">however</ta>
            <ta e="T441" id="Seg_10751" s="T440">show-FUT-1SG</ta>
            <ta e="T442" id="Seg_10752" s="T441">that</ta>
            <ta e="T443" id="Seg_10753" s="T442">play-NMNZ.[NOM]</ta>
            <ta e="T444" id="Seg_10754" s="T443">how</ta>
            <ta e="T445" id="Seg_10755" s="T444">play-PTCP.PRS-3PL-ACC</ta>
            <ta e="T446" id="Seg_10756" s="T445">some</ta>
            <ta e="T447" id="Seg_10757" s="T446">toys-PL-ACC</ta>
            <ta e="T448" id="Seg_10758" s="T447">make</ta>
            <ta e="T449" id="Seg_10759" s="T448">make-CVB.SEQ</ta>
            <ta e="T450" id="Seg_10760" s="T449">bring-PTCP.FUT-1SG-ACC</ta>
            <ta e="T451" id="Seg_10761" s="T450">human.being-POSS</ta>
            <ta e="T452" id="Seg_10762" s="T451">NEG-1SG</ta>
            <ta e="T453" id="Seg_10763" s="T452">old.man-EP-1SG.[NOM]</ta>
            <ta e="T454" id="Seg_10764" s="T453">that</ta>
            <ta e="T455" id="Seg_10765" s="T454">hospital-DAT/LOC</ta>
            <ta e="T456" id="Seg_10766" s="T455">lie-PTCP.PRS</ta>
            <ta e="T457" id="Seg_10767" s="T456">be-PST1-3SG</ta>
            <ta e="T458" id="Seg_10768" s="T457">MOD</ta>
            <ta e="T459" id="Seg_10769" s="T458">one</ta>
            <ta e="T460" id="Seg_10770" s="T459">son-EP-1SG.[NOM]</ta>
            <ta e="T461" id="Seg_10771" s="T460">however</ta>
            <ta e="T462" id="Seg_10772" s="T461">be.sick-CVB.SEQ</ta>
            <ta e="T463" id="Seg_10773" s="T462">hospital-ABL</ta>
            <ta e="T464" id="Seg_10774" s="T463">go.out-CVB.ANT</ta>
            <ta e="T465" id="Seg_10775" s="T464">well</ta>
            <ta e="T466" id="Seg_10776" s="T465">come-PST2-EP-1SG</ta>
            <ta e="T467" id="Seg_10777" s="T466">MOD</ta>
            <ta e="T468" id="Seg_10778" s="T467">that-3SG-1SG.[NOM]</ta>
            <ta e="T469" id="Seg_10779" s="T468">1SG.[NOM]</ta>
            <ta e="T470" id="Seg_10780" s="T469">tundra-DAT/LOC</ta>
            <ta e="T471" id="Seg_10781" s="T470">there.is</ta>
            <ta e="T472" id="Seg_10782" s="T471">be-PST1-3SG</ta>
            <ta e="T473" id="Seg_10783" s="T472">that-3SG-1SG.[NOM]</ta>
            <ta e="T474" id="Seg_10784" s="T473">village-DAT/LOC</ta>
            <ta e="T475" id="Seg_10785" s="T474">go.in-CVB.SEQ</ta>
            <ta e="T476" id="Seg_10786" s="T475">be.sick-EP-PST2-3SG</ta>
            <ta e="T477" id="Seg_10787" s="T476">then</ta>
            <ta e="T478" id="Seg_10788" s="T477">later</ta>
            <ta e="T479" id="Seg_10789" s="T478">go.out-EP-PST2-3SG</ta>
            <ta e="T480" id="Seg_10790" s="T479">come-PTCP.PRS-1PL.[NOM]</ta>
            <ta e="T481" id="Seg_10791" s="T480">EMPH</ta>
            <ta e="T482" id="Seg_10792" s="T481">that-ACC</ta>
            <ta e="T483" id="Seg_10793" s="T482">snowstorm-PL.[NOM]</ta>
            <ta e="T484" id="Seg_10794" s="T483">what-PL.[NOM]</ta>
            <ta e="T485" id="Seg_10795" s="T484">EMPH</ta>
            <ta e="T486" id="Seg_10796" s="T485">disturb-VBZ-CVB.SEQ</ta>
            <ta e="T487" id="Seg_10797" s="T486">EMPH-then</ta>
            <ta e="T488" id="Seg_10798" s="T487">what.[NOM]</ta>
            <ta e="T489" id="Seg_10799" s="T488">many</ta>
            <ta e="T490" id="Seg_10800" s="T489">toys.[NOM]</ta>
            <ta e="T491" id="Seg_10801" s="T490">bring-PST2.NEG-EP-1SG</ta>
            <ta e="T492" id="Seg_10802" s="T491">former</ta>
            <ta e="T493" id="Seg_10803" s="T492">child.[NOM]</ta>
            <ta e="T494" id="Seg_10804" s="T493">toys-3SG-ACC</ta>
            <ta e="T495" id="Seg_10805" s="T494">however</ta>
            <ta e="T496" id="Seg_10806" s="T495">then</ta>
            <ta e="T497" id="Seg_10807" s="T496">different</ta>
            <ta e="T498" id="Seg_10808" s="T497">show-NMNZ-DAT/LOC</ta>
            <ta e="T499" id="Seg_10809" s="T498">bring-PTCP.FUT</ta>
            <ta e="T500" id="Seg_10810" s="T499">be-PST1-1SG</ta>
            <ta e="T501" id="Seg_10811" s="T500">before</ta>
            <ta e="T502" id="Seg_10812" s="T501">self-1SG.[NOM]</ta>
            <ta e="T503" id="Seg_10813" s="T502">play-CVB.SEQ</ta>
            <ta e="T504" id="Seg_10814" s="T503">arise-PST2-1SG</ta>
            <ta e="T505" id="Seg_10815" s="T504">well</ta>
            <ta e="T506" id="Seg_10816" s="T505">then</ta>
            <ta e="T518" id="Seg_10817" s="T517">well</ta>
            <ta e="T519" id="Seg_10818" s="T518">thread-EP-INSTR</ta>
            <ta e="T520" id="Seg_10819" s="T519">wind-PRS-1PL</ta>
            <ta e="T521" id="Seg_10820" s="T520">hole-PROPR</ta>
            <ta e="T522" id="Seg_10821" s="T521">wood-DAT/LOC</ta>
            <ta e="T523" id="Seg_10822" s="T522">head-EP-INSTR</ta>
            <ta e="T524" id="Seg_10823" s="T523">across</ta>
            <ta e="T525" id="Seg_10824" s="T524">stand-CVB.SEQ</ta>
            <ta e="T526" id="Seg_10825" s="T525">turn-PRS.[3SG]</ta>
            <ta e="T527" id="Seg_10826" s="T526">that-ACC</ta>
            <ta e="T528" id="Seg_10827" s="T527">see-TEMP-2SG</ta>
            <ta e="T529" id="Seg_10828" s="T528">just</ta>
            <ta e="T530" id="Seg_10829" s="T529">get.to.know-FUT-2SG</ta>
            <ta e="T531" id="Seg_10830" s="T530">MOD</ta>
            <ta e="T532" id="Seg_10831" s="T531">how</ta>
            <ta e="T533" id="Seg_10832" s="T532">now</ta>
            <ta e="T534" id="Seg_10833" s="T533">tell-CVB.SEQ-1SG</ta>
            <ta e="T535" id="Seg_10834" s="T534">how</ta>
            <ta e="T536" id="Seg_10835" s="T535">tell-FUT-1SG=Q</ta>
            <ta e="T537" id="Seg_10836" s="T536">show-FUT-1SG</ta>
            <ta e="T538" id="Seg_10837" s="T537">that</ta>
            <ta e="T539" id="Seg_10838" s="T538">see-TEMP-2SG</ta>
            <ta e="T540" id="Seg_10839" s="T539">know-FUT.[IMP.2SG]</ta>
            <ta e="T559" id="Seg_10840" s="T558">AFFIRM</ta>
            <ta e="T560" id="Seg_10841" s="T559">know-PRS-1SG</ta>
            <ta e="T561" id="Seg_10842" s="T560">such-PL-ACC</ta>
            <ta e="T562" id="Seg_10843" s="T561">such</ta>
            <ta e="T563" id="Seg_10844" s="T562">such-PART</ta>
            <ta e="T564" id="Seg_10845" s="T563">simply</ta>
            <ta e="T565" id="Seg_10846" s="T564">any.[NOM]</ta>
            <ta e="T566" id="Seg_10847" s="T565">song-PART</ta>
            <ta e="T567" id="Seg_10848" s="T566">sing-FUT-1SG</ta>
            <ta e="T568" id="Seg_10849" s="T567">Q</ta>
            <ta e="T569" id="Seg_10850" s="T568">any-PART</ta>
            <ta e="T570" id="Seg_10851" s="T569">any.[NOM]</ta>
            <ta e="T571" id="Seg_10852" s="T570">song-PART</ta>
            <ta e="T572" id="Seg_10853" s="T571">sing-FUT-1SG</ta>
            <ta e="T573" id="Seg_10854" s="T572">wait</ta>
            <ta e="T574" id="Seg_10855" s="T573">come-CVB.SIM</ta>
            <ta e="T575" id="Seg_10856" s="T574">pull-NEG.[3SG]</ta>
            <ta e="T576" id="Seg_10857" s="T575">that-3SG-2SG.[NOM]</ta>
            <ta e="T577" id="Seg_10858" s="T576">come-FUT</ta>
            <ta e="T578" id="Seg_10859" s="T577">come-PTCP.FUT-PART</ta>
            <ta e="T579" id="Seg_10860" s="T578">NEG</ta>
            <ta e="T580" id="Seg_10861" s="T579">be-HAB.[3SG]</ta>
            <ta e="T581" id="Seg_10862" s="T580">melody-3PL-ACC</ta>
            <ta e="T582" id="Seg_10863" s="T581">remember-NEG-1SG</ta>
            <ta e="T583" id="Seg_10864" s="T582">forget-EP-PST2-1SG</ta>
            <ta e="T584" id="Seg_10865" s="T583">before</ta>
            <ta e="T585" id="Seg_10866" s="T584">though</ta>
            <ta e="T586" id="Seg_10867" s="T585">3SG-INSTR</ta>
            <ta e="T587" id="Seg_10868" s="T586">deal.with-VBZ-FUT-1SG</ta>
            <ta e="T588" id="Seg_10869" s="T587">think-CVB.SEQ-1SG</ta>
            <ta e="T589" id="Seg_10870" s="T588">very</ta>
            <ta e="T590" id="Seg_10871" s="T589">make-VBZ-NEG.CVB.SIM</ta>
            <ta e="T591" id="Seg_10872" s="T590">go-EP-PST2-EP-1SG</ta>
            <ta e="T592" id="Seg_10873" s="T591">MOD</ta>
            <ta e="T593" id="Seg_10874" s="T592">that.[NOM]</ta>
            <ta e="T594" id="Seg_10875" s="T593">because.of</ta>
            <ta e="T595" id="Seg_10876" s="T594">forget-PRS-1SG</ta>
            <ta e="T611" id="Seg_10877" s="T610">that.[NOM]</ta>
            <ta e="T612" id="Seg_10878" s="T611">die-PST2-3SG</ta>
            <ta e="T613" id="Seg_10879" s="T612">that.[NOM]</ta>
            <ta e="T614" id="Seg_10880" s="T613">Baya.[NOM]</ta>
            <ta e="T615" id="Seg_10881" s="T614">say-CVB.SEQ</ta>
            <ta e="T616" id="Seg_10882" s="T615">girl.[NOM]</ta>
            <ta e="T617" id="Seg_10883" s="T616">sing-PST2-3SG</ta>
            <ta e="T618" id="Seg_10884" s="T617">sing-PTCP.PST</ta>
            <ta e="T619" id="Seg_10885" s="T618">boy-3SG.[NOM]</ta>
            <ta e="T620" id="Seg_10886" s="T619">however</ta>
            <ta e="T621" id="Seg_10887" s="T620">die-PST2-3SG</ta>
            <ta e="T642" id="Seg_10888" s="T641">AFFIRM</ta>
            <ta e="T643" id="Seg_10889" s="T642">such-3PL-INSTR</ta>
            <ta e="T644" id="Seg_10890" s="T643">Tonya-ACC</ta>
            <ta e="T645" id="Seg_10891" s="T644">with</ta>
            <ta e="T646" id="Seg_10892" s="T645">however</ta>
            <ta e="T647" id="Seg_10893" s="T646">eh</ta>
            <ta e="T648" id="Seg_10894" s="T647">what-VBZ-FUT-1PL</ta>
            <ta e="T649" id="Seg_10895" s="T648">compete-PTCP.PRS-ACC</ta>
            <ta e="T650" id="Seg_10896" s="T649">however</ta>
            <ta e="T651" id="Seg_10897" s="T650">sing-FUT-1PL</ta>
            <ta e="T652" id="Seg_10898" s="T651">MOD</ta>
            <ta e="T653" id="Seg_10899" s="T652">simply</ta>
            <ta e="T654" id="Seg_10900" s="T653">breath-CVB.SIM</ta>
            <ta e="T655" id="Seg_10901" s="T654">breath-CVB.SIM</ta>
            <ta e="T656" id="Seg_10902" s="T655">eh</ta>
            <ta e="T657" id="Seg_10903" s="T656">holding.ones.breath-ACC</ta>
            <ta e="T658" id="Seg_10904" s="T657">breath-CVB.SIM</ta>
            <ta e="T659" id="Seg_10905" s="T658">breath-PRS-2SG</ta>
            <ta e="T660" id="Seg_10906" s="T659">MOD</ta>
            <ta e="T663" id="Seg_10907" s="T662">sing.[IMP.2SG]</ta>
            <ta e="T664" id="Seg_10908" s="T663">say-TEMP-2SG</ta>
            <ta e="T665" id="Seg_10909" s="T664">sing-FUT-1SG</ta>
            <ta e="T666" id="Seg_10910" s="T665">simply</ta>
            <ta e="T667" id="Seg_10911" s="T666">breath.[NOM]</ta>
            <ta e="T668" id="Seg_10912" s="T667">take.away-EP-RECP/COLL-PTCP.PRS</ta>
            <ta e="T669" id="Seg_10913" s="T668">poem.[NOM]</ta>
            <ta e="T685" id="Seg_10914" s="T684">that-ACC</ta>
            <ta e="T686" id="Seg_10915" s="T685">MOD</ta>
            <ta e="T687" id="Seg_10916" s="T686">lonely.[NOM]</ta>
            <ta e="T688" id="Seg_10917" s="T687">breath-INSTR</ta>
            <ta e="T689" id="Seg_10918" s="T688">sing-HAB-3PL</ta>
            <ta e="T690" id="Seg_10919" s="T689">it.is.said</ta>
            <ta e="T691" id="Seg_10920" s="T690">ancestor-PL.[NOM]</ta>
            <ta e="T692" id="Seg_10921" s="T691">lonely.[NOM]</ta>
            <ta e="T693" id="Seg_10922" s="T692">breath-INSTR</ta>
            <ta e="T694" id="Seg_10923" s="T693">breath.[NOM]</ta>
            <ta e="T695" id="Seg_10924" s="T694">take.away-EP-RECP/COLL-CVB.SEQ-3PL</ta>
            <ta e="T696" id="Seg_10925" s="T695">well</ta>
            <ta e="T697" id="Seg_10926" s="T696">3PL.[NOM]</ta>
            <ta e="T698" id="Seg_10927" s="T697">toys-3PL.[NOM]</ta>
            <ta e="T699" id="Seg_10928" s="T698">that</ta>
            <ta e="T700" id="Seg_10929" s="T699">be-PST1-3SG</ta>
            <ta e="T701" id="Seg_10930" s="T700">this</ta>
            <ta e="T702" id="Seg_10931" s="T701">artist-PL.[NOM]</ta>
            <ta e="T703" id="Seg_10932" s="T702">now</ta>
            <ta e="T704" id="Seg_10933" s="T703">play-PTCP.PRS-3PL-ACC</ta>
            <ta e="T705" id="Seg_10934" s="T704">similar</ta>
            <ta e="T706" id="Seg_10935" s="T705">that.EMPH</ta>
            <ta e="T707" id="Seg_10936" s="T706">lonely.[NOM]</ta>
            <ta e="T708" id="Seg_10937" s="T707">breath-INSTR</ta>
            <ta e="T709" id="Seg_10938" s="T708">reach-PTCP.HAB</ta>
            <ta e="T710" id="Seg_10939" s="T709">be-PST1-3SG</ta>
            <ta e="T711" id="Seg_10940" s="T710">it.is.said</ta>
            <ta e="T712" id="Seg_10941" s="T711">Innokentiy.[NOM]</ta>
            <ta e="T713" id="Seg_10942" s="T712">say-CVB.SEQ</ta>
            <ta e="T714" id="Seg_10943" s="T713">human.being.[NOM]</ta>
            <ta e="T715" id="Seg_10944" s="T714">that.[NOM]</ta>
            <ta e="T716" id="Seg_10945" s="T715">that-DAT/LOC</ta>
            <ta e="T728" id="Seg_10946" s="T727">that.[NOM]</ta>
            <ta e="T729" id="Seg_10947" s="T728">my</ta>
            <ta e="T730" id="Seg_10948" s="T729">daughter-EP-1SG.[NOM]</ta>
            <ta e="T731" id="Seg_10949" s="T730">that.[NOM]</ta>
            <ta e="T761" id="Seg_10950" s="T760">AFFIRM</ta>
            <ta e="T762" id="Seg_10951" s="T761">well</ta>
            <ta e="T763" id="Seg_10952" s="T762">that</ta>
            <ta e="T764" id="Seg_10953" s="T763">1SG.[NOM]</ta>
            <ta e="T765" id="Seg_10954" s="T764">tell-PTCP.PST</ta>
            <ta e="T766" id="Seg_10955" s="T765">tale-1SG-ACC</ta>
            <ta e="T767" id="Seg_10956" s="T766">%%</ta>
            <ta e="T768" id="Seg_10957" s="T767">Taiska-ACC</ta>
            <ta e="T769" id="Seg_10958" s="T768">tell-PST2-3PL</ta>
            <ta e="T770" id="Seg_10959" s="T769">1SG-ACC</ta>
            <ta e="T771" id="Seg_10960" s="T770">tell-EP-CAUS-CVB.SEQ</ta>
            <ta e="T772" id="Seg_10961" s="T771">after</ta>
            <ta e="T773" id="Seg_10962" s="T772">come-PST2-3PL</ta>
            <ta e="T774" id="Seg_10963" s="T773">hither</ta>
            <ta e="T775" id="Seg_10964" s="T774">come-CVB.PURP-3PL</ta>
            <ta e="T776" id="Seg_10965" s="T775">%%</ta>
            <ta e="T777" id="Seg_10966" s="T776">year-DAT/LOC</ta>
            <ta e="T778" id="Seg_10967" s="T777">come-PST2-3PL</ta>
            <ta e="T779" id="Seg_10968" s="T778">play-CVB.PURP-3PL</ta>
            <ta e="T780" id="Seg_10969" s="T779">that-3SG-3PL-ACC</ta>
            <ta e="T781" id="Seg_10970" s="T780">EMPH</ta>
            <ta e="T782" id="Seg_10971" s="T781">that.[NOM]</ta>
            <ta e="T783" id="Seg_10972" s="T782">how</ta>
            <ta e="T784" id="Seg_10973" s="T783">how</ta>
            <ta e="T785" id="Seg_10974" s="T784">tell-CVB.SEQ</ta>
            <ta e="T786" id="Seg_10975" s="T785">throw-PTCP.PST</ta>
            <ta e="T787" id="Seg_10976" s="T786">be-PST1-3PL</ta>
            <ta e="T788" id="Seg_10977" s="T787">then</ta>
            <ta e="T789" id="Seg_10978" s="T788">leftover-3SG.[NOM]</ta>
            <ta e="T793" id="Seg_10979" s="T792">AFFIRM</ta>
            <ta e="T794" id="Seg_10980" s="T793">hear-EP-PST2-EP-1SG</ta>
            <ta e="T795" id="Seg_10981" s="T794">that-ACC</ta>
            <ta e="T796" id="Seg_10982" s="T795">hear-CVB.SEQ-1SG</ta>
            <ta e="T797" id="Seg_10983" s="T796">make-PRS-1SG</ta>
            <ta e="T798" id="Seg_10984" s="T797">that.[NOM]</ta>
            <ta e="T800" id="Seg_10985" s="T799">that-PL-ACC</ta>
            <ta e="T802" id="Seg_10986" s="T801">that-PL-ACC</ta>
            <ta e="T803" id="Seg_10987" s="T802">come-PRS-3PL</ta>
            <ta e="T804" id="Seg_10988" s="T803">that-PL-ACC</ta>
            <ta e="T805" id="Seg_10989" s="T804">rebuke-PRS-1SG</ta>
            <ta e="T806" id="Seg_10990" s="T805">why</ta>
            <ta e="T807" id="Seg_10991" s="T806">well</ta>
            <ta e="T808" id="Seg_10992" s="T807">that.[NOM]</ta>
            <ta e="T809" id="Seg_10993" s="T808">that-INSTR-this-INSTR</ta>
            <ta e="T810" id="Seg_10994" s="T809">tell-PRS-2PL</ta>
            <ta e="T811" id="Seg_10995" s="T810">real-SIM</ta>
            <ta e="T812" id="Seg_10996" s="T811">tell-NEG-2PL</ta>
            <ta e="T813" id="Seg_10997" s="T812">tale-2PL-ACC</ta>
            <ta e="T820" id="Seg_10998" s="T819">however</ta>
            <ta e="T821" id="Seg_10999" s="T820">however</ta>
            <ta e="T822" id="Seg_11000" s="T821">notice-EP-REFL-PRS.[3SG]</ta>
            <ta e="T823" id="Seg_11001" s="T822">then</ta>
            <ta e="T824" id="Seg_11002" s="T823">basic-ACC</ta>
            <ta e="T825" id="Seg_11003" s="T824">however</ta>
            <ta e="T826" id="Seg_11004" s="T825">tell-PRS-3PL</ta>
            <ta e="T849" id="Seg_11005" s="T848">AFFIRM</ta>
            <ta e="T850" id="Seg_11006" s="T849">there.is</ta>
            <ta e="T851" id="Seg_11007" s="T850">EVID</ta>
            <ta e="T852" id="Seg_11008" s="T851">that</ta>
            <ta e="T853" id="Seg_11009" s="T852">tale-ACC</ta>
            <ta e="T854" id="Seg_11010" s="T853">know-PTCP.PRS</ta>
            <ta e="T855" id="Seg_11011" s="T854">1SG.[NOM]</ta>
            <ta e="T856" id="Seg_11012" s="T855">elder.brother-EP-1SG.[NOM]</ta>
            <ta e="T857" id="Seg_11013" s="T856">there.is</ta>
            <ta e="T858" id="Seg_11014" s="T857">elder.brother-PL-EP-1SG.[NOM]</ta>
            <ta e="T859" id="Seg_11015" s="T858">then</ta>
            <ta e="T860" id="Seg_11016" s="T859">two-COLL</ta>
            <ta e="T861" id="Seg_11017" s="T860">EMPH</ta>
            <ta e="T862" id="Seg_11018" s="T861">know-PRS-3PL</ta>
            <ta e="T863" id="Seg_11019" s="T862">two</ta>
            <ta e="T864" id="Seg_11020" s="T863">elder.brother-PROPR-1SG</ta>
            <ta e="T865" id="Seg_11021" s="T864">two-COLL</ta>
            <ta e="T866" id="Seg_11022" s="T865">EMPH</ta>
            <ta e="T867" id="Seg_11023" s="T866">know-EP</ta>
            <ta e="T868" id="Seg_11024" s="T867">one.out.of.two-3SG.[NOM]</ta>
            <ta e="T869" id="Seg_11025" s="T868">old.[NOM]</ta>
            <ta e="T870" id="Seg_11026" s="T869">one.out.of.two-3SG.[NOM]</ta>
            <ta e="T871" id="Seg_11027" s="T870">1SG-COMP</ta>
            <ta e="T872" id="Seg_11028" s="T871">however</ta>
            <ta e="T873" id="Seg_11029" s="T872">older.[NOM]</ta>
            <ta e="T874" id="Seg_11030" s="T873">know-PRS-3PL</ta>
            <ta e="T875" id="Seg_11031" s="T874">that-PL.[NOM]</ta>
            <ta e="T876" id="Seg_11032" s="T875">tale-PL-ACC</ta>
            <ta e="T877" id="Seg_11033" s="T876">song-PROPR-PL-ACC</ta>
            <ta e="T878" id="Seg_11034" s="T877">and</ta>
            <ta e="T879" id="Seg_11035" s="T878">know-PRS-3PL</ta>
            <ta e="T880" id="Seg_11036" s="T879">song-POSS</ta>
            <ta e="T881" id="Seg_11037" s="T880">and</ta>
            <ta e="T882" id="Seg_11038" s="T881">NEG-ACC</ta>
            <ta e="T883" id="Seg_11039" s="T882">know-EP</ta>
            <ta e="T888" id="Seg_11040" s="T887">simply</ta>
            <ta e="T889" id="Seg_11041" s="T888">human.being-PL.[NOM]</ta>
            <ta e="T890" id="Seg_11042" s="T889">that.[NOM]</ta>
            <ta e="T891" id="Seg_11043" s="T890">before</ta>
            <ta e="T892" id="Seg_11044" s="T891">tale-AG.[NOM]</ta>
            <ta e="T893" id="Seg_11045" s="T892">human.being-PL-1PL.[NOM]</ta>
            <ta e="T894" id="Seg_11046" s="T893">run.out-PST1-3PL</ta>
            <ta e="T895" id="Seg_11047" s="T894">MOD</ta>
            <ta e="T896" id="Seg_11048" s="T895">now</ta>
            <ta e="T897" id="Seg_11049" s="T896">indeed</ta>
            <ta e="T898" id="Seg_11050" s="T897">indeed</ta>
            <ta e="T899" id="Seg_11051" s="T898">1SG.[NOM]</ta>
            <ta e="T900" id="Seg_11052" s="T899">know-PTCP.PRS</ta>
            <ta e="T901" id="Seg_11053" s="T900">tale-AG.[NOM]</ta>
            <ta e="T902" id="Seg_11054" s="T901">human.being-PL-EP-1SG.[NOM]</ta>
            <ta e="T903" id="Seg_11055" s="T902">run.out-PST1-3PL</ta>
            <ta e="T904" id="Seg_11056" s="T903">who.[NOM]</ta>
            <ta e="T905" id="Seg_11057" s="T904">there.is-3SG</ta>
            <ta e="T906" id="Seg_11058" s="T905">be-FUT.[3SG]=Q</ta>
            <ta e="T907" id="Seg_11059" s="T906">now</ta>
            <ta e="T908" id="Seg_11060" s="T907">tale-AG.[NOM]</ta>
            <ta e="T909" id="Seg_11061" s="T908">human.being.[NOM]</ta>
            <ta e="T910" id="Seg_11062" s="T909">who-ACC</ta>
            <ta e="T911" id="Seg_11063" s="T910">NEG</ta>
            <ta e="T912" id="Seg_11064" s="T911">know-NEG-1SG</ta>
            <ta e="T913" id="Seg_11065" s="T912">now</ta>
            <ta e="T914" id="Seg_11066" s="T913">tale-AG.[NOM]</ta>
            <ta e="T915" id="Seg_11067" s="T914">human.being-ACC</ta>
            <ta e="T916" id="Seg_11068" s="T915">real</ta>
            <ta e="T917" id="Seg_11069" s="T916">tale-AG.[NOM]</ta>
            <ta e="T918" id="Seg_11070" s="T917">human.being-ACC</ta>
            <ta e="T933" id="Seg_11071" s="T932">no</ta>
            <ta e="T934" id="Seg_11072" s="T933">no</ta>
            <ta e="T935" id="Seg_11073" s="T934">before</ta>
            <ta e="T936" id="Seg_11074" s="T935">MOD</ta>
            <ta e="T937" id="Seg_11075" s="T936">who-3PL.[NOM]</ta>
            <ta e="T938" id="Seg_11076" s="T937">EMPH</ta>
            <ta e="T939" id="Seg_11077" s="T938">like.that-INTNS</ta>
            <ta e="T940" id="Seg_11078" s="T939">make-NEG-PST1-3PL</ta>
            <ta e="T941" id="Seg_11079" s="T940">this</ta>
            <ta e="T942" id="Seg_11080" s="T941">now</ta>
            <ta e="T943" id="Seg_11081" s="T942">this</ta>
            <ta e="T944" id="Seg_11082" s="T943">last.year-ADJZ-ABL</ta>
            <ta e="T945" id="Seg_11083" s="T944">closer</ta>
            <ta e="T946" id="Seg_11084" s="T945">this</ta>
            <ta e="T947" id="Seg_11085" s="T946">custom.[NOM]</ta>
            <ta e="T948" id="Seg_11086" s="T947">find-PST1-3PL</ta>
            <ta e="T949" id="Seg_11087" s="T948">this</ta>
            <ta e="T950" id="Seg_11088" s="T949">former</ta>
            <ta e="T951" id="Seg_11089" s="T950">tale.[NOM]</ta>
            <ta e="T952" id="Seg_11090" s="T951">that.[NOM]</ta>
            <ta e="T953" id="Seg_11091" s="T952">because.of</ta>
            <ta e="T954" id="Seg_11092" s="T953">1PL.[NOM]</ta>
            <ta e="T955" id="Seg_11093" s="T954">completely</ta>
            <ta e="T956" id="Seg_11094" s="T955">forget-EP-PST2-1PL</ta>
            <ta e="T957" id="Seg_11095" s="T956">that-ACC</ta>
            <ta e="T958" id="Seg_11096" s="T957">tale-PL-1PL-ACC</ta>
            <ta e="T959" id="Seg_11097" s="T958">whole-3SG-ACC</ta>
            <ta e="T960" id="Seg_11098" s="T959">forget-CVB.SEQ</ta>
            <ta e="T961" id="Seg_11099" s="T960">go-PRS-1PL</ta>
            <ta e="T962" id="Seg_11100" s="T961">EMPH</ta>
            <ta e="T963" id="Seg_11101" s="T962">whole-3SG-ACC</ta>
            <ta e="T964" id="Seg_11102" s="T963">when</ta>
            <ta e="T965" id="Seg_11103" s="T964">NEG</ta>
            <ta e="T966" id="Seg_11104" s="T965">tell-NEG-2SG</ta>
            <ta e="T967" id="Seg_11105" s="T966">song-ACC</ta>
            <ta e="T968" id="Seg_11106" s="T967">and</ta>
            <ta e="T969" id="Seg_11107" s="T968">forget-PRS-2SG</ta>
            <ta e="T970" id="Seg_11108" s="T969">tale-ACC</ta>
            <ta e="T971" id="Seg_11109" s="T970">and</ta>
            <ta e="T972" id="Seg_11110" s="T971">forget-PRS-2SG</ta>
            <ta e="T973" id="Seg_11111" s="T972">history-ACC</ta>
            <ta e="T974" id="Seg_11112" s="T973">tell-PTCP.PRS-3SG</ta>
            <ta e="T975" id="Seg_11113" s="T974">MOD</ta>
            <ta e="T976" id="Seg_11114" s="T975">Anisim</ta>
            <ta e="T977" id="Seg_11115" s="T976">old.man.[NOM]</ta>
            <ta e="T978" id="Seg_11116" s="T977">there.is</ta>
            <ta e="T979" id="Seg_11117" s="T978">tale-AG.[NOM]</ta>
            <ta e="T980" id="Seg_11118" s="T979">also</ta>
            <ta e="T991" id="Seg_11119" s="T990">what</ta>
            <ta e="T992" id="Seg_11120" s="T991">know-FUT.[3SG]=Q</ta>
            <ta e="T993" id="Seg_11121" s="T992">and</ta>
            <ta e="T994" id="Seg_11122" s="T993">that</ta>
            <ta e="T995" id="Seg_11123" s="T994">3PL-ACC</ta>
            <ta e="T996" id="Seg_11124" s="T995">tale.[NOM]</ta>
            <ta e="T997" id="Seg_11125" s="T996">get.saturated-PTCP.PRS-3SG.[3SG]</ta>
            <ta e="T998" id="Seg_11126" s="T997">Q</ta>
            <ta e="T999" id="Seg_11127" s="T998">3PL-DAT/LOC</ta>
            <ta e="T1000" id="Seg_11128" s="T999">what.[NOM]</ta>
            <ta e="T1001" id="Seg_11129" s="T1000">know</ta>
            <ta e="T1002" id="Seg_11130" s="T1001">often-often</ta>
            <ta e="T1003" id="Seg_11131" s="T1002">now</ta>
            <ta e="T1004" id="Seg_11132" s="T1003">tale-VBZ-EP-RECP/COLL-NEG-1PL</ta>
            <ta e="T1005" id="Seg_11133" s="T1004">EMPH</ta>
            <ta e="T1006" id="Seg_11134" s="T1005">simply</ta>
            <ta e="T1007" id="Seg_11135" s="T1006">tell.[IMP.2SG]</ta>
            <ta e="T1008" id="Seg_11136" s="T1007">just</ta>
            <ta e="T1009" id="Seg_11137" s="T1008">say-TEMP-3PL</ta>
            <ta e="T1010" id="Seg_11138" s="T1009">tell-PRS-1SG</ta>
            <ta e="T1011" id="Seg_11139" s="T1010">need.to-TEMP-3PL</ta>
            <ta e="T1012" id="Seg_11140" s="T1011">tell-EP-CAUS-TEMP-3PL</ta>
            <ta e="T1013" id="Seg_11141" s="T1012">just</ta>
            <ta e="T1014" id="Seg_11142" s="T1013">tell-PRS-2SG</ta>
            <ta e="T1015" id="Seg_11143" s="T1014">EMPH</ta>
            <ta e="T1016" id="Seg_11144" s="T1015">so</ta>
            <ta e="T1017" id="Seg_11145" s="T1016">EMPH</ta>
            <ta e="T1018" id="Seg_11146" s="T1017">simply</ta>
            <ta e="T1019" id="Seg_11147" s="T1018">when</ta>
            <ta e="T1020" id="Seg_11148" s="T1019">NEG</ta>
            <ta e="T1021" id="Seg_11149" s="T1020">tale-VBZ-EP-RECP/COLL-NEG-1PL</ta>
            <ta e="T1022" id="Seg_11150" s="T1021">EMPH</ta>
            <ta e="T1042" id="Seg_11151" s="T1041">AFFIRM</ta>
            <ta e="T1054" id="Seg_11152" s="T1053">well</ta>
            <ta e="T1055" id="Seg_11153" s="T1054">different-PART</ta>
            <ta e="T1056" id="Seg_11154" s="T1055">be-EP-NEG.CVB</ta>
            <ta e="T1075" id="Seg_11155" s="T1074">child-1SG.[NOM]</ta>
            <ta e="T1076" id="Seg_11156" s="T1075">side-3SG-INSTR</ta>
            <ta e="T1077" id="Seg_11157" s="T1076">tiny</ta>
            <ta e="T1078" id="Seg_11158" s="T1077">verse.[NOM]</ta>
            <ta e="T1079" id="Seg_11159" s="T1078">leftover-3SG-ACC</ta>
            <ta e="T1080" id="Seg_11160" s="T1079">make-PST2-EP-1SG</ta>
            <ta e="T1081" id="Seg_11161" s="T1080">Aysuluu.[NOM]</ta>
            <ta e="T1082" id="Seg_11162" s="T1081">side-3SG-INSTR</ta>
            <ta e="T1083" id="Seg_11163" s="T1082">star-PL.[NOM]</ta>
            <ta e="T1084" id="Seg_11164" s="T1083">sky-DAT/LOC</ta>
            <ta e="T1085" id="Seg_11165" s="T1084">to.be.on.view-PRS-3PL</ta>
            <ta e="T1086" id="Seg_11166" s="T1085">sleep.[IMP.2SG]</ta>
            <ta e="T1087" id="Seg_11167" s="T1086">sleep.[IMP.2SG]</ta>
            <ta e="T1088" id="Seg_11168" s="T1087">Aysuluu.[NOM]</ta>
            <ta e="T1089" id="Seg_11169" s="T1088">time-1PL.[NOM]</ta>
            <ta e="T1090" id="Seg_11170" s="T1089">change-PST1-3SG</ta>
            <ta e="T1091" id="Seg_11171" s="T1090">strange</ta>
            <ta e="T1092" id="Seg_11172" s="T1091">different</ta>
            <ta e="T1093" id="Seg_11173" s="T1092">time.[NOM]</ta>
            <ta e="T1094" id="Seg_11174" s="T1093">come-PST1-3SG</ta>
            <ta e="T1095" id="Seg_11175" s="T1094">fast-INTNS-ADVZ</ta>
            <ta e="T1096" id="Seg_11176" s="T1095">grow-FUT.[IMP.2SG]</ta>
            <ta e="T1097" id="Seg_11177" s="T1096">thread-1SG-ACC</ta>
            <ta e="T1098" id="Seg_11178" s="T1097">needle-1SG-ACC</ta>
            <ta e="T1099" id="Seg_11179" s="T1098">hold-FUT.[IMP.2SG]</ta>
            <ta e="T1100" id="Seg_11180" s="T1099">1SG.[NOM]</ta>
            <ta e="T1101" id="Seg_11181" s="T1100">life-1SG-ACC</ta>
            <ta e="T1102" id="Seg_11182" s="T1101">life-VBZ-FUT.[IMP.2SG]</ta>
            <ta e="T1103" id="Seg_11183" s="T1102">human.being-DAT/LOC</ta>
            <ta e="T1104" id="Seg_11184" s="T1103">human.being.[NOM]</ta>
            <ta e="T1105" id="Seg_11185" s="T1104">say-EP-CAUS-FUT.[IMP.2SG]</ta>
            <ta e="T1106" id="Seg_11186" s="T1105">child-DIM-EP-1SG.[NOM]</ta>
            <ta e="T1107" id="Seg_11187" s="T1106">child-3SG-ACC</ta>
            <ta e="T1108" id="Seg_11188" s="T1107">cradle-3SG-DAT/LOC</ta>
            <ta e="T1109" id="Seg_11189" s="T1108">swing-PRS-1SG</ta>
            <ta e="T1110" id="Seg_11190" s="T1109">place.beneath-3SG-DAT/LOC</ta>
            <ta e="T1111" id="Seg_11191" s="T1110">sit-PRS-1SG</ta>
            <ta e="T1112" id="Seg_11192" s="T1111">silently</ta>
            <ta e="T1113" id="Seg_11193" s="T1112">sing-EP-MED-PST1-1SG</ta>
         </annotation>
         <annotation name="gg" tierref="gg-UkET">
            <ta e="T23" id="Seg_11194" s="T21">1SG.[NOM]</ta>
            <ta e="T25" id="Seg_11195" s="T23">und</ta>
            <ta e="T26" id="Seg_11196" s="T25">selbst-1SG.[NOM]</ta>
            <ta e="T27" id="Seg_11197" s="T26">geboren.werden-PTCP.PST</ta>
            <ta e="T28" id="Seg_11198" s="T27">Erde-EP-1SG.[NOM]</ta>
            <ta e="T29" id="Seg_11199" s="T28">dieses.[NOM]</ta>
            <ta e="T30" id="Seg_11200" s="T29">zu</ta>
            <ta e="T31" id="Seg_11201" s="T30">sein-PST1-3SG</ta>
            <ta e="T32" id="Seg_11202" s="T31">Norden.[NOM]</ta>
            <ta e="T33" id="Seg_11203" s="T32">zu</ta>
            <ta e="T34" id="Seg_11204" s="T33">jenes.[NOM]</ta>
            <ta e="T35" id="Seg_11205" s="T34">zu</ta>
            <ta e="T37" id="Seg_11206" s="T36">wer.[NOM]</ta>
            <ta e="T38" id="Seg_11207" s="T37">zu</ta>
            <ta e="T39" id="Seg_11208" s="T38">äh</ta>
            <ta e="T40" id="Seg_11209" s="T39">dann</ta>
            <ta e="T41" id="Seg_11210" s="T40">Novorybnoe.[NOM]</ta>
            <ta e="T42" id="Seg_11211" s="T41">am.anderen.Ufer-2PL-DAT/LOC</ta>
            <ta e="T43" id="Seg_11212" s="T42">dieses.[NOM]</ta>
            <ta e="T44" id="Seg_11213" s="T43">Norden.[NOM]</ta>
            <ta e="T45" id="Seg_11214" s="T44">zu</ta>
            <ta e="T46" id="Seg_11215" s="T45">geboren.werden-PST2-EP-1SG</ta>
            <ta e="T47" id="Seg_11216" s="T46">dann</ta>
            <ta e="T48" id="Seg_11217" s="T47">Ehemann-DAT/LOC</ta>
            <ta e="T49" id="Seg_11218" s="T48">gehen-CVB.SEQ-1SG</ta>
            <ta e="T50" id="Seg_11219" s="T49">Süden.[NOM]</ta>
            <ta e="T51" id="Seg_11220" s="T50">zu</ta>
            <ta e="T52" id="Seg_11221" s="T51">Erde-VBZ-PST2-EP-1SG</ta>
            <ta e="T53" id="Seg_11222" s="T52">Kind-PL-EP-1SG.[NOM]</ta>
            <ta e="T54" id="Seg_11223" s="T53">EMPH</ta>
            <ta e="T55" id="Seg_11224" s="T54">was.[NOM]</ta>
            <ta e="T56" id="Seg_11225" s="T55">wie</ta>
            <ta e="T57" id="Seg_11226" s="T56">erzählen-CVB.SEQ</ta>
            <ta e="T58" id="Seg_11227" s="T57">EMPH</ta>
            <ta e="T59" id="Seg_11228" s="T58">was-ACC</ta>
            <ta e="T60" id="Seg_11229" s="T59">erzählen-FUT-1SG=Q</ta>
            <ta e="T61" id="Seg_11230" s="T60">eins</ta>
            <ta e="T62" id="Seg_11231" s="T61">Kind-1SG.[NOM]</ta>
            <ta e="T63" id="Seg_11232" s="T62">dieses.[NOM]</ta>
            <ta e="T64" id="Seg_11233" s="T63">Schule-DAT/LOC</ta>
            <ta e="T65" id="Seg_11234" s="T64">arbeiten-PRS.[3SG]</ta>
            <ta e="T66" id="Seg_11235" s="T65">eins</ta>
            <ta e="T67" id="Seg_11236" s="T66">Tochter-EP-1SG.[NOM]</ta>
            <ta e="T68" id="Seg_11237" s="T67">Direktor-VBZ-PRS.[3SG]</ta>
            <ta e="T69" id="Seg_11238" s="T68">eins</ta>
            <ta e="T70" id="Seg_11239" s="T69">äh</ta>
            <ta e="T71" id="Seg_11240" s="T70">eins</ta>
            <ta e="T72" id="Seg_11241" s="T71">Kind-1SG.[NOM]</ta>
            <ta e="T73" id="Seg_11242" s="T72">dieses.[NOM]</ta>
            <ta e="T74" id="Seg_11243" s="T73">Chatanga-DAT/LOC</ta>
            <ta e="T76" id="Seg_11244" s="T75">meist</ta>
            <ta e="T77" id="Seg_11245" s="T76">groß.[NOM]</ta>
            <ta e="T78" id="Seg_11246" s="T77">Tochter-EP-1SG.[NOM]</ta>
            <ta e="T79" id="Seg_11247" s="T78">Chatanga-DAT/LOC</ta>
            <ta e="T80" id="Seg_11248" s="T79">Schule-DAT/LOC</ta>
            <ta e="T81" id="Seg_11249" s="T80">Doktor-VBZ-PRS.[3SG]</ta>
            <ta e="T82" id="Seg_11250" s="T81">Nina</ta>
            <ta e="T84" id="Seg_11251" s="T83">jenes</ta>
            <ta e="T85" id="Seg_11252" s="T84">groß</ta>
            <ta e="T86" id="Seg_11253" s="T85">Tochter-EP-1SG.[NOM]</ta>
            <ta e="T87" id="Seg_11254" s="T86">Name-3SG.[NOM]</ta>
            <ta e="T88" id="Seg_11255" s="T87">klein.[NOM]</ta>
            <ta e="T89" id="Seg_11256" s="T88">jenes-3SG-1SG.[NOM]</ta>
            <ta e="T90" id="Seg_11257" s="T89">Unterteil-EP-3SG.[NOM]</ta>
            <ta e="T91" id="Seg_11258" s="T90">Katja</ta>
            <ta e="T92" id="Seg_11259" s="T91">jenes</ta>
            <ta e="T93" id="Seg_11260" s="T92">Schule-DAT/LOC</ta>
            <ta e="T94" id="Seg_11261" s="T93">Direktor-VBZ-PRS.[3SG]</ta>
            <ta e="T95" id="Seg_11262" s="T94">eins</ta>
            <ta e="T96" id="Seg_11263" s="T95">Tochter-EP-1SG.[NOM]</ta>
            <ta e="T97" id="Seg_11264" s="T96">Kindergarten-DAT/LOC</ta>
            <ta e="T98" id="Seg_11265" s="T97">arbeiten-PTCP.HAB</ta>
            <ta e="T99" id="Seg_11266" s="T98">sein-PST1-3SG</ta>
            <ta e="T100" id="Seg_11267" s="T99">jenes-3SG-1SG.[NOM]</ta>
            <ta e="T101" id="Seg_11268" s="T100">äh</ta>
            <ta e="T102" id="Seg_11269" s="T101">dieses</ta>
            <ta e="T103" id="Seg_11270" s="T102">Kindergarten-3PL.[NOM]</ta>
            <ta e="T104" id="Seg_11271" s="T103">was-3PL.[NOM]</ta>
            <ta e="T105" id="Seg_11272" s="T104">brechen-CVB.SEQ</ta>
            <ta e="T106" id="Seg_11273" s="T105">besonders</ta>
            <ta e="T107" id="Seg_11274" s="T106">EMPH</ta>
            <ta e="T108" id="Seg_11275" s="T107">Kind-VBZ-CVB.SEQ</ta>
            <ta e="T109" id="Seg_11276" s="T108">und.so.weiter-CVB.SEQ</ta>
            <ta e="T110" id="Seg_11277" s="T109">jetzt</ta>
            <ta e="T111" id="Seg_11278" s="T110">krank.sein-CVB.SEQ</ta>
            <ta e="T112" id="Seg_11279" s="T111">Dudinka.[NOM]</ta>
            <ta e="T113" id="Seg_11280" s="T112">zu</ta>
            <ta e="T114" id="Seg_11281" s="T113">sich.befinden-PRS.[3SG]</ta>
            <ta e="T115" id="Seg_11282" s="T114">dieses</ta>
            <ta e="T116" id="Seg_11283" s="T115">äh</ta>
            <ta e="T117" id="Seg_11284" s="T116">Norilsk-DAT/LOC</ta>
            <ta e="T118" id="Seg_11285" s="T117">heilen-EP-CAUS-CVB.SIM</ta>
            <ta e="T119" id="Seg_11286" s="T118">liegen-PRS.[3SG]</ta>
            <ta e="T120" id="Seg_11287" s="T119">jetzt</ta>
            <ta e="T121" id="Seg_11288" s="T120">dann</ta>
            <ta e="T122" id="Seg_11289" s="T121">eins</ta>
            <ta e="T123" id="Seg_11290" s="T122">Sohn-EP-1SG.[NOM]</ta>
            <ta e="T124" id="Seg_11291" s="T123">Jäger.[NOM]</ta>
            <ta e="T125" id="Seg_11292" s="T124">sein-PST1-3SG</ta>
            <ta e="T126" id="Seg_11293" s="T125">eins</ta>
            <ta e="T127" id="Seg_11294" s="T126">Junge-EP-1SG.[NOM]</ta>
            <ta e="T128" id="Seg_11295" s="T127">dieses.[NOM]</ta>
            <ta e="T129" id="Seg_11296" s="T128">Flugzeug-PL-ACC</ta>
            <ta e="T131" id="Seg_11297" s="T130">verschieden-3PL-ACC</ta>
            <ta e="T132" id="Seg_11298" s="T131">schicken</ta>
            <ta e="T133" id="Seg_11299" s="T132">was-VBZ-PRS.[3SG]</ta>
            <ta e="T134" id="Seg_11300" s="T133">machen-PRS.[3SG]</ta>
            <ta e="T135" id="Seg_11301" s="T134">Flugzeug-PL-ACC</ta>
            <ta e="T136" id="Seg_11302" s="T135">laden-PRS.[3SG]</ta>
            <ta e="T137" id="Seg_11303" s="T136">fliegen-EP-CAUS-PRS.[3SG]</ta>
            <ta e="T138" id="Seg_11304" s="T137">dort</ta>
            <ta e="T139" id="Seg_11305" s="T138">es.gibt</ta>
            <ta e="T140" id="Seg_11306" s="T139">eins</ta>
            <ta e="T141" id="Seg_11307" s="T140">Tochter-EP-1SG.[NOM]</ta>
            <ta e="T143" id="Seg_11308" s="T142">was-ACC</ta>
            <ta e="T144" id="Seg_11309" s="T143">aufhören-PTCP.PST</ta>
            <ta e="T145" id="Seg_11310" s="T144">sein-PST1-3SG</ta>
            <ta e="T146" id="Seg_11311" s="T145">Kindergarten.[NOM]</ta>
            <ta e="T147" id="Seg_11312" s="T146">Ausbildung-3SG-ACC</ta>
            <ta e="T148" id="Seg_11313" s="T147">jetzt</ta>
            <ta e="T149" id="Seg_11314" s="T148">selbst-1SG.[NOM]</ta>
            <ta e="T150" id="Seg_11315" s="T149">altern-CVB.SEQ-1SG</ta>
            <ta e="T151" id="Seg_11316" s="T150">jenes-3SG-1SG-ACC</ta>
            <ta e="T152" id="Seg_11317" s="T151">selbst-1SG-DAT/LOC</ta>
            <ta e="T153" id="Seg_11318" s="T152">arbeiten-EP-CAUS-CVB.PURP-1SG</ta>
            <ta e="T154" id="Seg_11319" s="T153">Tundra-DAT/LOC</ta>
            <ta e="T155" id="Seg_11320" s="T154">halten-PRS-1SG</ta>
            <ta e="T156" id="Seg_11321" s="T155">alter.Mann-EP-1SG.[NOM]</ta>
            <ta e="T157" id="Seg_11322" s="T156">früher</ta>
            <ta e="T158" id="Seg_11323" s="T157">Jäger.[NOM]</ta>
            <ta e="T159" id="Seg_11324" s="T158">sein-PST1-3SG</ta>
            <ta e="T160" id="Seg_11325" s="T159">doch</ta>
            <ta e="T162" id="Seg_11326" s="T161">Mensch-ABL</ta>
            <ta e="T163" id="Seg_11327" s="T162">äh</ta>
            <ta e="T164" id="Seg_11328" s="T163">Novorybnoe-DAT/LOC</ta>
            <ta e="T165" id="Seg_11329" s="T164">dann</ta>
            <ta e="T166" id="Seg_11330" s="T165">Sowchose-DAT/LOC</ta>
            <ta e="T167" id="Seg_11331" s="T166">führend-3SG-INSTR</ta>
            <ta e="T168" id="Seg_11332" s="T167">jagen-PTCP.HAB</ta>
            <ta e="T169" id="Seg_11333" s="T168">sein-PST1-3SG</ta>
            <ta e="T170" id="Seg_11334" s="T169">jetzt</ta>
            <ta e="T171" id="Seg_11335" s="T170">krank.sein-CVB.SEQ</ta>
            <ta e="T172" id="Seg_11336" s="T171">Hand-3SG.[NOM]</ta>
            <ta e="T173" id="Seg_11337" s="T172">krank.sein-CVB.SEQ</ta>
            <ta e="T174" id="Seg_11338" s="T173">jenes</ta>
            <ta e="T175" id="Seg_11339" s="T174">Jagd-EP-ABL</ta>
            <ta e="T176" id="Seg_11340" s="T175">EMPH</ta>
            <ta e="T177" id="Seg_11341" s="T176">hinausgehen-PST1-3SG</ta>
            <ta e="T178" id="Seg_11342" s="T177">jenes-DAT/LOC</ta>
            <ta e="T179" id="Seg_11343" s="T178">zusätzlich-VBZ-CVB.SEQ</ta>
            <ta e="T180" id="Seg_11344" s="T179">jetzt</ta>
            <ta e="T181" id="Seg_11345" s="T180">lesen-CAUS-CVB.SEQ</ta>
            <ta e="T182" id="Seg_11346" s="T181">Rente-DAT/LOC</ta>
            <ta e="T183" id="Seg_11347" s="T182">hineingehen-PST1-3SG</ta>
            <ta e="T184" id="Seg_11348" s="T183">dieses</ta>
            <ta e="T185" id="Seg_11349" s="T184">jetzt</ta>
            <ta e="T186" id="Seg_11350" s="T185">dieses</ta>
            <ta e="T188" id="Seg_11351" s="T186">Jahr-PL-DAT/LOC</ta>
            <ta e="T190" id="Seg_11352" s="T188">hineingehen-PST2-3SG</ta>
            <ta e="T203" id="Seg_11353" s="T202">AFFIRM</ta>
            <ta e="T204" id="Seg_11354" s="T203">jetzt</ta>
            <ta e="T205" id="Seg_11355" s="T204">dieses</ta>
            <ta e="T206" id="Seg_11356" s="T205">Kind-PL-EP-1SG.[NOM]</ta>
            <ta e="T207" id="Seg_11357" s="T206">Kind-1SG.[NOM]</ta>
            <ta e="T208" id="Seg_11358" s="T207">Kind-PL-3SG-ACC</ta>
            <ta e="T209" id="Seg_11359" s="T208">sich.kümmern-CVB.SEQ-1SG</ta>
            <ta e="T210" id="Seg_11360" s="T209">jenes</ta>
            <ta e="T211" id="Seg_11361" s="T210">Arzneimittel-DAT/LOC</ta>
            <ta e="T212" id="Seg_11362" s="T211">kommen-PTCP.HAB</ta>
            <ta e="T213" id="Seg_11363" s="T212">Kind-1SG.[NOM]</ta>
            <ta e="T214" id="Seg_11364" s="T213">eigen-3SG</ta>
            <ta e="T215" id="Seg_11365" s="T214">drei</ta>
            <ta e="T216" id="Seg_11366" s="T215">Kind-PROPR.[NOM]</ta>
            <ta e="T217" id="Seg_11367" s="T216">MOD</ta>
            <ta e="T218" id="Seg_11368" s="T217">jenes-3SG-PL-EP-1SG.[NOM]</ta>
            <ta e="T219" id="Seg_11369" s="T218">Kind-3SG-ACC</ta>
            <ta e="T220" id="Seg_11370" s="T219">sich.kümmern-CVB.SIM</ta>
            <ta e="T221" id="Seg_11371" s="T220">liegen-PTCP.PRS</ta>
            <ta e="T222" id="Seg_11372" s="T221">sein-PST1-1SG</ta>
            <ta e="T223" id="Seg_11373" s="T222">EMPH</ta>
            <ta e="T224" id="Seg_11374" s="T223">jetzt</ta>
            <ta e="T225" id="Seg_11375" s="T224">aber</ta>
            <ta e="T226" id="Seg_11376" s="T225">dieses</ta>
            <ta e="T227" id="Seg_11377" s="T226">Kind-1SG.[NOM]</ta>
            <ta e="T228" id="Seg_11378" s="T227">jenes</ta>
            <ta e="T229" id="Seg_11379" s="T228">Mascha-1SG.[NOM]</ta>
            <ta e="T230" id="Seg_11380" s="T229">kommen-CVB.SEQ</ta>
            <ta e="T231" id="Seg_11381" s="T230">kommen-PST1-1SG</ta>
            <ta e="T232" id="Seg_11382" s="T231">jenes</ta>
            <ta e="T233" id="Seg_11383" s="T232">Tundra-ABL</ta>
            <ta e="T234" id="Seg_11384" s="T233">Tundra-DAT/LOC</ta>
            <ta e="T235" id="Seg_11385" s="T234">halten-CVB.SIM</ta>
            <ta e="T236" id="Seg_11386" s="T235">liegen-PRS-1SG</ta>
            <ta e="T237" id="Seg_11387" s="T236">MOD</ta>
            <ta e="T238" id="Seg_11388" s="T237">jenes</ta>
            <ta e="T239" id="Seg_11389" s="T238">drei</ta>
            <ta e="T240" id="Seg_11390" s="T239">Kind-1SG-ACC</ta>
            <ta e="T241" id="Seg_11391" s="T240">jenes</ta>
            <ta e="T242" id="Seg_11392" s="T241">Mama-3SG.[NOM]</ta>
            <ta e="T243" id="Seg_11393" s="T242">dieses.[NOM]</ta>
            <ta e="T244" id="Seg_11394" s="T243">zu</ta>
            <ta e="T245" id="Seg_11395" s="T244">es.gibt</ta>
            <ta e="T246" id="Seg_11396" s="T245">Vater-3PL.[NOM]</ta>
            <ta e="T247" id="Seg_11397" s="T246">weggehen-PST2-3SG</ta>
            <ta e="T248" id="Seg_11398" s="T247">MOD</ta>
            <ta e="T249" id="Seg_11399" s="T248">jenes.[NOM]</ta>
            <ta e="T250" id="Seg_11400" s="T249">Nenze.[NOM]</ta>
            <ta e="T251" id="Seg_11401" s="T250">sein-PST1-3SG</ta>
            <ta e="T252" id="Seg_11402" s="T251">jenes-3SG-3SG.[NOM]</ta>
            <ta e="T253" id="Seg_11403" s="T252">weggehen-PST2-3SG</ta>
            <ta e="T254" id="Seg_11404" s="T253">jenes.[NOM]</ta>
            <ta e="T255" id="Seg_11405" s="T254">wegen</ta>
            <ta e="T256" id="Seg_11406" s="T255">jenes</ta>
            <ta e="T257" id="Seg_11407" s="T256">woher</ta>
            <ta e="T258" id="Seg_11408" s="T257">eins-ABL</ta>
            <ta e="T259" id="Seg_11409" s="T258">eins</ta>
            <ta e="T260" id="Seg_11410" s="T259">Kind.[NOM]</ta>
            <ta e="T261" id="Seg_11411" s="T260">MOD</ta>
            <ta e="T262" id="Seg_11412" s="T261">Kind-PL-EP-1SG.[NOM]</ta>
            <ta e="T263" id="Seg_11413" s="T262">Kind-3PL.[NOM]</ta>
            <ta e="T264" id="Seg_11414" s="T263">wann</ta>
            <ta e="T265" id="Seg_11415" s="T264">NEG</ta>
            <ta e="T266" id="Seg_11416" s="T265">weggehen-NEG-3PL</ta>
            <ta e="T267" id="Seg_11417" s="T266">EMPH</ta>
            <ta e="T268" id="Seg_11418" s="T267">im.Sommer</ta>
            <ta e="T269" id="Seg_11419" s="T268">Sommer-PL-ACC</ta>
            <ta e="T270" id="Seg_11420" s="T269">Winter-PL-ACC</ta>
            <ta e="T271" id="Seg_11421" s="T270">durch</ta>
            <ta e="T272" id="Seg_11422" s="T271">Kind-PROPR.[NOM]</ta>
            <ta e="T273" id="Seg_11423" s="T272">sein-HAB-1SG</ta>
            <ta e="T274" id="Seg_11424" s="T273">Enkel-PL-EP-1SG.[NOM]</ta>
            <ta e="T275" id="Seg_11425" s="T274">immer</ta>
            <ta e="T276" id="Seg_11426" s="T275">es.gibt</ta>
            <ta e="T277" id="Seg_11427" s="T276">sein-PRS-3PL</ta>
            <ta e="T300" id="Seg_11428" s="T299">AFFIRM</ta>
            <ta e="T301" id="Seg_11429" s="T300">erzählen-HAB-1PL</ta>
            <ta e="T302" id="Seg_11430" s="T301">verschieden-DIM-ACC</ta>
            <ta e="T303" id="Seg_11431" s="T302">EMPH</ta>
            <ta e="T304" id="Seg_11432" s="T303">jetzt</ta>
            <ta e="T305" id="Seg_11433" s="T304">groß-ADVZ</ta>
            <ta e="T306" id="Seg_11434" s="T305">NEG</ta>
            <ta e="T307" id="Seg_11435" s="T306">verstehen-EP-MED-CVB.SIM</ta>
            <ta e="T308" id="Seg_11436" s="T307">noch.nicht-3PL</ta>
            <ta e="T309" id="Seg_11437" s="T308">EVID</ta>
            <ta e="T310" id="Seg_11438" s="T309">groß</ta>
            <ta e="T311" id="Seg_11439" s="T310">Tochter-EP-1SG.[NOM]</ta>
            <ta e="T312" id="Seg_11440" s="T311">nur</ta>
            <ta e="T313" id="Seg_11441" s="T312">Kind-PL-3SG.[NOM]</ta>
            <ta e="T314" id="Seg_11442" s="T313">Schule-DAT/LOC</ta>
            <ta e="T315" id="Seg_11443" s="T314">gehen-PST2-3PL</ta>
            <ta e="T316" id="Seg_11444" s="T315">klein.[NOM]</ta>
            <ta e="T317" id="Seg_11445" s="T316">Kind-PL-EP-1SG.[NOM]</ta>
            <ta e="T318" id="Seg_11446" s="T317">Kind-3PL.[NOM]</ta>
            <ta e="T319" id="Seg_11447" s="T318">hineingehen-CVB.SIM</ta>
            <ta e="T320" id="Seg_11448" s="T319">noch.nicht-3PL</ta>
            <ta e="T321" id="Seg_11449" s="T320">Schule-DAT/LOC</ta>
            <ta e="T322" id="Seg_11450" s="T321">jenes-PL-DAT/LOC</ta>
            <ta e="T323" id="Seg_11451" s="T322">jenes</ta>
            <ta e="T324" id="Seg_11452" s="T323">verschieden-DIM-ACC</ta>
            <ta e="T325" id="Seg_11453" s="T324">erzählen-PRS-1SG</ta>
            <ta e="T326" id="Seg_11454" s="T325">EMPH</ta>
            <ta e="T327" id="Seg_11455" s="T326">verschieden-ACC-verschieden-ACC</ta>
            <ta e="T328" id="Seg_11456" s="T327">Großmutter.[NOM]</ta>
            <ta e="T329" id="Seg_11457" s="T328">sagen-CVB.SEQ</ta>
            <ta e="T330" id="Seg_11458" s="T329">sich.drehen-CVB.SIM</ta>
            <ta e="T331" id="Seg_11459" s="T330">gehen-HAB-3PL</ta>
            <ta e="T332" id="Seg_11460" s="T331">EMPH</ta>
            <ta e="T333" id="Seg_11461" s="T332">jenes-3SG-2SG-ACC</ta>
            <ta e="T334" id="Seg_11462" s="T333">erzählen.[IMP.2SG]</ta>
            <ta e="T335" id="Seg_11463" s="T334">jenes-ACC</ta>
            <ta e="T336" id="Seg_11464" s="T335">Märchen-VBZ.[IMP.2SG]</ta>
            <ta e="T337" id="Seg_11465" s="T336">dieses-ACC</ta>
            <ta e="T338" id="Seg_11466" s="T337">erzählen.[IMP.2SG]</ta>
            <ta e="T339" id="Seg_11467" s="T338">dieses-ACC</ta>
            <ta e="T340" id="Seg_11468" s="T339">singen.[IMP.2SG]</ta>
            <ta e="T341" id="Seg_11469" s="T340">jenes-PL-ACC</ta>
            <ta e="T342" id="Seg_11470" s="T341">unterhalten-CVB.PURP-2SG</ta>
            <ta e="T343" id="Seg_11471" s="T342">nun</ta>
            <ta e="T344" id="Seg_11472" s="T343">singen-EP-MED-EP-IMP.2PL</ta>
            <ta e="T345" id="Seg_11473" s="T344">erzählen-EP-CAUS-EP-IMP.2PL</ta>
            <ta e="T346" id="Seg_11474" s="T345">doch</ta>
            <ta e="T347" id="Seg_11475" s="T346">dickflüssig.[NOM]</ta>
            <ta e="T348" id="Seg_11476" s="T347">MOD</ta>
            <ta e="T356" id="Seg_11477" s="T355">na</ta>
            <ta e="T357" id="Seg_11478" s="T356">meine</ta>
            <ta e="T358" id="Seg_11479" s="T357">Vorfahre-PL-EP-1SG.[NOM]</ta>
            <ta e="T359" id="Seg_11480" s="T358">jeder-3SG.[NOM]</ta>
            <ta e="T360" id="Seg_11481" s="T359">Märchen-AG.[NOM]</ta>
            <ta e="T361" id="Seg_11482" s="T360">sein-PST1-3PL</ta>
            <ta e="T362" id="Seg_11483" s="T361">singen-PTCP.HAB</ta>
            <ta e="T363" id="Seg_11484" s="T362">und</ta>
            <ta e="T364" id="Seg_11485" s="T363">sein-PST1-3PL</ta>
            <ta e="T365" id="Seg_11486" s="T364">Lied.[NOM]</ta>
            <ta e="T366" id="Seg_11487" s="T365">singen-EP</ta>
            <ta e="T367" id="Seg_11488" s="T366">singen-EP-MED-CVB.SIM</ta>
            <ta e="T368" id="Seg_11489" s="T367">Lied-PROPR.[NOM]</ta>
            <ta e="T369" id="Seg_11490" s="T368">und</ta>
            <ta e="T370" id="Seg_11491" s="T369">Märchen-ACC</ta>
            <ta e="T371" id="Seg_11492" s="T370">erzählen-HAB.[3SG]</ta>
            <ta e="T372" id="Seg_11493" s="T371">einfach</ta>
            <ta e="T373" id="Seg_11494" s="T372">und</ta>
            <ta e="T374" id="Seg_11495" s="T373">Märchen-ACC</ta>
            <ta e="T375" id="Seg_11496" s="T374">erzählen-EP</ta>
            <ta e="T376" id="Seg_11497" s="T375">dann</ta>
            <ta e="T377" id="Seg_11498" s="T376">vor.langer.Zeit-ADJZ</ta>
            <ta e="T378" id="Seg_11499" s="T377">Zeit-DAT/LOC</ta>
            <ta e="T379" id="Seg_11500" s="T378">aber</ta>
            <ta e="T380" id="Seg_11501" s="T379">Kino-ACC</ta>
            <ta e="T381" id="Seg_11502" s="T380">sehen-PRS-2SG</ta>
            <ta e="T382" id="Seg_11503" s="T381">Q</ta>
            <ta e="T383" id="Seg_11504" s="T382">dann</ta>
            <ta e="T384" id="Seg_11505" s="T383">EMPH</ta>
            <ta e="T385" id="Seg_11506" s="T384">Spidola-ACC</ta>
            <ta e="T386" id="Seg_11507" s="T385">gehört.werden-PRS-2SG</ta>
            <ta e="T387" id="Seg_11508" s="T386">MOD</ta>
            <ta e="T388" id="Seg_11509" s="T387">Holz.[NOM]</ta>
            <ta e="T389" id="Seg_11510" s="T388">Spielzeug-EP-INSTR</ta>
            <ta e="T390" id="Seg_11511" s="T389">spielen-PRS-2SG</ta>
            <ta e="T391" id="Seg_11512" s="T390">Vögelchen-PL-EP-INSTR</ta>
            <ta e="T392" id="Seg_11513" s="T391">dieses.[NOM]</ta>
            <ta e="T393" id="Seg_11514" s="T392">Schlitten.[NOM]</ta>
            <ta e="T394" id="Seg_11515" s="T393">machen-CVB.SIM</ta>
            <ta e="T395" id="Seg_11516" s="T394">machen-PRS-2SG</ta>
            <ta e="T396" id="Seg_11517" s="T395">nach.draußen</ta>
            <ta e="T397" id="Seg_11518" s="T396">hinausgehen-TEMP-2SG</ta>
            <ta e="T398" id="Seg_11519" s="T397">Horn.[NOM]</ta>
            <ta e="T399" id="Seg_11520" s="T398">werfen-CVB.SEQ</ta>
            <ta e="T400" id="Seg_11521" s="T399">Horn-EP-INSTR</ta>
            <ta e="T401" id="Seg_11522" s="T400">spielen-FUT-2SG</ta>
            <ta e="T402" id="Seg_11523" s="T401">Junge.[NOM]</ta>
            <ta e="T403" id="Seg_11524" s="T402">Kind.[NOM]</ta>
            <ta e="T404" id="Seg_11525" s="T403">MOD</ta>
            <ta e="T405" id="Seg_11526" s="T404">Fangschlinge-VBZ-CVB.SIM</ta>
            <ta e="T406" id="Seg_11527" s="T405">spielen-FUT-3SG</ta>
            <ta e="T407" id="Seg_11528" s="T406">im.Sommer</ta>
            <ta e="T408" id="Seg_11529" s="T407">wie</ta>
            <ta e="T409" id="Seg_11530" s="T408">gehen-PTCP.PRS-2SG-ACC</ta>
            <ta e="T410" id="Seg_11531" s="T409">Stein-ACC</ta>
            <ta e="T411" id="Seg_11532" s="T410">sammeln-EP-MED-CVB.SEQ-2SG</ta>
            <ta e="T412" id="Seg_11533" s="T411">Karawane.[NOM]</ta>
            <ta e="T413" id="Seg_11534" s="T412">spielen-FUT-2SG</ta>
            <ta e="T414" id="Seg_11535" s="T413">jenes.EMPH.[NOM]</ta>
            <ta e="T415" id="Seg_11536" s="T414">wie-INTNS</ta>
            <ta e="T416" id="Seg_11537" s="T415">Tag-ACC</ta>
            <ta e="T417" id="Seg_11538" s="T416">schneiden-PRS-2SG</ta>
            <ta e="T418" id="Seg_11539" s="T417">MOD</ta>
            <ta e="T419" id="Seg_11540" s="T418">vor.langer.Zeit-ADJZ</ta>
            <ta e="T420" id="Seg_11541" s="T419">Kind.[NOM]</ta>
            <ta e="T421" id="Seg_11542" s="T420">alte.Zeiten-SIM</ta>
            <ta e="T422" id="Seg_11543" s="T421">spielen-PRS.[3SG]</ta>
            <ta e="T423" id="Seg_11544" s="T422">EMPH</ta>
            <ta e="T424" id="Seg_11545" s="T423">jenes</ta>
            <ta e="T425" id="Seg_11546" s="T424">schneiden-NMNZ-3SG-INSTR</ta>
            <ta e="T426" id="Seg_11547" s="T425">jetzt</ta>
            <ta e="T427" id="Seg_11548" s="T426">vor.langer.Zeit-ADJZ</ta>
            <ta e="T428" id="Seg_11549" s="T427">Spielzeug-ACC</ta>
            <ta e="T429" id="Seg_11550" s="T428">EMPH</ta>
            <ta e="T430" id="Seg_11551" s="T429">jetzt</ta>
            <ta e="T431" id="Seg_11552" s="T430">EMPH</ta>
            <ta e="T432" id="Seg_11553" s="T431">sehen-PTCP.FUT-2PL-ACC</ta>
            <ta e="T433" id="Seg_11554" s="T432">EMPH</ta>
            <ta e="T434" id="Seg_11555" s="T433">rund.und.dick</ta>
            <ta e="T435" id="Seg_11556" s="T434">Spielzeug-ACC</ta>
            <ta e="T436" id="Seg_11557" s="T435">bringen-PST2-EP-1SG</ta>
            <ta e="T437" id="Seg_11558" s="T436">MOD</ta>
            <ta e="T438" id="Seg_11559" s="T437">dieses</ta>
            <ta e="T439" id="Seg_11560" s="T438">Ort-DAT/LOC</ta>
            <ta e="T440" id="Seg_11561" s="T439">doch</ta>
            <ta e="T441" id="Seg_11562" s="T440">zeigen-FUT-1SG</ta>
            <ta e="T442" id="Seg_11563" s="T441">jenes</ta>
            <ta e="T443" id="Seg_11564" s="T442">spielen-NMNZ.[NOM]</ta>
            <ta e="T444" id="Seg_11565" s="T443">wie</ta>
            <ta e="T445" id="Seg_11566" s="T444">spielen-PTCP.PRS-3PL-ACC</ta>
            <ta e="T446" id="Seg_11567" s="T445">mancher</ta>
            <ta e="T447" id="Seg_11568" s="T446">Spielzeug-PL-ACC</ta>
            <ta e="T448" id="Seg_11569" s="T447">machen</ta>
            <ta e="T449" id="Seg_11570" s="T448">machen-CVB.SEQ</ta>
            <ta e="T450" id="Seg_11571" s="T449">bringen-PTCP.FUT-1SG-ACC</ta>
            <ta e="T451" id="Seg_11572" s="T450">Mensch-POSS</ta>
            <ta e="T452" id="Seg_11573" s="T451">NEG-1SG</ta>
            <ta e="T453" id="Seg_11574" s="T452">alter.Mann-EP-1SG.[NOM]</ta>
            <ta e="T454" id="Seg_11575" s="T453">jenes</ta>
            <ta e="T455" id="Seg_11576" s="T454">Krankenhaus-DAT/LOC</ta>
            <ta e="T456" id="Seg_11577" s="T455">liegen-PTCP.PRS</ta>
            <ta e="T457" id="Seg_11578" s="T456">sein-PST1-3SG</ta>
            <ta e="T458" id="Seg_11579" s="T457">MOD</ta>
            <ta e="T459" id="Seg_11580" s="T458">eins</ta>
            <ta e="T460" id="Seg_11581" s="T459">Sohn-EP-1SG.[NOM]</ta>
            <ta e="T461" id="Seg_11582" s="T460">doch</ta>
            <ta e="T462" id="Seg_11583" s="T461">krank.sein-CVB.SEQ</ta>
            <ta e="T463" id="Seg_11584" s="T462">Krankenhaus-ABL</ta>
            <ta e="T464" id="Seg_11585" s="T463">hinausgehen-CVB.ANT</ta>
            <ta e="T465" id="Seg_11586" s="T464">nun</ta>
            <ta e="T466" id="Seg_11587" s="T465">kommen-PST2-EP-1SG</ta>
            <ta e="T467" id="Seg_11588" s="T466">MOD</ta>
            <ta e="T468" id="Seg_11589" s="T467">jenes-3SG-1SG.[NOM]</ta>
            <ta e="T469" id="Seg_11590" s="T468">1SG.[NOM]</ta>
            <ta e="T470" id="Seg_11591" s="T469">Tundra-DAT/LOC</ta>
            <ta e="T471" id="Seg_11592" s="T470">es.gibt</ta>
            <ta e="T472" id="Seg_11593" s="T471">sein-PST1-3SG</ta>
            <ta e="T473" id="Seg_11594" s="T472">jenes-3SG-1SG.[NOM]</ta>
            <ta e="T474" id="Seg_11595" s="T473">Dorf-DAT/LOC</ta>
            <ta e="T475" id="Seg_11596" s="T474">hineingehen-CVB.SEQ</ta>
            <ta e="T476" id="Seg_11597" s="T475">krank.sein-EP-PST2-3SG</ta>
            <ta e="T477" id="Seg_11598" s="T476">dann</ta>
            <ta e="T478" id="Seg_11599" s="T477">später</ta>
            <ta e="T479" id="Seg_11600" s="T478">hinausgehen-EP-PST2-3SG</ta>
            <ta e="T480" id="Seg_11601" s="T479">kommen-PTCP.PRS-1PL.[NOM]</ta>
            <ta e="T481" id="Seg_11602" s="T480">EMPH</ta>
            <ta e="T482" id="Seg_11603" s="T481">jenes-ACC</ta>
            <ta e="T483" id="Seg_11604" s="T482">Schneesturm-PL.[NOM]</ta>
            <ta e="T484" id="Seg_11605" s="T483">was-PL.[NOM]</ta>
            <ta e="T485" id="Seg_11606" s="T484">EMPH</ta>
            <ta e="T486" id="Seg_11607" s="T485">stören-VBZ-CVB.SEQ</ta>
            <ta e="T487" id="Seg_11608" s="T486">EMPH-dann</ta>
            <ta e="T488" id="Seg_11609" s="T487">was.[NOM]</ta>
            <ta e="T489" id="Seg_11610" s="T488">viel</ta>
            <ta e="T490" id="Seg_11611" s="T489">Spielzeug.[NOM]</ta>
            <ta e="T491" id="Seg_11612" s="T490">bringen-PST2.NEG-EP-1SG</ta>
            <ta e="T492" id="Seg_11613" s="T491">früher</ta>
            <ta e="T493" id="Seg_11614" s="T492">Kind.[NOM]</ta>
            <ta e="T494" id="Seg_11615" s="T493">Spielzeug-3SG-ACC</ta>
            <ta e="T495" id="Seg_11616" s="T494">doch</ta>
            <ta e="T496" id="Seg_11617" s="T495">dann</ta>
            <ta e="T497" id="Seg_11618" s="T496">anders</ta>
            <ta e="T498" id="Seg_11619" s="T497">zeigen-NMNZ-DAT/LOC</ta>
            <ta e="T499" id="Seg_11620" s="T498">bringen-PTCP.FUT</ta>
            <ta e="T500" id="Seg_11621" s="T499">sein-PST1-1SG</ta>
            <ta e="T501" id="Seg_11622" s="T500">früher</ta>
            <ta e="T502" id="Seg_11623" s="T501">selbst-1SG.[NOM]</ta>
            <ta e="T503" id="Seg_11624" s="T502">spielen-CVB.SEQ</ta>
            <ta e="T504" id="Seg_11625" s="T503">entstehen-PST2-1SG</ta>
            <ta e="T505" id="Seg_11626" s="T504">nun</ta>
            <ta e="T506" id="Seg_11627" s="T505">dann</ta>
            <ta e="T518" id="Seg_11628" s="T517">doch</ta>
            <ta e="T519" id="Seg_11629" s="T518">Faden-EP-INSTR</ta>
            <ta e="T520" id="Seg_11630" s="T519">wickeln-PRS-1PL</ta>
            <ta e="T521" id="Seg_11631" s="T520">Loch-PROPR</ta>
            <ta e="T522" id="Seg_11632" s="T521">Holz-DAT/LOC</ta>
            <ta e="T523" id="Seg_11633" s="T522">Kopf-EP-INSTR</ta>
            <ta e="T524" id="Seg_11634" s="T523">quer</ta>
            <ta e="T525" id="Seg_11635" s="T524">stehen-CVB.SEQ</ta>
            <ta e="T526" id="Seg_11636" s="T525">sich.drehen-PRS.[3SG]</ta>
            <ta e="T527" id="Seg_11637" s="T526">jenes-ACC</ta>
            <ta e="T528" id="Seg_11638" s="T527">sehen-TEMP-2SG</ta>
            <ta e="T529" id="Seg_11639" s="T528">nur</ta>
            <ta e="T530" id="Seg_11640" s="T529">erfahren-FUT-2SG</ta>
            <ta e="T531" id="Seg_11641" s="T530">MOD</ta>
            <ta e="T532" id="Seg_11642" s="T531">wie</ta>
            <ta e="T533" id="Seg_11643" s="T532">jetzt</ta>
            <ta e="T534" id="Seg_11644" s="T533">erzählen-CVB.SEQ-1SG</ta>
            <ta e="T535" id="Seg_11645" s="T534">wie</ta>
            <ta e="T536" id="Seg_11646" s="T535">erzählen-FUT-1SG=Q</ta>
            <ta e="T537" id="Seg_11647" s="T536">zeigen-FUT-1SG</ta>
            <ta e="T538" id="Seg_11648" s="T537">jenes</ta>
            <ta e="T539" id="Seg_11649" s="T538">sehen-TEMP-2SG</ta>
            <ta e="T540" id="Seg_11650" s="T539">wissen-FUT.[IMP.2SG]</ta>
            <ta e="T559" id="Seg_11651" s="T558">AFFIRM</ta>
            <ta e="T560" id="Seg_11652" s="T559">wissen-PRS-1SG</ta>
            <ta e="T561" id="Seg_11653" s="T560">solch-PL-ACC</ta>
            <ta e="T562" id="Seg_11654" s="T561">solch</ta>
            <ta e="T563" id="Seg_11655" s="T562">solch-PART</ta>
            <ta e="T564" id="Seg_11656" s="T563">einfach</ta>
            <ta e="T565" id="Seg_11657" s="T564">beliebig.[NOM]</ta>
            <ta e="T566" id="Seg_11658" s="T565">Lied-PART</ta>
            <ta e="T567" id="Seg_11659" s="T566">singen-FUT-1SG</ta>
            <ta e="T568" id="Seg_11660" s="T567">Q</ta>
            <ta e="T569" id="Seg_11661" s="T568">beliebig-PART</ta>
            <ta e="T570" id="Seg_11662" s="T569">beliebig.[NOM]</ta>
            <ta e="T571" id="Seg_11663" s="T570">Lied-PART</ta>
            <ta e="T572" id="Seg_11664" s="T571">singen-FUT-1SG</ta>
            <ta e="T573" id="Seg_11665" s="T572">warte</ta>
            <ta e="T574" id="Seg_11666" s="T573">kommen-CVB.SIM</ta>
            <ta e="T575" id="Seg_11667" s="T574">ziehen-NEG.[3SG]</ta>
            <ta e="T576" id="Seg_11668" s="T575">dieses-3SG-2SG.[NOM]</ta>
            <ta e="T577" id="Seg_11669" s="T576">kommen-FUT</ta>
            <ta e="T578" id="Seg_11670" s="T577">kommen-PTCP.FUT-PART</ta>
            <ta e="T579" id="Seg_11671" s="T578">NEG</ta>
            <ta e="T580" id="Seg_11672" s="T579">sein-HAB.[3SG]</ta>
            <ta e="T581" id="Seg_11673" s="T580">Melodie-3PL-ACC</ta>
            <ta e="T582" id="Seg_11674" s="T581">erinnern-NEG-1SG</ta>
            <ta e="T583" id="Seg_11675" s="T582">vergessen-EP-PST2-1SG</ta>
            <ta e="T584" id="Seg_11676" s="T583">früher</ta>
            <ta e="T585" id="Seg_11677" s="T584">aber</ta>
            <ta e="T586" id="Seg_11678" s="T585">3SG-INSTR</ta>
            <ta e="T587" id="Seg_11679" s="T586">sich.beschäftigen.mit-VBZ-FUT-1SG</ta>
            <ta e="T588" id="Seg_11680" s="T587">denken-CVB.SEQ-1SG</ta>
            <ta e="T589" id="Seg_11681" s="T588">sehr</ta>
            <ta e="T590" id="Seg_11682" s="T589">machen-VBZ-NEG.CVB.SIM</ta>
            <ta e="T591" id="Seg_11683" s="T590">gehen-EP-PST2-EP-1SG</ta>
            <ta e="T592" id="Seg_11684" s="T591">MOD</ta>
            <ta e="T593" id="Seg_11685" s="T592">jenes.[NOM]</ta>
            <ta e="T594" id="Seg_11686" s="T593">wegen</ta>
            <ta e="T595" id="Seg_11687" s="T594">vergessen-PRS-1SG</ta>
            <ta e="T611" id="Seg_11688" s="T610">dieses.[NOM]</ta>
            <ta e="T612" id="Seg_11689" s="T611">sterben-PST2-3SG</ta>
            <ta e="T613" id="Seg_11690" s="T612">dieses.[NOM]</ta>
            <ta e="T614" id="Seg_11691" s="T613">Baja.[NOM]</ta>
            <ta e="T615" id="Seg_11692" s="T614">sagen-CVB.SEQ</ta>
            <ta e="T616" id="Seg_11693" s="T615">Mädchen.[NOM]</ta>
            <ta e="T617" id="Seg_11694" s="T616">singen-PST2-3SG</ta>
            <ta e="T618" id="Seg_11695" s="T617">singen-PTCP.PST</ta>
            <ta e="T619" id="Seg_11696" s="T618">Junge-3SG.[NOM]</ta>
            <ta e="T620" id="Seg_11697" s="T619">doch</ta>
            <ta e="T621" id="Seg_11698" s="T620">sterben-PST2-3SG</ta>
            <ta e="T642" id="Seg_11699" s="T641">AFFIRM</ta>
            <ta e="T643" id="Seg_11700" s="T642">solch-3PL-INSTR</ta>
            <ta e="T644" id="Seg_11701" s="T643">Tonja-ACC</ta>
            <ta e="T645" id="Seg_11702" s="T644">mit</ta>
            <ta e="T646" id="Seg_11703" s="T645">doch</ta>
            <ta e="T647" id="Seg_11704" s="T646">äh</ta>
            <ta e="T648" id="Seg_11705" s="T647">was-VBZ-FUT-1PL</ta>
            <ta e="T649" id="Seg_11706" s="T648">sich.messen-PTCP.PRS-ACC</ta>
            <ta e="T650" id="Seg_11707" s="T649">doch</ta>
            <ta e="T651" id="Seg_11708" s="T650">singen-FUT-1PL</ta>
            <ta e="T652" id="Seg_11709" s="T651">MOD</ta>
            <ta e="T653" id="Seg_11710" s="T652">einfach</ta>
            <ta e="T654" id="Seg_11711" s="T653">atmen-CVB.SIM</ta>
            <ta e="T655" id="Seg_11712" s="T654">atmen-CVB.SIM</ta>
            <ta e="T656" id="Seg_11713" s="T655">äh</ta>
            <ta e="T657" id="Seg_11714" s="T656">Luft.anhalten-ACC</ta>
            <ta e="T658" id="Seg_11715" s="T657">atmen-CVB.SIM</ta>
            <ta e="T659" id="Seg_11716" s="T658">atmen-PRS-2SG</ta>
            <ta e="T660" id="Seg_11717" s="T659">MOD</ta>
            <ta e="T663" id="Seg_11718" s="T662">singen.[IMP.2SG]</ta>
            <ta e="T664" id="Seg_11719" s="T663">sagen-TEMP-2SG</ta>
            <ta e="T665" id="Seg_11720" s="T664">singen-FUT-1SG</ta>
            <ta e="T666" id="Seg_11721" s="T665">einfach</ta>
            <ta e="T667" id="Seg_11722" s="T666">Atem.[NOM]</ta>
            <ta e="T668" id="Seg_11723" s="T667">wegnehmen-EP-RECP/COLL-PTCP.PRS</ta>
            <ta e="T669" id="Seg_11724" s="T668">Gedicht.[NOM]</ta>
            <ta e="T685" id="Seg_11725" s="T684">dieses-ACC</ta>
            <ta e="T686" id="Seg_11726" s="T685">MOD</ta>
            <ta e="T687" id="Seg_11727" s="T686">einsam.[NOM]</ta>
            <ta e="T688" id="Seg_11728" s="T687">Atem-INSTR</ta>
            <ta e="T689" id="Seg_11729" s="T688">singen-HAB-3PL</ta>
            <ta e="T690" id="Seg_11730" s="T689">man.sagt</ta>
            <ta e="T691" id="Seg_11731" s="T690">Vorfahre-PL.[NOM]</ta>
            <ta e="T692" id="Seg_11732" s="T691">einsam.[NOM]</ta>
            <ta e="T693" id="Seg_11733" s="T692">Atem-INSTR</ta>
            <ta e="T694" id="Seg_11734" s="T693">Atem.[NOM]</ta>
            <ta e="T695" id="Seg_11735" s="T694">wegnehmen-EP-RECP/COLL-CVB.SEQ-3PL</ta>
            <ta e="T696" id="Seg_11736" s="T695">doch</ta>
            <ta e="T697" id="Seg_11737" s="T696">3PL.[NOM]</ta>
            <ta e="T698" id="Seg_11738" s="T697">Spielzeug-3PL.[NOM]</ta>
            <ta e="T699" id="Seg_11739" s="T698">jenes</ta>
            <ta e="T700" id="Seg_11740" s="T699">sein-PST1-3SG</ta>
            <ta e="T701" id="Seg_11741" s="T700">dieses</ta>
            <ta e="T702" id="Seg_11742" s="T701">Künstler-PL.[NOM]</ta>
            <ta e="T703" id="Seg_11743" s="T702">jetzt</ta>
            <ta e="T704" id="Seg_11744" s="T703">spielen-PTCP.PRS-3PL-ACC</ta>
            <ta e="T705" id="Seg_11745" s="T704">ähnlich</ta>
            <ta e="T706" id="Seg_11746" s="T705">dieses.EMPH</ta>
            <ta e="T707" id="Seg_11747" s="T706">einsam.[NOM]</ta>
            <ta e="T708" id="Seg_11748" s="T707">Atem-INSTR</ta>
            <ta e="T709" id="Seg_11749" s="T708">ankommen-PTCP.HAB</ta>
            <ta e="T710" id="Seg_11750" s="T709">sein-PST1-3SG</ta>
            <ta e="T711" id="Seg_11751" s="T710">man.sagt</ta>
            <ta e="T712" id="Seg_11752" s="T711">Innokentij.[NOM]</ta>
            <ta e="T713" id="Seg_11753" s="T712">sagen-CVB.SEQ</ta>
            <ta e="T714" id="Seg_11754" s="T713">Mensch.[NOM]</ta>
            <ta e="T715" id="Seg_11755" s="T714">dieses.[NOM]</ta>
            <ta e="T716" id="Seg_11756" s="T715">dieses-DAT/LOC</ta>
            <ta e="T728" id="Seg_11757" s="T727">dieses.[NOM]</ta>
            <ta e="T729" id="Seg_11758" s="T728">mein</ta>
            <ta e="T730" id="Seg_11759" s="T729">Tochter-EP-1SG.[NOM]</ta>
            <ta e="T731" id="Seg_11760" s="T730">dieses.[NOM]</ta>
            <ta e="T761" id="Seg_11761" s="T760">AFFIRM</ta>
            <ta e="T762" id="Seg_11762" s="T761">doch</ta>
            <ta e="T763" id="Seg_11763" s="T762">jenes</ta>
            <ta e="T764" id="Seg_11764" s="T763">1SG.[NOM]</ta>
            <ta e="T765" id="Seg_11765" s="T764">erzählen-PTCP.PST</ta>
            <ta e="T766" id="Seg_11766" s="T765">Märchen-1SG-ACC</ta>
            <ta e="T767" id="Seg_11767" s="T766">%%</ta>
            <ta e="T768" id="Seg_11768" s="T767">Taiska-ACC</ta>
            <ta e="T769" id="Seg_11769" s="T768">erzählen-PST2-3PL</ta>
            <ta e="T770" id="Seg_11770" s="T769">1SG-ACC</ta>
            <ta e="T771" id="Seg_11771" s="T770">erzählen-EP-CAUS-CVB.SEQ</ta>
            <ta e="T772" id="Seg_11772" s="T771">nachdem</ta>
            <ta e="T773" id="Seg_11773" s="T772">kommen-PST2-3PL</ta>
            <ta e="T774" id="Seg_11774" s="T773">hierher</ta>
            <ta e="T775" id="Seg_11775" s="T774">kommen-CVB.PURP-3PL</ta>
            <ta e="T776" id="Seg_11776" s="T775">%%</ta>
            <ta e="T777" id="Seg_11777" s="T776">Jahr-DAT/LOC</ta>
            <ta e="T778" id="Seg_11778" s="T777">kommen-PST2-3PL</ta>
            <ta e="T779" id="Seg_11779" s="T778">spielen-CVB.PURP-3PL</ta>
            <ta e="T780" id="Seg_11780" s="T779">jenes-3SG-3PL-ACC</ta>
            <ta e="T781" id="Seg_11781" s="T780">EMPH</ta>
            <ta e="T782" id="Seg_11782" s="T781">dieses.[NOM]</ta>
            <ta e="T783" id="Seg_11783" s="T782">wie</ta>
            <ta e="T784" id="Seg_11784" s="T783">wie</ta>
            <ta e="T785" id="Seg_11785" s="T784">erzählen-CVB.SEQ</ta>
            <ta e="T786" id="Seg_11786" s="T785">werfen-PTCP.PST</ta>
            <ta e="T787" id="Seg_11787" s="T786">sein-PST1-3PL</ta>
            <ta e="T788" id="Seg_11788" s="T787">dann</ta>
            <ta e="T789" id="Seg_11789" s="T788">Überreste-3SG.[NOM]</ta>
            <ta e="T793" id="Seg_11790" s="T792">AFFIRM</ta>
            <ta e="T794" id="Seg_11791" s="T793">hören-EP-PST2-EP-1SG</ta>
            <ta e="T795" id="Seg_11792" s="T794">jenes-ACC</ta>
            <ta e="T796" id="Seg_11793" s="T795">hören-CVB.SEQ-1SG</ta>
            <ta e="T797" id="Seg_11794" s="T796">machen-PRS-1SG</ta>
            <ta e="T798" id="Seg_11795" s="T797">dieses.[NOM]</ta>
            <ta e="T800" id="Seg_11796" s="T799">jenes-PL-ACC</ta>
            <ta e="T802" id="Seg_11797" s="T801">jenes-PL-ACC</ta>
            <ta e="T803" id="Seg_11798" s="T802">kommen-PRS-3PL</ta>
            <ta e="T804" id="Seg_11799" s="T803">jenes-PL-ACC</ta>
            <ta e="T805" id="Seg_11800" s="T804">rügen-PRS-1SG</ta>
            <ta e="T806" id="Seg_11801" s="T805">warum</ta>
            <ta e="T807" id="Seg_11802" s="T806">nun</ta>
            <ta e="T808" id="Seg_11803" s="T807">dieses.[NOM]</ta>
            <ta e="T809" id="Seg_11804" s="T808">jenes-INSTR-dieses-INSTR</ta>
            <ta e="T810" id="Seg_11805" s="T809">erzählen-PRS-2PL</ta>
            <ta e="T811" id="Seg_11806" s="T810">echt-SIM</ta>
            <ta e="T812" id="Seg_11807" s="T811">erzählen-NEG-2PL</ta>
            <ta e="T813" id="Seg_11808" s="T812">Märchen-2PL-ACC</ta>
            <ta e="T820" id="Seg_11809" s="T819">gleichwohl</ta>
            <ta e="T821" id="Seg_11810" s="T820">doch</ta>
            <ta e="T822" id="Seg_11811" s="T821">bemerken-EP-REFL-PRS.[3SG]</ta>
            <ta e="T823" id="Seg_11812" s="T822">dann</ta>
            <ta e="T824" id="Seg_11813" s="T823">grundlegend-ACC</ta>
            <ta e="T825" id="Seg_11814" s="T824">doch</ta>
            <ta e="T826" id="Seg_11815" s="T825">erzählen-PRS-3PL</ta>
            <ta e="T849" id="Seg_11816" s="T848">AFFIRM</ta>
            <ta e="T850" id="Seg_11817" s="T849">es.gibt</ta>
            <ta e="T851" id="Seg_11818" s="T850">EVID</ta>
            <ta e="T852" id="Seg_11819" s="T851">jenes</ta>
            <ta e="T853" id="Seg_11820" s="T852">Märchen-ACC</ta>
            <ta e="T854" id="Seg_11821" s="T853">wissen-PTCP.PRS</ta>
            <ta e="T855" id="Seg_11822" s="T854">1SG.[NOM]</ta>
            <ta e="T856" id="Seg_11823" s="T855">älterer.Bruder-EP-1SG.[NOM]</ta>
            <ta e="T857" id="Seg_11824" s="T856">es.gibt</ta>
            <ta e="T858" id="Seg_11825" s="T857">älterer.Bruder-PL-EP-1SG.[NOM]</ta>
            <ta e="T859" id="Seg_11826" s="T858">dann</ta>
            <ta e="T860" id="Seg_11827" s="T859">zwei-COLL</ta>
            <ta e="T861" id="Seg_11828" s="T860">EMPH</ta>
            <ta e="T862" id="Seg_11829" s="T861">wissen-PRS-3PL</ta>
            <ta e="T863" id="Seg_11830" s="T862">zwei</ta>
            <ta e="T864" id="Seg_11831" s="T863">älterer.Bruder-PROPR-1SG</ta>
            <ta e="T865" id="Seg_11832" s="T864">zwei-COLL</ta>
            <ta e="T866" id="Seg_11833" s="T865">EMPH</ta>
            <ta e="T867" id="Seg_11834" s="T866">wissen-EP</ta>
            <ta e="T868" id="Seg_11835" s="T867">einer.von.zwei-3SG.[NOM]</ta>
            <ta e="T869" id="Seg_11836" s="T868">alt.[NOM]</ta>
            <ta e="T870" id="Seg_11837" s="T869">einer.von.zwei-3SG.[NOM]</ta>
            <ta e="T871" id="Seg_11838" s="T870">1SG-COMP</ta>
            <ta e="T872" id="Seg_11839" s="T871">doch</ta>
            <ta e="T873" id="Seg_11840" s="T872">älter.[NOM]</ta>
            <ta e="T874" id="Seg_11841" s="T873">wissen-PRS-3PL</ta>
            <ta e="T875" id="Seg_11842" s="T874">jenes-PL.[NOM]</ta>
            <ta e="T876" id="Seg_11843" s="T875">Märchen-PL-ACC</ta>
            <ta e="T877" id="Seg_11844" s="T876">Lied-PROPR-PL-ACC</ta>
            <ta e="T878" id="Seg_11845" s="T877">und</ta>
            <ta e="T879" id="Seg_11846" s="T878">wissen-PRS-3PL</ta>
            <ta e="T880" id="Seg_11847" s="T879">Lied-POSS</ta>
            <ta e="T881" id="Seg_11848" s="T880">und</ta>
            <ta e="T882" id="Seg_11849" s="T881">NEG-ACC</ta>
            <ta e="T883" id="Seg_11850" s="T882">wissen-EP</ta>
            <ta e="T888" id="Seg_11851" s="T887">einfach</ta>
            <ta e="T889" id="Seg_11852" s="T888">Mensch-PL.[NOM]</ta>
            <ta e="T890" id="Seg_11853" s="T889">dieses.[NOM]</ta>
            <ta e="T891" id="Seg_11854" s="T890">früher</ta>
            <ta e="T892" id="Seg_11855" s="T891">Märchen-AG.[NOM]</ta>
            <ta e="T893" id="Seg_11856" s="T892">Mensch-PL-1PL.[NOM]</ta>
            <ta e="T894" id="Seg_11857" s="T893">zu.Ende.gehen-PST1-3PL</ta>
            <ta e="T895" id="Seg_11858" s="T894">MOD</ta>
            <ta e="T896" id="Seg_11859" s="T895">jetzt</ta>
            <ta e="T897" id="Seg_11860" s="T896">tatsächlich</ta>
            <ta e="T898" id="Seg_11861" s="T897">tatsächlich</ta>
            <ta e="T899" id="Seg_11862" s="T898">1SG.[NOM]</ta>
            <ta e="T900" id="Seg_11863" s="T899">wissen-PTCP.PRS</ta>
            <ta e="T901" id="Seg_11864" s="T900">Märchen-AG.[NOM]</ta>
            <ta e="T902" id="Seg_11865" s="T901">Mensch-PL-EP-1SG.[NOM]</ta>
            <ta e="T903" id="Seg_11866" s="T902">zu.Ende.gehen-PST1-3PL</ta>
            <ta e="T904" id="Seg_11867" s="T903">wer.[NOM]</ta>
            <ta e="T905" id="Seg_11868" s="T904">es.gibt-3SG</ta>
            <ta e="T906" id="Seg_11869" s="T905">sein-FUT.[3SG]=Q</ta>
            <ta e="T907" id="Seg_11870" s="T906">jetzt</ta>
            <ta e="T908" id="Seg_11871" s="T907">Märchen-AG.[NOM]</ta>
            <ta e="T909" id="Seg_11872" s="T908">Mensch.[NOM]</ta>
            <ta e="T910" id="Seg_11873" s="T909">wer-ACC</ta>
            <ta e="T911" id="Seg_11874" s="T910">NEG</ta>
            <ta e="T912" id="Seg_11875" s="T911">wissen-NEG-1SG</ta>
            <ta e="T913" id="Seg_11876" s="T912">jetzt</ta>
            <ta e="T914" id="Seg_11877" s="T913">Märchen-AG.[NOM]</ta>
            <ta e="T915" id="Seg_11878" s="T914">Mensch-ACC</ta>
            <ta e="T916" id="Seg_11879" s="T915">echt</ta>
            <ta e="T917" id="Seg_11880" s="T916">Märchen-AG.[NOM]</ta>
            <ta e="T918" id="Seg_11881" s="T917">Mensch-ACC</ta>
            <ta e="T933" id="Seg_11882" s="T932">nein</ta>
            <ta e="T934" id="Seg_11883" s="T933">nein</ta>
            <ta e="T935" id="Seg_11884" s="T934">früher</ta>
            <ta e="T936" id="Seg_11885" s="T935">MOD</ta>
            <ta e="T937" id="Seg_11886" s="T936">wer-3PL.[NOM]</ta>
            <ta e="T938" id="Seg_11887" s="T937">EMPH</ta>
            <ta e="T939" id="Seg_11888" s="T938">so-INTNS</ta>
            <ta e="T940" id="Seg_11889" s="T939">machen-NEG-PST1-3PL</ta>
            <ta e="T941" id="Seg_11890" s="T940">dieses</ta>
            <ta e="T942" id="Seg_11891" s="T941">jetzt</ta>
            <ta e="T943" id="Seg_11892" s="T942">dieses</ta>
            <ta e="T944" id="Seg_11893" s="T943">letztes.Jahr-ADJZ-ABL</ta>
            <ta e="T945" id="Seg_11894" s="T944">näher</ta>
            <ta e="T946" id="Seg_11895" s="T945">dieses</ta>
            <ta e="T947" id="Seg_11896" s="T946">Gewohnheit.[NOM]</ta>
            <ta e="T948" id="Seg_11897" s="T947">finden-PST1-3PL</ta>
            <ta e="T949" id="Seg_11898" s="T948">dieses</ta>
            <ta e="T950" id="Seg_11899" s="T949">früher</ta>
            <ta e="T951" id="Seg_11900" s="T950">Märchen.[NOM]</ta>
            <ta e="T952" id="Seg_11901" s="T951">jenes.[NOM]</ta>
            <ta e="T953" id="Seg_11902" s="T952">wegen</ta>
            <ta e="T954" id="Seg_11903" s="T953">1PL.[NOM]</ta>
            <ta e="T955" id="Seg_11904" s="T954">ganz</ta>
            <ta e="T956" id="Seg_11905" s="T955">vergessen-EP-PST2-1PL</ta>
            <ta e="T957" id="Seg_11906" s="T956">jenes-ACC</ta>
            <ta e="T958" id="Seg_11907" s="T957">Märchen-PL-1PL-ACC</ta>
            <ta e="T959" id="Seg_11908" s="T958">ganz-3SG-ACC</ta>
            <ta e="T960" id="Seg_11909" s="T959">vergessen-CVB.SEQ</ta>
            <ta e="T961" id="Seg_11910" s="T960">gehen-PRS-1PL</ta>
            <ta e="T962" id="Seg_11911" s="T961">EMPH</ta>
            <ta e="T963" id="Seg_11912" s="T962">ganz-3SG-ACC</ta>
            <ta e="T964" id="Seg_11913" s="T963">wann</ta>
            <ta e="T965" id="Seg_11914" s="T964">NEG</ta>
            <ta e="T966" id="Seg_11915" s="T965">erzählen-NEG-2SG</ta>
            <ta e="T967" id="Seg_11916" s="T966">Lied-ACC</ta>
            <ta e="T968" id="Seg_11917" s="T967">und</ta>
            <ta e="T969" id="Seg_11918" s="T968">vergessen-PRS-2SG</ta>
            <ta e="T970" id="Seg_11919" s="T969">Märchen-ACC</ta>
            <ta e="T971" id="Seg_11920" s="T970">und</ta>
            <ta e="T972" id="Seg_11921" s="T971">vergessen-PRS-2SG</ta>
            <ta e="T973" id="Seg_11922" s="T972">Geschichte-ACC</ta>
            <ta e="T974" id="Seg_11923" s="T973">erzählen-PTCP.PRS-3SG</ta>
            <ta e="T975" id="Seg_11924" s="T974">MOD</ta>
            <ta e="T976" id="Seg_11925" s="T975">Anisim</ta>
            <ta e="T977" id="Seg_11926" s="T976">alter.Mann.[NOM]</ta>
            <ta e="T978" id="Seg_11927" s="T977">es.gibt</ta>
            <ta e="T979" id="Seg_11928" s="T978">Märchen-AG.[NOM]</ta>
            <ta e="T980" id="Seg_11929" s="T979">auch</ta>
            <ta e="T991" id="Seg_11930" s="T990">was</ta>
            <ta e="T992" id="Seg_11931" s="T991">wissen-FUT.[3SG]=Q</ta>
            <ta e="T993" id="Seg_11932" s="T992">und</ta>
            <ta e="T994" id="Seg_11933" s="T993">jenes</ta>
            <ta e="T995" id="Seg_11934" s="T994">3PL-ACC</ta>
            <ta e="T996" id="Seg_11935" s="T995">Märchen.[NOM]</ta>
            <ta e="T997" id="Seg_11936" s="T996">sich.sättigen-PTCP.PRS-3SG.[3SG]</ta>
            <ta e="T998" id="Seg_11937" s="T997">Q</ta>
            <ta e="T999" id="Seg_11938" s="T998">3PL-DAT/LOC</ta>
            <ta e="T1000" id="Seg_11939" s="T999">was.[NOM]</ta>
            <ta e="T1001" id="Seg_11940" s="T1000">wissen</ta>
            <ta e="T1002" id="Seg_11941" s="T1001">oft-oft</ta>
            <ta e="T1003" id="Seg_11942" s="T1002">jetzt</ta>
            <ta e="T1004" id="Seg_11943" s="T1003">Märchen-VBZ-EP-RECP/COLL-NEG-1PL</ta>
            <ta e="T1005" id="Seg_11944" s="T1004">EMPH</ta>
            <ta e="T1006" id="Seg_11945" s="T1005">einfach</ta>
            <ta e="T1007" id="Seg_11946" s="T1006">erzählen.[IMP.2SG]</ta>
            <ta e="T1008" id="Seg_11947" s="T1007">nur</ta>
            <ta e="T1009" id="Seg_11948" s="T1008">sagen-TEMP-3PL</ta>
            <ta e="T1010" id="Seg_11949" s="T1009">erzählen-PRS-1SG</ta>
            <ta e="T1011" id="Seg_11950" s="T1010">man.muss-TEMP-3PL</ta>
            <ta e="T1012" id="Seg_11951" s="T1011">erzählen-EP-CAUS-TEMP-3PL</ta>
            <ta e="T1013" id="Seg_11952" s="T1012">nur</ta>
            <ta e="T1014" id="Seg_11953" s="T1013">erzählen-PRS-2SG</ta>
            <ta e="T1015" id="Seg_11954" s="T1014">EMPH</ta>
            <ta e="T1016" id="Seg_11955" s="T1015">so</ta>
            <ta e="T1017" id="Seg_11956" s="T1016">EMPH</ta>
            <ta e="T1018" id="Seg_11957" s="T1017">einfach</ta>
            <ta e="T1019" id="Seg_11958" s="T1018">wann</ta>
            <ta e="T1020" id="Seg_11959" s="T1019">NEG</ta>
            <ta e="T1021" id="Seg_11960" s="T1020">Märchen-VBZ-EP-RECP/COLL-NEG-1PL</ta>
            <ta e="T1022" id="Seg_11961" s="T1021">EMPH</ta>
            <ta e="T1042" id="Seg_11962" s="T1041">AFFIRM</ta>
            <ta e="T1054" id="Seg_11963" s="T1053">na</ta>
            <ta e="T1055" id="Seg_11964" s="T1054">anders-PART</ta>
            <ta e="T1056" id="Seg_11965" s="T1055">sein-EP-NEG.CVB</ta>
            <ta e="T1075" id="Seg_11966" s="T1074">Kind-1SG.[NOM]</ta>
            <ta e="T1076" id="Seg_11967" s="T1075">Seite-3SG-INSTR</ta>
            <ta e="T1077" id="Seg_11968" s="T1076">winzig</ta>
            <ta e="T1078" id="Seg_11969" s="T1077">Vers.[NOM]</ta>
            <ta e="T1079" id="Seg_11970" s="T1078">Überreste-3SG-ACC</ta>
            <ta e="T1080" id="Seg_11971" s="T1079">machen-PST2-EP-1SG</ta>
            <ta e="T1081" id="Seg_11972" s="T1080">Ajsülüu.[NOM]</ta>
            <ta e="T1082" id="Seg_11973" s="T1081">Seite-3SG-INSTR</ta>
            <ta e="T1083" id="Seg_11974" s="T1082">Stern-PL.[NOM]</ta>
            <ta e="T1084" id="Seg_11975" s="T1083">Himmel-DAT/LOC</ta>
            <ta e="T1085" id="Seg_11976" s="T1084">zu.sehen.sein-PRS-3PL</ta>
            <ta e="T1086" id="Seg_11977" s="T1085">schlafen.[IMP.2SG]</ta>
            <ta e="T1087" id="Seg_11978" s="T1086">schlafen.[IMP.2SG]</ta>
            <ta e="T1088" id="Seg_11979" s="T1087">Ajsülüu.[NOM]</ta>
            <ta e="T1089" id="Seg_11980" s="T1088">Zeit-1PL.[NOM]</ta>
            <ta e="T1090" id="Seg_11981" s="T1089">sich.ändern-PST1-3SG</ta>
            <ta e="T1091" id="Seg_11982" s="T1090">merkwürdig</ta>
            <ta e="T1092" id="Seg_11983" s="T1091">anders</ta>
            <ta e="T1093" id="Seg_11984" s="T1092">Zeit.[NOM]</ta>
            <ta e="T1094" id="Seg_11985" s="T1093">kommen-PST1-3SG</ta>
            <ta e="T1095" id="Seg_11986" s="T1094">schnell-INTNS-ADVZ</ta>
            <ta e="T1096" id="Seg_11987" s="T1095">wachsen-FUT.[IMP.2SG]</ta>
            <ta e="T1097" id="Seg_11988" s="T1096">Faden-1SG-ACC</ta>
            <ta e="T1098" id="Seg_11989" s="T1097">Nadel-1SG-ACC</ta>
            <ta e="T1099" id="Seg_11990" s="T1098">halten-FUT.[IMP.2SG]</ta>
            <ta e="T1100" id="Seg_11991" s="T1099">1SG.[NOM]</ta>
            <ta e="T1101" id="Seg_11992" s="T1100">Leben-1SG-ACC</ta>
            <ta e="T1102" id="Seg_11993" s="T1101">Leben-VBZ-FUT.[IMP.2SG]</ta>
            <ta e="T1103" id="Seg_11994" s="T1102">Mensch-DAT/LOC</ta>
            <ta e="T1104" id="Seg_11995" s="T1103">Mensch.[NOM]</ta>
            <ta e="T1105" id="Seg_11996" s="T1104">sagen-EP-CAUS-FUT.[IMP.2SG]</ta>
            <ta e="T1106" id="Seg_11997" s="T1105">Kind-DIM-EP-1SG.[NOM]</ta>
            <ta e="T1107" id="Seg_11998" s="T1106">Kind-3SG-ACC</ta>
            <ta e="T1108" id="Seg_11999" s="T1107">Wiege-3SG-DAT/LOC</ta>
            <ta e="T1109" id="Seg_12000" s="T1108">schwingen-PRS-1SG</ta>
            <ta e="T1110" id="Seg_12001" s="T1109">Platz.neben-3SG-DAT/LOC</ta>
            <ta e="T1111" id="Seg_12002" s="T1110">sitzen-PRS-1SG</ta>
            <ta e="T1112" id="Seg_12003" s="T1111">ganz.leise</ta>
            <ta e="T1113" id="Seg_12004" s="T1112">singen-EP-MED-PST1-1SG</ta>
         </annotation>
         <annotation name="gr" tierref="gr-UkET">
            <ta e="T23" id="Seg_12005" s="T21">1SG.[NOM]</ta>
            <ta e="T25" id="Seg_12006" s="T23">и</ta>
            <ta e="T26" id="Seg_12007" s="T25">сам-1SG.[NOM]</ta>
            <ta e="T27" id="Seg_12008" s="T26">родиться-PTCP.PST</ta>
            <ta e="T28" id="Seg_12009" s="T27">земля-EP-1SG.[NOM]</ta>
            <ta e="T29" id="Seg_12010" s="T28">этот.[NOM]</ta>
            <ta e="T30" id="Seg_12011" s="T29">к</ta>
            <ta e="T31" id="Seg_12012" s="T30">быть-PST1-3SG</ta>
            <ta e="T32" id="Seg_12013" s="T31">север.[NOM]</ta>
            <ta e="T33" id="Seg_12014" s="T32">к</ta>
            <ta e="T34" id="Seg_12015" s="T33">тот.[NOM]</ta>
            <ta e="T35" id="Seg_12016" s="T34">к</ta>
            <ta e="T37" id="Seg_12017" s="T36">кто.[NOM]</ta>
            <ta e="T38" id="Seg_12018" s="T37">к</ta>
            <ta e="T39" id="Seg_12019" s="T38">ээ</ta>
            <ta e="T40" id="Seg_12020" s="T39">потом</ta>
            <ta e="T41" id="Seg_12021" s="T40">Новорыбное.[NOM]</ta>
            <ta e="T42" id="Seg_12022" s="T41">на.том.берегу-2PL-DAT/LOC</ta>
            <ta e="T43" id="Seg_12023" s="T42">тот.[NOM]</ta>
            <ta e="T44" id="Seg_12024" s="T43">север.[NOM]</ta>
            <ta e="T45" id="Seg_12025" s="T44">к</ta>
            <ta e="T46" id="Seg_12026" s="T45">родиться-PST2-EP-1SG</ta>
            <ta e="T47" id="Seg_12027" s="T46">потом</ta>
            <ta e="T48" id="Seg_12028" s="T47">муж-DAT/LOC</ta>
            <ta e="T49" id="Seg_12029" s="T48">идти-CVB.SEQ-1SG</ta>
            <ta e="T50" id="Seg_12030" s="T49">юг.[NOM]</ta>
            <ta e="T51" id="Seg_12031" s="T50">к</ta>
            <ta e="T52" id="Seg_12032" s="T51">земля-VBZ-PST2-EP-1SG</ta>
            <ta e="T53" id="Seg_12033" s="T52">ребенок-PL-EP-1SG.[NOM]</ta>
            <ta e="T54" id="Seg_12034" s="T53">EMPH</ta>
            <ta e="T55" id="Seg_12035" s="T54">что.[NOM]</ta>
            <ta e="T56" id="Seg_12036" s="T55">как</ta>
            <ta e="T57" id="Seg_12037" s="T56">рассказывать-CVB.SEQ</ta>
            <ta e="T58" id="Seg_12038" s="T57">EMPH</ta>
            <ta e="T59" id="Seg_12039" s="T58">что-ACC</ta>
            <ta e="T60" id="Seg_12040" s="T59">рассказывать-FUT-1SG=Q</ta>
            <ta e="T61" id="Seg_12041" s="T60">один</ta>
            <ta e="T62" id="Seg_12042" s="T61">ребенок-1SG.[NOM]</ta>
            <ta e="T63" id="Seg_12043" s="T62">тот.[NOM]</ta>
            <ta e="T64" id="Seg_12044" s="T63">школа-DAT/LOC</ta>
            <ta e="T65" id="Seg_12045" s="T64">работать-PRS.[3SG]</ta>
            <ta e="T66" id="Seg_12046" s="T65">один</ta>
            <ta e="T67" id="Seg_12047" s="T66">дочь-EP-1SG.[NOM]</ta>
            <ta e="T68" id="Seg_12048" s="T67">директор-VBZ-PRS.[3SG]</ta>
            <ta e="T69" id="Seg_12049" s="T68">один</ta>
            <ta e="T70" id="Seg_12050" s="T69">э</ta>
            <ta e="T71" id="Seg_12051" s="T70">один</ta>
            <ta e="T72" id="Seg_12052" s="T71">ребенок-1SG.[NOM]</ta>
            <ta e="T73" id="Seg_12053" s="T72">тот.[NOM]</ta>
            <ta e="T74" id="Seg_12054" s="T73">Хатанга-DAT/LOC</ta>
            <ta e="T76" id="Seg_12055" s="T75">самый</ta>
            <ta e="T77" id="Seg_12056" s="T76">большой.[NOM]</ta>
            <ta e="T78" id="Seg_12057" s="T77">дочь-EP-1SG.[NOM]</ta>
            <ta e="T79" id="Seg_12058" s="T78">Хатанга-DAT/LOC</ta>
            <ta e="T80" id="Seg_12059" s="T79">школа-DAT/LOC</ta>
            <ta e="T81" id="Seg_12060" s="T80">доктор-VBZ-PRS.[3SG]</ta>
            <ta e="T82" id="Seg_12061" s="T81">Нина</ta>
            <ta e="T84" id="Seg_12062" s="T83">тот</ta>
            <ta e="T85" id="Seg_12063" s="T84">большой</ta>
            <ta e="T86" id="Seg_12064" s="T85">дочь-EP-1SG.[NOM]</ta>
            <ta e="T87" id="Seg_12065" s="T86">имя-3SG.[NOM]</ta>
            <ta e="T88" id="Seg_12066" s="T87">маленький.[NOM]</ta>
            <ta e="T89" id="Seg_12067" s="T88">тот-3SG-1SG.[NOM]</ta>
            <ta e="T90" id="Seg_12068" s="T89">нижняя.часть-EP-3SG.[NOM]</ta>
            <ta e="T91" id="Seg_12069" s="T90">Катя</ta>
            <ta e="T92" id="Seg_12070" s="T91">тот</ta>
            <ta e="T93" id="Seg_12071" s="T92">школа-DAT/LOC</ta>
            <ta e="T94" id="Seg_12072" s="T93">директор-VBZ-PRS.[3SG]</ta>
            <ta e="T95" id="Seg_12073" s="T94">один</ta>
            <ta e="T96" id="Seg_12074" s="T95">дочь-EP-1SG.[NOM]</ta>
            <ta e="T97" id="Seg_12075" s="T96">садик-DAT/LOC</ta>
            <ta e="T98" id="Seg_12076" s="T97">работать-PTCP.HAB</ta>
            <ta e="T99" id="Seg_12077" s="T98">быть-PST1-3SG</ta>
            <ta e="T100" id="Seg_12078" s="T99">тот-3SG-1SG.[NOM]</ta>
            <ta e="T101" id="Seg_12079" s="T100">ээ</ta>
            <ta e="T102" id="Seg_12080" s="T101">этот</ta>
            <ta e="T103" id="Seg_12081" s="T102">садик-3PL.[NOM]</ta>
            <ta e="T104" id="Seg_12082" s="T103">что-3PL.[NOM]</ta>
            <ta e="T105" id="Seg_12083" s="T104">ломаться-CVB.SEQ</ta>
            <ta e="T106" id="Seg_12084" s="T105">специально</ta>
            <ta e="T107" id="Seg_12085" s="T106">EMPH</ta>
            <ta e="T108" id="Seg_12086" s="T107">ребенок-VBZ-CVB.SEQ</ta>
            <ta e="T109" id="Seg_12087" s="T108">и.так.далее-CVB.SEQ</ta>
            <ta e="T110" id="Seg_12088" s="T109">теперь</ta>
            <ta e="T111" id="Seg_12089" s="T110">быть.больным-CVB.SEQ</ta>
            <ta e="T112" id="Seg_12090" s="T111">Дудинка.[NOM]</ta>
            <ta e="T113" id="Seg_12091" s="T112">к</ta>
            <ta e="T114" id="Seg_12092" s="T113">находиться-PRS.[3SG]</ta>
            <ta e="T115" id="Seg_12093" s="T114">этот</ta>
            <ta e="T116" id="Seg_12094" s="T115">ээ</ta>
            <ta e="T117" id="Seg_12095" s="T116">Норильск-DAT/LOC</ta>
            <ta e="T118" id="Seg_12096" s="T117">лечить-EP-CAUS-CVB.SIM</ta>
            <ta e="T119" id="Seg_12097" s="T118">лежать-PRS.[3SG]</ta>
            <ta e="T120" id="Seg_12098" s="T119">теперь</ta>
            <ta e="T121" id="Seg_12099" s="T120">потом</ta>
            <ta e="T122" id="Seg_12100" s="T121">один</ta>
            <ta e="T123" id="Seg_12101" s="T122">сын-EP-1SG.[NOM]</ta>
            <ta e="T124" id="Seg_12102" s="T123">охотник.[NOM]</ta>
            <ta e="T125" id="Seg_12103" s="T124">быть-PST1-3SG</ta>
            <ta e="T126" id="Seg_12104" s="T125">один</ta>
            <ta e="T127" id="Seg_12105" s="T126">мальчик-EP-1SG.[NOM]</ta>
            <ta e="T128" id="Seg_12106" s="T127">тот.[NOM]</ta>
            <ta e="T129" id="Seg_12107" s="T128">самолет-PL-ACC</ta>
            <ta e="T131" id="Seg_12108" s="T130">разный-3PL-ACC</ta>
            <ta e="T132" id="Seg_12109" s="T131">послать</ta>
            <ta e="T133" id="Seg_12110" s="T132">что-VBZ-PRS.[3SG]</ta>
            <ta e="T134" id="Seg_12111" s="T133">делать-PRS.[3SG]</ta>
            <ta e="T135" id="Seg_12112" s="T134">самолет-PL-ACC</ta>
            <ta e="T136" id="Seg_12113" s="T135">зарядить-PRS.[3SG]</ta>
            <ta e="T137" id="Seg_12114" s="T136">летать-EP-CAUS-PRS.[3SG]</ta>
            <ta e="T138" id="Seg_12115" s="T137">там</ta>
            <ta e="T139" id="Seg_12116" s="T138">есть</ta>
            <ta e="T140" id="Seg_12117" s="T139">один</ta>
            <ta e="T141" id="Seg_12118" s="T140">дочь-EP-1SG.[NOM]</ta>
            <ta e="T143" id="Seg_12119" s="T142">что-ACC</ta>
            <ta e="T144" id="Seg_12120" s="T143">кончать-PTCP.PST</ta>
            <ta e="T145" id="Seg_12121" s="T144">быть-PST1-3SG</ta>
            <ta e="T146" id="Seg_12122" s="T145">садик.[NOM]</ta>
            <ta e="T147" id="Seg_12123" s="T146">учеба-3SG-ACC</ta>
            <ta e="T148" id="Seg_12124" s="T147">теперь</ta>
            <ta e="T149" id="Seg_12125" s="T148">сам-1SG.[NOM]</ta>
            <ta e="T150" id="Seg_12126" s="T149">постареть-CVB.SEQ-1SG</ta>
            <ta e="T151" id="Seg_12127" s="T150">тот-3SG-1SG-ACC</ta>
            <ta e="T152" id="Seg_12128" s="T151">сам-1SG-DAT/LOC</ta>
            <ta e="T153" id="Seg_12129" s="T152">работать-EP-CAUS-CVB.PURP-1SG</ta>
            <ta e="T154" id="Seg_12130" s="T153">тундра-DAT/LOC</ta>
            <ta e="T155" id="Seg_12131" s="T154">держать-PRS-1SG</ta>
            <ta e="T156" id="Seg_12132" s="T155">старик-EP-1SG.[NOM]</ta>
            <ta e="T157" id="Seg_12133" s="T156">раньше</ta>
            <ta e="T158" id="Seg_12134" s="T157">охотник.[NOM]</ta>
            <ta e="T159" id="Seg_12135" s="T158">быть-PST1-3SG</ta>
            <ta e="T160" id="Seg_12136" s="T159">ведь</ta>
            <ta e="T162" id="Seg_12137" s="T161">человек-ABL</ta>
            <ta e="T163" id="Seg_12138" s="T162">ээ</ta>
            <ta e="T164" id="Seg_12139" s="T163">Новорыбное-DAT/LOC</ta>
            <ta e="T165" id="Seg_12140" s="T164">потом</ta>
            <ta e="T166" id="Seg_12141" s="T165">совхоз-DAT/LOC</ta>
            <ta e="T167" id="Seg_12142" s="T166">передовой-3SG-INSTR</ta>
            <ta e="T168" id="Seg_12143" s="T167">охотить-PTCP.HAB</ta>
            <ta e="T169" id="Seg_12144" s="T168">быть-PST1-3SG</ta>
            <ta e="T170" id="Seg_12145" s="T169">теперь</ta>
            <ta e="T171" id="Seg_12146" s="T170">быть.больным-CVB.SEQ</ta>
            <ta e="T172" id="Seg_12147" s="T171">рука-3SG.[NOM]</ta>
            <ta e="T173" id="Seg_12148" s="T172">быть.больным-CVB.SEQ</ta>
            <ta e="T174" id="Seg_12149" s="T173">тот</ta>
            <ta e="T175" id="Seg_12150" s="T174">охота-EP-ABL</ta>
            <ta e="T176" id="Seg_12151" s="T175">EMPH</ta>
            <ta e="T177" id="Seg_12152" s="T176">выйти-PST1-3SG</ta>
            <ta e="T178" id="Seg_12153" s="T177">тот-DAT/LOC</ta>
            <ta e="T179" id="Seg_12154" s="T178">дополнительный-VBZ-CVB.SEQ</ta>
            <ta e="T180" id="Seg_12155" s="T179">теперь</ta>
            <ta e="T181" id="Seg_12156" s="T180">читать-CAUS-CVB.SEQ</ta>
            <ta e="T182" id="Seg_12157" s="T181">пенсия-DAT/LOC</ta>
            <ta e="T183" id="Seg_12158" s="T182">входить-PST1-3SG</ta>
            <ta e="T184" id="Seg_12159" s="T183">этот</ta>
            <ta e="T185" id="Seg_12160" s="T184">теперь</ta>
            <ta e="T186" id="Seg_12161" s="T185">этот</ta>
            <ta e="T188" id="Seg_12162" s="T186">год-PL-DAT/LOC</ta>
            <ta e="T190" id="Seg_12163" s="T188">входить-PST2-3SG</ta>
            <ta e="T203" id="Seg_12164" s="T202">AFFIRM</ta>
            <ta e="T204" id="Seg_12165" s="T203">теперь</ta>
            <ta e="T205" id="Seg_12166" s="T204">этот</ta>
            <ta e="T206" id="Seg_12167" s="T205">ребенок-PL-EP-1SG.[NOM]</ta>
            <ta e="T207" id="Seg_12168" s="T206">ребенок-1SG.[NOM]</ta>
            <ta e="T208" id="Seg_12169" s="T207">ребенок-PL-3SG-ACC</ta>
            <ta e="T209" id="Seg_12170" s="T208">заботиться-CVB.SEQ-1SG</ta>
            <ta e="T210" id="Seg_12171" s="T209">тот</ta>
            <ta e="T211" id="Seg_12172" s="T210">лекарство-DAT/LOC</ta>
            <ta e="T212" id="Seg_12173" s="T211">приходить-PTCP.HAB</ta>
            <ta e="T213" id="Seg_12174" s="T212">ребенок-1SG.[NOM]</ta>
            <ta e="T214" id="Seg_12175" s="T213">собственный-3SG</ta>
            <ta e="T215" id="Seg_12176" s="T214">три</ta>
            <ta e="T216" id="Seg_12177" s="T215">ребенок-PROPR.[NOM]</ta>
            <ta e="T217" id="Seg_12178" s="T216">MOD</ta>
            <ta e="T218" id="Seg_12179" s="T217">тот-3SG-PL-EP-1SG.[NOM]</ta>
            <ta e="T219" id="Seg_12180" s="T218">ребенок-3SG-ACC</ta>
            <ta e="T220" id="Seg_12181" s="T219">заботиться-CVB.SIM</ta>
            <ta e="T221" id="Seg_12182" s="T220">лежать-PTCP.PRS</ta>
            <ta e="T222" id="Seg_12183" s="T221">быть-PST1-1SG</ta>
            <ta e="T223" id="Seg_12184" s="T222">EMPH</ta>
            <ta e="T224" id="Seg_12185" s="T223">теперь</ta>
            <ta e="T225" id="Seg_12186" s="T224">однако</ta>
            <ta e="T226" id="Seg_12187" s="T225">этот</ta>
            <ta e="T227" id="Seg_12188" s="T226">ребенок-1SG.[NOM]</ta>
            <ta e="T228" id="Seg_12189" s="T227">тот</ta>
            <ta e="T229" id="Seg_12190" s="T228">Маша-1SG.[NOM]</ta>
            <ta e="T230" id="Seg_12191" s="T229">приходить-CVB.SEQ</ta>
            <ta e="T231" id="Seg_12192" s="T230">приходить-PST1-1SG</ta>
            <ta e="T232" id="Seg_12193" s="T231">тот</ta>
            <ta e="T233" id="Seg_12194" s="T232">тундра-ABL</ta>
            <ta e="T234" id="Seg_12195" s="T233">тундра-DAT/LOC</ta>
            <ta e="T235" id="Seg_12196" s="T234">держать-CVB.SIM</ta>
            <ta e="T236" id="Seg_12197" s="T235">лежать-PRS-1SG</ta>
            <ta e="T237" id="Seg_12198" s="T236">MOD</ta>
            <ta e="T238" id="Seg_12199" s="T237">тот</ta>
            <ta e="T239" id="Seg_12200" s="T238">три</ta>
            <ta e="T240" id="Seg_12201" s="T239">ребенок-1SG-ACC</ta>
            <ta e="T241" id="Seg_12202" s="T240">тот</ta>
            <ta e="T242" id="Seg_12203" s="T241">мама-3SG.[NOM]</ta>
            <ta e="T243" id="Seg_12204" s="T242">этот.[NOM]</ta>
            <ta e="T244" id="Seg_12205" s="T243">к</ta>
            <ta e="T245" id="Seg_12206" s="T244">есть</ta>
            <ta e="T246" id="Seg_12207" s="T245">отец-3PL.[NOM]</ta>
            <ta e="T247" id="Seg_12208" s="T246">уйти-PST2-3SG</ta>
            <ta e="T248" id="Seg_12209" s="T247">MOD</ta>
            <ta e="T249" id="Seg_12210" s="T248">тот.[NOM]</ta>
            <ta e="T250" id="Seg_12211" s="T249">ненец.[NOM]</ta>
            <ta e="T251" id="Seg_12212" s="T250">быть-PST1-3SG</ta>
            <ta e="T252" id="Seg_12213" s="T251">тот-3SG-3SG.[NOM]</ta>
            <ta e="T253" id="Seg_12214" s="T252">уйти-PST2-3SG</ta>
            <ta e="T254" id="Seg_12215" s="T253">тот.[NOM]</ta>
            <ta e="T255" id="Seg_12216" s="T254">из_за</ta>
            <ta e="T256" id="Seg_12217" s="T255">тот</ta>
            <ta e="T257" id="Seg_12218" s="T256">откуда</ta>
            <ta e="T258" id="Seg_12219" s="T257">один-ABL</ta>
            <ta e="T259" id="Seg_12220" s="T258">один</ta>
            <ta e="T260" id="Seg_12221" s="T259">ребенок.[NOM]</ta>
            <ta e="T261" id="Seg_12222" s="T260">MOD</ta>
            <ta e="T262" id="Seg_12223" s="T261">ребенок-PL-EP-1SG.[NOM]</ta>
            <ta e="T263" id="Seg_12224" s="T262">ребенок-3PL.[NOM]</ta>
            <ta e="T264" id="Seg_12225" s="T263">когда</ta>
            <ta e="T265" id="Seg_12226" s="T264">NEG</ta>
            <ta e="T266" id="Seg_12227" s="T265">уйти-NEG-3PL</ta>
            <ta e="T267" id="Seg_12228" s="T266">EMPH</ta>
            <ta e="T268" id="Seg_12229" s="T267">летом</ta>
            <ta e="T269" id="Seg_12230" s="T268">лето-PL-ACC</ta>
            <ta e="T270" id="Seg_12231" s="T269">зима-PL-ACC</ta>
            <ta e="T271" id="Seg_12232" s="T270">сквозь</ta>
            <ta e="T272" id="Seg_12233" s="T271">ребенок-PROPR.[NOM]</ta>
            <ta e="T273" id="Seg_12234" s="T272">быть-HAB-1SG</ta>
            <ta e="T274" id="Seg_12235" s="T273">внук-PL-EP-1SG.[NOM]</ta>
            <ta e="T275" id="Seg_12236" s="T274">всегда</ta>
            <ta e="T276" id="Seg_12237" s="T275">есть</ta>
            <ta e="T277" id="Seg_12238" s="T276">быть-PRS-3PL</ta>
            <ta e="T300" id="Seg_12239" s="T299">AFFIRM</ta>
            <ta e="T301" id="Seg_12240" s="T300">рассказывать-HAB-1PL</ta>
            <ta e="T302" id="Seg_12241" s="T301">разный-DIM-ACC</ta>
            <ta e="T303" id="Seg_12242" s="T302">EMPH</ta>
            <ta e="T304" id="Seg_12243" s="T303">теперь</ta>
            <ta e="T305" id="Seg_12244" s="T304">большой-ADVZ</ta>
            <ta e="T306" id="Seg_12245" s="T305">NEG</ta>
            <ta e="T307" id="Seg_12246" s="T306">понимать-EP-MED-CVB.SIM</ta>
            <ta e="T308" id="Seg_12247" s="T307">еще.не-3PL</ta>
            <ta e="T309" id="Seg_12248" s="T308">EVID</ta>
            <ta e="T310" id="Seg_12249" s="T309">большой</ta>
            <ta e="T311" id="Seg_12250" s="T310">дочь-EP-1SG.[NOM]</ta>
            <ta e="T312" id="Seg_12251" s="T311">только</ta>
            <ta e="T313" id="Seg_12252" s="T312">ребенок-PL-3SG.[NOM]</ta>
            <ta e="T314" id="Seg_12253" s="T313">школа-DAT/LOC</ta>
            <ta e="T315" id="Seg_12254" s="T314">идти-PST2-3PL</ta>
            <ta e="T316" id="Seg_12255" s="T315">маленький.[NOM]</ta>
            <ta e="T317" id="Seg_12256" s="T316">ребенок-PL-EP-1SG.[NOM]</ta>
            <ta e="T318" id="Seg_12257" s="T317">ребенок-3PL.[NOM]</ta>
            <ta e="T319" id="Seg_12258" s="T318">входить-CVB.SIM</ta>
            <ta e="T320" id="Seg_12259" s="T319">еще.не-3PL</ta>
            <ta e="T321" id="Seg_12260" s="T320">школа-DAT/LOC</ta>
            <ta e="T322" id="Seg_12261" s="T321">тот-PL-DAT/LOC</ta>
            <ta e="T323" id="Seg_12262" s="T322">тот</ta>
            <ta e="T324" id="Seg_12263" s="T323">разный-DIM-ACC</ta>
            <ta e="T325" id="Seg_12264" s="T324">рассказывать-PRS-1SG</ta>
            <ta e="T326" id="Seg_12265" s="T325">EMPH</ta>
            <ta e="T327" id="Seg_12266" s="T326">разный-ACC-разный-ACC</ta>
            <ta e="T328" id="Seg_12267" s="T327">бабушка.[NOM]</ta>
            <ta e="T329" id="Seg_12268" s="T328">говорить-CVB.SEQ</ta>
            <ta e="T330" id="Seg_12269" s="T329">вращаться-CVB.SIM</ta>
            <ta e="T331" id="Seg_12270" s="T330">идти-HAB-3PL</ta>
            <ta e="T332" id="Seg_12271" s="T331">EMPH</ta>
            <ta e="T333" id="Seg_12272" s="T332">тот-3SG-2SG-ACC</ta>
            <ta e="T334" id="Seg_12273" s="T333">рассказывать.[IMP.2SG]</ta>
            <ta e="T335" id="Seg_12274" s="T334">тот-ACC</ta>
            <ta e="T336" id="Seg_12275" s="T335">сказка-VBZ.[IMP.2SG]</ta>
            <ta e="T337" id="Seg_12276" s="T336">этот-ACC</ta>
            <ta e="T338" id="Seg_12277" s="T337">рассказывать.[IMP.2SG]</ta>
            <ta e="T339" id="Seg_12278" s="T338">тот-ACC</ta>
            <ta e="T340" id="Seg_12279" s="T339">петь.[IMP.2SG]</ta>
            <ta e="T341" id="Seg_12280" s="T340">тот-PL-ACC</ta>
            <ta e="T342" id="Seg_12281" s="T341">развлекать-CVB.PURP-2SG</ta>
            <ta e="T343" id="Seg_12282" s="T342">вот</ta>
            <ta e="T344" id="Seg_12283" s="T343">петь-EP-MED-EP-IMP.2PL</ta>
            <ta e="T345" id="Seg_12284" s="T344">рассказывать-EP-CAUS-EP-IMP.2PL</ta>
            <ta e="T346" id="Seg_12285" s="T345">ведь</ta>
            <ta e="T347" id="Seg_12286" s="T346">густой.[NOM]</ta>
            <ta e="T348" id="Seg_12287" s="T347">MOD</ta>
            <ta e="T356" id="Seg_12288" s="T355">эй</ta>
            <ta e="T357" id="Seg_12289" s="T356">мои</ta>
            <ta e="T358" id="Seg_12290" s="T357">предок-PL-EP-1SG.[NOM]</ta>
            <ta e="T359" id="Seg_12291" s="T358">каждый-3SG.[NOM]</ta>
            <ta e="T360" id="Seg_12292" s="T359">сказка-AG.[NOM]</ta>
            <ta e="T361" id="Seg_12293" s="T360">быть-PST1-3PL</ta>
            <ta e="T362" id="Seg_12294" s="T361">петь-PTCP.HAB</ta>
            <ta e="T363" id="Seg_12295" s="T362">да</ta>
            <ta e="T364" id="Seg_12296" s="T363">быть-PST1-3PL</ta>
            <ta e="T365" id="Seg_12297" s="T364">песня.[NOM]</ta>
            <ta e="T366" id="Seg_12298" s="T365">петь-EP</ta>
            <ta e="T367" id="Seg_12299" s="T366">петь-EP-MED-CVB.SIM</ta>
            <ta e="T368" id="Seg_12300" s="T367">песня-PROPR.[NOM]</ta>
            <ta e="T369" id="Seg_12301" s="T368">да</ta>
            <ta e="T370" id="Seg_12302" s="T369">сказка-ACC</ta>
            <ta e="T371" id="Seg_12303" s="T370">рассказывать-HAB.[3SG]</ta>
            <ta e="T372" id="Seg_12304" s="T371">просто</ta>
            <ta e="T373" id="Seg_12305" s="T372">да</ta>
            <ta e="T374" id="Seg_12306" s="T373">сказка-ACC</ta>
            <ta e="T375" id="Seg_12307" s="T374">рассказывать-EP</ta>
            <ta e="T376" id="Seg_12308" s="T375">тогда</ta>
            <ta e="T377" id="Seg_12309" s="T376">давно-ADJZ</ta>
            <ta e="T378" id="Seg_12310" s="T377">время-DAT/LOC</ta>
            <ta e="T379" id="Seg_12311" s="T378">однако</ta>
            <ta e="T380" id="Seg_12312" s="T379">кино-ACC</ta>
            <ta e="T381" id="Seg_12313" s="T380">видеть-PRS-2SG</ta>
            <ta e="T382" id="Seg_12314" s="T381">Q</ta>
            <ta e="T383" id="Seg_12315" s="T382">тогда</ta>
            <ta e="T384" id="Seg_12316" s="T383">EMPH</ta>
            <ta e="T385" id="Seg_12317" s="T384">Спидола-ACC</ta>
            <ta e="T386" id="Seg_12318" s="T385">слышаться-PRS-2SG</ta>
            <ta e="T387" id="Seg_12319" s="T386">MOD</ta>
            <ta e="T388" id="Seg_12320" s="T387">дерево.[NOM]</ta>
            <ta e="T389" id="Seg_12321" s="T388">игрушки-EP-INSTR</ta>
            <ta e="T390" id="Seg_12322" s="T389">играть-PRS-2SG</ta>
            <ta e="T391" id="Seg_12323" s="T390">птичка-PL-EP-INSTR</ta>
            <ta e="T392" id="Seg_12324" s="T391">тот.[NOM]</ta>
            <ta e="T393" id="Seg_12325" s="T392">сани.[NOM]</ta>
            <ta e="T394" id="Seg_12326" s="T393">делать-CVB.SIM</ta>
            <ta e="T395" id="Seg_12327" s="T394">делать-PRS-2SG</ta>
            <ta e="T396" id="Seg_12328" s="T395">на.улицу</ta>
            <ta e="T397" id="Seg_12329" s="T396">выйти-TEMP-2SG</ta>
            <ta e="T398" id="Seg_12330" s="T397">рог.[NOM]</ta>
            <ta e="T399" id="Seg_12331" s="T398">бросать-CVB.SEQ</ta>
            <ta e="T400" id="Seg_12332" s="T399">рог-EP-INSTR</ta>
            <ta e="T401" id="Seg_12333" s="T400">играть-FUT-2SG</ta>
            <ta e="T402" id="Seg_12334" s="T401">мальчик.[NOM]</ta>
            <ta e="T403" id="Seg_12335" s="T402">ребенок.[NOM]</ta>
            <ta e="T404" id="Seg_12336" s="T403">MOD</ta>
            <ta e="T405" id="Seg_12337" s="T404">маут-VBZ-CVB.SIM</ta>
            <ta e="T406" id="Seg_12338" s="T405">играть-FUT-3SG</ta>
            <ta e="T407" id="Seg_12339" s="T406">летом</ta>
            <ta e="T408" id="Seg_12340" s="T407">как</ta>
            <ta e="T409" id="Seg_12341" s="T408">идти-PTCP.PRS-2SG-ACC</ta>
            <ta e="T410" id="Seg_12342" s="T409">камень-ACC</ta>
            <ta e="T411" id="Seg_12343" s="T410">собирать-EP-MED-CVB.SEQ-2SG</ta>
            <ta e="T412" id="Seg_12344" s="T411">караван.[NOM]</ta>
            <ta e="T413" id="Seg_12345" s="T412">играть-FUT-2SG</ta>
            <ta e="T414" id="Seg_12346" s="T413">тот.EMPH.[NOM]</ta>
            <ta e="T415" id="Seg_12347" s="T414">как-INTNS</ta>
            <ta e="T416" id="Seg_12348" s="T415">день-ACC</ta>
            <ta e="T417" id="Seg_12349" s="T416">резать-PRS-2SG</ta>
            <ta e="T418" id="Seg_12350" s="T417">MOD</ta>
            <ta e="T419" id="Seg_12351" s="T418">давно-ADJZ</ta>
            <ta e="T420" id="Seg_12352" s="T419">ребенок.[NOM]</ta>
            <ta e="T421" id="Seg_12353" s="T420">старина-SIM</ta>
            <ta e="T422" id="Seg_12354" s="T421">играть-PRS.[3SG]</ta>
            <ta e="T423" id="Seg_12355" s="T422">EMPH</ta>
            <ta e="T424" id="Seg_12356" s="T423">тот</ta>
            <ta e="T425" id="Seg_12357" s="T424">резать-NMNZ-3SG-INSTR</ta>
            <ta e="T426" id="Seg_12358" s="T425">теперь</ta>
            <ta e="T427" id="Seg_12359" s="T426">давно-ADJZ</ta>
            <ta e="T428" id="Seg_12360" s="T427">игрушки-ACC</ta>
            <ta e="T429" id="Seg_12361" s="T428">EMPH</ta>
            <ta e="T430" id="Seg_12362" s="T429">теперь</ta>
            <ta e="T431" id="Seg_12363" s="T430">EMPH</ta>
            <ta e="T432" id="Seg_12364" s="T431">видеть-PTCP.FUT-2PL-ACC</ta>
            <ta e="T433" id="Seg_12365" s="T432">EMPH</ta>
            <ta e="T434" id="Seg_12366" s="T433">круглый.и.толстый</ta>
            <ta e="T435" id="Seg_12367" s="T434">игрушки-ACC</ta>
            <ta e="T436" id="Seg_12368" s="T435">принести-PST2-EP-1SG</ta>
            <ta e="T437" id="Seg_12369" s="T436">MOD</ta>
            <ta e="T438" id="Seg_12370" s="T437">этот</ta>
            <ta e="T439" id="Seg_12371" s="T438">место-DAT/LOC</ta>
            <ta e="T440" id="Seg_12372" s="T439">ведь</ta>
            <ta e="T441" id="Seg_12373" s="T440">показывать-FUT-1SG</ta>
            <ta e="T442" id="Seg_12374" s="T441">тот</ta>
            <ta e="T443" id="Seg_12375" s="T442">играть-NMNZ.[NOM]</ta>
            <ta e="T444" id="Seg_12376" s="T443">как</ta>
            <ta e="T445" id="Seg_12377" s="T444">играть-PTCP.PRS-3PL-ACC</ta>
            <ta e="T446" id="Seg_12378" s="T445">кое_какой</ta>
            <ta e="T447" id="Seg_12379" s="T446">игрушки-PL-ACC</ta>
            <ta e="T448" id="Seg_12380" s="T447">делать</ta>
            <ta e="T449" id="Seg_12381" s="T448">делать-CVB.SEQ</ta>
            <ta e="T450" id="Seg_12382" s="T449">принести-PTCP.FUT-1SG-ACC</ta>
            <ta e="T451" id="Seg_12383" s="T450">человек-POSS</ta>
            <ta e="T452" id="Seg_12384" s="T451">NEG-1SG</ta>
            <ta e="T453" id="Seg_12385" s="T452">старик-EP-1SG.[NOM]</ta>
            <ta e="T454" id="Seg_12386" s="T453">тот</ta>
            <ta e="T455" id="Seg_12387" s="T454">больница-DAT/LOC</ta>
            <ta e="T456" id="Seg_12388" s="T455">лежать-PTCP.PRS</ta>
            <ta e="T457" id="Seg_12389" s="T456">быть-PST1-3SG</ta>
            <ta e="T458" id="Seg_12390" s="T457">MOD</ta>
            <ta e="T459" id="Seg_12391" s="T458">один</ta>
            <ta e="T460" id="Seg_12392" s="T459">сын-EP-1SG.[NOM]</ta>
            <ta e="T461" id="Seg_12393" s="T460">ведь</ta>
            <ta e="T462" id="Seg_12394" s="T461">быть.больным-CVB.SEQ</ta>
            <ta e="T463" id="Seg_12395" s="T462">больница-ABL</ta>
            <ta e="T464" id="Seg_12396" s="T463">выйти-CVB.ANT</ta>
            <ta e="T465" id="Seg_12397" s="T464">вот</ta>
            <ta e="T466" id="Seg_12398" s="T465">приходить-PST2-EP-1SG</ta>
            <ta e="T467" id="Seg_12399" s="T466">MOD</ta>
            <ta e="T468" id="Seg_12400" s="T467">тот-3SG-1SG.[NOM]</ta>
            <ta e="T469" id="Seg_12401" s="T468">1SG.[NOM]</ta>
            <ta e="T470" id="Seg_12402" s="T469">тундра-DAT/LOC</ta>
            <ta e="T471" id="Seg_12403" s="T470">есть</ta>
            <ta e="T472" id="Seg_12404" s="T471">быть-PST1-3SG</ta>
            <ta e="T473" id="Seg_12405" s="T472">тот-3SG-1SG.[NOM]</ta>
            <ta e="T474" id="Seg_12406" s="T473">поселок-DAT/LOC</ta>
            <ta e="T475" id="Seg_12407" s="T474">входить-CVB.SEQ</ta>
            <ta e="T476" id="Seg_12408" s="T475">быть.больным-EP-PST2-3SG</ta>
            <ta e="T477" id="Seg_12409" s="T476">потом</ta>
            <ta e="T478" id="Seg_12410" s="T477">позже</ta>
            <ta e="T479" id="Seg_12411" s="T478">выйти-EP-PST2-3SG</ta>
            <ta e="T480" id="Seg_12412" s="T479">приходить-PTCP.PRS-1PL.[NOM]</ta>
            <ta e="T481" id="Seg_12413" s="T480">EMPH</ta>
            <ta e="T482" id="Seg_12414" s="T481">тот-ACC</ta>
            <ta e="T483" id="Seg_12415" s="T482">пурга-PL.[NOM]</ta>
            <ta e="T484" id="Seg_12416" s="T483">что-PL.[NOM]</ta>
            <ta e="T485" id="Seg_12417" s="T484">EMPH</ta>
            <ta e="T486" id="Seg_12418" s="T485">мешать-VBZ-CVB.SEQ</ta>
            <ta e="T487" id="Seg_12419" s="T486">EMPH-вот</ta>
            <ta e="T488" id="Seg_12420" s="T487">что.[NOM]</ta>
            <ta e="T489" id="Seg_12421" s="T488">много</ta>
            <ta e="T490" id="Seg_12422" s="T489">игрушки.[NOM]</ta>
            <ta e="T491" id="Seg_12423" s="T490">принести-PST2.NEG-EP-1SG</ta>
            <ta e="T492" id="Seg_12424" s="T491">прежний</ta>
            <ta e="T493" id="Seg_12425" s="T492">ребенок.[NOM]</ta>
            <ta e="T494" id="Seg_12426" s="T493">игрушки-3SG-ACC</ta>
            <ta e="T495" id="Seg_12427" s="T494">ведь</ta>
            <ta e="T496" id="Seg_12428" s="T495">потом</ta>
            <ta e="T497" id="Seg_12429" s="T496">другой</ta>
            <ta e="T498" id="Seg_12430" s="T497">показывать-NMNZ-DAT/LOC</ta>
            <ta e="T499" id="Seg_12431" s="T498">принести-PTCP.FUT</ta>
            <ta e="T500" id="Seg_12432" s="T499">быть-PST1-1SG</ta>
            <ta e="T501" id="Seg_12433" s="T500">раньше</ta>
            <ta e="T502" id="Seg_12434" s="T501">сам-1SG.[NOM]</ta>
            <ta e="T503" id="Seg_12435" s="T502">играть-CVB.SEQ</ta>
            <ta e="T504" id="Seg_12436" s="T503">создаваться-PST2-1SG</ta>
            <ta e="T505" id="Seg_12437" s="T504">вот</ta>
            <ta e="T506" id="Seg_12438" s="T505">потом</ta>
            <ta e="T518" id="Seg_12439" s="T517">вот</ta>
            <ta e="T519" id="Seg_12440" s="T518">нить-EP-INSTR</ta>
            <ta e="T520" id="Seg_12441" s="T519">мотать-PRS-1PL</ta>
            <ta e="T521" id="Seg_12442" s="T520">дыра-PROPR</ta>
            <ta e="T522" id="Seg_12443" s="T521">дерево-DAT/LOC</ta>
            <ta e="T523" id="Seg_12444" s="T522">голова-EP-INSTR</ta>
            <ta e="T524" id="Seg_12445" s="T523">наперерез</ta>
            <ta e="T525" id="Seg_12446" s="T524">стоять-CVB.SEQ</ta>
            <ta e="T526" id="Seg_12447" s="T525">вертеться-PRS.[3SG]</ta>
            <ta e="T527" id="Seg_12448" s="T526">тот-ACC</ta>
            <ta e="T528" id="Seg_12449" s="T527">видеть-TEMP-2SG</ta>
            <ta e="T529" id="Seg_12450" s="T528">только</ta>
            <ta e="T530" id="Seg_12451" s="T529">узнавать-FUT-2SG</ta>
            <ta e="T531" id="Seg_12452" s="T530">MOD</ta>
            <ta e="T532" id="Seg_12453" s="T531">как</ta>
            <ta e="T533" id="Seg_12454" s="T532">теперь</ta>
            <ta e="T534" id="Seg_12455" s="T533">рассказывать-CVB.SEQ-1SG</ta>
            <ta e="T535" id="Seg_12456" s="T534">как</ta>
            <ta e="T536" id="Seg_12457" s="T535">рассказывать-FUT-1SG=Q</ta>
            <ta e="T537" id="Seg_12458" s="T536">показывать-FUT-1SG</ta>
            <ta e="T538" id="Seg_12459" s="T537">тот</ta>
            <ta e="T539" id="Seg_12460" s="T538">видеть-TEMP-2SG</ta>
            <ta e="T540" id="Seg_12461" s="T539">знать-FUT.[IMP.2SG]</ta>
            <ta e="T559" id="Seg_12462" s="T558">AFFIRM</ta>
            <ta e="T560" id="Seg_12463" s="T559">знать-PRS-1SG</ta>
            <ta e="T561" id="Seg_12464" s="T560">такой-PL-ACC</ta>
            <ta e="T562" id="Seg_12465" s="T561">такой</ta>
            <ta e="T563" id="Seg_12466" s="T562">такой-PART</ta>
            <ta e="T564" id="Seg_12467" s="T563">просто</ta>
            <ta e="T565" id="Seg_12468" s="T564">любой.[NOM]</ta>
            <ta e="T566" id="Seg_12469" s="T565">песня-PART</ta>
            <ta e="T567" id="Seg_12470" s="T566">петь-FUT-1SG</ta>
            <ta e="T568" id="Seg_12471" s="T567">Q</ta>
            <ta e="T569" id="Seg_12472" s="T568">любой-PART</ta>
            <ta e="T570" id="Seg_12473" s="T569">любой.[NOM]</ta>
            <ta e="T571" id="Seg_12474" s="T570">песня-PART</ta>
            <ta e="T572" id="Seg_12475" s="T571">петь-FUT-1SG</ta>
            <ta e="T573" id="Seg_12476" s="T572">подожди</ta>
            <ta e="T574" id="Seg_12477" s="T573">приходить-CVB.SIM</ta>
            <ta e="T575" id="Seg_12478" s="T574">тянуть-NEG.[3SG]</ta>
            <ta e="T576" id="Seg_12479" s="T575">тот-3SG-2SG.[NOM]</ta>
            <ta e="T577" id="Seg_12480" s="T576">приходить-FUT</ta>
            <ta e="T578" id="Seg_12481" s="T577">приходить-PTCP.FUT-PART</ta>
            <ta e="T579" id="Seg_12482" s="T578">NEG</ta>
            <ta e="T580" id="Seg_12483" s="T579">быть-HAB.[3SG]</ta>
            <ta e="T581" id="Seg_12484" s="T580">мелодия-3PL-ACC</ta>
            <ta e="T582" id="Seg_12485" s="T581">помнить-NEG-1SG</ta>
            <ta e="T583" id="Seg_12486" s="T582">забывать-EP-PST2-1SG</ta>
            <ta e="T584" id="Seg_12487" s="T583">раньше</ta>
            <ta e="T585" id="Seg_12488" s="T584">однако</ta>
            <ta e="T586" id="Seg_12489" s="T585">3SG-INSTR</ta>
            <ta e="T587" id="Seg_12490" s="T586">заниматься-VBZ-FUT-1SG</ta>
            <ta e="T588" id="Seg_12491" s="T587">думать-CVB.SEQ-1SG</ta>
            <ta e="T589" id="Seg_12492" s="T588">очень</ta>
            <ta e="T590" id="Seg_12493" s="T589">делать-VBZ-NEG.CVB.SIM</ta>
            <ta e="T591" id="Seg_12494" s="T590">идти-EP-PST2-EP-1SG</ta>
            <ta e="T592" id="Seg_12495" s="T591">MOD</ta>
            <ta e="T593" id="Seg_12496" s="T592">тот.[NOM]</ta>
            <ta e="T594" id="Seg_12497" s="T593">из_за</ta>
            <ta e="T595" id="Seg_12498" s="T594">забывать-PRS-1SG</ta>
            <ta e="T611" id="Seg_12499" s="T610">тот.[NOM]</ta>
            <ta e="T612" id="Seg_12500" s="T611">умирать-PST2-3SG</ta>
            <ta e="T613" id="Seg_12501" s="T612">тот.[NOM]</ta>
            <ta e="T614" id="Seg_12502" s="T613">Бая.[NOM]</ta>
            <ta e="T615" id="Seg_12503" s="T614">говорить-CVB.SEQ</ta>
            <ta e="T616" id="Seg_12504" s="T615">девушка.[NOM]</ta>
            <ta e="T617" id="Seg_12505" s="T616">петь-PST2-3SG</ta>
            <ta e="T618" id="Seg_12506" s="T617">петь-PTCP.PST</ta>
            <ta e="T619" id="Seg_12507" s="T618">мальчик-3SG.[NOM]</ta>
            <ta e="T620" id="Seg_12508" s="T619">ведь</ta>
            <ta e="T621" id="Seg_12509" s="T620">умирать-PST2-3SG</ta>
            <ta e="T642" id="Seg_12510" s="T641">AFFIRM</ta>
            <ta e="T643" id="Seg_12511" s="T642">такой-3PL-INSTR</ta>
            <ta e="T644" id="Seg_12512" s="T643">Тоня-ACC</ta>
            <ta e="T645" id="Seg_12513" s="T644">с</ta>
            <ta e="T646" id="Seg_12514" s="T645">ведь</ta>
            <ta e="T647" id="Seg_12515" s="T646">ээ</ta>
            <ta e="T648" id="Seg_12516" s="T647">что-VBZ-FUT-1PL</ta>
            <ta e="T649" id="Seg_12517" s="T648">соревновать-PTCP.PRS-ACC</ta>
            <ta e="T650" id="Seg_12518" s="T649">ведь</ta>
            <ta e="T651" id="Seg_12519" s="T650">петь-FUT-1PL</ta>
            <ta e="T652" id="Seg_12520" s="T651">MOD</ta>
            <ta e="T653" id="Seg_12521" s="T652">просто</ta>
            <ta e="T654" id="Seg_12522" s="T653">дышать-CVB.SIM</ta>
            <ta e="T655" id="Seg_12523" s="T654">дышать-CVB.SIM</ta>
            <ta e="T656" id="Seg_12524" s="T655">ээ</ta>
            <ta e="T657" id="Seg_12525" s="T656">перехват.дыхания-ACC</ta>
            <ta e="T658" id="Seg_12526" s="T657">дышать-CVB.SIM</ta>
            <ta e="T659" id="Seg_12527" s="T658">дышать-PRS-2SG</ta>
            <ta e="T660" id="Seg_12528" s="T659">MOD</ta>
            <ta e="T663" id="Seg_12529" s="T662">петь.[IMP.2SG]</ta>
            <ta e="T664" id="Seg_12530" s="T663">говорить-TEMP-2SG</ta>
            <ta e="T665" id="Seg_12531" s="T664">петь-FUT-1SG</ta>
            <ta e="T666" id="Seg_12532" s="T665">просто</ta>
            <ta e="T667" id="Seg_12533" s="T666">дыхание.[NOM]</ta>
            <ta e="T668" id="Seg_12534" s="T667">отнимать-EP-RECP/COLL-PTCP.PRS</ta>
            <ta e="T669" id="Seg_12535" s="T668">стих.[NOM]</ta>
            <ta e="T685" id="Seg_12536" s="T684">тот-ACC</ta>
            <ta e="T686" id="Seg_12537" s="T685">MOD</ta>
            <ta e="T687" id="Seg_12538" s="T686">одинокий.[NOM]</ta>
            <ta e="T688" id="Seg_12539" s="T687">дыхание-INSTR</ta>
            <ta e="T689" id="Seg_12540" s="T688">петь-HAB-3PL</ta>
            <ta e="T690" id="Seg_12541" s="T689">говорят</ta>
            <ta e="T691" id="Seg_12542" s="T690">предок-PL.[NOM]</ta>
            <ta e="T692" id="Seg_12543" s="T691">одинокий.[NOM]</ta>
            <ta e="T693" id="Seg_12544" s="T692">дыхание-INSTR</ta>
            <ta e="T694" id="Seg_12545" s="T693">дыхание.[NOM]</ta>
            <ta e="T695" id="Seg_12546" s="T694">отнимать-EP-RECP/COLL-CVB.SEQ-3PL</ta>
            <ta e="T696" id="Seg_12547" s="T695">вот</ta>
            <ta e="T697" id="Seg_12548" s="T696">3PL.[NOM]</ta>
            <ta e="T698" id="Seg_12549" s="T697">игрушки-3PL.[NOM]</ta>
            <ta e="T699" id="Seg_12550" s="T698">тот</ta>
            <ta e="T700" id="Seg_12551" s="T699">быть-PST1-3SG</ta>
            <ta e="T701" id="Seg_12552" s="T700">этот</ta>
            <ta e="T702" id="Seg_12553" s="T701">артист-PL.[NOM]</ta>
            <ta e="T703" id="Seg_12554" s="T702">теперь</ta>
            <ta e="T704" id="Seg_12555" s="T703">играть-PTCP.PRS-3PL-ACC</ta>
            <ta e="T705" id="Seg_12556" s="T704">подобно</ta>
            <ta e="T706" id="Seg_12557" s="T705">тот.EMPH</ta>
            <ta e="T707" id="Seg_12558" s="T706">одинокий.[NOM]</ta>
            <ta e="T708" id="Seg_12559" s="T707">дыхание-INSTR</ta>
            <ta e="T709" id="Seg_12560" s="T708">доезжать-PTCP.HAB</ta>
            <ta e="T710" id="Seg_12561" s="T709">быть-PST1-3SG</ta>
            <ta e="T711" id="Seg_12562" s="T710">говорят</ta>
            <ta e="T712" id="Seg_12563" s="T711">Иннокентий.[NOM]</ta>
            <ta e="T713" id="Seg_12564" s="T712">говорить-CVB.SEQ</ta>
            <ta e="T714" id="Seg_12565" s="T713">человек.[NOM]</ta>
            <ta e="T715" id="Seg_12566" s="T714">тот.[NOM]</ta>
            <ta e="T716" id="Seg_12567" s="T715">тот-DAT/LOC</ta>
            <ta e="T728" id="Seg_12568" s="T727">тот.[NOM]</ta>
            <ta e="T729" id="Seg_12569" s="T728">мой</ta>
            <ta e="T730" id="Seg_12570" s="T729">дочь-EP-1SG.[NOM]</ta>
            <ta e="T731" id="Seg_12571" s="T730">тот.[NOM]</ta>
            <ta e="T761" id="Seg_12572" s="T760">AFFIRM</ta>
            <ta e="T762" id="Seg_12573" s="T761">вот</ta>
            <ta e="T763" id="Seg_12574" s="T762">тот</ta>
            <ta e="T764" id="Seg_12575" s="T763">1SG.[NOM]</ta>
            <ta e="T765" id="Seg_12576" s="T764">рассказывать-PTCP.PST</ta>
            <ta e="T766" id="Seg_12577" s="T765">сказка-1SG-ACC</ta>
            <ta e="T767" id="Seg_12578" s="T766">%%</ta>
            <ta e="T768" id="Seg_12579" s="T767">Таиска-ACC</ta>
            <ta e="T769" id="Seg_12580" s="T768">рассказывать-PST2-3PL</ta>
            <ta e="T770" id="Seg_12581" s="T769">1SG-ACC</ta>
            <ta e="T771" id="Seg_12582" s="T770">рассказывать-EP-CAUS-CVB.SEQ</ta>
            <ta e="T772" id="Seg_12583" s="T771">после</ta>
            <ta e="T773" id="Seg_12584" s="T772">приходить-PST2-3PL</ta>
            <ta e="T774" id="Seg_12585" s="T773">сюда</ta>
            <ta e="T775" id="Seg_12586" s="T774">приходить-CVB.PURP-3PL</ta>
            <ta e="T776" id="Seg_12587" s="T775">%%</ta>
            <ta e="T777" id="Seg_12588" s="T776">год-DAT/LOC</ta>
            <ta e="T778" id="Seg_12589" s="T777">приходить-PST2-3PL</ta>
            <ta e="T779" id="Seg_12590" s="T778">играть-CVB.PURP-3PL</ta>
            <ta e="T780" id="Seg_12591" s="T779">тот-3SG-3PL-ACC</ta>
            <ta e="T781" id="Seg_12592" s="T780">EMPH</ta>
            <ta e="T782" id="Seg_12593" s="T781">тот.[NOM]</ta>
            <ta e="T783" id="Seg_12594" s="T782">как</ta>
            <ta e="T784" id="Seg_12595" s="T783">как</ta>
            <ta e="T785" id="Seg_12596" s="T784">рассказывать-CVB.SEQ</ta>
            <ta e="T786" id="Seg_12597" s="T785">бросать-PTCP.PST</ta>
            <ta e="T787" id="Seg_12598" s="T786">быть-PST1-3PL</ta>
            <ta e="T788" id="Seg_12599" s="T787">потом</ta>
            <ta e="T789" id="Seg_12600" s="T788">остатки-3SG.[NOM]</ta>
            <ta e="T793" id="Seg_12601" s="T792">AFFIRM</ta>
            <ta e="T794" id="Seg_12602" s="T793">слышать-EP-PST2-EP-1SG</ta>
            <ta e="T795" id="Seg_12603" s="T794">тот-ACC</ta>
            <ta e="T796" id="Seg_12604" s="T795">слышать-CVB.SEQ-1SG</ta>
            <ta e="T797" id="Seg_12605" s="T796">делать-PRS-1SG</ta>
            <ta e="T798" id="Seg_12606" s="T797">тот.[NOM]</ta>
            <ta e="T800" id="Seg_12607" s="T799">тот-PL-ACC</ta>
            <ta e="T802" id="Seg_12608" s="T801">тот-PL-ACC</ta>
            <ta e="T803" id="Seg_12609" s="T802">приходить-PRS-3PL</ta>
            <ta e="T804" id="Seg_12610" s="T803">тот-PL-ACC</ta>
            <ta e="T805" id="Seg_12611" s="T804">ругать-PRS-1SG</ta>
            <ta e="T806" id="Seg_12612" s="T805">почему</ta>
            <ta e="T807" id="Seg_12613" s="T806">вот</ta>
            <ta e="T808" id="Seg_12614" s="T807">тот.[NOM]</ta>
            <ta e="T809" id="Seg_12615" s="T808">тот-INSTR-этот-INSTR</ta>
            <ta e="T810" id="Seg_12616" s="T809">рассказывать-PRS-2PL</ta>
            <ta e="T811" id="Seg_12617" s="T810">настоящий-SIM</ta>
            <ta e="T812" id="Seg_12618" s="T811">рассказывать-NEG-2PL</ta>
            <ta e="T813" id="Seg_12619" s="T812">сказка-2PL-ACC</ta>
            <ta e="T820" id="Seg_12620" s="T819">все.равно</ta>
            <ta e="T821" id="Seg_12621" s="T820">ведь</ta>
            <ta e="T822" id="Seg_12622" s="T821">замечать-EP-REFL-PRS.[3SG]</ta>
            <ta e="T823" id="Seg_12623" s="T822">потом</ta>
            <ta e="T824" id="Seg_12624" s="T823">основной-ACC</ta>
            <ta e="T825" id="Seg_12625" s="T824">ведь</ta>
            <ta e="T826" id="Seg_12626" s="T825">рассказывать-PRS-3PL</ta>
            <ta e="T849" id="Seg_12627" s="T848">AFFIRM</ta>
            <ta e="T850" id="Seg_12628" s="T849">есть</ta>
            <ta e="T851" id="Seg_12629" s="T850">EVID</ta>
            <ta e="T852" id="Seg_12630" s="T851">тот</ta>
            <ta e="T853" id="Seg_12631" s="T852">сказка-ACC</ta>
            <ta e="T854" id="Seg_12632" s="T853">знать-PTCP.PRS</ta>
            <ta e="T855" id="Seg_12633" s="T854">1SG.[NOM]</ta>
            <ta e="T856" id="Seg_12634" s="T855">старший.брат-EP-1SG.[NOM]</ta>
            <ta e="T857" id="Seg_12635" s="T856">есть</ta>
            <ta e="T858" id="Seg_12636" s="T857">старший.брат-PL-EP-1SG.[NOM]</ta>
            <ta e="T859" id="Seg_12637" s="T858">потом</ta>
            <ta e="T860" id="Seg_12638" s="T859">два-COLL</ta>
            <ta e="T861" id="Seg_12639" s="T860">EMPH</ta>
            <ta e="T862" id="Seg_12640" s="T861">знать-PRS-3PL</ta>
            <ta e="T863" id="Seg_12641" s="T862">два</ta>
            <ta e="T864" id="Seg_12642" s="T863">старший.брат-PROPR-1SG</ta>
            <ta e="T865" id="Seg_12643" s="T864">два-COLL</ta>
            <ta e="T866" id="Seg_12644" s="T865">EMPH</ta>
            <ta e="T867" id="Seg_12645" s="T866">знать-EP</ta>
            <ta e="T868" id="Seg_12646" s="T867">один.из.двух-3SG.[NOM]</ta>
            <ta e="T869" id="Seg_12647" s="T868">старый.[NOM]</ta>
            <ta e="T870" id="Seg_12648" s="T869">один.из.двух-3SG.[NOM]</ta>
            <ta e="T871" id="Seg_12649" s="T870">1SG-COMP</ta>
            <ta e="T872" id="Seg_12650" s="T871">ведь</ta>
            <ta e="T873" id="Seg_12651" s="T872">старший.[NOM]</ta>
            <ta e="T874" id="Seg_12652" s="T873">знать-PRS-3PL</ta>
            <ta e="T875" id="Seg_12653" s="T874">тот-PL.[NOM]</ta>
            <ta e="T876" id="Seg_12654" s="T875">сказка-PL-ACC</ta>
            <ta e="T877" id="Seg_12655" s="T876">песня-PROPR-PL-ACC</ta>
            <ta e="T878" id="Seg_12656" s="T877">да</ta>
            <ta e="T879" id="Seg_12657" s="T878">знать-PRS-3PL</ta>
            <ta e="T880" id="Seg_12658" s="T879">песня-POSS</ta>
            <ta e="T881" id="Seg_12659" s="T880">да</ta>
            <ta e="T882" id="Seg_12660" s="T881">NEG-ACC</ta>
            <ta e="T883" id="Seg_12661" s="T882">знать-EP</ta>
            <ta e="T888" id="Seg_12662" s="T887">просто</ta>
            <ta e="T889" id="Seg_12663" s="T888">человек-PL.[NOM]</ta>
            <ta e="T890" id="Seg_12664" s="T889">тот.[NOM]</ta>
            <ta e="T891" id="Seg_12665" s="T890">раньше</ta>
            <ta e="T892" id="Seg_12666" s="T891">сказка-AG.[NOM]</ta>
            <ta e="T893" id="Seg_12667" s="T892">человек-PL-1PL.[NOM]</ta>
            <ta e="T894" id="Seg_12668" s="T893">кончаться-PST1-3PL</ta>
            <ta e="T895" id="Seg_12669" s="T894">MOD</ta>
            <ta e="T896" id="Seg_12670" s="T895">теперь</ta>
            <ta e="T897" id="Seg_12671" s="T896">вправду</ta>
            <ta e="T898" id="Seg_12672" s="T897">вправду</ta>
            <ta e="T899" id="Seg_12673" s="T898">1SG.[NOM]</ta>
            <ta e="T900" id="Seg_12674" s="T899">знать-PTCP.PRS</ta>
            <ta e="T901" id="Seg_12675" s="T900">сказка-AG.[NOM]</ta>
            <ta e="T902" id="Seg_12676" s="T901">человек-PL-EP-1SG.[NOM]</ta>
            <ta e="T903" id="Seg_12677" s="T902">кончаться-PST1-3PL</ta>
            <ta e="T904" id="Seg_12678" s="T903">кто.[NOM]</ta>
            <ta e="T905" id="Seg_12679" s="T904">есть-3SG</ta>
            <ta e="T906" id="Seg_12680" s="T905">быть-FUT.[3SG]=Q</ta>
            <ta e="T907" id="Seg_12681" s="T906">теперь</ta>
            <ta e="T908" id="Seg_12682" s="T907">сказка-AG.[NOM]</ta>
            <ta e="T909" id="Seg_12683" s="T908">человек.[NOM]</ta>
            <ta e="T910" id="Seg_12684" s="T909">кто-ACC</ta>
            <ta e="T911" id="Seg_12685" s="T910">NEG</ta>
            <ta e="T912" id="Seg_12686" s="T911">знать-NEG-1SG</ta>
            <ta e="T913" id="Seg_12687" s="T912">теперь</ta>
            <ta e="T914" id="Seg_12688" s="T913">сказка-AG.[NOM]</ta>
            <ta e="T915" id="Seg_12689" s="T914">человек-ACC</ta>
            <ta e="T916" id="Seg_12690" s="T915">настоящий</ta>
            <ta e="T917" id="Seg_12691" s="T916">сказка-AG.[NOM]</ta>
            <ta e="T918" id="Seg_12692" s="T917">человек-ACC</ta>
            <ta e="T933" id="Seg_12693" s="T932">нет</ta>
            <ta e="T934" id="Seg_12694" s="T933">нет</ta>
            <ta e="T935" id="Seg_12695" s="T934">раньше</ta>
            <ta e="T936" id="Seg_12696" s="T935">MOD</ta>
            <ta e="T937" id="Seg_12697" s="T936">кто-3PL.[NOM]</ta>
            <ta e="T938" id="Seg_12698" s="T937">EMPH</ta>
            <ta e="T939" id="Seg_12699" s="T938">так-INTNS</ta>
            <ta e="T940" id="Seg_12700" s="T939">делать-NEG-PST1-3PL</ta>
            <ta e="T941" id="Seg_12701" s="T940">этот</ta>
            <ta e="T942" id="Seg_12702" s="T941">теперь</ta>
            <ta e="T943" id="Seg_12703" s="T942">этот</ta>
            <ta e="T944" id="Seg_12704" s="T943">прошлый.год-ADJZ-ABL</ta>
            <ta e="T945" id="Seg_12705" s="T944">ближе</ta>
            <ta e="T946" id="Seg_12706" s="T945">этот</ta>
            <ta e="T947" id="Seg_12707" s="T946">обыкновение.[NOM]</ta>
            <ta e="T948" id="Seg_12708" s="T947">найти-PST1-3PL</ta>
            <ta e="T949" id="Seg_12709" s="T948">этот</ta>
            <ta e="T950" id="Seg_12710" s="T949">прежний</ta>
            <ta e="T951" id="Seg_12711" s="T950">сказка.[NOM]</ta>
            <ta e="T952" id="Seg_12712" s="T951">тот.[NOM]</ta>
            <ta e="T953" id="Seg_12713" s="T952">из_за</ta>
            <ta e="T954" id="Seg_12714" s="T953">1PL.[NOM]</ta>
            <ta e="T955" id="Seg_12715" s="T954">совсем</ta>
            <ta e="T956" id="Seg_12716" s="T955">забывать-EP-PST2-1PL</ta>
            <ta e="T957" id="Seg_12717" s="T956">тот-ACC</ta>
            <ta e="T958" id="Seg_12718" s="T957">сказка-PL-1PL-ACC</ta>
            <ta e="T959" id="Seg_12719" s="T958">целый-3SG-ACC</ta>
            <ta e="T960" id="Seg_12720" s="T959">забывать-CVB.SEQ</ta>
            <ta e="T961" id="Seg_12721" s="T960">идти-PRS-1PL</ta>
            <ta e="T962" id="Seg_12722" s="T961">EMPH</ta>
            <ta e="T963" id="Seg_12723" s="T962">целый-3SG-ACC</ta>
            <ta e="T964" id="Seg_12724" s="T963">когда</ta>
            <ta e="T965" id="Seg_12725" s="T964">NEG</ta>
            <ta e="T966" id="Seg_12726" s="T965">рассказывать-NEG-2SG</ta>
            <ta e="T967" id="Seg_12727" s="T966">песня-ACC</ta>
            <ta e="T968" id="Seg_12728" s="T967">да</ta>
            <ta e="T969" id="Seg_12729" s="T968">забывать-PRS-2SG</ta>
            <ta e="T970" id="Seg_12730" s="T969">сказка-ACC</ta>
            <ta e="T971" id="Seg_12731" s="T970">да</ta>
            <ta e="T972" id="Seg_12732" s="T971">забывать-PRS-2SG</ta>
            <ta e="T973" id="Seg_12733" s="T972">история-ACC</ta>
            <ta e="T974" id="Seg_12734" s="T973">рассказывать-PTCP.PRS-3SG</ta>
            <ta e="T975" id="Seg_12735" s="T974">MOD</ta>
            <ta e="T976" id="Seg_12736" s="T975">Анисим</ta>
            <ta e="T977" id="Seg_12737" s="T976">старик.[NOM]</ta>
            <ta e="T978" id="Seg_12738" s="T977">есть</ta>
            <ta e="T979" id="Seg_12739" s="T978">сказка-AG.[NOM]</ta>
            <ta e="T980" id="Seg_12740" s="T979">тоже</ta>
            <ta e="T991" id="Seg_12741" s="T990">что</ta>
            <ta e="T992" id="Seg_12742" s="T991">знать-FUT.[3SG]=Q</ta>
            <ta e="T993" id="Seg_12743" s="T992">да</ta>
            <ta e="T994" id="Seg_12744" s="T993">тот</ta>
            <ta e="T995" id="Seg_12745" s="T994">3PL-ACC</ta>
            <ta e="T996" id="Seg_12746" s="T995">сказка.[NOM]</ta>
            <ta e="T997" id="Seg_12747" s="T996">пропитываться-PTCP.PRS-3SG.[3SG]</ta>
            <ta e="T998" id="Seg_12748" s="T997">Q</ta>
            <ta e="T999" id="Seg_12749" s="T998">3PL-DAT/LOC</ta>
            <ta e="T1000" id="Seg_12750" s="T999">что.[NOM]</ta>
            <ta e="T1001" id="Seg_12751" s="T1000">знать</ta>
            <ta e="T1002" id="Seg_12752" s="T1001">часто-часто</ta>
            <ta e="T1003" id="Seg_12753" s="T1002">теперь</ta>
            <ta e="T1004" id="Seg_12754" s="T1003">сказка-VBZ-EP-RECP/COLL-NEG-1PL</ta>
            <ta e="T1005" id="Seg_12755" s="T1004">EMPH</ta>
            <ta e="T1006" id="Seg_12756" s="T1005">просто</ta>
            <ta e="T1007" id="Seg_12757" s="T1006">рассказывать.[IMP.2SG]</ta>
            <ta e="T1008" id="Seg_12758" s="T1007">только</ta>
            <ta e="T1009" id="Seg_12759" s="T1008">говорить-TEMP-3PL</ta>
            <ta e="T1010" id="Seg_12760" s="T1009">рассказывать-PRS-1SG</ta>
            <ta e="T1011" id="Seg_12761" s="T1010">надо-TEMP-3PL</ta>
            <ta e="T1012" id="Seg_12762" s="T1011">рассказывать-EP-CAUS-TEMP-3PL</ta>
            <ta e="T1013" id="Seg_12763" s="T1012">только</ta>
            <ta e="T1014" id="Seg_12764" s="T1013">рассказывать-PRS-2SG</ta>
            <ta e="T1015" id="Seg_12765" s="T1014">EMPH</ta>
            <ta e="T1016" id="Seg_12766" s="T1015">так</ta>
            <ta e="T1017" id="Seg_12767" s="T1016">EMPH</ta>
            <ta e="T1018" id="Seg_12768" s="T1017">просто</ta>
            <ta e="T1019" id="Seg_12769" s="T1018">когда</ta>
            <ta e="T1020" id="Seg_12770" s="T1019">NEG</ta>
            <ta e="T1021" id="Seg_12771" s="T1020">сказка-VBZ-EP-RECP/COLL-NEG-1PL</ta>
            <ta e="T1022" id="Seg_12772" s="T1021">EMPH</ta>
            <ta e="T1042" id="Seg_12773" s="T1041">AFFIRM</ta>
            <ta e="T1054" id="Seg_12774" s="T1053">эй</ta>
            <ta e="T1055" id="Seg_12775" s="T1054">другой-PART</ta>
            <ta e="T1056" id="Seg_12776" s="T1055">быть-EP-NEG.CVB</ta>
            <ta e="T1075" id="Seg_12777" s="T1074">ребенок-1SG.[NOM]</ta>
            <ta e="T1076" id="Seg_12778" s="T1075">сторона-3SG-INSTR</ta>
            <ta e="T1077" id="Seg_12779" s="T1076">крошечный</ta>
            <ta e="T1078" id="Seg_12780" s="T1077">стих.[NOM]</ta>
            <ta e="T1079" id="Seg_12781" s="T1078">остатки-3SG-ACC</ta>
            <ta e="T1080" id="Seg_12782" s="T1079">делать-PST2-EP-1SG</ta>
            <ta e="T1081" id="Seg_12783" s="T1080">Айсюлю.[NOM]</ta>
            <ta e="T1082" id="Seg_12784" s="T1081">сторона-3SG-INSTR</ta>
            <ta e="T1083" id="Seg_12785" s="T1082">звезда-PL.[NOM]</ta>
            <ta e="T1084" id="Seg_12786" s="T1083">небо-DAT/LOC</ta>
            <ta e="T1085" id="Seg_12787" s="T1084">быть.видно-PRS-3PL</ta>
            <ta e="T1086" id="Seg_12788" s="T1085">спать.[IMP.2SG]</ta>
            <ta e="T1087" id="Seg_12789" s="T1086">спать.[IMP.2SG]</ta>
            <ta e="T1088" id="Seg_12790" s="T1087">Айсюлю.[NOM]</ta>
            <ta e="T1089" id="Seg_12791" s="T1088">время-1PL.[NOM]</ta>
            <ta e="T1090" id="Seg_12792" s="T1089">измениться-PST1-3SG</ta>
            <ta e="T1091" id="Seg_12793" s="T1090">странный</ta>
            <ta e="T1092" id="Seg_12794" s="T1091">другой</ta>
            <ta e="T1093" id="Seg_12795" s="T1092">час.[NOM]</ta>
            <ta e="T1094" id="Seg_12796" s="T1093">приходить-PST1-3SG</ta>
            <ta e="T1095" id="Seg_12797" s="T1094">быстрый-INTNS-ADVZ</ta>
            <ta e="T1096" id="Seg_12798" s="T1095">расти-FUT.[IMP.2SG]</ta>
            <ta e="T1097" id="Seg_12799" s="T1096">нить-1SG-ACC</ta>
            <ta e="T1098" id="Seg_12800" s="T1097">пгла-1SG-ACC</ta>
            <ta e="T1099" id="Seg_12801" s="T1098">держать-FUT.[IMP.2SG]</ta>
            <ta e="T1100" id="Seg_12802" s="T1099">1SG.[NOM]</ta>
            <ta e="T1101" id="Seg_12803" s="T1100">жизнь-1SG-ACC</ta>
            <ta e="T1102" id="Seg_12804" s="T1101">жизнь-VBZ-FUT.[IMP.2SG]</ta>
            <ta e="T1103" id="Seg_12805" s="T1102">человек-DAT/LOC</ta>
            <ta e="T1104" id="Seg_12806" s="T1103">человек.[NOM]</ta>
            <ta e="T1105" id="Seg_12807" s="T1104">говорить-EP-CAUS-FUT.[IMP.2SG]</ta>
            <ta e="T1106" id="Seg_12808" s="T1105">ребенок-DIM-EP-1SG.[NOM]</ta>
            <ta e="T1107" id="Seg_12809" s="T1106">ребенок-3SG-ACC</ta>
            <ta e="T1108" id="Seg_12810" s="T1107">колыбель-3SG-DAT/LOC</ta>
            <ta e="T1109" id="Seg_12811" s="T1108">качать-PRS-1SG</ta>
            <ta e="T1110" id="Seg_12812" s="T1109">место.около-3SG-DAT/LOC</ta>
            <ta e="T1111" id="Seg_12813" s="T1110">сидеть-PRS-1SG</ta>
            <ta e="T1112" id="Seg_12814" s="T1111">потихоньку</ta>
            <ta e="T1113" id="Seg_12815" s="T1112">петь-EP-MED-PST1-1SG</ta>
         </annotation>
         <annotation name="mc" tierref="mc-UkET">
            <ta e="T23" id="Seg_12816" s="T21">pers.[pro:case]</ta>
            <ta e="T25" id="Seg_12817" s="T23">conj</ta>
            <ta e="T26" id="Seg_12818" s="T25">emphpro-pro:(poss).[pro:case]</ta>
            <ta e="T27" id="Seg_12819" s="T26">v-v:ptcp</ta>
            <ta e="T28" id="Seg_12820" s="T27">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T29" id="Seg_12821" s="T28">dempro.[pro:case]</ta>
            <ta e="T30" id="Seg_12822" s="T29">post</ta>
            <ta e="T31" id="Seg_12823" s="T30">v-v:tense-v:poss.pn</ta>
            <ta e="T32" id="Seg_12824" s="T31">n.[n:case]</ta>
            <ta e="T33" id="Seg_12825" s="T32">post</ta>
            <ta e="T34" id="Seg_12826" s="T33">dempro.[pro:case]</ta>
            <ta e="T35" id="Seg_12827" s="T34">post</ta>
            <ta e="T37" id="Seg_12828" s="T36">que.[pro:case]</ta>
            <ta e="T38" id="Seg_12829" s="T37">post</ta>
            <ta e="T39" id="Seg_12830" s="T38">interj</ta>
            <ta e="T40" id="Seg_12831" s="T39">adv</ta>
            <ta e="T41" id="Seg_12832" s="T40">propr.[n:case]</ta>
            <ta e="T42" id="Seg_12833" s="T41">adv-n:poss-n:case</ta>
            <ta e="T43" id="Seg_12834" s="T42">dempro.[pro:case]</ta>
            <ta e="T44" id="Seg_12835" s="T43">n.[n:case]</ta>
            <ta e="T45" id="Seg_12836" s="T44">post</ta>
            <ta e="T46" id="Seg_12837" s="T45">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T47" id="Seg_12838" s="T46">adv</ta>
            <ta e="T48" id="Seg_12839" s="T47">n-n:case</ta>
            <ta e="T49" id="Seg_12840" s="T48">v-v:cvb-v:pred.pn</ta>
            <ta e="T50" id="Seg_12841" s="T49">n.[n:case]</ta>
            <ta e="T51" id="Seg_12842" s="T50">post</ta>
            <ta e="T52" id="Seg_12843" s="T51">n-n&gt;v-v:tense-v:(ins)-v:(poss)</ta>
            <ta e="T53" id="Seg_12844" s="T52">n-n:(num)-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T54" id="Seg_12845" s="T53">ptcl</ta>
            <ta e="T55" id="Seg_12846" s="T54">que.[pro:case]</ta>
            <ta e="T56" id="Seg_12847" s="T55">que</ta>
            <ta e="T57" id="Seg_12848" s="T56">v-v:cvb</ta>
            <ta e="T58" id="Seg_12849" s="T57">ptcl</ta>
            <ta e="T59" id="Seg_12850" s="T58">que-pro:case</ta>
            <ta e="T60" id="Seg_12851" s="T59">v-v:tense-v:poss.pn=ptcl</ta>
            <ta e="T61" id="Seg_12852" s="T60">cardnum</ta>
            <ta e="T62" id="Seg_12853" s="T61">n-n:(poss).[n:case]</ta>
            <ta e="T63" id="Seg_12854" s="T62">dempro.[pro:case]</ta>
            <ta e="T64" id="Seg_12855" s="T63">n-n:case</ta>
            <ta e="T65" id="Seg_12856" s="T64">v-v:tense.[v:pred.pn]</ta>
            <ta e="T66" id="Seg_12857" s="T65">cardnum</ta>
            <ta e="T67" id="Seg_12858" s="T66">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T68" id="Seg_12859" s="T67">n-n&gt;v-v:tense.[v:pred.pn]</ta>
            <ta e="T69" id="Seg_12860" s="T68">cardnum</ta>
            <ta e="T70" id="Seg_12861" s="T69">interj</ta>
            <ta e="T71" id="Seg_12862" s="T70">cardnum</ta>
            <ta e="T72" id="Seg_12863" s="T71">n-n:(poss).[n:case]</ta>
            <ta e="T73" id="Seg_12864" s="T72">dempro.[pro:case]</ta>
            <ta e="T74" id="Seg_12865" s="T73">propr-n:case</ta>
            <ta e="T76" id="Seg_12866" s="T75">ptcl</ta>
            <ta e="T77" id="Seg_12867" s="T76">adj.[n:case]</ta>
            <ta e="T78" id="Seg_12868" s="T77">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T79" id="Seg_12869" s="T78">propr-n:case</ta>
            <ta e="T80" id="Seg_12870" s="T79">n-n:case</ta>
            <ta e="T81" id="Seg_12871" s="T80">n-n&gt;v-v:tense.[v:pred.pn]</ta>
            <ta e="T82" id="Seg_12872" s="T81">propr</ta>
            <ta e="T84" id="Seg_12873" s="T83">dempro</ta>
            <ta e="T85" id="Seg_12874" s="T84">adj</ta>
            <ta e="T86" id="Seg_12875" s="T85">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T87" id="Seg_12876" s="T86">n-n:(poss).[n:case]</ta>
            <ta e="T88" id="Seg_12877" s="T87">adj.[n:case]</ta>
            <ta e="T89" id="Seg_12878" s="T88">dempro-pro:(poss)-pro:(poss).[pro:case]</ta>
            <ta e="T90" id="Seg_12879" s="T89">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T91" id="Seg_12880" s="T90">propr</ta>
            <ta e="T92" id="Seg_12881" s="T91">dempro</ta>
            <ta e="T93" id="Seg_12882" s="T92">n-n:case</ta>
            <ta e="T94" id="Seg_12883" s="T93">n-n&gt;v-v:tense.[v:pred.pn]</ta>
            <ta e="T95" id="Seg_12884" s="T94">cardnum</ta>
            <ta e="T96" id="Seg_12885" s="T95">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T97" id="Seg_12886" s="T96">n-n:case</ta>
            <ta e="T98" id="Seg_12887" s="T97">v-v:ptcp</ta>
            <ta e="T99" id="Seg_12888" s="T98">v-v:tense-v:poss.pn</ta>
            <ta e="T100" id="Seg_12889" s="T99">dempro-pro:(poss)-pro:(poss).[pro:case]</ta>
            <ta e="T101" id="Seg_12890" s="T100">interj</ta>
            <ta e="T102" id="Seg_12891" s="T101">dempro</ta>
            <ta e="T103" id="Seg_12892" s="T102">n-n:(poss).[n:case]</ta>
            <ta e="T104" id="Seg_12893" s="T103">que-pro:(poss).[pro:case]</ta>
            <ta e="T105" id="Seg_12894" s="T104">v-v:cvb</ta>
            <ta e="T106" id="Seg_12895" s="T105">adv</ta>
            <ta e="T107" id="Seg_12896" s="T106">ptcl</ta>
            <ta e="T108" id="Seg_12897" s="T107">n-n&gt;v-v:cvb</ta>
            <ta e="T109" id="Seg_12898" s="T108">v-v:cvb</ta>
            <ta e="T110" id="Seg_12899" s="T109">adv</ta>
            <ta e="T111" id="Seg_12900" s="T110">v-v:cvb</ta>
            <ta e="T112" id="Seg_12901" s="T111">propr.[n:case]</ta>
            <ta e="T113" id="Seg_12902" s="T112">post</ta>
            <ta e="T114" id="Seg_12903" s="T113">v-v:tense.[v:pred.pn]</ta>
            <ta e="T115" id="Seg_12904" s="T114">dempro</ta>
            <ta e="T116" id="Seg_12905" s="T115">interj</ta>
            <ta e="T117" id="Seg_12906" s="T116">propr-n:case</ta>
            <ta e="T118" id="Seg_12907" s="T117">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T119" id="Seg_12908" s="T118">v-v:tense.[v:pred.pn]</ta>
            <ta e="T120" id="Seg_12909" s="T119">adv</ta>
            <ta e="T121" id="Seg_12910" s="T120">adv</ta>
            <ta e="T122" id="Seg_12911" s="T121">cardnum</ta>
            <ta e="T123" id="Seg_12912" s="T122">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T124" id="Seg_12913" s="T123">n.[n:case]</ta>
            <ta e="T125" id="Seg_12914" s="T124">v-v:tense-v:poss.pn</ta>
            <ta e="T126" id="Seg_12915" s="T125">cardnum</ta>
            <ta e="T127" id="Seg_12916" s="T126">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T128" id="Seg_12917" s="T127">dempro.[pro:case]</ta>
            <ta e="T129" id="Seg_12918" s="T128">n-n:(num)-n:case</ta>
            <ta e="T131" id="Seg_12919" s="T130">adj-n:poss-n:case</ta>
            <ta e="T132" id="Seg_12920" s="T131">v</ta>
            <ta e="T133" id="Seg_12921" s="T132">que-que&gt;v-v:tense.[v:pred.pn]</ta>
            <ta e="T134" id="Seg_12922" s="T133">v-v:tense.[v:pred.pn]</ta>
            <ta e="T135" id="Seg_12923" s="T134">n-n:(num)-n:case</ta>
            <ta e="T136" id="Seg_12924" s="T135">v-v:tense.[v:pred.pn]</ta>
            <ta e="T137" id="Seg_12925" s="T136">v-v:(ins)-v&gt;v-v:tense.[v:pred.pn]</ta>
            <ta e="T138" id="Seg_12926" s="T137">adv</ta>
            <ta e="T139" id="Seg_12927" s="T138">ptcl</ta>
            <ta e="T140" id="Seg_12928" s="T139">cardnum</ta>
            <ta e="T141" id="Seg_12929" s="T140">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T143" id="Seg_12930" s="T142">que-pro:case</ta>
            <ta e="T144" id="Seg_12931" s="T143">v-v:ptcp</ta>
            <ta e="T145" id="Seg_12932" s="T144">v-v:tense-v:poss.pn</ta>
            <ta e="T146" id="Seg_12933" s="T145">n.[n:case]</ta>
            <ta e="T147" id="Seg_12934" s="T146">n-n:poss-n:case</ta>
            <ta e="T148" id="Seg_12935" s="T147">adv</ta>
            <ta e="T149" id="Seg_12936" s="T148">emphpro-pro:(poss).[pro:case]</ta>
            <ta e="T150" id="Seg_12937" s="T149">v-v:cvb-v:pred.pn</ta>
            <ta e="T151" id="Seg_12938" s="T150">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T152" id="Seg_12939" s="T151">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T153" id="Seg_12940" s="T152">v-v:(ins)-v&gt;v-v:cvb-v:pred.pn</ta>
            <ta e="T154" id="Seg_12941" s="T153">n-n:case</ta>
            <ta e="T155" id="Seg_12942" s="T154">v-v:tense-v:pred.pn</ta>
            <ta e="T156" id="Seg_12943" s="T155">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T157" id="Seg_12944" s="T156">adv</ta>
            <ta e="T158" id="Seg_12945" s="T157">n.[n:case]</ta>
            <ta e="T159" id="Seg_12946" s="T158">v-v:tense-v:poss.pn</ta>
            <ta e="T160" id="Seg_12947" s="T159">ptcl</ta>
            <ta e="T162" id="Seg_12948" s="T161">n-n:case</ta>
            <ta e="T163" id="Seg_12949" s="T162">interj</ta>
            <ta e="T164" id="Seg_12950" s="T163">propr-n:case</ta>
            <ta e="T165" id="Seg_12951" s="T164">adv</ta>
            <ta e="T166" id="Seg_12952" s="T165">n-n:case</ta>
            <ta e="T167" id="Seg_12953" s="T166">adj-n:poss-n:case</ta>
            <ta e="T168" id="Seg_12954" s="T167">v-v:ptcp</ta>
            <ta e="T169" id="Seg_12955" s="T168">v-v:tense-v:poss.pn</ta>
            <ta e="T170" id="Seg_12956" s="T169">adv</ta>
            <ta e="T171" id="Seg_12957" s="T170">v-v:cvb</ta>
            <ta e="T172" id="Seg_12958" s="T171">n-n:(poss).[n:case]</ta>
            <ta e="T173" id="Seg_12959" s="T172">v-v:cvb</ta>
            <ta e="T174" id="Seg_12960" s="T173">dempro</ta>
            <ta e="T175" id="Seg_12961" s="T174">n-n:(ins)-n:case</ta>
            <ta e="T176" id="Seg_12962" s="T175">ptcl</ta>
            <ta e="T177" id="Seg_12963" s="T176">v-v:tense-v:poss.pn</ta>
            <ta e="T178" id="Seg_12964" s="T177">dempro-pro:case</ta>
            <ta e="T179" id="Seg_12965" s="T178">adj-adj&gt;v-v:cvb</ta>
            <ta e="T180" id="Seg_12966" s="T179">adv</ta>
            <ta e="T181" id="Seg_12967" s="T180">v-v&gt;v-v:cvb</ta>
            <ta e="T182" id="Seg_12968" s="T181">n-n:case</ta>
            <ta e="T183" id="Seg_12969" s="T182">v-v:tense-v:poss.pn</ta>
            <ta e="T184" id="Seg_12970" s="T183">dempro</ta>
            <ta e="T185" id="Seg_12971" s="T184">adv</ta>
            <ta e="T186" id="Seg_12972" s="T185">dempro</ta>
            <ta e="T188" id="Seg_12973" s="T186">n-n:(num)-n:case</ta>
            <ta e="T190" id="Seg_12974" s="T188">v-v:tense-v:poss.pn</ta>
            <ta e="T203" id="Seg_12975" s="T202">ptcl</ta>
            <ta e="T204" id="Seg_12976" s="T203">adv</ta>
            <ta e="T205" id="Seg_12977" s="T204">dempro</ta>
            <ta e="T206" id="Seg_12978" s="T205">n-n:(num)-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T207" id="Seg_12979" s="T206">n-n:(poss).[n:case]</ta>
            <ta e="T208" id="Seg_12980" s="T207">n-n:(num)-n:poss-n:case</ta>
            <ta e="T209" id="Seg_12981" s="T208">v-v:cvb-v:pred.pn</ta>
            <ta e="T210" id="Seg_12982" s="T209">dempro</ta>
            <ta e="T211" id="Seg_12983" s="T210">n-n:case</ta>
            <ta e="T212" id="Seg_12984" s="T211">v-v:ptcp</ta>
            <ta e="T213" id="Seg_12985" s="T212">n-n:(poss).[n:case]</ta>
            <ta e="T214" id="Seg_12986" s="T213">adj-n:(poss)</ta>
            <ta e="T215" id="Seg_12987" s="T214">cardnum</ta>
            <ta e="T216" id="Seg_12988" s="T215">n-n&gt;adj.[n:case]</ta>
            <ta e="T217" id="Seg_12989" s="T216">ptcl</ta>
            <ta e="T218" id="Seg_12990" s="T217">dempro-pro:(poss)-pro:(num)-pro:(ins)-pro:(poss).[pro:case]</ta>
            <ta e="T219" id="Seg_12991" s="T218">n-n:poss-n:case</ta>
            <ta e="T220" id="Seg_12992" s="T219">v-v:cvb</ta>
            <ta e="T221" id="Seg_12993" s="T220">v-v:ptcp</ta>
            <ta e="T222" id="Seg_12994" s="T221">v-v:tense-v:poss.pn</ta>
            <ta e="T223" id="Seg_12995" s="T222">ptcl</ta>
            <ta e="T224" id="Seg_12996" s="T223">adv</ta>
            <ta e="T225" id="Seg_12997" s="T224">ptcl</ta>
            <ta e="T226" id="Seg_12998" s="T225">dempro</ta>
            <ta e="T227" id="Seg_12999" s="T226">n-n:(poss).[n:case]</ta>
            <ta e="T228" id="Seg_13000" s="T227">dempro</ta>
            <ta e="T229" id="Seg_13001" s="T228">propr-n:(poss).[n:case]</ta>
            <ta e="T230" id="Seg_13002" s="T229">v-v:cvb</ta>
            <ta e="T231" id="Seg_13003" s="T230">v-v:tense-v:poss.pn</ta>
            <ta e="T232" id="Seg_13004" s="T231">dempro</ta>
            <ta e="T233" id="Seg_13005" s="T232">n-n:case</ta>
            <ta e="T234" id="Seg_13006" s="T233">n-n:case</ta>
            <ta e="T235" id="Seg_13007" s="T234">v-v:cvb</ta>
            <ta e="T236" id="Seg_13008" s="T235">v-v:tense-v:pred.pn</ta>
            <ta e="T237" id="Seg_13009" s="T236">ptcl</ta>
            <ta e="T238" id="Seg_13010" s="T237">dempro</ta>
            <ta e="T239" id="Seg_13011" s="T238">cardnum</ta>
            <ta e="T240" id="Seg_13012" s="T239">n-n:poss-n:case</ta>
            <ta e="T241" id="Seg_13013" s="T240">dempro</ta>
            <ta e="T242" id="Seg_13014" s="T241">n-n:(poss).[n:case]</ta>
            <ta e="T243" id="Seg_13015" s="T242">dempro.[pro:case]</ta>
            <ta e="T244" id="Seg_13016" s="T243">post</ta>
            <ta e="T245" id="Seg_13017" s="T244">ptcl</ta>
            <ta e="T246" id="Seg_13018" s="T245">n-n:(poss).[n:case]</ta>
            <ta e="T247" id="Seg_13019" s="T246">v-v:tense-v:poss.pn</ta>
            <ta e="T248" id="Seg_13020" s="T247">ptcl</ta>
            <ta e="T249" id="Seg_13021" s="T248">dempro.[pro:case]</ta>
            <ta e="T250" id="Seg_13022" s="T249">n.[n:case]</ta>
            <ta e="T251" id="Seg_13023" s="T250">v-v:tense-v:poss.pn</ta>
            <ta e="T252" id="Seg_13024" s="T251">dempro-pro:(poss)-pro:(poss).[pro:case]</ta>
            <ta e="T253" id="Seg_13025" s="T252">v-v:tense-v:poss.pn</ta>
            <ta e="T254" id="Seg_13026" s="T253">dempro.[pro:case]</ta>
            <ta e="T255" id="Seg_13027" s="T254">post</ta>
            <ta e="T256" id="Seg_13028" s="T255">dempro</ta>
            <ta e="T257" id="Seg_13029" s="T256">que</ta>
            <ta e="T258" id="Seg_13030" s="T257">cardnum-n:case</ta>
            <ta e="T259" id="Seg_13031" s="T258">cardnum</ta>
            <ta e="T260" id="Seg_13032" s="T259">n.[n:case]</ta>
            <ta e="T261" id="Seg_13033" s="T260">ptcl</ta>
            <ta e="T262" id="Seg_13034" s="T261">n-n:(num)-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T263" id="Seg_13035" s="T262">n-n:(poss).[n:case]</ta>
            <ta e="T264" id="Seg_13036" s="T263">que</ta>
            <ta e="T265" id="Seg_13037" s="T264">ptcl</ta>
            <ta e="T266" id="Seg_13038" s="T265">v-v:(neg)-v:pred.pn</ta>
            <ta e="T267" id="Seg_13039" s="T266">ptcl</ta>
            <ta e="T268" id="Seg_13040" s="T267">adv</ta>
            <ta e="T269" id="Seg_13041" s="T268">n-n:(num)-n:case</ta>
            <ta e="T270" id="Seg_13042" s="T269">n-n:(num)-n:case</ta>
            <ta e="T271" id="Seg_13043" s="T270">post</ta>
            <ta e="T272" id="Seg_13044" s="T271">n-n&gt;adj.[n:case]</ta>
            <ta e="T273" id="Seg_13045" s="T272">v-v:mood-v:pred.pn</ta>
            <ta e="T274" id="Seg_13046" s="T273">n-n:(num)-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T275" id="Seg_13047" s="T274">adv</ta>
            <ta e="T276" id="Seg_13048" s="T275">ptcl</ta>
            <ta e="T277" id="Seg_13049" s="T276">v-v:tense-v:pred.pn</ta>
            <ta e="T300" id="Seg_13050" s="T299">ptcl</ta>
            <ta e="T301" id="Seg_13051" s="T300">v-v:mood-v:pred.pn</ta>
            <ta e="T302" id="Seg_13052" s="T301">adj-n&gt;n-n:case</ta>
            <ta e="T303" id="Seg_13053" s="T302">ptcl</ta>
            <ta e="T304" id="Seg_13054" s="T303">adv</ta>
            <ta e="T305" id="Seg_13055" s="T304">adj-adj&gt;adv</ta>
            <ta e="T306" id="Seg_13056" s="T305">ptcl</ta>
            <ta e="T307" id="Seg_13057" s="T306">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T308" id="Seg_13058" s="T307">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T309" id="Seg_13059" s="T308">ptcl</ta>
            <ta e="T310" id="Seg_13060" s="T309">adj</ta>
            <ta e="T311" id="Seg_13061" s="T310">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T312" id="Seg_13062" s="T311">ptcl</ta>
            <ta e="T313" id="Seg_13063" s="T312">n-n:(num)-n:(poss).[n:case]</ta>
            <ta e="T314" id="Seg_13064" s="T313">n-n:case</ta>
            <ta e="T315" id="Seg_13065" s="T314">v-v:tense-v:poss.pn</ta>
            <ta e="T316" id="Seg_13066" s="T315">adj.[n:case]</ta>
            <ta e="T317" id="Seg_13067" s="T316">n-n:(num)-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T318" id="Seg_13068" s="T317">n-n:(poss).[n:case]</ta>
            <ta e="T319" id="Seg_13069" s="T318">v-v:cvb</ta>
            <ta e="T320" id="Seg_13070" s="T319">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T321" id="Seg_13071" s="T320">n-n:case</ta>
            <ta e="T322" id="Seg_13072" s="T321">dempro-pro:(num)-pro:case</ta>
            <ta e="T323" id="Seg_13073" s="T322">dempro</ta>
            <ta e="T324" id="Seg_13074" s="T323">adj-n&gt;n-n:case</ta>
            <ta e="T325" id="Seg_13075" s="T324">v-v:tense-v:pred.pn</ta>
            <ta e="T326" id="Seg_13076" s="T325">ptcl</ta>
            <ta e="T327" id="Seg_13077" s="T326">adj-n:case-adj-n:case</ta>
            <ta e="T328" id="Seg_13078" s="T327">n.[n:case]</ta>
            <ta e="T329" id="Seg_13079" s="T328">v-v:cvb</ta>
            <ta e="T330" id="Seg_13080" s="T329">v-v:cvb</ta>
            <ta e="T331" id="Seg_13081" s="T330">v-v:mood-v:pred.pn</ta>
            <ta e="T332" id="Seg_13082" s="T331">ptcl</ta>
            <ta e="T333" id="Seg_13083" s="T332">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T334" id="Seg_13084" s="T333">v.[v:mood.pn]</ta>
            <ta e="T335" id="Seg_13085" s="T334">dempro-pro:case</ta>
            <ta e="T336" id="Seg_13086" s="T335">n-n&gt;v.[v:mood.pn]</ta>
            <ta e="T337" id="Seg_13087" s="T336">dempro-pro:case</ta>
            <ta e="T338" id="Seg_13088" s="T337">v.[v:mood.pn]</ta>
            <ta e="T339" id="Seg_13089" s="T338">dempro-pro:case</ta>
            <ta e="T340" id="Seg_13090" s="T339">v.[v:mood.pn]</ta>
            <ta e="T341" id="Seg_13091" s="T340">dempro-pro:(num)-pro:case</ta>
            <ta e="T342" id="Seg_13092" s="T341">v-v:cvb-v:pred.pn</ta>
            <ta e="T343" id="Seg_13093" s="T342">ptcl</ta>
            <ta e="T344" id="Seg_13094" s="T343">v-v:(ins)-v&gt;v-v:(ins)-v:mood.pn</ta>
            <ta e="T345" id="Seg_13095" s="T344">v-v:(ins)-v&gt;v-v:(ins)-v:mood.pn</ta>
            <ta e="T346" id="Seg_13096" s="T345">ptcl</ta>
            <ta e="T347" id="Seg_13097" s="T346">adj.[n:case]</ta>
            <ta e="T348" id="Seg_13098" s="T347">ptcl</ta>
            <ta e="T356" id="Seg_13099" s="T355">interj</ta>
            <ta e="T357" id="Seg_13100" s="T356">posspr</ta>
            <ta e="T358" id="Seg_13101" s="T357">n-n:(num)-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T359" id="Seg_13102" s="T358">adj-n:(poss).[n:case]</ta>
            <ta e="T360" id="Seg_13103" s="T359">n-n&gt;n.[n:case]</ta>
            <ta e="T361" id="Seg_13104" s="T360">v-v:tense-v:pred.pn</ta>
            <ta e="T362" id="Seg_13105" s="T361">v-v:ptcp</ta>
            <ta e="T363" id="Seg_13106" s="T362">conj</ta>
            <ta e="T364" id="Seg_13107" s="T363">v-v:tense-v:pred.pn</ta>
            <ta e="T365" id="Seg_13108" s="T364">n.[n:case]</ta>
            <ta e="T366" id="Seg_13109" s="T365">v-v:(ins)</ta>
            <ta e="T367" id="Seg_13110" s="T366">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T368" id="Seg_13111" s="T367">n-n&gt;adj.[n:case]</ta>
            <ta e="T369" id="Seg_13112" s="T368">conj</ta>
            <ta e="T370" id="Seg_13113" s="T369">n-n:case</ta>
            <ta e="T371" id="Seg_13114" s="T370">v-v:mood.[v:pred.pn]</ta>
            <ta e="T372" id="Seg_13115" s="T371">adv</ta>
            <ta e="T373" id="Seg_13116" s="T372">conj</ta>
            <ta e="T374" id="Seg_13117" s="T373">n-n:case</ta>
            <ta e="T375" id="Seg_13118" s="T374">v-v:(ins)</ta>
            <ta e="T376" id="Seg_13119" s="T375">adv</ta>
            <ta e="T377" id="Seg_13120" s="T376">adv-adv&gt;adj</ta>
            <ta e="T378" id="Seg_13121" s="T377">n-n:case</ta>
            <ta e="T379" id="Seg_13122" s="T378">ptcl</ta>
            <ta e="T380" id="Seg_13123" s="T379">n-n:case</ta>
            <ta e="T381" id="Seg_13124" s="T380">v-v:tense-v:pred.pn</ta>
            <ta e="T382" id="Seg_13125" s="T381">ptcl</ta>
            <ta e="T383" id="Seg_13126" s="T382">adv</ta>
            <ta e="T384" id="Seg_13127" s="T383">ptcl</ta>
            <ta e="T385" id="Seg_13128" s="T384">propr-n:case</ta>
            <ta e="T386" id="Seg_13129" s="T385">v-v:tense-v:pred.pn</ta>
            <ta e="T387" id="Seg_13130" s="T386">ptcl</ta>
            <ta e="T388" id="Seg_13131" s="T387">n.[n:case]</ta>
            <ta e="T389" id="Seg_13132" s="T388">n-n:(ins)-n:case</ta>
            <ta e="T390" id="Seg_13133" s="T389">v-v:tense-v:pred.pn</ta>
            <ta e="T391" id="Seg_13134" s="T390">n-n:(num)-n:(ins)-n:case</ta>
            <ta e="T392" id="Seg_13135" s="T391">dempro.[pro:case]</ta>
            <ta e="T393" id="Seg_13136" s="T392">n.[n:case]</ta>
            <ta e="T394" id="Seg_13137" s="T393">v-v:cvb</ta>
            <ta e="T395" id="Seg_13138" s="T394">v-v:tense-v:pred.pn</ta>
            <ta e="T396" id="Seg_13139" s="T395">adv</ta>
            <ta e="T397" id="Seg_13140" s="T396">v-v:mood-v:temp.pn</ta>
            <ta e="T398" id="Seg_13141" s="T397">n.[n:case]</ta>
            <ta e="T399" id="Seg_13142" s="T398">v-v:cvb</ta>
            <ta e="T400" id="Seg_13143" s="T399">n-n:(ins)-n:case</ta>
            <ta e="T401" id="Seg_13144" s="T400">v-v:tense-v:poss.pn</ta>
            <ta e="T402" id="Seg_13145" s="T401">n.[n:case]</ta>
            <ta e="T403" id="Seg_13146" s="T402">n.[n:case]</ta>
            <ta e="T404" id="Seg_13147" s="T403">ptcl</ta>
            <ta e="T405" id="Seg_13148" s="T404">n-n&gt;v-v:cvb</ta>
            <ta e="T406" id="Seg_13149" s="T405">v-v:tense-v:poss.pn</ta>
            <ta e="T407" id="Seg_13150" s="T406">adv</ta>
            <ta e="T408" id="Seg_13151" s="T407">que</ta>
            <ta e="T409" id="Seg_13152" s="T408">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T410" id="Seg_13153" s="T409">n-n:case</ta>
            <ta e="T411" id="Seg_13154" s="T410">v-v:(ins)-v&gt;v-v:cvb-v:pred.pn</ta>
            <ta e="T412" id="Seg_13155" s="T411">n.[n:case]</ta>
            <ta e="T413" id="Seg_13156" s="T412">v-v:tense-v:poss.pn</ta>
            <ta e="T414" id="Seg_13157" s="T413">dempro.[pro:case]</ta>
            <ta e="T415" id="Seg_13158" s="T414">post-post&gt;post</ta>
            <ta e="T416" id="Seg_13159" s="T415">n-n:case</ta>
            <ta e="T417" id="Seg_13160" s="T416">v-v:tense-v:pred.pn</ta>
            <ta e="T418" id="Seg_13161" s="T417">ptcl</ta>
            <ta e="T419" id="Seg_13162" s="T418">adv-adv&gt;adj</ta>
            <ta e="T420" id="Seg_13163" s="T419">n.[n:case]</ta>
            <ta e="T421" id="Seg_13164" s="T420">n-n&gt;adv</ta>
            <ta e="T422" id="Seg_13165" s="T421">v-v:tense.[v:pred.pn]</ta>
            <ta e="T423" id="Seg_13166" s="T422">ptcl</ta>
            <ta e="T424" id="Seg_13167" s="T423">dempro</ta>
            <ta e="T425" id="Seg_13168" s="T424">v-v&gt;n-n:poss-n:case</ta>
            <ta e="T426" id="Seg_13169" s="T425">adv</ta>
            <ta e="T427" id="Seg_13170" s="T426">adv-adv&gt;adj</ta>
            <ta e="T428" id="Seg_13171" s="T427">n-n:case</ta>
            <ta e="T429" id="Seg_13172" s="T428">ptcl</ta>
            <ta e="T430" id="Seg_13173" s="T429">adv</ta>
            <ta e="T431" id="Seg_13174" s="T430">ptcl</ta>
            <ta e="T432" id="Seg_13175" s="T431">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T433" id="Seg_13176" s="T432">ptcl</ta>
            <ta e="T434" id="Seg_13177" s="T433">adj</ta>
            <ta e="T435" id="Seg_13178" s="T434">n-n:case</ta>
            <ta e="T436" id="Seg_13179" s="T435">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T437" id="Seg_13180" s="T436">ptcl</ta>
            <ta e="T438" id="Seg_13181" s="T437">dempro</ta>
            <ta e="T439" id="Seg_13182" s="T438">n-n:case</ta>
            <ta e="T440" id="Seg_13183" s="T439">ptcl</ta>
            <ta e="T441" id="Seg_13184" s="T440">v-v:tense-v:poss.pn</ta>
            <ta e="T442" id="Seg_13185" s="T441">dempro</ta>
            <ta e="T443" id="Seg_13186" s="T442">v-v&gt;n.[n:case]</ta>
            <ta e="T444" id="Seg_13187" s="T443">que</ta>
            <ta e="T445" id="Seg_13188" s="T444">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T446" id="Seg_13189" s="T445">indfpro</ta>
            <ta e="T447" id="Seg_13190" s="T446">n-n:(num)-n:case</ta>
            <ta e="T448" id="Seg_13191" s="T447">v</ta>
            <ta e="T449" id="Seg_13192" s="T448">v-v:cvb</ta>
            <ta e="T450" id="Seg_13193" s="T449">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T451" id="Seg_13194" s="T450">n-n:(poss)</ta>
            <ta e="T452" id="Seg_13195" s="T451">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T453" id="Seg_13196" s="T452">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T454" id="Seg_13197" s="T453">dempro</ta>
            <ta e="T455" id="Seg_13198" s="T454">n-n:case</ta>
            <ta e="T456" id="Seg_13199" s="T455">v-v:ptcp</ta>
            <ta e="T457" id="Seg_13200" s="T456">v-v:tense-v:poss.pn</ta>
            <ta e="T458" id="Seg_13201" s="T457">ptcl</ta>
            <ta e="T459" id="Seg_13202" s="T458">cardnum</ta>
            <ta e="T460" id="Seg_13203" s="T459">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T461" id="Seg_13204" s="T460">ptcl</ta>
            <ta e="T462" id="Seg_13205" s="T461">v-v:cvb</ta>
            <ta e="T463" id="Seg_13206" s="T462">n-n:case</ta>
            <ta e="T464" id="Seg_13207" s="T463">v-v:cvb</ta>
            <ta e="T465" id="Seg_13208" s="T464">ptcl</ta>
            <ta e="T466" id="Seg_13209" s="T465">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T467" id="Seg_13210" s="T466">ptcl</ta>
            <ta e="T468" id="Seg_13211" s="T467">dempro-pro:(poss)-pro:(poss).[pro:case]</ta>
            <ta e="T469" id="Seg_13212" s="T468">pers.[pro:case]</ta>
            <ta e="T470" id="Seg_13213" s="T469">n-n:case</ta>
            <ta e="T471" id="Seg_13214" s="T470">ptcl</ta>
            <ta e="T472" id="Seg_13215" s="T471">v-v:tense-v:poss.pn</ta>
            <ta e="T473" id="Seg_13216" s="T472">dempro-pro:(poss)-pro:(poss).[pro:case]</ta>
            <ta e="T474" id="Seg_13217" s="T473">n-n:case</ta>
            <ta e="T475" id="Seg_13218" s="T474">v-v:cvb</ta>
            <ta e="T476" id="Seg_13219" s="T475">v-v:(ins)-v:tense-v:poss.pn</ta>
            <ta e="T477" id="Seg_13220" s="T476">adv</ta>
            <ta e="T478" id="Seg_13221" s="T477">adv</ta>
            <ta e="T479" id="Seg_13222" s="T478">v-v:(ins)-v:tense-v:poss.pn</ta>
            <ta e="T480" id="Seg_13223" s="T479">v-v:ptcp-v:(poss).[v:(case)]</ta>
            <ta e="T481" id="Seg_13224" s="T480">ptcl</ta>
            <ta e="T482" id="Seg_13225" s="T481">dempro-pro:case</ta>
            <ta e="T483" id="Seg_13226" s="T482">n-n:(num).[n:case]</ta>
            <ta e="T484" id="Seg_13227" s="T483">que-pro:(num).[pro:case]</ta>
            <ta e="T485" id="Seg_13228" s="T484">ptcl</ta>
            <ta e="T486" id="Seg_13229" s="T485">v-v&gt;v-v:cvb</ta>
            <ta e="T487" id="Seg_13230" s="T486">adv&gt;adv-adv</ta>
            <ta e="T488" id="Seg_13231" s="T487">que.[pro:case]</ta>
            <ta e="T489" id="Seg_13232" s="T488">quant</ta>
            <ta e="T490" id="Seg_13233" s="T489">n.[n:case]</ta>
            <ta e="T491" id="Seg_13234" s="T490">v-v:neg-v:(ins)-v:poss.pn</ta>
            <ta e="T492" id="Seg_13235" s="T491">adj</ta>
            <ta e="T493" id="Seg_13236" s="T492">n.[n:case]</ta>
            <ta e="T494" id="Seg_13237" s="T493">n-n:poss-n:case</ta>
            <ta e="T495" id="Seg_13238" s="T494">ptcl</ta>
            <ta e="T496" id="Seg_13239" s="T495">adv</ta>
            <ta e="T497" id="Seg_13240" s="T496">adj</ta>
            <ta e="T498" id="Seg_13241" s="T497">v-v&gt;n-n:case</ta>
            <ta e="T499" id="Seg_13242" s="T498">v-v:ptcp</ta>
            <ta e="T500" id="Seg_13243" s="T499">v-v:tense-v:poss.pn</ta>
            <ta e="T501" id="Seg_13244" s="T500">adv</ta>
            <ta e="T502" id="Seg_13245" s="T501">emphpro-pro:(poss).[pro:case]</ta>
            <ta e="T503" id="Seg_13246" s="T502">v-v:cvb</ta>
            <ta e="T504" id="Seg_13247" s="T503">v-v:tense-v:pred.pn</ta>
            <ta e="T505" id="Seg_13248" s="T504">ptcl</ta>
            <ta e="T506" id="Seg_13249" s="T505">adv</ta>
            <ta e="T518" id="Seg_13250" s="T517">ptcl</ta>
            <ta e="T519" id="Seg_13251" s="T518">n-n:(ins)-n:case</ta>
            <ta e="T520" id="Seg_13252" s="T519">v-v:tense-v:pred.pn</ta>
            <ta e="T521" id="Seg_13253" s="T520">n-n&gt;adj</ta>
            <ta e="T522" id="Seg_13254" s="T521">n-n:case</ta>
            <ta e="T523" id="Seg_13255" s="T522">n-n:(ins)-n:case</ta>
            <ta e="T524" id="Seg_13256" s="T523">adv</ta>
            <ta e="T525" id="Seg_13257" s="T524">v-v:cvb</ta>
            <ta e="T526" id="Seg_13258" s="T525">v-v:tense.[v:pred.pn]</ta>
            <ta e="T527" id="Seg_13259" s="T526">dempro-pro:case</ta>
            <ta e="T528" id="Seg_13260" s="T527">v-v:mood-v:temp.pn</ta>
            <ta e="T529" id="Seg_13261" s="T528">ptcl</ta>
            <ta e="T530" id="Seg_13262" s="T529">v-v:tense-v:poss.pn</ta>
            <ta e="T531" id="Seg_13263" s="T530">ptcl</ta>
            <ta e="T532" id="Seg_13264" s="T531">que</ta>
            <ta e="T533" id="Seg_13265" s="T532">adv</ta>
            <ta e="T534" id="Seg_13266" s="T533">v-v:cvb-v:pred.pn</ta>
            <ta e="T535" id="Seg_13267" s="T534">que</ta>
            <ta e="T536" id="Seg_13268" s="T535">v-v:tense-v:poss.pn=ptcl</ta>
            <ta e="T537" id="Seg_13269" s="T536">v-v:tense-v:poss.pn</ta>
            <ta e="T538" id="Seg_13270" s="T537">dempro</ta>
            <ta e="T539" id="Seg_13271" s="T538">v-v:mood-v:temp.pn</ta>
            <ta e="T540" id="Seg_13272" s="T539">v-v:(tense).[v:mood.pn]</ta>
            <ta e="T559" id="Seg_13273" s="T558">ptcl</ta>
            <ta e="T560" id="Seg_13274" s="T559">v-v:tense-v:pred.pn</ta>
            <ta e="T561" id="Seg_13275" s="T560">dempro-n:(num)-pro:case</ta>
            <ta e="T562" id="Seg_13276" s="T561">dempro</ta>
            <ta e="T563" id="Seg_13277" s="T562">dempro-pro:case</ta>
            <ta e="T564" id="Seg_13278" s="T563">adv</ta>
            <ta e="T565" id="Seg_13279" s="T564">adj.[n:case]</ta>
            <ta e="T566" id="Seg_13280" s="T565">n-n:case</ta>
            <ta e="T567" id="Seg_13281" s="T566">v-v:tense-v:poss.pn</ta>
            <ta e="T568" id="Seg_13282" s="T567">ptcl</ta>
            <ta e="T569" id="Seg_13283" s="T568">adj-n:case</ta>
            <ta e="T570" id="Seg_13284" s="T569">adj.[n:case]</ta>
            <ta e="T571" id="Seg_13285" s="T570">n-n:case</ta>
            <ta e="T572" id="Seg_13286" s="T571">v-v:tense-v:poss.pn</ta>
            <ta e="T573" id="Seg_13287" s="T572">interj</ta>
            <ta e="T574" id="Seg_13288" s="T573">v-v:cvb</ta>
            <ta e="T575" id="Seg_13289" s="T574">v-v:(neg).[v:pred.pn]</ta>
            <ta e="T576" id="Seg_13290" s="T575">dempro-pro:(poss)-pro:(poss).[pro:case]</ta>
            <ta e="T577" id="Seg_13291" s="T576">v-v:tense</ta>
            <ta e="T578" id="Seg_13292" s="T577">v-v:ptcp-v:(case)</ta>
            <ta e="T579" id="Seg_13293" s="T578">ptcl</ta>
            <ta e="T580" id="Seg_13294" s="T579">v-v:mood.[v:pred.pn]</ta>
            <ta e="T581" id="Seg_13295" s="T580">n-n:poss-n:case</ta>
            <ta e="T582" id="Seg_13296" s="T581">v-v:(neg)-v:pred.pn</ta>
            <ta e="T583" id="Seg_13297" s="T582">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T584" id="Seg_13298" s="T583">adv</ta>
            <ta e="T585" id="Seg_13299" s="T584">ptcl</ta>
            <ta e="T586" id="Seg_13300" s="T585">pers-pro:case</ta>
            <ta e="T587" id="Seg_13301" s="T586">v-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T588" id="Seg_13302" s="T587">v-v:cvb-v:pred.pn</ta>
            <ta e="T589" id="Seg_13303" s="T588">adv</ta>
            <ta e="T590" id="Seg_13304" s="T589">v-v&gt;v-v:cvb</ta>
            <ta e="T591" id="Seg_13305" s="T590">v-v:(ins)-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T592" id="Seg_13306" s="T591">ptcl</ta>
            <ta e="T593" id="Seg_13307" s="T592">dempro.[pro:case]</ta>
            <ta e="T594" id="Seg_13308" s="T593">post</ta>
            <ta e="T595" id="Seg_13309" s="T594">v-v:tense-v:pred.pn</ta>
            <ta e="T611" id="Seg_13310" s="T610">dempro.[pro:case]</ta>
            <ta e="T612" id="Seg_13311" s="T611">v-v:tense-v:poss.pn</ta>
            <ta e="T613" id="Seg_13312" s="T612">dempro.[pro:case]</ta>
            <ta e="T614" id="Seg_13313" s="T613">propr.[n:case]</ta>
            <ta e="T615" id="Seg_13314" s="T614">v-v:cvb</ta>
            <ta e="T616" id="Seg_13315" s="T615">n.[n:case]</ta>
            <ta e="T617" id="Seg_13316" s="T616">v-v:tense-v:poss.pn</ta>
            <ta e="T618" id="Seg_13317" s="T617">v-v:ptcp</ta>
            <ta e="T619" id="Seg_13318" s="T618">n-n:(poss).[n:case]</ta>
            <ta e="T620" id="Seg_13319" s="T619">ptcl</ta>
            <ta e="T621" id="Seg_13320" s="T620">v-v:tense-v:poss.pn</ta>
            <ta e="T642" id="Seg_13321" s="T641">ptcl</ta>
            <ta e="T643" id="Seg_13322" s="T642">dempro-pro:(poss)-pro:case</ta>
            <ta e="T644" id="Seg_13323" s="T643">propr-n:case</ta>
            <ta e="T645" id="Seg_13324" s="T644">post</ta>
            <ta e="T646" id="Seg_13325" s="T645">ptcl</ta>
            <ta e="T647" id="Seg_13326" s="T646">interj</ta>
            <ta e="T648" id="Seg_13327" s="T647">que-que&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T649" id="Seg_13328" s="T648">v-v:ptcp-v:(case)</ta>
            <ta e="T650" id="Seg_13329" s="T649">ptcl</ta>
            <ta e="T651" id="Seg_13330" s="T650">v-v:tense-v:poss.pn</ta>
            <ta e="T652" id="Seg_13331" s="T651">ptcl</ta>
            <ta e="T653" id="Seg_13332" s="T652">adv</ta>
            <ta e="T654" id="Seg_13333" s="T653">v-v:cvb</ta>
            <ta e="T655" id="Seg_13334" s="T654">v-v:cvb</ta>
            <ta e="T656" id="Seg_13335" s="T655">interj</ta>
            <ta e="T657" id="Seg_13336" s="T656">n-n:case</ta>
            <ta e="T658" id="Seg_13337" s="T657">v-v:cvb</ta>
            <ta e="T659" id="Seg_13338" s="T658">v-v:tense-v:pred.pn</ta>
            <ta e="T660" id="Seg_13339" s="T659">ptcl</ta>
            <ta e="T663" id="Seg_13340" s="T662">v.[v:mood.pn]</ta>
            <ta e="T664" id="Seg_13341" s="T663">v-v:mood-v:temp.pn</ta>
            <ta e="T665" id="Seg_13342" s="T664">v-v:tense-v:poss.pn</ta>
            <ta e="T666" id="Seg_13343" s="T665">adv</ta>
            <ta e="T667" id="Seg_13344" s="T666">n.[n:case]</ta>
            <ta e="T668" id="Seg_13345" s="T667">v-v:(ins)-v&gt;v-v:ptcp</ta>
            <ta e="T669" id="Seg_13346" s="T668">n.[n:case]</ta>
            <ta e="T685" id="Seg_13347" s="T684">dempro-pro:case</ta>
            <ta e="T686" id="Seg_13348" s="T685">ptcl</ta>
            <ta e="T687" id="Seg_13349" s="T686">adj.[n:case]</ta>
            <ta e="T688" id="Seg_13350" s="T687">n-n:case</ta>
            <ta e="T689" id="Seg_13351" s="T688">v-v:mood-v:pred.pn</ta>
            <ta e="T690" id="Seg_13352" s="T689">ptcl</ta>
            <ta e="T691" id="Seg_13353" s="T690">n-n:(num).[n:case]</ta>
            <ta e="T692" id="Seg_13354" s="T691">adj.[n:case]</ta>
            <ta e="T693" id="Seg_13355" s="T692">n-n:case</ta>
            <ta e="T694" id="Seg_13356" s="T693">n.[n:case]</ta>
            <ta e="T695" id="Seg_13357" s="T694">v-v:(ins)-v&gt;v-v:cvb-v:pred.pn</ta>
            <ta e="T696" id="Seg_13358" s="T695">ptcl</ta>
            <ta e="T697" id="Seg_13359" s="T696">pers.[pro:case]</ta>
            <ta e="T698" id="Seg_13360" s="T697">n-n:(poss).[n:case]</ta>
            <ta e="T699" id="Seg_13361" s="T698">dempro</ta>
            <ta e="T700" id="Seg_13362" s="T699">v-v:tense-v:poss.pn</ta>
            <ta e="T701" id="Seg_13363" s="T700">dempro</ta>
            <ta e="T702" id="Seg_13364" s="T701">n-n:(num).[n:case]</ta>
            <ta e="T703" id="Seg_13365" s="T702">adv</ta>
            <ta e="T704" id="Seg_13366" s="T703">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T705" id="Seg_13367" s="T704">post</ta>
            <ta e="T706" id="Seg_13368" s="T705">dempro</ta>
            <ta e="T707" id="Seg_13369" s="T706">adj.[n:case]</ta>
            <ta e="T708" id="Seg_13370" s="T707">n-n:case</ta>
            <ta e="T709" id="Seg_13371" s="T708">v-v:ptcp</ta>
            <ta e="T710" id="Seg_13372" s="T709">v-v:tense-v:poss.pn</ta>
            <ta e="T711" id="Seg_13373" s="T710">ptcl</ta>
            <ta e="T712" id="Seg_13374" s="T711">propr.[n:case]</ta>
            <ta e="T713" id="Seg_13375" s="T712">v-v:cvb</ta>
            <ta e="T714" id="Seg_13376" s="T713">n.[n:case]</ta>
            <ta e="T715" id="Seg_13377" s="T714">dempro.[pro:case]</ta>
            <ta e="T716" id="Seg_13378" s="T715">dempro-pro:case</ta>
            <ta e="T728" id="Seg_13379" s="T727">dempro.[pro:case]</ta>
            <ta e="T729" id="Seg_13380" s="T728">posspr</ta>
            <ta e="T730" id="Seg_13381" s="T729">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T731" id="Seg_13382" s="T730">dempro.[pro:case]</ta>
            <ta e="T761" id="Seg_13383" s="T760">ptcl</ta>
            <ta e="T762" id="Seg_13384" s="T761">ptcl</ta>
            <ta e="T763" id="Seg_13385" s="T762">dempro</ta>
            <ta e="T764" id="Seg_13386" s="T763">pers.[pro:case]</ta>
            <ta e="T765" id="Seg_13387" s="T764">v-v:ptcp</ta>
            <ta e="T766" id="Seg_13388" s="T765">n-n:poss-n:case</ta>
            <ta e="T767" id="Seg_13389" s="T766">adj</ta>
            <ta e="T768" id="Seg_13390" s="T767">propr-n:case</ta>
            <ta e="T769" id="Seg_13391" s="T768">v-v:tense-v:pred.pn</ta>
            <ta e="T770" id="Seg_13392" s="T769">pers-pro:case</ta>
            <ta e="T771" id="Seg_13393" s="T770">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T772" id="Seg_13394" s="T771">post</ta>
            <ta e="T773" id="Seg_13395" s="T772">v-v:tense-v:poss.pn</ta>
            <ta e="T774" id="Seg_13396" s="T773">adv</ta>
            <ta e="T775" id="Seg_13397" s="T774">v-v:cvb-v:pred.pn</ta>
            <ta e="T776" id="Seg_13398" s="T775">%%</ta>
            <ta e="T777" id="Seg_13399" s="T776">n-n:case</ta>
            <ta e="T778" id="Seg_13400" s="T777">v-v:tense-v:poss.pn</ta>
            <ta e="T779" id="Seg_13401" s="T778">v-v:cvb-v:pred.pn</ta>
            <ta e="T780" id="Seg_13402" s="T779">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T781" id="Seg_13403" s="T780">ptcl</ta>
            <ta e="T782" id="Seg_13404" s="T781">dempro.[pro:case]</ta>
            <ta e="T783" id="Seg_13405" s="T782">que</ta>
            <ta e="T784" id="Seg_13406" s="T783">que</ta>
            <ta e="T785" id="Seg_13407" s="T784">v-v:cvb</ta>
            <ta e="T786" id="Seg_13408" s="T785">v-v:ptcp</ta>
            <ta e="T787" id="Seg_13409" s="T786">v-v:tense-v:poss.pn</ta>
            <ta e="T788" id="Seg_13410" s="T787">adv</ta>
            <ta e="T789" id="Seg_13411" s="T788">n-n:(poss).[n:case]</ta>
            <ta e="T793" id="Seg_13412" s="T792">ptcl</ta>
            <ta e="T794" id="Seg_13413" s="T793">v-v:(ins)-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T795" id="Seg_13414" s="T794">dempro-pro:case</ta>
            <ta e="T796" id="Seg_13415" s="T795">v-v:cvb-v:pred.pn</ta>
            <ta e="T797" id="Seg_13416" s="T796">v-v:tense-v:pred.pn</ta>
            <ta e="T798" id="Seg_13417" s="T797">dempro.[pro:case]</ta>
            <ta e="T800" id="Seg_13418" s="T799">dempro-pro:(num)-pro:case</ta>
            <ta e="T802" id="Seg_13419" s="T801">dempro-pro:(num)-pro:case</ta>
            <ta e="T803" id="Seg_13420" s="T802">v-v:tense-v:pred.pn</ta>
            <ta e="T804" id="Seg_13421" s="T803">dempro-pro:(num)-pro:case</ta>
            <ta e="T805" id="Seg_13422" s="T804">v-v:tense-v:pred.pn</ta>
            <ta e="T806" id="Seg_13423" s="T805">que</ta>
            <ta e="T807" id="Seg_13424" s="T806">ptcl</ta>
            <ta e="T808" id="Seg_13425" s="T807">dempro.[pro:case]</ta>
            <ta e="T809" id="Seg_13426" s="T808">dempro-pro:case-dempro-pro:case</ta>
            <ta e="T810" id="Seg_13427" s="T809">v-v:tense-v:pred.pn</ta>
            <ta e="T811" id="Seg_13428" s="T810">adj-adj&gt;adv</ta>
            <ta e="T812" id="Seg_13429" s="T811">v-v:(neg)-v:pred.pn</ta>
            <ta e="T813" id="Seg_13430" s="T812">n-n:poss-n:case</ta>
            <ta e="T820" id="Seg_13431" s="T819">adv</ta>
            <ta e="T821" id="Seg_13432" s="T820">ptcl</ta>
            <ta e="T822" id="Seg_13433" s="T821">v-v:(ins)-v&gt;v-v:tense.[v:pred.pn]</ta>
            <ta e="T823" id="Seg_13434" s="T822">adv</ta>
            <ta e="T824" id="Seg_13435" s="T823">adj-n:case</ta>
            <ta e="T825" id="Seg_13436" s="T824">ptcl</ta>
            <ta e="T826" id="Seg_13437" s="T825">v-v:tense-v:pred.pn</ta>
            <ta e="T849" id="Seg_13438" s="T848">ptcl</ta>
            <ta e="T850" id="Seg_13439" s="T849">ptcl</ta>
            <ta e="T851" id="Seg_13440" s="T850">ptcl</ta>
            <ta e="T852" id="Seg_13441" s="T851">dempro</ta>
            <ta e="T853" id="Seg_13442" s="T852">n-n:case</ta>
            <ta e="T854" id="Seg_13443" s="T853">v-v:ptcp</ta>
            <ta e="T855" id="Seg_13444" s="T854">pers.[pro:case]</ta>
            <ta e="T856" id="Seg_13445" s="T855">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T857" id="Seg_13446" s="T856">ptcl</ta>
            <ta e="T858" id="Seg_13447" s="T857">n-n:(num)-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T859" id="Seg_13448" s="T858">adv</ta>
            <ta e="T860" id="Seg_13449" s="T859">cardnum-cardnum&gt;collnum</ta>
            <ta e="T861" id="Seg_13450" s="T860">ptcl</ta>
            <ta e="T862" id="Seg_13451" s="T861">v-v:tense-v:pred.pn</ta>
            <ta e="T863" id="Seg_13452" s="T862">cardnum</ta>
            <ta e="T864" id="Seg_13453" s="T863">n-n&gt;adj-n:(pred.pn)</ta>
            <ta e="T865" id="Seg_13454" s="T864">cardnum-cardnum&gt;collnum</ta>
            <ta e="T866" id="Seg_13455" s="T865">ptcl</ta>
            <ta e="T867" id="Seg_13456" s="T866">v-v:(ins)</ta>
            <ta e="T868" id="Seg_13457" s="T867">adj-n:(poss).[n:case]</ta>
            <ta e="T869" id="Seg_13458" s="T868">adj.[n:case]</ta>
            <ta e="T870" id="Seg_13459" s="T869">adj-n:(poss).[n:case]</ta>
            <ta e="T871" id="Seg_13460" s="T870">pers-pro:case</ta>
            <ta e="T872" id="Seg_13461" s="T871">ptcl</ta>
            <ta e="T873" id="Seg_13462" s="T872">adj.[n:case]</ta>
            <ta e="T874" id="Seg_13463" s="T873">v-v:tense-v:pred.pn</ta>
            <ta e="T875" id="Seg_13464" s="T874">dempro-pro:(num).[pro:case]</ta>
            <ta e="T876" id="Seg_13465" s="T875">n-n:(num)-n:case</ta>
            <ta e="T877" id="Seg_13466" s="T876">n-n&gt;adj-n:(num)-n:case</ta>
            <ta e="T878" id="Seg_13467" s="T877">conj</ta>
            <ta e="T879" id="Seg_13468" s="T878">v-v:tense-v:pred.pn</ta>
            <ta e="T880" id="Seg_13469" s="T879">n-n:(poss)</ta>
            <ta e="T881" id="Seg_13470" s="T880">conj</ta>
            <ta e="T882" id="Seg_13471" s="T881">ptcl-n:case</ta>
            <ta e="T883" id="Seg_13472" s="T882">v-v:(ins)</ta>
            <ta e="T888" id="Seg_13473" s="T887">adv</ta>
            <ta e="T889" id="Seg_13474" s="T888">n-n:(num).[n:case]</ta>
            <ta e="T890" id="Seg_13475" s="T889">dempro.[pro:case]</ta>
            <ta e="T891" id="Seg_13476" s="T890">adv</ta>
            <ta e="T892" id="Seg_13477" s="T891">n-n&gt;n.[n:case]</ta>
            <ta e="T893" id="Seg_13478" s="T892">n-n:(num)-n:(poss).[n:case]</ta>
            <ta e="T894" id="Seg_13479" s="T893">v-v:tense-v:pred.pn</ta>
            <ta e="T895" id="Seg_13480" s="T894">ptcl</ta>
            <ta e="T896" id="Seg_13481" s="T895">adv</ta>
            <ta e="T897" id="Seg_13482" s="T896">adv</ta>
            <ta e="T898" id="Seg_13483" s="T897">adv</ta>
            <ta e="T899" id="Seg_13484" s="T898">pers.[pro:case]</ta>
            <ta e="T900" id="Seg_13485" s="T899">v-v:ptcp</ta>
            <ta e="T901" id="Seg_13486" s="T900">n-n&gt;n.[n:case]</ta>
            <ta e="T902" id="Seg_13487" s="T901">n-n:(num)-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T903" id="Seg_13488" s="T902">v-v:tense-v:pred.pn</ta>
            <ta e="T904" id="Seg_13489" s="T903">que.[pro:case]</ta>
            <ta e="T905" id="Seg_13490" s="T904">ptcl-ptcl:(poss)</ta>
            <ta e="T906" id="Seg_13491" s="T905">v-v:tense.[v:poss.pn]=ptcl</ta>
            <ta e="T907" id="Seg_13492" s="T906">adv</ta>
            <ta e="T908" id="Seg_13493" s="T907">n-n&gt;n.[n:case]</ta>
            <ta e="T909" id="Seg_13494" s="T908">n.[n:case]</ta>
            <ta e="T910" id="Seg_13495" s="T909">que-pro:case</ta>
            <ta e="T911" id="Seg_13496" s="T910">ptcl</ta>
            <ta e="T912" id="Seg_13497" s="T911">v-v:(neg)-v:pred.pn</ta>
            <ta e="T913" id="Seg_13498" s="T912">adv</ta>
            <ta e="T914" id="Seg_13499" s="T913">n-n&gt;n.[n:case]</ta>
            <ta e="T915" id="Seg_13500" s="T914">n-n:case</ta>
            <ta e="T916" id="Seg_13501" s="T915">adj</ta>
            <ta e="T917" id="Seg_13502" s="T916">n-n&gt;n.[n:case]</ta>
            <ta e="T918" id="Seg_13503" s="T917">n-n:case</ta>
            <ta e="T933" id="Seg_13504" s="T932">ptcl</ta>
            <ta e="T934" id="Seg_13505" s="T933">ptcl</ta>
            <ta e="T935" id="Seg_13506" s="T934">adv</ta>
            <ta e="T936" id="Seg_13507" s="T935">ptcl</ta>
            <ta e="T937" id="Seg_13508" s="T936">que-pro:(poss).[pro:case]</ta>
            <ta e="T938" id="Seg_13509" s="T937">ptcl</ta>
            <ta e="T939" id="Seg_13510" s="T938">adv-adv&gt;adv</ta>
            <ta e="T940" id="Seg_13511" s="T939">v-v:(neg)-v:tense-v:pred.pn</ta>
            <ta e="T941" id="Seg_13512" s="T940">dempro</ta>
            <ta e="T942" id="Seg_13513" s="T941">adv</ta>
            <ta e="T943" id="Seg_13514" s="T942">dempro</ta>
            <ta e="T944" id="Seg_13515" s="T943">n-n&gt;adj-n:case</ta>
            <ta e="T945" id="Seg_13516" s="T944">adv</ta>
            <ta e="T946" id="Seg_13517" s="T945">dempro</ta>
            <ta e="T947" id="Seg_13518" s="T946">n.[n:case]</ta>
            <ta e="T948" id="Seg_13519" s="T947">v-v:tense-v:pred.pn</ta>
            <ta e="T949" id="Seg_13520" s="T948">dempro</ta>
            <ta e="T950" id="Seg_13521" s="T949">adj</ta>
            <ta e="T951" id="Seg_13522" s="T950">n.[n:case]</ta>
            <ta e="T952" id="Seg_13523" s="T951">dempro.[pro:case]</ta>
            <ta e="T953" id="Seg_13524" s="T952">post</ta>
            <ta e="T954" id="Seg_13525" s="T953">pers.[pro:case]</ta>
            <ta e="T955" id="Seg_13526" s="T954">adv</ta>
            <ta e="T956" id="Seg_13527" s="T955">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T957" id="Seg_13528" s="T956">dempro-pro:case</ta>
            <ta e="T958" id="Seg_13529" s="T957">n-n:(num)-n:poss-n:case</ta>
            <ta e="T959" id="Seg_13530" s="T958">adj-n:poss-n:case</ta>
            <ta e="T960" id="Seg_13531" s="T959">v-v:cvb</ta>
            <ta e="T961" id="Seg_13532" s="T960">v-v:tense-v:pred.pn</ta>
            <ta e="T962" id="Seg_13533" s="T961">ptcl</ta>
            <ta e="T963" id="Seg_13534" s="T962">adj-n:poss-n:case</ta>
            <ta e="T964" id="Seg_13535" s="T963">que</ta>
            <ta e="T965" id="Seg_13536" s="T964">ptcl</ta>
            <ta e="T966" id="Seg_13537" s="T965">v-v:(neg)-v:pred.pn</ta>
            <ta e="T967" id="Seg_13538" s="T966">n-n:case</ta>
            <ta e="T968" id="Seg_13539" s="T967">conj</ta>
            <ta e="T969" id="Seg_13540" s="T968">v-v:tense-v:pred.pn</ta>
            <ta e="T970" id="Seg_13541" s="T969">n-n:case</ta>
            <ta e="T971" id="Seg_13542" s="T970">conj</ta>
            <ta e="T972" id="Seg_13543" s="T971">v-v:tense-v:pred.pn</ta>
            <ta e="T973" id="Seg_13544" s="T972">n-n:case</ta>
            <ta e="T974" id="Seg_13545" s="T973">v-v:ptcp-v:(poss)</ta>
            <ta e="T975" id="Seg_13546" s="T974">ptcl</ta>
            <ta e="T976" id="Seg_13547" s="T975">propr</ta>
            <ta e="T977" id="Seg_13548" s="T976">n.[n:case]</ta>
            <ta e="T978" id="Seg_13549" s="T977">ptcl</ta>
            <ta e="T979" id="Seg_13550" s="T978">n-n&gt;n.[n:case]</ta>
            <ta e="T980" id="Seg_13551" s="T979">ptcl</ta>
            <ta e="T991" id="Seg_13552" s="T990">que</ta>
            <ta e="T992" id="Seg_13553" s="T991">v-v:tense.[v:poss.pn]=ptcl</ta>
            <ta e="T993" id="Seg_13554" s="T992">conj</ta>
            <ta e="T994" id="Seg_13555" s="T993">dempro</ta>
            <ta e="T995" id="Seg_13556" s="T994">pers-pro:case</ta>
            <ta e="T996" id="Seg_13557" s="T995">n.[n:case]</ta>
            <ta e="T997" id="Seg_13558" s="T996">v-v:ptcp-v:(poss).[v:pred.pn]</ta>
            <ta e="T998" id="Seg_13559" s="T997">ptcl</ta>
            <ta e="T999" id="Seg_13560" s="T998">pers-pro:case</ta>
            <ta e="T1000" id="Seg_13561" s="T999">que.[pro:case]</ta>
            <ta e="T1001" id="Seg_13562" s="T1000">v</ta>
            <ta e="T1002" id="Seg_13563" s="T1001">adv-adv</ta>
            <ta e="T1003" id="Seg_13564" s="T1002">adv</ta>
            <ta e="T1004" id="Seg_13565" s="T1003">n-n&gt;v-v:(ins)-v&gt;v-v:(neg)-v:pred.pn</ta>
            <ta e="T1005" id="Seg_13566" s="T1004">ptcl</ta>
            <ta e="T1006" id="Seg_13567" s="T1005">adv</ta>
            <ta e="T1007" id="Seg_13568" s="T1006">v.[v:mood.pn]</ta>
            <ta e="T1008" id="Seg_13569" s="T1007">ptcl</ta>
            <ta e="T1009" id="Seg_13570" s="T1008">v-v:mood-v:temp.pn</ta>
            <ta e="T1010" id="Seg_13571" s="T1009">v-v:tense-v:pred.pn</ta>
            <ta e="T1011" id="Seg_13572" s="T1010">ptcl-v:mood-v:temp.pn</ta>
            <ta e="T1012" id="Seg_13573" s="T1011">v-v:(ins)-v&gt;v-v:mood-v:temp.pn</ta>
            <ta e="T1013" id="Seg_13574" s="T1012">ptcl</ta>
            <ta e="T1014" id="Seg_13575" s="T1013">v-v:tense-v:pred.pn</ta>
            <ta e="T1015" id="Seg_13576" s="T1014">ptcl</ta>
            <ta e="T1016" id="Seg_13577" s="T1015">ptcl</ta>
            <ta e="T1017" id="Seg_13578" s="T1016">ptcl</ta>
            <ta e="T1018" id="Seg_13579" s="T1017">adv</ta>
            <ta e="T1019" id="Seg_13580" s="T1018">que</ta>
            <ta e="T1020" id="Seg_13581" s="T1019">ptcl</ta>
            <ta e="T1021" id="Seg_13582" s="T1020">n-n&gt;v-v:(ins)-v&gt;v-v:(neg)-v:pred.pn</ta>
            <ta e="T1022" id="Seg_13583" s="T1021">ptcl</ta>
            <ta e="T1042" id="Seg_13584" s="T1041">ptcl</ta>
            <ta e="T1054" id="Seg_13585" s="T1053">interj</ta>
            <ta e="T1055" id="Seg_13586" s="T1054">adj-n:case</ta>
            <ta e="T1056" id="Seg_13587" s="T1055">v-v:(ins)-v:cvb</ta>
            <ta e="T1075" id="Seg_13588" s="T1074">n-n:(poss).[n:case]</ta>
            <ta e="T1076" id="Seg_13589" s="T1075">n-n:poss-n:case</ta>
            <ta e="T1077" id="Seg_13590" s="T1076">adj</ta>
            <ta e="T1078" id="Seg_13591" s="T1077">n.[n:case]</ta>
            <ta e="T1079" id="Seg_13592" s="T1078">n-n:poss-n:case</ta>
            <ta e="T1080" id="Seg_13593" s="T1079">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T1081" id="Seg_13594" s="T1080">propr.[n:case]</ta>
            <ta e="T1082" id="Seg_13595" s="T1081">n-n:poss-n:case</ta>
            <ta e="T1083" id="Seg_13596" s="T1082">n-n:(num).[n:case]</ta>
            <ta e="T1084" id="Seg_13597" s="T1083">n-n:case</ta>
            <ta e="T1085" id="Seg_13598" s="T1084">v-v:tense-v:pred.pn</ta>
            <ta e="T1086" id="Seg_13599" s="T1085">v.[v:mood.pn]</ta>
            <ta e="T1087" id="Seg_13600" s="T1086">v.[v:mood.pn]</ta>
            <ta e="T1088" id="Seg_13601" s="T1087">propr.[n:case]</ta>
            <ta e="T1089" id="Seg_13602" s="T1088">n-n:(poss).[n:case]</ta>
            <ta e="T1090" id="Seg_13603" s="T1089">v-v:tense-v:poss.pn</ta>
            <ta e="T1091" id="Seg_13604" s="T1090">adj</ta>
            <ta e="T1092" id="Seg_13605" s="T1091">adj</ta>
            <ta e="T1093" id="Seg_13606" s="T1092">n.[n:case]</ta>
            <ta e="T1094" id="Seg_13607" s="T1093">v-v:tense-v:poss.pn</ta>
            <ta e="T1095" id="Seg_13608" s="T1094">adj-adj&gt;adj-adj&gt;adv</ta>
            <ta e="T1096" id="Seg_13609" s="T1095">v-v:(tense).[v:mood.pn]</ta>
            <ta e="T1097" id="Seg_13610" s="T1096">n-n:poss-n:case</ta>
            <ta e="T1098" id="Seg_13611" s="T1097">n-n:poss-n:case</ta>
            <ta e="T1099" id="Seg_13612" s="T1098">v-v:(tense).[v:mood.pn]</ta>
            <ta e="T1100" id="Seg_13613" s="T1099">pers.[pro:case]</ta>
            <ta e="T1101" id="Seg_13614" s="T1100">n-n:poss-n:case</ta>
            <ta e="T1102" id="Seg_13615" s="T1101">n-n&gt;v-v:(tense).[v:mood.pn]</ta>
            <ta e="T1103" id="Seg_13616" s="T1102">n-n:case</ta>
            <ta e="T1104" id="Seg_13617" s="T1103">n.[n:case]</ta>
            <ta e="T1105" id="Seg_13618" s="T1104">v-v:(ins)-v&gt;v-v:(tense).[v:mood.pn]</ta>
            <ta e="T1106" id="Seg_13619" s="T1105">n-n&gt;n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T1107" id="Seg_13620" s="T1106">n-n:poss-n:case</ta>
            <ta e="T1108" id="Seg_13621" s="T1107">n-n:poss-n:case</ta>
            <ta e="T1109" id="Seg_13622" s="T1108">v-v:tense-v:pred.pn</ta>
            <ta e="T1110" id="Seg_13623" s="T1109">n-n:poss-n:case</ta>
            <ta e="T1111" id="Seg_13624" s="T1110">v-v:tense-v:pred.pn</ta>
            <ta e="T1112" id="Seg_13625" s="T1111">adv</ta>
            <ta e="T1113" id="Seg_13626" s="T1112">v-v:(ins)-v&gt;v-v:tense-v:poss.pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps-UkET">
            <ta e="T23" id="Seg_13627" s="T21">pers</ta>
            <ta e="T25" id="Seg_13628" s="T23">conj</ta>
            <ta e="T26" id="Seg_13629" s="T25">emphpro</ta>
            <ta e="T27" id="Seg_13630" s="T26">v</ta>
            <ta e="T28" id="Seg_13631" s="T27">n</ta>
            <ta e="T29" id="Seg_13632" s="T28">dempro</ta>
            <ta e="T30" id="Seg_13633" s="T29">post</ta>
            <ta e="T31" id="Seg_13634" s="T30">cop</ta>
            <ta e="T32" id="Seg_13635" s="T31">n</ta>
            <ta e="T33" id="Seg_13636" s="T32">post</ta>
            <ta e="T34" id="Seg_13637" s="T33">dempro</ta>
            <ta e="T35" id="Seg_13638" s="T34">post</ta>
            <ta e="T37" id="Seg_13639" s="T36">que</ta>
            <ta e="T38" id="Seg_13640" s="T37">post</ta>
            <ta e="T39" id="Seg_13641" s="T38">interj</ta>
            <ta e="T40" id="Seg_13642" s="T39">adv</ta>
            <ta e="T41" id="Seg_13643" s="T40">propr</ta>
            <ta e="T42" id="Seg_13644" s="T41">adv</ta>
            <ta e="T43" id="Seg_13645" s="T42">dempro</ta>
            <ta e="T44" id="Seg_13646" s="T43">n</ta>
            <ta e="T45" id="Seg_13647" s="T44">post</ta>
            <ta e="T46" id="Seg_13648" s="T45">v</ta>
            <ta e="T47" id="Seg_13649" s="T46">adv</ta>
            <ta e="T48" id="Seg_13650" s="T47">n</ta>
            <ta e="T49" id="Seg_13651" s="T48">v</ta>
            <ta e="T50" id="Seg_13652" s="T49">n</ta>
            <ta e="T51" id="Seg_13653" s="T50">post</ta>
            <ta e="T52" id="Seg_13654" s="T51">v</ta>
            <ta e="T53" id="Seg_13655" s="T52">n</ta>
            <ta e="T54" id="Seg_13656" s="T53">ptcl</ta>
            <ta e="T55" id="Seg_13657" s="T54">que</ta>
            <ta e="T56" id="Seg_13658" s="T55">que</ta>
            <ta e="T57" id="Seg_13659" s="T56">v</ta>
            <ta e="T58" id="Seg_13660" s="T57">ptcl</ta>
            <ta e="T59" id="Seg_13661" s="T58">que</ta>
            <ta e="T60" id="Seg_13662" s="T59">v</ta>
            <ta e="T61" id="Seg_13663" s="T60">cardnum</ta>
            <ta e="T62" id="Seg_13664" s="T61">n</ta>
            <ta e="T63" id="Seg_13665" s="T62">dempro</ta>
            <ta e="T64" id="Seg_13666" s="T63">n</ta>
            <ta e="T65" id="Seg_13667" s="T64">v</ta>
            <ta e="T66" id="Seg_13668" s="T65">cardnum</ta>
            <ta e="T67" id="Seg_13669" s="T66">n</ta>
            <ta e="T68" id="Seg_13670" s="T67">v</ta>
            <ta e="T69" id="Seg_13671" s="T68">cardnum</ta>
            <ta e="T70" id="Seg_13672" s="T69">interj</ta>
            <ta e="T71" id="Seg_13673" s="T70">cardnum</ta>
            <ta e="T72" id="Seg_13674" s="T71">n</ta>
            <ta e="T73" id="Seg_13675" s="T72">dempro</ta>
            <ta e="T74" id="Seg_13676" s="T73">propr</ta>
            <ta e="T76" id="Seg_13677" s="T75">ptcl</ta>
            <ta e="T77" id="Seg_13678" s="T76">adj</ta>
            <ta e="T78" id="Seg_13679" s="T77">n</ta>
            <ta e="T79" id="Seg_13680" s="T78">propr</ta>
            <ta e="T80" id="Seg_13681" s="T79">n</ta>
            <ta e="T81" id="Seg_13682" s="T80">n</ta>
            <ta e="T82" id="Seg_13683" s="T81">propr</ta>
            <ta e="T84" id="Seg_13684" s="T83">dempro</ta>
            <ta e="T85" id="Seg_13685" s="T84">adj</ta>
            <ta e="T86" id="Seg_13686" s="T85">n</ta>
            <ta e="T87" id="Seg_13687" s="T86">n</ta>
            <ta e="T88" id="Seg_13688" s="T87">adj</ta>
            <ta e="T89" id="Seg_13689" s="T88">dempro</ta>
            <ta e="T90" id="Seg_13690" s="T89">n</ta>
            <ta e="T91" id="Seg_13691" s="T90">propr</ta>
            <ta e="T92" id="Seg_13692" s="T91">dempro</ta>
            <ta e="T93" id="Seg_13693" s="T92">n</ta>
            <ta e="T94" id="Seg_13694" s="T93">v</ta>
            <ta e="T95" id="Seg_13695" s="T94">cardnum</ta>
            <ta e="T96" id="Seg_13696" s="T95">n</ta>
            <ta e="T97" id="Seg_13697" s="T96">n</ta>
            <ta e="T98" id="Seg_13698" s="T97">v</ta>
            <ta e="T99" id="Seg_13699" s="T98">aux</ta>
            <ta e="T100" id="Seg_13700" s="T99">dempro</ta>
            <ta e="T101" id="Seg_13701" s="T100">interj</ta>
            <ta e="T102" id="Seg_13702" s="T101">dempro</ta>
            <ta e="T103" id="Seg_13703" s="T102">n</ta>
            <ta e="T104" id="Seg_13704" s="T103">que</ta>
            <ta e="T105" id="Seg_13705" s="T104">v</ta>
            <ta e="T106" id="Seg_13706" s="T105">adv</ta>
            <ta e="T107" id="Seg_13707" s="T106">ptcl</ta>
            <ta e="T108" id="Seg_13708" s="T107">v</ta>
            <ta e="T109" id="Seg_13709" s="T108">v</ta>
            <ta e="T110" id="Seg_13710" s="T109">adv</ta>
            <ta e="T111" id="Seg_13711" s="T110">v</ta>
            <ta e="T112" id="Seg_13712" s="T111">propr</ta>
            <ta e="T113" id="Seg_13713" s="T112">post</ta>
            <ta e="T114" id="Seg_13714" s="T113">v</ta>
            <ta e="T115" id="Seg_13715" s="T114">dempro</ta>
            <ta e="T116" id="Seg_13716" s="T115">interj</ta>
            <ta e="T117" id="Seg_13717" s="T116">propr</ta>
            <ta e="T118" id="Seg_13718" s="T117">v</ta>
            <ta e="T119" id="Seg_13719" s="T118">v</ta>
            <ta e="T120" id="Seg_13720" s="T119">adv</ta>
            <ta e="T121" id="Seg_13721" s="T120">adv</ta>
            <ta e="T122" id="Seg_13722" s="T121">cardnum</ta>
            <ta e="T123" id="Seg_13723" s="T122">n</ta>
            <ta e="T124" id="Seg_13724" s="T123">n</ta>
            <ta e="T125" id="Seg_13725" s="T124">cop</ta>
            <ta e="T126" id="Seg_13726" s="T125">cardnum</ta>
            <ta e="T127" id="Seg_13727" s="T126">n</ta>
            <ta e="T128" id="Seg_13728" s="T127">dempro</ta>
            <ta e="T129" id="Seg_13729" s="T128">n</ta>
            <ta e="T131" id="Seg_13730" s="T130">n</ta>
            <ta e="T132" id="Seg_13731" s="T131">v</ta>
            <ta e="T133" id="Seg_13732" s="T132">v</ta>
            <ta e="T134" id="Seg_13733" s="T133">v</ta>
            <ta e="T135" id="Seg_13734" s="T134">n</ta>
            <ta e="T136" id="Seg_13735" s="T135">v</ta>
            <ta e="T137" id="Seg_13736" s="T136">v</ta>
            <ta e="T138" id="Seg_13737" s="T137">adv</ta>
            <ta e="T139" id="Seg_13738" s="T138">ptcl</ta>
            <ta e="T140" id="Seg_13739" s="T139">cardnum</ta>
            <ta e="T141" id="Seg_13740" s="T140">n</ta>
            <ta e="T143" id="Seg_13741" s="T142">que</ta>
            <ta e="T144" id="Seg_13742" s="T143">v</ta>
            <ta e="T145" id="Seg_13743" s="T144">aux</ta>
            <ta e="T146" id="Seg_13744" s="T145">n</ta>
            <ta e="T147" id="Seg_13745" s="T146">n</ta>
            <ta e="T148" id="Seg_13746" s="T147">adv</ta>
            <ta e="T149" id="Seg_13747" s="T148">emphpro</ta>
            <ta e="T150" id="Seg_13748" s="T149">v</ta>
            <ta e="T151" id="Seg_13749" s="T150">dempro</ta>
            <ta e="T152" id="Seg_13750" s="T151">emphpro</ta>
            <ta e="T153" id="Seg_13751" s="T152">v</ta>
            <ta e="T154" id="Seg_13752" s="T153">n</ta>
            <ta e="T155" id="Seg_13753" s="T154">v</ta>
            <ta e="T156" id="Seg_13754" s="T155">n</ta>
            <ta e="T157" id="Seg_13755" s="T156">adv</ta>
            <ta e="T158" id="Seg_13756" s="T157">n</ta>
            <ta e="T159" id="Seg_13757" s="T158">cop</ta>
            <ta e="T160" id="Seg_13758" s="T159">ptcl</ta>
            <ta e="T162" id="Seg_13759" s="T161">n</ta>
            <ta e="T163" id="Seg_13760" s="T162">interj</ta>
            <ta e="T164" id="Seg_13761" s="T163">propr</ta>
            <ta e="T165" id="Seg_13762" s="T164">adv</ta>
            <ta e="T166" id="Seg_13763" s="T165">n</ta>
            <ta e="T167" id="Seg_13764" s="T166">n</ta>
            <ta e="T168" id="Seg_13765" s="T167">v</ta>
            <ta e="T169" id="Seg_13766" s="T168">aux</ta>
            <ta e="T170" id="Seg_13767" s="T169">adv</ta>
            <ta e="T171" id="Seg_13768" s="T170">v</ta>
            <ta e="T172" id="Seg_13769" s="T171">n</ta>
            <ta e="T173" id="Seg_13770" s="T172">v</ta>
            <ta e="T174" id="Seg_13771" s="T173">dempro</ta>
            <ta e="T175" id="Seg_13772" s="T174">n</ta>
            <ta e="T176" id="Seg_13773" s="T175">ptcl</ta>
            <ta e="T177" id="Seg_13774" s="T176">v</ta>
            <ta e="T178" id="Seg_13775" s="T177">dempro</ta>
            <ta e="T179" id="Seg_13776" s="T178">v</ta>
            <ta e="T180" id="Seg_13777" s="T179">adv</ta>
            <ta e="T181" id="Seg_13778" s="T180">v</ta>
            <ta e="T182" id="Seg_13779" s="T181">n</ta>
            <ta e="T183" id="Seg_13780" s="T182">v</ta>
            <ta e="T184" id="Seg_13781" s="T183">dempro</ta>
            <ta e="T185" id="Seg_13782" s="T184">adv</ta>
            <ta e="T186" id="Seg_13783" s="T185">dempro</ta>
            <ta e="T188" id="Seg_13784" s="T186">n</ta>
            <ta e="T190" id="Seg_13785" s="T188">v</ta>
            <ta e="T203" id="Seg_13786" s="T202">ptcl</ta>
            <ta e="T204" id="Seg_13787" s="T203">adv</ta>
            <ta e="T205" id="Seg_13788" s="T204">dempro</ta>
            <ta e="T206" id="Seg_13789" s="T205">n</ta>
            <ta e="T207" id="Seg_13790" s="T206">n</ta>
            <ta e="T208" id="Seg_13791" s="T207">n</ta>
            <ta e="T209" id="Seg_13792" s="T208">v</ta>
            <ta e="T210" id="Seg_13793" s="T209">dempro</ta>
            <ta e="T211" id="Seg_13794" s="T210">n</ta>
            <ta e="T212" id="Seg_13795" s="T211">v</ta>
            <ta e="T213" id="Seg_13796" s="T212">n</ta>
            <ta e="T214" id="Seg_13797" s="T213">adj</ta>
            <ta e="T215" id="Seg_13798" s="T214">cardnum</ta>
            <ta e="T216" id="Seg_13799" s="T215">adj</ta>
            <ta e="T217" id="Seg_13800" s="T216">ptcl</ta>
            <ta e="T218" id="Seg_13801" s="T217">dempro</ta>
            <ta e="T219" id="Seg_13802" s="T218">n</ta>
            <ta e="T220" id="Seg_13803" s="T219">v</ta>
            <ta e="T221" id="Seg_13804" s="T220">aux</ta>
            <ta e="T222" id="Seg_13805" s="T221">aux</ta>
            <ta e="T223" id="Seg_13806" s="T222">ptcl</ta>
            <ta e="T224" id="Seg_13807" s="T223">adv</ta>
            <ta e="T225" id="Seg_13808" s="T224">ptcl</ta>
            <ta e="T226" id="Seg_13809" s="T225">dempro</ta>
            <ta e="T227" id="Seg_13810" s="T226">n</ta>
            <ta e="T228" id="Seg_13811" s="T227">dempro</ta>
            <ta e="T229" id="Seg_13812" s="T228">propr</ta>
            <ta e="T230" id="Seg_13813" s="T229">v</ta>
            <ta e="T231" id="Seg_13814" s="T230">v</ta>
            <ta e="T232" id="Seg_13815" s="T231">dempro</ta>
            <ta e="T233" id="Seg_13816" s="T232">n</ta>
            <ta e="T234" id="Seg_13817" s="T233">n</ta>
            <ta e="T235" id="Seg_13818" s="T234">v</ta>
            <ta e="T236" id="Seg_13819" s="T235">aux</ta>
            <ta e="T237" id="Seg_13820" s="T236">ptcl</ta>
            <ta e="T238" id="Seg_13821" s="T237">dempro</ta>
            <ta e="T239" id="Seg_13822" s="T238">cardnum</ta>
            <ta e="T240" id="Seg_13823" s="T239">n</ta>
            <ta e="T241" id="Seg_13824" s="T240">dempro</ta>
            <ta e="T242" id="Seg_13825" s="T241">n</ta>
            <ta e="T243" id="Seg_13826" s="T242">dempro</ta>
            <ta e="T244" id="Seg_13827" s="T243">post</ta>
            <ta e="T245" id="Seg_13828" s="T244">ptcl</ta>
            <ta e="T246" id="Seg_13829" s="T245">n</ta>
            <ta e="T247" id="Seg_13830" s="T246">v</ta>
            <ta e="T248" id="Seg_13831" s="T247">ptcl</ta>
            <ta e="T249" id="Seg_13832" s="T248">dempro</ta>
            <ta e="T250" id="Seg_13833" s="T249">n</ta>
            <ta e="T251" id="Seg_13834" s="T250">cop</ta>
            <ta e="T252" id="Seg_13835" s="T251">dempro</ta>
            <ta e="T253" id="Seg_13836" s="T252">v</ta>
            <ta e="T254" id="Seg_13837" s="T253">dempro</ta>
            <ta e="T255" id="Seg_13838" s="T254">post</ta>
            <ta e="T256" id="Seg_13839" s="T255">dempro</ta>
            <ta e="T257" id="Seg_13840" s="T256">que</ta>
            <ta e="T258" id="Seg_13841" s="T257">cardnum</ta>
            <ta e="T259" id="Seg_13842" s="T258">cardnum</ta>
            <ta e="T260" id="Seg_13843" s="T259">n</ta>
            <ta e="T261" id="Seg_13844" s="T260">ptcl</ta>
            <ta e="T262" id="Seg_13845" s="T261">n</ta>
            <ta e="T263" id="Seg_13846" s="T262">n</ta>
            <ta e="T264" id="Seg_13847" s="T263">que</ta>
            <ta e="T265" id="Seg_13848" s="T264">ptcl</ta>
            <ta e="T266" id="Seg_13849" s="T265">v</ta>
            <ta e="T267" id="Seg_13850" s="T266">ptcl</ta>
            <ta e="T268" id="Seg_13851" s="T267">adv</ta>
            <ta e="T269" id="Seg_13852" s="T268">n</ta>
            <ta e="T270" id="Seg_13853" s="T269">n</ta>
            <ta e="T271" id="Seg_13854" s="T270">post</ta>
            <ta e="T272" id="Seg_13855" s="T271">adj</ta>
            <ta e="T273" id="Seg_13856" s="T272">cop</ta>
            <ta e="T274" id="Seg_13857" s="T273">n</ta>
            <ta e="T275" id="Seg_13858" s="T274">adv</ta>
            <ta e="T276" id="Seg_13859" s="T275">ptcl</ta>
            <ta e="T277" id="Seg_13860" s="T276">cop</ta>
            <ta e="T300" id="Seg_13861" s="T299">ptcl</ta>
            <ta e="T301" id="Seg_13862" s="T300">v</ta>
            <ta e="T302" id="Seg_13863" s="T301">n</ta>
            <ta e="T303" id="Seg_13864" s="T302">ptcl</ta>
            <ta e="T304" id="Seg_13865" s="T303">adv</ta>
            <ta e="T305" id="Seg_13866" s="T304">adv</ta>
            <ta e="T306" id="Seg_13867" s="T305">ptcl</ta>
            <ta e="T307" id="Seg_13868" s="T306">v</ta>
            <ta e="T308" id="Seg_13869" s="T307">ptcl</ta>
            <ta e="T309" id="Seg_13870" s="T308">ptcl</ta>
            <ta e="T310" id="Seg_13871" s="T309">adj</ta>
            <ta e="T311" id="Seg_13872" s="T310">n</ta>
            <ta e="T312" id="Seg_13873" s="T311">ptcl</ta>
            <ta e="T313" id="Seg_13874" s="T312">n</ta>
            <ta e="T314" id="Seg_13875" s="T313">n</ta>
            <ta e="T315" id="Seg_13876" s="T314">v</ta>
            <ta e="T316" id="Seg_13877" s="T315">adj</ta>
            <ta e="T317" id="Seg_13878" s="T316">n</ta>
            <ta e="T318" id="Seg_13879" s="T317">n</ta>
            <ta e="T319" id="Seg_13880" s="T318">v</ta>
            <ta e="T320" id="Seg_13881" s="T319">ptcl</ta>
            <ta e="T321" id="Seg_13882" s="T320">n</ta>
            <ta e="T322" id="Seg_13883" s="T321">dempro</ta>
            <ta e="T323" id="Seg_13884" s="T322">dempro</ta>
            <ta e="T324" id="Seg_13885" s="T323">n</ta>
            <ta e="T325" id="Seg_13886" s="T324">v</ta>
            <ta e="T326" id="Seg_13887" s="T325">ptcl</ta>
            <ta e="T327" id="Seg_13888" s="T326">n</ta>
            <ta e="T328" id="Seg_13889" s="T327">n</ta>
            <ta e="T329" id="Seg_13890" s="T328">v</ta>
            <ta e="T330" id="Seg_13891" s="T329">v</ta>
            <ta e="T331" id="Seg_13892" s="T330">aux</ta>
            <ta e="T332" id="Seg_13893" s="T331">ptcl</ta>
            <ta e="T333" id="Seg_13894" s="T332">dempro</ta>
            <ta e="T334" id="Seg_13895" s="T333">v</ta>
            <ta e="T335" id="Seg_13896" s="T334">dempro</ta>
            <ta e="T336" id="Seg_13897" s="T335">v</ta>
            <ta e="T337" id="Seg_13898" s="T336">dempro</ta>
            <ta e="T338" id="Seg_13899" s="T337">v</ta>
            <ta e="T339" id="Seg_13900" s="T338">dempro</ta>
            <ta e="T340" id="Seg_13901" s="T339">v</ta>
            <ta e="T341" id="Seg_13902" s="T340">dempro</ta>
            <ta e="T342" id="Seg_13903" s="T341">v</ta>
            <ta e="T343" id="Seg_13904" s="T342">ptcl</ta>
            <ta e="T344" id="Seg_13905" s="T343">v</ta>
            <ta e="T345" id="Seg_13906" s="T344">v</ta>
            <ta e="T346" id="Seg_13907" s="T345">ptcl</ta>
            <ta e="T347" id="Seg_13908" s="T346">adj</ta>
            <ta e="T348" id="Seg_13909" s="T347">ptcl</ta>
            <ta e="T356" id="Seg_13910" s="T355">interj</ta>
            <ta e="T357" id="Seg_13911" s="T356">posspr</ta>
            <ta e="T358" id="Seg_13912" s="T357">n</ta>
            <ta e="T359" id="Seg_13913" s="T358">adj</ta>
            <ta e="T360" id="Seg_13914" s="T359">n</ta>
            <ta e="T361" id="Seg_13915" s="T360">cop</ta>
            <ta e="T362" id="Seg_13916" s="T361">v</ta>
            <ta e="T363" id="Seg_13917" s="T362">conj</ta>
            <ta e="T364" id="Seg_13918" s="T363">aux</ta>
            <ta e="T365" id="Seg_13919" s="T364">n</ta>
            <ta e="T366" id="Seg_13920" s="T365">v</ta>
            <ta e="T367" id="Seg_13921" s="T366">v</ta>
            <ta e="T368" id="Seg_13922" s="T367">adj</ta>
            <ta e="T369" id="Seg_13923" s="T368">conj</ta>
            <ta e="T370" id="Seg_13924" s="T369">n</ta>
            <ta e="T371" id="Seg_13925" s="T370">v</ta>
            <ta e="T372" id="Seg_13926" s="T371">adv</ta>
            <ta e="T373" id="Seg_13927" s="T372">conj</ta>
            <ta e="T374" id="Seg_13928" s="T373">n</ta>
            <ta e="T375" id="Seg_13929" s="T374">v</ta>
            <ta e="T376" id="Seg_13930" s="T375">adv</ta>
            <ta e="T377" id="Seg_13931" s="T376">adj</ta>
            <ta e="T378" id="Seg_13932" s="T377">n</ta>
            <ta e="T379" id="Seg_13933" s="T378">ptcl</ta>
            <ta e="T380" id="Seg_13934" s="T379">n</ta>
            <ta e="T381" id="Seg_13935" s="T380">v</ta>
            <ta e="T382" id="Seg_13936" s="T381">ptcl</ta>
            <ta e="T383" id="Seg_13937" s="T382">adv</ta>
            <ta e="T384" id="Seg_13938" s="T383">ptcl</ta>
            <ta e="T385" id="Seg_13939" s="T384">propr</ta>
            <ta e="T386" id="Seg_13940" s="T385">v</ta>
            <ta e="T387" id="Seg_13941" s="T386">ptcl</ta>
            <ta e="T388" id="Seg_13942" s="T387">n</ta>
            <ta e="T389" id="Seg_13943" s="T388">n</ta>
            <ta e="T390" id="Seg_13944" s="T389">v</ta>
            <ta e="T391" id="Seg_13945" s="T390">n</ta>
            <ta e="T392" id="Seg_13946" s="T391">dempro</ta>
            <ta e="T393" id="Seg_13947" s="T392">n</ta>
            <ta e="T394" id="Seg_13948" s="T393">v</ta>
            <ta e="T395" id="Seg_13949" s="T394">v</ta>
            <ta e="T396" id="Seg_13950" s="T395">adv</ta>
            <ta e="T397" id="Seg_13951" s="T396">v</ta>
            <ta e="T398" id="Seg_13952" s="T397">n</ta>
            <ta e="T399" id="Seg_13953" s="T398">v</ta>
            <ta e="T400" id="Seg_13954" s="T399">n</ta>
            <ta e="T401" id="Seg_13955" s="T400">v</ta>
            <ta e="T402" id="Seg_13956" s="T401">n</ta>
            <ta e="T403" id="Seg_13957" s="T402">n</ta>
            <ta e="T404" id="Seg_13958" s="T403">ptcl</ta>
            <ta e="T405" id="Seg_13959" s="T404">v</ta>
            <ta e="T406" id="Seg_13960" s="T405">v</ta>
            <ta e="T407" id="Seg_13961" s="T406">adv</ta>
            <ta e="T408" id="Seg_13962" s="T407">que</ta>
            <ta e="T409" id="Seg_13963" s="T408">v</ta>
            <ta e="T410" id="Seg_13964" s="T409">n</ta>
            <ta e="T411" id="Seg_13965" s="T410">v</ta>
            <ta e="T412" id="Seg_13966" s="T411">n</ta>
            <ta e="T413" id="Seg_13967" s="T412">v</ta>
            <ta e="T414" id="Seg_13968" s="T413">dempro</ta>
            <ta e="T415" id="Seg_13969" s="T414">post</ta>
            <ta e="T416" id="Seg_13970" s="T415">n</ta>
            <ta e="T417" id="Seg_13971" s="T416">v</ta>
            <ta e="T418" id="Seg_13972" s="T417">ptcl</ta>
            <ta e="T419" id="Seg_13973" s="T418">adj</ta>
            <ta e="T420" id="Seg_13974" s="T419">n</ta>
            <ta e="T421" id="Seg_13975" s="T420">adv</ta>
            <ta e="T422" id="Seg_13976" s="T421">v</ta>
            <ta e="T423" id="Seg_13977" s="T422">ptcl</ta>
            <ta e="T424" id="Seg_13978" s="T423">dempro</ta>
            <ta e="T425" id="Seg_13979" s="T424">n</ta>
            <ta e="T426" id="Seg_13980" s="T425">adv</ta>
            <ta e="T427" id="Seg_13981" s="T426">adj</ta>
            <ta e="T428" id="Seg_13982" s="T427">n</ta>
            <ta e="T429" id="Seg_13983" s="T428">ptcl</ta>
            <ta e="T430" id="Seg_13984" s="T429">adv</ta>
            <ta e="T431" id="Seg_13985" s="T430">ptcl</ta>
            <ta e="T432" id="Seg_13986" s="T431">v</ta>
            <ta e="T433" id="Seg_13987" s="T432">ptcl</ta>
            <ta e="T434" id="Seg_13988" s="T433">adj</ta>
            <ta e="T435" id="Seg_13989" s="T434">n</ta>
            <ta e="T436" id="Seg_13990" s="T435">v</ta>
            <ta e="T437" id="Seg_13991" s="T436">ptcl</ta>
            <ta e="T438" id="Seg_13992" s="T437">dempro</ta>
            <ta e="T439" id="Seg_13993" s="T438">n</ta>
            <ta e="T440" id="Seg_13994" s="T439">ptcl</ta>
            <ta e="T441" id="Seg_13995" s="T440">v</ta>
            <ta e="T442" id="Seg_13996" s="T441">dempro</ta>
            <ta e="T443" id="Seg_13997" s="T442">n</ta>
            <ta e="T444" id="Seg_13998" s="T443">que</ta>
            <ta e="T445" id="Seg_13999" s="T444">v</ta>
            <ta e="T446" id="Seg_14000" s="T445">indfpro</ta>
            <ta e="T447" id="Seg_14001" s="T446">n</ta>
            <ta e="T448" id="Seg_14002" s="T447">v</ta>
            <ta e="T449" id="Seg_14003" s="T448">v</ta>
            <ta e="T450" id="Seg_14004" s="T449">v</ta>
            <ta e="T451" id="Seg_14005" s="T450">n</ta>
            <ta e="T452" id="Seg_14006" s="T451">ptcl</ta>
            <ta e="T453" id="Seg_14007" s="T452">n</ta>
            <ta e="T454" id="Seg_14008" s="T453">dempro</ta>
            <ta e="T455" id="Seg_14009" s="T454">n</ta>
            <ta e="T456" id="Seg_14010" s="T455">v</ta>
            <ta e="T457" id="Seg_14011" s="T456">aux</ta>
            <ta e="T458" id="Seg_14012" s="T457">ptcl</ta>
            <ta e="T459" id="Seg_14013" s="T458">cardnum</ta>
            <ta e="T460" id="Seg_14014" s="T459">n</ta>
            <ta e="T461" id="Seg_14015" s="T460">ptcl</ta>
            <ta e="T462" id="Seg_14016" s="T461">v</ta>
            <ta e="T463" id="Seg_14017" s="T462">n</ta>
            <ta e="T464" id="Seg_14018" s="T463">v</ta>
            <ta e="T465" id="Seg_14019" s="T464">ptcl</ta>
            <ta e="T466" id="Seg_14020" s="T465">v</ta>
            <ta e="T467" id="Seg_14021" s="T466">ptcl</ta>
            <ta e="T468" id="Seg_14022" s="T467">dempro</ta>
            <ta e="T469" id="Seg_14023" s="T468">pers</ta>
            <ta e="T470" id="Seg_14024" s="T469">n</ta>
            <ta e="T471" id="Seg_14025" s="T470">ptcl</ta>
            <ta e="T472" id="Seg_14026" s="T471">cop</ta>
            <ta e="T473" id="Seg_14027" s="T472">dempro</ta>
            <ta e="T474" id="Seg_14028" s="T473">n</ta>
            <ta e="T475" id="Seg_14029" s="T474">v</ta>
            <ta e="T476" id="Seg_14030" s="T475">v</ta>
            <ta e="T477" id="Seg_14031" s="T476">adv</ta>
            <ta e="T478" id="Seg_14032" s="T477">adv</ta>
            <ta e="T479" id="Seg_14033" s="T478">v</ta>
            <ta e="T480" id="Seg_14034" s="T479">v</ta>
            <ta e="T481" id="Seg_14035" s="T480">ptcl</ta>
            <ta e="T482" id="Seg_14036" s="T481">dempro</ta>
            <ta e="T483" id="Seg_14037" s="T482">n</ta>
            <ta e="T484" id="Seg_14038" s="T483">que</ta>
            <ta e="T485" id="Seg_14039" s="T484">ptcl</ta>
            <ta e="T486" id="Seg_14040" s="T485">v</ta>
            <ta e="T487" id="Seg_14041" s="T486">adv</ta>
            <ta e="T488" id="Seg_14042" s="T487">que</ta>
            <ta e="T489" id="Seg_14043" s="T488">quant</ta>
            <ta e="T490" id="Seg_14044" s="T489">n</ta>
            <ta e="T491" id="Seg_14045" s="T490">v</ta>
            <ta e="T492" id="Seg_14046" s="T491">adj</ta>
            <ta e="T493" id="Seg_14047" s="T492">n</ta>
            <ta e="T494" id="Seg_14048" s="T493">n</ta>
            <ta e="T495" id="Seg_14049" s="T494">ptcl</ta>
            <ta e="T496" id="Seg_14050" s="T495">adv</ta>
            <ta e="T497" id="Seg_14051" s="T496">adj</ta>
            <ta e="T498" id="Seg_14052" s="T497">n</ta>
            <ta e="T499" id="Seg_14053" s="T498">v</ta>
            <ta e="T500" id="Seg_14054" s="T499">aux</ta>
            <ta e="T501" id="Seg_14055" s="T500">adv</ta>
            <ta e="T502" id="Seg_14056" s="T501">emphpro</ta>
            <ta e="T503" id="Seg_14057" s="T502">v</ta>
            <ta e="T504" id="Seg_14058" s="T503">v</ta>
            <ta e="T505" id="Seg_14059" s="T504">ptcl</ta>
            <ta e="T506" id="Seg_14060" s="T505">adv</ta>
            <ta e="T518" id="Seg_14061" s="T517">ptcl</ta>
            <ta e="T519" id="Seg_14062" s="T518">n</ta>
            <ta e="T520" id="Seg_14063" s="T519">v</ta>
            <ta e="T521" id="Seg_14064" s="T520">adj</ta>
            <ta e="T522" id="Seg_14065" s="T521">n</ta>
            <ta e="T523" id="Seg_14066" s="T522">n</ta>
            <ta e="T524" id="Seg_14067" s="T523">adv</ta>
            <ta e="T525" id="Seg_14068" s="T524">v</ta>
            <ta e="T526" id="Seg_14069" s="T525">v</ta>
            <ta e="T527" id="Seg_14070" s="T526">dempro</ta>
            <ta e="T528" id="Seg_14071" s="T527">v</ta>
            <ta e="T529" id="Seg_14072" s="T528">ptcl</ta>
            <ta e="T530" id="Seg_14073" s="T529">v</ta>
            <ta e="T531" id="Seg_14074" s="T530">ptcl</ta>
            <ta e="T532" id="Seg_14075" s="T531">que</ta>
            <ta e="T533" id="Seg_14076" s="T532">adv</ta>
            <ta e="T534" id="Seg_14077" s="T533">v</ta>
            <ta e="T535" id="Seg_14078" s="T534">que</ta>
            <ta e="T536" id="Seg_14079" s="T535">v</ta>
            <ta e="T537" id="Seg_14080" s="T536">v</ta>
            <ta e="T538" id="Seg_14081" s="T537">dempro</ta>
            <ta e="T539" id="Seg_14082" s="T538">v</ta>
            <ta e="T540" id="Seg_14083" s="T539">v</ta>
            <ta e="T559" id="Seg_14084" s="T558">ptcl</ta>
            <ta e="T560" id="Seg_14085" s="T559">v</ta>
            <ta e="T561" id="Seg_14086" s="T560">dempro</ta>
            <ta e="T562" id="Seg_14087" s="T561">dempro</ta>
            <ta e="T563" id="Seg_14088" s="T562">dempro</ta>
            <ta e="T564" id="Seg_14089" s="T563">adv</ta>
            <ta e="T565" id="Seg_14090" s="T564">adj</ta>
            <ta e="T566" id="Seg_14091" s="T565">n</ta>
            <ta e="T567" id="Seg_14092" s="T566">v</ta>
            <ta e="T568" id="Seg_14093" s="T567">ptcl</ta>
            <ta e="T569" id="Seg_14094" s="T568">n</ta>
            <ta e="T570" id="Seg_14095" s="T569">adj</ta>
            <ta e="T571" id="Seg_14096" s="T570">n</ta>
            <ta e="T572" id="Seg_14097" s="T571">v</ta>
            <ta e="T573" id="Seg_14098" s="T572">interj</ta>
            <ta e="T574" id="Seg_14099" s="T573">v</ta>
            <ta e="T575" id="Seg_14100" s="T574">v</ta>
            <ta e="T576" id="Seg_14101" s="T575">dempro</ta>
            <ta e="T577" id="Seg_14102" s="T576">v</ta>
            <ta e="T578" id="Seg_14103" s="T577">v</ta>
            <ta e="T579" id="Seg_14104" s="T578">ptcl</ta>
            <ta e="T580" id="Seg_14105" s="T579">aux</ta>
            <ta e="T581" id="Seg_14106" s="T580">n</ta>
            <ta e="T582" id="Seg_14107" s="T581">v</ta>
            <ta e="T583" id="Seg_14108" s="T582">v</ta>
            <ta e="T584" id="Seg_14109" s="T583">adv</ta>
            <ta e="T585" id="Seg_14110" s="T584">ptcl</ta>
            <ta e="T586" id="Seg_14111" s="T585">pers</ta>
            <ta e="T587" id="Seg_14112" s="T586">v</ta>
            <ta e="T588" id="Seg_14113" s="T587">v</ta>
            <ta e="T589" id="Seg_14114" s="T588">adv</ta>
            <ta e="T590" id="Seg_14115" s="T589">v</ta>
            <ta e="T591" id="Seg_14116" s="T590">aux</ta>
            <ta e="T592" id="Seg_14117" s="T591">ptcl</ta>
            <ta e="T593" id="Seg_14118" s="T592">dempro</ta>
            <ta e="T594" id="Seg_14119" s="T593">post</ta>
            <ta e="T595" id="Seg_14120" s="T594">v</ta>
            <ta e="T611" id="Seg_14121" s="T610">dempro</ta>
            <ta e="T612" id="Seg_14122" s="T611">v</ta>
            <ta e="T613" id="Seg_14123" s="T612">dempro</ta>
            <ta e="T614" id="Seg_14124" s="T613">propr</ta>
            <ta e="T615" id="Seg_14125" s="T614">v</ta>
            <ta e="T616" id="Seg_14126" s="T615">n</ta>
            <ta e="T617" id="Seg_14127" s="T616">v</ta>
            <ta e="T618" id="Seg_14128" s="T617">v</ta>
            <ta e="T619" id="Seg_14129" s="T618">n</ta>
            <ta e="T620" id="Seg_14130" s="T619">ptcl</ta>
            <ta e="T621" id="Seg_14131" s="T620">v</ta>
            <ta e="T642" id="Seg_14132" s="T641">ptcl</ta>
            <ta e="T643" id="Seg_14133" s="T642">dempro</ta>
            <ta e="T644" id="Seg_14134" s="T643">propr</ta>
            <ta e="T645" id="Seg_14135" s="T644">post</ta>
            <ta e="T646" id="Seg_14136" s="T645">ptcl</ta>
            <ta e="T647" id="Seg_14137" s="T646">interj</ta>
            <ta e="T648" id="Seg_14138" s="T647">v</ta>
            <ta e="T649" id="Seg_14139" s="T648">v</ta>
            <ta e="T650" id="Seg_14140" s="T649">ptcl</ta>
            <ta e="T651" id="Seg_14141" s="T650">v</ta>
            <ta e="T652" id="Seg_14142" s="T651">ptcl</ta>
            <ta e="T653" id="Seg_14143" s="T652">adv</ta>
            <ta e="T654" id="Seg_14144" s="T653">v</ta>
            <ta e="T655" id="Seg_14145" s="T654">v</ta>
            <ta e="T656" id="Seg_14146" s="T655">interj</ta>
            <ta e="T657" id="Seg_14147" s="T656">n</ta>
            <ta e="T658" id="Seg_14148" s="T657">v</ta>
            <ta e="T659" id="Seg_14149" s="T658">v</ta>
            <ta e="T660" id="Seg_14150" s="T659">ptcl</ta>
            <ta e="T663" id="Seg_14151" s="T662">v</ta>
            <ta e="T664" id="Seg_14152" s="T663">v</ta>
            <ta e="T665" id="Seg_14153" s="T664">v</ta>
            <ta e="T666" id="Seg_14154" s="T665">adv</ta>
            <ta e="T667" id="Seg_14155" s="T666">n</ta>
            <ta e="T668" id="Seg_14156" s="T667">v</ta>
            <ta e="T669" id="Seg_14157" s="T668">n</ta>
            <ta e="T685" id="Seg_14158" s="T684">dempro</ta>
            <ta e="T686" id="Seg_14159" s="T685">ptcl</ta>
            <ta e="T687" id="Seg_14160" s="T686">adj</ta>
            <ta e="T688" id="Seg_14161" s="T687">n</ta>
            <ta e="T689" id="Seg_14162" s="T688">v</ta>
            <ta e="T690" id="Seg_14163" s="T689">ptcl</ta>
            <ta e="T691" id="Seg_14164" s="T690">n</ta>
            <ta e="T692" id="Seg_14165" s="T691">adj</ta>
            <ta e="T693" id="Seg_14166" s="T692">n</ta>
            <ta e="T694" id="Seg_14167" s="T693">n</ta>
            <ta e="T695" id="Seg_14168" s="T694">v</ta>
            <ta e="T696" id="Seg_14169" s="T695">ptcl</ta>
            <ta e="T697" id="Seg_14170" s="T696">pers</ta>
            <ta e="T698" id="Seg_14171" s="T697">n</ta>
            <ta e="T699" id="Seg_14172" s="T698">dempro</ta>
            <ta e="T700" id="Seg_14173" s="T699">cop</ta>
            <ta e="T701" id="Seg_14174" s="T700">dempro</ta>
            <ta e="T702" id="Seg_14175" s="T701">n</ta>
            <ta e="T703" id="Seg_14176" s="T702">adv</ta>
            <ta e="T704" id="Seg_14177" s="T703">v</ta>
            <ta e="T705" id="Seg_14178" s="T704">post</ta>
            <ta e="T706" id="Seg_14179" s="T705">dempro</ta>
            <ta e="T707" id="Seg_14180" s="T706">adj</ta>
            <ta e="T708" id="Seg_14181" s="T707">n</ta>
            <ta e="T709" id="Seg_14182" s="T708">v</ta>
            <ta e="T710" id="Seg_14183" s="T709">aux</ta>
            <ta e="T711" id="Seg_14184" s="T710">ptcl</ta>
            <ta e="T712" id="Seg_14185" s="T711">propr</ta>
            <ta e="T713" id="Seg_14186" s="T712">v</ta>
            <ta e="T714" id="Seg_14187" s="T713">n</ta>
            <ta e="T715" id="Seg_14188" s="T714">dempro</ta>
            <ta e="T716" id="Seg_14189" s="T715">dempro</ta>
            <ta e="T728" id="Seg_14190" s="T727">dempro</ta>
            <ta e="T729" id="Seg_14191" s="T728">posspr</ta>
            <ta e="T730" id="Seg_14192" s="T729">n</ta>
            <ta e="T731" id="Seg_14193" s="T730">dempro</ta>
            <ta e="T761" id="Seg_14194" s="T760">ptcl</ta>
            <ta e="T762" id="Seg_14195" s="T761">ptcl</ta>
            <ta e="T763" id="Seg_14196" s="T762">dempro</ta>
            <ta e="T764" id="Seg_14197" s="T763">pers</ta>
            <ta e="T765" id="Seg_14198" s="T764">v</ta>
            <ta e="T766" id="Seg_14199" s="T765">n</ta>
            <ta e="T767" id="Seg_14200" s="T766">adj</ta>
            <ta e="T768" id="Seg_14201" s="T767">propr</ta>
            <ta e="T769" id="Seg_14202" s="T768">v</ta>
            <ta e="T770" id="Seg_14203" s="T769">pers</ta>
            <ta e="T771" id="Seg_14204" s="T770">v</ta>
            <ta e="T772" id="Seg_14205" s="T771">post</ta>
            <ta e="T773" id="Seg_14206" s="T772">v</ta>
            <ta e="T774" id="Seg_14207" s="T773">adv</ta>
            <ta e="T775" id="Seg_14208" s="T774">v</ta>
            <ta e="T776" id="Seg_14209" s="T775">%%</ta>
            <ta e="T777" id="Seg_14210" s="T776">n</ta>
            <ta e="T778" id="Seg_14211" s="T777">v</ta>
            <ta e="T779" id="Seg_14212" s="T778">v</ta>
            <ta e="T780" id="Seg_14213" s="T779">dempro</ta>
            <ta e="T781" id="Seg_14214" s="T780">ptcl</ta>
            <ta e="T782" id="Seg_14215" s="T781">dempro</ta>
            <ta e="T783" id="Seg_14216" s="T782">que</ta>
            <ta e="T784" id="Seg_14217" s="T783">que</ta>
            <ta e="T785" id="Seg_14218" s="T784">v</ta>
            <ta e="T786" id="Seg_14219" s="T785">aux</ta>
            <ta e="T787" id="Seg_14220" s="T786">aux</ta>
            <ta e="T788" id="Seg_14221" s="T787">adv</ta>
            <ta e="T789" id="Seg_14222" s="T788">n</ta>
            <ta e="T793" id="Seg_14223" s="T792">ptcl</ta>
            <ta e="T794" id="Seg_14224" s="T793">v</ta>
            <ta e="T795" id="Seg_14225" s="T794">dempro</ta>
            <ta e="T796" id="Seg_14226" s="T795">v</ta>
            <ta e="T797" id="Seg_14227" s="T796">v</ta>
            <ta e="T798" id="Seg_14228" s="T797">dempro</ta>
            <ta e="T800" id="Seg_14229" s="T799">dempro</ta>
            <ta e="T802" id="Seg_14230" s="T801">dempro</ta>
            <ta e="T803" id="Seg_14231" s="T802">v</ta>
            <ta e="T804" id="Seg_14232" s="T803">dempro</ta>
            <ta e="T805" id="Seg_14233" s="T804">v</ta>
            <ta e="T806" id="Seg_14234" s="T805">que</ta>
            <ta e="T807" id="Seg_14235" s="T806">ptcl</ta>
            <ta e="T808" id="Seg_14236" s="T807">dempro</ta>
            <ta e="T809" id="Seg_14237" s="T808">adv</ta>
            <ta e="T810" id="Seg_14238" s="T809">v</ta>
            <ta e="T811" id="Seg_14239" s="T810">adv</ta>
            <ta e="T812" id="Seg_14240" s="T811">v</ta>
            <ta e="T813" id="Seg_14241" s="T812">n</ta>
            <ta e="T820" id="Seg_14242" s="T819">adv</ta>
            <ta e="T821" id="Seg_14243" s="T820">ptcl</ta>
            <ta e="T822" id="Seg_14244" s="T821">v</ta>
            <ta e="T823" id="Seg_14245" s="T822">adv</ta>
            <ta e="T824" id="Seg_14246" s="T823">n</ta>
            <ta e="T825" id="Seg_14247" s="T824">ptcl</ta>
            <ta e="T826" id="Seg_14248" s="T825">v</ta>
            <ta e="T849" id="Seg_14249" s="T848">ptcl</ta>
            <ta e="T850" id="Seg_14250" s="T849">ptcl</ta>
            <ta e="T851" id="Seg_14251" s="T850">ptcl</ta>
            <ta e="T852" id="Seg_14252" s="T851">dempro</ta>
            <ta e="T853" id="Seg_14253" s="T852">n</ta>
            <ta e="T854" id="Seg_14254" s="T853">v</ta>
            <ta e="T855" id="Seg_14255" s="T854">pers</ta>
            <ta e="T856" id="Seg_14256" s="T855">n</ta>
            <ta e="T857" id="Seg_14257" s="T856">ptcl</ta>
            <ta e="T858" id="Seg_14258" s="T857">n</ta>
            <ta e="T859" id="Seg_14259" s="T858">adv</ta>
            <ta e="T860" id="Seg_14260" s="T859">collnum</ta>
            <ta e="T861" id="Seg_14261" s="T860">ptcl</ta>
            <ta e="T862" id="Seg_14262" s="T861">v</ta>
            <ta e="T863" id="Seg_14263" s="T862">cardnum</ta>
            <ta e="T864" id="Seg_14264" s="T863">adj</ta>
            <ta e="T865" id="Seg_14265" s="T864">collnum</ta>
            <ta e="T866" id="Seg_14266" s="T865">ptcl</ta>
            <ta e="T867" id="Seg_14267" s="T866">v</ta>
            <ta e="T868" id="Seg_14268" s="T867">adj</ta>
            <ta e="T869" id="Seg_14269" s="T868">adj</ta>
            <ta e="T870" id="Seg_14270" s="T869">adj</ta>
            <ta e="T871" id="Seg_14271" s="T870">pers</ta>
            <ta e="T872" id="Seg_14272" s="T871">ptcl</ta>
            <ta e="T873" id="Seg_14273" s="T872">adj</ta>
            <ta e="T874" id="Seg_14274" s="T873">v</ta>
            <ta e="T875" id="Seg_14275" s="T874">dempro</ta>
            <ta e="T876" id="Seg_14276" s="T875">n</ta>
            <ta e="T877" id="Seg_14277" s="T876">n</ta>
            <ta e="T878" id="Seg_14278" s="T877">conj</ta>
            <ta e="T879" id="Seg_14279" s="T878">v</ta>
            <ta e="T880" id="Seg_14280" s="T879">n</ta>
            <ta e="T881" id="Seg_14281" s="T880">conj</ta>
            <ta e="T882" id="Seg_14282" s="T881">ptcl</ta>
            <ta e="T883" id="Seg_14283" s="T882">v</ta>
            <ta e="T888" id="Seg_14284" s="T887">adv</ta>
            <ta e="T889" id="Seg_14285" s="T888">n</ta>
            <ta e="T890" id="Seg_14286" s="T889">dempro</ta>
            <ta e="T891" id="Seg_14287" s="T890">adv</ta>
            <ta e="T892" id="Seg_14288" s="T891">n</ta>
            <ta e="T893" id="Seg_14289" s="T892">n</ta>
            <ta e="T894" id="Seg_14290" s="T893">v</ta>
            <ta e="T895" id="Seg_14291" s="T894">ptcl</ta>
            <ta e="T896" id="Seg_14292" s="T895">adv</ta>
            <ta e="T897" id="Seg_14293" s="T896">adv</ta>
            <ta e="T898" id="Seg_14294" s="T897">adv</ta>
            <ta e="T899" id="Seg_14295" s="T898">pers</ta>
            <ta e="T900" id="Seg_14296" s="T899">v</ta>
            <ta e="T901" id="Seg_14297" s="T900">n</ta>
            <ta e="T902" id="Seg_14298" s="T901">n</ta>
            <ta e="T903" id="Seg_14299" s="T902">v</ta>
            <ta e="T904" id="Seg_14300" s="T903">que</ta>
            <ta e="T905" id="Seg_14301" s="T904">ptcl</ta>
            <ta e="T906" id="Seg_14302" s="T905">cop</ta>
            <ta e="T907" id="Seg_14303" s="T906">adv</ta>
            <ta e="T908" id="Seg_14304" s="T907">n</ta>
            <ta e="T909" id="Seg_14305" s="T908">n</ta>
            <ta e="T910" id="Seg_14306" s="T909">que</ta>
            <ta e="T911" id="Seg_14307" s="T910">ptcl</ta>
            <ta e="T912" id="Seg_14308" s="T911">v</ta>
            <ta e="T913" id="Seg_14309" s="T912">adv</ta>
            <ta e="T914" id="Seg_14310" s="T913">n</ta>
            <ta e="T915" id="Seg_14311" s="T914">n</ta>
            <ta e="T916" id="Seg_14312" s="T915">adj</ta>
            <ta e="T917" id="Seg_14313" s="T916">n</ta>
            <ta e="T918" id="Seg_14314" s="T917">n</ta>
            <ta e="T933" id="Seg_14315" s="T932">ptcl</ta>
            <ta e="T934" id="Seg_14316" s="T933">ptcl</ta>
            <ta e="T935" id="Seg_14317" s="T934">adv</ta>
            <ta e="T936" id="Seg_14318" s="T935">ptcl</ta>
            <ta e="T937" id="Seg_14319" s="T936">que</ta>
            <ta e="T938" id="Seg_14320" s="T937">ptcl</ta>
            <ta e="T939" id="Seg_14321" s="T938">adv</ta>
            <ta e="T940" id="Seg_14322" s="T939">v</ta>
            <ta e="T941" id="Seg_14323" s="T940">dempro</ta>
            <ta e="T942" id="Seg_14324" s="T941">adv</ta>
            <ta e="T943" id="Seg_14325" s="T942">dempro</ta>
            <ta e="T944" id="Seg_14326" s="T943">n</ta>
            <ta e="T945" id="Seg_14327" s="T944">adv</ta>
            <ta e="T946" id="Seg_14328" s="T945">dempro</ta>
            <ta e="T947" id="Seg_14329" s="T946">n</ta>
            <ta e="T948" id="Seg_14330" s="T947">v</ta>
            <ta e="T949" id="Seg_14331" s="T948">dempro</ta>
            <ta e="T950" id="Seg_14332" s="T949">adj</ta>
            <ta e="T951" id="Seg_14333" s="T950">n</ta>
            <ta e="T952" id="Seg_14334" s="T951">dempro</ta>
            <ta e="T953" id="Seg_14335" s="T952">post</ta>
            <ta e="T954" id="Seg_14336" s="T953">pers</ta>
            <ta e="T955" id="Seg_14337" s="T954">adv</ta>
            <ta e="T956" id="Seg_14338" s="T955">v</ta>
            <ta e="T957" id="Seg_14339" s="T956">dempro</ta>
            <ta e="T958" id="Seg_14340" s="T957">n</ta>
            <ta e="T959" id="Seg_14341" s="T958">adj</ta>
            <ta e="T960" id="Seg_14342" s="T959">v</ta>
            <ta e="T961" id="Seg_14343" s="T960">aux</ta>
            <ta e="T962" id="Seg_14344" s="T961">ptcl</ta>
            <ta e="T963" id="Seg_14345" s="T962">adj</ta>
            <ta e="T964" id="Seg_14346" s="T963">que</ta>
            <ta e="T965" id="Seg_14347" s="T964">ptcl</ta>
            <ta e="T966" id="Seg_14348" s="T965">v</ta>
            <ta e="T967" id="Seg_14349" s="T966">n</ta>
            <ta e="T968" id="Seg_14350" s="T967">conj</ta>
            <ta e="T969" id="Seg_14351" s="T968">v</ta>
            <ta e="T970" id="Seg_14352" s="T969">n</ta>
            <ta e="T971" id="Seg_14353" s="T970">conj</ta>
            <ta e="T972" id="Seg_14354" s="T971">v</ta>
            <ta e="T973" id="Seg_14355" s="T972">n</ta>
            <ta e="T974" id="Seg_14356" s="T973">v</ta>
            <ta e="T975" id="Seg_14357" s="T974">ptcl</ta>
            <ta e="T976" id="Seg_14358" s="T975">propr</ta>
            <ta e="T977" id="Seg_14359" s="T976">n</ta>
            <ta e="T978" id="Seg_14360" s="T977">ptcl</ta>
            <ta e="T979" id="Seg_14361" s="T978">n</ta>
            <ta e="T980" id="Seg_14362" s="T979">ptcl</ta>
            <ta e="T991" id="Seg_14363" s="T990">que</ta>
            <ta e="T992" id="Seg_14364" s="T991">v</ta>
            <ta e="T993" id="Seg_14365" s="T992">conj</ta>
            <ta e="T994" id="Seg_14366" s="T993">dempro</ta>
            <ta e="T995" id="Seg_14367" s="T994">pers</ta>
            <ta e="T996" id="Seg_14368" s="T995">n</ta>
            <ta e="T997" id="Seg_14369" s="T996">v</ta>
            <ta e="T998" id="Seg_14370" s="T997">ptcl</ta>
            <ta e="T999" id="Seg_14371" s="T998">pers</ta>
            <ta e="T1000" id="Seg_14372" s="T999">que</ta>
            <ta e="T1001" id="Seg_14373" s="T1000">v</ta>
            <ta e="T1002" id="Seg_14374" s="T1001">adv</ta>
            <ta e="T1003" id="Seg_14375" s="T1002">adv</ta>
            <ta e="T1004" id="Seg_14376" s="T1003">v</ta>
            <ta e="T1005" id="Seg_14377" s="T1004">ptcl</ta>
            <ta e="T1006" id="Seg_14378" s="T1005">adv</ta>
            <ta e="T1007" id="Seg_14379" s="T1006">v</ta>
            <ta e="T1008" id="Seg_14380" s="T1007">ptcl</ta>
            <ta e="T1009" id="Seg_14381" s="T1008">v</ta>
            <ta e="T1010" id="Seg_14382" s="T1009">v</ta>
            <ta e="T1011" id="Seg_14383" s="T1010">v</ta>
            <ta e="T1012" id="Seg_14384" s="T1011">v</ta>
            <ta e="T1013" id="Seg_14385" s="T1012">ptcl</ta>
            <ta e="T1014" id="Seg_14386" s="T1013">v</ta>
            <ta e="T1015" id="Seg_14387" s="T1014">ptcl</ta>
            <ta e="T1016" id="Seg_14388" s="T1015">ptcl</ta>
            <ta e="T1017" id="Seg_14389" s="T1016">ptcl</ta>
            <ta e="T1018" id="Seg_14390" s="T1017">adv</ta>
            <ta e="T1019" id="Seg_14391" s="T1018">que</ta>
            <ta e="T1020" id="Seg_14392" s="T1019">ptcl</ta>
            <ta e="T1021" id="Seg_14393" s="T1020">v</ta>
            <ta e="T1022" id="Seg_14394" s="T1021">ptcl</ta>
            <ta e="T1042" id="Seg_14395" s="T1041">ptcl</ta>
            <ta e="T1054" id="Seg_14396" s="T1053">interj</ta>
            <ta e="T1055" id="Seg_14397" s="T1054">n</ta>
            <ta e="T1056" id="Seg_14398" s="T1055">v</ta>
            <ta e="T1075" id="Seg_14399" s="T1074">n</ta>
            <ta e="T1076" id="Seg_14400" s="T1075">n</ta>
            <ta e="T1077" id="Seg_14401" s="T1076">adj</ta>
            <ta e="T1078" id="Seg_14402" s="T1077">n</ta>
            <ta e="T1079" id="Seg_14403" s="T1078">n</ta>
            <ta e="T1080" id="Seg_14404" s="T1079">v</ta>
            <ta e="T1081" id="Seg_14405" s="T1080">propr</ta>
            <ta e="T1082" id="Seg_14406" s="T1081">n</ta>
            <ta e="T1083" id="Seg_14407" s="T1082">n</ta>
            <ta e="T1084" id="Seg_14408" s="T1083">n</ta>
            <ta e="T1085" id="Seg_14409" s="T1084">v</ta>
            <ta e="T1086" id="Seg_14410" s="T1085">v</ta>
            <ta e="T1087" id="Seg_14411" s="T1086">v</ta>
            <ta e="T1088" id="Seg_14412" s="T1087">propr</ta>
            <ta e="T1089" id="Seg_14413" s="T1088">n</ta>
            <ta e="T1090" id="Seg_14414" s="T1089">v</ta>
            <ta e="T1091" id="Seg_14415" s="T1090">adj</ta>
            <ta e="T1092" id="Seg_14416" s="T1091">adj</ta>
            <ta e="T1093" id="Seg_14417" s="T1092">n</ta>
            <ta e="T1094" id="Seg_14418" s="T1093">v</ta>
            <ta e="T1095" id="Seg_14419" s="T1094">adv</ta>
            <ta e="T1096" id="Seg_14420" s="T1095">v</ta>
            <ta e="T1097" id="Seg_14421" s="T1096">n</ta>
            <ta e="T1098" id="Seg_14422" s="T1097">n</ta>
            <ta e="T1099" id="Seg_14423" s="T1098">v</ta>
            <ta e="T1100" id="Seg_14424" s="T1099">pers</ta>
            <ta e="T1101" id="Seg_14425" s="T1100">n</ta>
            <ta e="T1102" id="Seg_14426" s="T1101">v</ta>
            <ta e="T1103" id="Seg_14427" s="T1102">n</ta>
            <ta e="T1104" id="Seg_14428" s="T1103">n</ta>
            <ta e="T1105" id="Seg_14429" s="T1104">v</ta>
            <ta e="T1106" id="Seg_14430" s="T1105">n</ta>
            <ta e="T1107" id="Seg_14431" s="T1106">n</ta>
            <ta e="T1108" id="Seg_14432" s="T1107">n</ta>
            <ta e="T1109" id="Seg_14433" s="T1108">v</ta>
            <ta e="T1110" id="Seg_14434" s="T1109">n</ta>
            <ta e="T1111" id="Seg_14435" s="T1110">v</ta>
            <ta e="T1112" id="Seg_14436" s="T1111">adv</ta>
            <ta e="T1113" id="Seg_14437" s="T1112">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-UkET" />
         <annotation name="SyF" tierref="SyF-UkET" />
         <annotation name="IST" tierref="IST-UkET" />
         <annotation name="Top" tierref="Top-UkET" />
         <annotation name="Foc" tierref="Foc-UkET" />
         <annotation name="BOR" tierref="BOR-UkET">
            <ta e="T25" id="Seg_14438" s="T23">RUS:gram</ta>
            <ta e="T41" id="Seg_14439" s="T40">RUS:cult</ta>
            <ta e="T64" id="Seg_14440" s="T63">RUS:cult</ta>
            <ta e="T68" id="Seg_14441" s="T67">RUS:cult</ta>
            <ta e="T80" id="Seg_14442" s="T79">RUS:cult</ta>
            <ta e="T81" id="Seg_14443" s="T80">RUS:cult</ta>
            <ta e="T82" id="Seg_14444" s="T81">RUS:cult</ta>
            <ta e="T91" id="Seg_14445" s="T90">RUS:cult</ta>
            <ta e="T93" id="Seg_14446" s="T92">RUS:cult</ta>
            <ta e="T94" id="Seg_14447" s="T93">RUS:cult</ta>
            <ta e="T97" id="Seg_14448" s="T96">RUS:cult</ta>
            <ta e="T103" id="Seg_14449" s="T102">RUS:cult</ta>
            <ta e="T112" id="Seg_14450" s="T111">RUS:cult</ta>
            <ta e="T117" id="Seg_14451" s="T116">RUS:cult</ta>
            <ta e="T124" id="Seg_14452" s="T123">RUS:core</ta>
            <ta e="T129" id="Seg_14453" s="T128">RUS:cult</ta>
            <ta e="T135" id="Seg_14454" s="T134">RUS:cult</ta>
            <ta e="T146" id="Seg_14455" s="T145">RUS:cult</ta>
            <ta e="T164" id="Seg_14456" s="T163">RUS:cult</ta>
            <ta e="T166" id="Seg_14457" s="T165">RUS:cult</ta>
            <ta e="T167" id="Seg_14458" s="T166">RUS:cult</ta>
            <ta e="T182" id="Seg_14459" s="T181">RUS:cult</ta>
            <ta e="T229" id="Seg_14460" s="T228">RUS:cult</ta>
            <ta e="T242" id="Seg_14461" s="T241">RUS:cult</ta>
            <ta e="T250" id="Seg_14462" s="T249">RUS:cult</ta>
            <ta e="T274" id="Seg_14463" s="T273">RUS:cult</ta>
            <ta e="T275" id="Seg_14464" s="T274">RUS:core</ta>
            <ta e="T302" id="Seg_14465" s="T301">EV:gram (DIM)</ta>
            <ta e="T307" id="Seg_14466" s="T306">RUS:cult</ta>
            <ta e="T314" id="Seg_14467" s="T313">RUS:cult</ta>
            <ta e="T321" id="Seg_14468" s="T320">RUS:cult</ta>
            <ta e="T324" id="Seg_14469" s="T323">EV:gram (DIM)</ta>
            <ta e="T358" id="Seg_14470" s="T357">RUS:core</ta>
            <ta e="T363" id="Seg_14471" s="T362">RUS:gram</ta>
            <ta e="T369" id="Seg_14472" s="T368">RUS:gram</ta>
            <ta e="T373" id="Seg_14473" s="T372">RUS:gram</ta>
            <ta e="T380" id="Seg_14474" s="T379">RUS:cult</ta>
            <ta e="T385" id="Seg_14475" s="T384">RUS:cult</ta>
            <ta e="T405" id="Seg_14476" s="T404">EV:cult</ta>
            <ta e="T412" id="Seg_14477" s="T411">EV:cult</ta>
            <ta e="T455" id="Seg_14478" s="T454">RUS:cult</ta>
            <ta e="T463" id="Seg_14479" s="T462">RUS:cult</ta>
            <ta e="T483" id="Seg_14480" s="T482">RUS:core</ta>
            <ta e="T486" id="Seg_14481" s="T485">RUS:core</ta>
            <ta e="T565" id="Seg_14482" s="T564">RUS:core</ta>
            <ta e="T569" id="Seg_14483" s="T568">RUS:core</ta>
            <ta e="T570" id="Seg_14484" s="T569">RUS:core</ta>
            <ta e="T587" id="Seg_14485" s="T586">RUS:cult</ta>
            <ta e="T590" id="Seg_14486" s="T589">RUS:core</ta>
            <ta e="T657" id="Seg_14487" s="T656">RUS:cult</ta>
            <ta e="T702" id="Seg_14488" s="T701">RUS:cult</ta>
            <ta e="T712" id="Seg_14489" s="T711">RUS:cult</ta>
            <ta e="T820" id="Seg_14490" s="T819">RUS:mod</ta>
            <ta e="T824" id="Seg_14491" s="T823">RUS:cult</ta>
            <ta e="T878" id="Seg_14492" s="T877">RUS:gram</ta>
            <ta e="T881" id="Seg_14493" s="T880">RUS:gram</ta>
            <ta e="T968" id="Seg_14494" s="T967">RUS:gram</ta>
            <ta e="T971" id="Seg_14495" s="T970">RUS:gram</ta>
            <ta e="T973" id="Seg_14496" s="T972">RUS:cult</ta>
            <ta e="T976" id="Seg_14497" s="T975">RUS:cult</ta>
            <ta e="T993" id="Seg_14498" s="T992">RUS:gram</ta>
            <ta e="T1011" id="Seg_14499" s="T1010">RUS:mod</ta>
            <ta e="T1016" id="Seg_14500" s="T1015">RUS:disc</ta>
            <ta e="T1078" id="Seg_14501" s="T1077">RUS:cult</ta>
            <ta e="T1095" id="Seg_14502" s="T1094">EV:gram (DIM)</ta>
            <ta e="T1106" id="Seg_14503" s="T1105">EV:gram (DIM)</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-UkET" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-UkET" />
         <annotation name="CS" tierref="CS-UkET" />
         <annotation name="fe" tierref="fe-UkET">
            <ta e="T43" id="Seg_14504" s="T21">– My place of birth was there, to the north, there, to whatchamacallit, eh, opposite to Novorybnoe.</ta>
            <ta e="T52" id="Seg_14505" s="T43">I was born in the north, having been married, I lived more southwards.</ta>
            <ta e="T55" id="Seg_14506" s="T52">My children so…</ta>
            <ta e="T68" id="Seg_14507" s="T55">What shall I tell: One of my children is working in school, one of my daughters is a director.</ta>
            <ta e="T81" id="Seg_14508" s="T68">One, eh, one of my children in Khtanga, my big daughter is a doctor in a school in Khatanga.</ta>
            <ta e="T82" id="Seg_14509" s="T81">Nina.</ta>
            <ta e="T87" id="Seg_14510" s="T82">The name of my big daughter.</ta>
            <ta e="T91" id="Seg_14511" s="T87">Small, after her Katya.</ta>
            <ta e="T94" id="Seg_14512" s="T91">She is a director in school. </ta>
            <ta e="T109" id="Seg_14513" s="T94">One of my daughters was working in kindergarten, that one, the kindergartens and so broke down(?) and she had children and so on (?).</ta>
            <ta e="T120" id="Seg_14514" s="T109">Now she is ill and she is there in Dudinka, eh, she is in Norilsk for healing.</ta>
            <ta e="T125" id="Seg_14515" s="T120">And one of my sons became a hunter.</ta>
            <ta e="T139" id="Seg_14516" s="T125">One of my sons whatchamacallit, he makes airplanes and other stuff, he dispatches airplanes (?), he makes them fly, he is there. </ta>
            <ta e="T155" id="Seg_14517" s="T139">One of my daughters finished whatchamacallit, the kindergartner training, having become old myself, I keep her in the tundra, for that I can make her work for myself. </ta>
            <ta e="T169" id="Seg_14518" s="T155">My husband was earlier a hunter, eh, in Novorybnoe he was the leading hunter at the sovkhoz.</ta>
            <ta e="T183" id="Seg_14519" s="T169">Now he is ill, his hand is injured, he has ceased hunting, additionally (…) he retired now.</ta>
            <ta e="T190" id="Seg_14520" s="T183">Now in these years he has gone in [= retired]. </ta>
            <ta e="T209" id="Seg_14521" s="T202">– Yes, my children, now I am caring for my grandchildren. </ta>
            <ta e="T226" id="Seg_14522" s="T209">The child, which has come for healing, has three children herself, I was caring about her children, so now. </ta>
            <ta e="T233" id="Seg_14523" s="T226">My child, when my Masha came, I came from the tundra.</ta>
            <ta e="T251" id="Seg_14524" s="T233">I hold these my three children in the tundra, her mummy is here, her father has gone away, it was a Nenets.</ta>
            <ta e="T253" id="Seg_14525" s="T251">That one has gone away.</ta>
            <ta e="T273" id="Seg_14526" s="T253">Therefore one child after another, the children of my children never go away, in summer, in both summer and winter I have children.</ta>
            <ta e="T277" id="Seg_14527" s="T273">My grandchildren are always there.</ta>
            <ta e="T315" id="Seg_14528" s="T299">– Yes, we are telling different things, they don't understand it well yet, only the children of big daughter have started school [already].</ta>
            <ta e="T321" id="Seg_14529" s="T315">The children of my younger children don't go to school yet.</ta>
            <ta e="T340" id="Seg_14530" s="T321">I am telling them various things: different stuff, they are turning around and say: "Granny, tell that, tell a tale, tell this, sing that."</ta>
            <ta e="T348" id="Seg_14531" s="T340">For entertaining them, sing and tell (?), that's arduous, though.</ta>
            <ta e="T361" id="Seg_14532" s="T355">– Well, my ancestors were all tale tellers.</ta>
            <ta e="T384" id="Seg_14533" s="T361">They were singing songs, they were telling tales with songs, or simply tales, at that time, in olden times one hardly watched [anything] in the cinema.</ta>
            <ta e="T387" id="Seg_14534" s="T384">One listens to the "Spidola".</ta>
            <ta e="T395" id="Seg_14535" s="T387">One plays with wooden toys, with small birds, one makes sledges.</ta>
            <ta e="T401" id="Seg_14536" s="T395">When you go out, you throw [pieces of] antlers, you play with [pieces of] antlers.</ta>
            <ta e="T406" id="Seg_14537" s="T401">The boys play with reindeer nooses.</ta>
            <ta e="T413" id="Seg_14538" s="T406">How you nomadize in summer, you collect stones and play reindeer caravan.</ta>
            <ta e="T423" id="Seg_14539" s="T413">So you spend the day, the former children play like in olden times.</ta>
            <ta e="T439" id="Seg_14540" s="T423">For that You can see such a toy from olden times, I have brought a round and thick toy to this place.</ta>
            <ta e="T445" id="Seg_14541" s="T439">I'll show it however, that game, how one plays it.</ta>
            <ta e="T465" id="Seg_14542" s="T445">I have nobody, so that I could make and bring some toys, my husband was lying in the hospital, one of my sons just came from the hospital…</ta>
            <ta e="T487" id="Seg_14543" s="T465">I came, that my one was in the tundra, he went to the village and became sick, than later he came out, our arrival, snowstorms and so on disturbed(?)…</ta>
            <ta e="T500" id="Seg_14544" s="T487">I didn't bring many toys, I would have brought toys of the former children for showing them.</ta>
            <ta e="T506" id="Seg_14545" s="T500">Earlier I myself played when growing up.</ta>
            <ta e="T522" id="Seg_14546" s="T517">Well, we wind it with a thread on a wood with holes.</ta>
            <ta e="T526" id="Seg_14547" s="T522">It is standing upside down and is turning.</ta>
            <ta e="T536" id="Seg_14548" s="T526">You only get it when you see it, how shall I tell it?</ta>
            <ta e="T537" id="Seg_14549" s="T536">I'll show it.</ta>
            <ta e="T540" id="Seg_14550" s="T537">If you see it, get to know it.</ta>
            <ta e="T561" id="Seg_14551" s="T558">– Yes, I know such.</ta>
            <ta e="T569" id="Seg_14552" s="T561">Such, then I'll just sing any song, any?</ta>
            <ta e="T573" id="Seg_14553" s="T569">I'll sing some song, wait.</ta>
            <ta e="T576" id="Seg_14554" s="T573">It doesn't come that easy.</ta>
            <ta e="T583" id="Seg_14555" s="T576">Something it doesn't come immediately (?), I don't remember the melody, I've forgotten.</ta>
            <ta e="T595" id="Seg_14556" s="T583">Earlier I thought I would deal with it, but I didn't do so much, therefore I forget.</ta>
            <ta e="T617" id="Seg_14557" s="T610">She has died, a girl named Baya sang it.</ta>
            <ta e="T621" id="Seg_14558" s="T617">The boy, to whom it was sung, also died.</ta>
            <ta e="T652" id="Seg_14559" s="T641">– Yes, with such ones with Tonya, eh, we'll whatchamacallit, we'll compete in singing.</ta>
            <ta e="T666" id="Seg_14560" s="T652">Simply breathing, breathing, eh, holding one's breath, breathing you sing until the end, when you say, I'll simply sing. </ta>
            <ta e="T669" id="Seg_14561" s="T666">A poem with holding one's breath.</ta>
            <ta e="T705" id="Seg_14562" s="T684">– The ancestors were singing it with one single breath, it is said, one breath, they were holding their breath, it was their game, like artists are playing today. </ta>
            <ta e="T716" id="Seg_14563" s="T705">One man named Innokentiy reached it with one single breath, it is said.</ta>
            <ta e="T731" id="Seg_14564" s="T727">– That's my daughter.</ta>
            <ta e="T769" id="Seg_14565" s="T760">– Yes, they told the tale "(…) Taiska" which I had told. </ta>
            <ta e="T779" id="Seg_14566" s="T769">After they had asked me to tell, I came here to come, they came (…) in a year to play.</ta>
            <ta e="T789" id="Seg_14567" s="T779">And they told those ones then the rest(?).</ta>
            <ta e="T799" id="Seg_14568" s="T792">– Yes, I heard, hearing it I make.</ta>
            <ta e="T805" id="Seg_14569" s="T799">They come and I scold them:</ta>
            <ta e="T813" id="Seg_14570" s="T805">"Why do you tell your tale like this and that, why don't you tell your tale correctly?"</ta>
            <ta e="T826" id="Seg_14571" s="T819">– In any case it is recognized, they tell the most important though.</ta>
            <ta e="T851" id="Seg_14572" s="T848">– Yes, there is.</ta>
            <ta e="T864" id="Seg_14573" s="T851">There is my brother who knows tales, both my brothers even know [some], I have two brothers.</ta>
            <ta e="T873" id="Seg_14574" s="T864">Both know, the one is old, the other is older than I.</ta>
            <ta e="T883" id="Seg_14575" s="T873">They know such tales, they know such with songs and such without songs.</ta>
            <ta e="T903" id="Seg_14576" s="T887">– Simply the people, our former tale tellers ran out, now really, the tale tellers which I know ran out.</ta>
            <ta e="T909" id="Seg_14577" s="T903">Who is a tale teller now?</ta>
            <ta e="T918" id="Seg_14578" s="T909">I don't know a tale teller now, a real tale teller.</ta>
            <ta e="T943" id="Seg_14579" s="T932">– No, no, earlier nobody did it like that, that's now.</ta>
            <ta e="T963" id="Seg_14580" s="T943">They found this trend just recently, the old tale, therefore we did forget it completely, we are forgetting our tales completely, everything.</ta>
            <ta e="T966" id="Seg_14581" s="T963">You never tell them.</ta>
            <ta e="T972" id="Seg_14582" s="T966">You forget the songs and you forget the tales.</ta>
            <ta e="T980" id="Seg_14583" s="T972">There is also the old man Anisim who tells stories, also a tale teller.</ta>
            <ta e="T995" id="Seg_14584" s="T990">– Who(?) knows them.</ta>
            <ta e="T1001" id="Seg_14585" s="T995">They are probably tired of tales (?)?</ta>
            <ta e="T1015" id="Seg_14586" s="T1001">We don't tell each other tales often, only when they say "just tell", then I tell, when they need it, when they ask to tell, then you tell.</ta>
            <ta e="T1022" id="Seg_14587" s="T1015">And so we simply never tell.</ta>
            <ta e="T1042" id="Seg_14588" s="T1041">– Yes.</ta>
            <ta e="T1056" id="Seg_14589" s="T1053">– Well, of course a different.</ta>
            <ta e="T1080" id="Seg_14590" s="T1074">– I made a verse about my child.</ta>
            <ta e="T1082" id="Seg_14591" s="T1080">About Ajsuluu.</ta>
            <ta e="T1085" id="Seg_14592" s="T1082">The stars are visible in the sky.</ta>
            <ta e="T1088" id="Seg_14593" s="T1085">Sleep, sleep, Aysuluu.</ta>
            <ta e="T1094" id="Seg_14594" s="T1088">The times are changing, an interesting and different time has come.</ta>
            <ta e="T1099" id="Seg_14595" s="T1094">Grow quickly, hold my thread and my needle.</ta>
            <ta e="T1102" id="Seg_14596" s="T1099">Take my fate as your fate (?).</ta>
            <ta e="T1105" id="Seg_14597" s="T1102">Treat a human like a human.</ta>
            <ta e="T1109" id="Seg_14598" s="T1105">I dandle my granchild in his cradle.</ta>
            <ta e="T1113" id="Seg_14599" s="T1109">I am sitting beneath him and sang [=sing] silently.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-UkET">
            <ta e="T43" id="Seg_14600" s="T21">– Mein Geburtsort war dort, in Richtung Norden, dort, in Richtung Dings, äh, auf der anderen Seite von Novorybnoe.</ta>
            <ta e="T52" id="Seg_14601" s="T43">Ich wurde im Norden geboren, nachdem ich geheiratet hatte, lebte ich weiter im Süden.</ta>
            <ta e="T55" id="Seg_14602" s="T52">Meine Kinder so…</ta>
            <ta e="T68" id="Seg_14603" s="T55">Was soll ich erzählen: Mein eines Kind arbeitet in der Schule, meine eine Tochter ist Direktorin.</ta>
            <ta e="T81" id="Seg_14604" s="T68">Ein, äh, eins meiner Kinder in Chatanga, meine große Tochter is Ärztin in einer Schule in Chatanga.</ta>
            <ta e="T82" id="Seg_14605" s="T81">Nina.</ta>
            <ta e="T87" id="Seg_14606" s="T82">Der Name meiner großen Tochter.</ta>
            <ta e="T91" id="Seg_14607" s="T87">Klein, nach ihr Katja.</ta>
            <ta e="T94" id="Seg_14608" s="T91">Die ist Direktorin in der Schule.</ta>
            <ta e="T109" id="Seg_14609" s="T94">Meine eine Tochter arbeitete im Kindergarten, jene, die Kindergärten und so gingen kaputt(?) und sie bekam Kinder und so (?).</ta>
            <ta e="T120" id="Seg_14610" s="T109">Jetzt ist sie krank und ist da Richtung Dudinka, äh, sie ist jetzt zur Heilung in Norilsk.</ta>
            <ta e="T125" id="Seg_14611" s="T120">Und mein einer Sohn wurde Jäger.</ta>
            <ta e="T139" id="Seg_14612" s="T125">Einer meiner Söhne dingst, macht Flugzeuge und andere Sachen, er fertig Flugzeuge ab (?), er lässt sie fliegen, er ist dort.</ta>
            <ta e="T155" id="Seg_14613" s="T139">Meine eine Tochter hat Dings beendet, die Ausbildung zur Kindergärtnerin, wo ich jetzt selbst alt geworden bin, halte ich sie in der Tundra, damit ich sie für mich selbst arbeiten lassen.</ta>
            <ta e="T169" id="Seg_14614" s="T155">Mein Mann war früher Jäger, äh, in Novorybnoe war er dann führender Jäger in der Sowchose.</ta>
            <ta e="T183" id="Seg_14615" s="T169">Jetzt ist er krank, seine Hand ist verletzt, er hat mit der Jagd aufgehört, zusätzlich (…) ist er jetzt in Rente gegangen.</ta>
            <ta e="T190" id="Seg_14616" s="T183">Jetzt in diesen Jahren ist er gegangen.</ta>
            <ta e="T209" id="Seg_14617" s="T202">– Ja, meine Kinder jetzt, jetzt kümmere ich mich um meine Enkel.</ta>
            <ta e="T226" id="Seg_14618" s="T209">Das Kind, das zur Heilung gekommen ist, hat selbst drei Kinder, um deren Kinder habe ich mich gekümmert, also jetzt.</ta>
            <ta e="T233" id="Seg_14619" s="T226">Mein Kind, als meine Mascha kam, da kam ich aus der Tundra.</ta>
            <ta e="T251" id="Seg_14620" s="T233">Ich halte diese meine drei Kinder in der Tundra, ihre Mama ist hier, ihr Vater ist weggegangen, das war ein Nenze.</ta>
            <ta e="T253" id="Seg_14621" s="T251">Der ist weggegangen.</ta>
            <ta e="T273" id="Seg_14622" s="T253">Deshalb ein Kind nach dem anderen, die Kinder meiner Kinder gehen nie weg, im Sommer, im Sommer wie im Winter habe ich Kinder.</ta>
            <ta e="T277" id="Seg_14623" s="T273">Meine Enkel sind immer da.</ta>
            <ta e="T315" id="Seg_14624" s="T299">– Ja, wir erzählen unterschiedliche Sachen, jetzt verstehen sie es noch nicht so, nur die Kinder von meiner großen Tochter sind [schon] zur Schule gekommen.</ta>
            <ta e="T321" id="Seg_14625" s="T315">Die Kinder von meinen jüngeren Kindern gehen noch nicht zur Schule.</ta>
            <ta e="T340" id="Seg_14626" s="T321">Jenen erzähle ich unterschiedliche Sachen: Unterschiedliches, sie drehen sich und sagen: "Oma, erzähl das, erzähl ein Märchen, erzähl dies, sing das."</ta>
            <ta e="T348" id="Seg_14627" s="T340">Um sie zu unterhalten, singt und erzählt (?), das ist doch zäh.</ta>
            <ta e="T361" id="Seg_14628" s="T355">– Nun, meine Vorfahren waren alle Märchenerzähler.</ta>
            <ta e="T384" id="Seg_14629" s="T361">Man hat Lieder gesungen, man hat Märchen mit Liedern erzählt, oder einfach Märchen, damals, in alten Zeiten hat man kaum [etwas] im Kino geguckt.</ta>
            <ta e="T387" id="Seg_14630" s="T384">Man hört die "Spidola".</ta>
            <ta e="T395" id="Seg_14631" s="T387">Man spielt mit Holzspielzeug, mit Vögelchen, man macht Schlitten.</ta>
            <ta e="T401" id="Seg_14632" s="T395">Wenn dur rausgehst, wirfst du Geweih[stücke], du spielst mit Geweih[stücken].</ta>
            <ta e="T406" id="Seg_14633" s="T401">Die Jungen spielen mit Rentierschlingen.</ta>
            <ta e="T413" id="Seg_14634" s="T406">Wie du im Sommer nomadisierst, du sammelst dir Steine und spielst Rentierkarawane.</ta>
            <ta e="T423" id="Seg_14635" s="T413">So verbringst du den Tag, die früheren Kinder spielen wie früher.</ta>
            <ta e="T439" id="Seg_14636" s="T423">Damit Sie so ein Spielzeug von früher sehen können, habe ich ein rundes und dickes Spielzeug mit hierher gebracht.</ta>
            <ta e="T445" id="Seg_14637" s="T439">Ich zeige es doch, das Spiel, wie man es spielt.</ta>
            <ta e="T465" id="Seg_14638" s="T445">Ich habe niemanden, damit ich einige Spielzeuge mache und mitbringen, mein Mann lag im Krankenhaus, mein einer Sohn kam gerade aus dem Krankenhaus…</ta>
            <ta e="T487" id="Seg_14639" s="T465">Ich kam, dieser meine war in der Tundra, er ging ins Dorf und wurde krank, dann später kam er heraus, unsere Ankunft, Schneestürme und so haben gestört(?)…</ta>
            <ta e="T500" id="Seg_14640" s="T487">Ich habe nicht viel Spielzeug mitgebracht, ich hätte zum Zeigen Spielzeug von den Kindern von damals mitgebracht.</ta>
            <ta e="T506" id="Seg_14641" s="T500">Früher habe ich selbst gespielt, als ich aufgewachsen bin.</ta>
            <ta e="T522" id="Seg_14642" s="T517">Nun, mit einem Faden wickeln wir das auf ein Holz mit Löchern.</ta>
            <ta e="T526" id="Seg_14643" s="T522">Es steht kopfüber und dreht sich.</ta>
            <ta e="T536" id="Seg_14644" s="T526">Das verstehst du nur, wenn du es siehst, wie soll ich das erzählen?</ta>
            <ta e="T537" id="Seg_14645" s="T536">Ich zeige es.</ta>
            <ta e="T540" id="Seg_14646" s="T537">Wenn du es siehst, erfahre es.</ta>
            <ta e="T561" id="Seg_14647" s="T558">– Ja, ich kenne solche.</ta>
            <ta e="T569" id="Seg_14648" s="T561">So eins, dann singe ich einfach irgendein Lied, irgendeins?</ta>
            <ta e="T573" id="Seg_14649" s="T569">Ich singe irgendein Lied, warte.</ta>
            <ta e="T576" id="Seg_14650" s="T573">Das kommt nicht einfach.</ta>
            <ta e="T583" id="Seg_14651" s="T576">Manchmal kommt es nicht sofort (?), ich erinnere die Melodie nicht, ich habe sie vergessen.</ta>
            <ta e="T595" id="Seg_14652" s="T583">Früher dachte ich, ich würde mich damit beschäftigen, ich habe aber nicht viel gemacht, deshalb vergesse ich.</ta>
            <ta e="T617" id="Seg_14653" s="T610">Sie ist gestorben, das hat ein Mädchen mit dem Namen Baja gesungen.</ta>
            <ta e="T621" id="Seg_14654" s="T617">Der Junge, dem es gesungen wurde, starb auch.</ta>
            <ta e="T652" id="Seg_14655" s="T641">– Ja, mit solchen mit Tonja, äh, dingsen wir, singen wir um die Wette.</ta>
            <ta e="T666" id="Seg_14656" s="T652">Einfach, atmend, atmend, äh, Luft anhalten, du atmest und sing bis zum Ende, wenn du sagst, singe ich einfach.</ta>
            <ta e="T669" id="Seg_14657" s="T666">Ein Gedicht mit Luft anhalten. </ta>
            <ta e="T705" id="Seg_14658" s="T684">– Das sangen die Vorfahren mit einem einzigen Atemzug, sagt man, ein Atemzug, sie hielten den Atem an, das war ihr Spiel, wie Künstler heute spielen.</ta>
            <ta e="T716" id="Seg_14659" s="T705">Mit einem einzigen Atemzug erreichte das, sagt man, ein Mensch namens Innokentij.</ta>
            <ta e="T731" id="Seg_14660" s="T727">– Das ist meine Tochter.</ta>
            <ta e="T769" id="Seg_14661" s="T760">– Ja, sie erzählten das Märchen "Die (…) Taiska", das ich erzählt hatte.</ta>
            <ta e="T779" id="Seg_14662" s="T769">Nachdem sie mich gebeten hatten zu erzählen, kam ich hierher um zu kommen, sie kamen (…) im Jahr um zu spielen.</ta>
            <ta e="T789" id="Seg_14663" s="T779">Und denen erzählten sie dann irgendwie die Reste(?).</ta>
            <ta e="T799" id="Seg_14664" s="T792">– Ja, habe ich gehört, ich höre das und mache.</ta>
            <ta e="T805" id="Seg_14665" s="T799">Sie kommen und ich schimpfe mit ihnen:</ta>
            <ta e="T813" id="Seg_14666" s="T805">"Warum erzählt ihr das Märchen so und so, warum erzählt ihr euer Märchen nicht richtig?"</ta>
            <ta e="T826" id="Seg_14667" s="T819">– Jedenfalls wird es erkannt, sie erzählen doch das Wichtigste.</ta>
            <ta e="T851" id="Seg_14668" s="T848">– Ja, gibt es.</ta>
            <ta e="T864" id="Seg_14669" s="T851">Es gibt meinen Bruder, der Märchen kennt, meine beiden Brüder sogar kennen [welche], ich habe zwei Brüder.</ta>
            <ta e="T873" id="Seg_14670" s="T864">Beide wissen, der eine ist alt, der andere ist älter als ich.</ta>
            <ta e="T883" id="Seg_14671" s="T873">Sie kennen solche Märchen, sie kennen sowohl solche mit Liedern als auch solche ohne Lieder.</ta>
            <ta e="T903" id="Seg_14672" s="T887">– Einfach die Leute, unsere früheren Märchenerzähler sind zuende gegangen, jetzt wirklich, die Märchenerzähler, die ich kenne, sind zuende gehen.</ta>
            <ta e="T909" id="Seg_14673" s="T903">Wer ist jetzt noch Märchenerzähler?</ta>
            <ta e="T918" id="Seg_14674" s="T909">Ich kenne jetzt keinen Märchenerzähler, keinen richtigen Märchenerzähler.</ta>
            <ta e="T943" id="Seg_14675" s="T932">– Nein, nein, früher hat das keiner so gemacht, das ist jetzt.</ta>
            <ta e="T963" id="Seg_14676" s="T943">Die Mode haben sie erst seit kurzem gefunden, das alte Märchen, deshalb haben wir das ganz vergessen, unsere Märchen vergessen wir ganz, alles.</ta>
            <ta e="T966" id="Seg_14677" s="T963">Man erzählt sie nie.</ta>
            <ta e="T972" id="Seg_14678" s="T966">Du vergisst die Lieder und du vergisst die Märchen.</ta>
            <ta e="T980" id="Seg_14679" s="T972">Es gibt noch den alten Mann Anisim, der Geschichten erzählt, auch ein Märchenerzähler.</ta>
            <ta e="T995" id="Seg_14680" s="T990">– Wer(?) kennt sie.</ta>
            <ta e="T1001" id="Seg_14681" s="T995">Sie haben wahrscheinlich genug von Märchen (?)?</ta>
            <ta e="T1015" id="Seg_14682" s="T1001">Wir erzählen einander nicht oft Märchen, nur wenn sie sagen "erzähl einfach", erzähle ich, wenn sie es brauchen, wenn sie bitten zu erzählen, dann erzählt man.</ta>
            <ta e="T1022" id="Seg_14683" s="T1015">Und so erzählen wir einfach nie.</ta>
            <ta e="T1042" id="Seg_14684" s="T1041">– Ja.</ta>
            <ta e="T1056" id="Seg_14685" s="T1053">– Nun, natürlich ein anderes.</ta>
            <ta e="T1080" id="Seg_14686" s="T1074">– Über mein Kind habe ich noch einen Vers gemacht.</ta>
            <ta e="T1082" id="Seg_14687" s="T1080">Über Ajsülüü.</ta>
            <ta e="T1085" id="Seg_14688" s="T1082">Die Sterne sind am Himmel zu sehen.</ta>
            <ta e="T1088" id="Seg_14689" s="T1085">Schlaf, schlaf, Ajsülüü.</ta>
            <ta e="T1094" id="Seg_14690" s="T1088">Die Zeiten verändern sich, es ist eine interessante andere Zeit gekommen.</ta>
            <ta e="T1099" id="Seg_14691" s="T1094">Wachse schnell, halt meinen Faden und meine Nadel.</ta>
            <ta e="T1102" id="Seg_14692" s="T1099">Nimm mein Schicksal als dein Schicksal (?).</ta>
            <ta e="T1105" id="Seg_14693" s="T1102">Behandele einen Menschen wie ein Mensch.</ta>
            <ta e="T1109" id="Seg_14694" s="T1105">Ich schaukele mein Enkelkind in seiner Wiege.</ta>
            <ta e="T1113" id="Seg_14695" s="T1109">Ich sitze daneben und sang [=singe] leise.</ta>
         </annotation>
         <annotation name="fr" tierref="fr-UkET" />
         <annotation name="ltr" tierref="ltr-UkET">
            <ta e="T43" id="Seg_14696" s="T21">ЕТ: Моё место рождения здесь было, севернее, там, Хат.. (Хатанга) в той стороне, э-э, от Новорыбной на противоположном берегу. </ta>
            <ta e="T52" id="Seg_14697" s="T43">ЕТ: На севере родилась, позже после замужества южнее проживала.</ta>
            <ta e="T55" id="Seg_14698" s="T52">ЕТ: Дети-то что… </ta>
            <ta e="T68" id="Seg_14699" s="T55">Как рассказать-то, что рассказать: Один ребёнок тут в школе работает, одна дочь директором работает.</ta>
            <ta e="T81" id="Seg_14700" s="T68">ЕТ: Один, э, один ребёнок в Хатанге, старш.. самая старшая дочь в Хатангe в школе врачом работает.</ta>
            <ta e="T82" id="Seg_14701" s="T81">Нина.</ta>
            <ta e="T87" id="Seg_14702" s="T82">ЕТ: Одн.. ту старшую дочь зовут.</ta>
            <ta e="T91" id="Seg_14703" s="T87">Младшая, после неё Катя.</ta>
            <ta e="T94" id="Seg_14704" s="T91">Та в школе директором работает.</ta>
            <ta e="T109" id="Seg_14705" s="T94">ЕТ: Одна дочь в детском саду работала, эта э-э, в саду их что-то вышло из строя и вообще детей родив и прочее…</ta>
            <ta e="T120" id="Seg_14706" s="T109">ЕТ: Сейчас заболев в стороне Дудинки находится вот, э-э, в Норильске на лечении находится сейчас.</ta>
            <ta e="T125" id="Seg_14707" s="T120">ЕТ: Потом один сын охотником стал.</ta>
            <ta e="T139" id="Seg_14708" s="T125">ЕТ: Один сын эти самолёты .. их прочее отправ.. это делает, ремонтирует, самолеты заправляет, в полет отправляет, там находится.</ta>
            <ta e="T155" id="Seg_14709" s="T139">ЕТ: Одна дочь моя .. это заканчивала, на садика работника, сейчас сама состарившись, её для себя чтобы попросить работать в тундре держу.</ta>
            <ta e="T169" id="Seg_14710" s="T155">ЕТ: Муж раньше добытчиком был всё-таки. челов…, от человека, э-э, в Новорыбной ведь в совхозе передовым охотником был.</ta>
            <ta e="T183" id="Seg_14711" s="T169">ET: Сейчас заболев, из-за болезни руки из охоты даже вышел, и к тому же сейчас на "списание", пенсию вошёл.</ta>
            <ta e="T190" id="Seg_14712" s="T183">Вот сейчас в этих годах вошёл.</ta>
            <ta e="T209" id="Seg_14713" s="T202">ЕТ: Да, сейчас вот детей моих, с внуками няньчась…</ta>
            <ta e="T226" id="Seg_14714" s="T209">Та, что на лечение приехавшей, у дочери трое детей, за их детьми смотрела, сейчас вот…</ta>
            <ta e="T233" id="Seg_14715" s="T226">ЕТ: Дочь вот, как Маша приехала, так и приехала из тундры. </ta>
            <ta e="T251" id="Seg_14716" s="T233">ЕТ: В тундре держу ведь этих троих детей, той (тех) мама здесь находится, папа разошёлся, тот ненец был.</ta>
            <ta e="T253" id="Seg_14717" s="T251">ЕТ: Тот разошёлся.</ta>
            <ta e="T273" id="Seg_14718" s="T253">ЕТ: Поэтому вот как же один за другим ребёнок ведь, внуки никогда не отходят ведь, летом, все летние времена, зимние времена подряд с детьми бываю.</ta>
            <ta e="T277" id="Seg_14719" s="T273">ЕТ: Внуки постоянно бывают.</ta>
            <ta e="T315" id="Seg_14720" s="T299">ЕТ: Да, рассказывали что-нибудь ведь, сейчас не очень понимают-то, старшей дочери дети только в школу пошли.</ta>
            <ta e="T321" id="Seg_14721" s="T315">ЕТ: Младших детей дети ещё не пошли в школу.</ta>
            <ta e="T340" id="Seg_14722" s="T321">ЕТ: Вот им что-нибудь рассказываю: и то, и другое, бабушка, говоря, вокруг ходят ведь – то расскажи, эту сказку расскажи, вот это расскажи, то спой.</ta>
            <ta e="T348" id="Seg_14723" s="T340">ЕТ: Тем чтобы ответить, и споешь, и расскажешь – всё-равно много вот.</ta>
            <ta e="T361" id="Seg_14724" s="T355">ЕТ: Так вот, мои предки все сказителями были.</ta>
            <ta e="T384" id="Seg_14725" s="T361">ЕТ: И пели песни, напевали, и с песнями сказку и рассказывали, простые сказки сказывали… тогда, в старые времена ведь кино смотришь разве.</ta>
            <ta e="T387" id="Seg_14726" s="T384">ЕТ: "Спидолу" слушаешь разве.</ta>
            <ta e="T395" id="Seg_14727" s="T387">ЕТ: Деревянной игрушкой играешь: птичками, вот сани делая. </ta>
            <ta e="T401" id="Seg_14728" s="T395">ЕТ: На улицу как выйдешь, рога раскидав, рогами играть будешь.</ta>
            <ta e="T406" id="Seg_14729" s="T401">ЕТ: Мальчики вот, маут кидая, играть будет.</ta>
            <ta e="T413" id="Seg_14730" s="T406">ЕТ: Летом как гуляешь (кочуешь), камни складывая (в 2лб2к66н ?) играешь. </ta>
            <ta e="T423" id="Seg_14731" s="T413">ЕТ: Таким образом и день проходит ведь, тогдашний ребёнок по-старинному играли вот.</ta>
            <ta e="T439" id="Seg_14732" s="T423">ЕТ: Тем временем сейчас старинную игру в наше время увидели чтобы, игру-ча8каркатаак привезла ли в эти места. </ta>
            <ta e="T445" id="Seg_14733" s="T439">ЕТ: Всё равно покажу, эта игра, как играют.</ta>
            <ta e="T465" id="Seg_14734" s="T445">ЕТ: Некоторые игрушки чтобы сдел.. сделав привезти, никого нет у меня, муж мой вот в больнице лежал ведь, один сын, приболев, из больницы выйдя вот..</ta>
            <ta e="T487" id="Seg_14735" s="T465">ЕТ: Так и приехала вот, тот (я, мой) в тундре был, и тот, на берег (т.е. в посёлок) приехав, заболел, потом позже вышел, и приезд наш вот – и пурги, и что-то ещё помешав, так и…</ta>
            <ta e="T500" id="Seg_14736" s="T487">ЕТ: Никаких много игрушек не привезла, тогдашних детей игрушки для показа привезла бы. </ta>
            <ta e="T506" id="Seg_14737" s="T500">ЕТ: Раньше сама играючи выросла ведь.</ta>
            <ta e="T522" id="Seg_14738" s="T517">ЕТ: Вот, нитки проводим через деревяшку с дырками.</ta>
            <ta e="T526" id="Seg_14739" s="T522">ЕТ: Головой вниз стоя, крутится.</ta>
            <ta e="T536" id="Seg_14740" s="T526">ЕТ: Это, увидев только, поймёшь, как сейчас рассказывая, как расскажу?</ta>
            <ta e="T537" id="Seg_14741" s="T536">ЕТ: Покажу.</ta>
            <ta e="T540" id="Seg_14742" s="T537">ЕТ: Как увидишь, узнай.</ta>
            <ta e="T561" id="Seg_14743" s="T558">ЕТ: Да, знаю такие.</ta>
            <ta e="T569" id="Seg_14744" s="T561">ЕТ: Такую.. такую просто любую песню спою да, любую?</ta>
            <ta e="T573" id="Seg_14745" s="T569">ЕТ: Любую песню спою, подожди.</ta>
            <ta e="T576" id="Seg_14746" s="T573">ЕТ: Не приходит сразу она.</ta>
            <ta e="T583" id="Seg_14747" s="T576">ЕТ: Придёт.. не сразу приходящая бывает, мелодию не помню, забыла.</ta>
            <ta e="T595" id="Seg_14748" s="T583">ЕТ: Раньше, между прочим, им займусь, подумавши, никак не разрабатывая ходила ведь, поэтому забываю.</ta>
            <ta e="T617" id="Seg_14749" s="T610">ЕТ: Та умерла, это Байа с именем девушка спела.</ta>
            <ta e="T621" id="Seg_14750" s="T617">ЕТ: Пела которому, парень тоже умер.</ta>
            <ta e="T652" id="Seg_14751" s="T641">ЕТ: Да, ими с Тоней вместе, э-э, сделаем, состязание все равно споём, конечно.</ta>
            <ta e="T666" id="Seg_14752" s="T652">ЕТ: Просто, вдыхая, э-э, перехват дыхания вдыхая – вдыхая ведь, до конца пой, если скажешь, спою просто.</ta>
            <ta e="T669" id="Seg_14753" s="T666">ЕТ: Перехвата дыхания стихи.</ta>
            <ta e="T705" id="Seg_14754" s="T684">ЕТ: Это вот на одном дыхании пели, говорят старики, на одном дыхании, дыхание перехватывая, ну их игра та была, вот как артисты сейчас как играют. </ta>
            <ta e="T716" id="Seg_14755" s="T705">ЕТ: Это на одном дыхании доходил, говорят, Иннокентий его звали -- во на этом.</ta>
            <ta e="T731" id="Seg_14756" s="T727">ЕТ: Это моя дочь это.</ta>
            <ta e="T769" id="Seg_14757" s="T760">ЕТ: Да, вот ту я которую рассказывала сказку мою" Коптоок Таисья" рассказала.</ta>
            <ta e="T779" id="Seg_14758" s="T769">ЕТ: После как попросили меня рассказать, пришли сюда чтобы приехать, (?) в год приехали, чтобы проиграть.</ta>
            <ta e="T789" id="Seg_14759" s="T779">ЕТ: И то даже это как, каким-то образом рассказали вроде. Да-да.</ta>
            <ta e="T799" id="Seg_14760" s="T792">ЕТ: Да, слушала. Её услышав говорю это, хе-хе-хе…</ta>
            <ta e="T805" id="Seg_14761" s="T799">ЕТ: .. Те приходят, тех ругаю:</ta>
            <ta e="T813" id="Seg_14762" s="T805">"Почему вот это как попало рассказываете, как следует не рассказываете сказку вашу?"</ta>
            <ta e="T826" id="Seg_14763" s="T819">ЕТ: Всё равно узнают же, основное всё равно рассказывают.</ta>
            <ta e="T851" id="Seg_14764" s="T848">ЕТ: Да, есть вот.</ta>
            <ta e="T864" id="Seg_14765" s="T851">ЕТ: Вот сказку знающий мой брат есть, братья мои даже вдвоем знают, два брата имею.</ta>
            <ta e="T873" id="Seg_14766" s="T864">ЕТ: Вдвоём даже зна.. другой старый, другой чем я всё равно старше.</ta>
            <ta e="T883" id="Seg_14767" s="T873">ЕТ: Знают они сказки. И с песнями знают, и без песен знаю..</ta>
            <ta e="T903" id="Seg_14768" s="T887">ЕТ: Простые люди ну вот сказители закончались ведь, sейчас по настоящему знающие сказители закончились.</ta>
            <ta e="T909" id="Seg_14769" s="T903">ЕТ: Кто есть сейчас сказитель.</ta>
            <ta e="T918" id="Seg_14770" s="T909">ЕТ: Никого не знаю сейчас настоящего сказителя.</ta>
            <ta e="T943" id="Seg_14771" s="T932">ЕТ: Нет, нет раньше ведь никто из них так не делали, это сейчас, это…</ta>
            <ta e="T963" id="Seg_14772" s="T943">ЕТ: С прошлого лет чуть ближе вот моду нашли на старинную сказку, поэтому мы напрочь забыли их, сказки все позабывали ведь, все.</ta>
            <ta e="T966" id="Seg_14773" s="T963">ЕТ: Никогда не рассказываешь.</ta>
            <ta e="T972" id="Seg_14774" s="T966">ЕТ: И песни забываешь, и сказки забываешь.</ta>
            <ta e="T980" id="Seg_14775" s="T972">ЕТ: Историю рассказывает наверно Анисим дед сказитель есть тоже.</ta>
            <ta e="T995" id="Seg_14776" s="T990">ЕТ: Кто знает их.</ta>
            <ta e="T1001" id="Seg_14777" s="T995">ЕТ: Сказки запоминаются им кто знает?</ta>
            <ta e="T1015" id="Seg_14778" s="T1001">Часто, часто сейчас не рассказываем ведь, просто расскажи если попросят рассказываешь. Как понадобится как попросят только тогда рассказываешь ведь.</ta>
            <ta e="T1022" id="Seg_14779" s="T1015">ЕТ: Так-то просто никогдa не рассказываем.</ta>
            <ta e="T1042" id="Seg_14780" s="T1041">ET: Мм.</ta>
            <ta e="T1056" id="Seg_14781" s="T1053">ЕТ: Ну, другую конечно же.</ta>
            <ta e="T1080" id="Seg_14782" s="T1074">ЕТ: О своём ребёнке маленький стишок сочинила.</ta>
            <ta e="T1082" id="Seg_14783" s="T1080">ЕТ: Про Айсуллу.</ta>
            <ta e="T1085" id="Seg_14784" s="T1082">ЕТ: Звёзды в небе светятся.</ta>
            <ta e="T1088" id="Seg_14785" s="T1085">ЕТ: Спи, спи Айсулу.</ta>
            <ta e="T1094" id="Seg_14786" s="T1088">ЕТ: Время меняется, интересное другое время пришло.</ta>
            <ta e="T1099" id="Seg_14787" s="T1094">ЕТ: Побыстрее подрастешь нитки, иголки мне подержешь.</ta>
            <ta e="T1102" id="Seg_14788" s="T1099">ЕТ: Мою судьбу за свою судьбу возьмёшь.</ta>
            <ta e="T1105" id="Seg_14789" s="T1102">ЕТ: Человеку человеком стань.</ta>
            <ta e="T1109" id="Seg_14790" s="T1105">ЕТ: Внучку в люльке покачиваю.</ta>
            <ta e="T1113" id="Seg_14791" s="T1109">ЕТ: Рядышком сижу, тихо песенку пою (спела).</ta>
         </annotation>
         <annotation name="nt" tierref="nt-UkET">
            <ta e="T387" id="Seg_14792" s="T384">[DCh]: "Spidola" was a common portable radio during the Soviet time.</ta>
            <ta e="T805" id="Seg_14793" s="T799">[DCh]: Not clear why "oloru" in accusative case in first clause.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T679" />
            <conversion-tli id="T680" />
            <conversion-tli id="T681" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T687" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
            <conversion-tli id="T691" />
            <conversion-tli id="T692" />
            <conversion-tli id="T693" />
            <conversion-tli id="T694" />
            <conversion-tli id="T695" />
            <conversion-tli id="T696" />
            <conversion-tli id="T697" />
            <conversion-tli id="T698" />
            <conversion-tli id="T699" />
            <conversion-tli id="T700" />
            <conversion-tli id="T701" />
            <conversion-tli id="T702" />
            <conversion-tli id="T703" />
            <conversion-tli id="T704" />
            <conversion-tli id="T705" />
            <conversion-tli id="T706" />
            <conversion-tli id="T707" />
            <conversion-tli id="T708" />
            <conversion-tli id="T709" />
            <conversion-tli id="T710" />
            <conversion-tli id="T711" />
            <conversion-tli id="T712" />
            <conversion-tli id="T713" />
            <conversion-tli id="T714" />
            <conversion-tli id="T715" />
            <conversion-tli id="T716" />
            <conversion-tli id="T717" />
            <conversion-tli id="T718" />
            <conversion-tli id="T719" />
            <conversion-tli id="T720" />
            <conversion-tli id="T721" />
            <conversion-tli id="T722" />
            <conversion-tli id="T723" />
            <conversion-tli id="T724" />
            <conversion-tli id="T725" />
            <conversion-tli id="T726" />
            <conversion-tli id="T727" />
            <conversion-tli id="T728" />
            <conversion-tli id="T729" />
            <conversion-tli id="T730" />
            <conversion-tli id="T731" />
            <conversion-tli id="T732" />
            <conversion-tli id="T733" />
            <conversion-tli id="T734" />
            <conversion-tli id="T735" />
            <conversion-tli id="T736" />
            <conversion-tli id="T737" />
            <conversion-tli id="T738" />
            <conversion-tli id="T739" />
            <conversion-tli id="T740" />
            <conversion-tli id="T741" />
            <conversion-tli id="T742" />
            <conversion-tli id="T743" />
            <conversion-tli id="T744" />
            <conversion-tli id="T745" />
            <conversion-tli id="T746" />
            <conversion-tli id="T747" />
            <conversion-tli id="T748" />
            <conversion-tli id="T749" />
            <conversion-tli id="T750" />
            <conversion-tli id="T751" />
            <conversion-tli id="T752" />
            <conversion-tli id="T753" />
            <conversion-tli id="T754" />
            <conversion-tli id="T755" />
            <conversion-tli id="T756" />
            <conversion-tli id="T757" />
            <conversion-tli id="T758" />
            <conversion-tli id="T759" />
            <conversion-tli id="T760" />
            <conversion-tli id="T761" />
            <conversion-tli id="T762" />
            <conversion-tli id="T763" />
            <conversion-tli id="T764" />
            <conversion-tli id="T765" />
            <conversion-tli id="T766" />
            <conversion-tli id="T767" />
            <conversion-tli id="T768" />
            <conversion-tli id="T769" />
            <conversion-tli id="T770" />
            <conversion-tli id="T771" />
            <conversion-tli id="T772" />
            <conversion-tli id="T773" />
            <conversion-tli id="T774" />
            <conversion-tli id="T775" />
            <conversion-tli id="T776" />
            <conversion-tli id="T777" />
            <conversion-tli id="T778" />
            <conversion-tli id="T779" />
            <conversion-tli id="T780" />
            <conversion-tli id="T781" />
            <conversion-tli id="T782" />
            <conversion-tli id="T783" />
            <conversion-tli id="T784" />
            <conversion-tli id="T785" />
            <conversion-tli id="T786" />
            <conversion-tli id="T787" />
            <conversion-tli id="T788" />
            <conversion-tli id="T789" />
            <conversion-tli id="T790" />
            <conversion-tli id="T791" />
            <conversion-tli id="T792" />
            <conversion-tli id="T793" />
            <conversion-tli id="T794" />
            <conversion-tli id="T795" />
            <conversion-tli id="T796" />
            <conversion-tli id="T797" />
            <conversion-tli id="T798" />
            <conversion-tli id="T799" />
            <conversion-tli id="T800" />
            <conversion-tli id="T801" />
            <conversion-tli id="T802" />
            <conversion-tli id="T803" />
            <conversion-tli id="T804" />
            <conversion-tli id="T805" />
            <conversion-tli id="T806" />
            <conversion-tli id="T807" />
            <conversion-tli id="T808" />
            <conversion-tli id="T809" />
            <conversion-tli id="T810" />
            <conversion-tli id="T811" />
            <conversion-tli id="T812" />
            <conversion-tli id="T813" />
            <conversion-tli id="T814" />
            <conversion-tli id="T815" />
            <conversion-tli id="T816" />
            <conversion-tli id="T817" />
            <conversion-tli id="T818" />
            <conversion-tli id="T819" />
            <conversion-tli id="T820" />
            <conversion-tli id="T821" />
            <conversion-tli id="T822" />
            <conversion-tli id="T823" />
            <conversion-tli id="T824" />
            <conversion-tli id="T825" />
            <conversion-tli id="T826" />
            <conversion-tli id="T827" />
            <conversion-tli id="T828" />
            <conversion-tli id="T829" />
            <conversion-tli id="T830" />
            <conversion-tli id="T831" />
            <conversion-tli id="T832" />
            <conversion-tli id="T833" />
            <conversion-tli id="T834" />
            <conversion-tli id="T835" />
            <conversion-tli id="T836" />
            <conversion-tli id="T837" />
            <conversion-tli id="T838" />
            <conversion-tli id="T839" />
            <conversion-tli id="T840" />
            <conversion-tli id="T841" />
            <conversion-tli id="T842" />
            <conversion-tli id="T843" />
            <conversion-tli id="T844" />
            <conversion-tli id="T845" />
            <conversion-tli id="T846" />
            <conversion-tli id="T847" />
            <conversion-tli id="T848" />
            <conversion-tli id="T849" />
            <conversion-tli id="T850" />
            <conversion-tli id="T851" />
            <conversion-tli id="T852" />
            <conversion-tli id="T853" />
            <conversion-tli id="T854" />
            <conversion-tli id="T855" />
            <conversion-tli id="T856" />
            <conversion-tli id="T857" />
            <conversion-tli id="T858" />
            <conversion-tli id="T859" />
            <conversion-tli id="T860" />
            <conversion-tli id="T861" />
            <conversion-tli id="T862" />
            <conversion-tli id="T863" />
            <conversion-tli id="T864" />
            <conversion-tli id="T865" />
            <conversion-tli id="T866" />
            <conversion-tli id="T867" />
            <conversion-tli id="T868" />
            <conversion-tli id="T869" />
            <conversion-tli id="T870" />
            <conversion-tli id="T871" />
            <conversion-tli id="T872" />
            <conversion-tli id="T873" />
            <conversion-tli id="T874" />
            <conversion-tli id="T875" />
            <conversion-tli id="T876" />
            <conversion-tli id="T877" />
            <conversion-tli id="T878" />
            <conversion-tli id="T879" />
            <conversion-tli id="T880" />
            <conversion-tli id="T881" />
            <conversion-tli id="T882" />
            <conversion-tli id="T883" />
            <conversion-tli id="T884" />
            <conversion-tli id="T885" />
            <conversion-tli id="T886" />
            <conversion-tli id="T887" />
            <conversion-tli id="T888" />
            <conversion-tli id="T889" />
            <conversion-tli id="T890" />
            <conversion-tli id="T891" />
            <conversion-tli id="T892" />
            <conversion-tli id="T893" />
            <conversion-tli id="T894" />
            <conversion-tli id="T895" />
            <conversion-tli id="T896" />
            <conversion-tli id="T897" />
            <conversion-tli id="T898" />
            <conversion-tli id="T899" />
            <conversion-tli id="T900" />
            <conversion-tli id="T901" />
            <conversion-tli id="T902" />
            <conversion-tli id="T903" />
            <conversion-tli id="T904" />
            <conversion-tli id="T905" />
            <conversion-tli id="T906" />
            <conversion-tli id="T907" />
            <conversion-tli id="T908" />
            <conversion-tli id="T909" />
            <conversion-tli id="T910" />
            <conversion-tli id="T911" />
            <conversion-tli id="T912" />
            <conversion-tli id="T913" />
            <conversion-tli id="T914" />
            <conversion-tli id="T915" />
            <conversion-tli id="T916" />
            <conversion-tli id="T917" />
            <conversion-tli id="T918" />
            <conversion-tli id="T919" />
            <conversion-tli id="T920" />
            <conversion-tli id="T921" />
            <conversion-tli id="T922" />
            <conversion-tli id="T923" />
            <conversion-tli id="T924" />
            <conversion-tli id="T925" />
            <conversion-tli id="T926" />
            <conversion-tli id="T927" />
            <conversion-tli id="T928" />
            <conversion-tli id="T929" />
            <conversion-tli id="T930" />
            <conversion-tli id="T931" />
            <conversion-tli id="T932" />
            <conversion-tli id="T933" />
            <conversion-tli id="T934" />
            <conversion-tli id="T935" />
            <conversion-tli id="T936" />
            <conversion-tli id="T937" />
            <conversion-tli id="T938" />
            <conversion-tli id="T939" />
            <conversion-tli id="T940" />
            <conversion-tli id="T941" />
            <conversion-tli id="T942" />
            <conversion-tli id="T943" />
            <conversion-tli id="T944" />
            <conversion-tli id="T945" />
            <conversion-tli id="T946" />
            <conversion-tli id="T947" />
            <conversion-tli id="T948" />
            <conversion-tli id="T949" />
            <conversion-tli id="T950" />
            <conversion-tli id="T951" />
            <conversion-tli id="T952" />
            <conversion-tli id="T953" />
            <conversion-tli id="T954" />
            <conversion-tli id="T955" />
            <conversion-tli id="T956" />
            <conversion-tli id="T957" />
            <conversion-tli id="T958" />
            <conversion-tli id="T959" />
            <conversion-tli id="T960" />
            <conversion-tli id="T961" />
            <conversion-tli id="T962" />
            <conversion-tli id="T963" />
            <conversion-tli id="T964" />
            <conversion-tli id="T965" />
            <conversion-tli id="T966" />
            <conversion-tli id="T967" />
            <conversion-tli id="T968" />
            <conversion-tli id="T969" />
            <conversion-tli id="T970" />
            <conversion-tli id="T971" />
            <conversion-tli id="T972" />
            <conversion-tli id="T973" />
            <conversion-tli id="T974" />
            <conversion-tli id="T975" />
            <conversion-tli id="T976" />
            <conversion-tli id="T977" />
            <conversion-tli id="T978" />
            <conversion-tli id="T979" />
            <conversion-tli id="T980" />
            <conversion-tli id="T981" />
            <conversion-tli id="T982" />
            <conversion-tli id="T983" />
            <conversion-tli id="T984" />
            <conversion-tli id="T985" />
            <conversion-tli id="T986" />
            <conversion-tli id="T987" />
            <conversion-tli id="T988" />
            <conversion-tli id="T989" />
            <conversion-tli id="T990" />
            <conversion-tli id="T991" />
            <conversion-tli id="T992" />
            <conversion-tli id="T993" />
            <conversion-tli id="T994" />
            <conversion-tli id="T995" />
            <conversion-tli id="T996" />
            <conversion-tli id="T997" />
            <conversion-tli id="T998" />
            <conversion-tli id="T999" />
            <conversion-tli id="T1000" />
            <conversion-tli id="T1001" />
            <conversion-tli id="T1002" />
            <conversion-tli id="T1003" />
            <conversion-tli id="T1004" />
            <conversion-tli id="T1005" />
            <conversion-tli id="T1006" />
            <conversion-tli id="T1007" />
            <conversion-tli id="T1008" />
            <conversion-tli id="T1009" />
            <conversion-tli id="T1010" />
            <conversion-tli id="T1011" />
            <conversion-tli id="T1012" />
            <conversion-tli id="T1013" />
            <conversion-tli id="T1014" />
            <conversion-tli id="T1015" />
            <conversion-tli id="T1016" />
            <conversion-tli id="T1017" />
            <conversion-tli id="T1018" />
            <conversion-tli id="T1019" />
            <conversion-tli id="T1020" />
            <conversion-tli id="T1021" />
            <conversion-tli id="T1022" />
            <conversion-tli id="T1023" />
            <conversion-tli id="T1024" />
            <conversion-tli id="T1025" />
            <conversion-tli id="T1026" />
            <conversion-tli id="T1027" />
            <conversion-tli id="T1028" />
            <conversion-tli id="T1029" />
            <conversion-tli id="T1030" />
            <conversion-tli id="T1031" />
            <conversion-tli id="T1032" />
            <conversion-tli id="T1033" />
            <conversion-tli id="T1034" />
            <conversion-tli id="T1035" />
            <conversion-tli id="T1036" />
            <conversion-tli id="T1037" />
            <conversion-tli id="T1038" />
            <conversion-tli id="T1039" />
            <conversion-tli id="T1040" />
            <conversion-tli id="T1041" />
            <conversion-tli id="T1042" />
            <conversion-tli id="T1043" />
            <conversion-tli id="T1044" />
            <conversion-tli id="T1045" />
            <conversion-tli id="T1046" />
            <conversion-tli id="T1047" />
            <conversion-tli id="T1048" />
            <conversion-tli id="T1049" />
            <conversion-tli id="T1050" />
            <conversion-tli id="T1051" />
            <conversion-tli id="T1052" />
            <conversion-tli id="T1053" />
            <conversion-tli id="T1054" />
            <conversion-tli id="T1055" />
            <conversion-tli id="T1056" />
            <conversion-tli id="T1057" />
            <conversion-tli id="T1058" />
            <conversion-tli id="T1059" />
            <conversion-tli id="T1060" />
            <conversion-tli id="T1061" />
            <conversion-tli id="T1062" />
            <conversion-tli id="T1063" />
            <conversion-tli id="T1064" />
            <conversion-tli id="T1065" />
            <conversion-tli id="T1066" />
            <conversion-tli id="T1067" />
            <conversion-tli id="T1068" />
            <conversion-tli id="T1069" />
            <conversion-tli id="T1070" />
            <conversion-tli id="T1071" />
            <conversion-tli id="T1072" />
            <conversion-tli id="T1073" />
            <conversion-tli id="T1074" />
            <conversion-tli id="T1075" />
            <conversion-tli id="T1076" />
            <conversion-tli id="T1077" />
            <conversion-tli id="T1078" />
            <conversion-tli id="T1079" />
            <conversion-tli id="T1080" />
            <conversion-tli id="T1081" />
            <conversion-tli id="T1082" />
            <conversion-tli id="T1083" />
            <conversion-tli id="T1084" />
            <conversion-tli id="T1085" />
            <conversion-tli id="T1086" />
            <conversion-tli id="T1087" />
            <conversion-tli id="T1088" />
            <conversion-tli id="T1089" />
            <conversion-tli id="T1090" />
            <conversion-tli id="T1091" />
            <conversion-tli id="T1092" />
            <conversion-tli id="T1093" />
            <conversion-tli id="T1094" />
            <conversion-tli id="T1095" />
            <conversion-tli id="T1096" />
            <conversion-tli id="T1097" />
            <conversion-tli id="T1098" />
            <conversion-tli id="T1099" />
            <conversion-tli id="T1100" />
            <conversion-tli id="T1101" />
            <conversion-tli id="T1102" />
            <conversion-tli id="T1103" />
            <conversion-tli id="T1104" />
            <conversion-tli id="T1105" />
            <conversion-tli id="T1106" />
            <conversion-tli id="T1107" />
            <conversion-tli id="T1108" />
            <conversion-tli id="T1109" />
            <conversion-tli id="T1110" />
            <conversion-tli id="T1111" />
            <conversion-tli id="T1112" />
            <conversion-tli id="T1113" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref-AkEE"
                          name="ref"
                          segmented-tier-id="tx-AkEE"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st-AkEE"
                          name="st"
                          segmented-tier-id="tx-AkEE"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-AkEE"
                          name="ts"
                          segmented-tier-id="tx-AkEE"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-AkEE"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-AkEE"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-AkEE"
                          name="mb"
                          segmented-tier-id="tx-AkEE"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-AkEE"
                          name="mp"
                          segmented-tier-id="tx-AkEE"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-AkEE"
                          name="ge"
                          segmented-tier-id="tx-AkEE"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg-AkEE"
                          name="gg"
                          segmented-tier-id="tx-AkEE"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-AkEE"
                          name="gr"
                          segmented-tier-id="tx-AkEE"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-AkEE"
                          name="mc"
                          segmented-tier-id="tx-AkEE"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-AkEE"
                          name="ps"
                          segmented-tier-id="tx-AkEE"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-AkEE"
                          name="SeR"
                          segmented-tier-id="tx-AkEE"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-AkEE"
                          name="SyF"
                          segmented-tier-id="tx-AkEE"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-AkEE"
                          name="IST"
                          segmented-tier-id="tx-AkEE"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top-AkEE"
                          name="Top"
                          segmented-tier-id="tx-AkEE"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc-AkEE"
                          name="Foc"
                          segmented-tier-id="tx-AkEE"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-AkEE"
                          name="BOR"
                          segmented-tier-id="tx-AkEE"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-AkEE"
                          name="BOR-Phon"
                          segmented-tier-id="tx-AkEE"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-AkEE"
                          name="BOR-Morph"
                          segmented-tier-id="tx-AkEE"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-AkEE"
                          name="CS"
                          segmented-tier-id="tx-AkEE"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-AkEE"
                          name="fe"
                          segmented-tier-id="tx-AkEE"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-AkEE"
                          name="fg"
                          segmented-tier-id="tx-AkEE"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-AkEE"
                          name="fr"
                          segmented-tier-id="tx-AkEE"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr-AkEE"
                          name="ltr"
                          segmented-tier-id="tx-AkEE"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-AkEE"
                          name="nt"
                          segmented-tier-id="tx-AkEE"
                          type="a" />
         <conversion-tier category="ref"
                          display-name="ref-UkET"
                          name="ref"
                          segmented-tier-id="tx-UkET"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st-UkET"
                          name="st"
                          segmented-tier-id="tx-UkET"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-UkET"
                          name="ts"
                          segmented-tier-id="tx-UkET"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-UkET"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-UkET"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-UkET"
                          name="mb"
                          segmented-tier-id="tx-UkET"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-UkET"
                          name="mp"
                          segmented-tier-id="tx-UkET"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-UkET"
                          name="ge"
                          segmented-tier-id="tx-UkET"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg-UkET"
                          name="gg"
                          segmented-tier-id="tx-UkET"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-UkET"
                          name="gr"
                          segmented-tier-id="tx-UkET"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-UkET"
                          name="mc"
                          segmented-tier-id="tx-UkET"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-UkET"
                          name="ps"
                          segmented-tier-id="tx-UkET"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-UkET"
                          name="SeR"
                          segmented-tier-id="tx-UkET"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-UkET"
                          name="SyF"
                          segmented-tier-id="tx-UkET"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-UkET"
                          name="IST"
                          segmented-tier-id="tx-UkET"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top-UkET"
                          name="Top"
                          segmented-tier-id="tx-UkET"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc-UkET"
                          name="Foc"
                          segmented-tier-id="tx-UkET"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-UkET"
                          name="BOR"
                          segmented-tier-id="tx-UkET"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-UkET"
                          name="BOR-Phon"
                          segmented-tier-id="tx-UkET"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-UkET"
                          name="BOR-Morph"
                          segmented-tier-id="tx-UkET"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-UkET"
                          name="CS"
                          segmented-tier-id="tx-UkET"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-UkET"
                          name="fe"
                          segmented-tier-id="tx-UkET"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-UkET"
                          name="fg"
                          segmented-tier-id="tx-UkET"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-UkET"
                          name="fr"
                          segmented-tier-id="tx-UkET"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr-UkET"
                          name="ltr"
                          segmented-tier-id="tx-UkET"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-UkET"
                          name="nt"
                          segmented-tier-id="tx-UkET"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
