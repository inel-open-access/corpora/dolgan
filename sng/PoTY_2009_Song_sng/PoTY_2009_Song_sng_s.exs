<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID2F52A6F0-113D-47D7-9D49-6258318EC418">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>PoTY_2009_Song_sng</transcription-name>
         <referenced-file url="PoTY_2009_Song_sng.wav" />
         <referenced-file url="PoTY_2009_Song_sng.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\sng\PoTY_2009_Song_sng\PoTY_2009_Song_sng.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">73</ud-information>
            <ud-information attribute-name="# HIAT:w">57</ud-information>
            <ud-information attribute-name="# e">56</ud-information>
            <ud-information attribute-name="# HIAT:u">16</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PoTY">
            <abbreviation>PoTY</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.0" type="appl" />
         <tli id="T1" time="5.286" type="appl" />
         <tli id="T2" time="10.572" type="appl" />
         <tli id="T3" time="15.858" type="appl" />
         <tli id="T4" time="17.847" type="appl" />
         <tli id="T5" time="19.835" type="appl" />
         <tli id="T6" time="21.824" type="appl" />
         <tli id="T7" time="23.898" type="appl" />
         <tli id="T8" time="25.973" type="appl" />
         <tli id="T9" time="28.047" type="appl" />
         <tli id="T10" time="29.885" type="appl" />
         <tli id="T11" time="31.722" type="appl" />
         <tli id="T12" time="33.56" type="appl" />
         <tli id="T13" time="35.457" type="appl" />
         <tli id="T14" time="37.353" type="appl" />
         <tli id="T15" time="39.25" type="appl" />
         <tli id="T16" time="40.592" type="appl" />
         <tli id="T17" time="41.935" type="appl" />
         <tli id="T18" time="43.277" type="appl" />
         <tli id="T19" time="44.62" type="appl" />
         <tli id="T20" time="46.052" type="appl" />
         <tli id="T21" time="47.485" type="appl" />
         <tli id="T22" time="48.918" type="appl" />
         <tli id="T23" time="50.35" type="appl" />
         <tli id="T24" time="51.93" type="appl" />
         <tli id="T25" time="53.51" type="appl" />
         <tli id="T26" time="55.09" type="appl" />
         <tli id="T27" time="56.67" type="appl" />
         <tli id="T28" time="58.172" type="appl" />
         <tli id="T29" time="59.675" type="appl" />
         <tli id="T30" time="61.178" type="appl" />
         <tli id="T31" time="62.68" type="appl" />
         <tli id="T32" time="64.132" type="appl" />
         <tli id="T33" time="65.584" type="appl" />
         <tli id="T34" time="67.035" type="appl" />
         <tli id="T35" time="68.487" type="appl" />
         <tli id="T36" time="70.568" type="appl" />
         <tli id="T37" time="72.649" type="appl" />
         <tli id="T38" time="74.73" type="appl" />
         <tli id="T39" time="76.603" type="appl" />
         <tli id="T40" time="78.477" type="appl" />
         <tli id="T41" time="80.35" type="appl" />
         <tli id="T42" time="82.357" type="appl" />
         <tli id="T43" time="84.363" type="appl" />
         <tli id="T44" time="86.37" type="appl" />
         <tli id="T45" time="87.782" type="appl" />
         <tli id="T46" time="89.195" type="appl" />
         <tli id="T47" time="90.608" type="appl" />
         <tli id="T48" time="92.02" type="appl" />
         <tli id="T49" time="93.46" type="appl" />
         <tli id="T50" time="94.9" type="appl" />
         <tli id="T51" time="96.34" type="appl" />
         <tli id="T52" time="97.78" type="appl" />
         <tli id="T53" time="99.501" type="appl" />
         <tli id="T54" time="101.222" type="appl" />
         <tli id="T55" time="102.943" type="appl" />
         <tli id="T56" time="104.664" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PoTY"
                      type="t">
         <timeline-fork end="T39" start="T38">
            <tli id="T38.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T56" id="Seg_0" n="sc" s="T0">
               <ts e="T3" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Töröːbüt</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">hirim</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">küne</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T6" id="Seg_14" n="HIAT:u" s="T3">
                  <ts e="T4" id="Seg_16" n="HIAT:w" s="T3">Hürekpin</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">ititer</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">dʼibarga</ts>
                  <nts id="Seg_23" n="HIAT:ip">.</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_26" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_28" n="HIAT:w" s="T6">Töröːbüt</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_31" n="HIAT:w" s="T7">hirim</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">huluha</ts>
                  <nts id="Seg_35" n="HIAT:ip">.</nts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_38" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_40" n="HIAT:w" s="T9">Oroppun</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_43" n="HIAT:w" s="T10">hɨrdatar</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_46" n="HIAT:w" s="T11">tumaŋŋa</ts>
                  <nts id="Seg_47" n="HIAT:ip">.</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_50" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_52" n="HIAT:w" s="T12">Tulaːjak</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_55" n="HIAT:w" s="T13">kerike</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_58" n="HIAT:w" s="T14">čokuːra</ts>
                  <nts id="Seg_59" n="HIAT:ip">.</nts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T19" id="Seg_62" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_64" n="HIAT:w" s="T15">Geri͡es</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_67" n="HIAT:w" s="T16">taːs</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_70" n="HIAT:w" s="T17">kohoːnum</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_73" n="HIAT:w" s="T18">mini͡ene</ts>
                  <nts id="Seg_74" n="HIAT:ip">.</nts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_77" n="HIAT:u" s="T19">
                  <ts e="T20" id="Seg_79" n="HIAT:w" s="T19">Tɨ͡a</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_82" n="HIAT:w" s="T20">hirbin</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_85" n="HIAT:w" s="T21">mu͡orabɨn</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_88" n="HIAT:w" s="T22">öjdötör</ts>
                  <nts id="Seg_89" n="HIAT:ip">.</nts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_92" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_94" n="HIAT:w" s="T23">Mini͡eke</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_97" n="HIAT:w" s="T24">ojuːlaːk</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_100" n="HIAT:w" s="T25">čokuːr</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_103" n="HIAT:w" s="T26">taːs</ts>
                  <nts id="Seg_104" n="HIAT:ip">.</nts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T31" id="Seg_107" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_109" n="HIAT:w" s="T27">Ü͡öhe</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_112" n="HIAT:w" s="T28">tɨ͡al</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_115" n="HIAT:w" s="T29">hürekpin</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_118" n="HIAT:w" s="T30">hojutar</ts>
                  <nts id="Seg_119" n="HIAT:ip">.</nts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T35" id="Seg_122" n="HIAT:u" s="T31">
                  <ts e="T32" id="Seg_124" n="HIAT:w" s="T31">Hɨtɨgan</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_127" n="HIAT:w" s="T32">u͡ot</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_130" n="HIAT:w" s="T33">hɨtɨn</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_133" n="HIAT:w" s="T34">egeler</ts>
                  <nts id="Seg_134" n="HIAT:ip">.</nts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T38" id="Seg_137" n="HIAT:u" s="T35">
                  <ts e="T36" id="Seg_139" n="HIAT:w" s="T35">Čikullik</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_142" n="HIAT:w" s="T36">mu͡oraga</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_145" n="HIAT:w" s="T37">čorgujar</ts>
                  <nts id="Seg_146" n="HIAT:ip">.</nts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T41" id="Seg_149" n="HIAT:u" s="T38">
                  <ts e="T38.tx.1" id="Seg_151" n="HIAT:w" s="T38">Aːn</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_154" n="HIAT:w" s="T38.tx.1">dojdu</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_157" n="HIAT:w" s="T39">ɨrɨ͡atɨn</ts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_160" n="HIAT:w" s="T40">ütükter</ts>
                  <nts id="Seg_161" n="HIAT:ip">.</nts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T44" id="Seg_164" n="HIAT:u" s="T41">
                  <ts e="T42" id="Seg_166" n="HIAT:w" s="T41">Tulaːjak</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_169" n="HIAT:w" s="T42">kerike</ts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_172" n="HIAT:w" s="T43">čokuːra</ts>
                  <nts id="Seg_173" n="HIAT:ip">.</nts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T48" id="Seg_176" n="HIAT:u" s="T44">
                  <ts e="T45" id="Seg_178" n="HIAT:w" s="T44">Geri͡es</ts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_181" n="HIAT:w" s="T45">taːs</ts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_184" n="HIAT:w" s="T46">kohoːnum</ts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_187" n="HIAT:w" s="T47">mini͡ene</ts>
                  <nts id="Seg_188" n="HIAT:ip">.</nts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T52" id="Seg_191" n="HIAT:u" s="T48">
                  <ts e="T49" id="Seg_193" n="HIAT:w" s="T48">Tɨ͡a</ts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_196" n="HIAT:w" s="T49">hirbin</ts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_199" n="HIAT:w" s="T50">mu͡orabɨn</ts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_202" n="HIAT:w" s="T51">öjdötör</ts>
                  <nts id="Seg_203" n="HIAT:ip">.</nts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T56" id="Seg_206" n="HIAT:u" s="T52">
                  <ts e="T53" id="Seg_208" n="HIAT:w" s="T52">Mini͡eke</ts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_211" n="HIAT:w" s="T53">ojuːlaːk</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_214" n="HIAT:w" s="T54">čokuːr</ts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_217" n="HIAT:w" s="T55">taːs</ts>
                  <nts id="Seg_218" n="HIAT:ip">.</nts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T56" id="Seg_220" n="sc" s="T0">
               <ts e="T1" id="Seg_222" n="e" s="T0">Töröːbüt </ts>
               <ts e="T2" id="Seg_224" n="e" s="T1">hirim </ts>
               <ts e="T3" id="Seg_226" n="e" s="T2">küne. </ts>
               <ts e="T4" id="Seg_228" n="e" s="T3">Hürekpin </ts>
               <ts e="T5" id="Seg_230" n="e" s="T4">ititer </ts>
               <ts e="T6" id="Seg_232" n="e" s="T5">dʼibarga. </ts>
               <ts e="T7" id="Seg_234" n="e" s="T6">Töröːbüt </ts>
               <ts e="T8" id="Seg_236" n="e" s="T7">hirim </ts>
               <ts e="T9" id="Seg_238" n="e" s="T8">huluha. </ts>
               <ts e="T10" id="Seg_240" n="e" s="T9">Oroppun </ts>
               <ts e="T11" id="Seg_242" n="e" s="T10">hɨrdatar </ts>
               <ts e="T12" id="Seg_244" n="e" s="T11">tumaŋŋa. </ts>
               <ts e="T13" id="Seg_246" n="e" s="T12">Tulaːjak </ts>
               <ts e="T14" id="Seg_248" n="e" s="T13">kerike </ts>
               <ts e="T15" id="Seg_250" n="e" s="T14">čokuːra. </ts>
               <ts e="T16" id="Seg_252" n="e" s="T15">Geri͡es </ts>
               <ts e="T17" id="Seg_254" n="e" s="T16">taːs </ts>
               <ts e="T18" id="Seg_256" n="e" s="T17">kohoːnum </ts>
               <ts e="T19" id="Seg_258" n="e" s="T18">mini͡ene. </ts>
               <ts e="T20" id="Seg_260" n="e" s="T19">Tɨ͡a </ts>
               <ts e="T21" id="Seg_262" n="e" s="T20">hirbin </ts>
               <ts e="T22" id="Seg_264" n="e" s="T21">mu͡orabɨn </ts>
               <ts e="T23" id="Seg_266" n="e" s="T22">öjdötör. </ts>
               <ts e="T24" id="Seg_268" n="e" s="T23">Mini͡eke </ts>
               <ts e="T25" id="Seg_270" n="e" s="T24">ojuːlaːk </ts>
               <ts e="T26" id="Seg_272" n="e" s="T25">čokuːr </ts>
               <ts e="T27" id="Seg_274" n="e" s="T26">taːs. </ts>
               <ts e="T28" id="Seg_276" n="e" s="T27">Ü͡öhe </ts>
               <ts e="T29" id="Seg_278" n="e" s="T28">tɨ͡al </ts>
               <ts e="T30" id="Seg_280" n="e" s="T29">hürekpin </ts>
               <ts e="T31" id="Seg_282" n="e" s="T30">hojutar. </ts>
               <ts e="T32" id="Seg_284" n="e" s="T31">Hɨtɨgan </ts>
               <ts e="T33" id="Seg_286" n="e" s="T32">u͡ot </ts>
               <ts e="T34" id="Seg_288" n="e" s="T33">hɨtɨn </ts>
               <ts e="T35" id="Seg_290" n="e" s="T34">egeler. </ts>
               <ts e="T36" id="Seg_292" n="e" s="T35">Čikullik </ts>
               <ts e="T37" id="Seg_294" n="e" s="T36">mu͡oraga </ts>
               <ts e="T38" id="Seg_296" n="e" s="T37">čorgujar. </ts>
               <ts e="T39" id="Seg_298" n="e" s="T38">Aːn dojdu </ts>
               <ts e="T40" id="Seg_300" n="e" s="T39">ɨrɨ͡atɨn </ts>
               <ts e="T41" id="Seg_302" n="e" s="T40">ütükter. </ts>
               <ts e="T42" id="Seg_304" n="e" s="T41">Tulaːjak </ts>
               <ts e="T43" id="Seg_306" n="e" s="T42">kerike </ts>
               <ts e="T44" id="Seg_308" n="e" s="T43">čokuːra. </ts>
               <ts e="T45" id="Seg_310" n="e" s="T44">Geri͡es </ts>
               <ts e="T46" id="Seg_312" n="e" s="T45">taːs </ts>
               <ts e="T47" id="Seg_314" n="e" s="T46">kohoːnum </ts>
               <ts e="T48" id="Seg_316" n="e" s="T47">mini͡ene. </ts>
               <ts e="T49" id="Seg_318" n="e" s="T48">Tɨ͡a </ts>
               <ts e="T50" id="Seg_320" n="e" s="T49">hirbin </ts>
               <ts e="T51" id="Seg_322" n="e" s="T50">mu͡orabɨn </ts>
               <ts e="T52" id="Seg_324" n="e" s="T51">öjdötör. </ts>
               <ts e="T53" id="Seg_326" n="e" s="T52">Mini͡eke </ts>
               <ts e="T54" id="Seg_328" n="e" s="T53">ojuːlaːk </ts>
               <ts e="T55" id="Seg_330" n="e" s="T54">čokuːr </ts>
               <ts e="T56" id="Seg_332" n="e" s="T55">taːs. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T3" id="Seg_333" s="T0">PoTY_2009_Song_sng.001 (001)</ta>
            <ta e="T6" id="Seg_334" s="T3">PoTY_2009_Song_sng.002 (002)</ta>
            <ta e="T9" id="Seg_335" s="T6">PoTY_2009_Song_sng.003 (003)</ta>
            <ta e="T12" id="Seg_336" s="T9">PoTY_2009_Song_sng.004 (004)</ta>
            <ta e="T15" id="Seg_337" s="T12">PoTY_2009_Song_sng.005 (005)</ta>
            <ta e="T19" id="Seg_338" s="T15">PoTY_2009_Song_sng.006 (006)</ta>
            <ta e="T23" id="Seg_339" s="T19">PoTY_2009_Song_sng.007 (007)</ta>
            <ta e="T27" id="Seg_340" s="T23">PoTY_2009_Song_sng.008 (008)</ta>
            <ta e="T31" id="Seg_341" s="T27">PoTY_2009_Song_sng.009 (009)</ta>
            <ta e="T35" id="Seg_342" s="T31">PoTY_2009_Song_sng.010 (010)</ta>
            <ta e="T38" id="Seg_343" s="T35">PoTY_2009_Song_sng.011 (011)</ta>
            <ta e="T41" id="Seg_344" s="T38">PoTY_2009_Song_sng.012 (012)</ta>
            <ta e="T44" id="Seg_345" s="T41">PoTY_2009_Song_sng.013 (013)</ta>
            <ta e="T48" id="Seg_346" s="T44">PoTY_2009_Song_sng.014 (014)</ta>
            <ta e="T52" id="Seg_347" s="T48">PoTY_2009_Song_sng.015 (015)</ta>
            <ta e="T56" id="Seg_348" s="T52">PoTY_2009_Song_sng.016 (016)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T3" id="Seg_349" s="T0">Төрөөбүт һирим күнэ.</ta>
            <ta e="T6" id="Seg_350" s="T3">Һүрэкпин (һүрэппин) ититэр дьибарга.</ta>
            <ta e="T9" id="Seg_351" s="T6">Төрөөбүт һирим (каллааным) һулуһа.</ta>
            <ta e="T12" id="Seg_352" s="T9">Орокпун (ороппун) һырдатар тумаӈӈа.</ta>
            <ta e="T15" id="Seg_353" s="T12">Тулайак кэрикэ чокуура.</ta>
            <ta e="T19" id="Seg_354" s="T15">Гэриэс-таас коһоонум миниэнэ.</ta>
            <ta e="T23" id="Seg_355" s="T19">Тыа һирбин муорабын өйдөтөр.</ta>
            <ta e="T27" id="Seg_356" s="T23">Миниэкэ ойуулак чокуур-таас.</ta>
            <ta e="T31" id="Seg_357" s="T27">Үүһэ тыал һүрэкпин (һүрэппин) һойутан.</ta>
            <ta e="T35" id="Seg_358" s="T31">Һытыган от һытын эгэлэр.</ta>
            <ta e="T38" id="Seg_359" s="T35">Чуукиллик муорага чоргуйан.</ta>
            <ta e="T41" id="Seg_360" s="T38">Аан дойду ырыатын үтүктэр.</ta>
            <ta e="T44" id="Seg_361" s="T41">Тулайак кэрикэ чокуура.</ta>
            <ta e="T48" id="Seg_362" s="T44">Гэриэс-таас коһоонум миниэнэ.</ta>
            <ta e="T52" id="Seg_363" s="T48">Тыа һирбин муорабын өйдөтөр.</ta>
            <ta e="T56" id="Seg_364" s="T52">Миниэкэ ойуулаак чокуур-таас.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T3" id="Seg_365" s="T0">Töröːbüt hirim küne. </ta>
            <ta e="T6" id="Seg_366" s="T3">Hürekpin ititer dʼibarga. </ta>
            <ta e="T9" id="Seg_367" s="T6">Töröːbüt hirim huluha. </ta>
            <ta e="T12" id="Seg_368" s="T9">Oroppun hɨrdatar tumaŋŋa. </ta>
            <ta e="T15" id="Seg_369" s="T12">Tulaːjak kerike čokuːra. </ta>
            <ta e="T19" id="Seg_370" s="T15">Geri͡es taːs kohoːnum mini͡ene. </ta>
            <ta e="T23" id="Seg_371" s="T19">Tɨ͡a hirbin mu͡orabɨn öjdötör. </ta>
            <ta e="T27" id="Seg_372" s="T23">Mini͡eke ojuːlaːk čokuːr taːs. </ta>
            <ta e="T31" id="Seg_373" s="T27">Ü͡öhe tɨ͡al hürekpin hojutar. </ta>
            <ta e="T35" id="Seg_374" s="T31">Hɨtɨgan u͡ot hɨtɨn egeler. </ta>
            <ta e="T38" id="Seg_375" s="T35">Čikullik mu͡oraga čorgujar. </ta>
            <ta e="T41" id="Seg_376" s="T38">Aːn dojdu ɨrɨ͡atɨn ütükter. </ta>
            <ta e="T44" id="Seg_377" s="T41">Tulaːjak kerike čokuːra. </ta>
            <ta e="T48" id="Seg_378" s="T44">Geri͡es taːs kohoːnum mini͡ene. </ta>
            <ta e="T52" id="Seg_379" s="T48">Tɨ͡a hirbin mu͡orabɨn öjdötör. </ta>
            <ta e="T56" id="Seg_380" s="T52">Mini͡eke ojuːlaːk čokuːr taːs. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_381" s="T0">töröː-büt</ta>
            <ta e="T2" id="Seg_382" s="T1">hir-i-m</ta>
            <ta e="T3" id="Seg_383" s="T2">kün-e</ta>
            <ta e="T4" id="Seg_384" s="T3">hürek-pi-n</ta>
            <ta e="T5" id="Seg_385" s="T4">iti-t-er</ta>
            <ta e="T6" id="Seg_386" s="T5">dʼibar-ga</ta>
            <ta e="T7" id="Seg_387" s="T6">töröː-büt</ta>
            <ta e="T8" id="Seg_388" s="T7">hir-i-m</ta>
            <ta e="T9" id="Seg_389" s="T8">huluh-a</ta>
            <ta e="T10" id="Seg_390" s="T9">orop-pu-n</ta>
            <ta e="T11" id="Seg_391" s="T10">hɨrd-a-t-ar</ta>
            <ta e="T12" id="Seg_392" s="T11">tumaŋ-ŋa</ta>
            <ta e="T13" id="Seg_393" s="T12">tulaːjak</ta>
            <ta e="T14" id="Seg_394" s="T13">kerike</ta>
            <ta e="T15" id="Seg_395" s="T14">čokuːr-a</ta>
            <ta e="T16" id="Seg_396" s="T15">geri͡es</ta>
            <ta e="T17" id="Seg_397" s="T16">taːs</ta>
            <ta e="T18" id="Seg_398" s="T17">kohoːn-u-m</ta>
            <ta e="T19" id="Seg_399" s="T18">mini͡ene</ta>
            <ta e="T20" id="Seg_400" s="T19">tɨ͡a</ta>
            <ta e="T21" id="Seg_401" s="T20">hir-bi-n</ta>
            <ta e="T22" id="Seg_402" s="T21">mu͡ora-bɨ-n</ta>
            <ta e="T23" id="Seg_403" s="T22">öj-d-ö-t-ör</ta>
            <ta e="T24" id="Seg_404" s="T23">mini͡e-ke</ta>
            <ta e="T25" id="Seg_405" s="T24">ojuː-laːk</ta>
            <ta e="T26" id="Seg_406" s="T25">čokuːr</ta>
            <ta e="T27" id="Seg_407" s="T26">taːs</ta>
            <ta e="T28" id="Seg_408" s="T27">ü͡öhe</ta>
            <ta e="T29" id="Seg_409" s="T28">tɨ͡al</ta>
            <ta e="T30" id="Seg_410" s="T29">hürek-pi-n</ta>
            <ta e="T31" id="Seg_411" s="T30">hoj-u-t-ar</ta>
            <ta e="T32" id="Seg_412" s="T31">hɨtɨgan</ta>
            <ta e="T33" id="Seg_413" s="T32">u͡ot</ta>
            <ta e="T34" id="Seg_414" s="T33">hɨt-ɨ-n</ta>
            <ta e="T35" id="Seg_415" s="T34">egel-er</ta>
            <ta e="T36" id="Seg_416" s="T35">čikullik</ta>
            <ta e="T37" id="Seg_417" s="T36">mu͡ora-ga</ta>
            <ta e="T38" id="Seg_418" s="T37">čorguj-ar</ta>
            <ta e="T39" id="Seg_419" s="T38">aːn dojdu</ta>
            <ta e="T40" id="Seg_420" s="T39">ɨrɨ͡a-tɨ-n</ta>
            <ta e="T41" id="Seg_421" s="T40">ütükt-er</ta>
            <ta e="T42" id="Seg_422" s="T41">tulaːjak</ta>
            <ta e="T43" id="Seg_423" s="T42">kerike</ta>
            <ta e="T44" id="Seg_424" s="T43">čokuːr-a</ta>
            <ta e="T45" id="Seg_425" s="T44">geri͡es</ta>
            <ta e="T46" id="Seg_426" s="T45">taːs</ta>
            <ta e="T47" id="Seg_427" s="T46">kohoːn-u-m</ta>
            <ta e="T48" id="Seg_428" s="T47">mini͡ene</ta>
            <ta e="T49" id="Seg_429" s="T48">tɨ͡a</ta>
            <ta e="T50" id="Seg_430" s="T49">hir-bi-n</ta>
            <ta e="T51" id="Seg_431" s="T50">mu͡ora-bɨ-n</ta>
            <ta e="T52" id="Seg_432" s="T51">öj-d-ö-t-ör</ta>
            <ta e="T53" id="Seg_433" s="T52">mini͡e-ke</ta>
            <ta e="T54" id="Seg_434" s="T53">ojuː-laːk</ta>
            <ta e="T55" id="Seg_435" s="T54">čokuːr</ta>
            <ta e="T56" id="Seg_436" s="T55">taːs</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_437" s="T0">töröː-BIT</ta>
            <ta e="T2" id="Seg_438" s="T1">hir-I-m</ta>
            <ta e="T3" id="Seg_439" s="T2">kün-tA</ta>
            <ta e="T4" id="Seg_440" s="T3">hürek-BI-n</ta>
            <ta e="T5" id="Seg_441" s="T4">itij-t-Ar</ta>
            <ta e="T6" id="Seg_442" s="T5">dʼibar-GA</ta>
            <ta e="T7" id="Seg_443" s="T6">töröː-BIT</ta>
            <ta e="T8" id="Seg_444" s="T7">hir-I-m</ta>
            <ta e="T9" id="Seg_445" s="T8">hulus-tA</ta>
            <ta e="T10" id="Seg_446" s="T9">orok-BI-n</ta>
            <ta e="T11" id="Seg_447" s="T10">hɨrdaː-A-t-Ar</ta>
            <ta e="T12" id="Seg_448" s="T11">tuman-GA</ta>
            <ta e="T13" id="Seg_449" s="T12">tulaːjak</ta>
            <ta e="T14" id="Seg_450" s="T13">keriske</ta>
            <ta e="T15" id="Seg_451" s="T14">čokuːr-tA</ta>
            <ta e="T16" id="Seg_452" s="T15">geri͡es</ta>
            <ta e="T17" id="Seg_453" s="T16">taːs</ta>
            <ta e="T18" id="Seg_454" s="T17">kohoːn-I-m</ta>
            <ta e="T19" id="Seg_455" s="T18">mini͡ene</ta>
            <ta e="T20" id="Seg_456" s="T19">tɨ͡a</ta>
            <ta e="T21" id="Seg_457" s="T20">hir-BI-n</ta>
            <ta e="T22" id="Seg_458" s="T21">mu͡ora-BI-n</ta>
            <ta e="T23" id="Seg_459" s="T22">öj-LAː-A-t-Ar</ta>
            <ta e="T24" id="Seg_460" s="T23">min-GA</ta>
            <ta e="T25" id="Seg_461" s="T24">ojuː-LAːK</ta>
            <ta e="T26" id="Seg_462" s="T25">čokuːr</ta>
            <ta e="T27" id="Seg_463" s="T26">taːs</ta>
            <ta e="T28" id="Seg_464" s="T27">ü͡öhe</ta>
            <ta e="T29" id="Seg_465" s="T28">tɨ͡al</ta>
            <ta e="T30" id="Seg_466" s="T29">hürek-BI-n</ta>
            <ta e="T31" id="Seg_467" s="T30">hoj-I-t-Ar</ta>
            <ta e="T32" id="Seg_468" s="T31">hɨtɨgan</ta>
            <ta e="T33" id="Seg_469" s="T32">u͡ot</ta>
            <ta e="T34" id="Seg_470" s="T33">hɨt-tI-n</ta>
            <ta e="T35" id="Seg_471" s="T34">egel-Ar</ta>
            <ta e="T36" id="Seg_472" s="T35">čikullik</ta>
            <ta e="T37" id="Seg_473" s="T36">mu͡ora-GA</ta>
            <ta e="T38" id="Seg_474" s="T37">čorguj-Ar</ta>
            <ta e="T39" id="Seg_475" s="T38">aːn dojdu</ta>
            <ta e="T40" id="Seg_476" s="T39">ɨrɨ͡a-tI-n</ta>
            <ta e="T41" id="Seg_477" s="T40">ütügün-Ar</ta>
            <ta e="T42" id="Seg_478" s="T41">tulaːjak</ta>
            <ta e="T43" id="Seg_479" s="T42">keriske</ta>
            <ta e="T44" id="Seg_480" s="T43">čokuːr-tA</ta>
            <ta e="T45" id="Seg_481" s="T44">geri͡es</ta>
            <ta e="T46" id="Seg_482" s="T45">taːs</ta>
            <ta e="T47" id="Seg_483" s="T46">kohoːn-I-m</ta>
            <ta e="T48" id="Seg_484" s="T47">mini͡ene</ta>
            <ta e="T49" id="Seg_485" s="T48">tɨ͡a</ta>
            <ta e="T50" id="Seg_486" s="T49">hir-BI-n</ta>
            <ta e="T51" id="Seg_487" s="T50">mu͡ora-BI-n</ta>
            <ta e="T52" id="Seg_488" s="T51">öj-LAː-A-t-Ar</ta>
            <ta e="T53" id="Seg_489" s="T52">min-GA</ta>
            <ta e="T54" id="Seg_490" s="T53">ojuː-LAːK</ta>
            <ta e="T55" id="Seg_491" s="T54">čokuːr</ta>
            <ta e="T56" id="Seg_492" s="T55">taːs</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_493" s="T0">be.born-PTCP.PST</ta>
            <ta e="T2" id="Seg_494" s="T1">earth-EP-1SG.[NOM]</ta>
            <ta e="T3" id="Seg_495" s="T2">sun-3SG.[NOM]</ta>
            <ta e="T4" id="Seg_496" s="T3">heart-1SG-ACC</ta>
            <ta e="T5" id="Seg_497" s="T4">become.warm-CAUS-PRS.[3SG]</ta>
            <ta e="T6" id="Seg_498" s="T5">morning.frost-DAT/LOC</ta>
            <ta e="T7" id="Seg_499" s="T6">be.born-PTCP.PST</ta>
            <ta e="T8" id="Seg_500" s="T7">earth-EP-1SG.[NOM]</ta>
            <ta e="T9" id="Seg_501" s="T8">star-3SG.[NOM]</ta>
            <ta e="T10" id="Seg_502" s="T9">path-1SG-ACC</ta>
            <ta e="T11" id="Seg_503" s="T10">shine-EP-CAUS-PRS.[3SG]</ta>
            <ta e="T12" id="Seg_504" s="T11">fog-DAT/LOC</ta>
            <ta e="T13" id="Seg_505" s="T12">orphan.[NOM]</ta>
            <ta e="T14" id="Seg_506" s="T13">hill.[NOM]</ta>
            <ta e="T15" id="Seg_507" s="T14">carnelian-3SG.[NOM]</ta>
            <ta e="T16" id="Seg_508" s="T15">memory.[NOM]</ta>
            <ta e="T17" id="Seg_509" s="T16">stone.[NOM]</ta>
            <ta e="T18" id="Seg_510" s="T17">poem-EP-1SG.[NOM]</ta>
            <ta e="T19" id="Seg_511" s="T18">my</ta>
            <ta e="T20" id="Seg_512" s="T19">tundra.[NOM]</ta>
            <ta e="T21" id="Seg_513" s="T20">earth-1SG-ACC</ta>
            <ta e="T22" id="Seg_514" s="T21">tundra-1SG-ACC</ta>
            <ta e="T23" id="Seg_515" s="T22">mind-VBZ-EP-CAUS-PRS.[3SG]</ta>
            <ta e="T24" id="Seg_516" s="T23">1SG-DAT/LOC</ta>
            <ta e="T25" id="Seg_517" s="T24">ornament-PROPR</ta>
            <ta e="T26" id="Seg_518" s="T25">carnelian.[NOM]</ta>
            <ta e="T27" id="Seg_519" s="T26">stone.[NOM]</ta>
            <ta e="T28" id="Seg_520" s="T27">upper</ta>
            <ta e="T29" id="Seg_521" s="T28">wind.[NOM]</ta>
            <ta e="T30" id="Seg_522" s="T29">heart-1SG-ACC</ta>
            <ta e="T31" id="Seg_523" s="T30">cool-EP-CAUS-PRS.[3SG]</ta>
            <ta e="T32" id="Seg_524" s="T31">smelly</ta>
            <ta e="T33" id="Seg_525" s="T32">fire.[NOM]</ta>
            <ta e="T34" id="Seg_526" s="T33">smell-3SG-ACC</ta>
            <ta e="T35" id="Seg_527" s="T34">bring-PRS.[3SG]</ta>
            <ta e="T36" id="Seg_528" s="T35">snipe.[NOM]</ta>
            <ta e="T37" id="Seg_529" s="T36">sea-DAT/LOC</ta>
            <ta e="T38" id="Seg_530" s="T37">scream-PRS.[3SG]</ta>
            <ta e="T39" id="Seg_531" s="T38">homeland.[NOM]</ta>
            <ta e="T40" id="Seg_532" s="T39">song-3SG-ACC</ta>
            <ta e="T41" id="Seg_533" s="T40">imitate-PRS.[3SG]</ta>
            <ta e="T42" id="Seg_534" s="T41">orphan.[NOM]</ta>
            <ta e="T43" id="Seg_535" s="T42">hill.[NOM]</ta>
            <ta e="T44" id="Seg_536" s="T43">carnelian-3SG.[NOM]</ta>
            <ta e="T45" id="Seg_537" s="T44">memory.[NOM]</ta>
            <ta e="T46" id="Seg_538" s="T45">stone.[NOM]</ta>
            <ta e="T47" id="Seg_539" s="T46">poem-EP-1SG.[NOM]</ta>
            <ta e="T48" id="Seg_540" s="T47">my</ta>
            <ta e="T49" id="Seg_541" s="T48">tundra.[NOM]</ta>
            <ta e="T50" id="Seg_542" s="T49">earth-1SG-ACC</ta>
            <ta e="T51" id="Seg_543" s="T50">tundra-1SG-ACC</ta>
            <ta e="T52" id="Seg_544" s="T51">mind-VBZ-EP-CAUS-PRS.[3SG]</ta>
            <ta e="T53" id="Seg_545" s="T52">1SG-DAT/LOC</ta>
            <ta e="T54" id="Seg_546" s="T53">ornament-PROPR</ta>
            <ta e="T55" id="Seg_547" s="T54">carnelian.[NOM]</ta>
            <ta e="T56" id="Seg_548" s="T55">stone.[NOM]</ta>
         </annotation>
         <annotation name="gg" tierref="gg">
            <ta e="T1" id="Seg_549" s="T0">geboren.werden-PTCP.PST</ta>
            <ta e="T2" id="Seg_550" s="T1">Erde-EP-1SG.[NOM]</ta>
            <ta e="T3" id="Seg_551" s="T2">Sonne-3SG.[NOM]</ta>
            <ta e="T4" id="Seg_552" s="T3">Herz-1SG-ACC</ta>
            <ta e="T5" id="Seg_553" s="T4">warm.werden-CAUS-PRS.[3SG]</ta>
            <ta e="T6" id="Seg_554" s="T5">Morgenfrost-DAT/LOC</ta>
            <ta e="T7" id="Seg_555" s="T6">geboren.werden-PTCP.PST</ta>
            <ta e="T8" id="Seg_556" s="T7">Erde-EP-1SG.[NOM]</ta>
            <ta e="T9" id="Seg_557" s="T8">Stern-3SG.[NOM]</ta>
            <ta e="T10" id="Seg_558" s="T9">Pfad-1SG-ACC</ta>
            <ta e="T11" id="Seg_559" s="T10">scheinen-EP-CAUS-PRS.[3SG]</ta>
            <ta e="T12" id="Seg_560" s="T11">Nebel-DAT/LOC</ta>
            <ta e="T13" id="Seg_561" s="T12">Waise.[NOM]</ta>
            <ta e="T14" id="Seg_562" s="T13">Hügel.[NOM]</ta>
            <ta e="T15" id="Seg_563" s="T14">Karneol-3SG.[NOM]</ta>
            <ta e="T16" id="Seg_564" s="T15">Gedenken.[NOM]</ta>
            <ta e="T17" id="Seg_565" s="T16">Stein.[NOM]</ta>
            <ta e="T18" id="Seg_566" s="T17">Gedicht-EP-1SG.[NOM]</ta>
            <ta e="T19" id="Seg_567" s="T18">mein</ta>
            <ta e="T20" id="Seg_568" s="T19">Tundra.[NOM]</ta>
            <ta e="T21" id="Seg_569" s="T20">Erde-1SG-ACC</ta>
            <ta e="T22" id="Seg_570" s="T21">Tundra-1SG-ACC</ta>
            <ta e="T23" id="Seg_571" s="T22">Verstand-VBZ-EP-CAUS-PRS.[3SG]</ta>
            <ta e="T24" id="Seg_572" s="T23">1SG-DAT/LOC</ta>
            <ta e="T25" id="Seg_573" s="T24">Ornament-PROPR</ta>
            <ta e="T26" id="Seg_574" s="T25">Karneol.[NOM]</ta>
            <ta e="T27" id="Seg_575" s="T26">Stein.[NOM]</ta>
            <ta e="T28" id="Seg_576" s="T27">oberer</ta>
            <ta e="T29" id="Seg_577" s="T28">Wind.[NOM]</ta>
            <ta e="T30" id="Seg_578" s="T29">Herz-1SG-ACC</ta>
            <ta e="T31" id="Seg_579" s="T30">abkühlen-EP-CAUS-PRS.[3SG]</ta>
            <ta e="T32" id="Seg_580" s="T31">stinkend</ta>
            <ta e="T33" id="Seg_581" s="T32">Feuer.[NOM]</ta>
            <ta e="T34" id="Seg_582" s="T33">Geruch-3SG-ACC</ta>
            <ta e="T35" id="Seg_583" s="T34">bringen-PRS.[3SG]</ta>
            <ta e="T36" id="Seg_584" s="T35">Schnepfe.[NOM]</ta>
            <ta e="T37" id="Seg_585" s="T36">Meer-DAT/LOC</ta>
            <ta e="T38" id="Seg_586" s="T37">schreien-PRS.[3SG]</ta>
            <ta e="T39" id="Seg_587" s="T38">Heimat.[NOM]</ta>
            <ta e="T40" id="Seg_588" s="T39">Lied-3SG-ACC</ta>
            <ta e="T41" id="Seg_589" s="T40">nachmachen-PRS.[3SG]</ta>
            <ta e="T42" id="Seg_590" s="T41">Waise.[NOM]</ta>
            <ta e="T43" id="Seg_591" s="T42">Hügel.[NOM]</ta>
            <ta e="T44" id="Seg_592" s="T43">Karneol-3SG.[NOM]</ta>
            <ta e="T45" id="Seg_593" s="T44">Gedenken.[NOM]</ta>
            <ta e="T46" id="Seg_594" s="T45">Stein.[NOM]</ta>
            <ta e="T47" id="Seg_595" s="T46">Gedicht-EP-1SG.[NOM]</ta>
            <ta e="T48" id="Seg_596" s="T47">mein</ta>
            <ta e="T49" id="Seg_597" s="T48">Tundra.[NOM]</ta>
            <ta e="T50" id="Seg_598" s="T49">Erde-1SG-ACC</ta>
            <ta e="T51" id="Seg_599" s="T50">Tundra-1SG-ACC</ta>
            <ta e="T52" id="Seg_600" s="T51">Verstand-VBZ-EP-CAUS-PRS.[3SG]</ta>
            <ta e="T53" id="Seg_601" s="T52">1SG-DAT/LOC</ta>
            <ta e="T54" id="Seg_602" s="T53">Ornament-PROPR</ta>
            <ta e="T55" id="Seg_603" s="T54">Karneol.[NOM]</ta>
            <ta e="T56" id="Seg_604" s="T55">Stein.[NOM]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_605" s="T0">родиться-PTCP.PST</ta>
            <ta e="T2" id="Seg_606" s="T1">земля-EP-1SG.[NOM]</ta>
            <ta e="T3" id="Seg_607" s="T2">солнце-3SG.[NOM]</ta>
            <ta e="T4" id="Seg_608" s="T3">сердце-1SG-ACC</ta>
            <ta e="T5" id="Seg_609" s="T4">согреться-CAUS-PRS.[3SG]</ta>
            <ta e="T6" id="Seg_610" s="T5">утренний.мороз-DAT/LOC</ta>
            <ta e="T7" id="Seg_611" s="T6">родиться-PTCP.PST</ta>
            <ta e="T8" id="Seg_612" s="T7">земля-EP-1SG.[NOM]</ta>
            <ta e="T9" id="Seg_613" s="T8">звезда-3SG.[NOM]</ta>
            <ta e="T10" id="Seg_614" s="T9">тропка-1SG-ACC</ta>
            <ta e="T11" id="Seg_615" s="T10">светить-EP-CAUS-PRS.[3SG]</ta>
            <ta e="T12" id="Seg_616" s="T11">туман-DAT/LOC</ta>
            <ta e="T13" id="Seg_617" s="T12">сирота.[NOM]</ta>
            <ta e="T14" id="Seg_618" s="T13">холм.[NOM]</ta>
            <ta e="T15" id="Seg_619" s="T14">сердолик-3SG.[NOM]</ta>
            <ta e="T16" id="Seg_620" s="T15">память</ta>
            <ta e="T17" id="Seg_621" s="T16">камень.[NOM]</ta>
            <ta e="T18" id="Seg_622" s="T17">стих-EP-1SG.[NOM]</ta>
            <ta e="T19" id="Seg_623" s="T18">мой</ta>
            <ta e="T20" id="Seg_624" s="T19">тундра.[NOM]</ta>
            <ta e="T21" id="Seg_625" s="T20">земля-1SG-ACC</ta>
            <ta e="T22" id="Seg_626" s="T21">тундра-1SG-ACC</ta>
            <ta e="T23" id="Seg_627" s="T22">ум-VBZ-EP-CAUS-PRS.[3SG]</ta>
            <ta e="T24" id="Seg_628" s="T23">1SG-DAT/LOC</ta>
            <ta e="T25" id="Seg_629" s="T24">орнамент-PROPR</ta>
            <ta e="T26" id="Seg_630" s="T25">сердолик.[NOM]</ta>
            <ta e="T27" id="Seg_631" s="T26">камень.[NOM]</ta>
            <ta e="T28" id="Seg_632" s="T27">верхний</ta>
            <ta e="T29" id="Seg_633" s="T28">ветер.[NOM]</ta>
            <ta e="T30" id="Seg_634" s="T29">сердце-1SG-ACC</ta>
            <ta e="T31" id="Seg_635" s="T30">охлаждать-EP-CAUS-PRS.[3SG]</ta>
            <ta e="T32" id="Seg_636" s="T31">вонющий</ta>
            <ta e="T33" id="Seg_637" s="T32">огонь.[NOM]</ta>
            <ta e="T34" id="Seg_638" s="T33">запах-3SG-ACC</ta>
            <ta e="T35" id="Seg_639" s="T34">принести-PRS.[3SG]</ta>
            <ta e="T36" id="Seg_640" s="T35">кулик.[NOM]</ta>
            <ta e="T37" id="Seg_641" s="T36">море-DAT/LOC</ta>
            <ta e="T38" id="Seg_642" s="T37">кричать-PRS.[3SG]</ta>
            <ta e="T39" id="Seg_643" s="T38">родина.[NOM]</ta>
            <ta e="T40" id="Seg_644" s="T39">песня-3SG-ACC</ta>
            <ta e="T41" id="Seg_645" s="T40">подражать-PRS.[3SG]</ta>
            <ta e="T42" id="Seg_646" s="T41">сирота.[NOM]</ta>
            <ta e="T43" id="Seg_647" s="T42">холм.[NOM]</ta>
            <ta e="T44" id="Seg_648" s="T43">сердолик-3SG.[NOM]</ta>
            <ta e="T45" id="Seg_649" s="T44">память.[NOM]</ta>
            <ta e="T46" id="Seg_650" s="T45">камень.[NOM]</ta>
            <ta e="T47" id="Seg_651" s="T46">стих-EP-1SG.[NOM]</ta>
            <ta e="T48" id="Seg_652" s="T47">мой</ta>
            <ta e="T49" id="Seg_653" s="T48">тундра.[NOM]</ta>
            <ta e="T50" id="Seg_654" s="T49">земля-1SG-ACC</ta>
            <ta e="T51" id="Seg_655" s="T50">тундра-1SG-ACC</ta>
            <ta e="T52" id="Seg_656" s="T51">ум-VBZ-EP-CAUS-PRS.[3SG]</ta>
            <ta e="T53" id="Seg_657" s="T52">1SG-DAT/LOC</ta>
            <ta e="T54" id="Seg_658" s="T53">орнамент-PROPR</ta>
            <ta e="T55" id="Seg_659" s="T54">сердолик.[NOM]</ta>
            <ta e="T56" id="Seg_660" s="T55">камень.[NOM]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_661" s="T0">v-v:ptcp</ta>
            <ta e="T2" id="Seg_662" s="T1">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T3" id="Seg_663" s="T2">n-n:(poss)-n:case</ta>
            <ta e="T4" id="Seg_664" s="T3">n-n:poss-n:case</ta>
            <ta e="T5" id="Seg_665" s="T4">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T6" id="Seg_666" s="T5">n-n:case</ta>
            <ta e="T7" id="Seg_667" s="T6">v-v:ptcp</ta>
            <ta e="T8" id="Seg_668" s="T7">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T9" id="Seg_669" s="T8">n-n:(poss)-n:case</ta>
            <ta e="T10" id="Seg_670" s="T9">n-n:poss-n:case</ta>
            <ta e="T11" id="Seg_671" s="T10">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T12" id="Seg_672" s="T11">n-n:case</ta>
            <ta e="T13" id="Seg_673" s="T12">n-n:case</ta>
            <ta e="T14" id="Seg_674" s="T13">n-n:case</ta>
            <ta e="T15" id="Seg_675" s="T14">n-n:(poss)-n:case</ta>
            <ta e="T16" id="Seg_676" s="T15">n-n:case</ta>
            <ta e="T17" id="Seg_677" s="T16">n-n:case</ta>
            <ta e="T18" id="Seg_678" s="T17">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T19" id="Seg_679" s="T18">posspr</ta>
            <ta e="T20" id="Seg_680" s="T19">n-n:case</ta>
            <ta e="T21" id="Seg_681" s="T20">n-n:poss-n:case</ta>
            <ta e="T22" id="Seg_682" s="T21">n-n:poss-n:case</ta>
            <ta e="T23" id="Seg_683" s="T22">n-n&gt;v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T24" id="Seg_684" s="T23">pers-pro:case</ta>
            <ta e="T25" id="Seg_685" s="T24">n-n&gt;adj</ta>
            <ta e="T26" id="Seg_686" s="T25">n-n:case</ta>
            <ta e="T27" id="Seg_687" s="T26">n-n:case</ta>
            <ta e="T28" id="Seg_688" s="T27">adj</ta>
            <ta e="T29" id="Seg_689" s="T28">n-n:case</ta>
            <ta e="T30" id="Seg_690" s="T29">n-n:poss-n:case</ta>
            <ta e="T31" id="Seg_691" s="T30">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T32" id="Seg_692" s="T31">adj</ta>
            <ta e="T33" id="Seg_693" s="T32">n-n:case</ta>
            <ta e="T34" id="Seg_694" s="T33">n-n:poss-n:case</ta>
            <ta e="T35" id="Seg_695" s="T34">v-v:tense-v:pred.pn</ta>
            <ta e="T36" id="Seg_696" s="T35">n-n:case</ta>
            <ta e="T37" id="Seg_697" s="T36">n-n:case</ta>
            <ta e="T38" id="Seg_698" s="T37">v-v:tense-v:pred.pn</ta>
            <ta e="T39" id="Seg_699" s="T38">n-n:case</ta>
            <ta e="T40" id="Seg_700" s="T39">n-n:poss-n:case</ta>
            <ta e="T41" id="Seg_701" s="T40">v-v:tense-v:pred.pn</ta>
            <ta e="T42" id="Seg_702" s="T41">n-n:case</ta>
            <ta e="T43" id="Seg_703" s="T42">n-n:case</ta>
            <ta e="T44" id="Seg_704" s="T43">n-n:(poss)-n:case</ta>
            <ta e="T45" id="Seg_705" s="T44">n-n:case</ta>
            <ta e="T46" id="Seg_706" s="T45">n-n:case</ta>
            <ta e="T47" id="Seg_707" s="T46">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T48" id="Seg_708" s="T47">posspr</ta>
            <ta e="T49" id="Seg_709" s="T48">n-n:case</ta>
            <ta e="T50" id="Seg_710" s="T49">n-n:poss-n:case</ta>
            <ta e="T51" id="Seg_711" s="T50">n-n:poss-n:case</ta>
            <ta e="T52" id="Seg_712" s="T51">n-n&gt;v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T53" id="Seg_713" s="T52">pers-pro:case</ta>
            <ta e="T54" id="Seg_714" s="T53">n-n&gt;adj</ta>
            <ta e="T55" id="Seg_715" s="T54">n-n:case</ta>
            <ta e="T56" id="Seg_716" s="T55">n-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_717" s="T0">v</ta>
            <ta e="T2" id="Seg_718" s="T1">n</ta>
            <ta e="T3" id="Seg_719" s="T2">n</ta>
            <ta e="T4" id="Seg_720" s="T3">n</ta>
            <ta e="T5" id="Seg_721" s="T4">v</ta>
            <ta e="T6" id="Seg_722" s="T5">n</ta>
            <ta e="T7" id="Seg_723" s="T6">v</ta>
            <ta e="T8" id="Seg_724" s="T7">n</ta>
            <ta e="T9" id="Seg_725" s="T8">n</ta>
            <ta e="T10" id="Seg_726" s="T9">n</ta>
            <ta e="T11" id="Seg_727" s="T10">v</ta>
            <ta e="T12" id="Seg_728" s="T11">n</ta>
            <ta e="T13" id="Seg_729" s="T12">n</ta>
            <ta e="T14" id="Seg_730" s="T13">n</ta>
            <ta e="T15" id="Seg_731" s="T14">n</ta>
            <ta e="T16" id="Seg_732" s="T15">n</ta>
            <ta e="T17" id="Seg_733" s="T16">n</ta>
            <ta e="T18" id="Seg_734" s="T17">n</ta>
            <ta e="T19" id="Seg_735" s="T18">posspr</ta>
            <ta e="T20" id="Seg_736" s="T19">n</ta>
            <ta e="T21" id="Seg_737" s="T20">n</ta>
            <ta e="T22" id="Seg_738" s="T21">n</ta>
            <ta e="T23" id="Seg_739" s="T22">v</ta>
            <ta e="T24" id="Seg_740" s="T23">pers</ta>
            <ta e="T25" id="Seg_741" s="T24">adj</ta>
            <ta e="T26" id="Seg_742" s="T25">n</ta>
            <ta e="T27" id="Seg_743" s="T26">n</ta>
            <ta e="T28" id="Seg_744" s="T27">adj</ta>
            <ta e="T29" id="Seg_745" s="T28">n</ta>
            <ta e="T30" id="Seg_746" s="T29">n</ta>
            <ta e="T31" id="Seg_747" s="T30">v</ta>
            <ta e="T32" id="Seg_748" s="T31">adj</ta>
            <ta e="T33" id="Seg_749" s="T32">n</ta>
            <ta e="T34" id="Seg_750" s="T33">n</ta>
            <ta e="T35" id="Seg_751" s="T34">v</ta>
            <ta e="T36" id="Seg_752" s="T35">n</ta>
            <ta e="T37" id="Seg_753" s="T36">n</ta>
            <ta e="T38" id="Seg_754" s="T37">v</ta>
            <ta e="T39" id="Seg_755" s="T38">n</ta>
            <ta e="T40" id="Seg_756" s="T39">n</ta>
            <ta e="T41" id="Seg_757" s="T40">v</ta>
            <ta e="T42" id="Seg_758" s="T41">n</ta>
            <ta e="T43" id="Seg_759" s="T42">n</ta>
            <ta e="T44" id="Seg_760" s="T43">n</ta>
            <ta e="T45" id="Seg_761" s="T44">n</ta>
            <ta e="T46" id="Seg_762" s="T45">n</ta>
            <ta e="T47" id="Seg_763" s="T46">n</ta>
            <ta e="T48" id="Seg_764" s="T47">posspr</ta>
            <ta e="T49" id="Seg_765" s="T48">n</ta>
            <ta e="T50" id="Seg_766" s="T49">n</ta>
            <ta e="T51" id="Seg_767" s="T50">n</ta>
            <ta e="T52" id="Seg_768" s="T51">v</ta>
            <ta e="T53" id="Seg_769" s="T52">pers</ta>
            <ta e="T54" id="Seg_770" s="T53">adj</ta>
            <ta e="T55" id="Seg_771" s="T54">n</ta>
            <ta e="T56" id="Seg_772" s="T55">n</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR" />
         <annotation name="SyF" tierref="SyF" />
         <annotation name="IST" tierref="IST" />
         <annotation name="Top" tierref="Top" />
         <annotation name="Foc" tierref="Foc" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T12" id="Seg_773" s="T11">RUS:core</ta>
            <ta e="T37" id="Seg_774" s="T36">RUS:core</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T3" id="Seg_775" s="T0">The sun of my homeland.</ta>
            <ta e="T6" id="Seg_776" s="T3">It warms my heart in morning frost.</ta>
            <ta e="T9" id="Seg_777" s="T6">The star of my home land.</ta>
            <ta e="T12" id="Seg_778" s="T9">It lightens my way in the fog.</ta>
            <ta e="T15" id="Seg_779" s="T12">The carnelian of the orphan's hill.</ta>
            <ta e="T19" id="Seg_780" s="T15">The memories' stone, my poem.</ta>
            <ta e="T23" id="Seg_781" s="T19">It makes remember my tundra land, my tundra.</ta>
            <ta e="T27" id="Seg_782" s="T23">I have got a decorated carnelian stone.</ta>
            <ta e="T31" id="Seg_783" s="T27">The upper wind cools down my heart.</ta>
            <ta e="T35" id="Seg_784" s="T31">The smelly fire brings its smell.</ta>
            <ta e="T38" id="Seg_785" s="T35">The snipe screams at the sea.</ta>
            <ta e="T41" id="Seg_786" s="T38">It imitates the song of the homeland.</ta>
            <ta e="T44" id="Seg_787" s="T41">The carnelian of the orphan's hill.</ta>
            <ta e="T48" id="Seg_788" s="T44">The stone of memories, my poem.</ta>
            <ta e="T52" id="Seg_789" s="T48">He makes remember my tundra land, my tundra.</ta>
            <ta e="T56" id="Seg_790" s="T52">I have got a decorated carnelian stone.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T3" id="Seg_791" s="T0">Die Sonne meines Heimatlandes.</ta>
            <ta e="T6" id="Seg_792" s="T3">Sie wärmt mein Herz im Morgenfrost.</ta>
            <ta e="T9" id="Seg_793" s="T6">Der Stern meines Heimatlandes.</ta>
            <ta e="T12" id="Seg_794" s="T9">Er erleuchtet meinen Weg im Nebel.</ta>
            <ta e="T15" id="Seg_795" s="T12">Der Karneol des Waisenhügels.</ta>
            <ta e="T19" id="Seg_796" s="T15">Der Gedenkstein, mein Gedicht.</ta>
            <ta e="T23" id="Seg_797" s="T19">Er lässt [mich] mein Tundraland, meine Tundra erinnern</ta>
            <ta e="T27" id="Seg_798" s="T23">Ich habe einen verzierten Karneol-Stein.</ta>
            <ta e="T31" id="Seg_799" s="T27">Der obere Wind kühlt mein Herz ab.</ta>
            <ta e="T35" id="Seg_800" s="T31">Das beißende Feuer bringt seinen Geruch.</ta>
            <ta e="T38" id="Seg_801" s="T35">Die Schnepfe schreit am Meer.</ta>
            <ta e="T41" id="Seg_802" s="T38">Sie imitiert das Lied der Heimat.</ta>
            <ta e="T44" id="Seg_803" s="T41">Der Karneol des Waisenhügels.</ta>
            <ta e="T48" id="Seg_804" s="T44">Der Gedenkstein, mein Gedicht.</ta>
            <ta e="T52" id="Seg_805" s="T48">Er lässt [mich] mein Tundraland, meine Tundra erinnern.</ta>
            <ta e="T56" id="Seg_806" s="T52">Ich habe einen verzierten Karneolstein.</ta>
         </annotation>
         <annotation name="fr" tierref="fr" />
         <annotation name="ltr" tierref="ltr">
            <ta e="T3" id="Seg_807" s="T0">Родной земли солнце.</ta>
            <ta e="T6" id="Seg_808" s="T3">Сердце согревает в прохладу.</ta>
            <ta e="T9" id="Seg_809" s="T6">Родной земли звезда.</ta>
            <ta e="T12" id="Seg_810" s="T9">Дорогу освещает в туман.</ta>
            <ta e="T15" id="Seg_811" s="T12">Сирота долина сердолик.</ta>
            <ta e="T19" id="Seg_812" s="T15">Памятный камень стих мой.</ta>
            <ta e="T23" id="Seg_813" s="T19">Тундру море напоминает.</ta>
            <ta e="T27" id="Seg_814" s="T23">Мне с рисунком сердолик-камень.</ta>
            <ta e="T31" id="Seg_815" s="T27">Верхний ветер сердце охлаждает.</ta>
            <ta e="T35" id="Seg_816" s="T31">Резкий костра запах приносит.</ta>
            <ta e="T38" id="Seg_817" s="T35">Кулик у моря кричит.</ta>
            <ta e="T41" id="Seg_818" s="T38">Родины песню повторяет.</ta>
            <ta e="T44" id="Seg_819" s="T41">Сирота долина сердолиг.</ta>
            <ta e="T48" id="Seg_820" s="T44">Памятный камень стих мой.</ta>
            <ta e="T52" id="Seg_821" s="T48">Тундру море напоминает.</ta>
            <ta e="T56" id="Seg_822" s="T52">Мне с рисунком сердолик-камень.</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
